get_one_file() {
  # Lookup one file according to current options.

    basic_copycmd="cp -f -L -R"
    basic_linkcmd="ln -s -f -n"
    copycmd=${basic_linkcmd}
    tempfile="/tmp/getfile_$$"
    opt=$1
    file=$2

    # Next loop is written with idea to break it when first match is found
    status_=
    if [ "$opt" == "jo" ]; then
        path_opt="./ ${JOBOPTSEARCHPATH//:/ }"
    elif [ "$opt" == "data" ]; then
        path_opt=${DATAPATH//:/ }
    elif [ "$opt" == "scripts" ]; then
      # Only keep PATH entries located in the installation areas to find scripts.
        path_opt=$(echo -e ${PATH//:/'\n'} | fgrep /InstallArea/)
    elif [ "$opt" == "xmls" ]; then
        path_opt=${XMLPATH//:/ }
    fi

  # Find strategy:
  #   ignore CVS stuff and backup files;
  #   use 'find -path' to honor package name (partial directory
  #   specification) and permit wildcard requests.
    
    for dir in ${path_opt}; do
        if [ -d $dir ]; then
            find $dir -mindepth 1 -maxdepth 2 -path "*/${file}" | \
              egrep -v '~|CVS' >> ${tempfile}
            if [ -s ${tempfile} ]; then status_="FOUND"; fi
            if [[ -n "$status_" && -z "$do_warn" ]]; then break; fi
        fi
    done
    
  # Copy/link file into current directory if something was found.
  # Do nothing if found file is from current directory;
  # Handle possible multiple matches - the first one is returned in any case.
  # Do not copy file if '-list' was specified.
    
    if [ -n "$status_" ]; then
        nfile=$(wc -l < ${tempfile}) ; nfile=$(echo $nfile) # strip spaces
        if [[ $nfile != "1" || -n "$do_list" ]]; then
            msg="Warning:"; if [ -n "$do_list" ]; then msg="Note:"; fi
            echo "$msg ${nfile} files found for '${file}':"
            echo "---------------------------------------------------"
            cat ${tempfile}
            echo "---------------------------------------------------"
        fi
        read fspec < ${tempfile}
        rm -f ${tempfile}
        if [ -z "$do_list" ]; then
            bname=${fspec##*/}  # bname=$(basename $fspec)
            echo -e "Willing to acquire file ${file} from\n${fspec}"
            if [[ "$fspec" == "./$bname" || "$fspec" == "$PWD/$bname" ]]; then
                echo "File $fspec is from current directory"
            else
                msg="copy"; if [ -z "$do_copy" ]; then msg="symlink"; fi
                if [ -f ./$bname ]; then
                    echo "File $bname exists in the current directory"
                    if [ -n "$do_keep" ]; then
                        echo "Keep $bname"
                    else
                        echo "Remove and $msg $bname"
                        rm -f $bname
                        $copycmd ${fspec} $bname
                    fi
                else
                    echo "$msg $bname"
                    $copycmd ${fspec} $bname
                fi
            fi
        fi
    else
        echo "Warning: get_files: nothing found for ${file}"
        if [ -z "$do_list" ]; then final_status=1; fi
    fi
}
