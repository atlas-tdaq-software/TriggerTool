# Script starts the trigger tool

# Setup Java
export JAVA_VER="1.8.0"
export _JAVA_OPTIONS="-Xms256m -Xmx1048m"
source /afs/cern.ch/sw/lcg/external/Java/bin/setup.sh

get_files -symlink -data TriggerTool.jar

echo "Starting TriggerTool using JAVA_HOME: $JAVA_HOME"

if test $# = 0; then
    java -jar TriggerTool.jar
else
    java -jar TriggerTool.jar $*
fi

