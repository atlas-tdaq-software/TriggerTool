# Script starts the trigger tool

# Increase amount of memory for java
export _JAVA_OPTIONS="-Xms250m -Xmx1400m"

# Authentication files
export CORAL_DBLOOKUP_PATH=${TRIGGER_EXP_CORAL_PATH:-/afs/cern.ch/user/a/attrgcnf/TriggerTool}
export CORAL_AUTH_PATH=${TRIGGER_EXP_CORAL_PATH:-/afs/cern.ch/user/a/attrgcnf/TriggerTool}

#JAVAEXE=/afs/cern.ch/sw/lcg/external/Java/JDK/1.6.0/ia32/jre/bin/java
JAVAEXE=/afs/cern.ch/sw/lcg/external/Java/JDK/Oracle_1.8.0_31/amd64/bin/java

TTPATH=${USERTTPATH:-/afs/cern.ch/user/a/attrgcnf/TriggerTool/jar}
LOGDIR=${USERLOGDIR:-"/tmp/"${USER}"/TriggerToolLogs/"}
TTJAR=${USERTTJAR:-TriggerTool-03-00-00-07.jar}

ARGS=$*;


## Check whether we are in command line more; if so log to terminal
COMMANDS=( -diff -gk -cp -down -r -psup -up -g -tDb )
GUIMODE=true
for c in ${COMMANDS[@]}
  do
  if [[ $* == *${c}* ]]
      then
      echo $* contains $c
      GUIMODE=false
  fi
done
## if we are in Command Line mode, log to term
## else create log dir and log to file 
if $GUIMODE
    then
#    echo "GUI"
    ## Create Logdir
    mkdir -p $LOGDIR
    ARGS="-l FINE -o $LOGDIR "$ARGS
else 
#    echo "CLI"
    LOGDIR="TERMINAL"
fi


# Start the TriggerTool.
echo "starting ${TTJAR}"
echo -e "\t Java       $JAVAEXE"
echo -e "\t Options    $ARGS"
echo -e "\t Logging to $LOGDIR "
echo -e "\t authentication uses"
echo -e "\t ${CORAL_AUTH_PATH}/authentication.xml"
echo -e "\t ${CORAL_DBLOOKUP_PATH}/dblookup.xml"
echo "$JAVAEXE -cp ${TTPATH}/${TTJAR}:${TTPATH}/TrigDb.jar triggertool.TriggerTool $ARGS"
$JAVAEXE -cp ${TTPATH}/${TTJAR}:${TTPATH}/TrigDb.jar triggertool.TriggerTool $ARGS

# End.
