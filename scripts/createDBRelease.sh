#!/usr/bin/env zsh

# <paul.bell@cern.ch>, 29-Apr-2009
####################################################################
# Unlike replicateTriggerDB.py this first puts the menus from the 
# release into atlr and then replicates them to the sqlite file!
####################################################################
#
#some setup options - these are for testing only: oracledb, location of sqlite file, who to email
sqlitefile="/afs/cern.ch/user/p/pbell/testrep/test.db" 
connection="Oracle://devdb10/ATLAS_PBELL;username=ATLAS_PBELL;password=xxxxx"
email=paul.bell@cern.ch
#
####################################################################
release=$AtlasVersion
echo "---------------------------------------------------------------"
echo "Will upload MC menus to ATLR and replicate to DBRelease file"
echo "In case of problems please email atlas-trigconf-support@cern.ch"
echo "---------------------------------------------------------------"
echo "***** The release is: $release *****"

#setup java and the triggertool
echo "***** Setting java version and the TriggerTool *****"
source /afs/cern.ch/sw/lcg/external/Java/bin/setup.sh
get_files -data -symlink TriggerTool.jar
export _JAVA_OPTIONS="-Xms256m -Xmx1048m"

#link to the menu files we want to upload
menu_names=(
    # lumi1E31
    #lumi1E31
    #lumi1E31_no_prescale
    # lumi1E32
    lumi1E32
    #lumi1E32_no_prescale
    # lumi1E33
    #lumi1E33
    #lumi1E33_no_prescale
    # cosmic
    #Cosmic_v1
)

#loop over menu files and upload them
((i = 1))
for menu in $menu_names; do
    echo "***** Linking to menu: $menu *****"
    get_files -xmls -symlink HLTconfig_${menu}_${release}.xml
    get_files -xmls -symlink LVL1config_${menu}_${release}.xml

    echo "***** Uploading menu $menu to MC DB on ATLR *****"
    java -jar TriggerTool.jar -up -release $release -l1_menu LVL1config_${menu}_${release}.xml -hlt_menu HLTconfig_${menu}_${release}.xml -name $menu -offlineconfig -markreplicate -dbConn $connection
    
    ((i = $i+1))
done

#now go over the keys marked for replication and put those in the sqlite file - this will try all of them, but those
#already replicated won't be copied again!
echo "***** Now replicating to sqlite file: $sqlitefile *****"
java -jar TriggerTool.jar -replicate $sqlitefile  -dbConn $connection &>! replicatelog

#finish
if grep -q "0 rows were affected" replicatelog
then
    echo "***** Done with replication: no update to DBRelease *****"
else
    echo "***** Done with replication: updated file $sqlitefile, email notification sent *****"
    echo "Hello, \n\nThis is an automated message to inform you that a new TriggerDB DBRelease file has been created in $sqlitefile following the build of $release. \n\nPlease can this be added to the shipped DB Release file? \n\nMany thanks, \n\n   Atlas Trig-Config" | mail -s "new TriggerDB DBRelease" $email
fi

