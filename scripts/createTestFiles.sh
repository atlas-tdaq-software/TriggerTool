#!/bin/bash
# author: Tiago Perez <tperez@cern.ch>
# date:   2010-12-20
# Creates control files needed for the JUnit test. 
# This only needs to be run once per "reference" TriggerTool.jar
## You need to run fisrt $> ant maketest in the ant dir.
#
# Need to provide path to ref TriggerTool.jar
if [ $# -eq '0' ] 
    then
    echo "please provide the path to the reference TriggerTool jar file.";
else 
    TT=$1;
fi;
## dir with jars needed.
TTTEST="/Users/tiago/atlas/TriggerTool/trunk/lib/TriggerToolTester.jar"
export CLASSPATH=${TTTEST}:${TT};
echo "classpath ${CLASSPATH}"
echo "start test"
java triggertool.junit.TriggerToolPrepareTest
echo "test done"
