#!/usr/bin/env python
#
# ----------------------------------------------------------------
# Script : replicateTriggerDB.py
# Project: DetCommon
# Package: Trigger/TrigConfiguration/TriggerTool
# Purpose: Script to replicate the trigger information into triggerDB for DBRelease
# Authors: Martin Goebel (DESY), Joerg Stelzer (DESY)
# Created: Dez 9, 2008
# ----------------------------------------------------------------

import os,sys,time
from commands import getoutput
from re import match
import subprocess # Popen, PIPE, call
from subprocess import Popen, PIPE, call

def _getTTLib2():
    """Find the TriggerTool library in $DATAPATH"""
    for p in os.environ["DATAPATH"].split(os.pathsep):
        ttlibcand = "%s/TriggerTool/TriggerTool.jar" % p
        try: os.lstat(ttlibcand)
        except: continue
        ttlib=ttlibcand
        break
    return ttlib

def _getTTLib():
    """Find the TriggerTool library in $DATAPATH"""
    try: os.remove("TriggerTool.jar")
    except OSError: pass
    call( ("get_files -data -symlink %s" % "TriggerTool.jar" ).split(),  stdout=FHout(), stderr=FHerr() )
    return "TriggerTool.jar"

def _getSQLiteDB():
    """Returns the location of the destination triggerDB"""
    sqlitefile  = "/tmp/%s/triggerDBMC.db" % os.environ["USER"]
    return sqlitefile

def _findNextRelease():
    from AthenaCommon.AppMgr import release_metadata
    return release_metadata()['release']

def _checkJavaVersion():
    """check for java >1.5"""
    version = getoutput("java -fullversion")
    print >>FHout(), version
    if not match('.*"1.[56]', version):
        print >>FHerr(), "Error: You need to set up java version > 1.5! [current: %s]" % version
        return False
    return True

def checkFile(fn,abortWhenMissing=True):
    """Returns true if the filename fn exists"""
    fileexists = os.path.exists(fn) and not os.path.isdir(fn)
    if not fileexists:
        print >>FHerr(), "File %s does not exist or is a directory" % fn
        if abortWhenMissing:
            print >>FHerr(), "Abort!"
            sys.exit(1)
    return fileexists

def FHout():
    return None#Replication.FHout

def FHerr():
    return None#Replication.FHerr

def Flush():
    if FHout(): FHout().flush()
    if FHerr(): FHerr().flush()



class Replication:

    TT    = None
    FHout = None
    FHerr = None
    nextRelease = None
    NightlyDB   = None
    #MCTriggerDB  ="Oracle://devdb10/ATLAS_PBELL;username=ATLAS_PBELL;password=PaulTrig2009"
    MCTriggerDB ="oracle://devdb10/ATLAS_TRIG_STELZER;username=ATLAS_TRIG_STELZER;password=mytrig2008"
    ReplicaDB   = None
    project     = os.environ["AtlasProject"]
    version     = os.environ["AtlasVersion"]
    verbose     = False
    execCommands= True
    mode = ''
    
    def __init__(self, mode='sqlite2oracle'):
        Replication.mode = mode


    def prepare(self):
        timestr = time.strftime("%Y.%m.%d:%H:%M:%S",time.gmtime())
        Replication.FHout = open("logReplTriggerDB_%s.out" % timestr,'w')
        Replication.FHerr = open("logReplTriggerDB_%s.err" % timestr,'w')
        if not _checkJavaVersion():
            return False
        Replication._nextRelease()
        Replication._TT()
        Replication._SQLiteDB()
        print >>FHout(), """
**********************************************************************

Stage 0: Preparing the TriggerDB Replication Job

         This job consists of two stages
         * Copying of the menus from the nightly sqlite DB to the oracle MC TriggerDB
         * Replicating the MC TriggerDB to the sqlite TriggerDB in the DBRelease

Parameters:
         Next release            : %s
         TT library              : %s
         Nightly TriggerDB       : %s
         MC TriggerDB            : %s
         TriggerDB for DBRelease : %s

**********************************************************************
""" % ( Replication.nextRelease,
        Replication.TT,
        Replication.NightlyDB,
        Replication.MCTriggerDB,
        Replication.ReplicaDB )
        return True

    def close(self):
        if FHout(): FHout().close()
        if FHerr(): FHerr().close()

    @classmethod
    def _TT(cls):
        if not Replication.TT:
            os.environ["_JAVA_OPTIONS"]="-Xms256m -Xmx1024m" 
            Replication.TT = _getTTLib()

    @classmethod
    def _nextRelease(cls):
        if not Replication.nextRelease:
            Replication.nextRelease=_findNextRelease()
  
    @classmethod
    def _SQLiteDB(cls):
        if not Replication.ReplicaDB:
            Replication.ReplicaDB=_getSQLiteDB()
        if not Replication.NightlyDB and Replication.mode=='sqlite2oracle':
            Replication.NightlyDB = "TriggerMenuSQLiteFile_%s.sqlite" % Replication.nextRelease
            if not os.path.exists(Replication.NightlyDB):
                call( ("get_files -data -symlink %s" % Replication.NightlyDB ).split(),  stdout=FHout(), stderr=FHerr() )
            checkFile(Replication.NightlyDB)


  
    def loadMenusIntoOracleMCDB(self):
        self.availablemenunames = [ "MC_lumi1E31", "MC_lumi1E31_no_prescale",
                               "MC_lumi1E32", "MC_lumi1E32_no_prescale",
                               "MC_lumi1E33", "MC_lumi1E33_no_prescale",
                               "Cosmic_v1" ]

        # [upload,replicate]
        self.menus = { "MC_lumi1E31"             : [1,1],
                       "MC_lumi1E31_no_prescale" : [0,0],
                       "MC_lumi1E32"             : [0,0],
                       "MC_lumi1E32_no_prescale" : [0,0],
                       "MC_lumi1E33"             : [0,0],
                       "MC_lumi1E33_no_prescale" : [0,0],
                       "Cosmic_v1"               : [0,0]
                       }

        if self.mode == 'sqlite2oracle':
            success = self._createKeysDict()
            if not success:
                return False
            else:
                print >>FHout(),"    --> Success"
            
        if self.mode == 'sqlite2oracle':
            menustocopy = [m for m in self.availablemenunames if self.menus[m][0]==1]
            print >>FHout(), """
**********************************************************************

Stage 1: Copying menus from nightly database to the MC TriggerDB

         The MC TriggerDB is the primary source of all persistified
         trigger menus used for simulation.

Parameters:
         Menus to copy     : %s
         Keys for Menus    :""" % ', '.join(menustocopy),

            for menu in menustocopy[:1]:
                print >>FHout(), "- %s [v%s]: SMK=%s" % (menu, self.menuKeys[menu]['version'], self.menuKeys[menu]['smk'])
            for menu in menustocopy[1:]:
                print >>FHout(), "                             - %s [v%s]: SMK=%s" % (menu, self.menuKeys[menu]['version'], self.menuKeys[menu]['smk'])

            print >>FHout(), """         Source Nightly DB : %s
         Destination DB    : TRIGGERDBMC

**********************************************************************
""" % Replication.NightlyDB
            
        for menuname in self.availablemenunames:
            if self.mode == 'xml2oracle':
                self._uploadMenu(menuname)
            elif self.mode == 'sqlite2oracle':
                self._copyMenu(menuname)


    def _uploadMenu(self,menuname):
        """Loads menu from xml file into destination database"""
        nextRelease = Replication.nextRelease
        
        [doUpload, doReplicate] = self.menus[menuname]
        if not doUpload: return
        print >>FHout(), "***** Linking to menu: %s *****" % menuname
        l1menu = "LVL1config_%s_%s.xml" % (menuname,nextRelease)
        hltmenu = "HLTconfig_%s_%s.xml" % (menuname,nextRelease)
        Flush()
        call( ("get_files -xmls -symlink %s" % l1menu ).split(),  stdout=FHout(), stderr=FHerr() )
        call( ("get_files -xmls -symlink %s" % hltmenu ).split(), stdout=FHout(), stderr=FHerr() )

        ff = True
        if not checkFile(l1menu):
            print >>FHerr(), "Could not find l1 menu %s in the release, will not upload" % l1menu
            ff = False
        if not checkFile(hltmenu):
            print >>FHerr(), "Could not find hlt menu %s in the release, will not upload" % hltmenu
            ff = False
        if not ff:
            return

        print >>FHout(), "... Uploading menu %s to MC DB on ATLR" % menuname
        cmd  = "java -jar %s -up -rel %s -l1 %s -hlt %s -n %s" % (Replication.TT, nextRelease, l1menu, hltmenu, menuname)
        cmd += " -off -dbConn '%s'" % Replication.MCTriggerDB
        if doReplicate:
            cmd += " -mr"
        print >>FHout(),cmd
        Flush()
        if Replication.execCommands:
            call( cmd.split(), stdout=FHout(), stderr=FHerr() )
            

    def _createKeysDict(self):
        """Creates a dictionary of keys from the information that is in the nightly SQLite database.
        It uses the TT getKeys function to retrieve this information.
        Returned is a dictionary with the (menuname,version) as index, providing access to all other information
        """
        print >>FHout(),"... Getting the configuration keys from the nightly triggerdb '%s'" % Replication.NightlyDB  
        cmd  = "java -jar %s -gk '%s' -db 'sqlite_file:%s'" % (Replication.TT, Replication.nextRelease, Replication.NightlyDB)
        print >>FHout(),"    [Command: %s]" % cmd
        Flush()

        if Replication.execCommands:
            res = Popen( cmd.replace("'",'').split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE ).communicate()[0].splitlines()
            lines = [l for l in res if l.startswith('GETKEYS')]
        else:
            f = open("testoutput.txt")
            lines = [l.rstrip() for l in f.readlines() if l.startswith('GETKEYS ')]
            f.close()

        nSMK = 0
        index = (0,0)
        readMenuKeys = {}
        for l in lines:
            desc,val = l.split(None,2)[1:3]
            if desc == 'nSMK':
                nSMK = int(val)
                continue

            key,name,ver = val.split()
            key  = int(key)
            name = name.strip('"')
            ver  = int(ver)

            if desc == 'SMK':
                index = (name,ver)
                readMenuKeys[index] = {}
                readMenuKeys[index]['smk'] = key
            if desc == 'L1PSK':
                readMenuKeys[index].setdefault('l1psk',{}).update( {name:(ver,key)})
            if desc == 'HLTPSK':
                readMenuKeys[index].setdefault('hltpsk',{}).update({name:(ver,key)})

        if nSMK==0:
            print lines
            print >>FHerr(),"No SMK found in nightly db"
            return False

        if nSMK != len(readMenuKeys):
            print >>FHerr(),"Read %i menus in nightly db, but TT says %r" % ( len(readMenuKeys), nSMK )
            return False

        if Replication.verbose:
            for menu in readMenuKeys:
                print >>FHout(), "    %s / v%s" % menu
                print >>FHout(), "        SMK    : %s" % readMenuKeys[menu]['smk']
                print >>FHout(), "        L1 PSK : %r" % readMenuKeys[menu]['l1psk']
                print >>FHout(), "        HLT PSK: %r" % readMenuKeys[menu]['hltpsk']

        self.menuKeys = {}

        lastMenuVersion = {}  # last version of a menu
        for menu in readMenuKeys:
            menuname = menu[0]
            if not menuname in lastMenuVersion  or (menuname in lastMenuVersion and lastMenuVersion[menuname]<menu[1]):
                lastMenuVersion[menuname]=menu[1]
                
        for menuname in lastMenuVersion:
            self.menuKeys[menuname] = readMenuKeys[(menuname,lastMenuVersion[menuname])]
            self.menuKeys[menuname]['version'] = lastMenuVersion[menuname]
        return True

                

    def _copyMenu(self,menuname):
        """Loads menu from xml file into destination database"""
        nextRelease = Replication.nextRelease
        
        [doCopy, doReplicate] = self.menus[menuname]
        if not doCopy: return

        rs = ''
        if doReplicate: rs = '[r]'
        print >>FHout(), "... Copying from %s to %s: %s %s" % (Replication.NightlyDB, Replication.MCTriggerDB, menuname,rs)
        smk = self._getKeys(menuname)
        
        cmd  = 'java -jar %s -cp -k %i -m "Default Menu for Release"' % (Replication.TT, smk)
        cmd += " -off -db 'sqlite_file:%s' -wDb '%s'" % (Replication.NightlyDB,Replication.MCTriggerDB)
        if doReplicate:
            cmd += " -mr"
        print >>FHout(),"    [Command: %s]" % cmd
        Flush()
        if Replication.execCommands:
            call( cmd.replace("'","").split(), stdout=FHout(), stderr=FHerr() )
            
    def _getKeys(self,menuname):
        smkkey = self.menuKeys[menuname]['smk']
        return smkkey
        
    def replicateMenus(self):
        # execute TriggerTool and replicate tagged configurations
        print >>FHout(), """
**********************************************************************

Stage 2: Replicating menus from the MC TriggerDB to the replica for
         the DBRelease

Parameters:
         Menus to replicate : %r
         MC Trigger DB      : %s
         Destination DB     : %s

**********************************************************************
""" % ([m for m in self.availablemenunames if self.menus[m][1]==1], Replication.MCTriggerDB, Replication.ReplicaDB)


        # cmd="echo java -jar %s --dbConn MCDB --offlineconfig --replicate %s" % (ttlib, sqlitefile)
        cmd="java -jar %s -dbConn '%s' -off -r %s" % (Replication.TT, Replication.MCTriggerDB, Replication.ReplicaDB)
        print >>FHout(),"    [Command: %s]" % cmd

        Flush()
        if Replication.execCommands:
            call(cmd.split(), stdout=FHout(), stderr=FHerr())

        



def doRun(replication):
    success = rep.prepare()
    if not success: return False
        
    success = rep.loadMenusIntoOracleMCDB()    
    if not success: return False

    success = rep.replicateMenus()
    if not success: return False

    rep.close()
    return True



if __name__ == "__main__":

    user = os.getenv("USER")
    if user!='stelzer' and user!='pbell':
        sys.exit(0)

    rep = Replication()
    success = doRun(rep)

    if success:
        print >>FHout(), """
**********************************************************************

Done with replication to file %s

**********************************************************************
""" % Replication.ReplicaDB
    else:
        logfile = ""
        if FHerr():
            logfile = ", see error file %r" % FHerr().name
        print >>FHout(), """
**********************************************************************

Replication failed%s

**********************************************************************
""" % logfile
        

    if FHout() and FHerr():
        print "Done! For more information see log file\n\t%s\n\t%s" % (FHout().name,FHerr().name)
