/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.SMT;

import java.sql.SQLException;
import javax.swing.JFrame;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggertool.Components.InputDialog;

/**
 *
 * @author Michele
 */
public class SuperMasterTableInteractor {
        
    private final SuperMasterTable smt;

    /**
     *
     * @param smt
     */
    public SuperMasterTableInteractor(SuperMasterTable smt)
    {
        this.smt = smt;
    }

    /**
     *
     * @param parent
     * @throws SQLException
     */
    public void editComment(JFrame parent)
            throws SQLException {
        InputDialog nd = new InputDialog(smt.getComment(), "Super Master Table comment: ", InputDialog.Panel.SMT, parent);
        String newcomment = nd.getNewval();
        if (newcomment != null && !newcomment.equals(smt.getComment())) {
            smt.setComment(newcomment);
        }
    }
}
