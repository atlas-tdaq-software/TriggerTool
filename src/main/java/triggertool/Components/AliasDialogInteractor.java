/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.Components;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.HLT.HLTTriggerMenu;
import triggerdb.Entities.L1.L1Menu;
import triggerdb.Entities.L1.L1Prescale;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.PrescaleSetAliasComponent;
import triggertool.XML.XMLprescaleUploaderCLI;

/**
 *
 * @author Michele
 */
public final class AliasDialogInteractor {
    /**
     * the logger.
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");     /**
     * The current SMT
     */
    public SuperMasterTable SMT = null;
    /**
     * The current L1 menu.
     */
    private L1Menu l1menu = null;
    /**
     * The current HLT menu.
     */
    private HLTTriggerMenu hltmenu = null;
    /**
     * Lists of aliases linked to the SMT.
     */
    public List<PrescaleSetAliasComponent> aliasList;
    /**
     * Set with the distinct ALIAS names: PHYSICS, STAND BY...
     */
    public final HashSet<String> aliasSetNames = new HashSet<>();
    /**
     * The Map of aliases linked to each AliasSet: "1318b, 768b, Muon..."
     */
    HashMap<String, HashSet<String>> aliasNamesMap = new HashMap<>();
    Vector<L1Prescale> L1PSKs = new Vector<>();
    Vector<HLTPrescaleSet> HLTPSKs = new Vector<>(); 

    /**
     *
     */
    public static final String EMPTY_STR = "--";
    
    private java.awt.Frame parent;
    
    /**
     *
     * @param s
     * @param parent
     * @throws SQLException
     */
    public AliasDialogInteractor(SuperMasterTable s, java.awt.Frame parent) throws SQLException
    {
        SMT = s;
        l1menu = SMT.get_l1_master_table().get_menu();
        hltmenu = SMT.get_hlt_master_table().get_menu();
        getSets();
        getAliases();
        this.parent = parent;
    }
    
    /**
    * Gets the existing aliases details from the DB
    * @throws java.sql.SQLException
    */
    public void getAliases() throws SQLException {
        this.aliasList = this.SMT.getAliases();
        List<PrescaleSetAliasComponent> _aliasList = this.aliasList;
        logger.log(Level.INFO, " SMK : {0}  number of alias: {1}", new Object[]{this.SMT.get_id(), _aliasList.size()});

        for (PrescaleSetAliasComponent alias : _aliasList) {
            aliasSetNames.add(alias.get_name());
        }
        for (String name : aliasSetNames) {
            HashSet<String> names_free = new HashSet<>();
            for (PrescaleSetAliasComponent alias : _aliasList) {
                if (alias.get_name().equals(name)) {
                    names_free.add(alias.get_name_free());
                }
            }
            aliasNamesMap.put(name, names_free);
        }
    }

    /**
    *
    * Gets the l1/hlt prescale sets
    *
    * @throws java.sql.SQLException
    */
    public void getSets() throws SQLException {
        List<L1Prescale> L1PSK = l1menu.getPrescaleSets();
        for (L1Prescale PSK : L1PSK) {
            L1PSKs.add(PSK);
        }
        List<HLTPrescaleSet> HLTPSK = hltmenu.getPrescaleSets();
        for (HLTPrescaleSet PSK : HLTPSK) {
            HLTPSKs.add(PSK);//.get_id() + ": " + PSK.get_name());
        }
    }
   
    /**
     *
     * @param psac
     */
    public void fillAliasNames(PrescaleSetAliasComponent psac) {
        if (psac != null && !psac.get_name().equals(EMPTY_STR)) {
            String setName = psac.get_name();
            String aliasName = psac.get_name_free();
            aliasSetNames.remove(EMPTY_STR);
            aliasSetNames.add(psac.get_name());
            //
            aliasNamesMap.remove(EMPTY_STR);
            if (aliasNamesMap.containsKey(setName)) {
                HashSet<String> names_free = aliasNamesMap.get(setName);
                names_free.add(aliasName);
                aliasNamesMap.put(setName, names_free);
            } else {
                HashSet<String> names_free = new HashSet<>();
                names_free.add(aliasName);
                aliasNamesMap.put(setName, names_free);
            }
        }
    }  
    
    /**
     *
     * @param filePath
     * @param results
     * @return
     * @throws ParserConfigurationException
     * @throws SQLException
     * @throws SAXException
     * @throws IOException
     */
    public boolean loadXML(String filePath, ArrayList<String> results) throws ParserConfigurationException, SQLException, SAXException, IOException{
        //Check if the path is pointing to a valid alias xml file.
        boolean check = checkFile(filePath.trim());
        boolean success = false;
        if(check){
            XMLprescaleUploaderCLI xmlAlias = new XMLprescaleUploaderCLI(filePath.trim(), this.parent);
            try{
                for(String file: xmlAlias.xmlFiles){
                    success = xmlAlias.isAliasSummaryFileThenLoad(file ,l1menu, hltmenu, SMT, results);
                }
            } catch (ParserConfigurationException ex) {
              logger.log(Level.SEVERE, "Could not configure parser: {0}", ex.toString());
              return false;
            } catch (SAXException ex) {
              logger.log(Level.SEVERE, "Could not parse xml file:{0}", ex.toString());
              return false;
            } catch (IOException ex) {
              logger.log(Level.SEVERE, "Could not read the file:{0}", ex.toString());
              return false;
            }
        }
        return success;
    }
    
        /**
     * Check if a folder contains an alias xml exists.
     * @param filePath
     * @return 
     * @throws javax.xml.parsers.ParserConfigurationException 
     * @throws org.xml.sax.SAXException 
     * @throws java.io.IOException 
     * @throws java.io.FileNotFoundException 
     */
    public boolean checkFile(String filePath) throws ParserConfigurationException, SAXException, IOException, NullPointerException, java.io.FileNotFoundException{
        if(filePath.isEmpty()){
            return false;
        }
        File f = new File(filePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(f);
        doc.getDocumentElement().normalize();
        return doc.getDocumentElement().getNodeName().equals("ALIAS_DEFINITION");
    }
    
}
