/*
 * NewPrescaleEdit.java
 *
 * Created on November 2, 2009, Paul Bell
 */
package triggertool.Components;

import triggerdb.Connections.ConnectionManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.JFrame;
import triggerdb.Entities.HLT.HLTComponent;
import triggerdb.Entities.HLT.HLTPrescale;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.HLT.HLTPrescaleType;
import triggerdb.Entities.HLT.HLTTriggerChain;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggertool.TTExceptionHandler;

/**
 * @author Paul Bell
 */
public final class AlgosDialog extends JDialog implements ListSelectionListener {

    private HLTComponent theAlgo;
    private int hlt_menu_id = -1;
    private int l1_menu_id = -1;
    private HLTPrescaleSet hlt_psset = null;

    private final ArrayList<String> allAlgos_v = new ArrayList<>();
    private final DefaultListModel<String> AlgoItems = new DefaultListModel<>();
    private ArrayList<HLTTriggerChain> chains_v;
    private ArrayList<L1Item> items_v;

    private DefaultTableModel ChainsModel = new DefaultTableModel();
    private DefaultTableModel ItemsModel = new DefaultTableModel();

    //trick so we know which was last selected
    HashMap<String, String> lowerchainnames_hs = null;
    String lowerchainnames = null;
    ArrayList<L1Item> matcheditems_v = null;

    /**
     * Required by Java
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");

    /**
     * Creates new form PrescaleEdit
     *
     * @param smt
     * @throws java.sql.SQLException
     */
    public AlgosDialog(SuperMasterTable smt, JFrame parent)
            throws SQLException {
        super((JDialog) null);
        try {
            hlt_menu_id = smt.get_hlt_master_table().get_menu_id();
            l1_menu_id = smt.get_l1_master_table().get_menu_id();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, rootPane, "Error while creating the Algo panel.");
        }

        //hlt_menu = smt.get_hlt_master_table().get_menu();
        //check which mode
        //mode = ConnectionManager.getInstance().getInitInfo().getMode();
        //find the algos
        getAlgos();

        //get all chains
        //draw the dialog
        initComponents();
        setTitle("Algorithms: SMT " + smt);
        FilterText.setText("");
        getSets();

        //getAllChains();
        //add list listener and set table model 
        listTablesAlgos.addListSelectionListener(this);
        ChainsModel = (DefaultTableModel) tableChains.getModel();
        TableSorter sorter1 = new TableSorter(tableChains.getModel());
        tableChains.setModel(sorter1);
        JTableHeader header1 = tableChains.getTableHeader();
        sorter1.setTableHeader(header1);
        tableChains.setColumnSelectionAllowed(false);
        tableChains.setRowSelectionAllowed(true);

        tableChains.getColumnModel().getColumn(0).setPreferredWidth(200);
        tableChains.getColumnModel().getColumn(1).setPreferredWidth(50);
        tableChains.getColumnModel().getColumn(2).setPreferredWidth(50);

        ItemsModel = (DefaultTableModel) tableItems.getModel();
        TableSorter sorter2 = new TableSorter(tableItems.getModel());
        tableItems.setModel(sorter2);
        JTableHeader header2 = tableItems.getTableHeader();
        sorter2.setTableHeader(header2);

        setLocationRelativeTo(parent);
        setVisible(true);
        System.out.println("done");

    }

    //load combo box of hlt prescale sets to apply
    /**
     *
     */
    public void getSets() {

        PSComboBox.removeAllItems();

        //get HLT prescales
        String query = "SELECT HPS_ID, HPS_NAME, HPS_VERSION, HTM2PS_PRESCALE_SET_ID, HTM2PS_TRIGGER_MENU_ID "
                + "FROM "
                + "HLT_PRESCALE_SET, HLT_TM_TO_PS "
                + "WHERE "
                + "HTM2PS_TRIGGER_MENU_ID=? "
                + "AND "
                + "HTM2PS_PRESCALE_SET_ID=HPS_ID "
                + "ORDER BY HPS_ID DESC";

        query = ConnectionManager.getInstance().fix_schema_name(query);

        try {
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ps.setInt(1, hlt_menu_id);
            ResultSet rset = ps.executeQuery();
            //PSComboBox.addItem("No set selected");
            while (rset.next()) {
                //HLTPrescaleSet hltps = new HLTPrescaleSet(rset.getInt(1));
                PSComboBox.addItem(rset.getInt("HPS_ID") + "   " + rset.getString("HPS_NAME") + " v" + rset.getString("HPS_VERSION"));
            }
            rset.close();
            ps.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Exception: {0}", ex);
        }
    }

    /**
     * When the user clicks on an algo in the list show the chains
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {

        ////////////////////////////////////////////////////////////////////////
        //clicked on list of streams
        if (e.getSource() == listTablesAlgos) {

            if (e.getValueIsAdjusting()) {
                return;
            }

            try {
                theAlgo = new HLTComponent(Integer.parseInt(listTablesAlgos.getSelectedValue().split(":")[0]));
            } catch (SQLException ex) {
                TTExceptionHandler.HandleSqlException(ex, rootPane, "Value changed triggered an error in retrieving HLT component: ");
            }

            logger.log(Level.INFO, "Selected algo: {0}", theAlgo);

            //show all chains for this algo
            if (theAlgo != null) {
                try {
                    getChains(theAlgo);
                } catch (SQLException ex) {
                    TTExceptionHandler.HandleSqlException(ex, rootPane, "Value changed triggered an error in retrieving HLT trigger chain: ");
                }
            }

            //show all items for this algo
            if (theAlgo != null) {
                getItems(theAlgo);
            }
        }

    }

    //find all the algos of this supermaster table, and fill in the list table
    /**
     *
     */
    public void getAlgos() {

        allAlgos_v.clear();
        String query = "SELECT "
                + "HTE2CP_COMPONENT_ID, HCP_NAME, HCP_ALIAS "
                + "FROM "
                + "HLT_TE_TO_CP, HLT_COMPONENT, HLT_TS_TO_TE, HLT_TC_TO_TS, HLT_TM_TO_TC "
                + "WHERE "
                + "HTM2TC_TRIGGER_MENU_ID=? "
                + "AND "
                + "HTE2CP_COMPONENT_ID = HCP_ID "
                + "AND "
                + "HTM2TC_TRIGGER_CHAIN_ID=HTC2TS_TRIGGER_CHAIN_ID "
                + "AND "
                + "HTC2TS_TRIGGER_SIGNATURE_ID=HTS2TE_TRIGGER_SIGNATURE_ID "
                + "AND "
                + "HTS2TE_TRIGGER_ELEMENT_ID=HTE2CP_TRIGGER_ELEMENT_ID "
                + "ORDER BY HCP_NAME,  HCP_ALIAS";

        query = ConnectionManager.getInstance().fix_schema_name(query);

        //System.out.println("Algos query " + query);
        ArrayList<Integer> algo_ids = new ArrayList<>();
        try {
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ps.setInt(1, hlt_menu_id);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                //HLTComponent algo = new HLTComponent(rset.getInt("HTE2CP_COMPONENT_ID"));
                //add if unique - more than one way to include the algo! (many chains)
                //this doesn't work:
                //if(!StreamItems.contains(stream)) StreamItems.add(stream);
                int id = rset.getInt("HTE2CP_COMPONENT_ID");
                if (!algo_ids.contains(id)) {
                    algo_ids.add(id);
                    String descriptor = id + ": " + rset.getString("HCP_ALIAS") + " (" + rset.getString("HCP_NAME") + ")";
                    AlgoItems.addElement(descriptor);
                    allAlgos_v.add(descriptor);
                }
            }
            rset.close();
            ps.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Exception: {0}", ex);
        }
    }

    //find all the chains of the selected algos of this supermaster table, and fill in the list table
    /**
     *
     * @param selectedAlgo
     * @throws SQLException
     */
    public void getChains(HLTComponent selectedAlgo) throws SQLException {

        chains_v = new ArrayList<>();

        lowerchainnames = new String();
        lowerchainnames_hs = new HashMap<>();

        int prescale_id = Integer.parseInt(PSComboBox.getSelectedItem().toString().split(" ")[0]);
        hlt_psset = new HLTPrescaleSet(prescale_id);

        chains_v = selectedAlgo.getChains(hlt_menu_id);

        //draw table
        ChainsModel.setRowCount(0);

        //loop over the chains 
        for (HLTTriggerChain chain : chains_v) {

            ArrayList<Object> tabledata_v = new ArrayList<>();
            //chain name
            tabledata_v.add(chain.get_name());
            //chain counter
            tabledata_v.add(chain.get_chain_counter());

            //prescale
            double prescale = -1;
            double passthrough = -1;
            for (HLTPrescale p : hlt_psset.GetPrescalesForChain(chain.get_chain_counter())) {
                if (p.get_type() == HLTPrescaleType.Prescale) {
                    prescale = p.get_value();
                } else if (p.get_type() == HLTPrescaleType.Pass_Through) {
                    passthrough = p.get_value();
                }
            }

            tabledata_v.add(prescale);

            //PT
            tabledata_v.add(passthrough);

            ChainsModel.addRow(tabledata_v.toArray());

            lowerchainnames = chain.get_lower_chain_name();
            //System.out.println("Lower "+lowerchainnames);
            lowerchainnames_hs.put(chain.get_name(), lowerchainnames);
            //System.out.println(lowerchainnames_hs.size());
            //System.out.println("Chain " +lowerchainnames_hs.get(chains_v.get(i).get_name()));
        }
    }

    ///get all items for the algo
    /**
     *
     * @param selectedAlgo
     */
    public void getItems(HLTComponent selectedAlgo) {

        //at this point, we already have all the chains in chains_v
        //so need to get all L1 items, and see if LCN matches
        items_v = new ArrayList<>();

        String query = "SELECT "
                + "L1TI_ID, L1TM2TI_TRIGGER_ITEM_ID, L1TM2TI_TRIGGER_MENU_ID "
                + "FROM "
                + "L1_TRIGGER_ITEM, L1_TM_TO_TI "
                + "WHERE "
                + "L1TM2TI_TRIGGER_MENU_ID =? "
                + "AND "
                + "L1TM2TI_TRIGGER_ITEM_ID = L1TI_ID "
                + "ORDER BY L1TI_ID ASC";

        query = ConnectionManager.getInstance().fix_schema_name(query);

        //System.out.println("Items query " + query);
        try {
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ps.setInt(1, l1_menu_id);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                L1Item item = new L1Item(rset.getInt("L1TI_ID"));
                items_v.add(item);
            }
            rset.close();
            ps.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Exception: {0}", ex);
        }

        matcheditems_v = new ArrayList<>();
        ArrayList<Integer> alreadygotitems_v = new ArrayList<>();
        ////System.out.println("items  " + items_v.size());
        ////System.out.println("chains " + chains_v.size());

        //now have all items, but need to see if name matches LCN
        for (L1Item item : items_v) {
            for (HLTTriggerChain chain : chains_v) {
                ////System.out.println("looking at " + item.get_name() + "-" + chain.get_lower_chain_name());
                //For L2 lower chain name can be a comma separated list
                boolean matched = false;
                String lower_chains[] = chain.get_lower_chain_name().split(",");
                for (String lower_chain : lower_chains) {
                    if (lower_chain.trim().equals(item.get_name())) {
                        matched = true;
                    }
                }
                if (matched) {
                    boolean alreadyhave = false;
                    for (int alreadygotitem_id : alreadygotitems_v) {
                        if (item.get_id() == alreadygotitem_id) {
                            alreadyhave = true;
                            break;
                        }
                    }
                    if (!alreadyhave) {
                        alreadygotitems_v.add(item.get_id());
                        matcheditems_v.add(item);
                        break; //end the chain loop and go to next item
                    }
                }
            }
        }

        //draw table - using matched items
        ItemsModel.setRowCount(0);

        //loop over the matched items
        for (int i = 0; i < matcheditems_v.size(); i++) {

            ArrayList<Object> tabledata_v = new ArrayList<>();
            //item name only
            tabledata_v.add(matcheditems_v.get(i).get_name());

            ItemsModel.addRow(tabledata_v.toArray());
        }
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane6 = new javax.swing.JScrollPane();
        listTablesAlgos = new javax.swing.JList<>();
        jScrollPane3 = new javax.swing.JScrollPane();
        tableChains = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        tableItems = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        FilterText = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        PSComboBox = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1157, 468));

        listTablesAlgos.setModel(AlgoItems);
        jScrollPane6.setViewportView(listTablesAlgos);

        tableChains.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Chain Name", "Counter", "Prescale", "Pass-through"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableChains.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableChainsMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tableChains);

        tableItems.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Seed"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableItems.setColumnSelectionAllowed(true);
        tableItems.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tableItemsFocusGained(evt);
            }
        });
        jScrollPane4.setViewportView(tableItems);

        jButton1.setText("Done"); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        FilterText.setText("jTextField1"); // NOI18N
        FilterText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                FilterTextKeyReleased(evt);
            }
        });

        jLabel1.setText("Algorithm filter:"); // NOI18N

        PSComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PSComboBoxActionPerformed(evt);
            }
        });

        jLabel2.setText("HLT Prescale set:"); // NOI18N

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(layout.createSequentialGroup()
                        .add(0, 0, Short.MAX_VALUE)
                        .add(jButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 71, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(layout.createSequentialGroup()
                                .add(14, 14, 14)
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(jLabel2)
                                    .add(jLabel1))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(FilterText, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 412, Short.MAX_VALUE)
                                    .add(PSComboBox, 0, 412, Short.MAX_VALUE)))
                            .add(jScrollPane6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 531, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jScrollPane3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 438, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jScrollPane4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 150, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(12, 12, 12)
                        .add(jLabel1)
                        .add(7, 7, 7)
                        .add(jLabel2)
                        .add(0, 0, Short.MAX_VALUE))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 417, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jScrollPane3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(FilterText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(PSComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jScrollPane6)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 31, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(7, 7, 7))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void tableChainsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableChainsMouseClicked

    int row = tableChains.rowAtPoint(evt.getPoint());
    String what = (String) tableChains.getValueAt(row, 0);
    //System.out.println(what);
    //System.out.println(lowerchainnames_hs.containsKey(what));

    String lcn = lowerchainnames_hs.get(what);

    //System.out.println("LCN " +lcn);
    //System.out.println("Clicked the chains table row " + row + " for chain with lcn " + lcn);
    //For case of L2 chains, refine the items column so its just for the selected chain.
    //have list of matched items already, throw away those not for this
    ArrayList<String> supermatcheditems_v = new ArrayList<>();
    for (L1Item matcheditem : matcheditems_v) {
        if (matcheditem.get_name().equals(lcn)) {
            supermatcheditems_v.add(matcheditem.get_name());
        }
    }
    /*
    //For ef chains, find the L2 chain
    for (HLTTriggerChain chain : allchains_v) {
        //System.out.println("chain " + chain);
        if (chain.get_name().equals(lcn)) {
            supermatcheditems_v.add(chain.get_name());
        }
    }
     */
    //redraw the table
    ItemsModel.setRowCount(0);

    //loop over the matched items
    for (int i = 0; i < supermatcheditems_v.size(); i++) {
        ArrayList<Object> tabledata_v = new ArrayList<>();
        //item name only
        tabledata_v.add(supermatcheditems_v.get(i));
        ItemsModel.addRow(tabledata_v.toArray());
    }

}//GEN-LAST:event_tableChainsMouseClicked

private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
    dispose();
}//GEN-LAST:event_jButton1ActionPerformed

private void tableItemsFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tableItemsFocusGained

}//GEN-LAST:event_tableItemsFocusGained

private void FilterTextKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_FilterTextKeyReleased
    // filter algos in the list
    AlgoItems.clear();

    for (String algo : allAlgos_v) {
        String label = algo.split(":")[1];
        if (label.toLowerCase().contains(FilterText.getText().toLowerCase())) {
            AlgoItems.addElement(algo);
        }
    }
}//GEN-LAST:event_FilterTextKeyReleased

private void PSComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PSComboBoxActionPerformed
    //see what prescale set we selected and apply to the table
    //System.out.println("selected ps set " + PSComboBox.getSelectedItem().toString());
    //look through the chains and add the ps info
    if (theAlgo != null) {
        try {
            getChains(theAlgo);
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, rootPane, "An action related to the prescale combo box triggered an error in retrieving the HLT trigger chaind: ");
        }
    }
}//GEN-LAST:event_PSComboBoxActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField FilterText;
    private javax.swing.JComboBox<String> PSComboBox;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JList<String> listTablesAlgos;
    private javax.swing.JTable tableChains;
    private javax.swing.JTable tableItems;
    // End of variables declaration//GEN-END:variables

}
