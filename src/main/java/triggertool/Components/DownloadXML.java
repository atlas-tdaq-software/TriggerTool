/*
 * DownloadXML.java
 *
 * Created on March 30, 2008, 4:38 PM
 */
package triggertool.Components;

import triggerdb.Connections.ConnectionManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggertool.TTExceptionHandler;
import triggertool.XML.HLT_DBtoXML;
import triggertool.XML.L1_DBtoXML;
import triggertool.XML.Topo_DBtoXML;


///Dialog box to download an entire configuration from the database to XML.
/**
 * The dialog allows you to select the L1 and HLT Prescale sets and the path
 * to record the XML files to.
 * 
 * @author Simon Head
 */
public class DownloadXML extends JDialog {

    /** Message Log. */
    private static final Logger logger = Logger.getLogger("TriggerTool");
    /** Reference of the super master table, filled by the constructor. */
    private SuperMasterTable smt = null;

    private boolean topoExists = false;
    /** Mils to Sec constant. */
    /** Flag: <code>true</code> if we want to download only the L1 Menu. */
    private boolean l1Exists = false;
     /** Flag: <code>true</code> if we want to download only the L1 Menu. */
    private boolean hltExists = false;
     /** Flag: <code>true</code> if we want to download only the L1 Menu. */
   
    private static final double MIL2SEC = 1e3;

    /**
     * This creates and shows the dialog box.  We also populate the path with 
     * the current user's home directory.
     * 
     * @param parent  The parent dialog box, can be null.
     * @param smk The id of the super master table to write to xml.
     */
    public DownloadXML(JFrame parent, int smk) {
        super((JDialog)null);
        try {
            initComponents();
            this.setTitle("Write XML");
            this.setLocationRelativeTo(parent);
            this.setVisible(true);
            this.textDirectory.setText(InitInfo.getSystemHome());
            this.smt = new SuperMasterTable(smk);
            
            long t0 = System.currentTimeMillis();
            this.loadL1Prescales();
            
            // Check whether this STM if L1/Topo only.
            checkTopo();
            checkL1();
            checkHLT();

            StringBuilder msg = new StringBuilder(1000);
            msg.append("SMK ");
            msg.append(smk);
            msg.append("\n  has L1Topo : ");
            msg.append(this.topoExists ? "yes" : "no");
            msg.append("\n  has L1     : ");
            msg.append(this.l1Exists ? "yes" : "no");
            msg.append("\n  has HLT    : ");
            msg.append(this.hltExists ? "yes" : "no");
            logger.info(msg.toString());
            
            loadBunchGroupSets();
            
            // if not L1Onlu load HLT, else disable HLT options.
            if (hltExists) {
                this.loadHLTPrescales();
                comboHLTPrescales.setEnabled(true);
                textHLTMenu.setText("HLTMenu_smk_" + smk + ".xml");
                textHLTMenu.setEnabled(true);
                textHLTRelease.setText("HLTRelease_smk_" + smk + ".xml");
                textHLTRelease.setEnabled(true);
                textHLTSetup.setText("HLTSetup_smk_" + smk + ".xml");
                textHLTSetup.setEnabled(true);
            } else {
                comboHLTPrescales.removeAllItems();
                comboHLTPrescales.setEnabled(false);
                textHLTMenu.setText("");
                textHLTMenu.setEditable(false);
                textHLTMenu.setEnabled(false);
                labelHLTMenu.setEnabled(false);
                textHLTRelease.setText("");
                textHLTRelease.setEditable(false);
                textHLTRelease.setEnabled(false);
                labelHLTRelease.setEnabled(false);
                textHLTSetup.setText("");
                textHLTSetup.setEditable(false);
                textHLTSetup.setEnabled(false);
                labelHLTSetup.setEnabled(false);
                labelHLTPS.setEnabled(false);
            }
            //if we have no HLT and the Topo does not exist then we have L1 only
            //else we only have topo
            if(topoExists){
                textTopoMenu.setText("L1TopoMenu_smk_" + smk + ".xml");
                textTopoMenu.setEditable(true);
            } else {
                textTopoMenu.setText("");
                textTopoMenu.setEditable(false);
            }
            
            if (l1Exists) {
                loadL1Prescales();
                comboL1Prescales.setEnabled(true);
                textL1Menu.setText("L1Menu_smk_" + smk + ".xml");
                textL1Menu.setEditable(true);
            } else {
                textL1Menu.setText("");
                textL1Menu.setEditable(false);
                comboL1Prescales.removeAllItems();
                comboL1Prescales.setEnabled(false);
            }
            long t1 = System.currentTimeMillis();
            logger.log(Level.FINE, " Constructor t {0}", (t1 - t0) / MIL2SEC);
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, rootPane, "Error occurred while creating the DownloadXML panel.");
        }
    }

    /**
     * Check whether this SMT is a L1Only menu.     
     * @return <code>true</code> if this SMT is a L1 Only.
     */
    private void checkHLT() throws SQLException {

        String query = "select COUNT(*) from " +
                "SUPER_MASTER_TABLE SM, HLT_MASTER_TABLE HM, HLT_TM_TO_TC TM2TC " +
                "where SM.SMT_ID=? AND SM.SMT_HLT_MASTER_TABLE_ID=HM.HMT_ID " +
                "AND HM.HMT_TRIGGER_MENU_ID = TM2TC.HTM2TC_TRIGGER_MENU_ID";

        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);

        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, smt.get_id());
        ResultSet rset = ps.executeQuery();
        if(rset.next()) {
            int count = rset.getInt(1);
            this.hltExists = count>0;
        }
        rset.close();
        ps.close();
    }

    /**
     * Check whether this SMT is a L1Only menu.     
     * @return <code>true</code> if this SMT is a L1 Only.
     */
    private void checkTopo() throws SQLException {
        String query = "select COUNT(*) from "+
                "SUPER_MASTER_TABLE SM, TOPO_MASTER_TABLE TM, TTM_TO_TA TM2TA "+
                "where SM.SMT_ID=? AND SM.SMT_TOPO_MASTER_TABLE_ID = TM.TMT_ID "+
                "AND TM.TMT_TRIGGER_MENU_ID = TM2TA.TTM2TA_MENU_ID";

        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);

        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, smt.get_id());
        ResultSet rset = ps.executeQuery();
        if(rset.next()) {
            int count = rset.getInt(1);
            this.topoExists = count>0;
        }
        //this.topoExists = rset.next();
        rset.close();
        ps.close();
    }

    
    private void checkL1() throws SQLException {
        
        String query = "select COUNT(*) from " +
                    "SUPER_MASTER_TABLE SM, L1_MASTER_TABLE MA, L1_TRIGGER_MENU TM, L1_TM_TO_TI TM2TI " + 
                    "WHERE SM.SMT_ID=? " +
                    "AND SM.SMT_L1_MASTER_TABLE_ID = MA.L1MT_ID " +
                    "AND MA.L1MT_TRIGGER_MENU_ID=TM2TI.L1TM2TI_TRIGGER_MENU_ID";
        
        

        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);

        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, smt.get_id());
        ResultSet rset = ps.executeQuery();
        if(rset.next()) {
            int count = rset.getInt(1);
            this.l1Exists = count>0;
        }
        rset.close();
        ps.close();
    }

    /**
     * Perform a query on the database and fill the selection box with all the
     * L1 prescale sets that are valid for this super master table.
     * TODO this should go to L1Prescales as a static function.
     */
    private void loadL1Prescales() throws SQLException {
        comboL1Prescales.removeAllItems();

        HashMap<Integer, String> psMap = new HashMap<>();
        ArrayList<Integer> psIdList = new ArrayList<>();

        String query = "SELECT L1PS_ID, L1PS_NAME, L1PS_VERSION, "
                + "L1TM2PS_PRESCALE_SET_ID, L1TM2PS_TRIGGER_MENU_ID "
                + "FROM L1_PRESCALE_SET, L1_TM_TO_PS "
                + "WHERE L1TM2PS_TRIGGER_MENU_ID=? "
                + "AND L1TM2PS_PRESCALE_SET_ID=L1PS_ID "
                + "ORDER BY L1PS_ID DESC";

        query = ConnectionManager.getInstance().fix_schema_name(query);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, smt.get_l1_master_table().get_menu_id());
        ResultSet rset = ps.executeQuery();

        while (rset.next()) {
            String Info = ": " + rset.getString("L1PS_NAME") + " v" + rset.getString("L1PS_VERSION");
            psMap.put(rset.getInt("L1PS_ID"), Info);
            psIdList.add(rset.getInt("L1PS_ID"));
        }
        rset.close();
        ps.close();
        for (Integer id : psIdList) {
            comboL1Prescales.addItem(id + psMap.get(id));
        }
    }

      private void loadBunchGroupSets() throws SQLException {
        comboBunchGroupSet.removeAllItems();

        String query = "SELECT L1BGS_ID, L1BGS_NAME, L1BGS_VERSION "
                + "from L1_BUNCH_GROUP_SET "
                + "ORDER BY L1BGS_ID DESC";

        query = ConnectionManager.getInstance().fix_schema_name(query);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ResultSet rset = ps.executeQuery();

        while (rset.next()) {
            String Info = rset.getString("L1BGS_ID") + ": " + rset.getString("L1BGS_NAME") + " v" + rset.getString("L1BGS_VERSION");
          comboBunchGroupSet.addItem(Info);
        }
        rset.close();
        ps.close();
    }

    
    /**
     * Query the database and ask for all the HLT prescale sets that are valid
     * for this super master table.
     */
    private void loadHLTPrescales() throws SQLException {
        comboHLTPrescales.removeAllItems();

        HashMap<Integer, String> PS = new HashMap<>();
        List<Integer> ID = new ArrayList<>();

        String query = "SELECT HPS_ID, HPS_NAME, HPS_VERSION, HTM2PS_PRESCALE_SET_ID, HTM2PS_TRIGGER_MENU_ID "
                + "FROM "
                + "HLT_PRESCALE_SET, HLT_TM_TO_PS "
                + "WHERE "
                + "HTM2PS_TRIGGER_MENU_ID=? "
                + "AND "
                + "HTM2PS_PRESCALE_SET_ID=HPS_ID";
        query = ConnectionManager.getInstance().fix_schema_name(query);
        //System.out.println(query);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, smt.get_hlt_master_table().get_menu_id());
        ResultSet rset = ps.executeQuery();

        while (rset.next()) {
            String Info = ": " + rset.getString("HPS_NAME") + " v" + rset.getString("HPS_VERSION");
            PS.put(rset.getInt("HPS_ID"), Info);
            ID.add(rset.getInt("HPS_ID"));
        }

        rset.close();
        ps.close();

        int[] id = new int[ID.size()];
        for (int next = 0; next < ID.size(); next++) {
            id[next] = ID.get(next);
        }

        Arrays.sort(id);
        for (int next = ID.size() - 1; next >= 0; next--) {
            comboHLTPrescales.addItem(id[next] + PS.get(id[next]));
        }
    }

    /**
     * Write the XML files to disk.  First we do L1, and if the HLT Menu name
     * is not blank then we go on to write the HLT XML files too.
     */
    private void write() throws SQLException {

        if (!textDirectory.getText().endsWith("/")) {
            textDirectory.setText(textDirectory.getText() + "/");
        }
        long t0 = System.currentTimeMillis();
        //L1
        if (!textL1Menu.getText().isEmpty()) {
            L1_DBtoXML l1_writer = new L1_DBtoXML();
            String l1 = textDirectory.getText() + textL1Menu.getText();

            //int prescale_id = ((L1Prescale) comboL1Prescales.getSelectedItem()).get_id();
            int prescale_id = Integer.parseInt(comboL1Prescales.getSelectedItem().toString().split(":")[0]);
            int bunch_group_set_id;
            if(jCheckBoxBGSDefault.isSelected()){
                bunch_group_set_id = -1;
            }
            else{
                bunch_group_set_id = Integer.parseInt(comboBunchGroupSet.getSelectedItem().toString().split(":")[0]);
            }
            l1_writer.writeXML(smt.get_l1_master_table_id(), prescale_id, l1, bunch_group_set_id, false);
        }
        if (!textTopoMenu.getText().isEmpty()) {
            Topo_DBtoXML topo_writer = new Topo_DBtoXML();
            String topo = textDirectory.getText() + textTopoMenu.getText();
            topo_writer.writeXML(smt.get_topo_master_table_id(), topo, false);
        }
        long t1 = System.currentTimeMillis();
        logger.log(Level.FINE, " Time to write L1 Menu (smt {0}) {1}", new Object[]{smt.get_id(), (t1 - t0) / MIL2SEC});
        //HLT
        if (hltExists) {
            if (!textHLTMenu.getText().isEmpty()) {
                //int prescale_id = ((HLTPrescaleSet) comboHLTPrescales.getSelectedItem()).get_id();
                int prescale_id = Integer.parseInt(comboHLTPrescales.getSelectedItem().toString().split(":")[0]);
                HLT_DBtoXML hlt_writer = new HLT_DBtoXML(smt.get_hlt_master_table_id(), prescale_id);
                String menu = textDirectory.getText() + textHLTMenu.getText();
                String release = textDirectory.getText() + textHLTRelease.getText();
                String setup = textDirectory.getText() + textHLTSetup.getText();
                hlt_writer.direct_write(smt, menu, release, setup);
                //
                long t2 = System.currentTimeMillis();
                logger.log(Level.FINE, " Time to write MLT Menu (smt {0}) {1}", new Object[]{smt.get_id(), (t2 - t1) / MIL2SEC});
            }
        }
    }

    /** 
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonCancel = new javax.swing.JButton();
        buttonWrite = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        labelHLTMenu = new javax.swing.JLabel();
        labelHLTRelease = new javax.swing.JLabel();
        labelHLTSetup = new javax.swing.JLabel();
        textDirectory = new javax.swing.JTextField();
        textL1Menu = new javax.swing.JTextField();
        textHLTMenu = new javax.swing.JTextField();
        textHLTRelease = new javax.swing.JTextField();
        textHLTSetup = new javax.swing.JTextField();
        buttonDirectory = new javax.swing.JButton();
        comboL1Prescales = new javax.swing.JComboBox<>();
        comboHLTPrescales = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        labelHLTPS = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        textTopoMenu = new javax.swing.JTextField();
        comboBunchGroupSet = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        jCheckBoxBGSDefault = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        buttonCancel.setText("Cancel");
        buttonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCancelActionPerformed(evt);
            }
        });

        buttonWrite.setText("Write XML");
        buttonWrite.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonWriteActionPerformed(evt);
            }
        });

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel1.setText("Directory:");

        jLabel2.setText("L1 Menu:");

        labelHLTMenu.setText("HLT Menu:");

        labelHLTRelease.setText("Release:");

        labelHLTSetup.setText("HLT Setup:");

        textL1Menu.setText("L1Menu.xml");

        textHLTMenu.setText("HLTMenu.xml");

        textHLTRelease.setText("HLTRelease.xml");

        buttonDirectory.setText("Browse");
        buttonDirectory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDirectoryActionPerformed(evt);
            }
        });

        jLabel7.setText("L1 Prescale Set:");

        labelHLTPS.setText("HLT Prescale Set:");

        jLabel9.setText("Topo Menu:");

        textTopoMenu.setText("TopoMenu.xml");

        jLabel8.setText("Bunch Group Set:");

        jCheckBoxBGSDefault.setText("Use Default");
        jCheckBoxBGSDefault.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxBGSDefaultActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(0, 389, Short.MAX_VALUE)
                        .add(buttonWrite)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(buttonCancel))
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jLabel9)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jLabel7)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jLabel2)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, labelHLTPS, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, labelHLTMenu)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, labelHLTRelease)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, labelHLTSetup)
                            .add(jLabel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jLabel8))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createSequentialGroup()
                                .add(textDirectory, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 358, Short.MAX_VALUE)
                                .add(18, 18, 18)
                                .add(buttonDirectory))
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, textTopoMenu, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 439, Short.MAX_VALUE)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, comboL1Prescales, 0, 439, Short.MAX_VALUE)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, textL1Menu, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 439, Short.MAX_VALUE)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, comboHLTPrescales, 0, 439, Short.MAX_VALUE)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, textHLTMenu, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 439, Short.MAX_VALUE)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, textHLTRelease, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 439, Short.MAX_VALUE)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, textHLTSetup, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 439, Short.MAX_VALUE)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                                        .add(comboBunchGroupSet, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jCheckBoxBGSDefault)))
                                .add(12, 12, 12)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel1)
                    .add(buttonDirectory)
                    .add(textDirectory, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel9)
                    .add(textTopoMenu, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel7)
                    .add(comboL1Prescales, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(textL1Menu, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel2))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(comboBunchGroupSet, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel8)
                    .add(jCheckBoxBGSDefault))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(comboHLTPrescales, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(labelHLTPS))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(labelHLTMenu)
                    .add(textHLTMenu, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(labelHLTRelease)
                    .add(textHLTRelease, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(labelHLTSetup)
                    .add(textHLTSetup, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(buttonCancel)
                    .add(buttonWrite))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * If the user wants to write some XML files then close the window, use 
     * this. Called by the write XML button.
     * 
     * @param evt The event that fires this function
     */
    private void buttonWriteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonWriteActionPerformed
        try {
            write();
            dispose();
        } catch (SQLException ex) {
            String message = "Error occurred while trying to write xml file from the database.";
            logger.log(Level.SEVERE, message, ex);
            JOptionPane.showMessageDialog(this, "SQL Error" , message + 
            " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
        }
}//GEN-LAST:event_buttonWriteActionPerformed

    /**
     * Do nothing and just close the dialog box.
     * 
     * @param evt The event that fires this function.
     */
    private void buttonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCancelActionPerformed
        dispose();
    }//GEN-LAST:event_buttonCancelActionPerformed

    /**
     * The user wants to pick a directory to record the files to.  Pop open a
     * FileChooser dialog and allow them to select it.  Then write the result
     * to the directory text box.
     * 
     * @param evt The event that fires this function.
     */
    private void buttonDirectoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDirectoryActionPerformed

        final JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int returnVal = fc.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            String filename = fc.getSelectedFile().toString();
            //textDirectory.setText(filename + "/");
            textDirectory.setText(filename);
        }
}//GEN-LAST:event_buttonDirectoryActionPerformed

    private void jCheckBoxBGSDefaultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxBGSDefaultActionPerformed
           if(jCheckBoxBGSDefault.isSelected()){
               comboBunchGroupSet.setEnabled(false);
           }
           else{
               comboBunchGroupSet.setEnabled(true);        
        }
    }//GEN-LAST:event_jCheckBoxBGSDefaultActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonCancel;
    private javax.swing.JButton buttonDirectory;
    private javax.swing.JButton buttonWrite;
    private javax.swing.JComboBox<String> comboBunchGroupSet;
    private javax.swing.JComboBox<String> comboHLTPrescales;
    private javax.swing.JComboBox<String> comboL1Prescales;
    private javax.swing.JCheckBox jCheckBoxBGSDefault;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel labelHLTMenu;
    private javax.swing.JLabel labelHLTPS;
    private javax.swing.JLabel labelHLTRelease;
    private javax.swing.JLabel labelHLTSetup;
    private javax.swing.JTextField textDirectory;
    private javax.swing.JTextField textHLTMenu;
    private javax.swing.JTextField textHLTRelease;
    private javax.swing.JTextField textHLTSetup;
    private javax.swing.JTextField textL1Menu;
    private javax.swing.JTextField textTopoMenu;
    // End of variables declaration//GEN-END:variables
}
