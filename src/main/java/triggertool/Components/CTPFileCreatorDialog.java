package triggertool.Components;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import java.awt.Frame;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggertool.Panels.CommandConsole;

/**
 * Interface for CTP file decisions
 * 
 * @author Alex Martyniuk
 */
@SuppressWarnings("serial")
public final class CTPFileCreatorDialog extends JDialog {

    private static String currentWorkdir = new File(".").getAbsolutePath();

    private static final Logger logger = Logger.getLogger("TriggerTool");
    
    private SuperMasterTable smt;

    /**
     *
     */
    public enum Selection { 

        /**
         *
         */
        CANCEL, 

        /**
         *
         */
        TMC, 

        /**
         *
         */
        UPLOAD };

    private Selection selectedAction;

    static {
        if(currentWorkdir.endsWith("/.")) { currentWorkdir = currentWorkdir.substring(0,currentWorkdir.length()-2); }
    }
    
    /**
     *
     * @param parent
     * @param modal
     * @param smt
     */
    public CTPFileCreatorDialog(Frame parent, final SuperMasterTable smt) {
        super((JDialog)null);
        this.selectedAction = Selection.CANCEL;
        this.smt = smt;
        
        initComponents();
        this.setTitle("CTP File Creator");
        this.setLocationRelativeTo(parent);
        workdirTextField.setText(currentWorkdir);
        tmcLocationLabel.setText(findTMCLocation());
    }

    /**
     *
     * @return
     */
    public Selection getSelectedAction() {
        return selectedAction;
    }

    /**
     *
     * @return
     */
    public static String getCurrentWorkdir() {
        return currentWorkdir;
    }
    

    /**
     *
     * @return location of triggerMenuCompilerApp
     */
    public static String findTMCLocation() {
        Runtime rt = Runtime.getRuntime();
        
        String output = "";
        try {
            //todo fix null here and then also call this from ctpfilecreator
//            String tmcPathFromCmdline = System.getProperty("TMCPATH");
//            //works, but kind of useless, because for running the TMC, more libraries are needed
//            logger.log(Level.INFO, "Checking for triggerMenuCompilerApp\nin environment" + "PATH="+tmcPathFromCmdline+"/bin:"+System.getenv("PATH"));
//            Process wh = rt.exec("which triggerMenuCompilerApp",new String[]{"PATH="+tmcPathFromCmdline+"/bin:" + System.getenv("PATH")});
            Process wh = rt.exec("which triggerMenuCompilerApp");
            BufferedReader wh_buf_reader = new BufferedReader(new InputStreamReader(wh.getInputStream()));
            String wh_line;
            StringBuilder sb = new StringBuilder(1000);
            while ((wh_line = wh_buf_reader.readLine()) != null) {
                sb.append(wh_line);
            }
            output = sb.toString();
        } catch (IOException ex) {
            Logger.getLogger(CTPFileCreatorDialog.class.getName()).log(Level.SEVERE, null, ex);
        }


        if (output.contains("no triggerMenuCompilerApp") || output.length() == 0) {
            logger.log(Level.WARNING, "triggerMenuCompilerApp not found in path ''{0}''. Can''t continue!", output);
            return "Not found";
        }

        logger.info("triggerMenuCompilerApp found in path");
        logger.log(Level.INFO, "Path: {0}", output);

        return output;
    }

    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titleLabel = new javax.swing.JLabel();
        runTMCButton = new javax.swing.JButton();
        UploadAll = new javax.swing.JButton();
        workdirLabel = new javax.swing.JLabel();
        workdirTextField = new javax.swing.JTextField();
        browseButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        tmcLabel = new javax.swing.JLabel();
        tmcLocationLabel = new javax.swing.JLabel();
        remoteCompileButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Exact DB Copy");
        setMinimumSize(new java.awt.Dimension(568, 336));
        setResizable(false);

        titleLabel.setFont(new java.awt.Font("DejaVu Sans", 0, 18)); // NOI18N
        titleLabel.setForeground(new java.awt.Color(2, 93, 253));
        titleLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleLabel.setText("Creating the CTP Firmware");

        runTMCButton.setText("Run the Trigger Menu Compiler");
        runTMCButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runTMCButtonActionPerformed(evt);
            }
        });

        UploadAll.setText("Upload Firmware to DB");
        UploadAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UploadAllActionPerformed(evt);
            }
        });

        workdirLabel.setText("Workdir:");

        workdirTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                workdirTextFieldActionPerformed(evt);
            }
        });

        browseButton.setText("Browse");
        browseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.setToolTipText("Close without any action");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        tmcLabel.setText("TMC:");

        tmcLocationLabel.setFont(new java.awt.Font("DejaVu Sans", 2, 13)); // NOI18N
        tmcLocationLabel.setText("A very long text that describes the TMC location. It usually is a afs path that one has to setup");

        remoteCompileButton.setText("Run remote SVFX Compiler");
        remoteCompileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                remoteCompileButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(workdirLabel)
                    .add(tmcLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(layout.createSequentialGroup()
                        .add(workdirTextField)
                        .add(18, 18, 18)
                        .add(browseButton))
                    .add(cancelButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 87, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(30, 30, 30))
            .add(layout.createSequentialGroup()
                .add(140, 140, 140)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(UploadAll, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 269, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(remoteCompileButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 269, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(runTMCButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 268, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(titleLabel))
                .addContainerGap(159, Short.MAX_VALUE))
            .add(layout.createSequentialGroup()
                .add(82, 82, 82)
                .add(tmcLocationLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(new java.awt.Component[] {UploadAll, remoteCompileButton, runTMCButton}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(titleLabel)
                .add(18, 18, 18)
                .add(runTMCButton)
                .add(18, 18, 18)
                .add(remoteCompileButton)
                .add(18, 18, 18)
                .add(UploadAll)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 53, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(tmcLabel)
                    .add(tmcLocationLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(workdirLabel)
                    .add(workdirTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(browseButton))
                .add(18, 18, 18)
                .add(cancelButton)
                .add(27, 27, 27))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Option to run the SMK and create a new SMK from the resulting match
     * @param evt
     */
    private void runTMCButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runTMCButtonActionPerformed

        CtpFileCreator cfc;
        try {
            cfc = new CtpFileCreator(this, false, smt);
            cfc.setVisible(true);
            cfc.execute();
        } catch (SQLException ex) {
            Logger.getLogger("TriggerTool").log(Level.SEVERE, null, ex);
        }
        
//        selectedAction = Selection.TMC;
//         this.setVisible(false);
//         this.dispose();
    }//GEN-LAST:event_runTMCButtonActionPerformed
   
    private void UploadAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UploadAllActionPerformed

        SVFIUploadDialog SVFI = new SVFIUploadDialog(null, true);
        if( SVFI.isCanceled()) { 
            return;
        }

        SVFIUpload uploadOutput = new SVFIUpload(this, false, SVFI.VHDLSlot7, SVFI.VHDLSlot8, SVFI.VHDLSlot9, SVFI.SVFISlot7, SVFI.SVFISlot8, SVFI.SVFISlot9, SVFI.SMXO);
        uploadOutput.setVisible(true);
        uploadOutput.execute();
    }//GEN-LAST:event_UploadAllActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        selectedAction = Selection.CANCEL;
        dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void workdirTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_workdirTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_workdirTextFieldActionPerformed

    private void browseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_browseButtonActionPerformed
        JFileChooser fc = new JFileChooser(currentWorkdir);
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = fc.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = fc.getSelectedFile();
            currentWorkdir = f.toString();
            workdirTextField.setText(currentWorkdir);
            //checkFolder();
        }
    }//GEN-LAST:event_browseButtonActionPerformed


    private void createRemoteCompileScript(final String dir) {

        logger.log(Level.INFO, "Creating remote compile script in {0}", dir);

        try(BufferedWriter buffWriter = new BufferedWriter(new FileWriter(dir + "/remoteCompile.sh"))) {
            buffWriter.write("cd `dirname $0`\n");
            buffWriter.write("echo working in `pwd`\n\n");

            buffWriter.write("echo ... installing software remotely\n");
            buffWriter.write("ssh -Y pc-adt-01 'tar -xvf /afs/cern.ch/atlas/project/tdaq/level1/ctp/external/compileSVFI.tar'\n\n");

            buffWriter.write("echo ... copying smx_SLOT7.abl smx_SLOT8.abl smx_SLOT9.abl to pc-adt-01.cern.ch\n");
            buffWriter.write("scp smx_SLOT7.abl smx_SLOT8.abl smx_SLOT9.abl pc-adt-01:compileSVFI/lattice/\n\n");

            buffWriter.write("echo ... running remote compilation\n");
            buffWriter.write("ssh -Y pc-adt-01 'cd compileSVFI/lattice;./compile_smx.sh'\n\n");

            buffWriter.write("echo ... copying ctpin_smx_slot?.svf from pc-adt-01.cern.ch to `pwd`\n");
            buffWriter.write("scp -p 'pc-adt-01:compileSVFI/lattice/ctpin_smx_slot?.svf' .\n");

            buffWriter.write("echo ... cleaning up remotely\n");
            buffWriter.write("ssh -Y pc-adt-01 'rm -rf compileSVFI'\n");
            buffWriter.write("echo ... done\n\n");
            buffWriter.close();
        } 
        catch (IOException e) {
            logger.log(Level.WARNING, "{0}smxi.dat writing failed: {1}", new Object[]{dir, e});
        }
        new File(dir + "/remoteCompile.sh").setExecutable(true);

    }

    private void remoteCompileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_remoteCompileButtonActionPerformed

        createRemoteCompileScript(currentWorkdir);

        CommandConsole cc = new CommandConsole(this, "Remote Compilation of SVFI Files", currentWorkdir + "/remoteCompile.sh");
 
        cc.setVisible(true);

    }//GEN-LAST:event_remoteCompileButtonActionPerformed
 

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton UploadAll;
    private javax.swing.JButton browseButton;
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton remoteCompileButton;
    private javax.swing.JButton runTMCButton;
    private javax.swing.JLabel titleLabel;
    private javax.swing.JLabel tmcLabel;
    private javax.swing.JLabel tmcLocationLabel;
    private javax.swing.JLabel workdirLabel;
    private javax.swing.JTextField workdirTextField;
    // End of variables declaration//GEN-END:variables
}
