/*
 * SVFIUploadDialog.java
 *
 * Created on March 17, 2009, 3:05 PM
 */
package triggertool.Components;

import java.awt.Color;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author  Alex Martyniuk
 */
@SuppressWarnings("serial")
public class SVFIUploadDialog extends javax.swing.JDialog {

    /**
     *
     */
    public String SMXO = null;

    /**
     *
     */
    public String VHDLSlot7 = null;

    /**
     *
     */
    public String VHDLSlot8 = null;

    /**
     *
     */
    public String VHDLSlot9 = null;

    /**
     *
     */
    public String SVFISlot7 = null;

    /**
     *
     */
    public String SVFISlot8 = null;

    /**
     *
     */
    public String SVFISlot9 = null;

    /**
     *
     */
    public String userCodeVHDLSlot7 = null;

    /**
     *
     */
    public String userCodeVHDLSlot8 = null;

    /**
     *
     */
    public String userCodeVHDLSlot9 = null;

    /**
     *
     */
    public String userCodeSVFISlot7 = null;

    /**
     *
     */
    public String userCodeSVFISlot8 = null;

    /**
     *
     */
    public String userCodeSVFISlot9 = null;

    private boolean fileExistsSMXO;
    private boolean fileExistsVHDLSlot7;
    private boolean fileExistsVHDLSlot8;
    private boolean fileExistsVHDLSlot9;
    private boolean fileExistsSVFISlot7;
    private boolean fileExistsSVFISlot8;
    private boolean fileExistsSVFISlot9;

    /** File chooser */
    private final JFileChooser fc = new JFileChooser();

    private static String workdir;
    
    private boolean canceled;
    
    /** Creates new form SVFIUploadDialog
     * @param parent
     * @param modal */
    public SVFIUploadDialog(java.awt.Frame parent, boolean modal) {
        super(parent);
        this.canceled = true;
        setModal(modal);
        initComponents();
        setTitle("SMX, VHDL and SVFI Upload");

        workdir = CTPFileCreatorDialog.getCurrentWorkdir();
        SMXOTextField.setText(workdir+"/smxo.dat");
        VHDLSlot7TextField.setText(workdir+"/smx_SLOT7.vhd");
        VHDLSlot8TextField.setText(workdir+"/smx_SLOT8.vhd");
        VHDLSlot9TextField.setText(workdir+"/smx_SLOT9.vhd");
        SVFISlot7TextField.setText(workdir+"/ctpin_smx_slot7.svf");
        SVFISlot8TextField.setText(workdir+"/ctpin_smx_slot8.svf");
        SVFISlot9TextField.setText(workdir+"/ctpin_smx_slot9.svf");

        fileExistsSMXO      = checkFileExistence(SMXOTextField, null);
        fileExistsVHDLSlot7 = checkFileExistence(VHDLSlot7TextField, ucVHDLSlot7TextField);
        fileExistsVHDLSlot8 = checkFileExistence(VHDLSlot8TextField, ucVHDLSlot8TextField);
        fileExistsVHDLSlot9 = checkFileExistence(VHDLSlot9TextField, ucVHDLSlot9TextField);
        fileExistsSVFISlot7 = checkFileExistence(SVFISlot7TextField, ucSVFISlot7TextField);
        fileExistsSVFISlot8 = checkFileExistence(SVFISlot8TextField, ucSVFISlot8TextField);
        fileExistsSVFISlot9 = checkFileExistence(SVFISlot9TextField, ucSVFISlot9TextField);

        setUploadButtonState();

        fc.setCurrentDirectory(new File(workdir));
      
        setVisible(true);
    }

    private boolean checkFileExistence(final javax.swing.JTextField textfield, javax.swing.JTextField userCodeTextField) {
        boolean readable = true;
        String filename = textfield.getText();
        File f = new File(filename);
        if(f.exists() && !f.isDirectory() && f.canRead()) {
            textfield.setForeground(Color.BLACK);
            textfield.setToolTipText(null);
        } else {
            readable = false;
            textfield.setForeground(Color.LIGHT_GRAY);
            if(!f.exists()) {
                textfield.setToolTipText("File does not exist");
            } else if(f.isDirectory()) {
                textfield.setToolTipText("File is a directory");
            } else if(!f.canRead()) {
                textfield.setToolTipText("File is not readable");            
            }
        }

        if(userCodeTextField!=null) {
            if(readable) {
                String userCode = getUserCode(f);
                userCodeTextField.setText(userCode);
            } else {
                userCodeTextField.setText("");
            }
        }
        return readable;
    }

    private boolean checkUserCodeConsistency() {
        boolean allConsistent = true;
        if(!ucVHDLSlot7TextField.getText().isEmpty() && !ucSVFISlot7TextField.getText().isEmpty()) {
            if(ucVHDLSlot7TextField.getText().equals(ucSVFISlot7TextField.getText())) {
                ucVHDLSlot7TextField.setBorder(null);
                ucSVFISlot7TextField.setBorder(null);
                ucVHDLSlot7TextField.setToolTipText(null);
                ucSVFISlot7TextField.setToolTipText(null);
            } else  {
                ucVHDLSlot7TextField.setBorder(BorderFactory.createLineBorder(Color.red));
                ucSVFISlot7TextField.setBorder(BorderFactory.createLineBorder(Color.red));
                ucVHDLSlot7TextField.setToolTipText("User codes don't match");
                ucSVFISlot7TextField.setToolTipText("User codes don't match");
                allConsistent = false;
            }
        }
        if(!ucVHDLSlot8TextField.getText().isEmpty() && !ucSVFISlot8TextField.getText().isEmpty()) {
            if(ucVHDLSlot8TextField.getText().equals(ucSVFISlot8TextField.getText())) {
                ucVHDLSlot8TextField.setBorder(null);
                ucSVFISlot8TextField.setBorder(null);
                ucVHDLSlot8TextField.setToolTipText(null);
                ucSVFISlot8TextField.setToolTipText(null);
            } else  {
                ucVHDLSlot8TextField.setBorder(BorderFactory.createLineBorder(Color.red));
                ucSVFISlot8TextField.setBorder(BorderFactory.createLineBorder(Color.red));
                ucVHDLSlot8TextField.setToolTipText("User codes don't match");
                ucSVFISlot8TextField.setToolTipText("User codes don't match");
                allConsistent = false;
            }
        }
        if(!ucVHDLSlot9TextField.getText().isEmpty() && !ucSVFISlot9TextField.getText().isEmpty()) {
            if(ucVHDLSlot9TextField.getText().equals(ucSVFISlot9TextField.getText())) {
                ucVHDLSlot9TextField.setBorder(null);
                ucSVFISlot9TextField.setBorder(null);
                ucVHDLSlot9TextField.setToolTipText(null);
                ucSVFISlot9TextField.setToolTipText(null);
            } else  {
                ucVHDLSlot9TextField.setBorder(BorderFactory.createLineBorder(Color.red));
                ucSVFISlot9TextField.setBorder(BorderFactory.createLineBorder(Color.red));
                ucVHDLSlot9TextField.setToolTipText("User codes don't match");
                ucSVFISlot9TextField.setToolTipText("User codes don't match");
                allConsistent = false;
            }
        }
        return allConsistent;
    }

    private String getUserCode(final File f) {
        String fileContent = SVFIUpload.getContents(f);
        if(f.getName().endsWith(".vhd")) {
            return SVFIUpload.getUserCodeVHDL(fileContent);
        }
        if(f.getName().endsWith(".svf")) {
            return SVFIUpload.getUserCodeSVFI(fileContent);
        }
        return "No Code";
    }


    private void setUploadButtonState() {
        boolean allConsistent = checkUserCodeConsistency();
        boolean allExist = fileExistsSMXO
            && fileExistsVHDLSlot7
            && fileExistsVHDLSlot8
            && fileExistsVHDLSlot9
            && fileExistsSVFISlot7
            && fileExistsSVFISlot8
            && fileExistsSVFISlot9;
        uploadButton.setEnabled(allExist && allConsistent);
    }



    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        SVFIPanel = new javax.swing.JPanel();
        Slot7Label = new javax.swing.JLabel();
        Slot8Label = new javax.swing.JLabel();
        Slot9Label = new javax.swing.JLabel();
        SVFISlot7TextField = new javax.swing.JTextField();
        SVFISlot8TextField = new javax.swing.JTextField();
        SVFISlot9TextField = new javax.swing.JTextField();
        SVFISlot7BrowseButton = new javax.swing.JButton();
        SVFISlot8BrowseButton = new javax.swing.JButton();
        SVFISlot9BrowseButton = new javax.swing.JButton();
        ucSVFISlot7TextField = new javax.swing.JTextField();
        ucSVFISlot8TextField = new javax.swing.JTextField();
        ucSVFISlot9TextField = new javax.swing.JTextField();
        VHDLPanel = new javax.swing.JPanel();
        Slot7Label2 = new javax.swing.JLabel();
        Slot8Label2 = new javax.swing.JLabel();
        Slot9Label2 = new javax.swing.JLabel();
        VHDLSlot7TextField = new javax.swing.JTextField();
        VHDLSlot8TextField = new javax.swing.JTextField();
        VHDLSlot9TextField = new javax.swing.JTextField();
        VHDLSlot7BrowseButton = new javax.swing.JButton();
        VHDLSlot8BrowseButton = new javax.swing.JButton();
        VHDLSlot9BrowseButton = new javax.swing.JButton();
        ucVHDLSlot7TextField = new javax.swing.JTextField();
        ucVHDLSlot8TextField = new javax.swing.JTextField();
        ucVHDLSlot9TextField = new javax.swing.JTextField();
        SMXOPanel = new javax.swing.JPanel();
        Slot7Label1 = new javax.swing.JLabel();
        SMXOTextField = new javax.swing.JTextField();
        SMXOBrowseButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        uploadButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SVFI Upload");

        SVFIPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("SVFI Files"));

        Slot7Label.setText("Slot7");

        Slot8Label.setText("Slot8");

        Slot9Label.setText("Slot9");

        SVFISlot7BrowseButton.setText("Browse");
        SVFISlot7BrowseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SVFISlot7BrowseButtonActionPerformed(evt);
            }
        });

        SVFISlot8BrowseButton.setText("Browse");
        SVFISlot8BrowseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SVFISlot8BrowseButtonActionPerformed(evt);
            }
        });

        SVFISlot9BrowseButton.setText("Browse");
        SVFISlot9BrowseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SVFISlot9BrowseButtonActionPerformed(evt);
            }
        });

        ucSVFISlot7TextField.setEditable(false);

        ucSVFISlot8TextField.setEditable(false);

        ucSVFISlot9TextField.setEditable(false);

        org.jdesktop.layout.GroupLayout SVFIPanelLayout = new org.jdesktop.layout.GroupLayout(SVFIPanel);
        SVFIPanel.setLayout(SVFIPanelLayout);
        SVFIPanelLayout.setHorizontalGroup(
            SVFIPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(SVFIPanelLayout.createSequentialGroup()
                .add(SVFIPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(SVFIPanelLayout.createSequentialGroup()
                        .add(SVFIPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(SVFIPanelLayout.createSequentialGroup()
                                .add(Slot9Label)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(SVFISlot9TextField)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(ucSVFISlot9TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 82, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(SVFIPanelLayout.createSequentialGroup()
                                .add(Slot8Label)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(SVFISlot8TextField)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(ucSVFISlot8TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 82, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(18, 18, 18)
                        .add(SVFIPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, SVFISlot8BrowseButton)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, SVFISlot9BrowseButton)))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, SVFIPanelLayout.createSequentialGroup()
                        .add(Slot7Label)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(SVFISlot7TextField)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(ucSVFISlot7TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 82, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(SVFISlot7BrowseButton)))
                .addContainerGap())
        );

        SVFIPanelLayout.linkSize(new java.awt.Component[] {SVFISlot7BrowseButton, SVFISlot8BrowseButton, SVFISlot9BrowseButton}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        SVFIPanelLayout.setVerticalGroup(
            SVFIPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(SVFIPanelLayout.createSequentialGroup()
                .add(SVFIPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(Slot7Label)
                    .add(SVFISlot7BrowseButton)
                    .add(SVFISlot7TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(ucSVFISlot7TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(SVFIPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(SVFISlot8BrowseButton)
                    .add(Slot8Label)
                    .add(SVFISlot8TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(ucSVFISlot8TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(SVFIPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(SVFISlot9BrowseButton)
                    .add(Slot9Label)
                    .add(SVFISlot9TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(ucSVFISlot9TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
        );

        SVFIPanelLayout.linkSize(new java.awt.Component[] {SVFISlot7BrowseButton, SVFISlot8BrowseButton, SVFISlot9BrowseButton}, org.jdesktop.layout.GroupLayout.VERTICAL);

        VHDLPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("VHDL Files"));
        VHDLPanel.setName("VHDL Files"); // NOI18N

        Slot7Label2.setText("Slot7");

        Slot8Label2.setText("Slot8");

        Slot9Label2.setText("Slot9");

        VHDLSlot7BrowseButton.setText("Browse");
        VHDLSlot7BrowseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VHDLSlot7BrowseButtonActionPerformed(evt);
            }
        });

        VHDLSlot8BrowseButton.setText("Browse");
        VHDLSlot8BrowseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VHDLSlot8BrowseButtonActionPerformed(evt);
            }
        });

        VHDLSlot9BrowseButton.setText("Browse");
        VHDLSlot9BrowseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VHDLSlot9BrowseButtonActionPerformed(evt);
            }
        });

        ucVHDLSlot7TextField.setEditable(false);
        ucVHDLSlot7TextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ucVHDLSlot7TextFieldActionPerformed(evt);
            }
        });

        ucVHDLSlot8TextField.setEditable(false);

        ucVHDLSlot9TextField.setEditable(false);

        org.jdesktop.layout.GroupLayout VHDLPanelLayout = new org.jdesktop.layout.GroupLayout(VHDLPanel);
        VHDLPanel.setLayout(VHDLPanelLayout);
        VHDLPanelLayout.setHorizontalGroup(
            VHDLPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(VHDLPanelLayout.createSequentialGroup()
                .add(VHDLPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(VHDLPanelLayout.createSequentialGroup()
                        .add(VHDLPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(VHDLPanelLayout.createSequentialGroup()
                                .add(Slot9Label2)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(VHDLSlot9TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE))
                            .add(VHDLPanelLayout.createSequentialGroup()
                                .add(Slot8Label2)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(VHDLSlot8TextField)))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(VHDLPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(ucVHDLSlot8TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 80, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(ucVHDLSlot9TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 80, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, VHDLPanelLayout.createSequentialGroup()
                        .add(Slot7Label2)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(VHDLSlot7TextField)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(ucVHDLSlot7TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 80, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(18, 18, 18)
                .add(VHDLPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, VHDLSlot7BrowseButton)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, VHDLSlot8BrowseButton)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, VHDLSlot9BrowseButton))
                .add(14, 14, 14))
        );
        VHDLPanelLayout.setVerticalGroup(
            VHDLPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(VHDLPanelLayout.createSequentialGroup()
                .add(15, 15, 15)
                .add(VHDLPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(VHDLPanelLayout.createSequentialGroup()
                        .add(4, 4, 4)
                        .add(Slot7Label2))
                    .add(VHDLPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(ucVHDLSlot7TextField)
                        .add(VHDLSlot7BrowseButton)
                        .add(VHDLSlot7TextField)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(VHDLPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(VHDLSlot8BrowseButton)
                    .add(Slot8Label2)
                    .add(VHDLSlot8TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(ucVHDLSlot8TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(VHDLPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(VHDLSlot9BrowseButton)
                    .add(Slot9Label2)
                    .add(VHDLSlot9TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(ucVHDLSlot9TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
        );

        VHDLPanelLayout.linkSize(new java.awt.Component[] {VHDLSlot7TextField, ucVHDLSlot7TextField}, org.jdesktop.layout.GroupLayout.VERTICAL);

        SMXOPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("SMXO File"));

        Slot7Label1.setText("smxo");

        SMXOBrowseButton.setText("Browse");
        SMXOBrowseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SMXOBrowseButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout SMXOPanelLayout = new org.jdesktop.layout.GroupLayout(SMXOPanel);
        SMXOPanel.setLayout(SMXOPanelLayout);
        SMXOPanelLayout.setHorizontalGroup(
            SMXOPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(SMXOPanelLayout.createSequentialGroup()
                .add(Slot7Label1)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(SMXOTextField)
                .add(18, 18, 18)
                .add(SMXOBrowseButton)
                .addContainerGap())
        );
        SMXOPanelLayout.setVerticalGroup(
            SMXOPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(SMXOPanelLayout.createSequentialGroup()
                .add(SMXOPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(Slot7Label1)
                    .add(SMXOBrowseButton)
                    .add(SMXOTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(76, 76, 76))
        );

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        uploadButton.setText("Upload");
        uploadButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uploadButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, VHDLPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, SVFIPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(SMXOPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(0, 0, Short.MAX_VALUE)
                        .add(cancelButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 92, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(uploadButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 106, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(VHDLPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(SVFIPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(SMXOPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 59, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(uploadButton)
                    .add(cancelButton))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void SVFISlot7BrowseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SVFISlot7BrowseButtonActionPerformed
    fc.setFileFilter(new FileNameExtensionFilter("SVFI files", "svf"));
    int returnVal = fc.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
        SVFISlot7TextField.setText(fc.getSelectedFile().toString());
        fileExistsSVFISlot7 = checkFileExistence(SVFISlot7TextField, ucSVFISlot7TextField);
        setUploadButtonState();
    }
}//GEN-LAST:event_SVFISlot7BrowseButtonActionPerformed

private void SVFISlot8BrowseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SVFISlot8BrowseButtonActionPerformed
    fc.setFileFilter(new FileNameExtensionFilter("SVFI files", "svf"));
    int returnVal = fc.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
        SVFISlot8TextField.setText(fc.getSelectedFile().toString());
        fileExistsSVFISlot8 = checkFileExistence(SVFISlot8TextField, ucSVFISlot8TextField);
        setUploadButtonState();
    }
}//GEN-LAST:event_SVFISlot8BrowseButtonActionPerformed

private void SVFISlot9BrowseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SVFISlot9BrowseButtonActionPerformed
    fc.setFileFilter(new FileNameExtensionFilter("SVFI files", "svf"));
    int returnVal = fc.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
        SVFISlot9TextField.setText(fc.getSelectedFile().toString());
        fileExistsSVFISlot9 = checkFileExistence(SVFISlot9TextField, ucSVFISlot9TextField);
        setUploadButtonState();
    }
}//GEN-LAST:event_SVFISlot9BrowseButtonActionPerformed

private void uploadButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uploadButtonActionPerformed
    VHDLSlot7 = VHDLSlot7TextField.getText();
    VHDLSlot8 = VHDLSlot8TextField.getText();
    VHDLSlot9 = VHDLSlot9TextField.getText();
    SVFISlot7 = SVFISlot7TextField.getText();
    SVFISlot8 = SVFISlot8TextField.getText();
    SVFISlot9 = SVFISlot9TextField.getText();
    SMXO = SMXOTextField.getText();
    canceled = false;
    this.setVisible(false);
    this.dispose();
}//GEN-LAST:event_uploadButtonActionPerformed

private void VHDLSlot7BrowseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VHDLSlot7BrowseButtonActionPerformed
    fc.setFileFilter(new FileNameExtensionFilter("VHDL files", "vhd"));
    int returnVal = fc.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
        VHDLSlot7TextField.setText(fc.getSelectedFile().toString());
        fileExistsVHDLSlot7 = checkFileExistence(VHDLSlot7TextField, ucVHDLSlot7TextField);
        setUploadButtonState();
    }
}//GEN-LAST:event_VHDLSlot7BrowseButtonActionPerformed

private void VHDLSlot8BrowseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VHDLSlot8BrowseButtonActionPerformed
    fc.setFileFilter(new FileNameExtensionFilter("VHDL files", "vhd"));
    int returnVal = fc.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
        VHDLSlot8TextField.setText(fc.getSelectedFile().toString());
        fileExistsVHDLSlot8 = checkFileExistence(VHDLSlot8TextField, ucVHDLSlot8TextField);
        setUploadButtonState();
    }
}//GEN-LAST:event_VHDLSlot8BrowseButtonActionPerformed

private void VHDLSlot9BrowseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VHDLSlot9BrowseButtonActionPerformed
    fc.setFileFilter(new FileNameExtensionFilter("VHDL files", "vhd"));
    int returnVal = fc.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
        VHDLSlot9TextField.setText(fc.getSelectedFile().toString());
        fileExistsVHDLSlot9 = checkFileExistence(VHDLSlot9TextField, ucVHDLSlot9TextField);
        setUploadButtonState();
    }
}//GEN-LAST:event_VHDLSlot9BrowseButtonActionPerformed

private void SMXOBrowseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SMXOBrowseButtonActionPerformed
    fc.setFileFilter(new FileNameExtensionFilter("SMX output files", "dat"));
    int returnVal = fc.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
        SMXOTextField.setText(fc.getSelectedFile().toString());
        fileExistsSMXO = checkFileExistence(SMXOTextField, null);
        setUploadButtonState();
    }
}//GEN-LAST:event_SMXOBrowseButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        canceled = true;
        dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void ucVHDLSlot7TextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ucVHDLSlot7TextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ucVHDLSlot7TextFieldActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton SMXOBrowseButton;
    private javax.swing.JPanel SMXOPanel;
    private javax.swing.JTextField SMXOTextField;
    private javax.swing.JPanel SVFIPanel;
    private javax.swing.JButton SVFISlot7BrowseButton;
    private javax.swing.JTextField SVFISlot7TextField;
    private javax.swing.JButton SVFISlot8BrowseButton;
    private javax.swing.JTextField SVFISlot8TextField;
    private javax.swing.JButton SVFISlot9BrowseButton;
    private javax.swing.JTextField SVFISlot9TextField;
    private javax.swing.JLabel Slot7Label;
    private javax.swing.JLabel Slot7Label1;
    private javax.swing.JLabel Slot7Label2;
    private javax.swing.JLabel Slot8Label;
    private javax.swing.JLabel Slot8Label2;
    private javax.swing.JLabel Slot9Label;
    private javax.swing.JLabel Slot9Label2;
    private javax.swing.JPanel VHDLPanel;
    private javax.swing.JButton VHDLSlot7BrowseButton;
    private javax.swing.JTextField VHDLSlot7TextField;
    private javax.swing.JButton VHDLSlot8BrowseButton;
    private javax.swing.JTextField VHDLSlot8TextField;
    private javax.swing.JButton VHDLSlot9BrowseButton;
    private javax.swing.JTextField VHDLSlot9TextField;
    private javax.swing.JButton cancelButton;
    private javax.swing.JTextField ucSVFISlot7TextField;
    private javax.swing.JTextField ucSVFISlot8TextField;
    private javax.swing.JTextField ucSVFISlot9TextField;
    private javax.swing.JTextField ucVHDLSlot7TextField;
    private javax.swing.JTextField ucVHDLSlot8TextField;
    private javax.swing.JTextField ucVHDLSlot9TextField;
    private javax.swing.JButton uploadButton;
    // End of variables declaration//GEN-END:variables

    /**
     * @return the canceled
     */
    public boolean isCanceled() {
        return canceled;
    }
}
