/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.Components;

/**
 *
 * @author stelzer
 */
class FileConsistencyException extends Exception {

    FileConsistencyException(String what) {
        super(what);
    }
    
}
