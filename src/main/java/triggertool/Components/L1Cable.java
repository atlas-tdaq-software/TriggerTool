/*
 * L1Cable.java
 *
 * Created on July 18, 2008, 3:25 PM
 */
package triggertool.Components;

import java.awt.Color;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.JDialog;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.CompoundHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.decorator.SearchPredicate;
import triggerdb.Entities.L1Links.L1TM_TT;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.Entities.L1.L1Menu;
import triggerdb.Entities.L1.L1Threshold;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.UserMode;
import triggerdb.Entities.L1Links.L1Pits;

/**
 * Initially grabs the TM_TT link tables in the database and displays them to screen.
 * If they arte emtpy or wrong offers a method to assign the mapping again from the 
 * mapping field and bitnum of each threshold.
 * Currently designed to work with the startup menu layout (ie MBTS etc)
 * Bits updated on 2011 Jan 18: SLOT 8 CON 0 - JEP3
 * 
 * @author  Alex Martyniuk
 */
public class L1Cable extends javax.swing.JDialog {

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    private L1Menu l1menu = null;
    private final HashSet<L1Threshold> l1thresh = new HashSet<>();
    private int l1_menu_id = -1;
    private SuperMasterTable super_master = null;
    private DefaultTableModel tabModel = null;
    
    private final L1CableTableModel CableMainModel;
    private final L1CTPCoreTableModel CTPCoreMainModel;
    private final CompoundHighlighter tableHighlightersIn = new CompoundHighlighter();
    private final CompoundHighlighter tableHighlightersCore = new CompoundHighlighter();
    private Integer[][] highlightPositionsIn;
    private Integer[][] highlightPositionsCore;

    ///Main constructor

    /**
     *
     * @param parent
     * @param modal
     * @param smt
     * @throws SQLException
     * @throws BadLocationException
     */

    public L1Cable(java.awt.Frame parent, SuperMasterTable smt)
            throws SQLException, BadLocationException {
        super((JDialog)null);
        initComponents();
        setTitle("L1 Cables: SMT " + smt);

        UserMode userMode = ConnectionManager.getInstance().getInitInfo().getUserMode();
        super_master = smt;
        l1_menu_id = smt.get_l1_master_table().get_menu_id();
        l1menu = smt.get_l1_master_table().get_menu();

        CableMainModel = (L1CableTableModel) tableCables.getModel();
        CTPCoreMainModel = (L1CTPCoreTableModel) tableCTPCore.getModel();
        
        highlightPositionsIn = new Integer[31][14];
        highlightPositionsCore = new Integer[32][8];
        populate();
        tableCables.packAll();
        tableCTPCore.packAll();
        //Write description
        Description.setText("Logic: Name of bit (Bit number in current threshold) TIP position. NB: TIP positions only exists if the TriggerMenuCompiler has been run.\n"
                          + "Colour Scheme for table:\n");
        SimpleAttributeSet set = new SimpleAttributeSet();
        StyleConstants.setForeground(set, Color.RED);
        Document doc = Description.getStyledDocument();
        doc.insertString(doc.getLength(), "Red text", set);
        set = new SimpleAttributeSet();
        doc.insertString(doc.getLength(), ": Cable bit exists, but threshold not defined in menu. (Not a problem unless you expected it)\n", set);
        set = new SimpleAttributeSet();
        StyleConstants.setForeground(set, Color.GREEN.darker());
        doc.insertString(doc.getLength(), "Green Text", set);
        set = new SimpleAttributeSet();
        doc.insertString(doc.getLength(), ": Cable bit exists, threshold defined in menu.\n", set);
        set = new SimpleAttributeSet();
        StyleConstants.setBackground(set, Color.YELLOW);
        doc.insertString(doc.getLength(), "Yellow Background", set);
        set = new SimpleAttributeSet();
        doc.insertString(doc.getLength(), ": Threshold && bit exist, but not used in an item (i.e. no TIP). (Only a problem if you thought it should be used)\n", set);
        set = new SimpleAttributeSet();
        StyleConstants.setBackground(set, Color.RED.brighter());
        doc.insertString(doc.getLength(), "Red Background", set);
        set = new SimpleAttributeSet();
        doc.insertString(doc.getLength(), ": Threshold placed on unknown cable bit. Text tells you which threshold. (Maybe this map is wrong? Check!)", set);

        setLocationRelativeTo(parent);
        setVisible(true);
    }

    /**
     *  Populates the table with the L1_TM_TTs from the l1menu.
     *  When L1Cables is first called it gets any L1_TMTTs in the SMT called.
     *  If called at the end of Assign map then it fills with the ones from memory linked to l1menu.
     */
    private void populate() throws SQLException {
        //Fill with some null values
        for(Integer i = 0; i<31; i++){
            ArrayList<Object> row = new ArrayList<>();
            row.add(0, i.toString());
            for(Integer j = 1; j <= CableMainModel.getColumnCount(); j++){
                row.add(j, "");
                highlightPositionsIn[i][j] = -1;
            }
            CableMainModel.add(row);
        }
        for(Integer i = 0; i<32; i++){
            ArrayList<Object> row = new ArrayList<>();
            row.add(0, i.toString());
            for(Integer j = 1; j <= CTPCoreMainModel.getColumnCount(); j++){
                row.add(j, "");
                highlightPositionsCore[i][j] = -1;
            }
            CTPCoreMainModel.add(row);
        }
        
        //Fill cables with defaults
        for(int i = 1; i < CableMainModel.getColumnCount(); i++){
            if(CableMainModel.getColumnName(i).equals(this.EM1_CONN)){
                for(int j = 0;j<8;j++){
                    CableMainModel.setValueAt("EM"+j+"(0)", (j*3), i);
                    CableMainModel.setValueAt("EM"+j+"(1)", 1+(j*3), i);
                    CableMainModel.setValueAt("EM"+j+"(2)", 2+(j*3), i);
                }
                CableMainModel.setValueAt("ZB_EM", 30, i);
            } else 
            if(CableMainModel.getColumnName(i).equals(this.EM2_CONN)){
                for(int j = 0;j<8;j++){
                    CableMainModel.setValueAt("EM"+(j+8)+"(0)", (j*3), i);
                    CableMainModel.setValueAt("EM"+(j+8)+"(1)", 1+(j*3), i);
                    CableMainModel.setValueAt("EM"+(j+8)+"(2)", 2+(j*3), i);
                }
                CableMainModel.setValueAt("ZB_EM", 30, i);
            } else
            if(CableMainModel.getColumnName(i).equals(this.TAU1_CONN)){
                for(int j = 0;j<8;j++){
                    CableMainModel.setValueAt("TAU"+j+"(0)", (j*3), i);
                    CableMainModel.setValueAt("TAU"+j+"(1)", 1+(j*3), i);
                    CableMainModel.setValueAt("TAU"+j+"(2)", 2+(j*3), i);
                }
            } else
            if(CableMainModel.getColumnName(i).equals(this.TAU2_CONN)){
                for(int j = 0;j<8;j++){
                    CableMainModel.setValueAt("TAU"+(j+8)+"(0)", (j*3), i);
                    CableMainModel.setValueAt("TAU"+(j+8)+"(1)", 1+(j*3), i);
                    CableMainModel.setValueAt("TAU"+(j+8)+"(2)", 2+(j*3), i);
                }
            } else
            if(CableMainModel.getColumnName(i).equals(this.JET1_CONN)){
                for(int j = 0;j<10;j++){
                    CableMainModel.setValueAt("JET"+j+"(0)", (j*3), i);
                    CableMainModel.setValueAt("JET"+j+"(1)", 1+(j*3), i);
                    CableMainModel.setValueAt("JET"+j+"(2)", 2+(j*3), i);
                }
            } else
            if(CableMainModel.getColumnName(i).equals(this.JET2_CONN)){
                for(int j = 0;j<15;j++){
                    CableMainModel.setValueAt("JET"+(j+10)+"(0)", (j*2), i);
                    CableMainModel.setValueAt("JET"+(j+10)+"(1)", 1+(j*2), i);
                }
            } else
            if(CableMainModel.getColumnName(i).equals(this.EN1_CONN)){
                for(int j = 0;j<8;j++){
                    CableMainModel.setValueAt("TE"+j, j, i);
                }
                for(int j = 0;j<8;j++){
                    CableMainModel.setValueAt("XE"+j, j+8, i);
                }
                for(int j = 0;j<8;j++){
                    CableMainModel.setValueAt("XS"+j, j+16, i);
                }
            } else
            if(CableMainModel.getColumnName(i).equals(this.EN2_CONN)){
                for(int j = 0;j<8;j++){
                    CableMainModel.setValueAt("RTE"+j, j, i);
                }
                for(int j = 0;j<8;j++){
                    CableMainModel.setValueAt("RXE"+j, j+8, i);
                }
            } else
            if(CableMainModel.getColumnName(i).equals(this.MUCTPI_CONN)){
                for(int j = 0;j<6;j++){
                    CableMainModel.setValueAt("MU"+j+"(0)", (j*3)+1, i);
                    CableMainModel.setValueAt("MU"+j+"(1)", (j*3)+2, i);
                    CableMainModel.setValueAt("MU"+j+"(2)", (j*3)+3, i);
                }
            } else
            if(CableMainModel.getColumnName(i).equals(this.CTPCAL_CONN)){
                CableMainModel.setValueAt("BCM_AtoC", 0, i);
                CableMainModel.setValueAt("BCM_CtoA", 1, i);
                CableMainModel.setValueAt("BCM_Wide", 2, i);
                for(int j = 0;j<3;j++){
                    CableMainModel.setValueAt("BCM_Comb("+j+")", j+3, i);
                }
                for(int j = 0;j<3;j++){
		    int ind=j+6;
                    CableMainModel.setValueAt("BCM"+ind, j+6, i);
                }
                for(int j = 0;j<8;j++){
                    CableMainModel.setValueAt("DBM"+j, j+9, i);
                }
                for(int j = 0;j<2;j++){
                    CableMainModel.setValueAt("BPTX"+j, j+17, i);
                }
                for(int j = 0;j<6;j++){
                    CableMainModel.setValueAt("LUCID"+j, j+19, i);
                }
                CableMainModel.setValueAt("ZDC_A", 25, i);
                CableMainModel.setValueAt("ZDC_C", 26, i);
                CableMainModel.setValueAt("ZDC_AND", 27, i);
                for(int j = 0;j<3;j++){
                    CableMainModel.setValueAt("CALREQ"+j, j+28, i);
                }
            } else
            if(CableMainModel.getColumnName(i).equals(this.NIM1_CONN)){
                for(int j = 0;j<9;j++){
                    CableMainModel.setValueAt("MBTS_A"+j, j, i);
                }
                for(int j = 0;j<3;j++){
                    CableMainModel.setValueAt("MBTS_A"+(j+5)*2, j+9, i);
                }
                for(int j = 0;j<3;j++){
                    CableMainModel.setValueAt("MBTS_A("+j+")", j+16, i);
                }
                CableMainModel.setValueAt("NIML1A", 19, i);
                CableMainModel.setValueAt("NIMLHCF", 20, i);
		CableMainModel.setValueAt("AFP_NSC", 21, i);
		CableMainModel.setValueAt("AFP_NSA", 22, i);
		CableMainModel.setValueAt("AFP_FSA_SIT", 23, i);
		CableMainModel.setValueAt("AFP_FSA_TOF", 24, i);
		CableMainModel.setValueAt("AFP_FSA_LOG", 25, i);
		CableMainModel.setValueAt("AFP_FSC_SIT", 26, i);
		CableMainModel.setValueAt("AFP_FSC_LOG", 27, i);
		CableMainModel.setValueAt("AFP_FSC_TOF", 28, i);

            } else
            if(CableMainModel.getColumnName(i).equals(this.NIM2_CONN)){
                for(int j = 0;j<9;j++){
                    CableMainModel.setValueAt("MBTS_C"+j, j, i);
                }
                for(int j = 0;j<3;j++){
                    CableMainModel.setValueAt("MBTS_C"+(j+5)*2, j+9, i);
                }
                for(int j = 0;j<3;j++){
                    CableMainModel.setValueAt("MBTS_C("+j+")", j+16, i);
                }
                CableMainModel.setValueAt("NIMTGC", 19, i);
                CableMainModel.setValueAt("NIMRPC", 20, i);
                CableMainModel.setValueAt("NIMTRT", 21, i);
            }
        }
        for(int i = 1; i < CTPCoreMainModel.getColumnCount(); i++){
            if(CTPCoreMainModel.getColumnName(i).equals(this.ALFA_CONN+"_Clock0")){
                for(int j = 0;j<5;j++){
                    CTPCoreMainModel.setValueAt("ALFA"+j+"(0)", 2+(j*6), i);
                    CTPCoreMainModel.setValueAt("ALFA"+j+"(1)", 3+(j*6), i);
                    CTPCoreMainModel.setValueAt("ALFA"+j+"(2)", 4+(j*6), i);
                    CTPCoreMainModel.setValueAt("ALFA"+j+"(3)", 5+(j*6), i);
                }
            } else if(CTPCoreMainModel.getColumnName(i).equals(this.ALFA_CONN+"_Clock1")){
                for(int j = 0;j<5;j++){
                    CTPCoreMainModel.setValueAt("ALFA"+j+"(0)", 2+(j*6), i);
                    CTPCoreMainModel.setValueAt("ALFA"+j+"(1)", 3+(j*6), i);
                    CTPCoreMainModel.setValueAt("ALFA"+j+"(2)", 4+(j*6), i);
                    CTPCoreMainModel.setValueAt("ALFA"+j+"(3)", 5+(j*6), i);
                }
            } else if(CTPCoreMainModel.getColumnName(i).equals(this.TOPO1_CONN+"_Clock0")){
                for(int j = 0;j<32;j++){
                    CTPCoreMainModel.setValueAt("TOPO1_"+j, j, i);
                }
            } else if(CTPCoreMainModel.getColumnName(i).equals(this.TOPO1_CONN+"_Clock1")){
                for(int j = 0;j<32;j++){
                    CTPCoreMainModel.setValueAt("TOPO1_"+(32+j), j, i);
                }
            } else if(CTPCoreMainModel.getColumnName(i).equals(this.TOPO2_CONN+"_Clock0")){
                for(int j = 0;j<32;j++){
                    CTPCoreMainModel.setValueAt("TOPO2_"+j, j, i);
                }
            } else if(CTPCoreMainModel.getColumnName(i).equals(this.TOPO2_CONN+"_Clock1")){
                for(int j = 0;j<32;j++){
                    CTPCoreMainModel.setValueAt("TOPO2_"+(32+j), j, i);
                }
            }
        }
        getMap();
    }


///Strings for hardcoding cables        
     String EMTAU_SLOT = "SLOT7";
     String EM1_CONN = "EM1";
     String EM2_CONN = "EM2";
     String TAU1_CONN = "TAU1";
     String TAU2_CONN = "TAU2";
     String JETM_SLOT = "SLOT8";
     String JET1_CONN = "JET1";
     String JET2_CONN = "JET2";
     String EN1_CONN = "EN1";
     String EN2_CONN = "EN2";
     String MUNIM_SLOT = "SLOT9";
     String MUCTPI_CONN = "MUCTPI";
     String CTPCAL_CONN = "CTPCAL";
     String NIM1_CONN = "NIM1";
     String NIM2_CONN = "NIM2";
     String COREP_SLOT = "CTPCORE";
     String ALFA_CONN = "ALFA";
     String TOPO1_CONN = "TOPO1";
     String TOPO2_CONN = "TOPO2";
    
    /**
     *
     * @param cable
     * @param start
     * @param col
     * @param table
     * @throws SQLException
     */
    public void linkPits(L1TM_TT cable, int start, int col, int table) throws SQLException {
        ArrayList<L1Pits> pits = cable.get_pits();
        if(table == 0){
            for(L1Pits pit:pits){
                String cell = (String) CableMainModel.getValueAt(start+pit.get_threshold_bit(), col);
                cell += " TIP:"+pit.get_pit_number();
                CableMainModel.setValueAt(cell,start+pit.get_threshold_bit(), col);
            }
            for(int row = 0; row < CableMainModel.getRowCount(); row++){
                String cell = (String) CableMainModel.getValueAt(row, col);
                if(!cell.contains("TIP") && highlightPositionsIn[row][col] != -1){
                    tableHighlightersIn.addHighlighter(newHighlighter(2, row, col,table), true);
                }
            }
        } else if (table ==1){
            for(L1Pits pit:pits){
                String cell = (String) CTPCoreMainModel.getValueAt(start+pit.get_threshold_bit(), col);
                cell += " TIP:"+pit.get_pit_number();
                CTPCoreMainModel.setValueAt(cell,start+pit.get_threshold_bit(), col);
            }
            for(int row = 0; row < CTPCoreMainModel.getRowCount(); row++){
                String cell = (String) CTPCoreMainModel.getValueAt(row, col);
                if(!cell.contains("TIP") && highlightPositionsCore[row][col] != -1){
                    tableHighlightersCore.addHighlighter(newHighlighter(2, row, col,table), true);
                }
            }
        }
    }
    
    /**
     * The main bit.
     * When passed a Threshold from the menu it gets its name, type, bitnum and mapping.
     * Then using the type of the threshold/the mapping is works out which cable it should go on.
     * Then it assigns it to a position on a cable and adds the TMTT to a storage vector.
     * @throws java.sql.SQLException
     */
    public void getMap() throws SQLException {
        List<L1TM_TT> cables = new ArrayList<>();
        cables = l1menu.getTMTT();
        for (Highlighter h : tableHighlightersIn.getHighlighters()) {
            tableHighlightersIn.removeHighlighter(h);
        }
        for(L1TM_TT cable:cables){
            String name = cable.get_cable_name();
            String ctpin = cable.get_cable_ctpin();
            String conn = cable.get_cable_connector();
            int start = cable.get_cable_start();
            int end = cable.get_cable_end();
            int clock = cable.get_cable_clock();
            //Start checking cables/bits in use
            if(name.equals(this.EM1_CONN) && ctpin.equals(this.EMTAU_SLOT)){
                for(int i = start; i <= end; i++){
                    if(!CableMainModel.getValueAt(i, L1CableTableModel.COL_EM1).equals("")){
                        tableHighlightersIn.addHighlighter(newHighlighter(1, i, L1CableTableModel.COL_EM1,0), true);
                    } else {
                        CableMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CableTableModel.COL_EM1);
                        tableHighlightersIn.addHighlighter(newHighlighter(0, i, L1CableTableModel.COL_EM1,0), true);
                    }
                }
                linkPits(cable, start, L1CableTableModel.COL_EM1,0);
            } else
            if(name.equals(this.EM2_CONN) && ctpin.equals(this.EMTAU_SLOT)){
                for(int i = start; i <= end; i++){
                    if(!CableMainModel.getValueAt(i, L1CableTableModel.COL_EM2).equals("")){
                        tableHighlightersIn.addHighlighter(newHighlighter(1, i, L1CableTableModel.COL_EM2,0), true);
                    } else {
                        CableMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CableTableModel.COL_EM2);
                        tableHighlightersIn.addHighlighter(newHighlighter(0, i, L1CableTableModel.COL_EM2,0), true);
                    }
                }
                linkPits(cable, start, L1CableTableModel.COL_EM2,0);
            } else
            if(name.equals(this.TAU1_CONN) && ctpin.equals(this.EMTAU_SLOT)){
                for(int i = start; i <= end; i++){
                    if(!CableMainModel.getValueAt(i, L1CableTableModel.COL_TAU1).equals("")){
                        tableHighlightersIn.addHighlighter(newHighlighter(1, i, L1CableTableModel.COL_TAU1,0), true);
                    } else {
                        CableMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CableTableModel.COL_TAU1);
                        tableHighlightersIn.addHighlighter(newHighlighter(0, i, L1CableTableModel.COL_TAU1,0), true);
                    }
                }
                linkPits(cable, start, L1CableTableModel.COL_TAU1,0);
            } else
            if(name.equals(this.TAU2_CONN) && ctpin.equals(this.EMTAU_SLOT)){
                for(int i = start; i <= end; i++){
                    if(!CableMainModel.getValueAt(i, L1CableTableModel.COL_TAU2).equals("")){
                        tableHighlightersIn.addHighlighter(newHighlighter(1, i, L1CableTableModel.COL_TAU2,0), true);
                    } else {
                        CableMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CableTableModel.COL_TAU2);
                        tableHighlightersIn.addHighlighter(newHighlighter(0, i, L1CableTableModel.COL_TAU2,0), true);
                    }
                }
                linkPits(cable, start, L1CableTableModel.COL_TAU2,0);
            } else 
            if(name.equals(this.JET1_CONN) && ctpin.equals(this.JETM_SLOT)){
                for(int i = start; i <= end; i++){
                    if(!CableMainModel.getValueAt(i, L1CableTableModel.COL_JET1).equals("")){
                        tableHighlightersIn.addHighlighter(newHighlighter(1, i, L1CableTableModel.COL_JET1,0), true);
                    } else {
                        CableMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CableTableModel.COL_JET1);
                        tableHighlightersIn.addHighlighter(newHighlighter(0, i, L1CableTableModel.COL_JET1,0), true);
                    }
                }
                linkPits(cable, start, L1CableTableModel.COL_JET1,0);
            } else
            if(name.equals(this.JET2_CONN) && ctpin.equals(this.JETM_SLOT)){
                for(int i = start; i <= end; i++){
                    if(!CableMainModel.getValueAt(i, L1CableTableModel.COL_JET2).equals("")){
                        tableHighlightersIn.addHighlighter(newHighlighter(1, i, L1CableTableModel.COL_JET2,0), true);
                    } else {
                        CableMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CableTableModel.COL_JET2);
                        tableHighlightersIn.addHighlighter(newHighlighter(0, i, L1CableTableModel.COL_JET2,0), true);
                    }
                }
                linkPits(cable, start, L1CableTableModel.COL_JET2,0);
            } else 
            if(name.equals(this.EN1_CONN) && ctpin.equals(this.JETM_SLOT)){
                for(int i = start; i <= end; i++){
                    if(!CableMainModel.getValueAt(i, L1CableTableModel.COL_EN1).equals("")){
                        tableHighlightersIn.addHighlighter(newHighlighter(1, i, L1CableTableModel.COL_EN1,0), true);
                    } else {
                        CableMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CableTableModel.COL_EN1);
                        tableHighlightersIn.addHighlighter(newHighlighter(0, i, L1CableTableModel.COL_EN1,0), true);
                    }
                }
                linkPits(cable, start, L1CableTableModel.COL_EN1,0);
            } else
            if(name.equals(this.EN2_CONN) && ctpin.equals(this.JETM_SLOT)){
                for(int i = start; i <= end; i++){
                    if(!CableMainModel.getValueAt(i, L1CableTableModel.COL_EN2).equals("")){
                        tableHighlightersIn.addHighlighter(newHighlighter(1, i, L1CableTableModel.COL_EN2,0), true);
                    } else {
                        CableMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CableTableModel.COL_EN2);
                        tableHighlightersIn.addHighlighter(newHighlighter(0, i, L1CableTableModel.COL_EN2,0), true);
                    }
                }
                linkPits(cable, start, L1CableTableModel.COL_EN2,0);
            } else
            if(name.equals(this.MUCTPI_CONN) && ctpin.equals(this.MUNIM_SLOT)){
                for(int i = start; i <= end; i++){
                    if(!CableMainModel.getValueAt(i, L1CableTableModel.COL_MUCTPI).equals("")){
                        tableHighlightersIn.addHighlighter(newHighlighter(1, i, L1CableTableModel.COL_MUCTPI,0), true);
                    } else {
                        CableMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CableTableModel.COL_MUCTPI);
                        tableHighlightersIn.addHighlighter(newHighlighter(0, i, L1CableTableModel.COL_MUCTPI,0), true);
                    }
                }
                linkPits(cable, start, L1CableTableModel.COL_MUCTPI,0);
            } else
            if(name.equals(this.CTPCAL_CONN) && ctpin.equals(this.MUNIM_SLOT)){
                for(int i = start; i <= end; i++){
                    if(!CableMainModel.getValueAt(i, L1CableTableModel.COL_CTPCAL).equals("")){
                        logger.info(cable.get_threshold().get_name());
                        tableHighlightersIn.addHighlighter(newHighlighter(1, i, L1CableTableModel.COL_CTPCAL,0), true);
                    } else {
                        CableMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CableTableModel.COL_CTPCAL);
                        tableHighlightersIn.addHighlighter(newHighlighter(0, i, L1CableTableModel.COL_CTPCAL,0), true);
                    }
                }
                linkPits(cable, start, L1CableTableModel.COL_CTPCAL,0);
            } else
            if(name.equals(this.NIM1_CONN) && ctpin.equals(this.MUNIM_SLOT)){
                for(int i = start; i <= end; i++){
                    if(!CableMainModel.getValueAt(i, L1CableTableModel.COL_NIM1).equals("")){
                        tableHighlightersIn.addHighlighter(newHighlighter(1, i, L1CableTableModel.COL_NIM1,0), true);
                    } else {
                        CableMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CableTableModel.COL_NIM1);
                        tableHighlightersIn.addHighlighter(newHighlighter(0, i, L1CableTableModel.COL_NIM1,0), true);
                    }
                }
                linkPits(cable, start, L1CableTableModel.COL_NIM1,0);
            } else
            if(name.equals(this.NIM2_CONN) && ctpin.equals(this.MUNIM_SLOT)){
                for(int i = start; i <= end; i++){
                    if(!CableMainModel.getValueAt(i, L1CableTableModel.COL_NIM2).equals("")){
                        tableHighlightersIn.addHighlighter(newHighlighter(1, i, L1CableTableModel.COL_NIM2,0), true);
                    } else {
                        CableMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CableTableModel.COL_NIM2);
                        tableHighlightersIn.addHighlighter(newHighlighter(0, i, L1CableTableModel.COL_NIM2,0), true);
                    }
                }
                linkPits(cable, start, L1CableTableModel.COL_NIM2,0);
            } else if(name.equals(this.ALFA_CONN) && ctpin.equals(this.COREP_SLOT)){
                if(clock == 0){
                    for(int i = start; i <= end; i++){    
                        if(!CTPCoreMainModel.getValueAt(i, L1CTPCoreTableModel.COL_ALFA1).equals("")){
                            CTPCoreMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CTPCoreTableModel.COL_ALFA1);
                            tableHighlightersCore.addHighlighter(newHighlighter(1, i, L1CTPCoreTableModel.COL_ALFA1,1), true);
                        } else {
                            CTPCoreMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CTPCoreTableModel.COL_ALFA1);
                            tableHighlightersCore.addHighlighter(newHighlighter(0, i, L1CTPCoreTableModel.COL_ALFA1,1), true);
                        }
                    }
                    linkPits(cable, start, L1CTPCoreTableModel.COL_ALFA1,1);
                } else if(clock == 1){
                    for(int i = start; i <= end; i++){ 
                        if(!CTPCoreMainModel.getValueAt(i, L1CTPCoreTableModel.COL_ALFA2).equals("")){
                            CTPCoreMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CTPCoreTableModel.COL_ALFA2);
                            tableHighlightersCore.addHighlighter(newHighlighter(1, i, L1CTPCoreTableModel.COL_ALFA2,1), true);
                        } else {
                            CTPCoreMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CTPCoreTableModel.COL_ALFA2);
                            tableHighlightersCore.addHighlighter(newHighlighter(0, i, L1CTPCoreTableModel.COL_ALFA2,1), true);
                        }
                    }
                    linkPits(cable, start, L1CTPCoreTableModel.COL_ALFA2,1);
                } 
            } else if(name.equals(this.TOPO1_CONN) && ctpin.equals(this.COREP_SLOT)){
                if(clock == 0){
                    for(int i = start; i <= end; i++){    
                        if(!CTPCoreMainModel.getValueAt(i, L1CTPCoreTableModel.COL_TOPO10).equals("")){
                            CTPCoreMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CTPCoreTableModel.COL_TOPO10);
                            tableHighlightersCore.addHighlighter(newHighlighter(1, i, L1CTPCoreTableModel.COL_TOPO10,1), true);
                        } else {
                            CTPCoreMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CTPCoreTableModel.COL_TOPO10);
                            tableHighlightersCore.addHighlighter(newHighlighter(0, i, L1CTPCoreTableModel.COL_TOPO10,1), true);
                        }
                    }
                    linkPits(cable, start, L1CTPCoreTableModel.COL_TOPO10,1);
                } else if(clock == 1){
                    for(int i = start; i <= end; i++){ 
                        if(!CTPCoreMainModel.getValueAt(i, L1CTPCoreTableModel.COL_TOPO11).equals("")){
                            CTPCoreMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CTPCoreTableModel.COL_TOPO11);
                            tableHighlightersCore.addHighlighter(newHighlighter(1, i, L1CTPCoreTableModel.COL_TOPO11,1), true);
                        } else {
                            CTPCoreMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CTPCoreTableModel.COL_TOPO11);
                            tableHighlightersCore.addHighlighter(newHighlighter(0, i, L1CTPCoreTableModel.COL_TOPO11,1), true);
                        }
                    }
                    linkPits(cable, start, L1CTPCoreTableModel.COL_TOPO11,1);
                } 
            } else if(name.equals(this.TOPO2_CONN) && ctpin.equals(this.COREP_SLOT)){
                if(clock == 0){
                    for(int i = start; i <= end; i++){    
                        if(!CTPCoreMainModel.getValueAt(i, L1CTPCoreTableModel.COL_TOPO20).equals("")){
                            CTPCoreMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CTPCoreTableModel.COL_TOPO20);
                            tableHighlightersCore.addHighlighter(newHighlighter(1, i, L1CTPCoreTableModel.COL_TOPO20,1), true);
                        } else {
                            CTPCoreMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CTPCoreTableModel.COL_TOPO20);
                            tableHighlightersCore.addHighlighter(newHighlighter(0, i, L1CTPCoreTableModel.COL_TOPO20,1), true);
                        }
                    }
                    linkPits(cable, start, L1CTPCoreTableModel.COL_TOPO20,1);
                } else if(clock == 1){
                    for(int i = start; i <= end; i++){ 
                        if(!CTPCoreMainModel.getValueAt(i, L1CTPCoreTableModel.COL_TOPO21).equals("")){
                            CTPCoreMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CTPCoreTableModel.COL_TOPO21);
                            tableHighlightersCore.addHighlighter(newHighlighter(1, i, L1CTPCoreTableModel.COL_TOPO21,1), true);
                        } else {
                            CTPCoreMainModel.setValueAt(cable.get_threshold().get_name(),i, L1CTPCoreTableModel.COL_TOPO21);
                            tableHighlightersCore.addHighlighter(newHighlighter(0, i, L1CTPCoreTableModel.COL_TOPO21,1), true);
                        }
                    }
                    linkPits(cable, start, L1CTPCoreTableModel.COL_TOPO21,1);
                } 
            }
        }
        //Highlight missing thresholds
        for(int col = 1; col<CableMainModel.getColumnCount(); col++){
            for(int row = 0; row<CableMainModel.getRowCount(); row++){
                if(!CableMainModel.getValueAt(row, col).toString().isEmpty() && highlightPositionsIn[row][col] == -1){
                    tableHighlightersIn.addHighlighter(newHighlighter(3, row, col,0), true);
                }
            }
        }
        tableCables.setHighlighters(tableHighlightersIn);
        tableCTPCore.setHighlighters(tableHighlightersCore);
    }

    /**
     *
     * @param which
     * @param row
     * @param column
     * @param table
     * @return
     */
    public ColorHighlighter newHighlighter(int which,int row, int column, int table) {
        ColorHighlighter Colour = new ColorHighlighter();
        if(table == 0){
            switch (which) {
                case 0:
                    Colour.setBackground(Color.RED.brighter());
                    highlightPositionsIn[row][column] = 0;
                    break;
                case 1:
                    Colour.setForeground(Color.GREEN.darker());
                    highlightPositionsIn[row][column] = 1;
                    break;
                case 2:
                    Colour.setBackground(Color.YELLOW);
                    highlightPositionsIn[row][column] = 2;
                    break;
                case 3:
                    Colour.setForeground(Color.RED);
                    highlightPositionsIn[row][column] = 3;
                    break;
                default:
                    break;
            }
        } else if (table == 1){
            switch (which) {
                case 0:
                    Colour.setBackground(Color.RED.brighter());
                    highlightPositionsCore[row][column] = 0;
                    break;
                case 1:
                    Colour.setForeground(Color.GREEN.darker());
                    highlightPositionsCore[row][column] = 1;
                    break;
                case 2:
                    Colour.setBackground(Color.YELLOW);
                    highlightPositionsCore[row][column] = 2;
                    break;
                case 3:
                    Colour.setForeground(Color.RED);
                    highlightPositionsCore[row][column] = 3;
                    break;
                default:
                    break;
            }
        }
        Pattern pattern = Pattern.compile("");
        HighlightPredicate predicate = new SearchPredicate(pattern, row, column);
        Colour.setHighlightPredicate(predicate);
        return Colour;
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        paneDescription = new javax.swing.JScrollPane();
        Description = new javax.swing.JTextPane();
        TabbedPane = new javax.swing.JTabbedPane();
        Tab1 = new javax.swing.JScrollPane();
        tableCables = new triggertool.Components.JTableX();
        Tab2 = new javax.swing.JScrollPane();
        tableCTPCore = new triggertool.Components.JTableX();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(600, 400));

        paneDescription.setViewportView(Description);

        tableCables.setModel(new triggertool.Components.L1CableTableModel());
        Tab1.setViewportView(tableCables);

        TabbedPane.addTab("CTPIN", Tab1);

        tableCTPCore.setModel(new triggertool.Components.L1CTPCoreTableModel());
        Tab2.setViewportView(tableCTPCore);

        TabbedPane.addTab("CTPCore", Tab2);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(TabbedPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 1030, Short.MAX_VALUE)
                    .add(paneDescription))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(TabbedPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 677, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(paneDescription, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        TabbedPane.getAccessibleContext().setAccessibleName("CTPCORE");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextPane Description;
    private javax.swing.JScrollPane Tab1;
    private javax.swing.JScrollPane Tab2;
    private javax.swing.JTabbedPane TabbedPane;
    private javax.swing.JScrollPane paneDescription;
    private triggertool.Components.JTableX tableCTPCore;
    private triggertool.Components.JTableX tableCables;
    // End of variables declaration//GEN-END:variables
}
