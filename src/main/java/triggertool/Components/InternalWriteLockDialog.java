/*
 * InternalWriteLockDialog.java
 *
 * Created on Nov 20, 2009, 10:53:40 AM
 */
package triggertool.Components;

import java.awt.Color;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Connections.UserMode;
import triggerdb.Entities.InternalWriteLock;
import triggerdb.UploadException;
import triggertool.Panels.MessageDialog;
import java.awt.datatransfer.*;
import java.awt.Toolkit;
import java.awt.Frame;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

/**
 * The panel to display the triggerDB lock and provides a way to "forceUnlock"
 * the trigger DB.
 * @author nozicka
 */
public final class InternalWriteLockDialog extends JDialog
        implements Runnable {

    /** Message Log. */
    private static final Logger logger = Logger.getLogger("TriggerTool");
    /** Internal WriteLock. */
    private static InternalWriteLock lock = null;
    /** TODO. */
    private boolean active = true;
    /** TODO. */
    public boolean forceDisplay = false;
    /** Thread for regular update of the status. */
    private Thread updateStatus = null;
    /** MESSAGES. */
    /** DB locked message.*/
    private static final String MSG_LOCKED = "TriggerDB is locked for writing\n"
            + "Please wait\n"
            + "If necessary inform expert\n";
    /** No Write lock present. */
    private static final String MSG_NOWRTLOCK = "\nNo Write Lock Present.\n";
    /** Remove lock message. */
    private static final String MSG_REMOVELCK =
            " \n Dialog will be closed in 5s. \n";
    private static final String MSG_ERROR_REMOVING = "Can not remove the lock."
            + "\nplease informe the expert"
            + "\nimmediately.";

    /**
     * Creates new form InternalWriteLock.
     * @param parent the parent window.
     */
    public InternalWriteLockDialog(final Frame parent) {
        // The Java 1.5 doesn't have: javax.swing.Dialog(java.awt.Window parent)
        // wait for Java 1.6
        // public InternalWriteLockDialog (java.awt.Window parent) {
        super((JDialog)null);
        this.initPanel();
        this.setLocationRelativeTo(parent);
    }

    /**
     * Creates new form InternalWriteLock with forced display set -
     * quit possible only through close.
     * @param parent The parent awt component.
     * @param force
     */
    public InternalWriteLockDialog(final Frame parent,
            final boolean force) {
        // The Java 1.5 doesn't have: javax.swing.Dialog(java.awt.Window parent)
        // wait for Java 1.6
        // public InternalWriteLockDialog (java.awt.Window parent) {
        super();
        this.forceDisplay = force;
        this.initPanel();
        this.setLocationRelativeTo(parent);

    }

    /**
     * do all standard things to initialize the panel. to share between the 2
     * constructors.
     */
    private void initPanel() {
        Boolean isLocked = false;
        try {
            isLocked = InternalWriteLock.getInstance().isLocked();

            if (!isLocked) {
                logger.fine("No internal lock present");
                forceDisplay = true;
            } else {
                lock = new InternalWriteLock(1);
            }
        } catch (SQLException e) {
            MessageDialog.showReportDialog(e);
            logger.log(Level.WARNING, "Caught in InternalWriteLockDialog:{0}", e.getMessage());
            return;
        }

        // Initialize the dialog components
        initComponents();
        // Start the thread to update the status
        this.start();

        InitInfo theInitInfo = ConnectionManager.getInstance().getInitInfo();
        // Get user level.
        UserMode userMode = theInitInfo.getUserMode();
        // TODO: check with Joerg whether this is safe. Anybody cold just set
        // this flag!
        forceUnlockButton.setEnabled(false);

        // Enable and Show lock button for DBA only.
        if (userMode == UserMode.DBA) {
            lockButton.setEnabled(true);
            lockButton.setVisible(true);
        } else {
            lockButton.setEnabled(false);
            lockButton.setVisible(false);
        }
        // Display the dialog window
        setVisible(true);
    }

    /** Implementation of the thread. */
    public void start() {
        updateStatus = new Thread(this);
        updateStatus.start();
    }

    /** Update the lock dialog status. */
    @Override
    public void run() {
        try {
            while (active || forceDisplay) {
                if (InternalWriteLock.getInstance().isLocked()) {
                    if (lock == null || lock.get_id() < 1) {
                        lock = new InternalWriteLock(1);
                    }
                    lock.forceLoad();
                    copyToClipboardButton.setEnabled(true);
                    copyToClipboardButton.setVisible(true);

                    forceUnlockButton.setEnabled(true);
                    forceUnlockButton.setVisible(true);
                    
                    userLabelValue.setText(lock.get_username());
                    processLabelValue.setText(lock.get_process());
                    timeLabelValue.setText(lock.get_modified().toString());
                    descriptionText.setText(lock.getDescription());
                    this.writeLockMsgTextArea.setText(MSG_LOCKED);
                } else {
                    userLabelValue.setText("");
                    processLabelValue.setText("");
                    timeLabelValue.setText("");
                    descriptionText.setText("");
                    copyToClipboardButton.setEnabled(false);
                    copyToClipboardButton.setVisible(false);
                    forceUnlockButton.setEnabled(false);
                    forceUnlockButton.setVisible(false);

                    if (forceDisplay) {
                        mainLabel.setText("Internal Write Lock");
                        this.writeLockMsgTextArea.setText(MSG_NOWRTLOCK);
                    } else {
                        mainLabel.setText("Internal Write Lock Removed");
                        this.writeLockMsgTextArea.setText(MSG_REMOVELCK);
                        try {
                            // Put to sleep to see the information for a time.
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            logger.log(Level.FINE, "Thread interrupted:\t{0}", e.getMessage());
                        }
                    }

                    lock = null;
                    active = false;
                }
                try {
                    Thread.sleep(100); // wait 1s before updating the status
                } catch (InterruptedException e) {
                    logger.log(Level.FINE, "Thread interrupted:\t{0}", e.getMessage());
                }
            }
            // Dispose of the dialog window
            if (!forceDisplay) {
                dispose();
            }
        } catch (SQLException e) {
            logger.log(Level.WARNING, "Caught:{0}", e.getMessage());
        }
    }

    /**
     * Access to the trigger DB lock.
     * @return the trigDB lock.
     */
    public InternalWriteLock get_lock() {
        return lock;
    }

    /** Set a dummy lock for testing.*/
    private void setLock() throws UploadException, SQLException {
        InternalWriteLock iwl = InternalWriteLock.getInstance();
        boolean locked = false;
        try {
            locked = iwl.isLocked();
        } catch (SQLException ex) {
            logger.log(Level.INFO, " cannot get lock \n{0}", ex);
        }
        if (locked) {
            if (iwl == null || lock.get_id() < 1) {
                iwl = new InternalWriteLock(1);
            }
            iwl.forceLoad();
            logger.info(" logger already set.");
        } else {
            InitInfo ii = ConnectionManager.getInstance().getInitInfo();
            iwl = new InternalWriteLock(-1);
            iwl.set_username(ii.getUserName());
            iwl.setDescription(" Testing Locking.");
            iwl.set_process(InternalWriteLock.getProcessString());
            Date now = new Date();
            Timestamp time = new Timestamp(now.getTime());
            logger.log(Level.SEVERE, "time {0} date {1}", new Object[]{time, now});
            iwl.set_modified_time(time);
            iwl.set_modified(now);
            iwl.setLock();
            logger.info(" setting lock.");
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainLabel = new javax.swing.JLabel();
        writeLockMsgTextArea = new javax.swing.JTextArea();
        jSeparator1 = new javax.swing.JSeparator();
        intWriteLockInfoPanel = new javax.swing.JPanel();
        userLabel = new javax.swing.JLabel();
        processLabel = new javax.swing.JLabel();
        timeLabel = new javax.swing.JLabel();
        descriptionLabel = new javax.swing.JLabel();
        userLabelValue = new javax.swing.JLabel();
        processLabelValue = new javax.swing.JLabel();
        timeLabelValue = new javax.swing.JLabel();
        descriptionPane = new javax.swing.JScrollPane();
        descriptionText = new javax.swing.JTextPane();
        buttonsPanel = new javax.swing.JPanel();
        lockButton = new javax.swing.JButton();
        forceUnlockButton = new javax.swing.JButton();
        copyToClipboardButton = new javax.swing.JButton();
        closeButton = new javax.swing.JButton();

        setMinimumSize(new java.awt.Dimension(557, 450));
        setResizable(false);
        setSize(new java.awt.Dimension(440, 100));

        mainLabel.setFont(mainLabel.getFont().deriveFont(mainLabel.getFont().getStyle() | java.awt.Font.BOLD, mainLabel.getFont().getSize()+8));
        mainLabel.setText("Internal Write Lock");

        writeLockMsgTextArea.setEditable(false);
        writeLockMsgTextArea.setBackground(new java.awt.Color(238, 238, 238));
        writeLockMsgTextArea.setColumns(20);
        writeLockMsgTextArea.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        writeLockMsgTextArea.setRows(3);
        writeLockMsgTextArea.setText("line1\nline2\nline3");
        writeLockMsgTextArea.setBorder(null);

        userLabel.setFont(userLabel.getFont().deriveFont(userLabel.getFont().getStyle() | java.awt.Font.BOLD, userLabel.getFont().getSize()+2));
        userLabel.setText("User:");

        processLabel.setFont(processLabel.getFont().deriveFont(processLabel.getFont().getStyle() | java.awt.Font.BOLD, processLabel.getFont().getSize()+2));
        processLabel.setText("Process:");

        timeLabel.setFont(timeLabel.getFont().deriveFont(timeLabel.getFont().getStyle() | java.awt.Font.BOLD, timeLabel.getFont().getSize()+2));
        timeLabel.setText("Start time:");

        descriptionLabel.setFont(descriptionLabel.getFont().deriveFont(descriptionLabel.getFont().getStyle() | java.awt.Font.BOLD, descriptionLabel.getFont().getSize()+2));
        descriptionLabel.setText("Description:");

        userLabelValue.setFont(userLabelValue.getFont().deriveFont(userLabelValue.getFont().getStyle() & ~java.awt.Font.BOLD, userLabelValue.getFont().getSize()+1));
        userLabelValue.setText("userLabel");

        processLabelValue.setFont(processLabelValue.getFont().deriveFont(processLabelValue.getFont().getStyle() & ~java.awt.Font.BOLD, processLabelValue.getFont().getSize()+1));
        processLabelValue.setText("nozicka@raistlin.desy.de; IP: 131.169.136.255");

        timeLabelValue.setFont(timeLabelValue.getFont().deriveFont(timeLabelValue.getFont().getStyle() & ~java.awt.Font.BOLD, timeLabelValue.getFont().getSize()+1));
        timeLabelValue.setText("timeLabel");

        descriptionText.setEditable(false);
        descriptionText.setFont(descriptionText.getFont().deriveFont(descriptionText.getFont().getStyle() & ~java.awt.Font.BOLD, descriptionText.getFont().getSize()+1));
        descriptionPane.setViewportView(descriptionText);

        org.jdesktop.layout.GroupLayout intWriteLockInfoPanelLayout = new org.jdesktop.layout.GroupLayout(intWriteLockInfoPanel);
        intWriteLockInfoPanel.setLayout(intWriteLockInfoPanelLayout);
        intWriteLockInfoPanelLayout.setHorizontalGroup(
            intWriteLockInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(intWriteLockInfoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(intWriteLockInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(intWriteLockInfoPanelLayout.createSequentialGroup()
                        .add(intWriteLockInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(timeLabel)
                            .add(descriptionLabel))
                        .add(18, 18, 18)
                        .add(intWriteLockInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(timeLabelValue, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(processLabelValue, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(userLabelValue, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(descriptionPane)))
                    .add(userLabel)
                    .add(processLabel))
                .addContainerGap())
        );
        intWriteLockInfoPanelLayout.setVerticalGroup(
            intWriteLockInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(intWriteLockInfoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(intWriteLockInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(userLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 17, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(userLabelValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 17, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(8, 8, 8)
                .add(intWriteLockInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(processLabel)
                    .add(processLabelValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 17, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(intWriteLockInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(timeLabel)
                    .add(timeLabelValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 17, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(intWriteLockInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(intWriteLockInfoPanelLayout.createSequentialGroup()
                        .add(descriptionLabel)
                        .add(79, 79, 79))
                    .add(intWriteLockInfoPanelLayout.createSequentialGroup()
                        .add(descriptionPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                        .addContainerGap())))
        );

        lockButton.setText("Lock");
        lockButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lockButtonActionPerformed(evt);
            }
        });

        forceUnlockButton.setText("Force Unlock");
        forceUnlockButton.setEnabled(false);
        forceUnlockButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                forceUnlockButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout buttonsPanelLayout = new org.jdesktop.layout.GroupLayout(buttonsPanel);
        buttonsPanel.setLayout(buttonsPanelLayout);
        buttonsPanelLayout.setHorizontalGroup(
            buttonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(buttonsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(lockButton)
                .add(26, 26, 26)
                .add(forceUnlockButton)
                .addContainerGap())
        );
        buttonsPanelLayout.setVerticalGroup(
            buttonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(buttonsPanelLayout.createSequentialGroup()
                .add(buttonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lockButton)
                    .add(forceUnlockButton))
                .add(26, 26, 26))
        );

        copyToClipboardButton.setText("Copy Lock Info To Clipboard");
        copyToClipboardButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyToClipboardButtonActionPerformed(evt);
            }
        });

        closeButton.setText("Close");
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(151, 151, 151)
                .add(writeLockMsgTextArea, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 326, Short.MAX_VALUE)
                .add(80, 80, 80))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(intWriteLockInfoPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                        .add(176, 176, 176)
                        .add(mainLabel)
                        .add(0, 0, Short.MAX_VALUE))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jSeparator1))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                        .addContainerGap()
                        .add(copyToClipboardButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(buttonsPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(closeButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 81, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(mainLabel)
                .add(18, 18, 18)
                .add(writeLockMsgTextArea, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 61, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(15, 15, 15)
                .add(intWriteLockInfoPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(buttonsPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(copyToClipboardButton)
                    .add(closeButton))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(575, 452));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
        active = false;
        forceDisplay = false;
        // Interrupt the running monitoring thread in order to stop it
        // updateStatus.interrupt();
        dispose();
}//GEN-LAST:event_closeButtonActionPerformed

    private void forceUnlockButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_forceUnlockButtonActionPerformed
        Boolean forceRemoveLock = false;
        try {
            Object[] options = {"Yes, I understand", "No, I will check again"};
            int showOptionDialog = JOptionPane.showOptionDialog(this,
                    "Are you sure you have understood the source of this lock?\nRemoving a valid lock can serious damage to ongoing database saves",
                    "Write lock confirmation ", JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
            if (showOptionDialog == 1) {
                return;
            }
            forceRemoveLock = InternalWriteLock.getInstance().forceRemoveLock();
        } catch (SQLException e) {
            logger.log(Level.WARNING, "Caught in forceUnlockButtonActionPerformed:{0}", e.getMessage());
            writeLockMsgTextArea.setForeground(Color.RED);
            String ex = "Exception removing lock:";
            ex += e.getLocalizedMessage();
            writeLockMsgTextArea.setText(ex);
            return;

        }
        if (!forceRemoveLock) {
            writeLockMsgTextArea.setForeground(Color.RED);
            writeLockMsgTextArea.setText(MSG_ERROR_REMOVING);
        }
        else{
            forceUnlockButton.setEnabled(false);
        }
}//GEN-LAST:event_forceUnlockButtonActionPerformed

    private void lockButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lockButtonActionPerformed
        try {
            this.setLock();
        } catch (UploadException | SQLException ex) {
            Logger.getLogger(InternalWriteLockDialog.class.getName()).log(Level.SEVERE, null, ex);
            MessageDialog.showReportDialog(ex);
        }
    }//GEN-LAST:event_lockButtonActionPerformed

    private void copyToClipboardButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyToClipboardButtonActionPerformed
        String lockInfoString = lock.get_username() + "\n" + lock.get_process() + "\n" + lock.get_modified().toString() + "\n" + lock.getDescription();
        StringSelection stringSelection = new StringSelection(lockInfoString);
        Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        systemClipboard.setContents(stringSelection, null);
    }//GEN-LAST:event_copyToClipboardButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel buttonsPanel;
    private javax.swing.JButton closeButton;
    private javax.swing.JButton copyToClipboardButton;
    private javax.swing.JLabel descriptionLabel;
    private javax.swing.JScrollPane descriptionPane;
    private javax.swing.JTextPane descriptionText;
    private javax.swing.JButton forceUnlockButton;
    private javax.swing.JPanel intWriteLockInfoPanel;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JButton lockButton;
    private javax.swing.JLabel mainLabel;
    private javax.swing.JLabel processLabel;
    private javax.swing.JLabel processLabelValue;
    private javax.swing.JLabel timeLabel;
    private javax.swing.JLabel timeLabelValue;
    private javax.swing.JLabel userLabel;
    private javax.swing.JLabel userLabelValue;
    private javax.swing.JTextArea writeLockMsgTextArea;
    // End of variables declaration//GEN-END:variables
}
