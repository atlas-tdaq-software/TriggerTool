package triggertool.Components;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.Frame;
import javax.swing.JDialog;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.DBTechnology;

///List the tables and fields in the database.
/**
 * A class that displays in a form a list of tables defined in the database.  Selecting a table
 * allows the user to see the columns defined for that table.  Works for Oracle. 
 * Useful for checking the schema.
 * 
 * @author  Simon Head
 */
public class SchemaCheck extends JDialog implements ListSelectionListener {

    ///Required by Java.
    private static final long serialVersionUID = 1L;
    ///Message Log.

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    ///A list of table names.
    private DefaultListModel<String> items = new DefaultListModel<>();

    /**
     * Constructor creates and displays the form.  It queries the database for a
     * list of tables only.  Information for a specific table is loaded when
     * that item is clicked in the list.
     * 
     * @param parent
     * @param modal
     */
    public SchemaCheck(final Frame parent) {
        super((JDialog)null);
        setTitle("Displaying Schema Details");
        initComponents();
        checkdb();
        listTables.addListSelectionListener(this);
        this.setLocationRelativeTo(parent);
        setVisible(true);
    }

    /** 
     * First get the schema number and tag of trig db from the database. 
     * Then load a list of table names.  This is done in different ways 
     * depending on the DB Technology.
     */
    private void checkdb() {
        
        String query = "SELECT TS_ID, TS_TRIGDB_TAG FROM TRIGGER_SCHEMA";
        query = ConnectionManager.getInstance().fix_schema_name(query);

        //System.out.println("---PJB IN SCHEMA CHECK " + query);
        
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query); ResultSet rset = ps.executeQuery()) {
            while (rset.next()) {
                labelSchema.setText("Schema Version " + rset.getInt("TS_ID") + ", tagged as " + rset.getString("TS_TRIGDB_TAG"));
            }
            rset.close();
        }
        catch (SQLException e) {
            logger.log(Level.WARNING, "Failed to check schema: {0}", e.getMessage());
            e.printStackTrace();
        }
        
        //mysql
        query = "SHOW TABLES";
        //oracle
        switch (ConnectionManager.getInstance().getInitInfo().getTechnology()) {
            case ORACLE:
                query = "SELECT TABLE_NAME FROM TABS ORDER BY TABLE_NAME ASC";
                break;
        }

        query = ConnectionManager.getInstance().fix_schema_name(query);
        //System.out.println("---PJB IN SCHEMA CHECK looking for tables " + query);
        
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query); ResultSet rset = ps.executeQuery()) {
            while (rset.next()) {
                //System.out.println("---PJB IN SCHEMA CHECK looking at result");
                String temp = rset.getString(1);
                items.addElement(temp);
            }
            rset.close();
        } catch (SQLException e) {
            logger.log(Level.WARNING, "Failed to check schema: {0}", e.getMessage());
            e.printStackTrace();
        }
    }

    /** 
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonDone = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        listTables = new javax.swing.JList<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableDetails = new javax.swing.JTable();
        labelSchema = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(645, 396));

        buttonDone.setText("Done");
        buttonDone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDoneActionPerformed(evt);
            }
        });

        listTables.setModel(items);
        jScrollPane1.setViewportView(listTables);

        tableDetails.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Column Name", "Column Type", "Column Length"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tableDetails);

        labelSchema.setText("jLabel1");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(labelSchema, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 621, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 223, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jScrollPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, buttonDone))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(labelSchema)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 315, Short.MAX_VALUE)
                    .add(jScrollPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 315, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(buttonDone)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void buttonDoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDoneActionPerformed
        dispose();
}//GEN-LAST:event_buttonDoneActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonDone;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel labelSchema;
    private javax.swing.JList<String> listTables;
    private javax.swing.JTable tableDetails;
    // End of variables declaration//GEN-END:variables

    /**
     * When the user clicks on a table display its details.
     * 
     * @param e The list selection event that caused this function to run.
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource() == listTables) {
            String query = "SELECT * FROM " + listTables.getSelectedValue();
            query = ConnectionManager.getInstance().fix_schema_name(query);

            try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query); ResultSet rset = ps.executeQuery()) {
                java.sql.ResultSetMetaData md = rset.getMetaData();
                int col = md.getColumnCount();

                DefaultTableModel mod = (DefaultTableModel) tableDetails.getModel();
                mod.setRowCount(col);

                for (int i = 1; i <= col; i++) {
                    tableDetails.getModel().setValueAt(md.getColumnName(i), i - 1, 0);
                    tableDetails.getModel().setValueAt(md.getColumnTypeName(i), i - 1, 1);
                    tableDetails.getModel().setValueAt(md.getColumnDisplaySize(i), i - 1, 2);
                }
                rset.close();
                
            } catch (SQLException e2) {
                logger.log(Level.WARNING, "Failed to check schema: {0}", e2.getMessage());
                e2.printStackTrace();
            }
        }
    }
}
