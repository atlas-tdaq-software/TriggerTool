package triggertool.Components;

//import org.jdesktop.swingx.decorator.FilterPipeline;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableRowSorter;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.UserMode;

import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.SMT.SuperMasterTable;

///Monitoring GUI
import triggerdb.Entities.L1.L1Menu;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.L1.L1Prescale;
import triggerdb.Entities.L1Links.L1TM_PS;

/**
 * A GUI that displays the monitor information for L1, for the selected 
 * Super Master Table.
 * 
 * @author Simon Head
 */
public class L1ItemMonitoring extends JDialog implements TableModelListener {

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    /** 
     * Pointer to the Super Master Table.  We edit the monitors for a specific
     * master table, so we need this.
     */
    private SuperMasterTable smt = null;
    private L1Menu l1menu = null;
    /** 
     * A set of items within the L1 Menu.  
     */
    private ArrayList<L1Item> items = new ArrayList<>();
    private TreeMap<String,L1Item> mappedItems = new TreeMap<>();

    private final L1ItemMonitoringTableModel MonMainModel;
    private TableRowSorter<L1ItemMonitoringTableModel> sorter;  

    // store the number of per-bunch monitoring counters
    // Format: LF: TAV, TAP, TBP, HF: TAV, TAP, TBP
    int nbPbmCounters[] = {0,0,0,0,0,0};
    int nbPbmBlocksLF[] = new int[3];
    int nbPbmBlocksHF[] = new int[3];
     
    /**
     * An integer representing the role of the user (see ConnectionManager).
     */
    private UserMode userMode = UserMode.UNKNOWN;

    /**
     * Creates the L1Monitoring form and displays it.
     * 
     * @param parent The frame that owns this one, can be null.
     * @param smt Super Master Table record which we wish to view / edit.
     */
    public L1ItemMonitoring(java.awt.Frame parent, SuperMasterTable smt) {
        super((JDialog)null);
        initComponents();
        setTitle("L1 Monitors: SMT " +smt);
        this.smt = smt;
        try {
            this.l1menu = smt.get_l1_master_table().get_menu();
        } catch (SQLException ex) {
            String message = "Error occurred while creating the L1 Monitoring panel.";
            logger.log(Level.SEVERE, message, ex);
            JOptionPane.showMessageDialog(parent, "SQL Error" , message +
            " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
        }

        MonMainModel = (L1ItemMonitoringTableModel) tableMonitors.getModel();
        sorter = new TableRowSorter<L1ItemMonitoringTableModel>(MonMainModel);
        tableMonitors.setRowSorter(sorter);
                
        JTableHeader headerL1 = tableMonitors.getTableHeader();
        try {
            items.addAll(l1menu.getItems());
            for(L1Item item:items){
               mappedItems.put(item.get_name(), item);
            }
            loadTable();
        } catch (SQLException ex) {
            String message = "Error occurred while creating the L1 Monitoring panel.";
            logger.log(Level.SEVERE, message, ex);
            JOptionPane.showMessageDialog(parent, "SQL Error" , message +
            " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
        }

        userMode = ConnectionManager.getInstance().getInitInfo().getUserMode();

        buttonClose.setEnabled(true);

        if (userMode.ordinal() >= UserMode.SHIFTER.ordinal()) {
            tableMonitors.setEditable(true);
        }
        setLocationRelativeTo(parent);
        MonMainModel.addTableModelListener(this);
        setVisible(true);
    }

    /**
     * Populate the table with counters loaded from the menu.
     */
    private void loadTable() throws SQLException {
//        DefaultTableModel mod = (DefaultTableModel) tableMonitors.getModel();
        MonMainModel.clearNumRows();
        int rows = MonMainModel.getRowCount();
        
        for (L1Item item : items) {
            ArrayList<Object> values = new ArrayList<>();
            values.add(item.get_name());
            String monitor = item.get_monitor();
	    // if monitors are not defined in the menu, set all PBM counters to 0
	    if(monitor.equals("None")) {
		for (int i=0; i<6; i++) values.add( false );
	    }
	    else {
		int idx = 0;
		for (int i = 0; i < monitor.length(); i++) {
		    char c = monitor.charAt(i);
		    if (Character.isDigit(c)) {
			int val = Character.getNumericValue(c);
			nbPbmCounters[idx] += val;
			values.add( val==1 );
			idx++;
		    }
		}
	    }

           MonMainModel.add(values);
          rows = MonMainModel.getRowCount();
        }
 
        for (int i=0; i<3; i++) {
            nbPbmBlocksLF[i] = (nbPbmCounters[i]%8 == 0) ? nbPbmCounters[i]/8 : nbPbmCounters[i]/8 +1 ;
            nbPbmBlocksHF[i] = (nbPbmCounters[i+3]%8 == 0) ? nbPbmCounters[i+3]/8 : nbPbmCounters[i+3]/8 +1 ;
       }   
       
        updateCellEditable();
        updateText();
        MonMainModel.setChanged(0);
    }

    /** 
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonClose = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableMonitors = new triggertool.Components.JTableX();
        LabelHF = new javax.swing.JLabel();
        LabelLF = new javax.swing.JLabel();
        LabelTotal = new javax.swing.JLabel();
        TextFilter = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        buttonSave = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1050, 400));

        buttonClose.setText("Close");
        buttonClose.setEnabled(false);
        buttonClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCloseActionPerformed(evt);
            }
        });

        tableMonitors.setModel(new triggertool.Components.L1ItemMonitoringTableModel());
        tableMonitors.setEditable(false);
        tableMonitors.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableMonitorsMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableMonitors);

        LabelHF.setText(" ");

        LabelLF.setText(" ");

        LabelTotal.setText(" ");

        TextFilter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TextFilterKeyReleased(evt);
            }
        });

        jLabel1.setText("Item name:");

        buttonSave.setText("Save");
        buttonSave.setEnabled(false);
        buttonSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSaveActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jScrollPane1)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(LabelTotal, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 1221, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                                .add(LabelHF, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(buttonSave))
                            .add(org.jdesktop.layout.GroupLayout.LEADING, LabelLF, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(buttonClose))
                    .add(layout.createSequentialGroup()
                        .add(jLabel1)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(TextFilter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 280, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(TextFilter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel1))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 575, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(LabelLF)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(LabelHF)
                    .add(buttonClose)
                    .add(buttonSave))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(LabelTotal)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Close the window.
     * 
     * @param evt Event that caused this.
     */
private void buttonCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCloseActionPerformed

    if (MonMainModel.getChanged() == 0) {
        dispose();
    } else if (MonMainModel.getChanged() != 0) {
        int Confirm;
        Object[] options = {"Yes", "No"};

        Confirm = JOptionPane.showOptionDialog(null,
                "This will save the current monitors to the DB. \nDo you want to do this?",
                "Are you sure?",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[1]);
        if (Confirm == 0) {
           dispose();
        } 
    }   
}//GEN-LAST:event_buttonCloseActionPerformed

    private void DoSaveAction() throws SQLException {
        //l1menu.getItems().clear();
        logger.log(Level.INFO, "Monitors PSK {0}", l1menu.getPrescaleSets());
        for (int row = 0; row < MonMainModel.getRowCount(); ++row) {
            //Check if the monitor changed, if it didn't don't do anything!
            String item_name = MonMainModel.getValueAt(row, 0).toString();
            String monitor = "LF:";
            
            // convert the PBM monitoring counters into the expected format
            for (int col=1; col<MonMainModel.getColumnCount(); col++) {
                if(col==4) monitor += "|HF:";
                String value = (boolean) MonMainModel.getValueAt(row, col) ? "1" : "0";
                monitor += value;
            }
            if(!mappedItems.get(item_name).get_monitor().equals(monitor)){
                L1Item item = mappedItems.get(item_name);
                item.set_monitor(monitor);
                item.set_id(-1);
                item.save();
                mappedItems.put(item_name, item);
                logger.info(item.toString());
            }
        }
        items.clear();
        items.addAll(mappedItems.values());
        l1menu.setItems(items);
        
        l1menu.getMonitors();
        l1menu.getTMTT();
        List<L1Prescale> PSKs = l1menu.getPrescaleSets();
        
        int menu_id = l1menu.save();
        for(L1Prescale psk:PSKs){
            L1TM_PS newlink = new L1TM_PS();
            newlink.set_menu_id(menu_id);
            newlink.set_prescale_set_id(psk.get_id());
            //save link
            newlink.save();
        }
        L1Master master = smt.get_l1_master_table();
        master.set_menu_id(menu_id);
        master.set_id(-1);
        int master_id = master.save();
        smt.set_l1_master_table_id(master_id);
        List<HLTRelease> releases = smt.getReleases();
        logger.fine(smt.toString());
        smt.set_origin("evolution of " + smt.get_id());
        smt.set_id(-1);
        smt.save();
        smt.save_releases(releases);
    }

    private void TextFilterKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TextFilterKeyReleased

    String pattern = TextFilter.getText();
     RowFilter<L1ItemMonitoringTableModel, Object> rf = null;
        //If current expression doesn't parse, don't update.
        try {
            rf = RowFilter.regexFilter(pattern, 0);
        } catch (java.util.regex.PatternSyntaxException e) {
            return;
        }
       sorter.setRowFilter(rf);
    }//GEN-LAST:event_TextFilterKeyReleased

    private void buttonSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSaveActionPerformed
        try {
            DoSaveAction();
            buttonSave.setEnabled(false);
        } catch (SQLException ex) {
            String message = "Error occurred while performing the cancel operation in L1 Monitoring panel.";
            logger.log(Level.SEVERE, message, ex);
            JOptionPane.showMessageDialog(this, "SQL Error", message
                    + " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_buttonSaveActionPerformed

    private void tableMonitorsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMonitorsMouseClicked
        if (userMode.ordinal() >= UserMode.SHIFTER.ordinal()) {
            buttonSave.setEnabled(true);
        }
    }//GEN-LAST:event_tableMonitorsMouseClicked

    @Override
    public void tableChanged(TableModelEvent e) {
         int row = tableMonitors.getSelectedRows()[0];
         int col = tableMonitors.getSelectedColumns()[0];
         
         boolean doMonitor = (boolean) tableMonitors.getValueAt(row,col);

         if(doMonitor) nbPbmCounters[col-1] += 1;
         else nbPbmCounters[col-1] -= 1;  

        // update the number of blocks
        if(col<4) nbPbmBlocksLF[col-1] = (nbPbmCounters[col-1]%8 == 0) ? nbPbmCounters[col-1]/8 : nbPbmCounters[col-1]/8 +1 ;
        else      nbPbmBlocksHF[col-4] = (nbPbmCounters[col-1]%8 == 0) ? nbPbmCounters[col-1]/8 : nbPbmCounters[col-1]/8 +1 ;


         updateCellEditable();
         updateText();

    }
    
    private void updateCellEditable() {

        // checks number of monitoring counters:
        // Each counter type (TAV, TAP, TBP) has 8 blocks of 8 counters
        // Each block can only have either low or high frequency monitoring counters    
  
        for (int i = 0; i < 3; i++) {
            // resut all columns to true
            MonMainModel.setColumnEditable(i+1, true);
            MonMainModel.setColumnEditable(i+4, true);
            // disable the columns coresponding to counters already full
            if ((nbPbmBlocksLF[i] + nbPbmBlocksHF[i]) == 8) {
                // can not add new block, check if the existing blocks are full
                if (nbPbmCounters[i] == nbPbmBlocksLF[i] * 8) {
                    // can not add any new LF monitoring counter
                    MonMainModel.setColumnEditable(i+1, false);
                }
                if (nbPbmCounters[i+3] == nbPbmBlocksHF[i] * 8) {
                    // can not add any new HF monitoring counter
                    MonMainModel.setColumnEditable(i+4, false);                  
                }
            }
        } 
    }

    private void updateText() {
        
        String txtLF = "Low Frequency:  ";
        txtLF += "TAV = " + nbPbmCounters[0] + " counters in " + nbPbmBlocksLF[0] + " blocks, ";
        txtLF += "TAP = " + nbPbmCounters[1] + " counters in " + nbPbmBlocksLF[1] + " blocks, ";
        txtLF += "TBP = " + nbPbmCounters[2] + " counters in " + nbPbmBlocksLF[2] + " blocks";
        LabelLF.setText(txtLF);

        String txtHF = "High Frequency: ";
        txtHF += "TAV = " + nbPbmCounters[3] + " counters in " + nbPbmBlocksHF[0] + " blocks, ";
        txtHF += "TAP = " + nbPbmCounters[4] + " counters in " + nbPbmBlocksHF[1] + " blocks, ";
        txtHF += "TBP = " + nbPbmCounters[5] + " counters in " + nbPbmBlocksHF[2] + " blocks";
        LabelHF.setText(txtHF);

        String txtTot = "Total: ";
        txtTot += "TAV = " + (nbPbmCounters[0]+nbPbmCounters[3]) + "/64 counters in " + (nbPbmBlocksLF[0]+nbPbmBlocksHF[0]) + "/8 blocks, ";
        txtTot += "TAP = " + (nbPbmCounters[1]+nbPbmCounters[4]) + "/64 counters in " + (nbPbmBlocksLF[1]+nbPbmBlocksHF[1]) + "/8 blocks, ";
        txtTot += "TBP = " + (nbPbmCounters[2]+nbPbmCounters[5]) + "/64 counters in " + (nbPbmBlocksLF[2]+nbPbmBlocksHF[2]) + "/8 blocks";
        LabelTotal.setText(txtTot);
        
    } 
            
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelHF;
    private javax.swing.JLabel LabelLF;
    private javax.swing.JLabel LabelTotal;
    private javax.swing.JTextField TextFilter;
    private javax.swing.JButton buttonClose;
    private javax.swing.JButton buttonSave;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private triggertool.Components.JTableX tableMonitors;
    // End of variables declaration//GEN-END:variables

}

