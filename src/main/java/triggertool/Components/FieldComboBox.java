package triggertool.Components;

import triggerdb.Entities.AbstractTable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;

import javax.swing.JComboBox;

///ComboBox used to search within a specific table
/**
 * This class controls the ComboBox that shows the user a list of field names in the
 * currently selected record.
 * 
 * @author Simon Head
 */
public class FieldComboBox extends JComboBox<String> {

    /** Message Log */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    /** Required by Java */
    private static final long serialVersionUID = 1L;
    /** A user-friendly string name for the field. For all fields in this table */
    private String[] elements = null;
    /** An array of field names as the database knows them */
    private String[] names = null;

    /**
     * Populate the ComboBox with the correct fields by loading a class of the
     * selected type.  Makes uses of the overloaded get_row_names and 
     * get_db_names of the class.
     * 
     * @param at Instance of class to populate ComboBox.
     */
    public void getFields(AbstractTable at) {
        HashMap data = at.getSearchData();

        elements = new String[data.size()];
        names = new String[data.size()];

        int i = 0;
        for (Iterator it = data.keySet().iterator(); it.hasNext();) {
            String key = (String) it.next();
            names[i] = key;
            elements[i] = (String) data.get(key);
            ++i;
        }
        
        addItem("Find all");

        for (String row : elements) {
            addItem(row);
        }
    }

    /**
     * Get the database name for the currently selected alias.
     * 
     * @return String database name.
     */
    public String get_db_name() {
        return names[getSelectedIndex() - 1];
    }
}