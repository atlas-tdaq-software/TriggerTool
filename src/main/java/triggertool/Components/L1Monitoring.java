package triggertool.Components;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.UserMode;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1Links.L1TM_TTM;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.L1.L1Threshold;
import triggerdb.Entities.SMT.SuperMasterTable;

///Monitoring GUI
import triggerdb.Entities.L1Links.L1TM_TT;
import triggerdb.Entities.L1.L1Menu;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.L1.L1Prescale;
import triggerdb.Entities.L1Links.L1TM_PS;

/**
 * A GUI that displays the monitor information for L1, for the selected 
 * Super Master Table.
 * 
 * @author Simon Head
 */
public class L1Monitoring extends JDialog {

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    /** 
     * Pointer to the Super Master Table.  We edit the monitors for a specific
     * master table, so we need this.
     */
    private SuperMasterTable smt = null;
    private final L1Menu l1menu = null;
    /** 
     * A set of unique thresholds within the L1 Menu.  Since a threshold can
     * appear more than once, we rely on the equals method of L1Threshold
     * to tell us if they're unique.
     */
    private HashSet<L1Threshold> set;

    private final L1MonitoringTableModel MonMainModel;

     /**
     * An integer representing the role of the user (see ConnectionManager).
     */
    private UserMode userMode = UserMode.UNKNOWN;

    /**
     * Creates the L1Monitoring form and displays it.
     * 
     * @param parent The frame that owns this one, can be null.
     * @param smt Super Master Table record which we wish to view / edit.
     */
    public L1Monitoring(java.awt.Frame parent, SuperMasterTable smt) {
        super((JDialog)null);
        initComponents();
        setTitle("L1 Monitors: SMT " +smt);
        this.smt = smt;

        MonMainModel = (L1MonitoringTableModel) tableMonitors.getModel();
        TableSorter sorterL1 = new TableSorter(tableMonitors.getModel());
        tableMonitors.setModel(sorterL1);
        JTableHeader headerL1 = tableMonitors.getTableHeader();
        sorterL1.setTableHeader(headerL1);
        try {
            getThresholds();
            loadTable();


            if (smt.get_l1_master_table().get_menu().getMonitors().isEmpty()) {
                //System.out.println("Having to create the monitors, none found");
                buttonAutoAddActionPerformed(null);
                String msg = "No monitors were found in the DB\nThese monitors have been automatically\ngenerated from the thresholds present.";
                JOptionPane.showMessageDialog(parent, msg, "Note", JOptionPane.INFORMATION_MESSAGE);
            } else {
                //System.out.println("Found some existing monitor triggers");
            }
        } catch (SQLException ex) {
            String message = "Error occurred while creating the L1 Monitoring panel.";
            logger.log(Level.SEVERE, message, ex);
            JOptionPane.showMessageDialog(parent, "SQL Error" , message +
            " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
        }
        
        userMode = ConnectionManager.getInstance().getInitInfo().getUserMode();

        /*if (userMode.ordinal() >= UserMode.SHIFTER.ordinal()) {
            buttonSave.setEnabled(true);
            buttonAutoAdd.setEnabled(true);
            jButtonAddRow.setEnabled(true);
            jButtonRemove.setEnabled(true);
            tableMonitors.setEditable(true);
        }*/
        setLocationRelativeTo(parent);
        setVisible(true);
    }

    /**
     * Load the thresholds by looking at all the L1 items and removing 
     * duplicates.
     */
    private void getThresholds() throws SQLException {

        set = new HashSet<>();

        for (L1Item item : smt.get_l1_master_table().get_menu().getItems()) {
            for (L1Threshold threshold : item.getThresholds()) {
                if (!threshold.get_name().startsWith("RND") && !threshold.get_name().startsWith("BGRP") && !threshold.get_name().startsWith("PCLK")) {
                    set.add(threshold);
                }
            }
        }

        for (L1Threshold threshold : smt.get_l1_master_table().get_menu().getForcedThresholds()) {
            if (!threshold.get_name().startsWith("RND") && !threshold.get_name().startsWith("BGRP") && !threshold.get_name().startsWith("PCLK")) {
                set.add(threshold);
            }
        }
    }

    /**
     * Populate the table with counters loaded from the menu.
     */
    private void loadTable() throws SQLException {
//        DefaultTableModel mod = (DefaultTableModel) tableMonitors.getModel();
        MonMainModel.clearNumRows();
        int rows = MonMainModel.getRowCount();
        List<String> Names = new ArrayList<>();
        for (L1Threshold threshold : set) {
            if(threshold.get_multiplicity() == 1){
                Names.add(threshold.get_name());
            }
        }
        for (L1TM_TTM counter : smt.get_l1_master_table().get_menu().getMonitors()) {
            ArrayList<Object> values = new ArrayList<>();
            values.add(counter.get_counter_name());
            values.add(counter.get_threshold().get_name());
            values.add(counter.get_multiplicity());
            values.add(counter.get_type());
            values.add(counter.get_bunch_group_id());
            values.add(counter.get_internal_id());
            if (values != null) {
                JComboBox<String> cb = new JComboBox<>(Names.toArray(new String[0]));
                DefaultCellEditor ed = new DefaultCellEditor(cb);
                tableMonitors.getRowEditorModel().addEditorForRow(rows, ed, 1);
            } else {
                tableMonitors.getRowEditorModel().removeEditorForRow(rows);
            }
            MonMainModel.add(values);
            rows = MonMainModel.getRowCount();
        }
        MonMainModel.setChanged(0);
    //System.out.println(rows + " Main load");
    }

    /** 
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonSave = new javax.swing.JButton();
        buttonAutoAdd = new javax.swing.JButton();
        jButtonAddRow = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableMonitors = new triggertool.Components.JTableX();
        jButtonRemove = new javax.swing.JButton();
        buttonClose = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(674, 334));

        buttonSave.setText("Save");
        buttonSave.setEnabled(false);
        buttonSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSaveActionPerformed(evt);
            }
        });

        buttonAutoAdd.setText("Add Monitor for each Threshold");
        buttonAutoAdd.setEnabled(false);
        buttonAutoAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAutoAddActionPerformed(evt);
            }
        });

        jButtonAddRow.setText("Add Row");
        jButtonAddRow.setEnabled(false);
        jButtonAddRow.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddRowActionPerformed(evt);
            }
        });

        tableMonitors.setModel(new triggertool.Components.L1MonitoringTableModel());
        tableMonitors.setEditable(false);
        jScrollPane1.setViewportView(tableMonitors);

        jButtonRemove.setText("Remove Row");
        jButtonRemove.setEnabled(false);
        jButtonRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRemoveActionPerformed(evt);
            }
        });

        buttonClose.setText("Close");
        buttonClose.setInheritsPopupMenu(true);
        buttonClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCloseActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(29, 29, 29)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createSequentialGroup()
                                .add(buttonAutoAdd)
                                .add(0, 0, Short.MAX_VALUE))
                            .add(layout.createSequentialGroup()
                                .add(jButtonAddRow)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jButtonRemove)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(buttonSave)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(buttonClose))))
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 647, Short.MAX_VALUE)
                        .add(3, 3, 3)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(26, 26, 26)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 231, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(buttonAutoAdd)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jButtonAddRow)
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jButtonRemove)
                        .add(buttonClose)
                        .add(buttonSave)))
                .addContainerGap())
        );

        layout.linkSize(new java.awt.Component[] {jButtonAddRow, jButtonRemove}, org.jdesktop.layout.GroupLayout.VERTICAL);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Close the window.
     * 
     * @param evt Event that caused this.
     */
private void buttonSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSaveActionPerformed
            try {
                DoSaveAction();
            } catch (SQLException ex) {
                String message = "Error occurred while performing the cancel operation in L1 Monitoring panel.";
                logger.log(Level.SEVERE, message, ex);
                JOptionPane.showMessageDialog(this, "SQL Error" , message +
                " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
            } 
}//GEN-LAST:event_buttonSaveActionPerformed


    /**
     * Add a new monitor for every L1 Threshold in the menu.  First removes any
     * monitors that might exist already.
     * 
     * @param evt Event that caused this. 
     */

    private void DoSaveAction() throws SQLException, NumberFormatException {
        L1Menu menu = smt.get_l1_master_table().get_menu();
        menu.getMonitors().clear();
        
        for (int row = 0; row < MonMainModel.getRowCount(); ++row) {
            L1TM_TTM in = new L1TM_TTM(-1);
            in.set_counter_name((String) MonMainModel.getValueAt(row, 0));
            String ThresholdN = ((String) MonMainModel.getValueAt(row, 1));
            for (L1Threshold threshold : set) {
                if (threshold.get_name().equals(ThresholdN)) {
                    in.set_threshold_id(threshold.get_id());
                }
            }
            int multi = Integer.parseInt(MonMainModel.getValueAt(row, 2).toString());
            in.set_multiplicity(multi);
            in.set_type((String) MonMainModel.getValueAt(row, 3));
            int bg = Integer.parseInt(MonMainModel.getValueAt(row, 4).toString());
            ////System.out.println(bg);
            in.set_bunch_group_id(bg);
            menu.getMonitors().add(in);
        }
        List<L1Prescale> PSKs = menu.getPrescaleSets();
        int menu_id = menu.save();
        for(L1Prescale psk:PSKs){
            L1TM_PS newlink = new L1TM_PS();
            newlink.set_menu_id(menu_id);
            newlink.set_prescale_set_id(psk.get_id());
            //save link
            newlink.save();
        }
        L1Master master = smt.get_l1_master_table();
        master.set_menu_id(menu_id);
        master.set_id(-1);
        int master_id = master.save();
        smt.set_l1_master_table_id(master_id);
        List<HLTRelease> releases = smt.getReleases();
        logger.fine(smt.toString());
        smt.set_origin("evolution of " + smt.get_id());
        smt.set_id(-1);
        smt.save();
        smt.save_releases(releases);
    }

private void buttonAutoAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAutoAddActionPerformed
    try {
        DoButtonAutoAddActionPerformed();
    } catch (SQLException ex) {
        String message = "Error occurred while performing the Auto Add action in the L1 Monitoring panel.";
        logger.log(Level.SEVERE, message, ex);
        JOptionPane.showMessageDialog(this, "SQL Error" , message +
        " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
    }
}

    private void DoButtonAutoAddActionPerformed() throws SQLException {
        int Confirm = 0;
        if (!smt.get_l1_master_table().get_menu().getMonitors().isEmpty()) {
            Object[] options = {"Yes, please", "No, thanks"};
            
            Confirm = JOptionPane.showOptionDialog(null,
                    "This will delete all Monitors and recreate them. \nDo you want to do this?",
                    "Are you sure?",
                    JOptionPane.YES_NO_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    options,
                    options[1]);
        }   
        
        if (Confirm == 0) {
                int MBTSA = -1;
                int MBTSC = -1;

                int ConfirmCombo = 0;
                Object[] options = {"Yes, please", "No, thanks"};
                JOptionPane.showOptionDialog(null,
                        "Accepting this will add the MBTS A/C combination monitors to the"+
                                "\nlist of automatically created counters"+
                                " \nDo you want to do this?",
                        "MBTS A/C Logical Combination",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        options,
                        options[1]);

                smt.get_l1_master_table().get_menu().getMonitors().clear();
                for (L1Threshold threshold : set) {
                    ////////System.out.println(threshold.get_name());
                    L1TM_TTM in = new L1TM_TTM(-1);
                    in.set_counter_name(threshold.get_multiplicity()+threshold.get_name());
                    in.set_multiplicity(threshold.get_multiplicity());
                    in.set_threshold_id(threshold.get_id());
                    in.set_type("CTPIN");
                    in.set_bunch_group_id(1);
                    smt.get_l1_master_table().get_menu().getMonitors().add(in);
                    if (threshold.get_name().equals("MBTS_A")) {
                        MBTSA = threshold.get_id();
                    }
                    if (threshold.get_name().equals("MBTS_C")) {
                        MBTSC = threshold.get_id();
                    }
                }

                ArrayList<L1Threshold> Thresh = new ArrayList<>();
                for (L1TM_TT link : smt.get_l1_master_table().get_menu().getTMTT()) {
                    L1Threshold threshold = link.get_threshold();
                    Thresh.add(threshold);
                }

                for (L1Threshold threshold : smt.get_l1_master_table().get_menu().getForcedThresholds()) {
                    if (Thresh.contains(threshold)) {
                        Thresh.remove(threshold);
                    }
                }
                for (L1Threshold threshold : Thresh) {
                    L1TM_TTM mon = new L1TM_TTM(-1);
                    mon.set_counter_name(threshold.get_multiplicity()+threshold.get_name());
                    mon.set_multiplicity(threshold.get_multiplicity());
                    mon.set_threshold_id(threshold.get_id());
                    mon.set_type("CTPMON");
                    mon.set_bunch_group_id(1);
                    smt.get_l1_master_table().get_menu().getMonitors().add(mon);
                }
                if (ConfirmCombo == 0 && MBTSA!=-1 && MBTSC!=-1) {
                    for (int i = 0; i < 2; i++) {
                        L1TM_TTM mon = new L1TM_TTM(-1);
                        mon.set_counter_name("MBTS_A_AND_MBTS_C");
                        mon.set_multiplicity(1);
                        if (i == 0) {
                            mon.set_threshold_id(MBTSA);
                        } else {
                            mon.set_threshold_id(MBTSC);
                        }
                        mon.set_type("CTPMON");
                        mon.set_bunch_group_id(1);
                        smt.get_l1_master_table().get_menu().getMonitors().add(mon);
                        L1TM_TTM mon2 = new L1TM_TTM(-1);
                        mon2.set_counter_name("MBTS_A_OR_MBTS_C");
                        mon2.set_multiplicity(1);
                        if (i == 0) {
                            mon2.set_threshold_id(MBTSA);
                        } else {
                            mon2.set_threshold_id(MBTSC);
                        }
                        mon2.set_type("CTPMON");
                        mon2.set_bunch_group_id(1);
                        mon2.set_internal_id(-1);
                        smt.get_l1_master_table().get_menu().getMonitors().add(mon2);
                    }
                }
                //System.out.println("Reloading table...");
            loadTable();
            MonMainModel.setChanged(1);
        }
}//GEN-LAST:event_buttonAutoAddActionPerformed

private void jButtonAddRowActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddRowActionPerformed
    MonMainModel.setChanged(1);
    int rows = MonMainModel.getRowCount();
    List<String> Names = new ArrayList<>();
    for (L1Threshold threshold : set) {
        Names.add(threshold.get_name());
    }
    //////System.out.println(Names);
    JComboBox<String> cb = new JComboBox<>(Names.toArray(new String[0]));
    DefaultCellEditor ed = new DefaultCellEditor(cb);
    tableMonitors.getRowEditorModel().addEditorForRow(rows, ed, 1);
    ArrayList<Object> values = new ArrayList<>();
    values.add("");
    values.add("");
    values.add("1");
    values.add("CTPIN");
    values.add("1");
    values.add("-1");
    MonMainModel.add(values);

//////System.out.println(rows + " Add load");
}//GEN-LAST:event_jButtonAddRowActionPerformed

private void jButtonRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemoveActionPerformed
    int row[] = tableMonitors.getSelectedRows();
    //////System.out.println(tableMonitors.getModel().getRowCount());
    //int rows = tableMonitors.getSelectedRowCount();
    TableModel model = tableMonitors.getModel();
    MonMainModel.setSort(1);
    for (int r : row) {
        //////System.out.println(r);
        model.setValueAt(false, r, 0);
    }
    //////System.out.println("And there should be " + rows + " of them");
    MonMainModel.remove();
    MonMainModel.fireTableDataChanged();
    MonMainModel.setSort(0);
    MonMainModel.setChanged(1);
}//GEN-LAST:event_jButtonRemoveActionPerformed

    private void buttonCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCloseActionPerformed

        if (MonMainModel.getChanged() == 0) {
        dispose();
    } else if (MonMainModel.getChanged() != 0) {
        int Confirm;
        Object[] options = {"Yes", "No"};

        Confirm = JOptionPane.showOptionDialog(null,
                "There there are unsaved changes, are you sure you want to quit?",
                "Are you sure?",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[1]);
        if (Confirm == 0) {
            dispose();
        }
    }
    }//GEN-LAST:event_buttonCloseActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonAutoAdd;
    private javax.swing.JButton buttonClose;
    private javax.swing.JButton buttonSave;
    private javax.swing.JButton jButtonAddRow;
    private javax.swing.JButton jButtonRemove;
    private javax.swing.JScrollPane jScrollPane1;
    private triggertool.Components.JTableX tableMonitors;
    // End of variables declaration//GEN-END:variables
}
