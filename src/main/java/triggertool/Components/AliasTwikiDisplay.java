/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggertool.Components;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import static java.lang.Math.round;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import triggerdb.Connections.ConnectionManager;

/**
 *
 * @author William
 */
public class AliasTwikiDisplay {

    String httpDb = "run2";
    String dbname = ConnectionManager.getInstance().getInitInfo().getUserName();
    private final JTextArea theText = new JTextArea();
    String aliasDescription;
    
    public AliasTwikiDisplay(String descriptor) {
        if (dbname.equals("ATLAS_CONF_TRIGGER_REPR_W")) {
            httpDb = "reprocessing";
        }
        aliasDescription = descriptor;
    }

    public void drawDisplay(ArrayList<ArrayList<Object>> savedPSKs) {
        String results = "| *Release* | *SMK* | *BGK* |\n";
        String temp = "| " + savedPSKs.get(0).get(0) + " | " + savedPSKs.get(0).get(1) + " | REPLACEME |\n\n";
        results += temp;
        results += "|  *SMK* | *L1PSK* | *HLTPSK* | *description* | *comment* |\n";
        String jiraResults = results;
        for (ArrayList line : savedPSKs) {
            String temp1 = "| [[https://atlas-trigconf.cern.ch/" + httpDb + "/smkey/" + line.get(1) + "/l1key/" + line.get(2) + "/hltkey/" + line.get(3) + "/][" + line.get(1) + "]] | " + line.get(2) + " | " + line.get(3) + " | " + line.get(4).toString() + " < lumi &le; " + line.get(5).toString() + " | |\n";
            results += temp1;
            String temp2 = "| [" + line.get(1) + "|https://atlas-trigconf.cern.ch/" + httpDb + "/smkey/" + line.get(1) + "/l1key/" + line.get(2) + "/hltkey/" + line.get(3) + "/] | " + line.get(2) + " | " + line.get(3) + " | " + line.get(4).toString() + " < lumi &le; " + line.get(5).toString() + " | |\n";
            jiraResults += temp2;
        }

        JPanel contents = new JPanel();
        contents.setBorder(new EmptyBorder(5, 5, 5, 5));
        contents.setLayout(new BorderLayout(0, 0));

        JPanel p1 = new JPanel();

        theText.setText(results);
        p1.add(theText);

        contents.add(p1, BorderLayout.CENTER);

        //need these variables to be final so they work in the button action
        final String jiraText = jiraResults;
        final String twikiText = results;

        //create button to allow change text to JIRA-formatted text
        JButton buttonNext = new JButton("Show JIRA text");
        buttonNext.addActionListener((ActionEvent ae) -> {
            theText.setText(jiraText);
        });

        //create button to allow change back to twiki-formatted text
        JButton buttonPrev = new JButton("Show Twiki text");
        buttonPrev.addActionListener((ActionEvent ae) -> {
            theText.setText(twikiText);
        });

        JPanel p2 = new JPanel();
        p2.add(buttonNext);
        p2.add(buttonPrev);
        contents.add(p2, BorderLayout.SOUTH); //put the buttons below the text area
        JOptionPane.showMessageDialog(null, contents, "Alias Prescale Keys for " + aliasDescription, JOptionPane.INFORMATION_MESSAGE);

    }
}
