/*
 * Filter for Browser dialogs to select xml files.
 */
package triggertool.Components;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author martyniu
 */
final class XMLExtensionFilter extends FileFilter {

    public XMLExtensionFilter() {
        }

    @Override
    public boolean accept(File f) {
        if (f == null) {
            return false;
        }
        if (f.isDirectory()) {
            return true;
        }
            return f.getName().endsWith(".xml");
        }

    @Override
        public String getDescription() {
        return "XML files";
    }
};
