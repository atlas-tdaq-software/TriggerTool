package triggertool.Components;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.L1.L1CtpSmx;

/**
 *
 * @author Alex Martyniuk
 */

public class SVFIUpload extends JDialog {

    private final JProgressBar progress;
    private final JTextArea info;
    private final JScrollPane scroll;
    private final JButton done;

    private final SVFIUploadWorker worker;

    /**
     *
     * @param parent
     * @param modal
     * @param VHDLslot7
     * @param VHDLslot8
     * @param VHDLslot9
     * @param SVFIslot7
     * @param SVFIslot8
     * @param SVFIslot9
     * @param smxo
     */
    public SVFIUpload( JDialog parent, boolean modal,
                       final String VHDLslot7, final String VHDLslot8, final String VHDLslot9,
                       final String SVFIslot7, final String SVFIslot8, final String SVFIslot9,
                       final String smxo)
    {
        super((JDialog)parent);
        setModal(modal);
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));

        progress = new JProgressBar();
        panel.add(progress);

        info = new JTextArea("", 80, 20);
        info.setPreferredSize(new Dimension(500, 600));
        info.setLineWrap(true);

        scroll = new JScrollPane(info);
        scroll.setPreferredSize(new Dimension(400, 400));
        scroll.setMinimumSize(new Dimension(400, 400));
        scroll.setMaximumSize(new Dimension(600, 800));
        panel.add(scroll);

        done = new JButton("Close");
        done.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
            }
        });
        panel.add(done);

        add(panel);

        pack();

        worker = new SVFIUploadWorker(VHDLslot7, VHDLslot8, VHDLslot9, 
                                      SVFIslot7, SVFIslot8, SVFIslot9, smxo);

    }

    /**
     *
     */
    public void execute() {
        worker.execute();
    }


    final class SVFIUploadWorker extends SwingWorker<Void,String> {

        private final String fileNameSMXO;
        private final String fileNameSVFISlot7;
        private final String fileNameSVFISlot8;
        private final String fileNameSVFISlot9;
        private final String fileNameVHDLSlot7;
        private final String fileNameVHDLSlot8;
        private final String fileNameVHDLSlot9;

        private String contentSVFISlot7 = null;
        private String contentSVFISlot8 = null;
        private String contentSVFISlot9 = null;
        private String contentVHDLSlot7 = null;
        private String contentVHDLSlot8 = null;
        private String contentVHDLSlot9 = null;

        private String vhdl7temp = null;
        private String vhdl8temp = null;
        private String vhdl9temp = null;
        private String smxo = null;
        private String SMXOtemp = null;
        HashMap<Integer, Integer> Conn7 = new HashMap<>();
        HashMap<Integer, Integer> Conn8 = new HashMap<>();
        HashMap<Integer, Integer> Conn9 = new HashMap<>();
        Vector<Integer> SMX_IDs = new Vector<>();
        int[] Selected = new int[3];
    
        private int uploadedSmxID;

    
        public SVFIUploadWorker(final String VHDLslot7, final String VHDLslot8, final String VHDLslot9,
                                final String SVFIslot7, final String SVFIslot8, final String SVFIslot9,
                                final String smxo)
        {
            this.uploadedSmxID = 0;
            this.fileNameVHDLSlot7 = VHDLslot7;
            this.fileNameVHDLSlot8 = VHDLslot8;
            this.fileNameVHDLSlot9 = VHDLslot9;
            this.fileNameSVFISlot7 = SVFIslot7;
            this.fileNameSVFISlot8 = SVFIslot8;
            this.fileNameSVFISlot9 = SVFIslot9;
            this.fileNameSMXO = smxo;
        }

        /**
         * @return the uploadedSmxID
         */
        public int getUploadedSmxID() {
            return uploadedSmxID;
        }

        @Override
        protected Void doInBackground() throws Exception {

            LoadFiles();
            L1CtpSmx ctpsmx = new L1CtpSmx();
            ctpsmx.set_name("Filled");
            ctpsmx.set_vhdl_slot7_fromfile(fileNameVHDLSlot7);
            ctpsmx.set_vhdl_slot8_fromfile(fileNameVHDLSlot8);
            ctpsmx.set_vhdl_slot9_fromfile(fileNameVHDLSlot9);
            ctpsmx.set_svfi_slot7_fromfile(fileNameSVFISlot7);
            ctpsmx.set_svfi_slot8_fromfile(fileNameSVFISlot8);
            ctpsmx.set_svfi_slot9_fromfile(fileNameSVFISlot9);
            ctpsmx.set_output_fromfile(fileNameSMXO);
        
            try {
                checkUserCodeConsistency();
            } catch (FileConsistencyException ex) {
                Logger.getLogger(SVFIUpload.class.getName()).log(Level.SEVERE, "Files inconsistent, can't upload", ex);
                publish("File usercode consistency can't be checked. Will not upload.");
                publish(ex.getMessage());
                publish("Will not upload.");
                uploadedSmxID = -1;
                return null;
            }

            try {
                uploadedSmxID = ctpsmx.save();
            }
            catch (SQLException e1) {
                e1.printStackTrace();
            }

            publish("SMXO, VHDL and SVFI files uploaded to L1_CTP_SMX table with ID " + uploadedSmxID + "\nDone !");
        
            return null;
        }
    
    
        @Override
        protected void done() {
            super.done(); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        protected void process(List<String> chunks) {
            super.process(chunks); //To change body of generated methods, choose Tools | Templates.
            for(String msg : chunks) {
                info.append(msg+"\n");
            }
        }

        private void checkUserCodeConsistency() throws FileConsistencyException {
            if (contentSVFISlot7 == null || contentSVFISlot8 == null || contentSVFISlot9 == null){
                throw new FileConsistencyException("SVFI slots are not specified");
            }
            String ucSVFI7 = getUserCodeSVFI(contentSVFISlot7);
            String ucSVFI8 = getUserCodeSVFI(contentSVFISlot8);
            String ucSVFI9 = getUserCodeSVFI(contentSVFISlot9);
            publish("SVFI7 usercode " + ucSVFI7);
            publish("SVFI8 usercode " + ucSVFI8);
            publish("SVFI9 usercode " + ucSVFI9);
            
            /*        if( !ucSVFI8.equals(ucSVFI7) || !ucSVFI9.equals(ucSVFI7) ) {
                      throw new FileConsistencyException("User codes of SVFI files are inconsistent");
                      }*/
        
            if (contentVHDLSlot7 == null || contentVHDLSlot8 == null || contentVHDLSlot9 == null){
                throw new FileConsistencyException("VHDL slots are not specified");
            }
            String ucVHDL7 = getUserCodeVHDL(contentVHDLSlot7);
            String ucVHDL8 = getUserCodeVHDL(contentVHDLSlot8);
            String ucVHDL9 = getUserCodeVHDL(contentVHDLSlot9);
            publish("VHDL7 usercode " + ucVHDL7);
            publish("VHDL8 usercode " + ucVHDL8);
            publish("VHDL9 usercode " + ucVHDL9);

            /*if( !ucVHDL8.equals(ucVHDL7) || !ucVHDL9.equals(ucVHDL7)) {
              throw new FileConsistencyException("User codes of VHDL files are inconsistent");
              }*/
            if( !ucVHDL7.equals(ucSVFI7) || !ucVHDL8.equals(ucSVFI8) || !ucVHDL9.equals(ucSVFI9) ) {
                throw new FileConsistencyException("User code between SVFI and VHDL files is inconsistent");
            }
        }
    
        void LoadFiles() {
            publish("Start loading files into memory");
            contentVHDLSlot7 = (String) getContents( new File(fileNameVHDLSlot7) );
            publish("File for VHDL7 loaded with size " + contentVHDLSlot7.length());
            contentVHDLSlot8 = (String) getContents( new File(fileNameVHDLSlot8) );
            publish("File for VHDL8 loaded with size " + contentVHDLSlot8.length());
            contentVHDLSlot9 = (String) getContents( new File(fileNameVHDLSlot9) );
            publish("File for VHDL9 loaded with size " + contentVHDLSlot9.length());
            contentSVFISlot7 = (String) getContents( new File(fileNameSVFISlot7) );
            publish("File for SVFI7 loaded with size " + contentSVFISlot7.length());
            contentSVFISlot8 = (String) getContents( new File(fileNameSVFISlot8) );
            publish("File for SVFI8 loaded with size " + contentSVFISlot8.length());
            contentSVFISlot9 = (String) getContents( new File(fileNameSVFISlot9) );
            publish("File for SVFI9 loaded with size " + contentSVFISlot9.length());
            smxo  = (String) getContents( new File(fileNameSMXO) );
            publish("File for SMXO loaded with size " + smxo.length());
            // load the smx table ids 
            publish("==> files loaded");
            getl1SMX_ID();
        }


        private boolean MatchSVFI(final String svfiContent, final int slot) {

            publish("Finding usercode for slot" + slot + " SVFI file");

       
            final String SVFIusercode = getUserCodeSVFI(svfiContent);

            for (int i = 0; i < this.SMX_IDs.size(); ++i) {

                int l1SMX_ID = this.SMX_IDs.elementAt(i);
            
                String VHDL = getVHDL("L1SMX_VHDL_SLOT" + slot, l1SMX_ID);

                if (VHDL != null) {
                
                    String VHDLusercode = getUserCodeVHDL(VHDL);
                    publish("The VHDL usercode is " + VHDLusercode);
                
                    if (SVFIusercode != null && VHDLusercode != null && SVFIusercode.equals(VHDLusercode) ) {
                        publish("Slot" + slot + " SVFI matched to VHDL");
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    publish("VHDL for SMX_ID " + l1SMX_ID + " is null.");
                }
            }
            return false;
        }

        private boolean MatchVHDL(String VHDL, int slot) {

            String VHDLusercode = getUserCodeVHDL(VHDL);
            String VHDLtemp = null;

            for (int l1SMX_ID : this.SMX_IDs) {
                switch (slot) {
                    case 7:
                        getVHDL("L1SMX_VHDL_SLOT7", l1SMX_ID);
                        VHDLtemp = vhdl7temp;
                        break;
                    case 8:
                        getVHDL("L1SMX_VHDL_SLOT8", l1SMX_ID);
                        VHDLtemp = vhdl8temp;
                        break;
                    case 9:
                        getVHDL("L1SMX_VHDL_SLOT9", l1SMX_ID);
                        VHDLtemp = vhdl9temp;
                        break;
                    default:
                        break;
                }
                String VHDLTempUsercode = getUserCodeVHDL(VHDLtemp);

                if (VHDLusercode.equals(VHDLTempUsercode)) {
                    publish("The VHDL file you are trying to upload to slot " + slot + "\nis already in the DB in SMX_ID " + l1SMX_ID);
                    return true;
                }
            }
            return false;
        }

        /**
         * Load the table IDs in reverse order
         */
        private void getl1SMX_ID() {
            SMX_IDs.clear();
            try {
                String query = "SELECT L1SMX_ID FROM L1_CTP_SMX ORDER BY L1SMX_ID DESC";
                query = ConnectionManager.getInstance().fix_schema_name(query);
                try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query); ResultSet rset = ps.executeQuery()) {
                    while (rset.next()) {
                        this.SMX_IDs.addElement(rset.getInt("L1SMX_ID"));
                    }
                    rset.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            publish("Found " + SMX_IDs.size() + " SMX entries in the database");
        }

        private String getVHDL(String Name, int l1SMX_ID) {

            String query = "SELECT " + Name + " FROM L1_CTP_SMX WHERE L1SMX_ID=?";
            query = ConnectionManager.getInstance().fix_schema_name(query);

            String result = null;
        
            try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
                ps.setInt(1, l1SMX_ID);
                try (ResultSet rset = ps.executeQuery()) {
                    while (rset.next()) {
                        result = (String) get(rset, Name);
                        if (Name.endsWith("7")) {
                            vhdl7temp = result;
                        } else if (Name.endsWith("8")) {
                            vhdl8temp = result;
                        } else if (Name.endsWith("9")) {
                            vhdl9temp = result;
                        }
                    }
                    rset.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            return result;
        }

        /**
         * Check content of SVFI
         * @param Name of DB field
         * @param l1SMX_ID table row index
         * @return true if svfi for slot X exists
         */
        private boolean checkField(String Name, int l1SMX_ID) {

            String query = "SELECT " + Name + " FROM L1_CTP_SMX WHERE L1SMX_ID=?";
            query = ConnectionManager.getInstance().fix_schema_name(query);

            try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
                ps.setInt(1, l1SMX_ID);
                try (ResultSet rset = ps.executeQuery()) {
                    if (rset.next()) {
                        return ( get(rset,Name) != null );
                    }
                    rset.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            return true;
        }

        private Object get(final ResultSet rs, final String name) throws SQLException {
            Reader reader = rs.getCharacterStream(name);
            if (reader == null) {
                return null;
            }
            StringBuilder sb = new StringBuilder(5000);
            try {
                char[] charbuf = new char[4096];
                for (int i = reader.read(charbuf); i > 0; i = reader.read(charbuf)) {
                    sb.append(charbuf, 0, i);
                }
            } catch (IOException e) {
                throw new SQLException(e.getMessage());
            }
            return sb.toString();
        }

        private boolean MatchSMXO(String SMXO) {
            Pattern regSMXOslot = Pattern.compile("(?:\'w+\'s\'w+)(\\d)");
            Pattern regSMXO = Pattern.compile("(?:\\S)(?:\\s+)(\\d+)(?:\\s+)(\\d+)");
            String SMXOslot = "";
            int startM = 0;
            if (SMXO != null) {
                publish("HELLO");
                //System.out.println(SMXO.length());
                while (startM < SMXO.length() - 11) {
                    //System.out.println(startM);
                    startM = SMXO.indexOf("\n", startM + 1) + 1;
                    //System.out.println(startM);
                    int endM = SMXO.indexOf("\n", startM + 1);
                    //System.out.println(endM);
                    String lineSMXO = SMXO.substring(startM, endM);
                    //System.out.println(lineSMXO);
                    if (lineSMXO.contains("SMX SLOT")) {
                        //System.out.println(lineSMXO);
                        SMXOslot = lineSMXO.replace("SMX SLOT", "");
                        //System.out.println(SMXOslot);
                        //                    Matcher SMXOSlotMatch = regSMXOslot.matcher(lineSMXO);
                        //                    boolean MatchSMXOslot = SMXOSlotMatch.matches();
                        //                    if (MatchSMXOslot) {
                        //                        String SlotTemp = SMXOSlotMatch.toMatchResult().group(1);
                        //                        SMXOslot = SlotTemp;
                        //                        //System.out.println(SMXOslot);
                        //                        info.append("The SMXO slot is " + SMXOslot + "\n");
                        //                    }
                    }
                    if (lineSMXO.startsWith("-") && SMXOslot.equals("7")) {
                        //System.out.println(lineSMXO);
                        Matcher SMXOMatch = regSMXO.matcher(lineSMXO);
                        boolean Matchsmxo = SMXOMatch.matches();
                        if (Matchsmxo) {
                            Integer cable = Integer.valueOf(SMXOMatch.toMatchResult().group(1));
                            Integer conn = Integer.valueOf(SMXOMatch.toMatchResult().group(2));
                            Conn7.put(cable, conn);
                        }
                    }
                    if (lineSMXO.startsWith("-") && SMXOslot.equals("8")) {
                        //System.out.println(lineSMXO);
                        Matcher SMXOMatch = regSMXO.matcher(lineSMXO);
                        boolean Matchsmxo = SMXOMatch.matches();
                        if (Matchsmxo) {
                            Integer cable = Integer.valueOf(SMXOMatch.toMatchResult().group(1));
                            Integer conn = Integer.valueOf(SMXOMatch.toMatchResult().group(2));
                            Conn8.put(cable, conn);
                        }
                    }
                    if (lineSMXO.startsWith("-") && SMXOslot.equals("9")) {
                        //System.out.println(lineSMXO);
                        Matcher SMXOMatch = regSMXO.matcher(lineSMXO);
                        boolean Matchsmxo = SMXOMatch.matches();
                        if (Matchsmxo) {
                            Integer cable = Integer.valueOf(SMXOMatch.toMatchResult().group(1));
                            Integer conn = Integer.valueOf(SMXOMatch.toMatchResult().group(2));
                            Conn9.put(cable, conn);
                        }
                    }
                }
            }
            //System.out.println(Conn7 + "\n" + Conn8 + "\n" + Conn9 + "\n");
            return false;
        }
    
    
    }

    static String getContents(File aFile) {
        StringBuilder contents = new StringBuilder();
        try (BufferedReader input = new BufferedReader(new FileReader(aFile))) {
            String line;
            while ((line = input.readLine()) != null) {
                contents.append(line);
                contents.append(System.getProperty("line.separator"));
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return contents.toString();
    }

    static String getUserCodeSVFI(final String svfiContent) {
        Pattern regSVFI = Pattern.compile("(?:[\\S\\s]+)(?:\\s+)(?:\\S)(\\S+)(?:\\S\\S)");
        int startM = -1;
        String SVFIusercode = null;      
        while (startM < svfiContent.length() - 50) {
            startM = svfiContent.indexOf("\n", startM + 1) + 1;
            int endM = svfiContent.indexOf("\n", startM + 1);
            String lineSVFI = svfiContent.substring(startM, endM);
            if (lineSVFI.contains("SDR	32	TDI  (")) {
                Matcher SVFIMatch = regSVFI.matcher(lineSVFI);
                boolean MatchSVFI = SVFIMatch.matches();
                if (MatchSVFI) {
                    String UsercodeTemp = SVFIMatch.toMatchResult().group(1);
                    if ( ! UsercodeTemp.contains("FFFFFFF")) {
                        SVFIusercode = UsercodeTemp.toLowerCase();
                        //info.append("The SVFI usercode is " + SVFIusercode + "\n");
                    }
                }
            }
        }
        return SVFIusercode;
    }
   
    static String getUserCodeVHDL(final String vhdlContent) {
        Pattern regVHDL = Pattern.compile("(?:[\\S\\s]+)(?:\\s+)(?:\\S\\S)(\\S+)");
        String VHDLusercode = null;
        int startM = 0;
        if (vhdlContent != null) {
            while (startM < vhdlContent.length() - 50) {
                startM = vhdlContent.indexOf("\n", startM + 1) + 1;
                int endM = vhdlContent.indexOf("\n", startM + 1);
                String lineVHDL = vhdlContent.substring(startM, endM);
                if (lineVHDL.contains("-- User code    : 0x")) {
                    Matcher VHDLMatch = regVHDL.matcher(lineVHDL);
                    boolean MatchVHDL = VHDLMatch.matches();
                    if (MatchVHDL) {
                        String UsercodeTemp = VHDLMatch.toMatchResult().group(1);
                        VHDLusercode = UsercodeTemp;
                        //info.append("The VHDL usercode is " + VHDLusercode + "\n");
                    }
                }
            }
        }
        return VHDLusercode;
    }

}



