package triggertool.Components;

import java.sql.SQLException;
import triggerdb.Entities.AbstractTable;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;
import triggerdb.Entities.SMT.SuperMasterTable;

/**
 * Class that holds the results from a search in a vector
 * 
 * @author Simon Head
 */
public class ResultsTableModel extends AbstractTableModel {

    /** Serial ID for Java 1.5 */
    private static final long serialVersionUID = 1L;
    /** A vector to hold the records found by the search */
    private ArrayList<AbstractTable> smts = new ArrayList<>();
    private ArrayList<String> cols = new ArrayList<>();

    /**
     * Add a record to the table
     * 
     * @param inp The record to add (casted back to an AbstractTable)
     */
    public void add(AbstractTable inp) {
        if (cols.size() != inp.get_min_names().size()) {
            cols = inp.get_min_names();
            fireTableStructureChanged();
        }
        smts.add(inp);
        fireTableDataChanged();
    }

    /**
     *
     * @param superMasterTable
     * @return
     */
    public boolean contains(SuperMasterTable superMasterTable) {
        for (AbstractTable table : smts) {
            if (table.get_id() == superMasterTable.get_id()) {
                return true;
            }
        }

        return false;
    }

    /** 
     * Get the column count 
     * 
     * @return Number of columns
     */
    @Override
    public int getColumnCount() {
        return cols.size();
    }

    /** 
     * Get the row count 
     * 
     * @return Number of rows
     */
    @Override
    public int getRowCount() {
        return smts.size();
    }

    /**
     * Get the value at a certain position in the table
     * 
     * @param x
     * @param y
     * @return The value if it's available.  Note that an 'error' here is pretty serious.
     */
    @Override
    public Object getValueAt(int x, int y) {
        String re = "";
        try {
            re = smts.get(x).get_min_info().get(y).toString();
        } catch (Exception e) {
            re = "Error";
        }

        return re;
    }

    /** 
     * Return a column name.  Used internally by Java
     * 
     *  @param col the column we want the name for
     *  @return the name of the column at position col
     */
    @Override
    public String getColumnName(int col) {
        return cols.get(col);
    }

    /** 
     * Clear the table, update the headings 
     * 
     * @param inp The table we're switching to
     */
    public void clear(AbstractTable inp) {
        smts.clear();

        cols = inp.get_min_names();

        fireTableStructureChanged();
        fireTableDataChanged();
    }

    /**
     * Remove all data from the table
     */
    public void clear() {
        smts.clear();
        fireTableDataChanged();
    }

    /** 
     * Get the record at this position
     * 
     * @param item The index of the selected record
     * @return The record at row item
     */
    public AbstractTable get(int item) {
        return smts.get(item);
    }

    /** 
     * When we sort the table it's no longer in order 
     * 
     * @param id The id in the database of the selected item
     * @return The record with this id
     */
    public AbstractTable getfromid(int id) {
        for (AbstractTable at : smts) {
            if (at.get_id() == id) {
                return at;
            }
        }
        // we shouldn't get here
        return null;
    }

    /** 
     * We need to force the current record to be reloaded 
     * because we're edited it or something 
     * 
     * @param item The item (row) we want to force a reload for.
     * @throws java.sql.SQLException
     */
    public void reload(int item) throws SQLException {
        smts.get(item).forceLoad();
        fireTableDataChanged();
    }

    /** 
     * Remove a record from this class 
     * 
     * @param item Remove an item from the table and the vector only.  
     * Doesn't remove from database
     */
    public void deleteRecord(int item) {
        smts.remove(item);
        fireTableDataChanged();
    }

    /**
     *
     * @param at
     */
    public void deleteRecord(AbstractTable at) {
        int item = 0;
        for (AbstractTable at_in_list : smts) {
            if (at_in_list.get_id() == at.get_id()) {
                smts.remove(item);
                fireTableDataChanged();
                break;
            }
            ++item;
        }
    }
}
