package triggertool.Components;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import triggerdb.Entities.L1.L1CtpSmx;
import triggerdb.Entities.L1.L1Menu;
import triggerdb.Entities.SMT.SuperMasterTable;
import static triggertool.Components.L1ItemMonitoring.logger;

/**
 *
 * @author amarzin
 */
public class L1DumpSmxFiles extends JDialog{

    protected static final Logger logger = Logger.getLogger("TriggerTool");

    private L1CtpSmx smx = null;
    /* 
     * @param parent The frame that owns this one, can be null.
     * @param smt Super Master Table record which we wish to view
     */
    public L1DumpSmxFiles(java.awt.Frame parent, SuperMasterTable smt) {
        
        super((JDialog)null);
        
        try {
            this.smx = smt.get_l1_master_table().get_menu().get_smx() ;
        } catch (SQLException ex) {
            String message = "Error occurred while getting the switch matrices.";
            logger.log(Level.SEVERE, message, ex);
            JOptionPane.showMessageDialog(parent, "SQL Error" , message +
            " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
        }
        
        System.out.println(smx);
        
        String current_path = Paths.get(".").toAbsolutePath().normalize().toString();
        
         JOptionPane jop = new JOptionPane();
         String path = jop.showInputDialog(parent, "Save SMX files in path: ", current_path);
    
        
        File svfi_slot7 = new File(path + "/ctpin_smx_slot7.svf");
        File svfi_slot8 = new File(path + "/ctpin_smx_slot8.svf");
        File svfi_slot9 = new File(path + "/ctpin_smx_slot9.svf");
        File vhdl_slot7 = new File(path + "/smx_SLOT7.vhd");
        File vhdl_slot8 = new File(path + "/smx_SLOT8.vhd");
        File vhdl_slot9 = new File(path + "/smx_SLOT9.vhd");
        File smxo = new File(path + "/smxo.dat");
        
        FileWriter fw_svfi_slot7;   
        FileWriter fw_svfi_slot8; 
        FileWriter fw_svfi_slot9;         
        FileWriter fw_vhdl_slot7;         
        FileWriter fw_vhdl_slot8; 
        FileWriter fw_vhdl_slot9; 
        FileWriter fw_smxo;      
        try {
            fw_svfi_slot7 = new FileWriter(svfi_slot7);
            fw_svfi_slot8 = new FileWriter(svfi_slot8);
            fw_svfi_slot9 = new FileWriter(svfi_slot9);
            fw_vhdl_slot7 = new FileWriter(vhdl_slot7);
            fw_vhdl_slot8 = new FileWriter(vhdl_slot8);
            fw_vhdl_slot9 = new FileWriter(vhdl_slot9);
            fw_smxo = new FileWriter(smxo);
            
            fw_svfi_slot7.write(smx.get_svfi_slot7());
            fw_svfi_slot8.write(smx.get_svfi_slot8());
            fw_svfi_slot9.write(smx.get_svfi_slot9());            
            fw_vhdl_slot7.write(smx.get_vhdl_slot7());
            fw_vhdl_slot8.write(smx.get_vhdl_slot8());
            fw_vhdl_slot9.write(smx.get_vhdl_slot9());    
            fw_smxo.write(smx.get_output());  
            
            fw_svfi_slot7.close();
            fw_svfi_slot8.close();
            fw_svfi_slot9.close();
            fw_vhdl_slot7.close();
            fw_vhdl_slot8.close();
            fw_vhdl_slot9.close();
            fw_smxo.close();

            System.out.println("SMX files saved in " + path);
        } catch (IOException e) {
          e.printStackTrace();
        }        
    }
}
