package triggertool.Components;

import triggerdb.Entities.AbstractTable;
import java.util.Iterator;
import java.util.TreeMap;
import javax.swing.table.AbstractTableModel;

/**
 * Class handles displaying the data from a full record after it has been 
 * clicked in the search results window
 * 
 * @author Simon Head
 */
public class RowTableModel extends AbstractTableModel {

    /** Serial ID */
    private static final long serialVersionUID = 1L;
    /** Column names */
    private String[] columnNames = {"Field", "Value"};
    /** Array for the row names */
    private String[] rowNames;
    /** Array for the row data */
    private Object[] rowData;
    /** The record we're looking at */
    private AbstractTable smt;
    /** Is it loaded or not? */
    private boolean loaded = false;

    /** Initialise */
    public RowTableModel() {
        rowNames = new String[0];
    }

    /** Display all the details from a certain table
     * @param inp */
    public void load(AbstractTable inp) {
        smt = inp;
        loaded = true;

        TreeMap data = smt.getRowData();

        rowNames = new String[data.size()];
        rowData = new Object[data.size()];

        int i = 0;
        for (Iterator it = data.keySet().iterator(); it.hasNext();) {
            String key = (String) it.next();
            rowNames[i] = key;
            rowData[i] = data.get(key);
            ++i;
        }

        fireTableDataChanged();
    }

    /**
     * Get the number of columns 
     * 
     * @return Number of columns
     */
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    /** 
     * Get the number of rows 
     * 
     * @return the number of rows
     */
    @Override
    public int getRowCount() {
        return rowNames.length;
    }

    /** 
     * Get the value at specific co-ordinated. Used internally to
     * draw the table
     * 
     * @param x X for the required value
     * @param y Y for the required value
     * @return Either the data, or empty.  Empty should never been seen
     */
    @Override
    public Object getValueAt(int x, int y) {
        Object theReturn = "";
        if (loaded) {
            if (y == 0) {
                return rowNames[x];
            } else {
                if (rowData[x] != null) {
                    return rowData[x].toString();
                } else {
                    return "empty";
                }
            }
        }
        return theReturn;
    }

    /** 
     * Get the column names for a specific column.
     * 
     * @param col The column in question
     * @return The String value of col
     */
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    /** Clear the table */
    public void clear() {
        rowNames = new String[0];
        loaded = false;
        fireTableDataChanged();
    }
}
