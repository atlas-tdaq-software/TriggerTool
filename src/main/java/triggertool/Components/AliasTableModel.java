package triggertool.Components;

import static java.lang.Math.round;
import java.sql.SQLException;
import triggerdb.PrescaleSetAliasLumi;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.L1.L1Prescale;
import triggertool.TTExceptionHandler;

/**
 * Tablemodel to hold the L1Prescales. Contains functions to link the boolean
 * column to the prescales. Allows for easy turning on or off of prescales. To
 * enable sorting will be decorated by the table sorter after being called.
 *
 * @author Alex Martyniuk
 */
public class AliasTableModel extends AbstractTableModel {

    /**
     * Stores the data that the table is drawn from. The first vector holds the
     * row position information The second vector holds the Object (name of
     * prescale), String(Prescale value), Bool(Prescale On/Off). Can be filled
     * with rows using add and passing a vector< object > with Object, string,
     * Bool in it. Can edit specific positions using setValueAt Can return
     * values stored at any position with getValueAt.
     */
    //private List<AliasRow> dataVector = new ArrayList<AliasRow>();
    private final List<PrescaleSetAliasLumi> dataVector = new ArrayList<>();
    /**
     * COL_L1PS
     */
    public static final int COL_L1PS = 0;
    /**
     * COL_HLTPS
     */
    public static final int COL_HLTPS = 1;
    /**
     * COL_HLTPS
     */
    public static final int COL_COMMENT = 2;
    /**
     * lumi min
     */
    public static final int COL_LMIN = 3;
    /**
     * Lumi max
     */
    public static final int COL_LMAX = 4;
    /**
     * Stores the names of the columns for usage in getColumnName.
     */
    private final String[] columnNames = {
        "L1 PSK",
        "HLT PSK",
        "Comment",
        "Lumi Min",
        "Lumi Max"
    };
    /**
     * Stored the class of the columns for usage in getColumnClass.
     */
    private final Class[] types = new Class[]{
        java.lang.String.class,
        java.lang.String.class,
        java.lang.String.class,
        java.lang.String.class,
        java.lang.String.class
    };
    /**
     * Stores the edit info to be passed to is CellEditable.
     */
    private final boolean[] canEdit = new boolean[]{
        true, true, true, true, true
    };
    private int SortingFlag = 0;

    /**
     * Constructor.
     */
    public AliasTableModel() {
    }

    /**
     * Returns the number of columns.
     *
     * @return number of columns.
     */
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    /**
     * @param col
     * @return 
     * @{@inheritDoc}
     */
    @Override
    public String getColumnName(final int col) {
        return columnNames[col];
    }

    /**
     * Returns the type of column passed to it.
     *
     * @param columnIndex the column number.
     * @return the class of the column.
     */
    @Override
    public Class getColumnClass(final int columnIndex) {
        return types[columnIndex];
    }

    /**
     * Gets whether a cell is editable or not.
     *
     * @param rowIdx the row number.
     * @param colIdx the column number.
     * @return
     * <code>true</code> if cell is editable, else
     * <code>false</code>.
     */
    @Override
    public boolean isCellEditable(final int rowIdx, final int colIdx) {
        if (colIdx < canEdit.length) {
            return canEdit[colIdx];
        } else {
            return false;
        }
    }

    /**
     * Clears the datavector so a new prescale set can be loaded.
     */
    public void clearNumRows() {
        dataVector.clear();
        fireTableDataChanged();
    }

    /**
     * Gets the current size of the table by finding the size of the datavector.
     *
     * @return the number of rows.
     */
    @Override
    public int getRowCount() {
        return dataVector.size();
    }

    /**
     * @param rowIndex
     * @param colIndex
     * @return 
     * @{@inheritDoc}
     */
    @Override
    public Object getValueAt(final int rowIndex, final int colIndex) {
        PrescaleSetAliasLumi lumi = dataVector.get(rowIndex);
        try {
            return get(lumi, colIndex);
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, null, "Error while value changed in ALIASTable model.");
        }
        
        return null;
    }

    private Object get(final PrescaleSetAliasLumi lumi, int colIndex) throws SQLException {
        switch (colIndex) {
            case COL_L1PS:
                return lumi.get_l1_prescale_set_alias().get_prescale_set();
            case COL_HLTPS:
                return lumi.get_hlt_prescale_set_alias().get_prescale_set();
            case COL_COMMENT:
                return lumi.get_comment();
            case COL_LMIN:
                return lumi.get_lum_min();
            case COL_LMAX:
                return lumi.get_lum_max();
            default:
                return null;
        }
    }

    /**
     * Adds a row to the datavector. Then tells the table that the data has been
     * updated so needs to be redrawn.
     *
     * @param row a AliasRow object with data of a row.
     */
    public void add(PrescaleSetAliasLumi row) {
        this.dataVector.add(row);
        fireTableDataChanged();
    }

    /**
     * Adds a row to the datavector at the specified position. Then tells the
     * table that the data has been updated so needs to be redrawn.
     *
     * @param row a AliasRow object with data of a row.
     * @param pos an Integer object for the position to be inserted.
     */
    public void add(final PrescaleSetAliasLumi row, final int pos) {
        int s = this.dataVector.size();
        int max = s - 1;
        if (pos < 0 || pos > max) {
            this.dataVector.add(row);
        } else {
            this.dataVector.add(pos, row);
        }
        fireTableDataChanged();
    }

    /**
     * Allows values on the table to be changed after being initially filled.
     * Implements boolean/integer linking. Tick box on = positive. Tick box off
     * = negative. Updates table after a cell has been changed, so it can be
     * redrawn. Also allows alternative setValueAt to be called if sorting flag
     * is on.
     *
     * @param value the value to set.
     * @param row the row number.
     * @param col the column number.
     */
    @Override
    public void setValueAt(final Object value, final int row, final int col) {
        try {
            this.setElementAt(value, dataVector.get(row), col);
            if (getFlag() == 1) {
                setValueAtSort(value, row, col);
                return;
            }
            this.fireTableDataChanged();
            this.fireTableCellUpdated(row, col);
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, null, "Error while value changed in ALIASTable model.");
        }

    }

    /**
     *
     * @param value
     * @param row
     * @param col
     * @throws SQLException
     */
    public void setValueAtSort(Object value, int row, int col) throws SQLException {
        this.setElementAt(value, dataVector.get(row), col);
    }

    /**
     * Returns the current sorting flag.
     *
     * @return the sorting flag.
     */
    public int getFlag() {
        return this.SortingFlag;
    }

    /**
     * Sets the sorting flag.
     *
     * @param flag the new value of the sorting flag.
     */
    public void setFlag(final int flag) {
        this.SortingFlag = flag;
    }

    /**
     *
     * @param row
     * @return
     */
    public L1Prescale getL1PS(final int row) {
        return (L1Prescale) this.getValueAt(row, AliasRow.COL_L1PS);
    }

    /**
     *
     * @param l1ps
     * @param row
     */
    public void getL1PS(final L1Prescale l1ps, final int row) {
        this.setValueAt(l1ps, row, AliasRow.COL_L1PS);
    }

    /**
     *
     * @param row
     * @return
     */
    public HLTPrescaleSet getHLTPS(final int row) {
        return (HLTPrescaleSet) this.getValueAt(row, AliasRow.COL_HLTPS);
    }

    /**
     *
     * @param hps
     * @param row
     */
    public void setHLTPS(final HLTPrescaleSet hps, final int row) {
        this.setValueAt(hps, row, AliasRow.COL_HLTPS);
    }

    /**
     *
     * @param row
     * @return
     */
    public String getComment(final int row) {
        return this.dataVector.get(row).get_comment();
    }

    /**
     *
     * @param row
     * @return
     */
    public String getLumiMax(int row) {
        return this.dataVector.get(row).get_lum_max();
    }

    /**
     *
     * @param row
     * @return
     */
    public String getLumiMin(int row) {
        return this.dataVector.get(row).get_lum_min();
    }

    /**
     *
     * @param lMin
     * @param row
     */
    public void setLumiMin(final String lMin, int row) {
        this.setValueAt(Double.toString(round(Double.parseDouble(lMin) * 1e30)), row, AliasRow.COL_LMIN);
    }

    /**
     *
     * @param lMax
     * @param row
     */
    public void setLumiMax(final String lMax, int row) {
        this.setValueAt(Double.toString(round(Double.parseDouble(lMax) * 1e30)), row, AliasRow.COL_LMAX);
    }

    /**
     *
     * @param row
     * @return
     */
    public PrescaleSetAliasLumi getPrescaleSetAliasLumi(final int row) {
        return dataVector.get(row);
    }

    /**
     * Removes the row with the given indices and deletes the lumis from DB.
     *
     * @param rows the array with the indices to remove.
     * @throws java.sql.SQLException
     */
    public void removeRows(final int[] rows) throws SQLException {
        ArrayList<PrescaleSetAliasLumi> _rmRows = new ArrayList<>();
        for (int row : rows) {
            _rmRows.add(this.dataVector.get(row));
        }
        for (PrescaleSetAliasLumi row : _rmRows) {
            row.delete(); // Delete from DB
            this.dataVector.remove(row); // Remove from Lumi List.
        }
    }

    /**
     * Method set the data by "column" number.
     *
     * @param obj the value to set
     * @param col the col number
     */
    private void setElementAt(final Object obj, final PrescaleSetAliasLumi row, final int col) throws SQLException {
        switch (col) {
            case COL_L1PS:
                if (obj instanceof L1Prescale) {
                    row.set_l1_prescale_set((L1Prescale) obj);
                } else {
                    int id;
                    if (obj instanceof Integer) {
                        id = (Integer) obj;
                    } else {
                        id = Integer.parseInt(obj.toString());
                    }
                    row.set_l1_prescale_set(new L1Prescale(id));
                }
                break;
            case COL_HLTPS:
                if (obj instanceof HLTPrescaleSet) {
                    row.set_hlt_prescale_set((HLTPrescaleSet) obj);
                } else {
                    int id;
                    if (obj instanceof Integer) {
                        id = (Integer) obj;
                    } else {
                        id = Integer.parseInt(obj.toString());
                    }
                    row.set_hlt_prescale_set(new HLTPrescaleSet(id));
                }
                break;
            case COL_COMMENT:
                row.set_comment(obj.toString());
                break;
            case COL_LMIN:
                row.set_lum_min(obj.toString().trim());
                break;
            case COL_LMAX:
                row.set_lum_max(obj.toString().trim());
                break;
            default:
                break;
        }
    }
}
