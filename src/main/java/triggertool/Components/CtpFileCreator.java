package triggertool.Components;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.L1.L1CtpFiles;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.L1Links.L1Pits;
import triggerdb.Entities.L1Links.L1TM_TT;
import triggerdb.Entities.L1Links.L1TM_TTM;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggertool.XML.L1_DBtoXML;

/**
 * Class that handles the communication between the trigger tool and the
 * trigger menu compiler.
 * 
 * @author Simon Head
 */
public final class CtpFileCreator extends JDialog {

    /** Message Log */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    /** Serial id, not used but required */
    private static final long serialVersionUID = 1L;
    private String tmcxml_path;
    /** Holds the output of the TriggerMenuCompiler.*/
    private String std_out;
    /** L1 Master table to work with */
    private final L1Master master;
    private final SuperMasterTable smt;
    private final JProgressBar progress = new JProgressBar();
    private final JTextArea info;
    private final JScrollPane scroll;

    private final JButton done;

    private final FileCreatorWorker worker;

    /**
     * temporary to choose between direct and wrapped TMC
     *
     * the wrapper allows to change the directory
     */
    private final boolean runWrapper = true;

    /**
     * Create a window and start the trigger menu compiler interface.
     * 
     * @param parent
     * @param modal
     * @param smt
     * @throws java.sql.SQLException
     */
    public CtpFileCreator( JDialog parent, boolean modal,
                           final SuperMasterTable smt ) throws SQLException {
        super((JDialog)parent);
        setModal(modal);
        this.tmcxml_path = "";
        this.std_out = "";
        this.master = smt.get_l1_master_table();
        this.smt = smt;
        
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));

        panel.add(progress);

        info = new JTextArea("", 80, 20);
        info.setPreferredSize(new Dimension(700, 600));
        info.setLineWrap(true);
        scroll = new JScrollPane(info);

        scroll.setPreferredSize(new Dimension(400, 400));
        scroll.setMinimumSize(new Dimension(400, 400));
        scroll.setMaximumSize(new Dimension(600, 800));

        panel.add(scroll);

        done = new JButton("Close");
        done.setEnabled(false);
        done.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
            }
        });
        panel.add(done);

        this.add(panel);
        this.pack();
        
        this.worker = new FileCreatorWorker();
    }

    /**
     *
     */
    public void execute() {
        worker.execute();
    }

    enum SMXResult { NOTLINKED, LINKED, MATCH, NOMATCH, EXECUTIONEXCEPTION };

    final class FileCreatorWorker extends SwingWorker<Void,String> {

        int linkedSMX;
        int linkedCTP;

        public FileCreatorWorker() {
            this.linkedSMX = -1;
            this.linkedCTP = -1;
        }

        /**
         * Process that does all the hard work.  First it looks through the database to see
         * if a compatible switch matrix file already exists.  If it does then we store only
         * the ctp_files (monitor / lut and cam).  If it doesn't then we run the trigger menu
         * compiler to create a new smx and store this in a new record.
         */
        @Override
        protected Void doInBackground() throws Exception {

            logger.info("Starting TMC");

            if (look_for_tmc()==1) {
                String msg = "Trigger Menu Compiler not in path\nCheck-out and compile the package\n"
                    + "Trigger/TrigConfiguration/triggerMenuCompiler";
                JOptionPane.showMessageDialog(null, msg, "Error", JOptionPane.ERROR_MESSAGE);
                publish(msg);
                return null;
            }

            String workdir = getFileDir();

            logger.info("Dumping to XML");
            publish("Writing menu XML file to " + workdir + "/l1menu.xml");
            L1_DBtoXML writer = new L1_DBtoXML();
            writer.writeXML(master.get_id(), -1, workdir + "l1menu.xml", -1, true);

            publish("Checking for an existing smx that matches the menu");

            SMXResult smxResult = SMXResult.EXECUTIONEXCEPTION;
            try {
                smxResult = loop_over_DB_smx();
            }
            catch(SQLException ex) {
                logger.log(Level.SEVERE, "Could not find PITs.", ex);
                publish("ERROR: Could not find PITs: " + ex.getMessage());                
            }

            if (null != smxResult) switch (smxResult) {
                case NOTLINKED:
                    String msg = "This L1 master is not linked to any SMX. Something is very wrong";
                    logger.warning(msg);
                    publish(msg);
                    break;
                case LINKED:
                    publish("Using existing smx file link " + this.linkedSMX);
                    break;
                case NOMATCH:
                    publish("None of the existing smx worked\nNeed to create a new switch matrix file");
                    logger.info("None of the existing smx worked. Creating a new one");
                    logger.info("Calling TMC");
                    publish("Calling Trigger Menu Compiler");
                    call_tmc_write_smx();
                    logger.info("Created a new smxo.dat file. It is in your working directory.");
                    publish("Created a new smxo.dat file. It is in your working directory.");
                    try {
                        logger.info("Removing current pit info");
                        publish("Removing current PITs");
                        remove_current_pits();
                    }
                    catch (SQLException ex) {
                        logger.log(Level.SEVERE, "Failed removing PITs.", ex);
                        publish("ERROR: Failed removing PITs: " + ex.getMessage());
                    }   try {
                        logger.info("Parse std::out for PIT IDs");
                        publish("Finding PITs");
                        parse_xml_pits();
                    }
                    catch (SQLException ex) {
                        logger.log(Level.SEVERE, "Failed parsing and adding PITs.", ex);
                        publish("ERROR: Failed parsing and adding PITs: " + ex.getMessage());
                    }   try {
                        logger.info("Import Mon files");
                        publish("Finding monitors");
                        parse_xml_mon();
                    }
                    catch (SQLException ex) {
                        logger.log(Level.SEVERE, "Failed parsing and adding MON", ex);
                        publish("ERROR: Failed parsing and adding MON:" + ex.getMessage());
                    }   break;
                case MATCH:
                    publish("Use matching SMX Files " + this.linkedSMX + " and CTP Files " + this.linkedCTP);
                    break;
                case EXECUTIONEXCEPTION:
                    publish("Failure in finding SMX");
                    break;
                default:
                    break;
            }
            logger.info("Finished");
            publish("Finished");
            
            return null;
        }

        @Override
        protected void done() {
            super.done(); //To change body of generated methods, choose Tools | Templates.
            done.setEnabled(true);
        }

        @Override
        protected void process(List<String> chunks) {
            super.process(chunks); //To change body of generated methods, choose Tools | Templates.
            for(String msg : chunks) {
                info.append(msg+"\n");
            }
        }

        private String getFileDir() {
            // String workdir = ConnectionManager.getInstance().get_log_path();
            String workdir = CTPFileCreatorDialog.getCurrentWorkdir() + "/";
            return workdir;
        }
    
        /**
         * Find all the L1_CTP_SMX records in the database.  Then loop through them
         * and ask the trigger menu compiler if they are compatible with the l1 XML
         * file that we have dumped
         * 
         * @return true if this SMX file is compatible
         */
        private SMXResult loop_over_DB_smx() throws SQLException {

            publish("Checking for an existing switch matrix for L1 master ID = " + smt.get_l1_master_table().get_menu_id());

            String query = "SELECT L1TM_CTP_SMX_ID FROM L1_TRIGGER_MENU WHERE l1TM_ID=?";
            query = ConnectionManager.getInstance().fix_schema_name(query);
            try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
                ps.setInt(1, smt.get_l1_master_table().get_menu_id());
                try(ResultSet rset = ps.executeQuery()) {
                    while (rset.next()) {
                        linkedSMX = rset.getInt("L1TM_CTP_SMX_ID");
                    }
                    rset.close();
                }
            }
            if (linkedSMX == -1) {
                return SMXResult.NOTLINKED;
            }

            if (linkedSMX != 1) {
                Object[] options = {"Use this SMX", "Find a new match"};
                int Confirm = JOptionPane.showOptionDialog(null,
                                                           "This L1 master table (id=" +  smt.get_l1_master_table().get_menu_id() +
                                                           ") is already linked to an smx file (id="+linkedSMX+")\n" +
                                                           "It is recommend to use this SMX.",
                                                           "Are you sure?",
                                                           JOptionPane.YES_NO_CANCEL_OPTION,
                                                           JOptionPane.QUESTION_MESSAGE,
                                                           null,
                                                           options,
                                                           options[0]);
                if (Confirm == JOptionPane.YES_OPTION) {
                    return SMXResult.LINKED;
                }
            }



            Map<Integer, String> map = readSMXFromDB();
            int numberOfSMX = map.size();

            List<Integer> smxIDs = new ArrayList<>(map.keySet());
            Collections.sort(smxIDs, new Comparator<Integer>() {
                @Override
                public int compare(Integer t, Integer t1) {
                    return t1-t; // descending
                }
            });

            int j = 0;
            for(final Integer id : smxIDs) {
                final String output  = map.get(id);
                logger.log(Level.INFO, "reading smx table. id={0}", id);

                publish("Checking SMX file " + (j + 1) + "/" + numberOfSMX + ". SMX ID = " + id);
                ++j;

                if (call_tmc_read_smx(output)) {
                    publish("Existing switch matrix (ID = " + id + ") found to work.");

                    if (linkedSMX == 1) {
                        this.saveMenu(id);
                        std_out = "";
                        return SMXResult.MATCH;
                    }
                    
                    //add in a section to check if read_and_save_mon_files information has changed.
                    if (didMonitorFilesChange()) {
                        this.resaveMenu("Since the monitoring has changed we should re-save.\nDo you want to re-save?",id);
                        std_out = "";
                        return SMXResult.MATCH;
                    }

                    if (linkedSMX == id) {
                        if(this.resaveMenu("Existing switch matrix is already linked to this menu. Re-save\ncauses the PIT assignment and Monitoring information to be updated.\nDo you want to re-save?",id))
                        {
                            std_out = "";
                            return SMXResult.MATCH;
                        }
                        JOptionPane.showMessageDialog( null, "Testing the next SMX.\nPlease contact TrigConfigSupport if you have problems",
                                "Warning", JOptionPane.WARNING_MESSAGE);
                    } else {
                        if(this.resaveMenu("The switch matrix found does not match the one already linked to this menu.\nDo you want to re-save?",id)){
                            std_out = "";
                            return SMXResult.MATCH;
                        }
                        JOptionPane.showMessageDialog( null, "Testing the next SMX.\nPlease contact TrigConfigSupport if you have problems",
                                "Warning", JOptionPane.WARNING_MESSAGE);
                    }

                }
                //Clearing std_out at end of the loop
                std_out = "";
            }

            // none of the SMXs matched
            return SMXResult.NOMATCH;
        }

        private Map<Integer, String> readSMXFromDB() throws SQLException {
            String query = "SELECT L1SMX_ID, L1SMX_OUTPUT FROM L1_CTP_SMX"
                    + " WHERE length(L1SMX_SVFI_SLOT7)>0"
                    + " ORDER BY L1SMX_ID DESC";
            query = ConnectionManager.getInstance().fix_schema_name(query);

            logger.log(Level.INFO, "L1SMX: {0}", query);

            Map<Integer, String> map2 = new HashMap<>();
            Map<Integer, String> map = new TreeMap<>(map2);
            try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query); ResultSet rset = ps.executeQuery()) {
                while (rset.next()) {
                    Integer id = rset.getInt("L1SMX_ID");
                    Clob out = rset.getClob("L1SMX_OUTPUT");
                    
                    String output = "";
                    
                    if (out != null) {
                        StringBuilder strOut = new StringBuilder(1000);
                        String aux;
                        BufferedReader br = new BufferedReader(out.getCharacterStream());
                        
                        try {
                            while ((aux = br.readLine()) != null) {
                                strOut.append(aux).append("\n");
                            }
                        } catch (IOException e) {
                            logger.warning("problem converting clob to string");
                            e.printStackTrace();
                        }
                        output = strOut.toString();
                    }
                    if (output == null) {
                        output = "";
                    }
                    map.put(id, output);
                }
                rset.close();
            }
            return map;
        }

        /**
         * Dump the output field of the db to a text file, and call the trigger
         * menu compiler
         * 
         * @param output
         * @return true if this smx file works with the menu
         */
        private boolean call_tmc_read_smx(String output) {
            String filedir = getFileDir();
            logger.log(Level.INFO, "Writing smx input file to {0}", filedir);
            try {
                FileWriter fileWriter = new FileWriter(filedir + "smxi.dat");
                try (BufferedWriter buffWriter = new BufferedWriter(fileWriter)) {
                    buffWriter.write(output);
                }
            } catch (IOException e) {
                logger.log(Level.WARNING, "{0}smxi.dat writing failed: {1}", new Object[]{filedir, e});
            }

            logger.info("Calling trigger menu compiler with smxi.dat");
            try {
                Runtime rt = Runtime.getRuntime();

                String oldargs = "-m " + filedir + "l1menu.xml -c " + tmcxml_path + "ctp_ddr.xml"
                    + " -s " + filedir + "smxi.dat"
                    + " -l " + filedir + "lut.dat -k " + filedir + "cam.dat"
                    + " -x mon_sel_ -y mon_dec_ -w smx_" 
                    + " -t " + filedir + "smxo.dat "
                    + " -p 1";

                String newargs = "--dir " + filedir + " -m l1menu.xml -c " + tmcxml_path + "ctp_ddr.xml"
                    + " -s smxi.dat -l lut.dat -k cam.dat -x mon_sel_ -y mon_dec_ -w smx_ -t smxo.dat -p 1";

                String app = runWrapper ? "runTriggerMenuCompilerApp.sh" : "triggerMenuCompilerApp";
                String args = runWrapper ? newargs : oldargs;
            
                logger.log(Level.INFO, "About to run: {0} {1}", new Object[]{app,args});
                Process process = rt.exec(app + " " + args);

                InputStreamReader reader = new InputStreamReader(process.getInputStream());
                InputStreamReader err = new InputStreamReader(process.getErrorStream());

                BufferedReader buf_reader = new BufferedReader(reader);
                BufferedReader buf_reader_err = new BufferedReader(err);

                String line;
                boolean save_std_out=false;
            
                while ((line = buf_reader.readLine()) != null) {
                    //Removed as often too much to screen
                    //logger.info(line);
                    //std_out += (line + '\n');
                    if(!save_std_out && line.indexOf("Output File Summary")>0){
                        save_std_out=true;
                    }
                    //if have passed the summary point start saving 
                    if(save_std_out){    
                        std_out += (line + '\n');
                    }
                
                }

                //this isn't really an error because sometimes this wont work
                //so the output is info and not warning
                logger.info("TMC std::err output follows");
                String error = "";
                while ((line = buf_reader_err.readLine()) != null) {
                    logger.info(line);
                    error += line;
                }

                logger.info("TMC std::err output finished");

                if (error.contains("ERROR")) {
                    return false;
                }

            } catch (IOException e) {
                logger.log(Level.WARNING, "FAILED to exec ''triggerMenuCompilerApp'' {0}", e);
            }
        
            return true;
        }
        
        private boolean didMonitorFilesChange() throws SQLException {
            publish("Now checking if monitoring files in the directory are different from the linked information");
            boolean foundChangedMonFile = false;
            String workdir = getFileDir();
            String msg = "The following CTP mon files have changed after running the TMC";
            L1CtpFiles ctpfiles = master.get_menu().get_files();
            //Compare LUT
            if (!ctpfiles.get_lut().equals(ctpfiles.get_lut_fromfile(workdir + "lut.dat"))) {
                msg += "\nlut.dat";
                foundChangedMonFile = true;
            }
            if (!ctpfiles.get_cam().equals(ctpfiles.get_cam_fromfile(workdir + "cam.dat"))) {
                msg += "\ncam.dat";
                foundChangedMonFile = true;
            }
            if (!ctpfiles.get_mon_sel_slot7().equals(ctpfiles.get_mon_sel_slot7_fromfile(workdir + "mon_sel_SLOT7.dat"))) {
                msg += "\nmon_sel_slot7.dat";
                foundChangedMonFile = true;
            }
            if (!ctpfiles.get_mon_sel_slot8().equals(ctpfiles.get_mon_sel_slot8_fromfile(workdir + "mon_sel_SLOT8.dat"))) {
                msg += "\nmon_sel_slot8.dat";
                foundChangedMonFile = true;
            }
            if (!ctpfiles.get_mon_sel_slot9().equals(ctpfiles.get_mon_sel_slot9_fromfile(workdir + "mon_sel_SLOT9.dat"))) {
                msg += "\nmon_sel_slot9.dat";
                foundChangedMonFile = true;
            }
            if (!ctpfiles.get_mon_sel_ctpmon().equals(ctpfiles.get_mon_sel_ctpmon_fromfile(workdir + "mon_sel_CTPMON.dat"))) {
                msg += "\nmon_sel_CTPMON.dat ";
                foundChangedMonFile = true;
            }
            if (!ctpfiles.get_mon_dec_slot7().equals(ctpfiles.get_mon_dec_slot7_fromfile(workdir + "mon_dec_SLOT7.dat"))) {
                msg += "\nmon_dec_slot7.dat";
                foundChangedMonFile = true;
            }
            if (!ctpfiles.get_mon_dec_slot8().equals(ctpfiles.get_mon_dec_slot8_fromfile(workdir + "mon_dec_SLOT8.dat"))) {
                msg += "\nmon_dec_slot8.dat";
                foundChangedMonFile = true;
            }
            if (!ctpfiles.get_mon_dec_slot9().equals(ctpfiles.get_mon_dec_slot9_fromfile(workdir + "mon_dec_SLOT9.dat"))) {
                msg += "\nmon_dec_slot9.dat";
                foundChangedMonFile = true;
            }
            if (!ctpfiles.get_mon_dec_ctpmon().equals(ctpfiles.get_mon_dec_ctpmon_fromfile(workdir + "mon_dec_CTPMON.dat"))) {
                msg += "\nmon_dec_CTPMON.dat";
                foundChangedMonFile = true;
            }
            if (foundChangedMonFile) {
                JOptionPane.showMessageDialog(null, msg, "CTP Files Changed", JOptionPane.INFORMATION_MESSAGE);
                return true;
            } else {
                publish("Monitoring files match");
                return false;
            }
        }
        
        /**
         * Make sure the TMC is in the path.  Also find the path to ctp_ddr.xml
         * 
         * @return 0 if it's good, 1 if it's bad
         */
        private int look_for_tmc() {

            final String tmcLocation = CTPFileCreatorDialog.findTMCLocation();

            if (tmcLocation.isEmpty()) {
                return 1;
            }

            logger.log(Level.INFO, "triggerMenuCompilerApp found in path{0}", tmcLocation);

            tmcxml_path = tmcLocation.replace("/bin/triggerMenuCompiler", "");
            logger.log(Level.FINER, "1 {0}", tmcxml_path);
            tmcxml_path = tmcxml_path.substring(0, tmcxml_path.lastIndexOf("/"));
            logger.log(Level.FINER, "2 {0}", tmcxml_path);
            tmcxml_path += "/share/data/TriggerMenuCompiler/";

            logger.log(Level.INFO, "ctp_ddr.xml and TriggerMenuResult.dtd are assumed to be here: {0}", tmcxml_path);

            return 0;
        }
        
        /**
         * Delete the current pit assignments from this menu in the db
         */
        private void remove_current_pits() throws SQLException {
            for (L1TM_TT link : master.get_menu().getTMTT()) {
                for (L1Pits pit : link.get_pits()) {
                    pit.delete();
                } 
            }
        }
        
        /**
         * Call the trigger menu compiler in the mode where it'll write out 
         * the files to needed to create a new smx file
         */
        private int call_tmc_write_smx() {
            String filedir = getFileDir();
            try {
                Runtime rt = Runtime.getRuntime();

                String oldargs = "-m " + filedir + "l1menu.xml -c " + tmcxml_path + "ctp_ddr.xml -u "
                    + "-t " + filedir + "smxo.dat" + " -l " + filedir + "lut.dat "
                    + "-k " + filedir + "cam.dat "
                    + "-x mon_sel_ -y mon_dec_ -w smx_ -p 1";

                String newargs = "--dir " + filedir + " -m l1menu.xml -c " + tmcxml_path + "ctp_ddr.xml -u "
                    + "-t smxo.dat -l lut.dat -k cam.dat -x mon_sel_ -y mon_dec_ -w smx_ -p 1";

                String app = runWrapper ? "runTriggerMenuCompilerApp.sh" : "triggerMenuCompilerApp";
                String args = runWrapper ? newargs : oldargs;

                logger.log(Level.INFO, "About to run: {0} {1}", new Object[]{app,args});
                Process process = rt.exec(app + " " + args);

                InputStreamReader reader = new InputStreamReader(process.getInputStream());
                InputStreamReader err = new InputStreamReader(process.getErrorStream());

                BufferedReader buf_reader = new BufferedReader(reader);
                BufferedReader buf_reader_err = new BufferedReader(err);

                String line;
                boolean save_std_out=false;
                //counter was used when we read in all of std_out and tmc was slow
                //int counter=0;
            
                while ((line = buf_reader.readLine()) != null) {
                    if(!save_std_out && line.indexOf("Output File Summary")>0){
                        save_std_out=true;
                    }
                    //if have passed the summary point start saving 
                    if(save_std_out){    
                        std_out += (line + '\n');
                    }
                    //counter++;
                    //if(counter>2500){
                    //    counter=0;
                    //    logger.info("tmc active, please wait");
                    //}
                }
                //no longer grep std_out for pits/monitors so not 
                //important to output this to the screen
                logger.log(Level.FINEST, "\n\n  output of triggerMenuCompilerApp: \n{0}", std_out);

                logger.info("TMC std::err output follows");
                while ((line = buf_reader_err.readLine()) != null) {
                    logger.warning(line);
                }
                logger.info("TMC std::err output finished");
            
            } catch (IOException e) {
                logger.log(Level.WARNING, "FAILED to exec ''riggerMenuCompilerApp\n{0}", e);
            }
            return 0;
        }

        /**
         * Load the smx files from disk in to the database
         * 
         * @param master ID of the L1 Master table
         * @throws java.sql.SQLException
         */
        /*
        private void read_smx_files(L1Master master) throws SQLException {
            String workdir = getFileDir();
            L1CtpSmx ctpsmx = new L1CtpSmx(-1);
            ctpsmx.set_name(master.get_menu().get_name());

            ctpsmx.set_output_fromfile(workdir + "smxo.dat");
            publish("Read smxo.dat");
            ctpsmx.set_vhdl_slot7_fromfile(workdir + "smx_SLOT7.vhd");
            publish("Read smx_SLOT7.vhd");
            ctpsmx.set_vhdl_slot8_fromfile(workdir + "smx_SLOT8.vhd");
            publish("Read smx_SLOT8.vhd");
            ctpsmx.set_vhdl_slot9_fromfile(workdir + "smx_SLOT9.vhd");
            publish("Read smx_SLOT9.vhd");

            linkedSMX = ctpsmx.save();
            publish("Saving files to DB with ID " + linkedSMX);

            logger.log( Level.INFO, "Saved smx_id={0} {1} {2}", 
                        new Object[]{linkedSMX, ctpsmx.get_name(), ctpsmx.get_version()});


            // Update the menu.
            master.get_menu().set_ctp_smx_id(linkedSMX);
            master.get_menu().replace_ctp_links();
            master.get_menu().forceLoad();
        }
        */
        /**
         * Reads the CTP_FILES from disk and saves it to the DB
         * @param master ID of the L1 Master table
         */
        public void read_and_save_mon_files(L1Master master) throws SQLException {
            String filedir = getFileDir();

            publish("Reading monitoring files from " + filedir);
            L1CtpFiles ctpfiles = master.get_menu().get_files();
            ctpfiles.set_name(master.get_menu().get_name());

            logger.log(Level.INFO, "Menu Compiler save path: {0}", filedir);

            ctpfiles.set_lut_fromfile(filedir + "lut.dat");
            logger.info("LUT read. Inporting into L1CtpFiles object");
            publish("Read lut.dat");

            ctpfiles.set_cam_fromfile(filedir + "cam.dat");
            logger.info("CAM read. Importing into L1CtpFiles object");
            publish("Read cam.dat");

            logger.info("Importing mon sel files");
            ctpfiles.set_mon_sel_slot7_fromfile(filedir + "mon_sel_SLOT7.dat");
            publish("Read mon_sel_SLOT7.dat");
            ctpfiles.set_mon_sel_slot8_fromfile(filedir + "mon_sel_SLOT8.dat");
            publish("Read mon_sel_SLOT8.dat");
            ctpfiles.set_mon_sel_slot9_fromfile(filedir + "mon_sel_SLOT9.dat");
            publish("Read mon_sel_SLOT9.dat");
            ctpfiles.set_mon_sel_ctpmon_fromfile(filedir + "mon_sel_CTPMON.dat");
            logger.info("Importing mon dec files");
            publish("Read mon_sel_CTPMON.dat");
            ctpfiles.set_mon_dec_slot7_fromfile(filedir + "mon_dec_SLOT7.dat");
            publish("Read mon_dec_SLOT7.dat");
            ctpfiles.set_mon_dec_slot8_fromfile(filedir + "mon_dec_SLOT8.dat");
            publish("Read mon_dec_SLOT8.dat");
            ctpfiles.set_mon_dec_slot9_fromfile(filedir + "mon_dec_SLOT9.dat");
            publish("Read mon_dec_SLOT9.dat");
            ctpfiles.set_mon_dec_ctpmon_fromfile(filedir + "mon_dec_CTPMON.dat");
            publish("Read mon_dec_CTPMON.dat");

            // Now save
            logger.info("Saving L1CtpFiles");
            linkedCTP = ctpfiles.save();
            publish("Saved these monitoring files with CTP Files ID " + linkedCTP);

            master.get_menu().set_ctp_files_id(linkedCTP);
            master.get_menu().replace_ctp_links();
            publish("Linking menu (id="+master.get_menu().get_id()+") to CTP Files ID = " + linkedCTP);
        }

        /**
         * Parse the trigger menu compiler xml output and store the pit assignments
         * in the database
         */
        private void parse_xml_pits() throws SQLException {

            HashMap<String, Integer> threshold_ids = new HashMap<>();

            for (L1TM_TT link : master.get_menu().getTMTT()) {
                threshold_ids.put(link.get_threshold().get_name(), link.get_id());
            }
        
            try {
                File file = new File("tmc.xml");
            
                //need to copy the dtd file
                try{
                    File from = new File(tmcxml_path+"TriggerMenuResult.dtd");
                    File to = new File("TriggerMenuResult.dtd");
                    Files.copy( from.toPath(), to.toPath() );
                } catch (IOException e) {
                    logger.log(Level.WARNING, "TriggerMenuResult.dtd already found in folder ({0})", e.getMessage());
                }
            
            
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(file);
    
                logger.log(Level.INFO, "About to loop over tmc.xml to find PITS");
                
                //first we loop over the CTPIN 
                //- this was the old method for finding PITs kept as a cross check
                int pit_counter_ctpin=0;
  
                //each CTPIN SLOT is listed under SwitchMatrix
                NodeList smx_nodelst = doc.getElementsByTagName("SwitchMatrix");
                for (int smx = 0; smx < smx_nodelst.getLength(); smx++) {
                    //keep smx (i.e. slot) name
                    Element smx_ele = (Element) smx_nodelst.item(smx);
                    String smx_name = smx_ele.getAttribute("name");
                    //now find the pits for this slot
                    NodeList pit_nodelst = smx_ele.getElementsByTagName("PatternInTime");
                    for (int pit = 0; pit < pit_nodelst.getLength(); pit++) {
                        //get number of current PIT
                        Element pit_ele = (Element) pit_nodelst.item(pit);
                        //Each pit_ele has two signals - differentiated by thr_phase
                        Integer pit_number = 2 * Integer.parseInt(pit_ele.getAttribute("number"));
                        //get signals for current PIT
                        NodeList sig_nodelst = pit_ele.getElementsByTagName("TriggerInputSignal");
                        if(sig_nodelst.getLength()>2) {
                            logger.log(Level.INFO, "Have more than 2 signals for PIT Pair {0}", pit_number/2 );
                        }
                        for (int sig = 0; sig < sig_nodelst.getLength(); sig++) {
                            pit_counter_ctpin++;
                            Element sig_ele = (Element) sig_nodelst.item(sig);
                            String thr_name = sig_ele.getAttribute("name");
                            String thr_bit = sig_ele.getAttribute("bit");
                            Integer thr_phase = Integer.parseInt( sig_ele.getAttribute("phase") );
                            //As above use thr_phase to distinguish the two pits
                            if(thr_phase==1) {
                                pit_number++;
                            }
                            logger.log(Level.FINE, "Input (pit={0} and phase={1}) gives pit number={2}", new Object[]{pit_ele.getAttribute("number"), thr_phase, pit_number});

                            Integer thr_id = threshold_ids.get(thr_name);
                            if (thr_id == null) {
                                thr_id = -1;
                            }

                            logger.log(Level.FINE,"\tPIT {0} IDENTIFIED \t" + "smx name: {1} pit number : {2} (threshold: name: {3} id: {4} bit: {5} phase: {6})", new Object[]{pit_counter_ctpin, smx_name, pit_number, thr_name, thr_id, thr_bit, thr_phase});
                        
                            /*
                            L1Pits my_pit = new L1Pits(-1);
                            my_pit.set_pit_number(pit_number);
                            my_pit.set_threshold_bit(Integer.parseInt(thr_bit));
                            my_pit.set_tm_to_tt_id(thr_id);
                            try {
                                my_pit.save();
                                logger.fine("Pit info saved ok");
                            } catch (SQLException e) {
                                logger.log(Level.WARNING, "Failed to save PIT info\n{0}", e.getMessage());
                                e.printStackTrace();
                            }
                            */
                            
                        }//end loop over signals
                    }//end loop over pits for current slot
                }//end loop over slots
                
                logger.log(Level.FINE,"PIT summary: from CTPIN found {0} PITs, now loop over CTPCORE", pit_counter_ctpin);
                
                //The actual assignment of pits should be done from 
                //this allows us to add the direct inputs
                
                int pit_counter=0;
                
                NodeList core_nodelst = doc.getElementsByTagName("CtpcoreModule");
                for (int core = 0; core < core_nodelst.getLength(); core++) {
                    //now find the pits
                    Element core_ele = (Element) core_nodelst.item(core);
                    NodeList ti_nodelst = core_ele.getElementsByTagName("TriggerInput");
                    for (int ti = 0; ti < ti_nodelst.getLength(); ti++) {
                        pit_counter++;
                        //get number of current PIT
                        Element pit_ele = (Element) ti_nodelst.item(ti);
                        //In this list of pits the number should be correct
                        Integer pit_number = Integer.parseInt(pit_ele.getAttribute("number"));
                        //get signals for current PIT
                        NodeList sig_nodelst = pit_ele.getElementsByTagName("TriggerInputSignal");
                        if(sig_nodelst.getLength()>1) {
                            logger.log(Level.WARNING, "Have more than 1 signal for PIT {0}", pit_counter);
                        }

                        Element sig_ele = (Element) sig_nodelst.item(0);
                        String thr_name = sig_ele.getAttribute("name");
                        String thr_bit = sig_ele.getAttribute("bit");
                        Integer thr_phase = Integer.parseInt( sig_ele.getAttribute("phase") );
                        String thr_type = sig_ele.getAttribute("type");
                        Integer thr_id = threshold_ids.get(thr_name);
                        if (thr_id == null) {
                            thr_id = -1;
                        }
                        //found a PIT, track number compared to number from CTPIN
                        if(thr_type.equals("PIT")){
                            pit_counter_ctpin--;
                        }
                        logger.log(Level.INFO,"\t {0} IDENTIFIED (#{1}) \t" + "tip number : {2} (threshold name: {3} id: {4} bit: {5} phase: {6})", new Object[]{thr_type, pit_counter, pit_number, thr_name, thr_id, thr_bit, thr_phase});
                        //now save TIP (pit+dir)                        
			L1Pits my_pit = new L1Pits(-1);
			my_pit.set_pit_number(pit_number);
			my_pit.set_threshold_bit(Integer.parseInt(thr_bit));
			my_pit.set_tm_to_tt_id(thr_id);
			try {
			    my_pit.save();
			    logger.fine("Pit info saved ok");
			} catch (SQLException e) {
			    logger.log(Level.WARNING, "Failed to save PIT info\n{0}", e.getMessage());
			    e.printStackTrace();
			} 
                    }//end loop over pits
                }//end loop over ctpcore
                logger.log(Level.INFO,"TIP summary: after CTPCORE found {0} PITs/DIRs", pit_counter);
                if(pit_counter_ctpin>0) {
                    logger.log(Level.WARNING,"TIP summary: {0} different in number of pits from CTPCORE and CTPIN", pit_counter_ctpin);
                } else {
                    logger.log(Level.FINE,"TIP summary: no difference in number of pits from CTPCORE and CTPIN");
                }
            } catch (IOException e) {
                logger.severe(e.getMessage());
            } catch (ParserConfigurationException | SAXException e) {
                logger.severe(e.getMessage());
            }

            logger.info("Parse xml for pits completed");
        }
   
        /**
         * Parse the trigger menu compiler xml output and store the mon assignments
         * in the database
         */
        private void parse_xml_mon() throws SQLException {

            Map<String, Integer> INmap = new HashMap<>();
            Map<String, Integer> MONmap = new HashMap<>();

            try {
                File file = new File("tmc.xml");
            
                //need to copy the dtd file
                try{
                    File from = new File(tmcxml_path+"TriggerMenuResult.dtd");
                    File to = new File("TriggerMenuResult.dtd");
                    Files.copy( from.toPath(), to.toPath() );
                } catch (IOException e) {
                    //file should already be there from checking the pits
                    logger.log(Level.INFO, "TriggerMenuResult.dtd already found in folder ({0})", e.getMessage());
                }
            
            
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(file);
    
                logger.log(Level.INFO, "About to loop over tmc.xml to find MON");

                //total counter
                int mon_counter=0;
            
            
            
                //first go through CTPINs
                int SLOT = -1;
                int CON = -1;
                NodeList cim_nodelst = doc.getElementsByTagName("CtpinModule");
                for (int cim = 0; cim < cim_nodelst.getLength(); cim++) {
                    //keep slot name
                    Element cim_ele = (Element) cim_nodelst.item(cim);
                    String cim_slot = cim_ele.getAttribute("slot");
                    SLOT = Integer.parseInt(cim_slot);
                    //now find the cables for this slot
                    NodeList cab_nodelst = cim_ele.getElementsByTagName("Cable");
                    for (int cab = 0; cab < cab_nodelst.getLength(); cab++) {
                        Element cab_ele = (Element) cab_nodelst.item(cab);
                        String cab_name = cab_ele.getAttribute("name");
        
                        if (cab_name.contains("CON0")) {
                            CON = 0;
                        } else if (cab_name.contains("CON1")) {
                            CON = 1;
                        } else if (cab_name.contains("CON2")) {
                            CON = 2;
                        } else if (cab_name.contains("CON3")) {
                            CON = 3;
                        } else {
                            CON = -1;
                            logger.log(Level.WARNING, "Error finding MON cable");
                        }
                
                        //now find the outputs for this cable
                        NodeList out_nodelst = cab_ele.getElementsByTagName("Output");
                        for (int out = 0; out < out_nodelst.getLength(); out++) {
                            //cable counter
                            Element out_ele = (Element) out_nodelst.item(out);
                            String out_num = out_ele.getAttribute("number");
                            int Out_INint = Integer.parseInt(out_num);
                            //threshold name - should be one value
                            NodeList tc_nodelst = out_ele.getElementsByTagName("TriggerCounter");
                            Element tc_ele = (Element) tc_nodelst.item(0);
                            String tc_name = tc_ele.getAttribute("name");
                            String threshold = tc_name;
                            //update total counter
                            mon_counter++;
                            //correct Out_INint for given slot+cable
                            switch (SLOT) {
                                case 7:
                                    switch (CON) {
                                        case 0:
                                            break;
                                        case 1:
                                            Out_INint = Out_INint + 64;
                                            break;
                                        case 2:
                                            Out_INint = Out_INint + 128;
                                            break;
                                        case 3:
                                            Out_INint = Out_INint + 192;
                                            break;
                                        default:
                                            logger.log(Level.WARNING, "CON7 gone wrong");
                                            break;
                                    }
                                    break;
                                case 8:
                                    switch (CON) {
                                        case 0:
                                            Out_INint = Out_INint + 256;
                                            break;
                                        case 1:
                                            Out_INint = Out_INint + 320;
                                            break;
                                        case 2:
                                            Out_INint = Out_INint + 384;
                                            break;
                                        case 3:
                                            Out_INint = Out_INint + 448;
                                            break;
                                        default:
                                            logger.log(Level.WARNING, "CON8 gone wrong");
                                            break;
                                    }
                                    break;
                                case 9:
                                    switch (CON) {
                                        case 0:
                                            Out_INint = Out_INint + 512;
                                            break;
                                        case 1:
                                            Out_INint = Out_INint + 576;
                                            break;
                                        case 2:
                                            Out_INint = Out_INint + 640;
                                            break;
                                        case 3:
                                            Out_INint = Out_INint + 704;
                                            break;
                                        default:
                                            logger.log(Level.WARNING, "CON9 gone wrong");
                                            break;
                                    }
                                    break;
                                default:
                                    logger.log(Level.WARNING, "SLOT selection gone wrong");
                                    break;
                            }
              
                            //save monitor to map
                            INmap.put(threshold, Out_INint);
                        }//end loop over outputs
                    }//end loop over cables for current slot
                }//end loop over slots
            
                //Now loop over the CTPMON
                NodeList mon_nodelst = doc.getElementsByTagName("CtpmonModule");
                for (int mon = 0; mon < mon_nodelst.getLength(); mon++) {
                    Element mon_ele = (Element) mon_nodelst.item(mon);
                    //now find the outputs for the ctpmon
                    NodeList out_nodelst = mon_ele.getElementsByTagName("Output");
                    for (int out = 0; out < out_nodelst.getLength(); out++) {
                        //cable counter
                        Element out_ele = (Element) out_nodelst.item(out);
                        String out_num = out_ele.getAttribute("number");
                        int Out_MONint = Integer.parseInt(out_num);
                        //threshold name - should be one value
                        NodeList tc_nodelst = out_ele.getElementsByTagName("TriggerCounter");
                        Element tc_ele = (Element) tc_nodelst.item(0);
                        String tc_name = tc_ele.getAttribute("name");
                        String threshold = tc_name;
                        //update total counter
                        mon_counter++;
                
                        //save monitor to map
                        MONmap.put(threshold, Out_MONint);
                    }                 
                }
            } catch (IOException e) {
                logger.severe(e.getMessage());
            } catch (ParserConfigurationException | SAXException e) {
                logger.severe(e.getMessage());
            }

            logger.log(Level.INFO,"Monitors summary: CTPIN \n" + "CTPIN size {0}\n{1}" + "\nCTPMON \n" + "CTPMON size {2}\n{3}", new Object[]{INmap.size(), INmap, MONmap.size(), MONmap});
        
            MonOut(INmap, MONmap);
        
            logger.info("parse xml mon done");
        }

        public void MonOut(Map<String, Integer> IN, Map<String, Integer> MON) throws SQLException {
            for (L1TM_TTM link : master.get_menu().getMonitors()) {
                String Combi = link.get_counter_name();
                //System.out.println("Checking " + link.get_type() +" " + Combi);
                if(link.get_type().equals("CTPIN") && IN.get(Combi) != null){
                    int counter = IN.get(Combi);
                    link.set_internal_id(counter);
                    link.replace_TM2TTM_links();
                }else if(link.get_type().equals("CTPMON") && MON.get(Combi) != null){
                    int counter = MON.get(Combi);
                    link.set_internal_id(counter);
                    link.replace_TM2TTM_links();
                } else {
                    //System.out.println("Are we a combined monitor?" + link.get_threshold().get_name());
                    switch (link.get_counter_name()) {
                    case "MBTS_A_AND_MBTS_C":
                        if(MON.get("1MBTS_A AND 1MBTS_C") != null){
                            int counter = MON.get("1MBTS_A AND 1MBTS_C");
                            //System.out.println(counter);
                            link.set_internal_id(counter);
                            link.replace_TM2TTM_links();
                        }  break;
                    case "MBTS_A_OR_MBTS_C":
                        if(MON.get("1MBTS_A OR 1MBTS_C") != null){
                            int counter = MON.get("1MBTS_A OR 1MBTS_C");
                            //System.out.println(counter);
                            link.set_internal_id(counter);
                            link.replace_TM2TTM_links();
                        }   break;
                    default:
                        logger.log(Level.INFO, "Lost a monitor called: {0}", link.get_counter_name());
                        break;
                    }
                }
            }
        }

        /**
         * Write a StringBuilder to a file.
         * @param tmcCheckOutput the StringBuilder to write to file.
         * @param fileName the name of the file. The file will be created (or 
         * overwritten) in the current directory.
         */
        private void writeBufferToFile(final StringBuilder tmcCheckOutput,
                                       final String fileName) {
            FileWriter fWriter = null;
            try {
                fWriter = new FileWriter(fileName);
                try (BufferedWriter out = new BufferedWriter(fWriter)) {
                    out.write(tmcCheckOutput.toString());
                }
            } catch (IOException ex) {
                logger.log(Level.SEVERE, null, ex);
            } finally {
                try {
                    if (fWriter!= null){
                        fWriter.close();
                    }
                } catch (IOException ex) {
                    logger.log(Level.SEVERE, null, ex);
                }
            }

        }
    
        // function to re-save menu with smx
        //Function requested by Michiru to re-save menus with problems.
        private boolean saveMenu(int id) throws SQLException{
            return this.resaveMenu(null,id);
        }

        // function to re-save menu with smx
        //Function requested by Michiru to re-save menus with problems.
        private boolean resaveMenu(final String message, final int id) throws SQLException {
            if (message != null){
                Object[] options = {"Save menu", "Cancel"};

                int Confirm = JOptionPane.showOptionDialog(null, message,
                                                           "Are you sure?",
                                                           JOptionPane.YES_NO_CANCEL_OPTION,
                                                           JOptionPane.QUESTION_MESSAGE,
                                                           null,
                                                           options,
                                                           options[1]);
                if (Confirm == 1) {
                    return false;
                }
            }

            //change the pointer in the menu to this smx id
            publish("Linking menu (id=" + master.get_menu().get_id() + ") to SMX Files ID = " + id);

            master.get_menu().set_ctp_smx_id(id);
            master.get_menu().replace_ctp_links();

            logger.info("Import Mon files");
            publish("Import Mon files");
            read_and_save_mon_files(master);

            //create and upload ctp files
            logger.info("Removing current pit info");
            publish("Removing current pit info");
            remove_current_pits();

            logger.info("Parse xml for PIT IDs");
            publish("Parse xml for PIT IDs");
            parse_xml_pits();

            logger.info("Parse monitoring xml");
            publish("Parse monitoring xml");
            parse_xml_mon();

            return true;
        }
    }
}

