package triggertool.Components;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.table.*;
import java.util.logging.Logger;

/**
 * Tablemodel to hold the L1Prescales. Contains functions to link the boolean column to the prescales.
 * Allows for easy turning on or off of prescales.
 * To enable sorting will be decorated by the table sorter after being called.
 * @author Alex Martyniuk
 */
public class L1MonitoringTableModel extends AbstractTableModel {

    /**
     * Stores the data that the table is drawn from.
     * The first vector holds the row position information
     * The second vector holds the Object (name of prescale), Integer(Prescale value), Bool(Prescale On/Off)
     * Can be filled with rows using add and passing a vector< object > with Object, integer, Bool in it.
     * Can edit specific positions using setValueAt
     * Can return values stored at any position with getValueAt 
     */
    protected List<ArrayList<Object>> dataVector = new ArrayList<>();
    /**
     * Stores the names of the columns for usage in getColumnName
     */
    private final String[] columnNames = {
        "Name", "Threshold", "Multiplicity", "Type", "Bunch Group Id", "Internal Counter"};
    /**
     * Stored the class of the columns for usage in getColumnClass
     */
    private final Class[] types = new Class[]{
        java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class
    };
    /**
     * Stores the edit info to be passed to is CellEditable
     */
    boolean[] canEdit = new boolean[]{
        true, true, true, true, true, false
    };
    private int ChangedFlag = 0;
    private int SortFlag = 0;
    
    /**
     *
     */
    public static final Logger logger = Logger.getLogger("TriggerTool");
    /**
     * Constructor for the tablemodel
     */
    public L1MonitoringTableModel() {
    }

    /**
     * Returns the number of columns
     * @return
     */
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    /**
     * Returns the columns name
     * @param col
     * @return
     */
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    /**
     * Returns the type of column passed to it.
     * @param columnIndex
     * @return
     */
    @Override
    public Class getColumnClass(int columnIndex) {
        return types[columnIndex];
    }

    /**
     * Sets whether a cell is editable or not.
     * @param rowIndex
     * @param columnIndex
     * @return
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return canEdit[columnIndex];
    }

    /**
     * Clears the datavector so a new prescale set can be loaded.
     */
    void clearNumRows() {
        dataVector.clear();
    }

    /**
     * Gets the current size of the table by finding the size of the datavector.
     * @return
     */
    @Override
    public int getRowCount() {
        return dataVector.size();
    }

    /**
     * Gets the value at specified row/col position from the datavector where the table data is stored.
     * @param row
     * @param col
     * @return
     */
    @Override
    public Object getValueAt(int row, int col) {
        List<Object> value = dataVector.get(row);
        return value.get(col);
    }

    /**
     * Adds a row to the datavector. 
     * Then tells the table that the data has been updated so needs to be redrawn.
     * @param L1prescalesdata_v
     */
    public void add(ArrayList<Object> L1prescalesdata_v) {
        dataVector.add(L1prescalesdata_v);
        fireTableDataChanged();
    }

    /**
     *
     */
    public void remove() {
        Iterator<ArrayList<Object>> I = dataVector.iterator();
        while(I.hasNext()){
            ArrayList<Object> next = I.next();
            if(next.get(0).equals(false)){
                I.remove();
            }
        }
    }

    /**
     *
     * @return
     */
    public int getChanged() {
        return ChangedFlag;
    }

    /**
     *
     * @return
     */
    public int getSort() {
        return SortFlag;
    }

    /**
     * Sets the sorting flag.
     * @param Flag
     */
    public void setChanged(int Flag) {
        ChangedFlag = Flag;
    }

    /**
     *
     * @param Flag
     */
    public void setSort(int Flag) {
        SortFlag = Flag;
    }

    /**
     * Allows values on the table to be changed after being initially filled.
     * Impliments boolean/integeter linking. 
     * Tick box on = positive. 
     * Tick box off = negative.
     * Updates table after a cell has been changed, so it can be redrawn.
     * Also allows alternative setvalueat to be called if sorting flag is on.
     * @param value
     * @param row
     * @param col
     */
    @Override
    public void setValueAt(Object value, int row, int col) {
        if (getSort() == 1) {
            setValueAtSort(value, row, col);
            return;
        }
        dataVector.get(row).set(col, value);
        setChanged(1);
        fireTableDataChanged();
    }

    /**
     *
     * @param value
     * @param row
     * @param col
     */
    public void setValueAtSort(Object value, int row, int col) {
        dataVector.get(row).set(col, value);
        setChanged(1);
    }
}
