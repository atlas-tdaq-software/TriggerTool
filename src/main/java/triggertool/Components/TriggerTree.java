package triggertool.Components;

import triggerdb.Entities.AbstractTable;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;

/**
 * Handles the tree view.  Only loads 2 branches ahead of what the user is currently looking at.
 * 
 * @author Simon Head
 *
 */
public class TriggerTree extends JTree implements ActionListener,
        TreeExpansionListener, TreeWillExpandListener, TreeSelectionListener,
        MouseListener {

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    private static final long serialVersionUID = 1L;
    private final DefaultMutableTreeNode top;
    private final RowTableModel rowModel;
    private final JPopupMenu popupTreeMenu = new JPopupMenu();

    private final DefaultTreeModel myTreeModel;
    private DefaultMutableTreeNode node;

    /**
     *
     * @param tableModel
     * @param rModel
     */
    public TriggerTree(RowTableModel tableModel, ResultsTableModel rModel) {
        rowModel = tableModel;
        top = new DefaultMutableTreeNode("");
        myTreeModel = new DefaultTreeModel(top);

        setModel(myTreeModel);

        getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

        addTreeSelectionListener(this);
        addTreeExpansionListener(this);
        addTreeWillExpandListener(this);
        addMouseListener(this);
    }

    /**
     *
     */
    public void clear() {
        myTreeModel.setRoot(top);
        myTreeModel.reload();
        fireTreeExpanded(null);
    }

    /**
     *
     * @param temp
     * @throws SQLException
     */
    public void update(AbstractTable temp) throws SQLException {
        myTreeModel.reload();
        DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode(temp);
        myTreeModel.setRoot(treeNode);
        temp.addToTree(treeNode, 3);
        fireTreeExpanded(null);
    }

    @Override
    public void treeCollapsed(TreeExpansionEvent event) {
    }

    @Override
    public void treeExpanded(TreeExpansionEvent event) {
    }

    @Override
    public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException {
    }

    @Override
    public void treeWillExpand(TreeExpansionEvent event) throws ExpandVetoException{
        DefaultMutableTreeNode nd = (DefaultMutableTreeNode) event.getPath().getLastPathComponent();

        if (nd != null && nd.getUserObject() instanceof AbstractTable) {
            AbstractTable at = (AbstractTable) nd.getUserObject();
            try {
                at.addToTree(nd, 3);
            } catch (SQLException ex) {
                String message = "Error occurred while expanding the tree.";
                logger.log(Level.SEVERE, message, ex);
                JOptionPane.showMessageDialog(this, "SQL Error" , message +
                " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
        node = (DefaultMutableTreeNode) getLastSelectedPathComponent();

        if (node != null && node.getUserObject() instanceof AbstractTable) {
            AbstractTable temp = (AbstractTable) node.getUserObject();
            rowModel.load(temp);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}