package triggertool.Components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

/**
 * TableSorter is a decorator for TableModels; adding sorting functionality to a
 * supplied TableModel. TableSorter does not store or copy the data in its
 * TableModel; instead it maintains a map from the row indexes of the view to
 * the row indexes of the model. As requests are made of the sorter (like
 * getValueAt(row, col)) they are passed to the underlying model after the row
 * numbers have been translated via the internal mapping array. This way, the
 * TableSorter appears to hold another copy of the table with the rows in a
 * different order.
 * <p/>
 * TableSorter registers itself as a listener to the underlying model, just as
 * the JTable itself would. Events recieved from the model are examined,
 * sometimes manipulated (typically widened), and then passed on to the
 * TableSorter's listeners (typically the JTable). If a change to the model has
 * invalidated the order of TableSorter's rows, a note of this is made and the
 * sorter will resort the rows the next time a value is requested.
 * <p/>
 * When the tableHeader property is set, either by using the setTableHeader()
 * method or the two argument constructor, the table header may be used as a
 * complete UI for TableSorter. The default renderer of the tableHeader is
 * decorated with a renderer that indicates the sorting status of each column.
 * In addition, a mouse listener is installed with the following behavior: <ul>
 * <li> Mouse-click: Clears the sorting status of all other columns and advances
 * the sorting status of that column through three values: {NOT_SORTED,
 * ASCENDING, DESCENDING} (then back to NOT_SORTED again). <li>
 * SHIFT-mouse-click: Clears the sorting status of all other columns and cycles
 * the sorting status of the column through the same three values, in the
 * opposite order: {NOT_SORTED, DESCENDING, ASCENDING}. <li> CONTROL-mouse-click
 * and CONTROL-SHIFT-mouse-click: as above except that the changes to the column
 * do not cancel the statuses of columns that are already sorting - giving a way
 * to initiate a compound sort. </ul>
 * <p/>
 * This is a long overdue rewrite of a class of the same name that first
 * appeared in the swing table demos in 1997.
 *
 * @author Philip Milne
 * @author Brendon McLean
 * @author Dan van Enckevort
 * @author Parwinder Sekhon
 * @author Alex Martyniuk
 * @version 2.0 02/27/04
 */
public final class TableSorter extends AbstractTableModel {

    /**
     * the logger.
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    protected TableModel tableModel;

    /**
     *
     */
    public static final int DESCENDING = -1;

    /**
     *
     */
    public static final int NOT_SORTED = 0;

    /**
     *
     */
    public static final int ASCENDING = 1;
    private static Directive EMPTY_DIRECTIVE = new Directive(-1, NOT_SORTED);
    /**
     * Compare luminosities for L1PresacleSetAlias and HLTPrescaleSetAlias.
     */
    public static final Comparator<Object> LUMI_COMPARATOR = new Comparator<Object>() {

        @Override
        public int compare(final Object l1, final Object l2) {
            int comp;

            Float l1f = null;
            Float l2f = null;
            try {
                l1f = Float.parseFloat(l1.toString().trim());
                l2f = Float.parseFloat(l2.toString().trim());

            } catch (NumberFormatException e) {
                logger.log(Level.WARNING," Luminosities are not numbers." + "\n L1 = {0} \n L2 = {1}", new Object[]{l1, l2});
            }

            //If numeric compare number, else use String compare.
            if (l1f != null && l2f != null) {
                comp = l1f.compareTo(l2f);
            }else {
                comp=l1.toString().trim().compareTo(l2.toString().trim());
            }

            return comp;
        }
    };
    /**
     * Comparator to compare L1prescale names as set out in NewPrescale edit.
     * Uses regex to find the CTP ID number of the prescale and sort numerically
     * by that instead of a simplistic string comparison. Returns -1 if o1 < o2
     * Returns 1 if o1 > o2 Returns 0 if they are equal. Called when column 0s
     * header is clicked
     */
    public static final Comparator<Object> PRESCALE_COMPARATOR = new Comparator<Object>() {

        @Override
        public int compare(Object o1, Object o2) {
            Pattern reg = Pattern.compile("(?:\\S+)(?:\\s+)(?:\\S+)(?:\\s+)(\\d+)(?:[\\S\\s]+)");
            int Comp;
            // Stytem.out.println("First column clicked");
            Matcher Matcher1 = reg.matcher(o1.toString());
            boolean Match1 = Matcher1.matches();
            if (Match1 != true) {
                return 0;
            }
            Matcher Matcher2 = reg.matcher(o2.toString());
            boolean Match2 = Matcher2.matches();
            if (Match2 != true) {
                return 0;
            }
            int M1 = Integer.parseInt(Matcher1.toMatchResult().group(1));
            int M2 = Integer.parseInt(Matcher2.toMatchResult().group(1));
            if (M1 < M2) {
                Comp = -1;
            } else if (M1 > M2) {
                Comp = 1;
            } else {
                Comp = 0;
            }
            return Comp;
        }
    };

    /**
     *
     */
    public static final Comparator<Object> MONITORS_COMPARATOR = new Comparator<Object>() {

        @Override
        public int compare(Object o1, Object o2) {
            // Pattern reg =
            // Pattern.compile("(?:[\\S\\s]+)(\\d+)(?:[\\S\\s]+)");
            Pattern reg = Pattern.compile("(\\D+)(\\d+)");
            Matcher Matcher1 = reg.matcher(o1.toString());
            boolean Match1;
            Match1 = Matcher1.find();

            if (Match1 != true) {
            } else {
                int M1 = Integer.parseInt(Matcher1.toMatchResult().group(2));
            }
            Matcher Matcher2 = reg.matcher(o2.toString());
            boolean Match2;
            Match2 = Matcher2.find();

            if (Match2 != true) {
            } else {
                int M2 = Integer.parseInt(Matcher2.toMatchResult().group(2));
            }

            if (Match1 && Match2) {
                int Comp;
                int M1 = Integer.parseInt(Matcher1.toMatchResult().group(2));
                int M2 = Integer.parseInt(Matcher2.toMatchResult().group(2));

                if (Matcher1.toMatchResult().group(1).equals(
                        Matcher2.toMatchResult().group(1))) {
                    if (M1 < M2) {
                        Comp = -1;
                    } else if (M1 > M2) {
                        Comp = 1;
                    } else {
                        Comp = 0;
                    }
                    return Comp;
                }
                return o1.toString().compareTo(o2.toString());
            } else {
                return o1.toString().compareTo(o2.toString());
            }
        }
    };

    /**
     *
     */
    public static final Comparator<Object> REL_COMPARATOR = new Comparator<Object>() {

        @Override
        public int compare(Object o1, Object o2) {
            return o1.toString().compareTo(o2.toString());
        }
    };
    /**
     * Comparator to compare prescale values.. Returns -1 if o1 < o2 Returns 1
     * if o1 > o2 Returns 0 if they are equal. Called when column 1s header is
     * clicked
     */
    public static final Comparator<Object> INTEGER_COMPARATOR = new Comparator<Object>() {

        @Override
        public int compare(Object o1, Object o2) {
            int Comp;
            String O1toS1 = o1.toString();
            if (O1toS1.equals("Error")) {
                return -1;
            }
            int S1toI1 = Integer.parseInt(O1toS1);
            String O2toS2 = o2.toString();
            if (O2toS2.equals("Error")) {
                return -1;
            }
            int S2toI2 = Integer.parseInt(O2toS2);
            if (S1toI1 < S2toI2) {
                Comp = 1;
            } else if (S1toI1 > S2toI2) {
                Comp = -1;
            } else {
                Comp = 0;
            }
            return Comp;
        }
    };
    /**
     * Comparator to compare prescale values.. Returns -1 if o1 < o2 Returns 1
     * if o1 > o2 Returns 0 if they are equal. Called when column 1s header is
     * clicked
     */
    public static final Comparator<Object> FLOAT_COMPARATOR = new Comparator<Object>() {

        @Override
        public int compare(Object o1, Object o2) {
            int Comp = 0;
            int String1 = 0;
            int String2 = 0;
            Float S1toI1 = null;
            Float S2toI2 = null;
            String O1toS1 = o1.toString();
            if (O1toS1.equals("Error")) {
                return -1;
            }
            try {
                S1toI1 = Float.parseFloat(O1toS1);
            } catch (NumberFormatException e) {
                if (O1toS1.equals("na")) {
                    String1 = 1;
                } else {
                    e.printStackTrace();
                }
            }
            String O2toS2 = o2.toString();
            if (O2toS2.equals("Error")) {
                return -1;
            }
            try {
                S2toI2 = Float.parseFloat(O2toS2);
            } catch (NumberFormatException e) {
                if (O2toS2.equals("na")) {
                    String2 = 1;
                } else {
                    e.printStackTrace();
                }
            }
            if (String1 == 0 && String2 == 0) {
                if (S1toI1 < S2toI2) {
                    Comp = 1;
                } else if (S1toI1 > S2toI2) {
                    Comp = -1;
                } else {
                    Comp = 0;
                }
            } else if (String1 == 1 || String2 == 1) {
                if (String1 == 1 && String2 == 1) {
                    Comp = 0;
                } else if (String1 == 1 && String2 == 0) {
                    Comp = 1;
                } else if (String1 == 0 && String2 == 1) {
                    Comp = -1;
                }
            }
            return Comp;
        }
    };

    /**
     *
     */
    public static final Comparator<Object> STRING_COMPARATOR = new Comparator<Object>() {

        @Override
        public int compare(Object o1, Object o2) {
            return o1.toString().compareTo(o2.toString());
        }
    };
    /**
     * Comparator to compare L1prescale on/off booleans. Returns -1 if o1 =
     * false & o2 = true Returns 1 if o1 = true & o2 = false Returns 0 if they
     * are equal. Called when column 0 header is clicked
     */
    public static final Comparator<Object> BOOLEAN_COMPARATOR = new Comparator<Object>() {

        @Override
        public int compare(Object o1, Object o2) {
            int Comp = 0;
            String O1toS1 = o1.toString();
            boolean S1toB1 = Boolean.parseBoolean(O1toS1);
            String O2toS2 = o2.toString();
            boolean S2toB2 = Boolean.parseBoolean(O2toS2);
            if (S1toB1 != S2toB2) {
                if (S1toB1 == true) {
                    Comp = -1;
                } else {
                    Comp = 1;
                }
            } else {
                Comp = 0;
            }
            return Comp;
        }
    };
    
    /**
     * Comparator to compare L1prescale on/off booleans. Returns -1 if o1 =
     * false & o2 = true Returns 1 if o1 = true & o2 = false Returns 0 if they
     * are equal. Called when column 0 header is clicked
     */
    public static final Comparator<Object> HEX_COMPARATOR = new Comparator<Object>() {

        @Override
        public int compare(Object o1, Object o2) {
            int Comp = 0;
            String O1toS1 = o1.toString();
            Integer S1toI1 = Integer.parseInt(O1toS1,16);
            String O2toS2 = o2.toString();
            Integer S2toI2 = Integer.parseInt(O2toS2,16);
            if (!Objects.equals(S1toI1, S2toI2)) {
                if (S1toI1 > S2toI2) {
                    Comp = -1;
                } else if (S1toI1 < S2toI2){
                    Comp = 1;
                }
            } else {
                Comp = 0;
            }
            return Comp;
        }
    };

    
    private Row[] viewToModel;
    private int[] modelToView;
    private JTableHeader tableHeader;
    private MouseListener mouseListener;
    private TableModelListener tableModelListener;
    private final HashMap<Class<Object>, Comparator<Object>> columnComparators = new HashMap<>();
    private final List<Directive> sortingColumns = new ArrayList<>();

    /**
     *
     */
    public TableSorter() {
        this.mouseListener = new MouseHandler();
        this.tableModelListener = new TableModelHandler();
    }

    /**
     *
     * @param tableModel
     */
    public TableSorter(TableModel tableModel) {
        this();
        setTableModel(tableModel);
    }

    /**
     *
     * @param tableModel
     * @param tableHeader
     */
    public TableSorter(TableModel tableModel, JTableHeader tableHeader) {
        this();
        setTableHeader(tableHeader);
        setTableModel(tableModel);
    }

    private void clearSortingState() {
        viewToModel = null;
        modelToView = null;
    }

    /**
     *
     * @return
     */
    public TableModel getTableModel() {
        return tableModel;
    }

    /**
     *
     * @param tableModel
     */
    public void setTableModel(TableModel tableModel) {
        if (this.tableModel != null) {
            this.tableModel.removeTableModelListener(tableModelListener);
        }

        this.tableModel = tableModel;
        if (this.tableModel != null) {
            this.tableModel.addTableModelListener(tableModelListener);
        }

        clearSortingState();
        fireTableStructureChanged();
    }

    /**
     *
     * @return
     */
    public JTableHeader getTableHeader() {
        return tableHeader;
    }

    /**
     *
     * @param tableHeader
     */
    public void setTableHeader(JTableHeader tableHeader) {
        if (this.tableHeader != null) {
            this.tableHeader.removeMouseListener(mouseListener);
            TableCellRenderer defaultRenderer = this.tableHeader.getDefaultRenderer();
            if (defaultRenderer instanceof SortableHeaderRenderer) {
                this.tableHeader.setDefaultRenderer(((SortableHeaderRenderer) defaultRenderer).tableCellRenderer);
            }
        }
        this.tableHeader = tableHeader;
        if (this.tableHeader != null) {
            this.tableHeader.addMouseListener(mouseListener);
            this.tableHeader.setDefaultRenderer(new SortableHeaderRenderer(
                    this.tableHeader.getDefaultRenderer()));
        }
    }

    /**
     *
     * @return
     */
    public boolean isSorting() {
        return !sortingColumns.isEmpty();
    }

    private Directive getDirective(int column) {
        for (int i = 0; i < sortingColumns.size(); i++) {
            Directive directive = sortingColumns.get(i);
            if (directive.column == column) {
                return directive;
            }
        }
        return EMPTY_DIRECTIVE;
    }

    /**
     *
     * @param column
     * @return
     */
    public int getSortingStatus(int column) {
        return getDirective(column).direction;
    }

    private void sortingStatusChanged() {
        clearSortingState();
        fireTableDataChanged();
        if (tableHeader != null) {
            tableHeader.repaint();
        }
    }

    /**
     *
     * @param column
     * @param status
     */
    public void setSortingStatus(int column, int status) {
        Directive directive = getDirective(column);
        if (directive != EMPTY_DIRECTIVE) {
            sortingColumns.remove(directive);
        }
        if (status != NOT_SORTED) {
            sortingColumns.add(new Directive(column, status));
        }
        sortingStatusChanged();
    }

    /**
     *
     * @param column
     * @param size
     * @return
     */
    protected Icon getHeaderRendererIcon(int column, int size) {
        Directive directive = getDirective(column);
        if (directive == EMPTY_DIRECTIVE) {
            return null;
        }
        return new Arrow(directive.direction == DESCENDING, size,
                sortingColumns.indexOf(directive));
    }

    private void cancelSorting() {
        sortingColumns.clear();
        sortingStatusChanged();
    }

    /**
     *
     * @param type
     * @param comparator
     */
    public void setColumnComparator(Class<Object> type,
            Comparator<Object> comparator) {
        if (comparator == null) {
            columnComparators.remove(type);
        } else {
            columnComparators.put(type, comparator);
        }
    }

    /**
     *
     * @param column
     * @return
     */
    protected Comparator<Object> getComparator(Integer column) {

        Class<?> columnType = tableModel.getColumnClass(column);
        String columnName = tableModel.getColumnName(column);
        Comparator<Object> comparator = columnComparators.get(columnType);
        if (comparator != null) {
            return comparator;
        }

        if (columnType == java.lang.Float.class) {
            return FLOAT_COMPARATOR;
        } else if (columnName.equals("CTP Item")) {
            return PRESCALE_COMPARATOR;
        } else if (columnName.equals("Status") || columnName.equals("Creator")
                || columnName.equals("Origin") || columnName.equals("Comment")
                || columnName.equals("Name") || columnName.equals("Threshold")
                || columnName.equals("Type")) {
            return MONITORS_COMPARATOR;
        } else if (columnName.equals("Version")
                || columnName.equals("ID")
                // Old L1 Prescale Value.
                // || columnName.equals("Prescale Value")
                // || columnName.equals("Prescale: Set 1")
                // || columnName.equals("Prescale: Set 2")
                || columnName.equals("Chain Counter")
                || columnName.equals("Pass Through")
                || columnName.equals("Pass Through: Set 1")
                || columnName.equals("Pass Through: Set 2")
                || columnName.equals("Bunch Group Id")
                || columnName.equals("Counter")
                || columnName.equals("Internal Counter")
                || columnName.equals("PSK")) {
            return INTEGER_COMPARATOR;
        } else if (columnName.equals("Multiplicity")
                || columnName.equals("HLT Prescale")
                || columnName.equals("Prescale: Set 1")
                || columnName.equals("Prescale: Set 2")
                || columnName.equals("HLT Pass Through")
                || columnName.equals("ExStr PS")
                || columnName.equals("HLT Pass Through: Set 1")
                || columnName.equals("HLT Pass Through: Set 2")
                || columnName.equals("Rerun PS")
                || columnName.equals("Prescale Value")
                || columnName.equals("Pass-through")
                || columnName.equals("Stream PS (menu)")
                || columnName.equals("Stream PS")
                || columnName.equals("Chain PS")
                || columnName.equals("Chain PT")
                || columnName.equals("Stream")
                || columnName.equals("PS")
                || columnName.equals("PT")
                || columnName.equals("PS: 1")
                || columnName.equals("PS: 2")
                || columnName.equals("PT: 1")
                || columnName.equals("PT: 2")
                || columnName.equals("Stream PS (prescale set)") 
                || columnName.equals("Input Prescale") 
                || columnName.equals("Actual Prescale")
                || columnName.equals("Prescale Value: 1")
                || columnName.equals("Prescale Value: 2")
                || columnName.equals("Passthrough: 1")
                || columnName.equals("Passthrough: 2")
                ) {
            return FLOAT_COMPARATOR;
        } else if (columnName.equals("Chain")
                || columnName.equals("L2/EF")
                || columnName.equals("Level")
                || columnName.equals("Seed")
                || columnName.equals("L1 seed")
                || columnName.equals("Condition")
                || columnName.equals("ReRun")
                || columnName.equals("ReRun: 1")
                || columnName.equals("ReRun: 2")
                || columnName.equals("Name")
                || columnName.equals("Chain Name")
                || columnName.equals("Stream: 1")
                || columnName.equals("Stream: 2")
                || columnName.equals("Condition: 1")
                || columnName.equals("Condition: 2")
                || columnName.equals("Cond: 1")
                || columnName.equals("Cond: 2")) {
            return STRING_COMPARATOR;
        } else if (columnName.equals("In/Out")) {
            return BOOLEAN_COMPARATOR;
        } else if (columnName.equals("Releases")) {
            return REL_COMPARATOR;
        } else if (columnName.equals("Prescale cut")) {
            return HEX_COMPARATOR;
        } else if (columnName.equals("Lumi Min")
                || columnName.equals("Lumi Max")) {
            return LUMI_COMPARATOR;
        } else {

            return BOOLEAN_COMPARATOR;
        }
    }

    private Row[] getViewToModel() {
        if (viewToModel == null) {
            int tableModelRowCount = tableModel.getRowCount();
            viewToModel = new Row[tableModelRowCount];
            for (int row = 0; row < tableModelRowCount; row++) {
                viewToModel[row] = new Row(row);
            }

            if (isSorting()) {
                Arrays.sort(viewToModel);
            }
        }
        return viewToModel;
    }

    /**
     *
     * @param viewIndex
     * @return
     */
    public int modelIndex(int viewIndex) {
        return getViewToModel()[viewIndex].modelIndex;
    }

    private int[] getModelToView() {
        if (modelToView == null) {
            int n = getViewToModel().length;
            modelToView = new int[n];
            for (int i = 0; i < n; i++) {
                modelToView[modelIndex(i)] = i;
            }
        }
        return modelToView;
    }

    // TableModel interface methods
    @Override
    public int getRowCount() {
        return (tableModel == null) ? 0 : tableModel.getRowCount();
    }

    @Override
    public int getColumnCount() {
        return (tableModel == null) ? 0 : tableModel.getColumnCount();
    }

    @Override
    public String getColumnName(int column) {
        return tableModel.getColumnName(column);
    }

    @Override
    public Class getColumnClass(int column) {
        return tableModel.getColumnClass(column);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return tableModel.isCellEditable(modelIndex(row), column);
    }

    /**
     *
     * @param s
     * @return
     */
    public static boolean isNumber(String s) {
        for (int j = 0; j < s.length(); j++) {
            if (!Character.isDigit(s.charAt(j))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Object getValueAt(int row, int column) {
        return tableModel.getValueAt(modelIndex(row), column);
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
        tableModel.setValueAt(aValue, modelIndex(row), column);
    }

    // Helper classes
    private class Row implements Comparable<Object> {

        private int modelIndex;

        public Row(int index) {
            this.modelIndex = index;
        }

        @Override
        public int compareTo(Object o) {
            int row1 = modelIndex;
            int row2 = ((Row) o).modelIndex;
            for (Directive directive : sortingColumns) {
                int column = directive.column;
                Object o1 = tableModel.getValueAt(row1, column);
                Object o2 = tableModel.getValueAt(row2, column);

                int comparison;
                // Define null less than everything, except null.
                if (o1 == null && o2 == null) {
                    comparison = 0;
                } else if (o1 == null) {
                    comparison = -1;
                } else if (o2 == null) {
                    comparison = 1;
                } else {
                    comparison = getComparator(column).compare(o1, o2);
                }
                if (comparison != 0) {
                    return directive.direction == DESCENDING ? -comparison
                            : comparison;
                }
            }
            return 0;
        }
    }

    private class TableModelHandler implements TableModelListener {

        @Override
        public void tableChanged(TableModelEvent e) {
            // If we're not sorting by anything, just pass the event along.
            if (!isSorting()) {
                clearSortingState();
                fireTableChanged(e);
                return;
            }

            // If the table structure has changed, cancel the sorting; the
            // sorting columns may have been either moved or deleted from
            // the model.
            if (e.getFirstRow() == TableModelEvent.HEADER_ROW) {
                cancelSorting();
                fireTableChanged(e);
                return;
            }

            // We can map a cell event through to the view without widening
            // when the following conditions apply:
            //
            // a) all the changes are on one row (e.getFirstRow() ==
            // e.getLastRow()) and,
            // b) all the changes are in one column (column !=
            // TableModelEvent.ALL_COLUMNS) and,
            // c) we are not sorting on that column (getSortingStatus(column) ==
            // NOT_SORTED) and,
            // d) a reverse lookup will not trigger a sort (modelToView != null)
            //
            // Note: INSERT and DELETE events fail this test as they have column
            // == ALL_COLUMNS.
            //
            // The last check, for (modelToView != null) is to see if
            // modelToView
            // is already allocated. If we don't do this check; sorting can
            // become
            // a performance bottleneck for applications where cells
            // change rapidly in different parts of the table. If cells
            // change alternately in the sorting column and then outside of
            // it this class can end up re-sorting on alternate cell updates -
            // which can be a performance problem for large tables. The last
            // clause avoids this problem.
            int column = e.getColumn();
            if (e.getFirstRow() == e.getLastRow()
                    && column != TableModelEvent.ALL_COLUMNS
                    && getSortingStatus(column) == NOT_SORTED
                    && modelToView != null) {
                int viewIndex = getModelToView()[e.getFirstRow()];
                fireTableChanged(new TableModelEvent(TableSorter.this,
                        viewIndex, viewIndex, column, e.getType()));
                return;
            }

            // Something has happened to the data that may have invalidated the
            // row order.
            clearSortingState();
            fireTableDataChanged();
        }
    }

    private class MouseHandler extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {

            JTableHeader h = (JTableHeader) e.getSource();
            TableColumnModel columnModel = h.getColumnModel();
            int viewColumn = columnModel.getColumnIndexAtX(e.getX());
            int column = columnModel.getColumn(viewColumn).getModelIndex();
            if (column != -1) {
                int status = getSortingStatus(column);
                if (!e.isControlDown()) {
                    cancelSorting();
                }
                // Cycle the sorting states through {NOT_SORTED, ASCENDING,
                // DESCENDING} or
                // {NOT_SORTED, DESCENDING, ASCENDING} depending on whether
                // shift is pressed.
                status = status + (e.isShiftDown() ? -1 : 1);
                status = (status + 4) % 3 - 1; // signed mod, returning {-1, 0,
                // 1}
                setSortingStatus(column, status);
            }
        }
    }

    private static class Arrow implements Icon {

        private final boolean descending;
        private final int size;
        private final int priority;

        public Arrow(boolean descending, int size, int priority) {
            this.descending = descending;
            this.size = size;
            this.priority = priority;
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            Color color = c == null ? Color.GRAY : c.getBackground();
            // In a compound sort, make each succesive triangle 20%
            // smaller than the previous one.
            int dx = (int) (size / 2 * Math.pow(0.8, priority));
            int dy = descending ? dx : -dx;
            // Align icon (roughly) with font baseline.
            y = y + 5 * size / 6 + (descending ? -dy : 0);
            int shift = descending ? 1 : -1;
            g.translate(x, y);

            // Right diagonal.
            g.setColor(color.darker());
            g.drawLine(dx / 2, dy, 0, 0);
            g.drawLine(dx / 2, dy + shift, 0, shift);

            // Left diagonal.
            g.setColor(color.brighter());
            g.drawLine(dx / 2, dy, dx, 0);
            g.drawLine(dx / 2, dy + shift, dx, shift);

            // Horizontal line.
            if (descending) {
                g.setColor(color.darker().darker());
            } else {
                g.setColor(color.brighter().brighter());
            }
            g.drawLine(dx, 0, 0, 0);

            g.setColor(color);
            g.translate(-x, -y);
        }

        @Override
        public int getIconWidth() {
            return size;
        }

        @Override
        public int getIconHeight() {
            return size;
        }
    }

    private class SortableHeaderRenderer implements TableCellRenderer {

        private TableCellRenderer tableCellRenderer;

        public SortableHeaderRenderer(TableCellRenderer tableCellRenderer) {
            this.tableCellRenderer = tableCellRenderer;
        }

        @Override
        public Component getTableCellRendererComponent(JTable table,
                Object value, boolean isSelected, boolean hasFocus, int row,
                int column) {
            Component c = tableCellRenderer.getTableCellRendererComponent(
                    table, value, isSelected, hasFocus, row, column);
            if (c instanceof JLabel) {
                JLabel l = (JLabel) c;
                l.setHorizontalTextPosition(JLabel.LEFT);
                int modelColumn = table.convertColumnIndexToModel(column);
                l.setIcon(getHeaderRendererIcon(modelColumn, l.getFont().getSize()));
            }
            return c;
        }
    }

    private static class Directive {

        private int column;
        private int direction;

        public Directive(int column, int direction) {
            this.column = column;
            this.direction = direction;
        }
    }
}
