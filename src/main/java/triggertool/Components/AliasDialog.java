package triggertool.Components;

///Dialog box to assign alias aliasNames
import triggerdb.PrescaleSetAliasLumi;
import triggerdb.PrescaleSetAliasComponent;
import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import javax.xml.parsers.ParserConfigurationException;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.CompoundHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.decorator.SearchPredicate;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.renderer.LabelProvider;
import org.xml.sax.SAXException;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.UserMode;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.L1.L1Prescale;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.Entities.alias.PrescaleSetAlias;
import triggertool.Panels.MainPanel;
import triggertool.TTExceptionHandler;

/**
 * Dialog box to assign alias aliasNames
 *
 * @author Alex Martyniuk
 */
public final class AliasDialog extends JDialog {

    /**
     * the logger.
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");

    CellEditorListener listen = null;
    //Table models for the display
    private AliasTableModel modelAlias;
    private boolean tableChange = false;
    //TableHighlighter set
    private final CompoundHighlighter tableHighlighters = new CompoundHighlighter();

    private AliasDialogInteractor interactor;
    private String newAliasName;
    private SuperMasterTable smt;

    private MainPanel parent;
    ArrayList<String> unsavedAliases = new ArrayList<>();
    /**
     * An integer representing the role of the user (see ConnectionManager).
     */
    private UserMode userMode = UserMode.UNKNOWN;

    /**
     * File chooser
     */
    private final JFileChooser fc = new JFileChooser(System.getProperty("user.dir"));

    /**
     * Create and show this dialog box.
     *
     * @param parent The parent frame, which may be null.
     * @param s
     * @param otherAliasWindowActive
     */
    public AliasDialog(java.awt.Frame parent, SuperMasterTable s, Boolean otherAliasWindowActive) {
        super((JDialog) null);
        try {
            this.parent = (MainPanel) parent;
            interactor = new AliasDialogInteractor(s, this.parent);
            this.initComponents();
            this.setTitle("Prescale Set Aliases");
            //this.fillAliasSetCombo();
            this.fillAliasCombo();
            this.loadTable();
            this.loadEditors();
            this.fillTable();
            this.setLocationRelativeTo(parent);
            this.setVisible(true);
            if (otherAliasWindowActive) {
                viewModeWarningLabel.setVisible(true);
            } else {
                viewModeWarningLabel.setVisible(false);
            }
            saveProgressLabel.setVisible(false);
            if (!AliasCombo.getSelectedItem().equals("--")) {
                if (userMode.ordinal() > UserMode.SHIFTER.ordinal() && !otherAliasWindowActive) {
                    AddAbove.setEnabled(true);
                    AddBelow.setEnabled(true);
                    Remove.setEnabled(true);
                    RowNumber.setEnabled(true);
                }
            } else {
                AddAbove.setEnabled(false);
                AddBelow.setEnabled(false);
                Remove.setEnabled(false);
                RowNumber.setEnabled(false);
            }
            smt = s;
            this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
            this.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                    DoCloseAction();
                }
            });
            AliasSetCombo.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    if (AliasSetCombo.getSelectedItem() != null) {
                        fillAliasCombo();
                        try {
                            fillTable();
                        } catch (SQLException ex) {
                            String message = "Error occurred while filling table in the Alias Dialog panel.";
                            logger.log(Level.SEVERE, message, ex);
                            JOptionPane.showMessageDialog(AliasSetCombo, "SQL Error", message + " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
                        }
                        if (!AliasCombo.getSelectedItem().equals("--")) {
                            if (userMode.ordinal() > UserMode.SHIFTER.ordinal() && !otherAliasWindowActive) {
                                AddAbove.setEnabled(true);
                                AddBelow.setEnabled(true);
                                Remove.setEnabled(true);
                                RowNumber.setEnabled(true);
                            }
                        } else {
                            AddAbove.setEnabled(false);
                            AddBelow.setEnabled(false);
                            Remove.setEnabled(false);
                            RowNumber.setEnabled(false);
                        }

                    }
                }
            });

            userMode = ConnectionManager.getInstance().getInitInfo().getUserMode();
            if (userMode.ordinal() < UserMode.SHIFTER.ordinal() || otherAliasWindowActive) {
                AddAbove.setEnabled(false);
                AddBelow.setEnabled(false);
                CommentBox.setEditable(false);
                Create.setEnabled(false);
                aliasTable.setEditable(false);
                XMLpath.setEditable(false);
                Browse.setEnabled(false);
                XMLLoad.setEnabled(false);
                Remove.setEnabled(false);
                RowNumber.setEnabled(false);
            }

            SaveAs.setEnabled(false);
            Save.setEnabled(false);
            XMLLoad.setEnabled(false);

            this.parent.lockAlias();
            XMLpath.addKeyListener(new KeyListener() {

                @Override
                public void keyTyped(KeyEvent e) {
                    try {
                        checkPath();
                    } catch (ParserConfigurationException | SAXException | IOException ex) {
                        Logger.getLogger(AliasDialog.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                @Override
                public void keyPressed(KeyEvent e) {
                    try {
                        checkPath();
                    } catch (ParserConfigurationException | SAXException | IOException ex) {
                        Logger.getLogger(AliasDialog.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                @Override
                public void keyReleased(KeyEvent e) {
                    try {
                        checkPath();
                    } catch (ParserConfigurationException | SAXException | IOException ex) {
                        Logger.getLogger(AliasDialog.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });

            CommentBox.addKeyListener(new KeyListener() {

                @Override
                public void keyTyped(KeyEvent e) {
                    Save.setEnabled(true);
                    SaveAs.setEnabled(true);
                }

                @Override
                public void keyPressed(KeyEvent e) {
                    Save.setEnabled(true);
                    SaveAs.setEnabled(true);
                }

                @Override
                public void keyReleased(KeyEvent e) {
                    Save.setEnabled(true);
                    SaveAs.setEnabled(true);
                }
            });

        } catch (SQLException ex) {
            String message = "Error occurred while creating the Alias Dialog panel.";
            logger.log(Level.SEVERE, message, ex);
            JOptionPane.showMessageDialog(parent, "SQL Error", message
                    + " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
        }
    }

    private void checkPath() throws ParserConfigurationException, SAXException, IOException {
        if (XMLpath.getText().isEmpty()) {
            XMLLoad.setEnabled(false);
        } else {
            if (checkFile()) {
                XMLLoad.setEnabled(true);
            } else {
                XMLLoad.setEnabled(false);
            }
        }
    }

    /**
     *
     * @throws SQLException
     */
    public void reload() throws SQLException {
        interactor.getAliases();
        //find the alias information
        //this.fillAliasSetCombo();
        //Load the table models
        this.loadTable();
        //Load the table models
        //Fill the tables
        this.fillTable();
        this.setVisible(true);
    }

    /**
     * Gets the existing aliases from the DB Adds a listener to refill the table
     * if a change is made
     *
     */
    public void fillAliasCombo() {
        this.AliasCombo.removeAllItems();
        if (!interactor.aliasNamesMap.isEmpty()) {
            Object sel = AliasSetCombo.getSelectedItem();
            HashSet<String> names_free = interactor.aliasNamesMap.get(sel.toString());
            if (names_free != null) {
                for (String name_free : names_free) {
                    AliasCombo.addItem(name_free);
                }
            } else {
                AliasCombo.addItem(AliasDialogInteractor.EMPTY_STR);
            }
        } else {
            AliasCombo.addItem(AliasDialogInteractor.EMPTY_STR);
        }
        AliasCombo.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                if (AliasCombo.getSelectedItem() != null) {
                    try {
                        fillTable();
                    } catch (SQLException ex) {
                        String message = "Error occurred while filling table in the Alias Dialog panel.";
                        logger.log(Level.SEVERE, message, ex);
                        JOptionPane.showMessageDialog(AliasSetCombo, "SQL Error", message
                                + " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
    }

    /**
     *
     * Loads the table models setting preferred settings.
     *
     */
    public void loadTable() {
        logger.finest("Loading Alias Table");
        //JXTable sorting is broken for some things
        aliasTable.setSortable(false);
        modelAlias = (AliasTableModel) aliasTable.getModel();
        aliasTable.setColumnSelectionAllowed(false);
        aliasTable.setRowSelectionAllowed(true);

        //Set some default sizes
        aliasTable.getColumnModel().getColumn(0).setPreferredWidth(50);
        aliasTable.getColumnModel().getColumn(1).setPreferredWidth(50);
        aliasTable.getColumnModel().getColumn(2).setPreferredWidth(100);
        aliasTable.getColumnModel().getColumn(3).setPreferredWidth(50);
        aliasTable.getColumnModel().getColumn(4).setPreferredWidth(50);

        //Remove the annoying comma and right align the integer column
        TableCellRenderer numberRenderer = aliasTable.getDefaultRenderer(Number.class);
        aliasTable.setDefaultRenderer(Float.class, numberRenderer);
        aliasTable.setDefaultRenderer(Double.class, numberRenderer);
        LabelProvider provider = new LabelProvider();
        aliasTable.setDefaultRenderer(Number.class, new DefaultTableRenderer(provider));
    }

    private void registerUnsavedAlias() {
        String alias = AliasSetCombo.getSelectedItem().toString() + ": " + AliasCombo.getSelectedItem().toString();
        if (!unsavedAliases.contains(alias)) {
            unsavedAliases.add(alias);
        }
    }

    private void removeUnsavedAlias() {
        String alias = AliasSetCombo.getSelectedItem().toString() + ": " + AliasCombo.getSelectedItem().toString();
        if (unsavedAliases.contains(alias)) {
            unsavedAliases.remove(alias);
        }
    }

    /**
     *
     * Loads the table models editors to fire the lumi code when the table is
     * updated.
     *
     */
    public void loadEditors() {
        //Allow the table to fire checkLumi when we change things
        aliasTable.getDefaultEditor(String.class).addCellEditorListener(new CellEditorListener() {

            @Override
            public void editingStopped(ChangeEvent ce) {
                checkLumi();
                registerUnsavedAlias();
                Save.setEnabled(true);
                SaveAs.setEnabled(true);
            }

            @Override
            public void editingCanceled(ChangeEvent ce) {
                checkLumi();
                registerUnsavedAlias();
                Save.setEnabled(true);
                SaveAs.setEnabled(true);
            }
        });
    }

    /**
     * Fills the table with the information
     *
     * @throws java.sql.SQLException
     */
    public void fillTable() throws SQLException {
        if (tableChange) {
            logger.fine("Table changed");
        }
        PrescaleSetAliasComponent current = getCurrentAlias();
        // Fill Comment Area
        this.CommentBox.setText(current.get_comment());
        //
        //// Clear and refill Lumi Table
        AliasTableModel model = (AliasTableModel) this.aliasTable.getModel();
        model.clearNumRows();
        int rows = model.getRowCount();

        List<PrescaleSetAliasLumi> _lumis = current.get_lumis();
        Collections.sort(_lumis);
        for (PrescaleSetAliasLumi row : _lumis) {
            if (row != null && rows < 1) {
                JComboBox<L1Prescale> L1cb = new JComboBox<>(interactor.L1PSKs);
                DefaultCellEditor L1ed = new DefaultCellEditor(L1cb);
                aliasTable.getColumn(0).setCellEditor(L1ed);
                loadCBListener(L1cb, "L1");
                JComboBox<HLTPrescaleSet> HLTcb = new JComboBox<>(interactor.HLTPSKs);
                DefaultCellEditor HLTed = new DefaultCellEditor(HLTcb);
                aliasTable.getColumn(1).setCellEditor(HLTed);
                loadCBListener(HLTcb, "HLT");
            } else {
                aliasTable.getRowEditorModel().removeEditorForRow(rows);
            }
            model.add(row);
            rows = model.getRowCount();
        }
        this.checkLumi();
        tableChange = false;
        model.fireTableDataChanged();
        model.addTableModelListener(new TableModelListener() {

            @Override
            public void tableChanged(TableModelEvent tme) {
                tableChange = true;
            }
        });
    }

    /**
     *
     * Adds a listener to the combo box to listen for updates
     *
     * @param cb
     * @param L1_HLT
     */
    public void loadCBListener(JComboBox cb, final String L1_HLT) {
        cb.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent ie) {
                int row = aliasTable.getSelectedRow();
                registerUnsavedAlias();
                // Change L1 PSK
                if (L1_HLT.equals("L1") && row >= 0) {
                    L1Prescale newPS = (L1Prescale) ie.getItem();
                    L1Prescale oldPS = modelAlias.getL1PS(row);

                    if (!newPS.equals(oldPS)) {
                        modelAlias.setValueAt(newPS, row, AliasRow.COL_L1PS);
                        // Automatically adjust Lmin and Lmax based on prev and next row.
                        /*if (row - 1 >= 0) {
                            String l = modelAlias.getLumiMax(row - 1);
                            modelAlias.setLumiMin(l, row);
                        }
                        if (row + 1 < modelAlias.getRowCount()) {
                            String l = modelAlias.getLumiMin(row + 1);
                            modelAlias.setLumiMax(l, row);
                        }*/
                        checkLumi();
                        modelAlias.fireTableDataChanged();
                        Save.setEnabled(true);
                        SaveAs.setEnabled(true);
                    }
                } else if (L1_HLT.equals("HLT") && row >= 0) {
                    HLTPrescaleSet newPS = (HLTPrescaleSet) ie.getItem();
                    HLTPrescaleSet oldPS = (HLTPrescaleSet) modelAlias.getHLTPS(row);
                    if (!newPS.equals(oldPS)) {
                        modelAlias.setHLTPS(newPS, row);
                        /*if (row - 1 >= 0) {
                            String l = modelAlias.getLumiMax(row - 1);
                            modelAlias.setLumiMin(l, row);
                        }
                        if (row + 1 < modelAlias.getRowCount()) {
                            String l = modelAlias.getLumiMin(row + 1);
                            modelAlias.setLumiMax(l, row);
                        }*/
                        checkLumi();
                        modelAlias.fireTableDataChanged();
                        Save.setEnabled(true);
                        SaveAs.setEnabled(true);
                    }
                }
            }
        });
    }

    /**
     * Grabs the currently selected alias set
     */
    PrescaleSetAliasComponent getCurrentAlias() throws SQLException {
        PrescaleSetAliasComponent output = null;
        Boolean found = false;
        for (PrescaleSetAliasComponent alias : interactor.aliasList) {
            if (alias.get_name().equals(AliasSetCombo.getSelectedItem())
                    && alias.get_name_free().equals(AliasCombo.getSelectedItem())) {
                output = alias;
                found = true;
            }
        }
        if (found) {
            return output;
        } else {
            return new PrescaleSetAliasComponent(-1, interactor.SMT);
        }
    }

    ///Netbeans. Don't touch.
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        aliasTablePanel = new javax.swing.JPanel();
        TableScrollPane = new javax.swing.JScrollPane();
        aliasTable = new triggertool.Components.JTableX();
        RowNumber = new javax.swing.JComboBox<>();
        AddAbove = new javax.swing.JButton();
        Remove = new javax.swing.JButton();
        XMLpath = new javax.swing.JTextField();
        XMLLoad = new javax.swing.JButton();
        Create = new javax.swing.JButton();
        Save = new javax.swing.JButton();
        Close = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        AliasSetCombo = new javax.swing.JComboBox<>();
        AliasCombo = new javax.swing.JComboBox<>();
        CommentScroller = new javax.swing.JScrollPane();
        CommentBox = new javax.swing.JTextArea();
        Comment = new javax.swing.JLabel();
        Browse = new javax.swing.JButton();
        Message = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        AddBelow = new javax.swing.JButton();
        viewTwiki = new javax.swing.JButton();
        SaveAs = new javax.swing.JButton();
        saveProgressLabel = new javax.swing.JLabel();
        viewModeWarningLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Alias Panel"); // NOI18N
        setMinimumSize(new java.awt.Dimension(770, 575));
        setResizable(false);

        aliasTable.setModel(new triggertool.Components.AliasTableModel());
        TableScrollPane.setViewportView(aliasTable);

        RowNumber.setModel(new javax.swing.DefaultComboBoxModel<String>(new String[] { "1 Rows", "2 Rows", "3 Rows", "4 Rows", "5 Rows", "6 Rows", "7 Rows", "8 Rows", "9 Rows", "10 Rows" }));
        RowNumber.setMinimumSize(new java.awt.Dimension(0, 0));

        AddAbove.setText("Insert Rows Above"); // NOI18N
        AddAbove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddAboveActionPerformed(evt);
            }
        });

        Remove.setText("Remove Row"); // NOI18N
        Remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RemoveActionPerformed(evt);
            }
        });

        XMLLoad.setText("Upload & Save"); // NOI18N
        XMLLoad.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                XMLLoadMouseClicked(evt);
            }
        });

        Create.setText("Create Empty Alias"); // NOI18N
        Create.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CreateActionPerformed(evt);
            }
        });

        Save.setText("Save"); // NOI18N
        Save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaveActionPerformed(evt);
            }
        });

        Close.setText("Close"); // NOI18N
        Close.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CloseActionPerformed(evt);
            }
        });

        jLabel3.setText("Select existing alias to view or edit"); // NOI18N

        jLabel1.setText("Select type of alias"); // NOI18N

        AliasSetCombo.setModel(new javax.swing.DefaultComboBoxModel<Object>(AliasTypeRef.types.toArray()));
        AliasSetCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AliasSetComboActionPerformed(evt);
            }
        });

        AliasCombo.setModel(new javax.swing.DefaultComboBoxModel<String>(new String[] { "Aliases" }));

        CommentBox.setColumns(20);
        CommentBox.setRows(5);
        CommentScroller.setViewportView(CommentBox);

        Comment.setText("Edit Comment"); // NOI18N

        Browse.setText("Browse"); // NOI18N
        Browse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BrowseActionPerformed(evt);
            }
        });

        jLabel2.setText("Bulk Upload and Save from XML");

        AddBelow.setText("Insert Rows Below"); // NOI18N
        AddBelow.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddBelowActionPerformed(evt);
            }
        });

        viewTwiki.setText("View As TWiki");
        viewTwiki.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewTwikiActionPerformed(evt);
            }
        });

        SaveAs.setText("Save As New Alias");
        SaveAs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaveAsActionPerformed(evt);
            }
        });

        saveProgressLabel.setFont(new java.awt.Font("Tahoma", 3, 13)); // NOI18N
        saveProgressLabel.setText("Saving in progress, please wait...");

        viewModeWarningLabel.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        viewModeWarningLabel.setText("Another Instance Open - VIEW MODE");

        org.jdesktop.layout.GroupLayout aliasTablePanelLayout = new org.jdesktop.layout.GroupLayout(aliasTablePanel);
        aliasTablePanel.setLayout(aliasTablePanelLayout);
        aliasTablePanelLayout.setHorizontalGroup(
            aliasTablePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(aliasTablePanelLayout.createSequentialGroup()
                .add(aliasTablePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(aliasTablePanelLayout.createSequentialGroup()
                        .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(Close, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, aliasTablePanelLayout.createSequentialGroup()
                        .add(0, 11, Short.MAX_VALUE)
                        .add(aliasTablePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, aliasTablePanelLayout.createSequentialGroup()
                                .add(Comment, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 92, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(CommentScroller, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 607, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, XMLLoad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 137, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(aliasTablePanelLayout.createSequentialGroup()
                        .add(20, 20, 20)
                        .add(aliasTablePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, TableScrollPane)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jSeparator1)
                            .add(aliasTablePanelLayout.createSequentialGroup()
                                .add(Remove, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 110, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(AddAbove)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(AddBelow)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(RowNumber, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 129, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 42, Short.MAX_VALUE)
                                .add(viewTwiki))
                            .add(aliasTablePanelLayout.createSequentialGroup()
                                .add(aliasTablePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(jLabel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
                                    .add(AliasSetCombo, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(aliasTablePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(AliasCombo, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .add(aliasTablePanelLayout.createSequentialGroup()
                                        .add(jLabel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(viewModeWarningLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 252, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                            .add(aliasTablePanelLayout.createSequentialGroup()
                                .add(XMLpath)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(Browse, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .add(aliasTablePanelLayout.createSequentialGroup()
                                .add(aliasTablePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(Message, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 272, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(jLabel2))
                                .add(0, 0, Short.MAX_VALUE))
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, aliasTablePanelLayout.createSequentialGroup()
                                .add(saveProgressLabel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(Create, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 162, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(SaveAs)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(Save, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 85, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );

        aliasTablePanelLayout.linkSize(new java.awt.Component[] {Browse, Close, Save}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        aliasTablePanelLayout.setVerticalGroup(
            aliasTablePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(aliasTablePanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(aliasTablePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel1)
                    .add(jLabel3)
                    .add(viewModeWarningLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(aliasTablePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(AliasSetCombo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(AliasCombo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(TableScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 240, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(aliasTablePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(Remove)
                    .add(AddAbove)
                    .add(RowNumber, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(AddBelow)
                    .add(viewTwiki))
                .add(aliasTablePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(aliasTablePanelLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(CommentScroller, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 61, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(aliasTablePanelLayout.createSequentialGroup()
                        .add(35, 35, 35)
                        .add(Comment)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(aliasTablePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(Save)
                    .add(Create)
                    .add(SaveAs)
                    .add(saveProgressLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(aliasTablePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(aliasTablePanelLayout.createSequentialGroup()
                        .add(jLabel2)
                        .add(18, 18, 18)
                        .add(aliasTablePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(XMLpath, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(Browse))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(aliasTablePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(XMLLoad, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(aliasTablePanelLayout.createSequentialGroup()
                                .add(0, 0, Short.MAX_VALUE)
                                .add(Message, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 25, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(0, 52, Short.MAX_VALUE))
                    .add(aliasTablePanelLayout.createSequentialGroup()
                        .add(0, 0, Short.MAX_VALUE)
                        .add(Close)))
                .addContainerGap())
        );

        aliasTablePanelLayout.linkSize(new java.awt.Component[] {AliasCombo, AliasSetCombo}, org.jdesktop.layout.GroupLayout.VERTICAL);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(aliasTablePanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(aliasTablePanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BrowseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BrowseActionPerformed
        XMLExtensionFilter filter = new XMLExtensionFilter();
        fc.setFileFilter(filter);
        int returnVal = fc.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = fc.getSelectedFile();
            XMLpath.setText(f.toString());
            try {
                if (checkFile()) {
                    XMLLoad.setEnabled(true);
                } else {
                    XMLLoad.setEnabled(false);
                }
            } catch (ParserConfigurationException | SAXException | IOException | NullPointerException ex) {
                String message = "Error occurred browsing for a file Alias Dialog panel.";
                logger.log(Level.SEVERE, message, ex);
                JOptionPane.showMessageDialog(this, "SQL Error", message
                        + " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_BrowseActionPerformed

    private void AliasSetComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AliasSetComboActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_AliasSetComboActionPerformed

    private void CloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CloseActionPerformed
        DoCloseAction();
    }//GEN-LAST:event_CloseActionPerformed

    private void DoCloseAction() throws HeadlessException {
        if (!unsavedAliases.isEmpty()) {
            Object[] options = {"Yes, close", "No, cancel"};
            int n = JOptionPane.showOptionDialog(null, "The following aliases may have unsaved changes: " + unsavedAliases.toString() + " Are you sure you wish to proceed?", "Unsaved Changes", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
                    null,
                    options,
                    options[1]);
            if (n == JOptionPane.YES_OPTION) {
                this.parent.unlockAlias();
                this.dispose();
            }
        } else {
            this.parent.unlockAlias();
            this.dispose();
        }
    }

    private void SaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaveActionPerformed
        try {
            DoSaveAction();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error occurred while performing a save action in the Alias Dialog panel.");
        }
    }

    private void DoSaveAction() throws SQLException {

        PrescaleSetAlias psAlias = new PrescaleSetAlias(-1);
        psAlias.set_name(AliasSetCombo.getSelectedItem().toString());
        psAlias.set_name_free(AliasCombo.getSelectedItem().toString());
        psAlias.set_comment(Comment.getText());
        psAlias.set_default(false);
        ArrayList<Integer> psaIds = psAlias.get_matchingAliases();

        if (!psaIds.isEmpty()) {
            JOptionPane.showMessageDialog(null,
                    "Saving will overwrite exiting alias, please use 'Save As New Alias'.", "Attempt to Overwrite Alias",
                    JOptionPane.WARNING_MESSAGE);
            /*Object[] options = {"Yes, proceed", "No, cancel"};
            int n = JOptionPane.showOptionDialog(null, "Saving will overwrite existing alias. Are you sure you wish to proceed?", " Overwrite Warning", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
                    null,
                    options,
                    options[1]);
            if (n != JOptionPane.YES_OPTION) {
                return;
            }*/
            return;
        }

        //Try to save the alias set first....
        PrescaleSetAliasComponent psa = this.getCurrentAlias();
        String comment = this.CommentBox.getText().trim();
        psa.set_comment(comment);
        psa.save();
        psa.update_comment(comment);
        int aliasId = psa.get_alias().get_id();
        //loop over the table, try and save the table....
        AliasTableModel model = (AliasTableModel) aliasTable.getModel();
        Boolean errors = false;
        for (int row = 0; row < model.getRowCount(); row++) {
            PrescaleSetAliasLumi psal = model.getPrescaleSetAliasLumi(row);
            psal.set_alias_id(aliasId);
            int res = psal.save();
            if (res == -1) {
                errors = true;
            }
        }
        if (errors) {
            JOptionPane.showMessageDialog(null,
                    "One or more saved alias rows were missing a prescale set, alias may be incomplete!'.", "Alias with missing prescales",
                    JOptionPane.WARNING_MESSAGE);
        }
        fillTable();
        removeUnsavedAlias();
        if (unsavedAliases.isEmpty()) {
            Save.setEnabled(false);
            SaveAs.setEnabled(false);
        }
    }//GEN-LAST:event_SaveActionPerformed

    private void CreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CreateActionPerformed
        try {
            newAliasName = ((String) JOptionPane.showInputDialog(null, "Specify name for new alias of type " + AliasSetCombo.getSelectedItem().toString()));
            if (((DefaultComboBoxModel) AliasCombo.getModel()).getIndexOf(newAliasName) != -1) {
                JOptionPane.showMessageDialog(null,
                        "Alias " + newAliasName + " already exists, please choose another or edit this alias directly.", "Duplicate Alias",
                        JOptionPane.WARNING_MESSAGE);
                return;
            }
            if (newAliasName.equals("")) {
                JOptionPane.showMessageDialog(null,
                        "Alias names cannot be empty. Please enter a descriptive name.", "Empty Alias Name",
                        JOptionPane.WARNING_MESSAGE);
                return;
            }
            DoCreateActionPerformed();
        } catch (SQLException ex) {
            triggertool.TTExceptionHandler.HandleSqlException(ex, this, "Error occurred while performing a create action in the Alias Dialog panel.");
        }
    }

    private void DoCreateActionPerformed() throws SQLException {
        String newAliasSetName = AliasSetCombo.getSelectedItem().toString();

        PrescaleSetAliasComponent newAlias = new PrescaleSetAliasComponent(-1, interactor.SMT);
        newAlias.set_name(newAliasSetName);
        newAlias.set_name_free(newAliasName);
        interactor.aliasList.add(newAlias);
        interactor.fillAliasNames(newAlias);
        //fillAliasSetCombo();
        AliasSetCombo.setSelectedItem(newAliasSetName);
        AliasCombo.setSelectedItem(newAliasName);
    }//GEN-LAST:event_CreateActionPerformed

    private void XMLLoadMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_XMLLoadMouseClicked
        saveProgressLabel.setVisible(true);
        Save.setEnabled(false);
        SaveAs.setEnabled(false);
        Create.setEnabled(false);
        AddAbove.setEnabled(false);
        AddBelow.setEnabled(false);
        Remove.setEnabled(false);
        Browse.setEnabled(false);
        Close.setEnabled(false);
        viewTwiki.setEnabled(false);
        RowNumber.setEnabled(false);
        aliasTable.setEnabled(false);
        AliasSetCombo.setEnabled(false);
        AliasCombo.setEnabled(false);
        XMLpath.setEnabled(false);
        CommentBox.setEnabled(false);
        XMLLoad.setEnabled(false);

        ArrayList<String> results = new ArrayList<>();
        try {
            _XMLLoadMouseClicked(results);
        } catch (SQLException ex) {
            String message = "Error occurred loading the XML load button in the Alias Dialog panel.";
            logger.log(Level.SEVERE, message, ex);
            JOptionPane.showMessageDialog(this, message
                    + " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, "SQL Error", JOptionPane.ERROR_MESSAGE);
        } catch (ParserConfigurationException ex) {
            String message = "Error occurred loading the XML load button in the Alias Dialog panel.";
            logger.log(Level.SEVERE, message, ex);
            JOptionPane.showMessageDialog(this, message
                    + " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, "Parsing Error", JOptionPane.ERROR_MESSAGE);
        } catch (SAXException ex) {
            String message = "Error occurred loading the XML load button in the Alias Dialog panel.";
            logger.log(Level.SEVERE, message, ex);
            JOptionPane.showMessageDialog(this, message
                    + " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, "SAX Error", JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            String message = "Error occurred loading the XML load button in the Alias Dialog panel.";
            logger.log(Level.SEVERE, message, ex);
            JOptionPane.showMessageDialog(this, message
                    + " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, "IO Error", JOptionPane.ERROR_MESSAGE);
        }
        if (unsavedAliases.isEmpty()) {
            Save.setEnabled(false);
            SaveAs.setEnabled(false);
        } else {
            Save.setEnabled(true);
            SaveAs.setEnabled(true);
        }

        Create.setEnabled(true);
        AddAbove.setEnabled(true);
        AddBelow.setEnabled(true);
        Remove.setEnabled(true);
        Browse.setEnabled(true);
        Close.setEnabled(true);
        viewTwiki.setEnabled(true);
        RowNumber.setEnabled(true);
        aliasTable.setEnabled(true);
        AliasSetCombo.setEnabled(true);
        AliasCombo.setEnabled(true);
        XMLpath.setEnabled(true);
        CommentBox.setEnabled(true);
        saveProgressLabel.setVisible(false);
        fillAliasCombo();
        Message.setText("");
        AliasCombo.setSelectedItem(results.get(1));
    }//GEN-LAST:event_XMLLoadMouseClicked

    /**
     * Remove the selected row form the table and DB.
     *
     * @param evt
     */
    private void RemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RemoveActionPerformed
        try {
            int rows[] = aliasTable.getSelectedRows();
            modelAlias.setFlag(1);
            modelAlias.removeRows(rows);
            this.checkLumi();
            modelAlias.fireTableDataChanged();
            modelAlias.setFlag(0);
            registerUnsavedAlias();
            Save.setEnabled(true);
            SaveAs.setEnabled(true);
        } catch (SQLException ex) {
            String message = "Error occurred while performing a remove action in the Alias Dialog panel.";
            logger.log(Level.SEVERE, message, ex);
            JOptionPane.showMessageDialog(this, "SQL Error", message
                    + " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_RemoveActionPerformed

    /**
     * Add a new Lumi row to the table.
     *
     * @param evt
     */
    private void AddAboveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddAboveActionPerformed
        try {
            DoAddActionPerformed(false);
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, rootPane, "Error occurred while performing an add action in the Alias Dialog panel.");
        }
    }

    private void DoAddActionPerformed(boolean below) throws SQLException {
        registerUnsavedAlias();
        HashMap<String, Integer> Numbers = new HashMap<>();
        for (int i = 1; i < 11; i++) {
            Numbers.put(i + " Rows", i);
        }
        int numberOfRowsToInsert = Numbers.get(RowNumber.getSelectedItem().toString());
        int insertAt = aliasTable.getSelectedRow();
        if (insertAt == -1) {
            if (below == false) {
                insertAt = 0;
            } else {
                insertAt = modelAlias.getRowCount();
            }
        }
        if (below) {
            insertAt += 1;
        }
        logger.log(Level.FINE, " insert {0} at: {1}", new Object[]{numberOfRowsToInsert, insertAt});
        for (int i = 0; i < numberOfRowsToInsert; i++) {
            //AliasRow row = new AliasRow(this.getCurrentAlias());
            PrescaleSetAliasLumi lumi = new PrescaleSetAliasLumi(interactor.SMT, this.getCurrentAlias().get_alias().get_id(), -1, -1);
            this.getCurrentAlias().add_lumi(lumi);
            this.modelAlias.add(lumi, insertAt);
        }
        checkLumi();
        //fillTable();
        //modelAlias.fireTableDataChanged();
    }//GEN-LAST:event_AddAboveActionPerformed

    private void AddBelowActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddBelowActionPerformed
        try {
            DoAddActionPerformed(true);
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, rootPane, "Error occurred while performing an add action in the Alias Dialog panel.");
        }
    }//GEN-LAST:event_AddBelowActionPerformed

    private void viewTwikiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewTwikiActionPerformed

        ArrayList<ArrayList<Object>> savedPSKs = new ArrayList<>();

        String release = "";
        try {
            release = smt.getReleases().get(0).get_name();
        } catch (SQLException ex) {
            Logger.getLogger(AliasDialog.class.getName()).log(Level.SEVERE, null, ex);
        }

        int rows = modelAlias.getRowCount();
        for (int i = 0; i < rows; i++) {
            ArrayList<Object> tempStore = new ArrayList<>();
            tempStore.add(release);
            tempStore.add(smt.get_id());
            tempStore.add(modelAlias.getL1PS(i).get_id());
            tempStore.add(modelAlias.getHLTPS(i).get_id());
            tempStore.add(modelAlias.getLumiMin(i));
            tempStore.add(modelAlias.getLumiMax(i));
            savedPSKs.add(tempStore);
        }
        AliasTwikiDisplay disp = new AliasTwikiDisplay(AliasSetCombo.getSelectedItem().toString() + ": " + AliasCombo.getSelectedItem().toString());
        disp.drawDisplay(savedPSKs);
    }//GEN-LAST:event_viewTwikiActionPerformed

    private void SaveAsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaveAsActionPerformed

        saveProgressLabel.setVisible(true);
        Save.setEnabled(false);
        SaveAs.setEnabled(false);
        Create.setEnabled(false);
        AddAbove.setEnabled(false);
        AddBelow.setEnabled(false);
        Remove.setEnabled(false);
        Browse.setEnabled(false);
        Close.setEnabled(false);
        viewTwiki.setEnabled(false);
        RowNumber.setEnabled(false);
        aliasTable.setEnabled(false);
        AliasSetCombo.setEnabled(false);
        AliasCombo.setEnabled(false);
        XMLpath.setEnabled(false);
        CommentBox.setEnabled(false);
        Boolean stateSave = XMLLoad.isEnabled();
        XMLLoad.setEnabled(false);

        newAliasName = ((String) JOptionPane.showInputDialog(null, "Specify name for new alias of type " + AliasSetCombo.getSelectedItem().toString()));

        try {
            if (newAliasName != null) {
                ArrayList<ArrayList<Object>> storedPSKs = new ArrayList<>();

                int rows = modelAlias.getRowCount();
                for (int i = 0; i < rows; i++) {
                    ArrayList<Object> temp = new ArrayList<>();
                    temp.add(modelAlias.getL1PS(i));
                    temp.add(modelAlias.getHLTPS(i));
                    temp.add(modelAlias.getLumiMin(i));
                    temp.add(modelAlias.getLumiMax(i));
                    temp.add(modelAlias.getComment(i));
                    storedPSKs.add(temp);
                }

                String commentStore = Comment.getText();
                this.getCurrentAlias().reset_lumis();

                removeUnsavedAlias();
                String newAliasSetName = AliasSetCombo.getSelectedItem().toString();

                PrescaleSetAliasComponent newAlias = new PrescaleSetAliasComponent(-1, interactor.SMT);
                newAlias.set_name(newAliasSetName);
                newAlias.set_name_free(newAliasName);
                interactor.aliasList.add(newAlias);
                interactor.fillAliasNames(newAlias);
                //fillAliasSetCombo();
                AliasSetCombo.setSelectedItem(newAliasSetName);
                AliasCombo.setSelectedItem(newAliasName);
                newAlias.set_comment(commentStore);
                newAlias.save();

                int aliasId = newAlias.get_alias().get_id();
                //loop over the table, try and save the table....
                Boolean errors = false;
                for (int row = 0; row < rows; row++) {

                    PrescaleSetAliasLumi lumi = new PrescaleSetAliasLumi(interactor.SMT, this.getCurrentAlias().get_alias().get_id(), -1, -1);
                    lumi.set_l1_prescale_set((L1Prescale) storedPSKs.get(row).get(0));
                    lumi.set_hlt_prescale_set((HLTPrescaleSet) storedPSKs.get(row).get(1));
                    lumi.set_lum_min((String) storedPSKs.get(row).get(2));
                    lumi.set_lum_max((String) storedPSKs.get(row).get(3));
                    lumi.set_comment((String) storedPSKs.get(row).get(4));
                    this.getCurrentAlias().add_lumi(lumi);
                    this.modelAlias.add(lumi, row);
                    lumi.set_alias_id(aliasId);
                    int res = lumi.save();
                    if (res == -1) {
                        errors = true;
                    }
                }
                if (errors) {
                    JOptionPane.showMessageDialog(null,
                            "One or more saved alias rows were missing a prescale set, alias may be incomplete!'.", "Alias with missing prescales",
                            JOptionPane.WARNING_MESSAGE);
                }

                fillTable();
            }
            if (unsavedAliases.isEmpty()) {
                Save.setEnabled(false);
                SaveAs.setEnabled(false);
            } else {
                Save.setEnabled(true);
                SaveAs.setEnabled(true);
            }

            Create.setEnabled(true);
            AddAbove.setEnabled(true);
            AddBelow.setEnabled(true);
            Remove.setEnabled(true);
            Browse.setEnabled(true);
            Close.setEnabled(true);
            viewTwiki.setEnabled(true);
            RowNumber.setEnabled(true);
            aliasTable.setEnabled(true);
            AliasSetCombo.setEnabled(true);
            AliasCombo.setEnabled(true);
            XMLpath.setEnabled(true);
            CommentBox.setEnabled(true);
            XMLLoad.setEnabled(stateSave);
            saveProgressLabel.setVisible(false);

        } catch (SQLException ex) {
            saveProgressLabel.setVisible(false);
            Logger.getLogger(AliasDialog.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_SaveAsActionPerformed

    /**
     * Check if a folder contains an alias xml exists.
     */
    private boolean checkFile() throws ParserConfigurationException, SAXException, IOException, NullPointerException {
        boolean check;
        try {
            check = interactor.checkFile(XMLpath.getText());
        } catch (IOException | NullPointerException | ParserConfigurationException | SAXException e) { //one of a number of possible parser exceptions
            Message.setText("");
            return false;
        }
        if (check) {
            //maybe dig in and say how many psks this will upload?
            Message.setText("XML is an alias XML. Proceed to load!");
        } else {
            Message.setText("This is not an alias set XML!");
        }
        return check;
    }

    /**
     * Load an alias set form a XML file into the table with the selected set.
     *
     * @return 0 if OK, else 1.
     */
    private void _XMLLoadMouseClicked(ArrayList<String> results) throws SQLException, ParserConfigurationException, SAXException, IOException {
        //if everything looks good fire a refresh of the dialog box.
        if (interactor.loadXML(XMLpath.getText(), results)) {
            reload();
        }
    }

    /**
     *
     */
    public void checkLumi() {
        int rows = modelAlias.getRowCount();
        //Clear the current highlighters
        for (Highlighter h : tableHighlighters.getHighlighters()) {
            tableHighlighters.removeHighlighter(h);
        }
        Comparator<Object> comp = triggertool.Components.TableSorter.LUMI_COMPARATOR;
        for (int i = 0; i < rows; i++) {

            if (modelAlias.getL1PS(i).get_id() == -1) {
                tableHighlighters.addHighlighter(newHighlighter(0, i, AliasRow.COL_L1PS), true);
                aliasTable.setHighlighters(tableHighlighters);
            }
            if (modelAlias.getHLTPS(i).get_id() == -1) {
                tableHighlighters.addHighlighter(newHighlighter(0, i, AliasRow.COL_HLTPS), true);
                aliasTable.setHighlighters(tableHighlighters);
            }

            if (AliasSetCombo.getSelectedItem().toString().equals("PHYSICS")) {
                //
                // Check that Lmax > Lmin
                String lmax = modelAlias.getLumiMax(i);
                String lmin = modelAlias.getLumiMin(i);

                int lumiCompare = comp.compare(lmin, lmax);
                //            if (comp.compare(modelAlias.getValueAt(i, 3), modelAlias.getValueAt(i, 4)) == 1) {
                // if Lmin> Lmax hisghlight lmin
                if (lumiCompare == 1) {
                    tableHighlighters.addHighlighter(newHighlighter(1, i, AliasRow.COL_LMIN), true);
                    aliasTable.setHighlighters(tableHighlighters);
                    // Else if Lmin==Lmax, hightlight both LMIN and LMAX
                } else if (lumiCompare == 0) {
                    tableHighlighters.addHighlighter(newHighlighter(1, i, AliasRow.COL_LMIN), true);
                    tableHighlighters.addHighlighter(newHighlighter(1, i, AliasRow.COL_LMAX), true);
                    aliasTable.setHighlighters(tableHighlighters);
                }
                //             
                // Check with nxt row.
                if (i != 0 && i < rows - 1) {
                    String lmin1 = modelAlias.getLumiMin(i - 1);
                    if (comp.compare(lmax, lmin1) != 0) {
                        tableHighlighters.addHighlighter(newHighlighter(0, i, AliasRow.COL_LMAX), true);
                        tableHighlighters.addHighlighter(newHighlighter(0, i - 1, AliasRow.COL_LMIN), true);
                        aliasTable.setHighlighters(tableHighlighters);
                    }
                }
            } else {
                tableHighlighters.addHighlighter(newHighlighter(2, i, AliasRow.COL_LMIN), true);
                tableHighlighters.addHighlighter(newHighlighter(2, i, AliasRow.COL_LMAX), true);
                aliasTable.setHighlighters(tableHighlighters);
            }

        }
    }

    /**
     *
     * @param bf
     * @param row
     * @param column
     * @return
     */
    public ColorHighlighter newHighlighter(int bf, int row, int column) {
        ColorHighlighter Colour = new ColorHighlighter();
        if (bf == 0) {
            Colour.setBackground(Color.orange);
        } else if (bf == 1) {
            Colour.setBackground(Color.red);
        } else {
            Colour.setForeground(Color.gray);
        }
        Pattern pattern = Pattern.compile("");
        HighlightPredicate predicate = new SearchPredicate(pattern, row, column);
        Colour.setHighlightPredicate(predicate);
        return Colour;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AddAbove;
    private javax.swing.JButton AddBelow;
    private javax.swing.JComboBox<String> AliasCombo;
    private javax.swing.JComboBox<Object> AliasSetCombo;
    private javax.swing.JButton Browse;
    private javax.swing.JButton Close;
    private javax.swing.JLabel Comment;
    private javax.swing.JTextArea CommentBox;
    private javax.swing.JScrollPane CommentScroller;
    private javax.swing.JButton Create;
    private javax.swing.JLabel Message;
    private javax.swing.JButton Remove;
    private javax.swing.JComboBox<String> RowNumber;
    private javax.swing.JButton Save;
    private javax.swing.JButton SaveAs;
    private javax.swing.JScrollPane TableScrollPane;
    private javax.swing.JButton XMLLoad;
    private javax.swing.JTextField XMLpath;
    private triggertool.Components.JTableX aliasTable;
    private javax.swing.JPanel aliasTablePanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel saveProgressLabel;
    private javax.swing.JLabel viewModeWarningLabel;
    private javax.swing.JButton viewTwiki;
    // End of variables declaration//GEN-END:variables
}
