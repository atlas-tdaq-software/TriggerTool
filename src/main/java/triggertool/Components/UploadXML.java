/*
 * UploadXML.java
 *
 * Created on October 28, 2007, 11:41 AM
 */
package triggertool.Components;

import java.awt.HeadlessException;
import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.InternalWriteLock;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.Entities.Topo.TopoMaster;
import triggertool.BackEnd.XMLUploader;
import triggertool.SMT.SuperMasterTableInteractor;
import triggertool.XML.ConfigurationException;
import triggerdb.UploadException;
import triggertool.Panels.MessageDialog;
import triggertool.TTExceptionHandler;

/**
 * Dialog box to upload a configuration. The configuration is a set of XML
 * files. Just enter a L1 XML file to upload L1 only. One can either upload L1
 * XML, or select an L1 Master table and upload the HLT configuration only.
 *
 * @author Simon Head
 */
public class UploadXML extends JDialog {

    /**
     * Message Log
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    /**
     * Required by Java
     */
    private static final long serialVersionUID = 1L;
    /**
     * File chooser
     */
    private final JFileChooser fc = new JFileChooser();
    /**
     * Super Master Table
     */
    private final SuperMasterTable smt = null;
    /**
     * to read xml files on the fly
     */
    private final DocumentBuilderFactory factory;

    private int maxUploadLevel = 2;

    /**
     * Check that enough info to upload the SMT is provided. This requires: -
     * Rel string or Id - L1 file or existing id - Topo file or existing id -
     * HLT files or Existing Id - Online/MC
     *
     * @return <code>true</code> if OK.
     */
    private boolean checkInputData() {
        String errorMsg = "";
        String errorHeader = "";
        boolean inputOK = true;
        //
        //// Check L1
        if (maxUploadLevel >= 1) {
            if (radioUseExistingL1Key.isSelected()) {
                if (getL1Id() < 0) {
                    errorHeader = "L1 Menu";
                    errorMsg = "Please select a valid L1 key.";
                    inputOK = false;
                }
            } else if (textL1Filename.getText() == null || textL1Filename.getText().length() == 0) {
                errorHeader = "L1 Menu";
                errorMsg = "Please provide the L1 menu XML file or select an existing L1 key form the drop-down menu.";
                inputOK = false;
            }
        }

        //// Check Topo
        if (radioUseExistingL1TopoKey.isSelected()) {
            if (getTopoId() < 0) {
                errorHeader = "Topo Menu";
                errorMsg = "Please select a valid Topo key.";
                inputOK = false;
            }
        } else if (textTopoFilename.getText() == null || textTopoFilename.getText().length() == 0) {
            errorHeader = "Topo Menu";
            errorMsg = "Please provide the Topo menu XML file or select an existing Topo key form the drop-down menu.";
            inputOK = false;
        }

        //// Check Release
        if (radioUseExistingRelKey.isSelected()) {
            if (getReleaseId() < 0) {
                inputOK = false;
                errorMsg = "Selected Release key is invalid";
                errorHeader = "Invalid Release selected";
            }
        } else if (textHltRelease.getText() == null || textHltRelease.getText().length() == 0) {
            errorHeader = "Release";
            errorMsg = "Please provide a release number or select an existing one form the drop-down menu.";
            inputOK = false;
        }
        //
        /// Check HLT, at least HLT menu or existing.
        if (maxUploadLevel == 2) {
            if (radioUseExistingHLTKey.isSelected()) {
                if (getHLTid() < 0) {
                    errorHeader = "HLT Menu";
                    errorMsg = "Please select a valid HLT key.";
                    inputOK = false;
                }
            } else if (textHltMenu.getText() == null || textHltMenu.getText().length() == 0) {
                errorHeader = "HLT Menu";
                errorMsg = "Please provide at least a HLT Menu file or select an existing HLT Key form the drop-down menu.";
                inputOK = false;
            }
        }
        
        boolean skip = false;
        //
        //select either mc or online
        if (!jRadioOnline.isSelected() && !jRadioOffline.isSelected()) {
            errorMsg = "Please select at least one of Online or Offline for the configuration type";
            errorHeader = "Configuration type";
            inputOK = false;
        } else if (jRadioOnline.isSelected() && ThreeLevels.isSelected() && !radioUseExistingHLTKey.isSelected() && (textHltSetup.getText() == null || textHltSetup.getText().length() == 0 || textHltSetup.getText().isEmpty())) {
            if (ConnectionManager.getInstance().getInitInfo().getOnlineDB()) {
                errorMsg = "Online HLT configuration in online Database require a HLT setup file";
                errorHeader = "Configuration type";
                inputOK = false;
            } else {
                String msg = "You are attempting to load an online menu without a HLT Setup file"
                        + "\nAre you sure you want to continue and automatically create a dummy setup file?"
                        + "\nSelect YES to continue and NO to go back and change your configuration";
                int saveOpt = JOptionPane.showOptionDialog(this, msg, "proceed and create dummy file", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
                if (saveOpt == JOptionPane.NO_OPTION) {
                    inputOK = false;
                    skip = true;
                } else {
                    inputOK = true;
                }

            }
        }
        if (!inputOK && !skip) {
            JOptionPane.showMessageDialog(this, errorMsg, errorHeader, JOptionPane.ERROR_MESSAGE);
        }
        return inputOK;

    }

    private boolean verifyLevel(File f, final String doctype) {
        if (f == null || f.isDirectory() || !f.getName().endsWith(".xml")) {
            return false;
        }
        try {
            DocumentBuilder parser = factory.newDocumentBuilder();
            Document document = parser.parse(f);
            String top = document.getDocumentElement().getTagName();
            if (!top.equals(doctype)) {
                JOptionPane.showMessageDialog(this,
                        "The XML file \n" + f.getAbsolutePath() + "\nis of the wrong type '" + top + "'\nIt should be of type '" + doctype + "'.", "Error; Wrong XML file",
                        JOptionPane.ERROR_MESSAGE);
                return false;
            }
        } catch (HeadlessException ex) {
            logger.log(Level.SEVERE, "HeadlessException in verifyLevel function", ex);
            TTExceptionHandler.HandleUploadException(new UploadException(ex.getMessage()), rootPane, "Cannot parse L1 Config");
            return false;
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "IOException in verifyLevel function", ex);
            TTExceptionHandler.HandleUploadException(new UploadException(ex.getMessage()), rootPane, "Cannot parse L1 Config");
            return false;
        } catch (ParserConfigurationException ex) {
            logger.log(Level.SEVERE, "ParserConfigurationException in verifyLevel function", ex);
            TTExceptionHandler.HandleUploadException(new UploadException(ex.getMessage()), rootPane, "Cannot parse L1 Config");
            return false;
        } catch (SAXException ex) {
            logger.log(Level.SEVERE, "SAXException in verifyLevel function", ex);
            TTExceptionHandler.HandleUploadException(new UploadException(ex.getMessage()), rootPane, "Cannot parse L1 Config");
            return false;
        }
        return true;
    }

    /**
     * Create the dialog box and draw it to the screen.
     *
     * @param parent Can be null.
     */
    public UploadXML(JFrame parent) {
        super((JDialog)null);

        //FileNameExtensionFilter filter = new FileNameExtensionFilter("XML files", "xml"); // in java 1.6
        XMLExtensionFilter filter = new XMLExtensionFilter();
        fc.setFileFilter(filter);
        fc.setCurrentDirectory(new File("."));
        fc.setAcceptAllFileFilterUsed(false); // without this disabled it is possible to use the 'all files' filter to bypass the XML-only filter


        factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringComments(true);
        factory.setCoalescing(true);
        factory.setNamespaceAware(false);
        factory.setValidating(false);

        initComponents();
        setTitle("XML Upload");

        // fill drop-down menus
        loadNames();
        loadKeysL1();
        loadKeysTopo();
        loadKeysRel();
        loadKeysHLT();

        enableByLevel();

        setLocationRelativeTo(parent);
        setVisible(true);

    }

    /**
     * Function to find out the super master, after the window is closed for
     * example
     *
     * @return The Super Master table.
     */
    public SuperMasterTable getSuperMasterTable() {
        return smt;
    }

    /**
     * If the user want to use an existing L1 key we must ask the database for a
     * list of all the L1 keys that are defined already. Order them by
     * descending ID, since the last menu uploaded is probably more relevant
     * than the first.
     */
    private void loadKeysL1() {
        try {
            String query = "SELECT L1MT_ID, L1MT_NAME, L1MT_VERSION FROM L1_MASTER_TABLE ORDER BY L1MT_ID DESC";
            //String query = (new L1Master(-1)).getQueryString();
            //query += " ORDER BY L1MT_ID DESC";
            query = ConnectionManager.getInstance().fix_schema_name(query);
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                //L1Master master = new L1Master(-1);
                //master.loadFromRset(rset); 
                //comboL1Keys.addItem(master);
                comboL1Keys.addItem(rset.getInt("L1MT_ID") + "   " + rset.getString("L1MT_NAME") + " v" + rset.getString("L1MT_VERSION"));
            }
            rset.close();
            ps.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * If the user want to use an existing Topo key we must ask the database for
     * a list of all the Topo keys that are defined already. Order them by
     * descending ID, since the last menu uploaded is probably more relevant
     * than the first.
     */
    private void loadKeysTopo() {
        try {
            String query = "SELECT TMT_ID, TMT_NAME, TMT_VERSION FROM TOPO_MASTER_TABLE ORDER BY TMT_ID DESC";
            //String query = (new L1Master(-1)).getQueryString();
            //query += " ORDER BY L1MT_ID DESC";
            query = ConnectionManager.getInstance().fix_schema_name(query);
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                comboTopoKeys.addItem(rset.getInt("TMT_ID") + "   " + rset.getString("TMT_NAME") + " v" + rset.getString("TMT_VERSION"));
            }
            rset.close();
            ps.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * If the user want to use an existing rel key we must ask the database for
     * a list of all the rel keys that are defined already.
     */
    private void loadKeysRel() {
        try {
            String query = "SELECT HRE_ID, HRE_NAME FROM HLT_RELEASE ORDER BY HRE_ID DESC";
            //String query = (new HLTRelease(-1)).getQueryString();
            //query += " ORDER BY HRE_ID DESC";
            query = ConnectionManager.getInstance().fix_schema_name(query);
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                //HLTRelease release = new HLTRelease(-1);
                //release.loadFromRset(rset);
                //comboRelKeys.addItem(release);
                comboRelKeys.addItem(rset.getInt("HRE_ID") + "   " + rset.getString("HRE_NAME"));
            }
            rset.close();
            ps.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * If the user want to use an existing HLT key we must ask the database for
     * a list of all the HLT keys that are defined already. Order them by
     * descending ID, since the last menu uploaded is probably more relevant
     * than the first.
     */
    private void loadKeysHLT() {
        try {
            String query = "SELECT HMT_ID, HMT_NAME, HMT_VERSION FROM HLT_MASTER_TABLE ORDER BY HMT_ID DESC";
            query = ConnectionManager.getInstance().fix_schema_name(query);
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                comboHLTKeys.addItem(rset.getInt("HMT_ID") + "   " + rset.getString("HMT_NAME") + " v" + rset.getString("HMT_VERSION"));
            }
            rset.close();
            ps.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * If the user want to use an existing menu name need to populate the combo.
     * Nothing clever here for now, just text. Might want to read from a file?
     */
    private void loadNames() {
        comboNames.removeAllItems();
        String[] menuNames = {
            "Use name from menu ->",
            "Define a new name ->",
            "Physics_pp_v5",
            "MC_pp_v5",
            "LS1_v1",
            "DC14"};
        for (String name : menuNames) {
            comboNames.addItem(name);
        }
        if (comboNames.getItemCount() > 0) {
            comboNames.setSelectedIndex(0);
        }
    }

    /**
     * Handle the event when the L1 radio button is clicked. Set the correct
     * things visible and / or enabled.
     */
    private void radioButtonsL1() {
        boolean useL1Key = radioUseExistingL1Key.isSelected();
        labelL1XML.setEnabled(!useL1Key);
        textL1Filename.setEnabled(!useL1Key);
        buttonBrowseL1.setEnabled(!useL1Key);
        comboL1Keys.setEnabled(useL1Key);
        setMenuNameFromSource();
    }

    /**
     * Handle the event when the L1 radio button is clicked. Set the correct
     * things visible and / or enabled.
     */
    private void radioButtonsTopo() {
        boolean useTopoKey = radioUseExistingL1TopoKey.isSelected();
        labelTopoXML.setEnabled(!useTopoKey);
        textTopoFilename.setEnabled(!useTopoKey);
        buttonBrowseTopo.setEnabled(!useTopoKey);
        comboTopoKeys.setEnabled(useTopoKey);
        setMenuNameFromSource();
    }

    /**
     * read the db id form the L1 combo box.
     *
     * @return L1Master Id or -1;
     */
    private int getL1Id() {
        int l1id = -1;
        if (comboL1Keys.getSelectedIndex() != -1) {
            l1id = Integer.parseInt(comboL1Keys.getSelectedItem().toString().split(" ")[0]);
        }
        return l1id;
    }

    /**
     * read the db id form the Topo combo box.
     *
     * @return TopoMaster Id or -1;
     */
    private int getTopoId() {
        int Topoid = -1;
        if (comboTopoKeys.getSelectedIndex() != -1) {
            Topoid = Integer.parseInt(comboTopoKeys.getSelectedItem().toString().split(" ")[0]);
        }
        return Topoid;
    }

    /**
     * Read the db id of the selected release in the releases combo box.
     *
     * @return the id of the selected release.
     */
    private int getReleaseId() {
        int relid = -1;
        if (comboRelKeys.getSelectedIndex() != -1) {
            relid = Integer.parseInt(comboRelKeys.getSelectedItem().toString().split(" ")[0]);
        }

        return relid;
    }

    /**
     * Read the db id of the HLT Master Table in the HLT combo box.
     *
     * @return the id of the selected HLT Master Table.
     */
    private int getHLTid() {
        int hltId = -1;
        if (comboHLTKeys.getSelectedIndex() != -1) {
            hltId = Integer.parseInt(comboHLTKeys.getSelectedItem().toString().split(" ")[0]);
        }
        return hltId;
    }

    /**
     * Obtain the Name of the super master table. This name is read either from
     * the XML file, from the Text Box or from the list of standard names.
     * Depending upon user's decision.
     *
     * @return The name of the configuration to save.
     */
    private String getSuperMasterName() {
        String supermastername;
        if (comboNames.getSelectedIndex() <= 1) {
            supermastername = textSuperMasterName.getText();
        } else {
            supermastername = comboNames.getSelectedItem().toString();
        }

        if (supermastername.isEmpty()) {
            supermastername = "USEXML";
        }
        return supermastername;
    }

    private void enableByLevel() {

        boolean l1 = maxUploadLevel >= 1;
        boolean hlt = maxUploadLevel == 2;

        textTopoFilename.setEnabled(true);
        buttonBrowseTopo.setEnabled(true);
        radioUseExistingL1TopoKey.setEnabled(true && (comboTopoKeys.getItemCount() > 0));

        comboL1Keys.setEnabled(l1);
        radioUseExistingL1Key.setEnabled(l1 && (comboL1Keys.getItemCount() > 0));
        buttonBrowseL1.setEnabled(l1);
        labelL1XML.setEnabled(l1);
        textL1Filename.setEnabled(l1);
        L1Panel.setEnabled(l1);

        textHltMenu.setEnabled(hlt);
        textHltSetup.setEnabled(hlt);
        buttonBrowseMenu.setEnabled(hlt);
        buttonBrowseHltSetup.setEnabled(hlt);
        comboHLTKeys.setEnabled(hlt);
        radioUseExistingHLTKey.setEnabled(hlt && (comboHLTKeys.getItemCount() > 0));
        HLTPanel.setEnabled(hlt);
        labelHLTMenu.setEnabled(hlt);
        labelHLTSetup.setEnabled(hlt);

        radioUseExistingRelKey.setEnabled(comboRelKeys.getItemCount() > 0);
        textSuperMasterName.setEnabled(true);

        radioButtonsTopo();
        if (l1) {
            radioButtonsL1();
        }
        if (hlt) {
            radioButtonsHLT();
        }
        radioButtonRel();
        this.setMenuNameFromSource();
    }

    private void radioButtonRel() {
        boolean useRelKey = radioUseExistingRelKey.isSelected();
        labelRelease.setEnabled(!useRelKey);
        textHltRelease.setEnabled(!useRelKey);
        comboRelKeys.setEnabled(useRelKey);
    }

    /**
     * Handle the event when the HLT radio button is clicked. Set the correct
     * things visible and / or enabled.
     */
    private void radioButtonsHLT() {
        boolean useHLTKey = radioUseExistingHLTKey.isSelected();
        labelHLTMenu.setEnabled(!useHLTKey);
        textHltMenu.setEnabled(!useHLTKey);
        buttonBrowseMenu.setEnabled(!useHLTKey);
        if(jRadioOffline.isSelected()){
            labelHLTSetup.setEnabled(false);
            textHltSetup.setEnabled(false);
            buttonBrowseHltSetup.setEnabled(false);     
        }
        else{
            labelHLTSetup.setEnabled(!useHLTKey);
            textHltSetup.setEnabled(!useHLTKey);
            buttonBrowseHltSetup.setEnabled(!useHLTKey);     

        }
        comboHLTKeys.setEnabled(useHLTKey);
        setMenuNameFromSource();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LevelSelGroup = new javax.swing.ButtonGroup();
        OneLevel = new javax.swing.JRadioButton();
        TwoLevels = new javax.swing.JRadioButton();
        ThreeLevels = new javax.swing.JRadioButton();
        TopoPanel = new javax.swing.JPanel();
        comboTopoKeys = new javax.swing.JComboBox<>();
        textTopoFilename = new javax.swing.JTextField();
        buttonBrowseTopo = new javax.swing.JButton();
        labelTopoXML = new javax.swing.JLabel();
        radioUseExistingL1TopoKey = new javax.swing.JRadioButton();
        L1Panel = new javax.swing.JPanel();
        comboL1Keys = new javax.swing.JComboBox<>();
        textL1Filename = new javax.swing.JTextField();
        buttonBrowseL1 = new javax.swing.JButton();
        labelL1XML = new javax.swing.JLabel();
        radioUseExistingL1Key = new javax.swing.JRadioButton();
        HLTPanel = new javax.swing.JPanel();
        labelHLTSetup = new javax.swing.JLabel();
        labelHLTMenu = new javax.swing.JLabel();
        textHltMenu = new javax.swing.JTextField();
        textHltSetup = new javax.swing.JTextField();
        buttonBrowseMenu = new javax.swing.JButton();
        buttonBrowseHltSetup = new javax.swing.JButton();
        radioUseExistingHLTKey = new javax.swing.JRadioButton();
        comboHLTKeys = new javax.swing.JComboBox<>();
        ConfigPanel = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        textSuperMasterName = new javax.swing.JTextField();
        jRadioOffline = new javax.swing.JRadioButton();
        jRadioOnline = new javax.swing.JRadioButton();
        jLabel10 = new javax.swing.JLabel();
        comboNames = new javax.swing.JComboBox<>();
        ReleasePanel = new javax.swing.JPanel();
        textHltRelease = new javax.swing.JTextField();
        labelRelease = new javax.swing.JLabel();
        radioUseExistingRelKey = new javax.swing.JRadioButton();
        comboRelKeys = new javax.swing.JComboBox<>();
        buttonUpload = new javax.swing.JButton();
        buttonHelp = new javax.swing.JButton();
        buttonCancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        LevelSelGroup.add(OneLevel);
        OneLevel.setText("Topo only");
        OneLevel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OneLevelActionPerformed(evt);
            }
        });

        LevelSelGroup.add(TwoLevels);
        TwoLevels.setText("Topo & CTP");
        TwoLevels.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TwoLevelsActionPerformed(evt);
            }
        });

        LevelSelGroup.add(ThreeLevels);
        ThreeLevels.setSelected(true);
        ThreeLevels.setText("Topo, CTP & HLT");
        ThreeLevels.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ThreeLevelsActionPerformed(evt);
            }
        });

        TopoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Topo"));
        TopoPanel.setName("Topo"); // NOI18N

        comboTopoKeys.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboTopoKeysActionPerformed(evt);
            }
        });

        buttonBrowseTopo.setText("Browse"); // NOI18N
        buttonBrowseTopo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonBrowseTopoActionPerformed(evt);
            }
        });

        labelTopoXML.setText("Topo xml file");

        radioUseExistingL1TopoKey.setText("Use existing Topo Key");
        radioUseExistingL1TopoKey.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioUseExistingTopoActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout TopoPanelLayout = new org.jdesktop.layout.GroupLayout(TopoPanel);
        TopoPanel.setLayout(TopoPanelLayout);
        TopoPanelLayout.setHorizontalGroup(
            TopoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(TopoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(TopoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(radioUseExistingL1TopoKey)
                    .add(TopoPanelLayout.createSequentialGroup()
                        .add(buttonBrowseTopo)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(labelTopoXML)))
                .add(14, 14, 14)
                .add(TopoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(textTopoFilename, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 645, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(comboTopoKeys, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 300, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        TopoPanelLayout.setVerticalGroup(
            TopoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(TopoPanelLayout.createSequentialGroup()
                .add(TopoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(labelTopoXML)
                    .add(textTopoFilename, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(buttonBrowseTopo))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(TopoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(radioUseExistingL1TopoKey)
                    .add(comboTopoKeys, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        L1Panel.setBorder(javax.swing.BorderFactory.createTitledBorder("L1"));
        L1Panel.setToolTipText("");

        buttonBrowseL1.setText("Browse"); // NOI18N
        buttonBrowseL1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonBrowseL1ActionPerformed(evt);
            }
        });

        labelL1XML.setText("L1 xml file");

        radioUseExistingL1Key.setText("Use existing L1 Key");
        radioUseExistingL1Key.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioUseExistingL1KeyActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout L1PanelLayout = new org.jdesktop.layout.GroupLayout(L1Panel);
        L1Panel.setLayout(L1PanelLayout);
        L1PanelLayout.setHorizontalGroup(
            L1PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(L1PanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(L1PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(L1PanelLayout.createSequentialGroup()
                        .add(buttonBrowseL1)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(labelL1XML)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, L1PanelLayout.createSequentialGroup()
                        .add(radioUseExistingL1Key)
                        .add(30, 30, 30)))
                .add(L1PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(textL1Filename, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 647, Short.MAX_VALUE)
                    .add(L1PanelLayout.createSequentialGroup()
                        .add(comboL1Keys, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 300, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        L1PanelLayout.setVerticalGroup(
            L1PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(L1PanelLayout.createSequentialGroup()
                .add(L1PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(labelL1XML)
                    .add(textL1Filename, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(buttonBrowseL1))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(L1PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(radioUseExistingL1Key)
                    .add(comboL1Keys, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        HLTPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("HLT"));

        labelHLTSetup.setText("HLT Setup"); // NOI18N

        labelHLTMenu.setText("HLT Menu"); // NOI18N

        buttonBrowseMenu.setText("Browse"); // NOI18N
        buttonBrowseMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonBrowseMenuActionPerformed(evt);
            }
        });

        buttonBrowseHltSetup.setText("Browse"); // NOI18N
        buttonBrowseHltSetup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonBrowseHltSetupActionPerformed(evt);
            }
        });

        radioUseExistingHLTKey.setText("Use existing HLT Key");
        radioUseExistingHLTKey.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioUseExistingHLTKeyActionPerformed(evt);
            }
        });

        comboHLTKeys.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboHLTKeysItemStateChanged(evt);
            }
        });

        org.jdesktop.layout.GroupLayout HLTPanelLayout = new org.jdesktop.layout.GroupLayout(HLTPanel);
        HLTPanel.setLayout(HLTPanelLayout);
        HLTPanelLayout.setHorizontalGroup(
            HLTPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(HLTPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(HLTPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(HLTPanelLayout.createSequentialGroup()
                        .add(buttonBrowseHltSetup)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(labelHLTSetup))
                    .add(radioUseExistingHLTKey)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, HLTPanelLayout.createSequentialGroup()
                        .add(buttonBrowseMenu)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(labelHLTMenu)))
                .add(22, 22, 22)
                .add(HLTPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(HLTPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, textHltSetup, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, textHltMenu))
                    .add(comboHLTKeys, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 300, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        HLTPanelLayout.setVerticalGroup(
            HLTPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(HLTPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(HLTPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(labelHLTMenu)
                    .add(textHltMenu, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(buttonBrowseMenu))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(HLTPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(labelHLTSetup)
                    .add(textHltSetup, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(buttonBrowseHltSetup))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(HLTPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(radioUseExistingHLTKey)
                    .add(comboHLTKeys, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        ConfigPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Name and type of configuration"));

        jLabel9.setText("Predefined name"); // NOI18N

        textSuperMasterName.setText("SuperMaster"); // NOI18N

        jRadioOffline.setText("Monte Carlo configuration");
        jRadioOffline.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioOfflineActionPerformed(evt);
            }
        });

        jRadioOnline.setText("Online & Reprocessing configuration");
        jRadioOnline.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioOnlineActionPerformed(evt);
            }
        });

        jLabel10.setText("Name:"); // NOI18N

        comboNames.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboNamesActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout ConfigPanelLayout = new org.jdesktop.layout.GroupLayout(ConfigPanel);
        ConfigPanel.setLayout(ConfigPanelLayout);
        ConfigPanelLayout.setHorizontalGroup(
            ConfigPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(ConfigPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(ConfigPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(ConfigPanelLayout.createSequentialGroup()
                        .add(jRadioOffline)
                        .add(0, 0, Short.MAX_VALUE))
                    .add(ConfigPanelLayout.createSequentialGroup()
                        .add(ConfigPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jRadioOnline)
                            .add(jLabel9))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(comboNames, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 263, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(jLabel10)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(textSuperMasterName)))
                .addContainerGap())
        );
        ConfigPanelLayout.setVerticalGroup(
            ConfigPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(ConfigPanelLayout.createSequentialGroup()
                .add(ConfigPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel9)
                    .add(comboNames, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel10)
                    .add(textSuperMasterName, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jRadioOnline)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jRadioOffline))
        );

        ReleasePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Release"));

        labelRelease.setText("SW Release"); // NOI18N

        radioUseExistingRelKey.setText("Use existing Rel. Key");
        radioUseExistingRelKey.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioUseExistingRelKeyActionPerformed(evt);
            }
        });

        comboRelKeys.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboRelKeysActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout ReleasePanelLayout = new org.jdesktop.layout.GroupLayout(ReleasePanel);
        ReleasePanel.setLayout(ReleasePanelLayout);
        ReleasePanelLayout.setHorizontalGroup(
            ReleasePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, ReleasePanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(ReleasePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, labelRelease)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, radioUseExistingRelKey))
                .add(22, 22, 22)
                .add(ReleasePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(textHltRelease, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 645, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(comboRelKeys, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 300, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        ReleasePanelLayout.setVerticalGroup(
            ReleasePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(ReleasePanelLayout.createSequentialGroup()
                .add(ReleasePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(labelRelease)
                    .add(textHltRelease, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(ReleasePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(radioUseExistingRelKey)
                    .add(comboRelKeys, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        buttonUpload.setText("OK"); // NOI18N
        buttonUpload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonUploadActionPerformed(evt);
            }
        });

        buttonHelp.setText("Help"); // NOI18N
        buttonHelp.setEnabled(false);
        buttonHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonHelpActionPerformed(evt);
            }
        });

        buttonCancel.setText("Cancel"); // NOI18N
        buttonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCancelActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(0, 0, Short.MAX_VALUE)
                        .add(buttonUpload)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(buttonCancel)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(buttonHelp))
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, ConfigPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, ReleasePanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, HLTPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, L1Panel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, TopoPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .add(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .add(layout.createSequentialGroup()
                .add(27, 27, 27)
                .add(OneLevel)
                .add(18, 18, 18)
                .add(TwoLevels)
                .add(18, 18, 18)
                .add(ThreeLevels)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(OneLevel)
                    .add(TwoLevels)
                    .add(ThreeLevels))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(TopoPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(L1Panel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(HLTPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(ReleasePanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(ConfigPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(buttonHelp)
                    .add(buttonCancel)
                    .add(buttonUpload))
                .addContainerGap())
        );

        ConfigPanel.getAccessibleContext().setAccessibleName("Table Names");

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void buttonBrowseL1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonBrowseL1ActionPerformed
        int returnVal = fc.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = fc.getSelectedFile();
            if (!verifyLevel(f, "LVL1Config")) {
                return;
            }
            textL1Filename.setText(f.toString());
            setMenuNameFromSource();
        }
}//GEN-LAST:event_buttonBrowseL1ActionPerformed

    /**
     * Placeholder. Not yet implemented. Probably should pop open the help
     * dialog at some relevant page.
     *
     * @param evt The event that fires this function.
     */
    private void buttonHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonHelpActionPerformed
}//GEN-LAST:event_buttonHelpActionPerformed

    /**
     * Exit this dialog without doing anything.
     *
     * @param evt The event that fires this function.
     */
    private void buttonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCancelActionPerformed
        dispose();
}//GEN-LAST:event_buttonCancelActionPerformed

    private String extractMenuNameFromXML(final String filename, final String[] attrs) {
        String menuname = "";
        try {
            File f = new File(filename);
            if (f.isDirectory() || !f.getName().endsWith(".xml")) {
                return menuname;
            }
            DocumentBuilder parser = factory.newDocumentBuilder();
            Document document = parser.parse(f);
            Element menu = document.getDocumentElement();
            for (String attr : attrs) {
                if (menu.hasAttribute(attr)) {
                    menuname = menu.getAttribute(attr);
                    break;
                }
            }
        } catch (IOException ex) {
            logger.log(Level.INFO, "Exception:{0}", ex);
        } catch (ParserConfigurationException ex) {
            logger.log(Level.INFO, "Exception:{0}", ex);
        } catch (SAXException ex) {
            logger.log(Level.INFO, "Exception:{0}", ex);
        }
        return menuname;
    }

    /**
     * returns menu name based on the name attribute in the specified XML files
     * it takes the name from the HLT menu (xml file or DB) in case it is a
     * level 1 only upload it takes the name from the L1 menu (xml file or DB)
     *
     */
    private void setMenuNameFromSource() {

        if (comboNames.getSelectedIndex() != 0) {
            return;
        }

        String menuname = "";
        if (maxUploadLevel == 0) {
            if (radioUseExistingL1TopoKey.isSelected()) { // use existing key
                int key = Integer.parseInt(comboTopoKeys.getSelectedItem().toString().split(" ")[0]);
                try {
                    menuname = (new TopoMaster(key)).get_name();
                } catch (SQLException ex) {
                    Logger.getLogger(UploadXML.class.getName()).log(Level.SEVERE, null, ex);
                    MessageDialog.showReportDialog(ex);
                }
            } else {
                String topoxml = textTopoFilename.getText();
                if (!topoxml.isEmpty()) {
                    menuname = extractMenuNameFromXML(topoxml, new String[]{"menu_name"});
                }
            }
        } else if (maxUploadLevel == 1) {
            if (radioUseExistingL1Key.isSelected()) {
                //System.out.println(comboL1Keys.getSelectedItem().toString());
                menuname = comboL1Keys.getSelectedItem().toString().split("\\s+")[1];
            } else {
                String l1xml = textL1Filename.getText();
                if (!l1xml.isEmpty()) {
                    menuname = extractMenuNameFromXML(l1xml, new String[]{"name"});
                }
            }
        } else if (radioUseExistingHLTKey.isSelected()) { // use existing key
            int hltid = Integer.parseInt(comboHLTKeys.getSelectedItem().toString().split(" ")[0]);
            try {
                menuname = (new HLTMaster(hltid)).get_name();
            } catch (SQLException ex) {
                Logger.getLogger(UploadXML.class.getName()).log(Level.SEVERE, null, ex);
                MessageDialog.showReportDialog(ex);
            }
        } else {
            String hltxml = textHltMenu.getText();
            if (!hltxml.isEmpty()) {
                menuname = extractMenuNameFromXML(hltxml, new String[]{"menu_name", "name"});
            }
        }

        textSuperMasterName.setText(menuname);
        textSuperMasterName.setEnabled(true);
    }

    /**
     * Perform an upload. If the L1 xml file is not blank we don't upload.
     * Unless we're using an existing key. Similar rules for the HLT upload. It
     * is only attempted if the menu and release fields are filled.
     *
     * @param evt The event that fires this function.
     */
    private void buttonUploadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonUploadActionPerformed
        XMLUploader uploader = new XMLUploader();
        // 
        // 1.- Check that enought info on panel
        //
        if (!this.checkInputData()) {
            return;
        }

        // Read the name of the Super Master Table.
        String supermastername = this.getSuperMasterName();
        uploader.setConfigurationName(supermastername);
        //
        if (radioUseExistingRelKey.isSelected()) {
            uploader.setRelease(getReleaseId());
        } else {
            uploader.setRelease(textHltRelease.getText());
        }
        //
        if (radioUseExistingL1Key.isSelected()) {
            uploader.setL1Id(getL1Id());
        } else if (maxUploadLevel >= 1) {
            uploader.setL1xml(textL1Filename.getText());
        }

        if (radioUseExistingL1TopoKey.isSelected()) {
            uploader.setTopoId(getTopoId());
        } else {
            uploader.setTopoxml(textTopoFilename.getText());
        }

        // HLT
        if (radioUseExistingHLTKey.isSelected()) {
            uploader.setHLTid(getHLTid());
        } else {
            String hltMenu = textHltMenu.getText();
            String hltSetup = textHltSetup.getText();
            if (hltMenu == null) {
                hltMenu = "";
            }
            if (hltSetup == null) {
                hltSetup = "";
            }
       
            uploader.setHLTfiles(hltMenu, hltSetup);
        }
        uploader.setOffline(jRadioOffline.isSelected());
        uploader.setOnline(jRadioOnline.isSelected());

        uploader.setMaxUploadLevel(maxUploadLevel);
        //
        // Do the upload.
        int smtid;
        try {
            smtid = uploader.upload();
            //add comment to the supermaster table
            if (smtid > 0) {
                SuperMasterTableInteractor _smt = new SuperMasterTableInteractor(new SuperMasterTable(smtid));
                _smt.editComment(null);
            } else {
                logger.log(Level.SEVERE, "Error saving trigger configuration (name={0}, id={1})", new Object[]{supermastername, smtid});
            }
        } catch (UploadException | ConfigurationException | SQLException ex) {
            logger.log(Level.SEVERE, null, ex);
            InternalWriteLock writeLock;
            try {
                writeLock = InternalWriteLock.getInstance();
                writeLock.removeLock();
            } catch (SQLException ex1) {
                Logger.getLogger(UploadXML.class.getName()).log(Level.SEVERE, null, ex1);
            }
            MessageDialog.showReportDialog(ex);
        }

        dispose();
    }//GEN-LAST:event_buttonUploadActionPerformed

    private void radioUseExistingL1KeyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioUseExistingL1KeyActionPerformed
        radioButtonsL1();
    }//GEN-LAST:event_radioUseExistingL1KeyActionPerformed

private void radioUseExistingRelKeyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioUseExistingRelKeyActionPerformed
    radioButtonRel();
}//GEN-LAST:event_radioUseExistingRelKeyActionPerformed

private void comboRelKeysActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboRelKeysActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_comboRelKeysActionPerformed

private void comboNamesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboNamesActionPerformed
    textSuperMasterName.setEnabled(comboNames.getSelectedIndex() <= 1); // black
    textSuperMasterName.setEditable(comboNames.getSelectedIndex() == 1); // can edit
    setMenuNameFromSource();
}//GEN-LAST:event_comboNamesActionPerformed

    private void buttonBrowseTopoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonBrowseTopoActionPerformed
        int returnVal = fc.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = fc.getSelectedFile();
            //Need to define a TOPO level confirmation? Or just get the text right?
            if (!verifyLevel(f, "TOPO_MENU")) {
                return;
            }
            textTopoFilename.setText(f.toString());
            setMenuNameFromSource();
        }
    }//GEN-LAST:event_buttonBrowseTopoActionPerformed

    private void radioUseExistingTopoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioUseExistingTopoActionPerformed
        radioButtonsTopo();
    }//GEN-LAST:event_radioUseExistingTopoActionPerformed

    private void ThreeLevelsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ThreeLevelsActionPerformed
        maxUploadLevel = 2;
        enableByLevel();
    }//GEN-LAST:event_ThreeLevelsActionPerformed

    private void OneLevelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OneLevelActionPerformed
        maxUploadLevel = 0;
        enableByLevel();
    }//GEN-LAST:event_OneLevelActionPerformed

    private void TwoLevelsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TwoLevelsActionPerformed
        maxUploadLevel = 1;
        enableByLevel();
    }//GEN-LAST:event_TwoLevelsActionPerformed

    private void comboHLTKeysItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboHLTKeysItemStateChanged
        setMenuNameFromSource();
    }//GEN-LAST:event_comboHLTKeysItemStateChanged

    /**
     * The user wants to switch between reading the L1 Menu from XML and using
     * one that already exists.
     *
     * @param evt The event that casues this to be run.
     */
    private void radioUseExistingHLTKeyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioUseExistingHLTKeyActionPerformed
        radioButtonsHLT();
    }//GEN-LAST:event_radioUseExistingHLTKeyActionPerformed

    /**
     * The user wants to browse for a release XML file. So let them see a file
     * chooser dialog.
     *
     * @param evt The event that fires this function.
     */
    /**
     * The user wants to browse for a L2 setup XML file. So let them see a file
     * chooser dialog.
     *
     * @param evt The event that fires this function.
     */
    private void buttonBrowseHltSetupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonBrowseHltSetupActionPerformed
        int returnVal = fc.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = fc.getSelectedFile();
            if (!verifyLevel(f, "setup")) {
                return;
            }
            textHltSetup.setText(f.toString());
        }
    }//GEN-LAST:event_buttonBrowseHltSetupActionPerformed

    private void buttonBrowseMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonBrowseMenuActionPerformed
        int returnVal = fc.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = fc.getSelectedFile();
            if (!verifyLevel(f, "HLT_MENU")) {
                return;
            }
            textHltMenu.setText(f.toString());
            setMenuNameFromSource();
        }
    }//GEN-LAST:event_buttonBrowseMenuActionPerformed

    private void comboTopoKeysActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboTopoKeysActionPerformed
        setMenuNameFromSource();
    }//GEN-LAST:event_comboTopoKeysActionPerformed

    private void jRadioOnlineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioOnlineActionPerformed
        boolean useHLTKey = radioUseExistingHLTKey.isSelected();
        jRadioOnline.setSelected(true);
        jRadioOffline.setSelected(false);
        if (maxUploadLevel == 2) { //if hlt upload disabled should not enable UI portion
            textHltSetup.setEnabled(!useHLTKey);
            buttonBrowseHltSetup.setEnabled(!useHLTKey);
            labelHLTSetup.setEnabled(!useHLTKey);
        } else {
            textHltSetup.setEnabled(false);
            buttonBrowseHltSetup.setEnabled(false);
            labelHLTSetup.setEnabled(false);
        }
    }//GEN-LAST:event_jRadioOnlineActionPerformed

    private void jRadioOfflineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioOfflineActionPerformed
        jRadioOnline.setSelected(false);
        jRadioOffline.setSelected(true);
        labelHLTSetup.setEnabled(false);
        textHltSetup.setEnabled(false);
        textHltSetup.setText("");
        buttonBrowseHltSetup.setEnabled(false);
    }//GEN-LAST:event_jRadioOfflineActionPerformed

    /**
     * The user wants to switch between reading the L1 Menu from XML and using
     * one that already exists.
     *
     * @param evt The event that causes this to be run.
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel ConfigPanel;
    private javax.swing.JPanel HLTPanel;
    private javax.swing.JPanel L1Panel;
    private javax.swing.ButtonGroup LevelSelGroup;
    private javax.swing.JRadioButton OneLevel;
    private javax.swing.JPanel ReleasePanel;
    private javax.swing.JRadioButton ThreeLevels;
    private javax.swing.JPanel TopoPanel;
    private javax.swing.JRadioButton TwoLevels;
    private javax.swing.JButton buttonBrowseHltSetup;
    private javax.swing.JButton buttonBrowseL1;
    private javax.swing.JButton buttonBrowseMenu;
    private javax.swing.JButton buttonBrowseTopo;
    private javax.swing.JButton buttonCancel;
    private javax.swing.JButton buttonHelp;
    private javax.swing.JButton buttonUpload;
    private javax.swing.JComboBox<String> comboHLTKeys;
    private javax.swing.JComboBox<String> comboL1Keys;
    private javax.swing.JComboBox<String> comboNames;
    private javax.swing.JComboBox<String> comboRelKeys;
    private javax.swing.JComboBox<String> comboTopoKeys;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JRadioButton jRadioOffline;
    private javax.swing.JRadioButton jRadioOnline;
    private javax.swing.JLabel labelHLTMenu;
    private javax.swing.JLabel labelHLTSetup;
    private javax.swing.JLabel labelL1XML;
    private javax.swing.JLabel labelRelease;
    private javax.swing.JLabel labelTopoXML;
    private javax.swing.JRadioButton radioUseExistingHLTKey;
    private javax.swing.JRadioButton radioUseExistingL1Key;
    private javax.swing.JRadioButton radioUseExistingL1TopoKey;
    private javax.swing.JRadioButton radioUseExistingRelKey;
    private javax.swing.JTextField textHltMenu;
    private javax.swing.JTextField textHltRelease;
    private javax.swing.JTextField textHltSetup;
    private javax.swing.JTextField textL1Filename;
    private javax.swing.JTextField textSuperMasterName;
    private javax.swing.JTextField textTopoFilename;
    // End of variables declaration//GEN-END:variables
}
