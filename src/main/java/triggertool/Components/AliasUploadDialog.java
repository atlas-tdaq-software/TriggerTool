/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggertool.Components;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import triggerdb.Entities.alias.PrescaleSetAlias;

/**
 *
 * @author William
 */
public class AliasUploadDialog extends javax.swing.JDialog {

    Boolean status;
    Integer smk;
    
    /**
     * Creates new form AliasUploadDialog
     *
     * @param parent
     * @param modal
     * @param aliasType
     * @param aliasName
     * @param aliasComment
     */
    public AliasUploadDialog(java.awt.Frame parent, boolean modal, String aliasType, String aliasName, String aliasComment, Integer smk) {
        super(parent, modal);
        initComponents();
        AliasTypeCombo.setSelectedItem(aliasType);
        AliasName.setText(aliasName);
        AliasComment.setText(aliasComment);
        setTitle("Review and Confirm Alias Details");
        status = false;
        this.smk = smk;
    }

    public String getAliasType() {
        return AliasTypeCombo.getSelectedItem().toString();
    }

    public String getAliasName() {
        return AliasName.getText();
    }

    public String getAliasComment() {
        return AliasName.getText();
    }

    public ArrayList<Object> showDialog() {
        setVisible(true);
        ArrayList<Object> results = new ArrayList<>();
        results.add(AliasTypeCombo.getSelectedItem().toString());
        results.add(AliasName.getText());
        results.add(AliasComment.getText());
        results.add(status);
        return results;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        AliasCommentLabel = new javax.swing.JLabel();
        MainLabel = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        AliasName = new javax.swing.JTextField();
        AliasTypeLabel = new javax.swing.JLabel();
        AliasNameLabel = new javax.swing.JLabel();
        WarningLabel = new javax.swing.JLabel();
        AliasTypeCombo = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        AliasComment = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        AliasCommentLabel.setText("Comment");

        MainLabel.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        MainLabel.setText("Please select a meaningful alias name (e.g. _BGRPX)");

        jButton1.setText("OK");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        AliasName.setText("jTextField1");

        AliasTypeLabel.setText("Alias Type");

        AliasNameLabel.setText("Alias Name");

        WarningLabel.setText("");

        AliasTypeCombo.setModel(new javax.swing.DefaultComboBoxModel<Object>(AliasTypeRef.types.toArray()));

        AliasComment.setColumns(20);
        AliasComment.setLineWrap(true);
        AliasComment.setRows(5);
        jScrollPane1.setViewportView(AliasComment);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(MainLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(WarningLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jButton1))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(AliasNameLabel)
                                .addComponent(AliasCommentLabel, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(AliasTypeLabel, javax.swing.GroupLayout.Alignment.LEADING))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(AliasTypeCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(AliasName)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(MainLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(AliasTypeLabel)
                    .addComponent(AliasTypeCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(AliasName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(AliasNameLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(AliasCommentLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(WarningLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 436, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 321, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (AliasTypeCombo.getSelectedItem().toString().equals(AliasName.getText()) && AliasName.getText().equals(AliasComment.getText())) {
            WarningLabel.setText("Alias Type, Name and Comment cannot be the same!");
            return;
        }
        PrescaleSetAlias psAlias;
        try {
            psAlias = new PrescaleSetAlias(-1);
            psAlias.set_name(AliasTypeCombo.getSelectedItem().toString());
            psAlias.set_name_free(AliasName.getText());
            psAlias.set_comment(AliasComment.getText());
            psAlias.set_default(false);
            ArrayList<String> psaIds = PrescaleSetAlias.getAliasesForSMK(smk, AliasTypeCombo.getSelectedItem().toString(), AliasName.getText());
            if (!psaIds.isEmpty()) {
                 JOptionPane.showMessageDialog(null, "Selection duplicates existing alias.\nPlease modify name if you wish to save it. ", "Duplicate alias", JOptionPane.WARNING_MESSAGE);
                    return;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AliasUploadDialog.class.getName()).log(Level.SEVERE, null, ex);
        }
        status = true;
        setVisible(false);
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea AliasComment;
    private javax.swing.JLabel AliasCommentLabel;
    private javax.swing.JTextField AliasName;
    private javax.swing.JLabel AliasNameLabel;
    private javax.swing.JComboBox<Object> AliasTypeCombo;
    private javax.swing.JLabel AliasTypeLabel;
    private javax.swing.JLabel MainLabel;
    private javax.swing.JLabel WarningLabel;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
