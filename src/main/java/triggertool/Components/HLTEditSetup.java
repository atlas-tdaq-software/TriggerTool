/*
 * HLTEditSetup.java
 *
 * Created on February 29, 2008, 6:17 PM
 */
package triggertool.Components;

import java.awt.Cursor;
import triggerdb.Connections.ConnectionManager;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;
import javax.swing.AbstractAction;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import triggerdb.Connections.UserMode;
import triggerdb.Entities.HLT.HLTComponent;
import triggerdb.Entities.HLT.HLTParameter;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.HLT.HLTSetup;
import triggerdb.Entities.HLT.HLTTriggerChain;
import triggerdb.Entities.HLT.HLTTriggerElement;
import triggerdb.Entities.HLT.HLTTriggerGroup;
import triggerdb.Entities.HLT.HLTTriggerMenu;
import triggerdb.Entities.HLT.HLTTriggerSignature;
import triggerdb.Entities.HLTLinks.HLTTM_PS;
import triggerdb.Entities.InternalWriteLock;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.UploadException;
import static triggertool.Components.L1ItemMonitoring.logger;
import triggertool.Diff.DiffTree;
import triggertool.Panels.MessageDialog;
import triggertool.TTExceptionHandler;

/**
 * Dialog box to edit the setup tables for the HLT. Doesn't actually save
 * anything. The idea is that this makes all the changes to the in-memory
 * objects and then you save them some other way.
 *
 * @author Paul Bell
 * @author Simon Head
 */
public final class HLTEditSetup extends JDialog
        implements ListSelectionListener, TableModelListener {

    /**
     * List model for the list on the left.
     */
    private final DefaultListModel<HLTComponent> items = new DefaultListModel<>();
    /**
     * The HLTSetup object we're working with.
     */
    private HLTSetup Isetup = null;
    /**
     * Flag for when we insert new data in the table. Since this is because
     * we're loading from the database and not due to the user we use this to
     * disable the methods that record the data.
     */
    private boolean updatingTable = false;
    private boolean diffSelected = false;
    /**
     * unique comp list
     */
    private final List<HLTComponent> allcomps_v = new ArrayList<>();
    /**
     * Set of unique components in memory.
     */
    private final Set<HLTComponent> comp_set = new HashSet<>();
    /**
     * the component we are editing.
     */
    private HLTComponent theComponent = null;
    private UserMode userMode = UserMode.UNKNOWN;
    private int icc;
    private int ic;
    private int mc;
    private int mcc;
    private SuperMasterTable smt;
    private int originalID;
    
    private DiffTree diffTree = null;
    
    InternalWriteLock writeLock = InternalWriteLock.getInstance();

    /**
     * Constructor that also displays the dialog box. Loads a list of all the
     * components from the database.
     *
     * @param parent The parent frame. It's allowed to be null.
     * @param sm The super master table we want to get the menu from.
     * @throws java.sql.SQLException
     */
    public HLTEditSetup(final java.awt.Frame parent, final SuperMasterTable sm) throws SQLException {
        super((JDialog)null);

        smt = new SuperMasterTable(sm.get_id());
        originalID = sm.get_id();
        initComponents();
        setTitle("Edit HLT_Setup for " + smt.get_name() + "/" + smt.get_version());
        jTableX1.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        ParameterPanel.setEnabledAt(1, false);
        ParameterPanel.addChangeListener(new ChangeListener(){
            @Override
            public void stateChanged(ChangeEvent ce) {
                logger.info("Here");
                checkChecked(false);
            }
        });
        this.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent evnt){
                logger.info("There");
                if(diffSelected){
                    checkChecked(true);
                } else {
                    dispose();
                }
            }
        });
        listComponents.setModel(items);
        listComponents.addListSelectionListener(this);

        //check which user mode
        userMode = ConnectionManager.getInstance().getInitInfo().getUserMode();
        if (userMode.ordinal() <= UserMode.SHIFTER.ordinal()) {
            jTableX1.setEnabled(false);
        }

        //vector of comp ids so we don't add more than once
        List<Integer> alreadyadded_v = new ArrayList<>();
        jLabel1.setText("Components for SMK: " + smt.get_name());

        try {
            Isetup = smt.get_hlt_master_table().get_setup();
            AddInfrastructureComponents(alreadyadded_v);
            AddMenuComponents(alreadyadded_v, mc, mcc);
            updateTable();
        } catch (SQLException ex) {
            String message = "Error occurred while creating the HLT Edit panel.";
            logger.log(Level.SEVERE, message, ex);
            MessageDialog.showReportDialog(ex);
        }

        ////System.out.println("Num of icomps " + ic);
        ////System.out.println("Num of icomps children " + icc);
        ////System.out.println("Num of mcomps " + mc);
        ////System.out.println("Num of mcomps children " + mcc);
        ////System.out.println("TOTAL NUMBER OF COMPS " + items.size());
        jLabelMC.setText("Menu components (algorithms): " + mc);
        jLabelMCC.setText("Menu child components (algorithm tools): " + mcc);
        jLabelIC.setText("Infrastructure components: " + ic);
        jLabelICC.setText("Infrastructure child components: " + icc);
        buttonDone.setEnabled(false);
        
        setLocationRelativeTo(parent);
        setVisible(true);
    }

    private void AddInfrastructureComponents(List<Integer> alreadyadded_v) throws SQLException {
        Boolean alreadyadded;
        for (HLTComponent comp : Isetup.getInfraStructureComponents()) {

            //add if not already done so - in fact, the Icompts should already be unique, though
            //maybe not their children
            alreadyadded = false;
            for (Integer alreadyadded_id : alreadyadded_v) {
                if (alreadyadded_id == comp.get_id()) {
                    alreadyadded = true;
                }
            }
            if (!alreadyadded) {
                comp.set_list_mode(true);
                items.addElement(comp);
                allcomps_v.add(comp);
                ic++;
                alreadyadded_v.add(comp.get_id());
                AddInfrastuctureComponentInChildren(comp, alreadyadded_v, icc);
            }
        }
    }

    private void AddInfrastuctureComponentInChildren(HLTComponent comp, List<Integer> alreadyadded_v, Integer icc) throws SQLException {
        Boolean alreadyadded;
        //now add cp-cp linked children - recursive
        for (HLTComponent childcomp : comp.getAllChildComponents()) {
            //add if not already done so
            alreadyadded = false;
            for (Integer alreadyadded_id : alreadyadded_v) {
                if (alreadyadded_id == childcomp.get_id()) {
                    alreadyadded = true;
                }
            }
            if (!alreadyadded) {
                childcomp.set_list_mode(true);
                items.addElement(childcomp);
                allcomps_v.add(childcomp);
                alreadyadded_v.add(childcomp.get_id());
                icc++;
            }
        }
    }

    private void AddMenuComponents(List<Integer> alreadyadded_v, Integer mc, Integer mcc) throws SQLException {
        //get all menu components
        HLTTriggerMenu menu = smt.get_hlt_master_table().get_menu();
        for (HLTTriggerChain chain : menu.getChains()) {
            for (HLTTriggerSignature sig : chain.getSignatures()) {
                for (HLTTriggerElement ele : sig.getElements()) {
                    addAlgos(ele, alreadyadded_v, mc, mcc);
                }
            }
        }
    }

    ///special routine for saving children of children for components

    /**
     *
     * @param comp
     * @return
     * @throws SQLException
     */
    public static boolean save_recursive(HLTComponent comp) throws SQLException {

        boolean foundnew = false;
        //get all children ids and if one is negative then save it
        //or if id of this is negative, save it
        if (comp.get_id() == -1 || comp.getAllChildComponentIDs().contains(-1)) {
            ////System.out.println("modified comp found " + comp.get_alias());
            foundnew = true;

            //parameters
            for (HLTParameter para : comp.getParameters()) {
                para.set_id(-1);
                para.save();
            }

            //save the children
            for (HLTComponent childcomp : comp.getChildComponents()) {
                boolean foundnew2 = save_recursive(childcomp);
                if (foundnew2 && foundnew == false) {
                    foundnew = true;
                }
            }

            comp.set_id(-1);//since if only child is -1 this needs a force save
            ////System.out.println("going to save comp " + comp.get_alias());
            comp.save();
        }
        return foundnew;
    }

    private void saveSetup() throws SQLException, UploadException {
        writeLock.setLock("Algorithm parameter editing.");
        //Make the spinning icon....
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        //save infrastructure compts - menu comps anyway saved with menu in the end
        TreeMap<String, HLTComponent> newComponent = new TreeMap<>();
        HLTSetup setup = smt.get_hlt_master_table().get_setup();
        for (HLTComponent component : setup.getInfraStructureComponents()) {
            //logger.info("Component "+component.toString());
            boolean newComp = save_recursive(component);
            if (newComp) {
                newComponent.put(component.get_name() + ":" + component.get_alias(), component);
            }
        }
        //menu compts - this also forces a new menu, with new chain etc!
        boolean newcomp = false;
        boolean newlink = false;
        //logger.info(viewInteractor.hltMenu.getChains().toString());
        for (HLTTriggerChain chain : smt.get_hlt_master_table().get_menu().getChains()) {
            chain.forceLoad();
            for (HLTTriggerSignature signature : chain.getSignatures()) {
                for (HLTTriggerElement element : signature.getElements()) {
                    ArrayList<HLTComponent> remove = new ArrayList<>();
                    for (HLTComponent algo : element.getAlgorithms()) {
                        boolean newcomponent = save_recursive(algo);
                        if (newcomponent) {
                            newcomp = true;
                        }
                        String compare = algo.get_name() + ":" + algo.get_alias();
                        if (newComponent.containsKey(compare)) {
                            newlink = true;
                            newComponent.get(compare).set_algorithm_counter(algo.get_algorithm_counter());
                            remove.add(algo);
                        }
                    }
                    if (newlink) {
                        ArrayList<HLTComponent> links = element.getAlgorithms();
                        for (HLTComponent removeThis : remove) {
                            //logger.info("This link needs to be changed " +removeThis.toString());
                            links.remove(removeThis);
                        }
                        links.addAll(newComponent.values());
                        element.setAlgorithms(links);
                    }
                    if (newcomp || newlink) {
                        element.getInputElements(); //make sure input TE are properly loaded
                        ArrayList<HLTComponent> links = element.getAlgorithms();

                        //logger.info("Setting ele id to -1 for " + element.get_name());
                        element.set_id(-1);
                        element.set_hash();
                        element.save();
                    }
                }
                if (newcomp || newlink) {
                    //logger.info("Setting sig id to -1 for " + signature.get_name());
                    signature.set_id(-1);
                    ArrayList<Integer> hashes = new ArrayList<>();
                    hashes.add(signature.hashCode());
                    signature.set_hashfull(hashes);
                    signature.save();
                    
                }
            }
            if (newcomp || newlink) {
                /// Make sure links are correctly set before setting id to -1
                chain.getSignatures();
                chain.getStreams();
                chain.getTriggerTypes();
                chain.getTriggerGroups();
                for(HLTTriggerGroup group : chain.getTriggerGroups()){
                    group.set_trigger_chain_id(-1);
                    group.set_id(-1);
                }
                //logger.info("Setting chain id to -1 for " + chain.get_name());
                chain.set_id(-1);
                chain.save();
            }
            //reset checks
            newcomp = false;
            newlink = false;
        }
        ////System.out.println("Setting menu id to -1 for " + super_master.get_hlt_master_table().get_menu().get_name());
        ArrayList<HLTPrescaleSet> setsToLink = smt.get_hlt_master_table().get_menu().getPrescaleSets();
        smt.get_hlt_master_table().get_menu().set_id(-1);

        setup.set_id(-1);
        smt.get_hlt_master_table().set_setup_id(setup.save());
        smt.get_hlt_master_table().set_menu_id(smt.get_hlt_master_table().get_menu().save());
        for (HLTPrescaleSet PSSToSave : setsToLink) {
            HLTTM_PS newpslink = new HLTTM_PS();
            newpslink.set_menu_id(smt.get_hlt_master_table().get_menu().get_id());
            newpslink.set_prescale_set_id(PSSToSave.get_id());
            newpslink.save();
        }
        smt.get_hlt_master_table().set_id(-1);
        smt.get_hlt_master_table().set_id(smt.get_hlt_master_table().save());

        //save the smt and the prescales here:
        smt.set_hlt_master_table_id(smt.get_hlt_master_table().get_id());
        ArrayList<HLTRelease> savedReleases = smt.getReleases();
        smt.set_origin("evolution of " + originalID);
        smt.set_id(-1);
        smt.save();
        smt.save_releases(savedReleases);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        writeLock.removeLock();
        
        if (smt.get_id() != originalID) {
            //Make the spinning icon....
            setCursor(new Cursor(Cursor.WAIT_CURSOR));
            //Enable the tab
            ParameterPanel.setEnabledAt(1, false);
            //Do the diff
            diffTree = new DiffTree();
            SuperMasterTable originalSMT = new SuperMasterTable(originalID);
            diffTree.update(smt,originalSMT);
            jScrollPaneDiff.setViewportView(diffTree);
            expandAll();
            //Flick to the tab
            ParameterPanel.setSelectedIndex(1);
            jScrollPaneDiff.setEnabled(true);
            
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            originalID = smt.get_id();
        }
        jScrollPaneDiff.setEnabled(false);
        buttonDone.setEnabled(false);
    }

    private void checkChecked(boolean closing) {
        if(diffSelected){
            int Confirm;
            Object[] options = {"Yes, continue", "No, let me check"};

            Confirm = JOptionPane.showOptionDialog(null,
                    "Were the changes in the diff as expected? \nDo you want to continue?",
                    "Are you sure?",
                    JOptionPane.YES_NO_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    options,
                    options[1]);
            if (Confirm == 0 && closing) {
                this.dispose();
            }else if (Confirm == 0 && !closing) {
                ParameterPanel.setSelectedIndex(0);
            } else if (Confirm == 1) {
                diffSelected = false;
                ParameterPanel.setSelectedIndex(1);
            }
        }
        if(ParameterPanel.getSelectedIndex() == 0){
            diffSelected = false;
        } else {
            diffSelected = true;
        }
    }
    
    private void expandAll() {   
        for(int i=0; i<diffTree.getRowCount(); ++i) {
            diffTree.expandRow(i);
      }
      
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonDone = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jSplitPane1 = new javax.swing.JSplitPane();
        ParameterPanel = new javax.swing.JTabbedPane();
        jScrollPaneParam = new javax.swing.JScrollPane();
        jTableX1 = new triggertool.Components.JTableX();
        jScrollPaneDiff = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        textFilter = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listComponents = new javax.swing.JList<HLTComponent>();
        jLabelMC = new javax.swing.JLabel();
        jLabelIC = new javax.swing.JLabel();
        jLabelICC = new javax.swing.JLabel();
        jLabelMCC = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        buttonDone.setText("Save+Diff"); // NOI18N
        buttonDone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDoneActionPerformed(evt);
            }
        });

        jLabel1.setText("HLT Setup"); // NOI18N

        jSplitPane1.setBorder(null);
        jSplitPane1.setDividerLocation(300);

        jTableX1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Parameter", "Value"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableX1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableX1MouseClicked(evt);
            }
        });
        jTableX1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTableX1FocusGained(evt);
            }
        });
        jScrollPaneParam.setViewportView(jTableX1);

        ParameterPanel.addTab("Parameter", jScrollPaneParam);
        ParameterPanel.addTab("Diff", jScrollPaneDiff);

        jSplitPane1.setRightComponent(ParameterPanel);
        ParameterPanel.getAccessibleContext().setAccessibleName("Parameters");

        textFilter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                textFilterKeyPressed(evt);
            }
        });

        jLabel2.setText("Filter:"); // NOI18N

        jScrollPane1.setViewportView(listComponents);

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel2)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(textFilter, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel2)
                    .add(textFilter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE))
        );

        jSplitPane1.setLeftComponent(jPanel1);

        jLabelMC.setText("Menu Components"); // NOI18N

        jLabelIC.setText("Infrastructure Components"); // NOI18N

        jLabelICC.setText("Infrastructure Components Children"); // NOI18N

        jLabelMCC.setText("Menu Components Children"); // NOI18N

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(0, 0, Short.MAX_VALUE)
                .add(buttonDone))
            .add(jSplitPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 972, Short.MAX_VALUE)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 282, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(jLabelMC, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jLabelMCC, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE))
                .add(16, 16, 16)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jLabelIC, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
                    .add(jLabelICC, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabelMC, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jLabelIC, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(1, 1, 1)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabelMCC, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jLabelICC, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(layout.createSequentialGroup()
                        .add(11, 11, 11)
                        .add(jLabel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jSplitPane1)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(buttonDone))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void buttonDoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDoneActionPerformed
        try {
            saveSetup();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, rootPane, "Cannot save HLT setup.");

        } catch (UploadException ex) {
            TTExceptionHandler.HandleUploadException(ex, rootPane, "Error in locking the DB");
        }
    }//GEN-LAST:event_buttonDoneActionPerformed

    /**
     * The filter box can be used to narrow the components that are shown. When
     * a key is pressed we loop over all the components and check for a name or
     * alias that contains the current text.
     *
     * @param evt The key pressed event.
     */
private void textFilterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textFilterKeyPressed

    items.clear();

    for (HLTComponent comp : allcomps_v) {
        if (comp.get_name().toLowerCase().contains(textFilter.getText().toLowerCase()) || comp.get_alias().toLowerCase().contains(textFilter.getText().toLowerCase())) {
            comp.set_list_mode(true);
            items.addElement(comp);
        }
    }
}//GEN-LAST:event_textFilterKeyPressed

private void jTableX1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableX1MouseClicked

        //Open an extra window for editing long lists
        final int row = jTableX1.rowAtPoint(evt.getPoint());
        if (row >= 0) {
            ActionOnValidRowSelected(row, evt);
        }

    }
//GEN-LAST:event_jTableX1MouseClicked

    private void jTableX1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTableX1FocusGained
        buttonDone.setEnabled(true);
    }//GEN-LAST:event_jTableX1FocusGained

    private void ActionOnValidRowSelected(final int row, MouseEvent evt) {
        jTableX1.changeSelection(row, 1, false, false);

        theComponent = (HLTComponent) listComponents.getSelectedValue();

        final DefaultTableModel mod = (DefaultTableModel) jTableX1.getModel();

        final String value = mod.getValueAt(row, 1).toString();
        final String name = mod.getValueAt(row, 0).toString();

        if (value.startsWith("[") && value.endsWith("]") && value.contains(",")) {

            //option to open a custom text editor window
            JPopupMenu menu = new JPopupMenu();
            AbstractAction edit = new AbstractAction("View list parameter") {

                private static final long serialVersionUID = 1L;

                @Override
                public void actionPerformed(ActionEvent evt) {
                    //InputDialog id = new InputDialog(value);

                    InputDialog id = new InputDialog(value, "Edit Parameter", InputDialog.Panel.HLTPARAM, null);
                    String newval = id.getNewval();
                    //now update the table here
                    //unnecessary, can't edit anyway!
                    if (newval != null && userMode.ordinal() > UserMode.SHIFTER.ordinal()) {
                        try {
                            for (HLTParameter parameter : theComponent.getParameters()) {

                                if (parameter.get_name().equals(name)) {
                                    parameter.set_value(newval);
                                    theComponent.set_id(-1);
                                }
                            }
                            updateTable();
                        } catch (SQLException ex) {
                            String message = "Error occurred while an action was performed on table " + jTableX1.getName() + " in HLTEdit panel.";
                            logger.log(Level.SEVERE, message, ex);
                            JOptionPane.showMessageDialog(jTableX1, "SQL Error", message
                                    + " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
            };
            menu.add(edit);
            menu.show(evt.getComponent(), evt.getX(), evt.getY());
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane ParameterPanel;
    private javax.swing.JButton buttonDone;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabelIC;
    private javax.swing.JLabel jLabelICC;
    private javax.swing.JLabel jLabelMC;
    private javax.swing.JLabel jLabelMCC;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPaneDiff;
    private javax.swing.JScrollPane jScrollPaneParam;
    private javax.swing.JSplitPane jSplitPane1;
    private triggertool.Components.JTableX jTableX1;
    private javax.swing.JList<HLTComponent> listComponents;
    private javax.swing.JTextField textFilter;
    // End of variables declaration//GEN-END:variables

    /**
     * When the user clicks on the list on the left, show the properties of this
     * component in the table on the right. Note that for the Message level, or
     * when a value is True or False we create a drop down box with the allowed
     * values in only.
     *
     * @param e The list event
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource() == listComponents) {
            if (e.getValueIsAdjusting()) {
                return;
            }
            try {
                updateTable();
            } catch (SQLException ex) {
                String message = "Error occurred while retrieving updating table after a value was changed in HLTEdit panel.";
                logger.log(Level.SEVERE, message, ex);
                JOptionPane.showMessageDialog(this, "SQL Error", message
                        + " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     *
     * @throws SQLException
     */
    public void updateTable() throws SQLException {

        updatingTable = true;

        jTableX1.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

        theComponent = (HLTComponent) listComponents.getSelectedValue();

        DefaultTableModel mod = (DefaultTableModel) jTableX1.getModel();
        mod.addTableModelListener(this);
        mod.setNumRows(0);

        int row = 0;

        if (theComponent != null) {

            for (HLTParameter pa : theComponent.getParameters()) {

                //System.out.println(pa + " value: " + pa.get_value());
                List<String> rowData = new ArrayList<>();
                rowData.add(pa.get_name());

                List<String> values = new ArrayList<>();
                if (pa.get_name().equals("OutputLevel")) {
                    values.add("1 VERBOSE");
                    values.add("2 DEBUG");
                    values.add("3 INFO");
                    values.add("4 WARNING");
                    values.add("5 ERROR");
                    values.add("6 FATAL");
                    values.add("7 UNKNOWN");
                    if (Integer.parseInt(pa.get_value()) < 1 || Integer.parseInt(pa.get_value()) > 6) {
                        rowData.add(values.get(6));
                    } else {
                        rowData.add(values.get(Integer.parseInt(pa.get_value()) - 1));
                    }
                } else {
                    rowData.add(pa.get_value());
                }

                if (pa.get_value().equals("True") || pa.get_value().equals("False")) {
                    values.add("True");
                    values.add("False");
                }

                if (values.size() > 0) {
                    JComboBox<String> cb = new JComboBox<>(values.toArray(new String[0]));
                    DefaultCellEditor ed = new DefaultCellEditor(cb);

                    jTableX1.getRowEditorModel().addEditorForRow(row, ed, 0);

                    //System.out.println("PJB adding combo for " + pa.get_name());
                } else {
                    jTableX1.getRowEditorModel().removeEditorForRow(row);
                }

                mod.addRow(rowData.toArray());

                ++row;
            }
        }

        updatingTable = false;

    }

    /**
     * When the user changes the data in the table we need to put it back in the
     * parameter that this row belongs to.
     *
     * @param e The list event
     */
    @Override
    public void tableChanged(TableModelEvent e) {
        if (!updatingTable && e.getFirstRow() >= 0 && e.getColumn() > 0) {
            DefaultTableModel mod = (DefaultTableModel) jTableX1.getModel();
            //Object data = mod.getValueAt(e.getFirstRow(), e.getColumn());

            theComponent = (HLTComponent) listComponents.getSelectedValue();
            try {
                for (HLTParameter parameter : theComponent.getParameters()) {
                    if (parameter.get_name().equals(mod.getValueAt(e.getFirstRow(), 0))) {

                        //set id to -1 so we know to save
                        theComponent.set_id(-1);
                        String value = (String) mod.getValueAt(e.getFirstRow(), e.getColumn());
                        switch (value) {
                            case "1 VERBOSE":
                                value = "1";
                                break;
                            case "2 DEBUG":
                                value = "2";
                                break;
                            case "3 INFO":
                                value = "3";
                                break;
                            case "4 WARNING":
                                value = "4";
                                break;
                            case "5 ERROR":
                                value = "5";
                                break;
                            case "6 FATAL":
                                value = "6";
                                break;
                            case "7 UNKNOWN":
                                value = "7";
                                break;
                        }

                        parameter.set_value(value);
                        //System.out.println("New data is " + value.toString());
                    }
                }
            } catch (SQLException ex) {
                String message = "Error occurred while retrieving HLTParameter in table changed event in HLTEdit panel.";
                logger.log(Level.SEVERE, message, ex);
                JOptionPane.showMessageDialog(this, "SQL Error", message
                        + " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * This functino is used to loop over the algorithms for the Trigger
     * elements and put them in the table. It also makes sure that for repeated
     * components the instance in the table is the instance linked from all the
     * relevant Trigger Elements (by 'swapping' components).
     *
     * @param ele
     * @param alreadyadded_v
     * @param mc
     * @param mcc
     */
    private void addAlgos(HLTTriggerElement ele, List<Integer> alreadyadded_v, Integer mc, Integer mcc) throws SQLException {
        List<HLTComponent> comps_to_swap_te = new ArrayList<>();
        for (HLTComponent comp : ele.getAlgorithms()) {
            boolean alreadyadded = false;
            for (Integer alreadyadded_id : alreadyadded_v) {
                if (alreadyadded_id == comp.get_id()) {
                    alreadyadded = true;
                }
            }
            /// Add this component to our set
            if (!comp_set.add(comp)) {
                comps_to_swap_te.add(comp);
            }

            if (!alreadyadded) {
                comp.set_list_mode(true);
                items.addElement(comp);
                allcomps_v.add(comp);
                mc++;
                alreadyadded_v.add(comp.get_id());
                //now add cp-cp linked children
                //for (HLTComponent childcomp : comp.getChildComponents()) {
                for (HLTComponent childcomp : comp.getAllChildComponents()) {
                    //add if not already done so
                    alreadyadded = false;
                    for (Integer alreadyadded_id : alreadyadded_v) {
                        if (alreadyadded_id == childcomp.get_id()) {
                            alreadyadded = true;
                        }
                    }
                    if (!alreadyadded) {
                        ////System.out.println("------- adding M-child " + childcomp.get_alias());
                        childcomp.set_list_mode(true);
                        ////System.out.println("------- before adding M-child " + items.size());
                        items.addElement(childcomp);
                        allcomps_v.add(childcomp);
                        mcc++;
                        ////System.out.println("------- after  adding M-child " + items.size());
                        alreadyadded_v.add(childcomp.get_id());
                    }
                }
            }
        }// loop over comps in algos

        /// Now do the swapping
        for (HLTComponent comp : comps_to_swap_te) {

            HLTComponent comp_for_replace = null;
            for (HLTComponent testcomp : comp_set) {
                if (testcomp.equals(comp)) {
                    comp_for_replace = testcomp;
                }
            }

            if (comp_for_replace == null) {
                //System.out.println("ERROR: Did not find a component to swap, skipping");
            } else {
                ele.getAlgorithms().remove(comp);
                ele.getAlgorithms().add(comp_for_replace);
                ////System.out.println("Swapping component " + comp.get_alias());
            }
        }

    }
}
