package triggertool.Components;


import java.util.Hashtable;
import java.util.Vector;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.jdesktop.swingx.JXTable;
import triggerdb.Entities.HLT.HLTComponent;

///Table that allows different editors in rows
/**
 * Expand the normal JTable to allow different editors in different rows. Is
 * used in the parameter editing, where the parameters are given as rows. I
 * think the code is based on a website somewhere, but can't remember where.
 * 
 * @author Simon Head
 */
public class JTableX extends JXTable {
        private int flag;

    /**
     *
     */
    public class RowEditorModel {

        private Hashtable<Integer, TableCellEditor> data;

            /**
             *
             */
            public RowEditorModel() {
            data = new Hashtable<>();
        }
//Flag is to distiguish between usage in HLTEditSetup/HLTComponent and L1Monitoring

            /**
             *
             * @param row
             * @param e
             * @param Flag
             */
        public void addEditorForRow(int row, TableCellEditor e, int Flag) {
            data.put(row, e);
            flag = Flag;
        }

            /**
             *
             * @param row
             */
            public void removeEditorForRow(int row) {
            data.remove(row);
        }

            /**
             *
             * @param row
             * @return
             */
            public TableCellEditor getEditor(int row) {
            return (TableCellEditor) data.get(row);
        }
    }

    /**
     *
     */
    protected RowEditorModel rm;
    private HLTComponent component;

    /**
     *
     */
    public JTableX() {
        super();
        setRowEditorModel(new RowEditorModel());
    }

    /**
     *
     * @param tm
     */
    public JTableX(TableModel tm) {
        super(tm);
        setRowEditorModel(new RowEditorModel());
    }

    /**
     *
     * @param tm
     * @param cm
     */
    public JTableX(TableModel tm, TableColumnModel cm) {
        super(tm, cm);
        setRowEditorModel(new RowEditorModel());
    }

    /**
     *
     * @param tm
     * @param cm
     * @param sm
     */
    public JTableX(TableModel tm, TableColumnModel cm, ListSelectionModel sm) {
        super(tm, cm, sm);
        setRowEditorModel(new RowEditorModel());
    }

    /**
     *
     * @param rows
     * @param cols
     */
    public JTableX(int rows, int cols) {
        super(rows, cols);
        setRowEditorModel(new RowEditorModel());
    }

    /**
     *
     * @param rowData
     * @param columnNames
     */
    public JTableX(final Vector rowData, final Vector columnNames) {
        super(rowData, columnNames);
        setRowEditorModel(new RowEditorModel());
    }

    /**
     *
     * @param rowData
     * @param colNames
     */
    public JTableX(final Object[][] rowData, final Object[] colNames) {
        super(rowData, colNames);
        setRowEditorModel(new RowEditorModel());
    }

    /**
     *
     * @param tm
     * @param rm
     */
    public JTableX(TableModel tm, RowEditorModel rm) {
        super(tm, null, null);
        this.rm = rm;
    }

    /**
     *
     * @param rm
     */
    public void setRowEditorModel(RowEditorModel rm) {
        this.rm = rm;
    }

    /**
     *
     * @return
     */
    public RowEditorModel getRowEditorModel() {
        return rm;
    }

    @Override
    public TableCellEditor getCellEditor(int row, int col) {
        TableCellEditor tmpEditor = null;
            //HLTEditSetup/HLTComponent use
            switch (flag) {
                case 0:
                    if (rm != null) {
                        tmpEditor = rm.getEditor(row);
                    }
                    if (col > 0 && tmpEditor != null) {
                        return tmpEditor;
                    }
                    return super.getCellEditor(row, col);
                    //L1monitoring use
                case 1:
                    if (rm != null) {
                        tmpEditor = rm.getEditor(row);
                    }
                    if (col == 1 && tmpEditor != null) {
                        return tmpEditor;
                    }
                    if (col == 3){
                        Vector<String> InMon = new Vector<>();
                        InMon.add("CTPIN");
                        InMon.add("CTPMON");
                        JComboBox<String> cb = new JComboBox<>(InMon);
                        DefaultCellEditor ed = new DefaultCellEditor(cb);
                        tmpEditor = ed;
                        return tmpEditor;
                    }
                    return super.getCellEditor(row, col);
                    //Usage for Alias Dialog
                default:
                    return super.getCellEditor(row, col);
            }
    }

    /**
     *
     * @param inp
     */
    public void set_component(HLTComponent inp) {
        component = inp;
    }

    /**
     *
     * @return
     */
    public HLTComponent get_component() {
        return component;
    }
}
