/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package triggertool.Components;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.HLT.HLTTriggerMenu;
import triggerdb.Entities.L1.L1Menu;
import triggerdb.Entities.L1.L1Prescale;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.PrescaleSetAliasComponent;
import triggerdb.PrescaleSetAliasLumi;

/**
 * Container of a row in the Alias table. It contains the prescale key, Name of
 * the psk, lumi min and lumi max.
 *
 * @author tiago
 * @author Alex
 */
public final class AliasRow {

    //
    // COLUMN INDEX CONSTANTS
    /**
     * COL_L1PS
     */
    public static final int COL_L1PS = 0;
    /**
     * COL_HLTPS
     */
    public static final int COL_HLTPS = 1;
    /**
     * COL_HLTPS
     */
    public static final int COL_COMMENT = 2;
    /**
     * lumi min
     */
    public static final int COL_LMIN = 3;
    /**
     * Lumi max
     */
    public static final int COL_LMAX = 4;
    // Data fields (Columns) of one row.
    private PrescaleSetAliasLumi lumi;

    /**
     * Empty constructor.
     */
    public AliasRow() {
        this.lumi = new PrescaleSetAliasLumi();               
        this.lumi.set_comment("New");
        this.lumi.set_lum_max("0.0e32");
        this.lumi.set_lum_min("0.0e32");        
    }

    /**
     * Construct an empty row from a PrescaleSetAlias component.
     * @param alias
     * @throws java.sql.SQLException
     */
    public AliasRow(final PrescaleSetAliasComponent alias) throws SQLException {
        this.lumi = new PrescaleSetAliasLumi();               
        this.lumi.set_comment("New");
        this.lumi.set_lum_max("0.0e32");
        this.lumi.set_lum_min("0.0e32");
        this.lumi.set_alias(alias);
        SuperMasterTable smt = alias.get_super_master_table();
        L1Menu l1Menu = smt.get_l1_master_table().get_menu();
        HLTTriggerMenu hMenu = smt.get_hlt_master_table().get_menu();
        this.lumi.get_l1_prescale_set_alias().set_menu(l1Menu);
        this.lumi.get_hlt_prescale_set_alias().set_menu(hMenu);    
    }

    /**
     * New AliasRow with the values from the given PrescaleSetAliasLumi object
     * @param _lumi 
     */
    public AliasRow(final PrescaleSetAliasLumi _lumi){
        this.setRow(_lumi);
    }
    
    /**
     * Creates and return a Vector of objects to easily set the values in the
     * Alias table model
     *
     * @return a vector containing all the values in the right order needed by
     * the AliasTableModel.
     * @throws java.sql.SQLException
     */
    public List<Object> getDataVector() throws SQLException {
        List<Object> dataVector = new ArrayList<>();
        dataVector.add(lumi.get_l1_prescale_set_alias().get_prescale_set());
        dataVector.add(lumi.get_hlt_prescale_set_alias().get_prescale_set());
        dataVector.add(lumi.get_comment());
        dataVector.add(lumi.get_lum_min());
        dataVector.add(lumi.get_lum_max());
        return dataVector;
    }

    /**
     * Method access the data by "column" number.
     *
     * @param idx the col number.
     * @return the object at column idx.
     * @throws java.sql.SQLException
     */
    public Object get(final int idx) throws SQLException {
        switch (idx) {
            case COL_L1PS:
                return this.getL1PS();
            case COL_HLTPS:
                return this.getHLTPS();
            case COL_COMMENT:
                return this.getComment();
            case COL_LMIN:
                return this.getLumiMin();
            case COL_LMAX:
                return this.getLumiMax();
            default:
                return null;
        }
    }

    /**
     * Method set the data by "column" number.
     *
     * @param obj the value to set
     * @param idx the col number
     * @throws java.sql.SQLException
     */
    public void setElementAt(final Object obj, final int idx) throws SQLException {
        switch (idx) {
            case COL_L1PS:
                if (obj instanceof L1Prescale) {
                    this.setL1PSK((L1Prescale) obj);
                } else {
                    int id;
                    if (obj instanceof Integer) {
                        id = (Integer) obj;
                    } else {
                        id = Integer.parseInt(obj.toString());
                    }
                    this.setL1PSK(id);
                }
                break;
            case COL_HLTPS:
                if (obj instanceof HLTPrescaleSet) {
                    this.setHLTPSK((HLTPrescaleSet) obj);
                } else {
                    int id;
                    if (obj instanceof Integer) {
                        id = (Integer) obj;
                    } else {
                        id = Integer.parseInt(obj.toString());
                    }
                    this.setHLTPSK(id);
                }
                break;
            case COL_COMMENT:
                this.setComment(obj.toString());
                break;
            case COL_LMIN:
                this.setLumiMin(obj.toString());
                break;
            case COL_LMAX:
                this.setLumiMax(obj.toString());
                break;
            default:
                break;
        }
    }

    /**
     * Get the label for the L1 Prescale Column.
     *
     * @return the PSK.
     * @throws java.sql.SQLException
     */
    public String getL1PSKLabel() throws SQLException {
        L1Prescale l1ps = this.lumi.get_l1_prescale_set_alias().get_prescale_set();
        return l1ps.get_id() + ": " + l1ps.get_name();
    }

    /**
     * Get the label for the L1 Prescale Column.
     *
     * @return the PSK.
     * @throws java.sql.SQLException
     */
    public L1Prescale getL1PS() throws SQLException {
        L1Prescale l1ps = this.lumi.get_l1_prescale_set_alias().get_prescale_set();
        return l1ps;
    }

    /**
     * Set the PSK.
     *
     * @param newPSK the new PSK.
     */
    public void setL1PSK(final L1Prescale newPSK) {
        this.lumi.set_l1_prescale_set(newPSK);

    }

    /**
     *
     * @param newPSK
     * @throws SQLException
     */
    public void setL1PSK(final Integer newPSK) throws SQLException {
        this.lumi.set_l1_prescale_set(new L1Prescale(newPSK));
    }

    /**
     * Get the HLT Prescale Set.
     *
     * @return the hlt prescale set.
     * @throws java.sql.SQLException
     */
    public HLTPrescaleSet getHLTPS() throws SQLException {
        HLTPrescaleSet ps = this.lumi.get_hlt_prescale_set_alias().get_prescale_set();
        return ps;
    }

    /**
     * Get the HLT Prescale Set.
     *
     * @return the hlt prescale set.
     * @throws java.sql.SQLException
     */
    public String getHLTPSLabel() throws SQLException {
        HLTPrescaleSet ps = this.getHLTPS();
        return ps.get_id() + ": " + ps.get_name();
    }

    /**
     * Set the HLT Prescale Set.
     *
     * @param hltPS the new HLT PS.
     */
    public void setHLTPSK(final HLTPrescaleSet hltPS) {
        this.lumi.set_hlt_prescale_set(hltPS);
    }

    /**
     * Set the HLT Prescale Set from the PSK.
     *
     * @param hltPSK the HLT PSK.
     * @throws java.sql.SQLException
     */
    public void setHLTPSK(final Integer hltPSK) throws SQLException {
        this.lumi.set_hlt_prescale_set(new HLTPrescaleSet(hltPSK));
    }

    /**
     * Get the name of this PSK.
     *
     * @return the name of this PSK.
     */
    public String getComment() {
        return this.lumi.get_comment();
    }

    /**
     * Set the name of this PSK.
     *
     * @param newComment the name of this PSK.
     */
    public void setComment(final String newComment) {
        this.lumi.set_comment(newComment);
    }

    /**
     * Get the Lumi min value.
     *
     * @return the Lumi min value.
     */
    public String getLumiMin() {
        return this.lumi.get_lum_min();
    }

    /**
     * Set the Lumi min value.
     *
     * @param LMin the new Lumi min value.
     */
    public void setLumiMin(final String LMin) {
        this.lumi.set_lum_min(LMin);
    }

    /**
     * Get the Lumi Max value.
     *
     * @return the Lumi max value.
     */
    public String getLumiMax() {
        return this.lumi.get_lum_max();
    }

    /**
     * Set the Lumi max value.
     *
     * @param LMax the new Lumi max value.
     */
    public void setLumiMax(final String LMax) {
        this.lumi.set_lum_max(LMax);
    }

    /**
     *
     * @return
     */
    public PrescaleSetAliasLumi getLumiObject() {
        return this.lumi;
    }

    /**
     *
     * @param l
     */
    public void setLumi(final PrescaleSetAliasLumi l) {
        this.lumi = l;
    }

    /**
     *
     * @param l
     */
    public void setRow(final PrescaleSetAliasLumi l) {
        this.setLumi(l);
    }
}