package triggertool;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 *
 * Reads the version number from the file "resources.version"
 * @author Tiago Perez -- 20.04.2010, Adam Bozson -- 06.02.2017
 */
public class ReadTriggerToolVersion {

    private static final String BUNDLE_NAME = "resources.version"; //$NON-NLS-1$
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    

    private ReadTriggerToolVersion() {
    }

    /**
     *
     * @param key
     * @return
     */
    public static String getString(String key) {
        String version = "NOT SET";
        try {
            version = RESOURCE_BUNDLE.getString(key);
        } catch (MissingResourceException e) {
            version = '!' + key + '!';
        } catch (Exception e) {
            logger.severe(" problems reading version information. Version set to \" NOT SET\" ");
        }
        return version;
    }
}
