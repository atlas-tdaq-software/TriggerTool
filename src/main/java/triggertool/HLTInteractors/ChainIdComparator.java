/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.HLTInteractors;

import java.util.Comparator;
import triggerdb.Entities.HLT.HLTTriggerChain;

/**
 *
 * @author Michele
 */
public class ChainIdComparator implements Comparator<HLTTriggerChain> {
    @Override
    public int compare(HLTTriggerChain chain1, HLTTriggerChain chain2) {
        return chain1.get_id()- chain2.get_id();
    }
}