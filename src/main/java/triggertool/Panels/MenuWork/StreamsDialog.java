/*
 * NewPrescaleEdit.java
 *
 * Created on September 9 , 2008, 9:15 AM
 */
package triggertool.Panels.MenuWork;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.JFrame;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.HLT.HLTPrescale;
import triggertool.Components.TableSorter;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.HLT.HLTPrescaleType;
import triggerdb.Entities.HLT.HLTTriggerChain;
import triggerdb.Entities.HLT.HLTTriggerStream;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggertool.TTExceptionHandler;

/**
 * @author Paul Bell
 */
public final class StreamsDialog extends JDialog implements ListSelectionListener {

    private HLTTriggerStream theStream;
    private int hlt_menu_id = -1;
    private int l1_menu_id = -1;
    private int CurrentStreamID = 0;
    //private int mode = -1;
    private HLTPrescaleSet hlt_psset = null;

    private DefaultListModel<HLTTriggerStream> StreamItems = new DefaultListModel<>();

    private ArrayList<HLTTriggerChain> chains_v;

    private ArrayList<L1Item> items_v;

    private DefaultTableModel ChainsModel = new DefaultTableModel();
    private DefaultTableModel ItemsModel = new DefaultTableModel();

    //trick so we know which was last selected
    HashMap<String, String> lowerchainnames_hs = null;
    String lowerchainnames = null;
    ArrayList<L1Item> matcheditems_v = null;
    Boolean updatingStreams = false;

    /**
     * Required by Java
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");

    /**
     * Creates new form PrescaleEdit
     *
     * @param smt
     * @throws java.sql.SQLException
     */
    public StreamsDialog(SuperMasterTable smt, JFrame parent) throws SQLException {
        super((JDialog)null);    
        try {
            hlt_menu_id = smt.get_hlt_master_table().get_menu_id();
            l1_menu_id = smt.get_l1_master_table().get_menu_id();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, rootPane, "Error while creating the Steam Dialog panel.");
        }
        //check which mode
        //mode = ConnectionManager.getInstance().getInitInfo().getMode();



        //draw the dialog, get PS sets
        initComponents();
        setTitle("Streams: SMT " + smt);        
        getSets();
        //find the streams; get all streams by default (Enabled Streams = false)
        getStreams(false);


        //add list listener and set table model 
        listTablesStreams.addListSelectionListener(this);
        ChainsModel = (DefaultTableModel) tableChains.getModel();
        TableSorter sorter1 = new TableSorter(tableChains.getModel());
        tableChains.setModel(sorter1);
        JTableHeader header1 = tableChains.getTableHeader();
        sorter1.setTableHeader(header1);
        tableChains.setColumnSelectionAllowed(false);
        tableChains.setRowSelectionAllowed(true);

        tableChains.getColumnModel().getColumn(0).setPreferredWidth(400);
        tableChains.getColumnModel().getColumn(1).setPreferredWidth(100);
        tableChains.getColumnModel().getColumn(2).setPreferredWidth(100);
        tableChains.getColumnModel().getColumn(3).setPreferredWidth(100);
        tableChains.getColumnModel().getColumn(4).setPreferredWidth(100);
        tableChains.getColumnModel().getColumn(5).setPreferredWidth(100);

        ItemsModel = (DefaultTableModel) tableItems.getModel();
        TableSorter sorter2 = new TableSorter(tableItems.getModel());
        tableItems.setModel(sorter2);
        JTableHeader header2 = tableItems.getTableHeader();
        sorter2.setTableHeader(header2);
        
        setLocationRelativeTo(parent);
        setVisible(true);
    }

    //load combo box of hlt prescale sets to apply

    /**
     *
     */
    public void getSets() {

        PSComboBox.removeAllItems();

        //get HLT prescales
        String query = "SELECT HPS_ID, HPS_NAME, HPS_VERSION, HTM2PS_PRESCALE_SET_ID, HTM2PS_TRIGGER_MENU_ID "
                + "FROM "
                + "HLT_PRESCALE_SET, HLT_TM_TO_PS "
                + "WHERE "
                + "HTM2PS_TRIGGER_MENU_ID=? "
                + "AND "
                + "HTM2PS_PRESCALE_SET_ID=HPS_ID "
                + "ORDER BY HPS_ID DESC";

        query = ConnectionManager.getInstance().fix_schema_name(query);

        try {
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ps.setInt(1, hlt_menu_id);
            ResultSet rset = ps.executeQuery();
            //PSComboBox.addItem("No set selected");
            while (rset.next()) {
                PSComboBox.addItem(rset.getInt("HPS_ID") + "   " + rset.getString("HPS_NAME") + " v" + rset.getString("HPS_VERSION"));
            }
            rset.close();
            ps.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Exception: {0}", ex);
        }
    }

    /**
     * When the user clicks on a stream in the list show the chains
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {

        ////////////////////////////////////////////////////////////////////////
        //clicked on list of streams
        if (e.getSource() == listTablesStreams) {
            
            //if getStreams in progress, ignore any internal streams list selections made 
            if(updatingStreams){
                return;
            }
            if (e.getValueIsAdjusting()) {
                return;
            }
            //get the position in the list of the most recently selected stream
            theStream = (HLTTriggerStream) listTablesStreams.getSelectedValue();
            if (theStream != null) {
                CurrentStreamID = listTablesStreams.getSelectedIndex();
            }
            //if nothing currently selected, select the most recently selected stream 
            //(defaults to first in list if nothing ever selected)
            if (theStream == null) {
                listTablesStreams.setSelectedIndex(CurrentStreamID);
                theStream = (HLTTriggerStream) listTablesStreams.getSelectedValue();
            }
            logger.log(Level.INFO, "Selected stream: {0}", theStream);
            if (theStream != null) {
                try {
                    getChains(theStream, EnabledFilterCheckBox.isSelected());
                    getItems(theStream, EnabledFilterCheckBox.isSelected());
                } catch (SQLException ex) {
                    TTExceptionHandler.HandleSqlException(ex, rootPane, "Error while changing value: ");
                }
            }
        }    
    }
    
    /**
     * find all the streams of this supermaster table, and fill in the list
     * table. TODO: This should go to HLTTriggerMenu. TODO: This should return
     * an list of ID's or HLTTriggerStream's
     * @param EnabledStreamsOnly
     * @throws java.sql.SQLException
     */
    public void getStreams(Boolean EnabledStreamsOnly) throws SQLException {
        
        int prescale_id = Integer.parseInt(PSComboBox.getSelectedItem().toString().split(" ")[0]);
        String query = "SELECT DISTINCT "
                + "HTC2TR_TRIGGER_STREAM_ID "
                + "FROM "
                + "HLT_TC_TO_TR, HLT_TM_TO_TC, HLT_TRIGGER_CHAIN, HLT_PRESCALE "
                + "WHERE "
                + "HTM2TC_TRIGGER_MENU_ID=? AND "
                + "HTM2TC_TRIGGER_CHAIN_ID=HTC2TR_TRIGGER_CHAIN_ID AND "
                + "HTM2TC_TRIGGER_CHAIN_ID=HTC_ID AND "
                + "HPR_CHAIN_COUNTER=HTC_CHAIN_COUNTER AND "
                + "HPR_PRESCALE_SET_ID=? AND "
                + "HPR_TYPE='Prescale' ";
        //filter to streams with chain prescales greater than zero if the EnabledStreams checkbox is selected
        if (EnabledStreamsOnly == true){
            query += "AND HPR_VALUE>0 ";
        }
        query +=  "ORDER BY "
                + "HTC2TR_TRIGGER_STREAM_ID";

        query = ConnectionManager.getInstance().fix_schema_name(query);

        try {
            //start updating streams list
            updatingStreams = true;
            logger.log(Level.INFO, "Fetching streams");
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ps.setInt(1, hlt_menu_id);
            ps.setInt(2, prescale_id);
            ResultSet rset = ps.executeQuery();
            //set currently selected stream from Streams scroll pane
            theStream = (HLTTriggerStream) listTablesStreams.getSelectedValue();
            //if theStream is not null, store its current scroll pane list index
            if (theStream != null) {
                CurrentStreamID = listTablesStreams.getSelectedIndex();
            }
            //clear and repopulate streams list using current query
            StreamItems.clear();
            while (rset.next()) {
                HLTTriggerStream stream = new HLTTriggerStream(rset.getInt("HTC2TR_TRIGGER_STREAM_ID"));
                StreamItems.addElement(stream);
            }
            rset.close();
            ps.close();
     
            //done updating streams list
            updatingStreams = false;
            listTablesStreams.setModel(StreamItems);
            //if new list no longer contains previously selected stream, have no stream in the new list selected
            //if it does, and the selected stream isn't null, set the selection to be the same stream, even if it's moved in the list
            if (!StreamItems.contains(theStream)) {
                listTablesStreams.setSelectedIndex(-1);
            } else if (theStream != null) {
                listTablesStreams.setSelectedIndex(StreamItems.indexOf(theStream));
                theStream = (HLTTriggerStream) listTablesStreams.getSelectedValue();
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Exception: {0}", ex);
        }
    }

    //find all the chains of the selected streams of this supermaster table, and fill in the list table

    /**
     *
     * @param selectedStream
     * @param EnabledStreamsOnly
     * @throws SQLException
     */

    public void getChains(HLTTriggerStream selectedStream, boolean EnabledStreamsOnly) throws SQLException {

        //this method should be streamlined for faster loading at the front end;
        //simple lightweight structure with only basic variables necessary for human use (e.g. Name, ID) needed;
        //only once selected load the bulk of the data for that chain
            
        chains_v = new ArrayList<>();

        lowerchainnames = new String();
        lowerchainnames_hs = new HashMap<>();

        int prescale_id = Integer.parseInt(PSComboBox.getSelectedItem().toString().split(" ")[0]);
        hlt_psset = new HLTPrescaleSet(prescale_id);

        chains_v = selectedStream.getChains(hlt_menu_id);

        //draw table
        ChainsModel.setRowCount(0);
        
        logger.log(Level.INFO, "Getting chains");

        //loop over the chains 
        for (int i = 0; i < chains_v.size(); i++) {

            ArrayList<Object> tabledata_v = new ArrayList<>();
            //chain name
            tabledata_v.add(chains_v.get(i).get_name());
            //chain counter
            tabledata_v.add(chains_v.get(i).get_chain_counter());
            //prescale for the stream
            double streamprescale = -1;
            String condition = "";
            double chainprescale = -1;
            double chainpassthrough = -1;
            for (HLTPrescale p : hlt_psset.GetPrescalesForChain(chains_v.get(i).get_chain_counter())) {
                if (p.get_type() == HLTPrescaleType.Stream) {
                    streamprescale = p.get_value();
                    condition = p.get_condition();
                }
                if (p.get_type() == HLTPrescaleType.Prescale) {
                    chainprescale = p.get_value();
                }
                if (p.get_type() == HLTPrescaleType.Pass_Through) {
                    chainpassthrough = p.get_value();
                }
            }
            //if Enabled Streams checkbox selected, only display chains whose PS > 0
            if (EnabledStreamsOnly == true) {
                if(chainprescale > 0) {
            
                    tabledata_v.add(streamprescale);
                    tabledata_v.add(condition);
                    tabledata_v.add(chainprescale);
                    tabledata_v.add(chainpassthrough);

                   ChainsModel.addRow(tabledata_v.toArray());
            
                   lowerchainnames = chains_v.get(i).get_lower_chain_name();
                   lowerchainnames_hs.put(chains_v.get(i).get_name(), lowerchainnames);
                }
            }
            else {
                tabledata_v.add(streamprescale);
                tabledata_v.add(condition);
                tabledata_v.add(chainprescale);
                tabledata_v.add(chainpassthrough);

                ChainsModel.addRow(tabledata_v.toArray());
            
                lowerchainnames = chains_v.get(i).get_lower_chain_name();
                lowerchainnames_hs.put(chains_v.get(i).get_name(), lowerchainnames);
            }
        }
        logger.log(Level.INFO, "Finished getting chains");
    }


    ///get all items for the stream

    /**
     *
     * @param selectedStream
     * @param EnabledStreamsOnly
     * @throws SQLException
     */
    public void getItems(HLTTriggerStream selectedStream, boolean EnabledStreamsOnly) throws SQLException {

        //at this point, we already have all the chains in chains_v
        //so need to get all L1 items, and see if LCN matches
        items_v = new ArrayList<>();

        String query = "SELECT "
                + "L1TI_ID, L1TM2TI_TRIGGER_ITEM_ID, L1TM2TI_TRIGGER_MENU_ID "
                + "FROM "
                + "L1_TRIGGER_ITEM, L1_TM_TO_TI "
                + "WHERE "
                + "L1TM2TI_TRIGGER_MENU_ID =? "
                + "AND "
                + "L1TM2TI_TRIGGER_ITEM_ID = L1TI_ID "
                + "ORDER BY L1TI_ID ASC";

        query = ConnectionManager.getInstance().fix_schema_name(query);

        //System.out.println("Items query " + query);
        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, l1_menu_id);
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            L1Item item = new L1Item(rset.getInt("L1TI_ID"));
            items_v.add(item);
        }
        rset.close();
        ps.close();

        logger.log(Level.INFO, "Matching items");
        matcheditems_v = new ArrayList<>();
        ArrayList<Integer> alreadygotitems_v = new ArrayList<>();
        ////System.out.println("items  " + items_v.size());
        ////System.out.println("chains " + chains_v.size());

        //now have all items, but need to see if name matches LCN
        //if Enabled Streams checkbox selected, only display chains whose PS > 0
        if (EnabledStreamsOnly == true){
            for (HLTTriggerChain chain : chains_v) {
                for (HLTPrescale p : hlt_psset.GetPrescalesForChain(chain.get_chain_counter())) {
                    if (p.get_type() == HLTPrescaleType.Prescale) {
                        double chainprescale = p.get_value();
                        if (chainprescale > 0) {
                            for (L1Item item : items_v) {
                                boolean matched = false;
                                String lower_chains[] = chain.get_lower_chain_name().split(",");
                                for (String lower_chain : lower_chains) {
                                    if (lower_chain.trim().equals(item.get_name())) {
                                        matched = true;
                                    }
                                }
                                if (matched) {
                                    boolean alreadyhave = false;
                                    for (int alreadygotitem_id : alreadygotitems_v) {
                                        if (item.get_id() == alreadygotitem_id) {
                                            alreadyhave = true;
                                            break;
                                        }
                                    }
                                    if (!alreadyhave) {
                                    alreadygotitems_v.add(item.get_id());
                                    matcheditems_v.add(item);
                                    break; //end the chain loop and go to next item
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            for (L1Item item : items_v) {
                for (HLTTriggerChain chain : chains_v) {                 
                    boolean matched = false;
                    String lower_chains[] = chain.get_lower_chain_name().split(",");
                    for (String lower_chain : lower_chains) {
                        if (lower_chain.trim().equals(item.get_name())) {
                            matched = true;
                        }
                    }
                    if (matched) {
                        boolean alreadyhave = false;
                        for (int alreadygotitem_id : alreadygotitems_v) {
                            if (item.get_id() == alreadygotitem_id) {
                                alreadyhave = true;
                                break;
                            }
                        }
                        if (!alreadyhave) {
                            alreadygotitems_v.add(item.get_id());
                            matcheditems_v.add(item);
                            break; //end the chain loop and go to next item
                        }
                    }
                }
            }
        }

        logger.log(Level.INFO, "Getting items list");
        //draw table - using matched items
        ItemsModel.setRowCount(0);

        //loop over the matched items
        for (int i = 0; i < matcheditems_v.size(); i++) {

            ArrayList<Object> tabledata_v = new ArrayList<>();
            //item name only
            tabledata_v.add(matcheditems_v.get(i).get_name());
            

            ItemsModel.addRow(tabledata_v.toArray());
           
        }
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        listTablesStreams = new javax.swing.JList<>();
        jScrollPane3 = new javax.swing.JScrollPane();
        tableChains = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        tableItems = new javax.swing.JTable();
        DoneButton = new javax.swing.JButton();
        PSComboBox = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        EnabledFilterCheckBox = new javax.swing.JCheckBox();

        jButton2.setText("jButton2");

        jButton3.setText("jButton3");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1234, 422));

        listTablesStreams.setModel(StreamItems);
        jScrollPane6.setViewportView(listTablesStreams);

        tableChains.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Chain Name", "Counter", "Stream PS", "Condition", "Chain PS", "Chain PT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableChains.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableChainsMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tableChains);

        tableItems.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Seed"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableItems.setColumnSelectionAllowed(true);
        jScrollPane4.setViewportView(tableItems);

        DoneButton.setText("Done");
        DoneButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DoneButtonActionPerformed(evt);
            }
        });

        PSComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PSComboBoxActionPerformed(evt);
            }
        });

        jLabel2.setText("HLT Prescale set:");

        EnabledFilterCheckBox.setText("Enabled Streams/Chains Only");
        EnabledFilterCheckBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                EnabledFilterCheckBoxMouseClicked(evt);
            }
        });
        EnabledFilterCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EnabledFilterCheckBoxActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createSequentialGroup()
                                .add(jLabel2)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(PSComboBox, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                                .add(jScrollPane6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 369, Short.MAX_VALUE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jScrollPane3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 675, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(10, 10, 10)
                        .add(jScrollPane4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 150, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(EnabledFilterCheckBox)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(DoneButton)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel2)
                    .add(PSComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jScrollPane6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 348, Short.MAX_VALUE)
                    .add(jScrollPane3, 0, 0, Short.MAX_VALUE)
                    .add(jScrollPane4, 0, 0, Short.MAX_VALUE))
                .add(1, 1, 1)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(DoneButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 27, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(EnabledFilterCheckBox))
                .add(4, 4, 4))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void tableChainsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableChainsMouseClicked

    int row = tableChains.rowAtPoint(evt.getPoint());
    String what = (String) tableChains.getValueAt(row, 0);

    String lcn = lowerchainnames_hs.get(what);

    //System.out.println("Clicked the chains table row " + row + " for chain with lcn " + lcn);
    //For case of L2 chains, refine the items column so its just for the selected chain.
    //have list of matched items already, throw away those not for this
    ArrayList<String> supermatcheditems_v = new ArrayList<>();
    for (L1Item matcheditem : matcheditems_v) {
        if (matcheditem.get_name().equals(lcn)) {
            supermatcheditems_v.add(matcheditem.get_name());
        }
    }

    //For ef chains, find the L2 chain
    for (HLTTriggerChain chain : chains_v) {
        if (chain.get_name().equals(lcn)) {
            supermatcheditems_v.add(chain.get_name());
        }
    }
    
    logger.log(Level.INFO, "Selected chain: {0}", what);
    //redraw the table
    ItemsModel.setRowCount(0);

    //loop over the matched items
    for (int i = 0; i < supermatcheditems_v.size(); i++) {
        ArrayList<Object> tabledata_v = new ArrayList<>();
        //item name only
        tabledata_v.add(supermatcheditems_v.get(i));
        ItemsModel.addRow(tabledata_v.toArray());
    }

}//GEN-LAST:event_tableChainsMouseClicked

private void DoneButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DoneButtonActionPerformed
    dispose();
}//GEN-LAST:event_DoneButtonActionPerformed

private void PSComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PSComboBoxActionPerformed
    //see what prescale set we selected and apply to the table
    //System.out.println("selected ps set " + PSComboBox.getSelectedItem().toString());
    //look through the chains and add the ps infoEnEnabledFilterCheckBoxrCheckBox{
    try{    
        //redraw the items list
        ItemsModel.setRowCount(0);
        logger.log(Level.INFO, "Selected prescale set: {0}", PSComboBox.getSelectedItem());
        getStreams(EnabledFilterCheckBox.isSelected());
        //failsafe to avoid null pointer when getting chains
        if (theStream != null) {
            getChains(theStream, EnabledFilterCheckBox.isSelected());
        }
        
    } catch (SQLException ex) {
        TTExceptionHandler.HandleSqlException(ex, rootPane, "Error while getting chains/streams for combo boxes: ");
    }

}//GEN-LAST:event_PSComboBoxActionPerformed

    private void EnabledFilterCheckBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_EnabledFilterCheckBoxMouseClicked
        try {
            //redraw the items list
            logger.log(Level.INFO, "Enabled Streams Only set to {0}", EnabledFilterCheckBox.isSelected());
            ItemsModel.setRowCount(0);
            getStreams(EnabledFilterCheckBox.isSelected());
            //failsafe to avoid null pointer when getting chains
            if (theStream != null) {
                getChains(theStream, EnabledFilterCheckBox.isSelected());
                getItems(theStream, EnabledFilterCheckBox.isSelected());
            }
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, rootPane, "Error while getting chains/streams for combo boxes: ");
        }
    }//GEN-LAST:event_EnabledFilterCheckBoxMouseClicked

    private void EnabledFilterCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EnabledFilterCheckBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EnabledFilterCheckBoxActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton DoneButton;
    private javax.swing.JCheckBox EnabledFilterCheckBox;
    private javax.swing.JComboBox<String> PSComboBox;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JList<HLTTriggerStream> listTablesStreams;
    private javax.swing.JTable tableChains;
    private javax.swing.JTable tableItems;
    // End of variables declaration//GEN-END:variables

}
