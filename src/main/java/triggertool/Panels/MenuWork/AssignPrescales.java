package triggertool.Panels.MenuWork;

import java.awt.Dimension;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JFrame;
import triggerdb.Connections.ConnectionManager;
import static triggerdb.Connections.ConnectionManager.executeSelect;
import triggerdb.Entities.HLTLinks.HLTTM_PS;
import triggerdb.Entities.HLT.HLTPrescale;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.HLT.HLTPrescaleType;
import triggerdb.Entities.HLT.HLTTriggerChain;
import triggerdb.Entities.HLT.HLTTriggerMenu;
import triggerdb.Entities.L1.L1Menu;
import triggerdb.Entities.L1.L1Prescale;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggertool.TTExceptionHandler;

/**
 * List all L1 and HLT PrescaleSets present in DB and let them assign to the
 * SuperMasterTable given in the constructor.
 *
 * @author Paul Bell
 */
public final class AssignPrescales extends JDialog {

    /**
     * The logger.
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    /**
     * The Super Master Key.
     */
    private int smk;
    /**
     * The L1 Menu Id.
     */
    private int l1MenuId = -1;
    /**
     * The HLT Menu Id.
     */
    private int hltMenuId = -1;
    /**
     * The list with all L1 Prescale Sets.
     */
    private final DefaultListModel<String> l1PrescaleAllItems = new DefaultListModel<>();
    /**
     * The list with the prescale sets linked to the given SMT.
     */
    private final DefaultListModel<String> l1PrescaleLinkedItems = new DefaultListModel<>();
    /**
     * The list with all HLT Prescale Sets.
     */
    private final DefaultListModel<String> hltPrescaleAllItems = new DefaultListModel<>();
    /**
     * The list with the prescale sets linked to the given SMT.
     */
    private final DefaultListModel<String> hltPrescaleLinkedItems = new DefaultListModel<>();

    /**
     * Create and display the dialog screen.
     *
     * @param smt the super master table to add prescale sets to.
     * @throws java.sql.SQLException
     */
    public AssignPrescales(final SuperMasterTable smt, final JFrame parent)
            throws SQLException {

        super((JDialog) null);

        try {
            this.smk = smt.get_id();
            l1MenuId = smt.get_l1_master_table().get_menu_id();
            hltMenuId = smt.get_hlt_master_table().get_menu_id();
            //popluate the sets table and the all groups table
            this.getAllSets();
            this.getLinkedSets();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, rootPane, "Error while creating the Assign Prescale panel.");
        }

        //draw things
        initComponents();
        populateSMKList(smt);
        jLabel6.setText("Current SMK " + smk + ": " + smt.get_name());
        this.setTitle("Assign Prescales");
        this.setLocationRelativeTo(parent);
    }

    /**
     * find all prescale sets in the DB.
     */
    private void getAllSets() throws SQLException {

        l1PrescaleAllItems.removeAllElements();
        hltPrescaleAllItems.removeAllElements();

        for (String l1ps : L1Prescale.loadAvailableSetNames()) {
            l1PrescaleAllItems.addElement(l1ps);
        }
        for (String hltPs : HLTPrescaleSet.loadAvailableSetNames()) {
            hltPrescaleAllItems.addElement(hltPs);
        }
    }

    private void getSelectedSets(Integer smk) throws SQLException {

        SuperMasterTable superMaster = new SuperMasterTable(smk);
        Integer l1MenuID = superMaster.get_l1_master_table().get_menu_id();
        Integer hltMenuID = superMaster.get_hlt_master_table().get_menu_id();

        l1PrescaleAllItems.removeAllElements();
        hltPrescaleAllItems.removeAllElements();

        for (String l1ps : L1Prescale.loadAvailableSetNames(l1MenuID)) {
            l1PrescaleAllItems.addElement(l1ps);
        }
        for (String hltPs : HLTPrescaleSet.loadAvailableSetNames(hltMenuID)) {
            hltPrescaleAllItems.addElement(hltPs);
        }
    }

    /**
     * find all prescale sets in the DB linked to this SMT.
     *
     * @throws java.sql.SQLException
     */
    public void getLinkedSets() throws SQLException {

        l1PrescaleLinkedItems.removeAllElements();
        hltPrescaleLinkedItems.removeAllElements();

        for (String l1ps : L1Prescale.loadAvailableSetNames(l1MenuId)) {
            l1PrescaleLinkedItems.addElement(l1ps);
        }
        for (String hltPs : HLTPrescaleSet.loadAvailableSetNames(hltMenuId)) {
            hltPrescaleLinkedItems.addElement(hltPs);
        }

    }

    ///Netbeans. Don't touch.
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        listTableLinkedL1 = new javax.swing.JList<String>();
        jLabel2 = new javax.swing.JLabel();
        buttonAddL1 = new javax.swing.JButton();
        jScrollPane7 = new javax.swing.JScrollPane();
        listTableLinkedHLT = new javax.swing.JList<String>();
        buttonAddHLT = new javax.swing.JButton();
        jScrollPane8 = new javax.swing.JScrollPane();
        listTableAllL1 = new javax.swing.JList<String>();
        jScrollPane9 = new javax.swing.JScrollPane();
        listTableAllHLT = new javax.swing.JList<String>();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        buttonCancel = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox<String>();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1002, 511));

        jLabel1.setText("All Available L1 Prescale Sets:");

        listTableLinkedL1.setModel(l1PrescaleLinkedItems);
        jScrollPane6.setViewportView(listTableLinkedL1);

        jLabel2.setText("All Available HLT Prescale Sets:");

        buttonAddL1.setText("Add");
        buttonAddL1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddL1ActionPerformed(evt);
            }
        });

        listTableLinkedHLT.setModel(hltPrescaleLinkedItems);
        jScrollPane7.setViewportView(listTableLinkedHLT);

        buttonAddHLT.setText("Add");
        buttonAddHLT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddHLTActionPerformed(evt);
            }
        });

        listTableAllL1.setModel(l1PrescaleAllItems);
        jScrollPane8.setViewportView(listTableAllL1);

        listTableAllHLT.setModel(hltPrescaleAllItems);
        jScrollPane9.setViewportView(listTableAllHLT);

        jLabel3.setText("Currently Linked L1 Prescale Sets:");

        jLabel4.setText("Currently Linked HLT Prescale Sets:");

        buttonCancel.setText("Done");
        buttonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCancelActionPerformed(evt);
            }
        });

        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Liberation Sans", 1, 11)); // NOI18N
        jLabel5.setText("Select Origin SMK");

        jLabel6.setFont(new java.awt.Font("Liberation Sans", 1, 11)); // NOI18N
        jLabel6.setText("Current SMK");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createSequentialGroup()
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(buttonAddHLT, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 71, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(layout.createSequentialGroup()
                                        .add(jLabel2)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 217, Short.MAX_VALUE)
                                        .add(buttonAddL1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 71, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jScrollPane9, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jScrollPane8, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE))
                                .add(10, 10, 10))
                            .add(layout.createSequentialGroup()
                                .add(jLabel1)
                                .add(168, 168, 168))))
                    .add(layout.createSequentialGroup()
                        .add(20, 20, 20)
                        .add(jLabel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 127, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jComboBox1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 215, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(jLabel6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(18, 18, 18)
                        .add(buttonCancel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 74, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE)
                    .add(jScrollPane6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel3)
                            .add(jLabel4))
                        .add(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel1)
                    .add(jLabel3))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(jScrollPane8, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE)
                        .add(6, 6, 6))
                    .add(layout.createSequentialGroup()
                        .add(jScrollPane6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)))
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(layout.createSequentialGroup()
                        .add(buttonAddL1)
                        .add(18, 18, 18))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 25, Short.MAX_VALUE)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jLabel4)
                            .add(jLabel2))
                        .add(6, 6, 6)))
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jScrollPane9, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
                    .add(jScrollPane7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(buttonAddHLT)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(buttonCancel)
                    .add(jLabel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jComboBox1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Add the selected L1Prescale form the list to the menu with id l1ManuId.
     *
     * @param evt the swing event.
     */
private void buttonAddL1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAddL1ActionPerformed

    //if nothing selected don't do anything
    if (listTableAllL1.getSelectedIndex() < 0) {
        return;
    }
    // Get selected sets.
    Object[] l1sets = listTableAllL1.getSelectedValues();
    for (Object l1set : l1sets) {
        String l1ps = (String) l1set;
        try {

            boolean alreadylinked = false;
            String selection[] = l1ps.split("\\:");

            for (int i = 0; i < l1PrescaleLinkedItems.size(); i++) {
                String linksel[] = l1PrescaleLinkedItems.get(i).split("\\:");
                if (linksel[0] == null ? selection[0] == null : linksel[0].equals(selection[0])) {
                    alreadylinked = true;
                }
            }
            if (!alreadylinked) {
                SuperMasterTable smt = new SuperMasterTable(smk);
                ArrayList<Object> results = this.linkL1PrescaleSet(Integer.parseInt((selection[0])), getSMKfromSelector(), smk, smt.get_l1_master_table().get_menu_id(),true);
                L1Prescale newPrescale = (L1Prescale)results.get(0);
                Boolean alreadyInList = false;
                for (int i = 0; i < l1PrescaleLinkedItems.size(); i++) {
                    String linksel[] = l1PrescaleLinkedItems.get(i).split("\\:");
                    if (Integer.parseInt(linksel[0]) == newPrescale.get_id()) {
                        alreadyInList = true;
                    }
                }
                if (alreadyInList) {
                    JOptionPane.showMessageDialog(this,
                            "Selected L1 prescale set has already been migrated to this SMK as ID " + newPrescale.get_id(),
                            "Set already linked", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    l1PrescaleLinkedItems.add(0, newPrescale.get_id() + ":" + selection[1]);
                }
            } else {
                JOptionPane.showMessageDialog(null,
                        "Selected L1 prescale set is already linked",
                        "Set already linked", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, rootPane, "Error while performing Add L1 action.");
        }
    }

}//GEN-LAST:event_buttonAddL1ActionPerformed

    public static ArrayList<Object> compareL1Menus(Integer incomingSMK, Integer currentSMK, Boolean showReport) throws SQLException {
        String query = "select * from (";
        query += "select l1ti_name as lhs_name,l1ti_ctp_id as lhs_ctp from (";
        query += "select l1tm2ti_trigger_item_id from (";
        query += "select l1mt_trigger_menu_id from (select smt_id,smt_l1_master_table_id from super_master_table where smt_id = ?) lhs ";
        query += "join (select l1mt_id,l1mt_trigger_menu_id from l1_master_table) rhs ";
        query += "on lhs.smt_l1_master_table_id = rhs.l1mt_id";
        query += ") lhs1 ";
        query += "join (select l1tm2ti_trigger_menu_id,l1tm2ti_trigger_item_id from l1_tm_to_ti) rhs1 ";
        query += "on lhs1.l1mt_trigger_menu_id = rhs1.l1tm2ti_trigger_menu_id";
        query += ") lhs2 ";
        query += "join (select l1ti_id,l1ti_name,l1ti_ctp_id from l1_trigger_item) rhs2 ";
        query += "on lhs2.l1tm2ti_trigger_item_id = rhs2.l1ti_id";
        query += ") megalhs ";
        query += "full outer join (";
        query += "select l1ti_name as rhs_name,l1ti_ctp_id as rhs_ctp from (";
        query += "select l1tm2ti_trigger_item_id from (";
        query += "select l1mt_trigger_menu_id from (select smt_id,smt_l1_master_table_id from super_master_table where smt_id = ?) lhs ";
        query += "join (select l1mt_id,l1mt_trigger_menu_id from l1_master_table) rhs ";
        query += "on lhs.smt_l1_master_table_id = rhs.l1mt_id ";
        query += ") lhs1 ";
        query += "join (select l1tm2ti_trigger_menu_id,l1tm2ti_trigger_item_id from l1_tm_to_ti) rhs1 ";
        query += "on lhs1.l1mt_trigger_menu_id = rhs1.l1tm2ti_trigger_menu_id";
        query += ") lhs2 ";
        query += "join (select l1ti_id,l1ti_name,l1ti_ctp_id from l1_trigger_item) rhs2 ";
        query += "on lhs2.l1tm2ti_trigger_item_id = rhs2.l1ti_id";
        query += ") megarhs ";
        query += "on megalhs.lhs_name = megarhs.rhs_name order by rhs_ctp";

        query = query.toUpperCase();

        query = ConnectionManager.getInstance().fix_schema_name(query);
        //System.out.println(query);
        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, incomingSMK);
        ps.setInt(2, currentSMK);

        ResultSet rset = executeSelect(ps);
        //Extract mapping of old to new CTP ID - if no entry then assume this is a new item and create default prescale
        //Otherwise pull old prescale values from old set and copy into new set at new ctp id
        ArrayList<AbstractMap.SimpleEntry<Integer, Integer>> resultsMap = new ArrayList<>();

        ArrayList<String> itemsRemoved = new ArrayList();
        ArrayList<String> itemsAdded = new ArrayList();
        while (rset.next()) {
            //System.out.println("Incoming " + rset.getInt("lhs_ctp") + " " + rset.wasNull() + " current " + rset.getInt("rhs_ctp") + " " + rset.wasNull());

            Integer lhs = rset.getInt("lhs_ctp");
            Boolean leftIsNull = rset.wasNull();
            Integer rhs = rset.getInt("rhs_ctp");
            Boolean rightIsNull = rset.wasNull();
            if (leftIsNull) {
                //Item doesn't exist in incoming set, default will be automatically created, but report which they are
                itemsAdded.add("CTP ID " + rhs + ": " + rset.getString("rhs_name"));
            } else if (rightIsNull) {
                //Item doesn't exist in destination set, ignore
                itemsRemoved.add("CTP ID " + lhs + ": " + rset.getString("lhs_name"));
            } else {
                resultsMap.add(new AbstractMap.SimpleEntry<>(rset.getInt("lhs_ctp"), rset.getInt("rhs_ctp")));
            }
        }
        rset.close();
        Collections.sort(itemsAdded);
        Collections.sort(itemsRemoved);
        //Improve ordering!
        JTextArea textArea = new JTextArea("Items Removed (total " + itemsRemoved.size() + "):\n\n");

        for (String element : itemsRemoved) {
            textArea.append(element + "\n");
        }
        textArea.append("\nItems Added to Set (total " + itemsAdded.size() + "):\n\n");
        for (String element : itemsAdded) {
            textArea.append(element + "\n");
        }
        if (showReport) {
            JScrollPane scrollPane = new JScrollPane(textArea);
            textArea.setLineWrap(true);
            textArea.setWrapStyleWord(true);
            textArea.setEditable(false);
            scrollPane.setPreferredSize(new Dimension(600, 600));
            JOptionPane.showMessageDialog(null, scrollPane, "Item Difference Between Menus!",
                    JOptionPane.INFORMATION_MESSAGE);
        }
        
        ArrayList<Object> result = new ArrayList<>();
        result.add(resultsMap);
        result.add(textArea.getText());
        return result;
    }

    /**
     * Add the given L1 Prescale to the selected L1 Trigger Menu.
     *
     * @param l1ps_id
     * @param incomingSMK
     * @param destinationSMK
     * @param l1psname
     * @param l1MenuId
     * @param l1ps the L1 prescale set to add.
     * @return 
     * @throws java.sql.SQLException
     */
    public static ArrayList<Object> linkL1PrescaleSet(final int l1ps_id, final int incomingSMK, final int destinationSMK, int l1MenuId, Boolean showReport) throws SQLException {
        //loop over the linked ones and see if there already

        //if not already linked then link it
        // Compare Item names in old and new menu (if menus different)
        // Create new l1psk reordering the items as needed
        // Report changes
        L1Prescale incomingL1Prescale = new L1Prescale(l1ps_id);
        L1Prescale newPrescale = new L1Prescale();
        newPrescale.set_name(incomingL1Prescale.get_name());
        newPrescale.set_type(incomingL1Prescale.get_type());
        newPrescale.set_partition(incomingL1Prescale.get_partition());
        for (Integer index = 1; index < L1Prescale.LENGTH + 1; index++) {
            newPrescale.set_int_val(index, -1);
        }
        ArrayList<Object> result = compareL1Menus(incomingSMK, destinationSMK, showReport);
        ArrayList<AbstractMap.SimpleEntry<Integer, Integer>> comparator = (ArrayList<AbstractMap.SimpleEntry<Integer, Integer>>)result.get(0);
        for (AbstractMap.SimpleEntry<Integer, Integer> entry : comparator) {
            newPrescale.set_int_val(entry.getValue() + 1, incomingL1Prescale.get_val(entry.getKey() + 1));
        }
        newPrescale.save(new L1Menu(l1MenuId));
        ArrayList<Object> results = new ArrayList<>();
        results.add(newPrescale);
        results.add(result.get(1));
        return results;
        //System.out.println("Saved new L1 Prescale Set with ID " + newPrescale.get_id());

    }

    /**
     * Associate HLT prescale set.
     *
     * @param evt
     */
private void buttonAddHLTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAddHLTActionPerformed

    //if nothing selected don't do anything
    if (listTableAllHLT.getSelectedIndex() < 0) {
        return;
    }

    Object[] sets = this.listTableAllHLT.getSelectedValues();
    for (Object set : sets) {
        //(HLTPrescaleSet) listTableAllHLT.getSelectedValue();
        String hltPsName = (String) set;
        logger.log(Level.FINE, " Adding HLT Prescale Set {0} to SMT {1}", new Object[]{hltPsName, smk});
        try {
            int incomingID = Integer.parseInt((hltPsName.split("\\:"))[0]);

            for (int i = 0; i < hltPrescaleLinkedItems.size(); i++) {
                int linkedID = Integer.parseInt(((hltPrescaleLinkedItems.get(i)).split("\\:"))[0]);

                if (linkedID == incomingID) {
                    JOptionPane.showMessageDialog(null,
                            "Selected HLT prescale set is already linked as set " + linkedID,
                            "Set already linked", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
            }
            SuperMasterTable incomingKey = new SuperMasterTable(getSMKfromSelector());
            ArrayList<Integer> listOfIDs = new ArrayList<>();
            for (Integer linkedItem = 0; linkedItem < hltPrescaleLinkedItems.size(); linkedItem++) {
                listOfIDs.add(Integer.parseInt(((hltPrescaleLinkedItems.getElementAt(linkedItem)).split("\\:"))[0]));
            }
            ArrayList<Object> results = this.addHLTset(incomingID, true, incomingKey, hltMenuId, listOfIDs);
            HLTPrescaleSet newHLTPs = (HLTPrescaleSet) results.get(0);
            hltPrescaleLinkedItems.add(0, newHLTPs.get_id() + ": " + newHLTPs.get_name());

        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, rootPane, "Error while performing Add HLT action.");
        }
    }
}//GEN-LAST:event_buttonAddHLTActionPerformed

    public static ArrayList<Object> addHLTset(final int incomingID, final Boolean reportToScreen, final SuperMasterTable incomingKey, final int hltMenuId, ArrayList<Integer> listOfIDs) throws SQLException {

        //Nomenclature:
        //Active key - the supermaster key (and all associated menus and sets) which the user selected to migrate a set into
        //Incoming key - the supermaster key (and all associated menus and sets) which the user selected to migrate a set from
        //Process:
        //By running multiple loops to compare the chains the active and incoming keys we can establish:
        //1: Chains that are present in the incoming key and not the active key, and should therefore be ignored
        //2: Chains that are in both keys, which should be kept
        //3: Chains that are present in the active key and not the incoming key, and should therefore have dummy prescales created for them
        //
        //Further to this, a second pass has to be completed to perform the same logic for stream prescales.
        //If prescale set with same ID already exists in both incoming and active keys then no need to go any further
        //if not already linked then proceed
        //create containers for popup window
        ArrayList<String> namesAdded = new ArrayList<>();
        ArrayList<String> namesRemoved = new ArrayList<>();
        ArrayList<String> streamsAdded = new ArrayList<>();
        ArrayList<String> streamsRemoved = new ArrayList<>();

        //Copy incoming prescale set to new container for editing (otherwise list model may change)
        HLTPrescaleSet newHLTPs = new HLTPrescaleSet(incomingID);

        //Get HLT Menu associated with incoming prescale set
        HLTTriggerMenu comparatorMenu = new HLTTriggerMenu(incomingKey.get_hlt_master_table().get_menu_id());

        //Get HLT menu for active SMK
        HLTTriggerMenu themenu = new HLTTriggerMenu(hltMenuId);

        //Get all chain names in the active menu
        HashMap<String, Integer> menuChainNames = new HashMap<>();
        for (HLTTriggerChain chain : themenu.getChains()) {
            menuChainNames.put(chain.get_name(), chain.get_chain_counter());
        }

        //Get all regular prescales in the set for the incoming menu key.
        HashMap<String, Integer> psChainNames = new HashMap<>();
        for (HLTPrescale ps : newHLTPs.getPrescales()) {
            if (ps.get_type() == HLTPrescaleType.Prescale) {
                psChainNames.put(ps.get_chain_name(comparatorMenu), ps.get_chain_counter());
            }
        }

        //Compare incoming and active sets and establish which chains exist in the active set and not in the incoming set
        HashMap<String, Integer> psChainsToKeep = new HashMap<>();
        List<Integer> neededPsChainCounters = new ArrayList<>();
        for (Map.Entry<String, Integer> chainNameAndCounter : menuChainNames.entrySet()) {
            if (!psChainNames.containsKey(chainNameAndCounter.getKey())) {
                namesAdded.add(chainNameAndCounter.getKey());
                neededPsChainCounters.add(chainNameAndCounter.getValue());
            } else {
                psChainsToKeep.put(chainNameAndCounter.getKey(), chainNameAndCounter.getValue());
            }
        }

        //Exit if there is no overlap between sets, as there is no prescale information to transfer from incoming to active
        if (psChainsToKeep.entrySet().isEmpty()) {
            JOptionPane.showMessageDialog(null,
                    "Selected Prescale Set associated with no chains present in new menu, stopping.", "Set has no effect.",
                    JOptionPane.WARNING_MESSAGE);
            return new ArrayList<>();
        }

        //Compare incoming and active sets and establish which chains exist in the incoming set and NOT in the active set (and can therefore be deleted)
        List<Integer> notNeededPsChaincounters = new ArrayList<>();
        for (Map.Entry<String, Integer> incomingChainNameAndCounter : psChainNames.entrySet()) {
            if (!menuChainNames.containsKey(incomingChainNameAndCounter.getKey())) {
                namesRemoved.add(incomingChainNameAndCounter.getKey());
                notNeededPsChaincounters.add(incomingChainNameAndCounter.getValue());
            }
        }

        //Remove any prescales that don't exist in active set
        if (!notNeededPsChaincounters.isEmpty()) {
            Iterator psIt = newHLTPs.getPrescales().iterator();
            while (psIt.hasNext()) {
                HLTPrescale ps = (HLTPrescale) psIt.next();

                if (notNeededPsChaincounters.contains(
                        ps.get_chain_counter())) {
                    psIt.remove();
                }
            }
        }

        //Get prescale set associated with menu - should be immune to and subsequent loading bugs
        Collections.sort(listOfIDs);

        //Get sample HLT prescale set for active SMK (for stream prescale comparison
        //and store which chains have stream prescales, these will need to contain streams
        //in the linked PSS whether the incoming set has them or not
        //System.out.println("test " + Integer.parseInt(((hltPrescaleLinkedItems.get(0)).split("\\:"))[0]));
        HLTPrescaleSet SamplePSSFromCurrentMenu = new HLTPrescaleSet(listOfIDs.get(0));
        ArrayList<Integer> chainsWithStreams = new ArrayList<>();
        for (HLTPrescale prescale : SamplePSSFromCurrentMenu.getPrescales()) {
            if (prescale.get_type() == HLTPrescaleType.Stream) {
                chainsWithStreams.add(prescale.get_chain_counter());
            }
        }

        ArrayList<Integer> hasStreamPrescaleInIncomingMenu = new ArrayList<>();
        //correct chain counters to match new menu
        if (!newHLTPs.getPrescales().isEmpty()) {
            for (HLTPrescale ps : newHLTPs.getPrescales()) {
                if (psChainsToKeep.containsKey(ps.get_chain_name(comparatorMenu))) {
                    //int temp = ps.get_chain_counter();
                    //String nametemp = ps.get_chain_name(comparatorMenu);
                    ps.set_chain_counter(psChainsToKeep.get(ps.get_chain_name(comparatorMenu)));
                    //System.out.println("Keep " + temp + " new counter " + ps.get_chain_counter() + " name " + nametemp);
                    if (ps.get_type() == HLTPrescaleType.Stream) {
                        hasStreamPrescaleInIncomingMenu.add(ps.get_chain_counter());
                    }
                }
            }
        }

        //If incoming key contains a stream but active key doesn't, remove the stream
        ArrayList<Integer> streamsToRemove = new ArrayList<>();
        for (Integer counter : hasStreamPrescaleInIncomingMenu) {
            if (!chainsWithStreams.contains(counter)) {
                streamsToRemove.add(counter);
            }
        }

        //if both incoming and active keys have streams, retain value from incoming key
        //otherwise if active key has stream but not incoming one, create defaults
        ArrayList<Integer> streamsToCreate = new ArrayList<>();
        for (Integer counter : chainsWithStreams) {
            if (!hasStreamPrescaleInIncomingMenu.contains(counter)) {
                //chain has streams in new key but not in incoming key, create defaults
                streamsToCreate.add(counter);
            }
        }

        //Remove unneeded streams
        if (!streamsToRemove.isEmpty()) {
            Iterator psIt = newHLTPs.getPrescales().iterator();
            while (psIt.hasNext()) {
                HLTPrescale ps = (HLTPrescale) psIt.next();
                if (streamsToRemove.contains(ps.get_chain_counter()) && ps.get_type() == HLTPrescaleType.Stream) {
                    streamsRemoved.add(ps.get_chain_name(themenu));
                    psIt.remove();
                }
            }
        }

        //Generate remaining prescales for chains that exist in active key but not incoming key
        if (!neededPsChainCounters.isEmpty()) {
            for (Integer cc : neededPsChainCounters) {
                //Creating extra HLT PS with default values and adding to set
                HLTPrescale newps = new HLTPrescale();
                newps.set_type(HLTPrescaleType.Prescale);
                newps.set_chain_counter(cc);
                newps.set_value(-1);
                //add to set
                newHLTPs.getPrescales().add(newps);

                HLTPrescale newps1 = new HLTPrescale();
                newps1.set_type(HLTPrescaleType.Pass_Through);
                newps1.set_chain_counter(cc);
                newps1.set_value(0);
                //add to set
                newHLTPs.getPrescales().add(newps1);

                HLTPrescale newps2 = new HLTPrescale();
                newps2.set_type(HLTPrescaleType.ReRun);
                newps2.set_chain_counter(cc);
                newps2.set_value(-1);
                newps2.set_condition("0");
                //add to set
                newHLTPs.getPrescales().add(newps2);

            }
        }

        //Fix condition variables
        for (HLTPrescale ps : newHLTPs.getPrescales()) {
            if (ps.get_type() == HLTPrescaleType.Prescale || ps.get_type() == HLTPrescaleType.Pass_Through) {
                ps.set_condition("");
            }
        }

        //Create missing streams
        ArrayList<HLTPrescale> streamsToAdd = new ArrayList<>();
        for (HLTPrescale ps : newHLTPs.getPrescales()) {
            if (ps.get_type() == HLTPrescaleType.Prescale) {
                if (streamsToCreate.contains(ps.get_chain_counter())) {
                    HLTPrescale newps = new HLTPrescale();
                    newps.set_type(HLTPrescaleType.Stream);
                    newps.set_value(0);
                    newps.set_chain_counter(ps.get_chain_counter());
                    newps.set_condition("express");
                    streamsAdded.add(ps.get_chain_name(themenu));
                    //add to set
                    streamsToAdd.add(newps);
                }
            }
        }
        newHLTPs.addPrescales(streamsToAdd);

        //see if any with both na, then remove from the set
        //WPV commented out until re-run prescales with value 0 are fully understood
        /*Iterator it = newHLTPs.getPrescales().iterator();
         while (it.hasNext()) {
         HLTPrescale sps = (HLTPrescale) it.next();
         if (sps.get_condition().equals("") && sps.get_value() == 0) {
         it.remove();
         }
         }*/
        //Store old PSS ID, save new set and check in case save returns that the desired new set is already duplicated in the active key
        int oldpssid = newHLTPs.get_id();
        int newpssid;
        if (neededPsChainCounters.size() > 0 || notNeededPsChaincounters.size() > 0 || !streamsAdded.isEmpty() || !streamsRemoved.isEmpty()) {
            newHLTPs.set_id(-1);
            newpssid = newHLTPs.save(themenu.get_id());
        } else {
            newpssid = oldpssid;
        }

        //make a new link to the menu, note that if the new PSS duplicates an old one for the ky the link will be to the old one as the new
        //one will not have been saved
        HLTTM_PS newlink = new HLTTM_PS();
        newlink.set_menu_id(hltMenuId);
        newlink.set_prescale_set_id(newpssid);
        //this is default, means hidden=false i.e. see it.
        newlink.set_used(false);
        newlink.save();

        // add to model - if not there already! (since if the save returned a duplicate set there is no need for further changes)
        for (int alreadylinkedPSID : listOfIDs) {
            if (alreadylinkedPSID == newHLTPs.get_id()) {
                JOptionPane.showMessageDialog(null,
                        "When associated with this menu, set becomes equivalent"
                        + " to ID " + alreadylinkedPSID,
                        "Equivalent set already linked",
                        JOptionPane.INFORMATION_MESSAGE);
                return new ArrayList<>();
            }
        }

        ArrayList<Object> results = new ArrayList<>();
        results.add(newHLTPs);
        //hltPrescaleLinkedItems.add(0, newHLTPs.get_id() + ": " + newHLTPs.get_name());

        if (!neededPsChainCounters.isEmpty() || !notNeededPsChaincounters.isEmpty() || !streamsAdded.isEmpty() || !streamsRemoved.isEmpty()) {

            JTextArea textArea = new JTextArea("Chains Removed (total " + notNeededPsChaincounters.size() + "):\n\n");
            for (String element : namesRemoved) {
                textArea.append(element + "\n");
            }
            textArea.append("\nChains Added (total " + neededPsChainCounters.size() + "):\n\n");
            for (String element : namesAdded) {
                textArea.append(element + "\n");
            }
            if (!streamsAdded.isEmpty() || !streamsRemoved.isEmpty()) {
                textArea.append("\nDue to mismatch between stream prescales between keys the following modifications have also been made:\n");

                textArea.append("\nStream Prescales Created (total " + streamsAdded.size() + "), default OFF:\n\n");
                for (String element : streamsAdded) {
                    textArea.append(element + "\n");
                }
                textArea.append("\nStreams Prescales Removed that were in incoming set (total " + streamsRemoved.size() + "):\n\n");
                for (String element : streamsRemoved) {
                    textArea.append(element + "\n");
                }
            }
            if (reportToScreen) {
                JScrollPane scrollPane = new JScrollPane(textArea);
                textArea.setLineWrap(true);
                textArea.setWrapStyleWord(true);
                textArea.setEditable(false);
                scrollPane.setPreferredSize(new Dimension(500, 500));
                JOptionPane.showMessageDialog(null, scrollPane, "Chain Difference Between Menus!",
                        JOptionPane.INFORMATION_MESSAGE);
            }
            results.add(textArea.getText());
        }
        return results;
    }

    private void populateSMKList(SuperMasterTable smt) throws SQLException {
        ArrayList<String> keys = smt.getAllSMKsAndNames();
        jComboBox1.removeAllItems();
        Collections.sort(keys);
        for (String key : keys) {
            //Remove destination SMK from selector
            String selection[] = key.split("\\:");
            //System.out.println(selection[0] + " " + smk);
            if (!(Integer.parseInt(selection[0]) == smk)) {
                jComboBox1.addItem(key);
            }
        }

    }

///Close the screen
private void buttonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCancelActionPerformed
    dispose();
}//GEN-LAST:event_buttonCancelActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        try {
            getSelectedSets(getSMKfromSelector());
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, rootPane, "Error while populating prescale sets for selected SMK.");
        }
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private Integer getSMKfromSelector() {
        String selection[] = jComboBox1.getSelectedItem().toString().split("\\:");
        return Integer.parseInt(selection[0]);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    ///Cancel and close.
    private javax.swing.JButton buttonAddHLT;
    ///Cancel and close.
    private javax.swing.JButton buttonAddL1;
    ///Cancel and close.
    private javax.swing.JButton buttonCancel;
    private javax.swing.JComboBox<String> jComboBox1;
    ///Bunch Group Name.
    private javax.swing.JLabel jLabel1;
    ///Bunch Group Name.
    private javax.swing.JLabel jLabel2;
    ///Bunch Group Name.
    private javax.swing.JLabel jLabel3;
    ///Bunch Group Name.
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JList<String> listTableAllHLT;
    private javax.swing.JList<String> listTableAllL1;
    private javax.swing.JList<String> listTableLinkedHLT;
    private javax.swing.JList<String> listTableLinkedL1;
    // End of variables declaration//GEN-END:variables
}
