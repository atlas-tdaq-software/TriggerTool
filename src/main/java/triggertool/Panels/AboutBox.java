package triggertool.Panels;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Connections.TriggerSchema;

///Displays a simple dialog box with some information about the connection
/**
 * Displays simple information about the connection to the database, the
 * Trigger Tool and the schema installed.
 * 
 * @author Simon Head
 */
public final class AboutBox extends javax.swing.JDialog {

    /** Message Log */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    ///Constructor creates and displays the dialog

    /**
     * Creates a dialog for the AboutBox.  All the work is done in here.  It
     * performs a simple query on the database and shows information about the 
     * connection.  If the TriggerTool is open and not connected to a database
     * (which should not happen) we could have a problem here.
     * 
     * @param parent The frame that is the parent of this. Null is allowed.
     * @param modal Modal dialogs pause the execution of the thing that called 
     *              them until they are closed.
     */
    public AboutBox(java.awt.Frame parent) {
        super();
        initComponents();

        setTitle("About the TriggerTool");
        String label = "<html>\n";
        label += "<B>TriggerTool " + triggertool.TriggerTool.TT_TAG + "</B>";

        Handler[] logHandlers = logger.getHandlers();
        for (Handler h : logHandlers) {
            if (h instanceof ConsoleHandler) {
                String lvl = ((ConsoleHandler) h).getLevel().getName();
                label += "\n<br><br> Logging level <b>" + lvl + "</b> to : <br> <tt>    console</tt>. ";
            } else if (h instanceof FileHandler) {
                String lvl = ((FileHandler) h).getLevel().getName();
                String file = getLogFileName((FileHandler) h);
                label += "\n<br><br> Logging level <b>"+lvl+"</b> to :"+
                        "<br> to file : <tt>    " + file + "</tt> ";
            } else {
                String lvl = h.getLevel().getName();
                label += "\n<br><br> Logging to: " + h.getClass() + " \t LEVEL :" + lvl;
            }
        }
        label += "\n</html>";

        labelVersion.setText(label);

        int ver;
        try {
            ver = TriggerSchema.GetVersion();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error occurred while getting the version.", ex);
            JOptionPane.showMessageDialog(parent, "SQL Error" ,"Error occurred while getting the version. "
                    + "Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
            ver = -1;
        }

        InitInfo ii = ConnectionManager.getInstance().getInitInfo();

        labelSchema.setText("<html><p align=\"center\">Current connection<br>"
                + "Mode: " + ii.getUserMode() + "<br>"
                + "Username: " + ii.getSystemUsername() + "<br>"
                + "DB Username: " + ii.getUserName() + "<br>"
                + "DB Table: " + ii.getSchemaName() + "<br>"
                + "DB Server: " + ii.getdbServer() + "<br>"
                + "DB Technology: " + ii.getTechnology()
                + "<br><br>"
                + "Database is using schema version " + ver
                + "</p></html>");

        setLocationRelativeTo(parent);
        setVisible(true);
    }

    /**
     * Get file info from logger.
     */
    private String getLogFileName(FileHandler file) {
        Field pattern = getFieldFromClass("java.util.logging.FileHandler", "pattern");
        pattern.setAccessible(true);
        String fileName = null;
        try {
            fileName = (String) pattern.get(file);
        } catch (IllegalArgumentException ex) {
            logger.log(Level.SEVERE, "IllegalArgumentException in getLogFileName function.", ex);
        } catch (IllegalAccessException ex) {
            logger.log(Level.SEVERE, "IllegalAccessException in getLogFileName function.", ex);
        }
        return fileName;
    }

    /**
     * Get file info from logger.
     */
    private Field getFieldFromClass(String className, String fieldName) {
        try {
            Class clazz = Class.forName(className);
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                if (field.getName().equals(fieldName)) {
                    return field;
                }
            }        
        } catch (ClassNotFoundException ex) {
            logger.log(Level.SEVERE, "ClassNotFoundException in getLogFileName function.", ex);
        }

        return null;
    }

    ///Owned by Netbeans. Constructs form.
    /** 
     * This method is called from within the constructor to
     * initialise the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonDone = new javax.swing.JButton();
        labelVersion = new javax.swing.JLabel();
        labelSchema = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);

        buttonDone.setText("Done");
        buttonDone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDoneActionPerformed(evt);
            }
        });

        labelVersion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelVersion.setText("jLabel1");
        labelVersion.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        labelSchema.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelSchema.setText("jLabel1");
        labelSchema.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(labelVersion, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 437, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(labelSchema, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, buttonDone))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(labelVersion, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 113, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(labelSchema, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(buttonDone)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    ///Done button. Close window
    private void buttonDoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDoneActionPerformed
        dispose();
    }//GEN-LAST:event_buttonDoneActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    ///Button to close window.
    private javax.swing.JButton buttonDone;
    ///Label to display connection information.
    private javax.swing.JLabel labelSchema;
    ///Label to display TriggerTool version.
    private javax.swing.JLabel labelVersion;
    // End of variables declaration//GEN-END:variables
}
