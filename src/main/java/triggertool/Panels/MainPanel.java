package triggertool.Panels;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Cursor;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.text.BadLocationException;
import org.jdesktop.layout.GroupLayout;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Connections.UserMode;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.HLT.HLTComponent;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTParameter;
import triggerdb.Entities.HLT.HLTPrescale;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.HLT.HLTSetup;
import triggerdb.Entities.HLT.HLTTriggerChain;
import triggerdb.Entities.HLT.HLTTriggerElement;
import triggerdb.Entities.HLT.HLTTriggerGroup;
import triggerdb.Entities.HLT.HLTTriggerMenu;
import triggerdb.Entities.HLT.HLTTriggerSignature;
import triggerdb.Entities.HLT.HLTTriggerStream;
import triggerdb.Entities.HLT.HLTTriggerType;
import triggerdb.Entities.L1.L1BunchGroup;
import triggerdb.Entities.L1.L1BunchGroupSet;
import triggerdb.Entities.L1.L1CaloInfo;
import triggerdb.Entities.L1.L1CaloSinCos;
import triggerdb.Entities.L1.L1CaloMinTob;
import triggerdb.Entities.L1.L1CaloIsolation;
import triggerdb.Entities.L1.L1CaloIsoParam;
import triggerdb.Entities.L1.L1CtpFiles;
import triggerdb.Entities.L1.L1CtpSmx;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.L1.L1Menu;
import triggerdb.Entities.L1.L1MuctpiInfo;
import triggerdb.Entities.L1.L1MuonThresholdSet;
import triggerdb.Entities.L1.L1Prescale;
import triggerdb.Entities.L1.L1PrescaledClock;
import triggerdb.Entities.L1.L1Random;
import triggerdb.Entities.L1.L1Threshold;
import triggerdb.Entities.L1.L1ThresholdValue;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.Entities.Topo.TopoAlgo;
import triggerdb.Entities.Topo.TopoAlgoInput;
import triggerdb.Entities.Topo.TopoAlgoOutput;
import triggerdb.Entities.Topo.TopoConfig;
import triggerdb.Entities.Topo.TopoGeneric;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.Entities.Topo.TopoMenu;
import triggerdb.Entities.Topo.TopoOutputLine;
import triggerdb.Entities.Topo.TopoOutputList;
import triggerdb.Entities.Topo.TopoParameter;
import triggertool.Components.AlgosDialog;
import triggertool.Components.AliasDialog;
import triggertool.Components.CTPFileCreatorDialog;
import triggertool.Components.DownloadXML;
import triggertool.Components.HLTEditSetup;
import triggertool.Components.InternalWriteLockDialog;
import triggertool.Components.L1Cable;
import triggertool.Components.L1DumpSmxFiles;
import triggertool.Components.L1ItemMonitoring;
import triggertool.Components.L1Monitoring;
import triggertool.Components.LogViewer;
import triggertool.Components.PasswordDialog;
import triggertool.Components.ResultsTableModel;
import triggertool.Components.RowTableModel;
import triggertool.Components.SchemaCheck;
import triggertool.Components.TriggerTree;
import triggertool.Components.UploadXML;
import triggertool.Components.UsersDialogExpert;
import triggertool.Components.UsersDialogShifter;
import triggertool.Diff.DiffDisplayDialog;
import triggertool.Diff.DiffObjectSel;
import triggertool.L1BunchGroupPanels.BunchGroupEditMasterPanel;
import triggertool.Panels.MenuWork.AssignAliasDialog;
import triggertool.Panels.MenuWork.AssignPrescales;
import triggertool.Panels.MenuWork.StreamsDialog;
import triggertool.Panels.PrescaleEditPanel.L1PartitionPrescaleEdit;
import triggertool.Panels.PrescaleEditPanel.PrescaleEditMasterPanel;
import triggertool.SMT.SuperMasterTableInteractor;
import triggertool.TTExceptionHandler;
import triggertool.TriggerTool;

/**
 * The Main Screen. Designed with netbeans, which means the MainPanel.form file
 * is needed and you shouldn't edit the bits by hand it tells you not to. From
 * this screen the user can search the trigger db, and look through the results.
 *
 * @author Simon Head
 */
public final class MainPanel extends javax.swing.JFrame {

    /**
     * Required by Java.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Message Log.
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");

    static void SetIconImage(JDialog dialog) {
        URL iconUrl = MainPanel.class.getResource("/resources/TTIcon2.gif");
        if (iconUrl != null) {
            ImageIcon icon = new ImageIcon(iconUrl);
            dialog.setIconImage(icon.getImage());
        }
    }

    static void SetIconImage(JFrame frame) {
        URL iconUrl = MainPanel.class.getResource("/resources/TTIcon2.gif");
        if (iconUrl != null) {
            ImageIcon icon = new ImageIcon(iconUrl);
            frame.setIconImage(icon.getImage());
        }
    }

    /**
     * Table model to display the search results.
     */
    private ResultsTableModel resultsModel = new ResultsTableModel();
    /**
     * Table model to display one row from the search results.
     */
    private RowTableModel rowModel = new RowTableModel();
    /**
     * The currently selected table.
     */
    private AbstractTable selectedTable = null;
    /**
     * A tree view.
     */
    private TriggerTree treeTrigger;
    /**
     * Table we are looking for.
     */
    private AbstractTable table = null;
    /**
     * Query we use.
     */
    private String query = "";
    private static MainPanel mainPanel = null;
    /**
     * Flag to show/hide hidden items.
     */
    private Boolean showHidden = false;
    /**
     * The user level as defined in InitInfo which is linked through the
     * ConnectionManager as a singleton.
     */
    private UserMode userMode = UserMode.UNKNOWN;
    private int storedRow = -1;
    /**
     * Counter of how many prescale windows we have open.
     */
    private int[] psWindows = {0, 0, 0, 0};
    /**
     * True if compatible with prescale set alias. If false, disables
     * AliasDialog.
     */
    private boolean DBALIASOK = true;

    final String iconLocation = "image/TTIcon.xpm";

    private Boolean aliasWindowActive = false;
    private Boolean aliasAssignWindowActive = false;
    
    /**
     * Create and display the form. Make sure it's displayed in the correct mode
     * so the user isn't given options they're not allowed.
     */
    public MainPanel() {
        treeTrigger = new TriggerTree(rowModel, resultsModel);
        initComponents();

        resultsModel = (ResultsTableModel) tableSearchResults.getModel();
        // speed up scrolling
        tabMenu.getVerticalScrollBar().setUnitIncrement(20);

        mainPanel = this;

        setup();

        //setup combo box
        setupComboBox();

        changeUserMode(UserMode.EXIT);

        SetIconImage(this);

        // run the check on the DB version
        int dbCheck = ConnectionManager.getInstance().checkDBVersionCompatible();
        if (dbCheck < 1) {
            switch (dbCheck) {
                case TriggerTool.TRIGDB_NO_XS_SUPPORT:
                    DBALIASOK = false;
                    break;
                case TriggerTool.TRIGDB_NO_ALIAS_SUPPORT:
                    DBALIASOK = false;
                    break;
            }
            String msg = ConnectionManager.getInstance().getDBVersionWarningMsg(dbCheck);
            JOptionPane.showMessageDialog(rootPane, msg, "Warning", JOptionPane.WARNING_MESSAGE);
        }

        /**
         * A list selection listener that knows when the user clicks on the top
         * table.
         */
        tableSearchResults.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = tableSearchResults.rowAtPoint(e.getPoint());
                reload(row);
                if (e.getClickCount() == 2) {
                    buttonPrescalesActionPerformed(null);
                }
            }
            @Override
            public void mousePressed(MouseEvent e) {}
            @Override
            public void mouseReleased(MouseEvent e) {}
            @Override
            public void mouseEntered(MouseEvent e) {}
            @Override
            public void mouseExited(MouseEvent e) {}           
        });
        
        this.buttonSearch.doClick();
    }

    /**
     * MainPanel is a singleton.
     *
     * @return the MainPanel singleton.
     */
    public static synchronized MainPanel getMainPanel() {
        return MainPanel.mainPanel;
    }

    /**
     * Sneakily load a SuperMaster key by ID using the search. Is used by the
     * command line loader.
     *
     * @param key Integer ID of the record to load.
     * @throws java.sql.SQLException
     */
    public void loadKey(final int key) throws SQLException {
        logger.log(Level.INFO, "Load key {0}", key);

        comboFieldSelect.setSelectedItem("ID");
        editSearchText.setText(key + "");
        buttonSearchActionPerformed(null);

        tableSearchResults.setRowSelectionInterval(0, 0);
        reload(0);
        logger.info("Done loading key");
    }

    /**
     * Load data from the row and show the correct buttons to the user, based on
     * the mode the user is in and also the type of table.
     *
     * @param row The ID to load.
     */
    private void reload(final int row) {

        if (row >= 0) {
            selectedTable = resultsModel.get(row);

            jlsMIwriteXML.setEnabled(false);
            jmwMIstrm.setEnabled(false);
            jmwMIalgos.setEnabled(false);
            jmwMIpres.setEnabled(false);
            jl1MIlBG.setEnabled(false); //But not allowed to save
            jMenuPartitionPSP1.setEnabled(false);
            jMenuPartitionPSP2.setEnabled(false);
            jMenuPartitionPSP3.setEnabled(false);
            jmwMIAlgoEdit.setEnabled(false);
            jMenuPartitionBGP1.setEnabled(false);
            jMenuPartitionBGP2.setEnabled(false);
            jMenuPartitionBGP3.setEnabled(false);
            jmwMIasps.setEnabled(false);
            jmwMIcomm.setEnabled(false);
            jmwMIalia.setEnabled(DBALIASOK);
            jmwMIasalia.setEnabled(DBALIASOK);
            jl1MIrTMC.setEnabled(false);
            jl1MIlBG.setEnabled(false);
            jButtonHide.setEnabled(false);
            Monitors.setEnabled(false);
            ItemMonitors.setEnabled(false);
            CTPCables.setEnabled(false);
            DumpSmxFiles.setEnabled(false);

            if (selectedTable instanceof SuperMasterTable) {
                SuperMasterTable smt = (SuperMasterTable) selectedTable;
                
                tab.setEnabledAt(1, true);
                if (tab.getSelectedIndex() == 1) {
                    quickMenuView.drawSuperMaster(smt);
                }
                
                jlsMIwriteXML.setEnabled(true);
                jmwMIstrm.setEnabled(true);
                jmwMIalgos.setEnabled(true);
                jmwMIpres.setEnabled(true);
                jl1MIlBG.setEnabled(true); //But not allowed to save
                jMenuPartitionPSP1.setEnabled(true);
                jMenuPartitionPSP2.setEnabled(true);
                jMenuPartitionPSP3.setEnabled(true);
                jmwMIAlgoEdit.setEnabled(true);
                jMenuPartitionBGP1.setEnabled(true);
                jMenuPartitionBGP2.setEnabled(true);
                jMenuPartitionBGP3.setEnabled(true);
                Monitors.setEnabled(true);
                ItemMonitors.setEnabled(true);
                CTPCables.setEnabled(true);
                DumpSmxFiles.setEnabled(true);

                //can assign prescales if >shifter
                if (userMode.ordinal() >= UserMode.SHIFTER.ordinal()) {
                    jmwMIasps.setEnabled(true);
                    jmwMIcomm.setEnabled(true);
                    jmwMIalia.setEnabled(DBALIASOK);
                    jmwMIasalia.setEnabled(DBALIASOK);
                }

                //L1 expert or menu mesiter
                if (userMode == UserMode.L1EXPERT || userMode.ordinal() >= UserMode.EXPERT.ordinal()) {
                    jl1MIrTMC.setEnabled(true);
                    jl1MIlBG.setEnabled(true);
                    //can show or hide keys
                    jButtonHide.setEnabled(true);
                }

            } else {
                //if not sm, don't show menu view
                tab.setSelectedIndex(0);
                tab.setEnabledAt(1, false);
            }

            rowModel.load(selectedTable);
            try {
                treeTrigger.update(selectedTable);
            } catch (SQLException ex) {
                TTExceptionHandler.HandleSqlException(ex, rootPane, "Error loading the Trigger tree in the Main panel.");
            }
        }

        storedRow = row;
    }

    /**
     * Code to switch between different user modes. Note that this is only for
     * atonr and that if the user wants to switch between a reader and writer
     * account we have to form a new connection to the database. We also set the
     * buttons that are allowed here.
     *
     * @param newMode The mode to change to.
     */
    private void changeUserMode(final UserMode newMode) {
        ConnectionManager cmgr = ConnectionManager.getInstance();
        if (cmgr.getInitInfo().getdbServer().contains("atonr")) {

            if (newMode.ordinal() > UserMode.USER.ordinal()) {
                new PasswordDialog(null, true, newMode);
            } else if (newMode == UserMode.USER) {
                cmgr.changeUser("test", "", newMode);
            }
        }

        setup();

        //what's generally allowed
        jPanel3.removeAll();
        GroupLayout jPanel3Layout = new GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(jPanel3Layout.createParallelGroup(GroupLayout.LEADING).add(tab, GroupLayout.DEFAULT_SIZE, 956, Short.MAX_VALUE));
        jPanel3Layout.setVerticalGroup(jPanel3Layout.createParallelGroup(GroupLayout.LEADING).add(tab, GroupLayout.DEFAULT_SIZE, 209, Short.MAX_VALUE));
        tab.addTab("Menu", tabMenu);
        tab.setSelectedIndex(0);

        //seen by all
        menuItemViewLogs.setEnabled(true);
        menuItemInternalWriteLock.setEnabled(true);
        comboTable.setVisible(true);

        //not allowed unless set otherwise
        jl1MIlBG.setEnabled(true); //Allowed at all levels but only save at Expert.

        jmwMIdiff.setEnabled(true);
        menuItemUsers.setEnabled(false);

        jMenuItem2.setEnabled(false); //check schema

        if (userMode.ordinal() >= UserMode.SHIFTER.ordinal()) {
            if (ConnectionManager.getInstance().getInitInfo().getdbServer().contains("atonr")) {
                menuItemUsers.setEnabled(true);
            }
        }

        if (userMode.ordinal() >= UserMode.EXPERT.ordinal()) {
            jl1MIlBG.setEnabled(true);
            jMenuItem2.setEnabled(true); //check schema
        }

        if (userMode.ordinal() >= UserMode.EXPERT.ordinal()) {
            jlsMIreadXML.setEnabled(true);
        }

        reload(storedRow);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        jPanel2 = new javax.swing.JPanel();
        editSearchText = new javax.swing.JTextField();
        buttonSearch = new javax.swing.JButton();
        comboFieldSelect = new triggertool.Components.FieldComboBox();
        comboTable = new javax.swing.JComboBox<>();
        jButtonHide = new javax.swing.JButton();
        jToggleButtonHidden = new javax.swing.JToggleButton();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel3 = new javax.swing.JPanel();
        tab = new javax.swing.JTabbedPane();
        tabTree = new javax.swing.JPanel();
        jSplitPane2 = new javax.swing.JSplitPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane(treeTrigger);
        tabMenu = new javax.swing.JScrollPane();
        quickMenuView = new triggertool.Panels.QuickMenuView();
        jScrollPane3 = new javax.swing.JScrollPane();
        tableSearchResults = new javax.swing.JTable();
        jToolBar3 = new javax.swing.JToolBar();
        jPanel1 = new javax.swing.JPanel();
        labelStatus = new javax.swing.JLabel();
        menuBar2 = new javax.swing.JMenuBar();
        fileMenu2 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        menuItemUsers = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        jMenuItem1 = new javax.swing.JMenuItem();
        exitMenuItem2 = new javax.swing.JMenuItem();
        workMenu = new javax.swing.JMenu();
        jmwMIpres = new javax.swing.JMenuItem();
        jmwMIasps = new javax.swing.JMenuItem();
        jmwMIcomm = new javax.swing.JMenuItem();
        jmwMIalia = new javax.swing.JMenuItem();
        jmwMIasalia = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        jmwMIstrm = new javax.swing.JMenuItem();
        jmwMIalgos = new javax.swing.JMenuItem();
        jmwMIdiff = new javax.swing.JMenuItem();
        l1Menu = new javax.swing.JMenu();
        jl1MIrTMC = new javax.swing.JMenuItem();
        jl1MIlBG = new javax.swing.JMenuItem();
        DumpSmxFiles = new javax.swing.JMenuItem();
        Monitors = new javax.swing.JMenuItem();
        ItemMonitors = new javax.swing.JMenuItem();
        CTPCables = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        jMenu2 = new javax.swing.JMenu();
        jMenuPartitionPSP1 = new javax.swing.JMenuItem();
        jMenuPartitionPSP2 = new javax.swing.JMenuItem();
        jMenuPartitionPSP3 = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        jMenuPartitionBGP1 = new javax.swing.JMenuItem();
        jMenuPartitionBGP2 = new javax.swing.JMenuItem();
        jMenuPartitionBGP3 = new javax.swing.JMenuItem();
        hltMenu = new javax.swing.JMenu();
        jmwMIAlgoEdit = new javax.swing.JMenuItem();
        loadSaveMenu = new javax.swing.JMenu();
        jlsMIreadXML = new javax.swing.JMenuItem();
        jlsMIwriteXML = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        menuItemViewLogs = new javax.swing.JMenuItem();
        menuItemInternalWriteLock = new javax.swing.JMenuItem();
        LoggerMenuItem = new javax.swing.JMenuItem();
        helpMenu2 = new javax.swing.JMenu();
        aboutMenuItem2 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setMinimumSize(new java.awt.Dimension(950, 600));

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);
        jToolBar1.setMargin(new java.awt.Insets(5, 0, 0, 0));
        jToolBar1.setPreferredSize(new java.awt.Dimension(1296, 28));

        editSearchText.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        editSearchText.setText("Search"); // NOI18N
        editSearchText.setMaximumSize(new java.awt.Dimension(200, 20));
        editSearchText.setMinimumSize(new java.awt.Dimension(10, 20));
        editSearchText.setPreferredSize(new java.awt.Dimension(55, 20));
        editSearchText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                searchFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                searchFocusLost(evt);
            }
        });

        buttonSearch.setText("Search"); // NOI18N
        buttonSearch.setFocusable(false);
        buttonSearch.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonSearch.setMaximumSize(new java.awt.Dimension(57, 20));
        buttonSearch.setMinimumSize(new java.awt.Dimension(57, 20));
        buttonSearch.setPreferredSize(new java.awt.Dimension(57, 20));
        buttonSearch.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSearchActionPerformed(evt);
            }
        });

        comboFieldSelect.setMinimumSize(new java.awt.Dimension(41, 20));
        comboFieldSelect.setPreferredSize(new java.awt.Dimension(41, 20));

        comboTable.setMaximumSize(new java.awt.Dimension(200, 20));
        comboTable.setMinimumSize(new java.awt.Dimension(200, 20));
        comboTable.setPreferredSize(new java.awt.Dimension(200, 20));
        comboTable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboTableActionPerformed(evt);
            }
        });

        jButtonHide.setText("Hide");
        jButtonHide.setEnabled(false);
        jButtonHide.setMaximumSize(new java.awt.Dimension(40, 20));
        jButtonHide.setMinimumSize(new java.awt.Dimension(40, 20));
        jButtonHide.setPreferredSize(new java.awt.Dimension(40, 20));
        jButtonHide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHideActionPerformed(evt);
            }
        });

        jToggleButtonHidden.setText("Show hidden SMKs");
        jToggleButtonHidden.setMaximumSize(new java.awt.Dimension(150, 20));
        jToggleButtonHidden.setMinimumSize(new java.awt.Dimension(150, 20));
        jToggleButtonHidden.setPreferredSize(new java.awt.Dimension(130, 20));
        jToggleButtonHidden.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButtonHiddenActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(6, 6, 6)
                .add(comboTable, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(comboFieldSelect, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 195, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(editSearchText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 133, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(buttonSearch, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 119, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 319, Short.MAX_VALUE)
                .add(jButtonHide, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 74, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jToggleButtonHidden, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 156, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(comboTable, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(comboFieldSelect, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(buttonSearch, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jButtonHide, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jToggleButtonHidden, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(editSearchText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jToolBar1.add(jPanel2);

        jSplitPane1.setDividerLocation(320);
        jSplitPane1.setDividerSize(8);
        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane1.setOneTouchExpandable(true);

        tab.setTabPlacement(javax.swing.JTabbedPane.BOTTOM);
        tab.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                tabStateChanged(evt);
            }
        });

        jSplitPane2.setBorder(null);
        jSplitPane2.setDividerLocation(350);

        jTable1.setModel(rowModel);
        jScrollPane1.setViewportView(jTable1);

        jSplitPane2.setRightComponent(jScrollPane1);
        jSplitPane2.setLeftComponent(jScrollPane2);

        org.jdesktop.layout.GroupLayout tabTreeLayout = new org.jdesktop.layout.GroupLayout(tabTree);
        tabTree.setLayout(tabTreeLayout);
        tabTreeLayout.setHorizontalGroup(
            tabTreeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jSplitPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 1218, Short.MAX_VALUE)
        );
        tabTreeLayout.setVerticalGroup(
            tabTreeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jSplitPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 765, Short.MAX_VALUE)
        );

        tab.addTab("Tree", tabTree);

        org.jdesktop.layout.GroupLayout quickMenuViewLayout = new org.jdesktop.layout.GroupLayout(quickMenuView);
        quickMenuView.setLayout(quickMenuViewLayout);
        quickMenuViewLayout.setHorizontalGroup(
            quickMenuViewLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 1318, Short.MAX_VALUE)
        );
        quickMenuViewLayout.setVerticalGroup(
            quickMenuViewLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 750, Short.MAX_VALUE)
        );

        tabMenu.setViewportView(quickMenuView);

        tab.addTab("Menu", tabMenu);

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(tab, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 1230, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(tab)
        );

        jSplitPane1.setBottomComponent(jPanel3);

        tableSearchResults.setModel(resultsModel);
        jScrollPane3.setViewportView(tableSearchResults);

        jSplitPane1.setLeftComponent(jScrollPane3);

        jToolBar3.setFloatable(false);
        jToolBar3.setRollover(true);

        jPanel1.setOpaque(false);
        jPanel1.setPreferredSize(new java.awt.Dimension(336, 21));

        labelStatus.setText("status"); // NOI18N

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                .add(labelStatus, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(734, 734, 734))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                .add(labelStatus, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(20, 20, 20))
        );

        jToolBar3.add(jPanel1);

        menuBar2.setMaximumSize(new java.awt.Dimension(336, 30));
        menuBar2.setMinimumSize(new java.awt.Dimension(50, 21));

        fileMenu2.setText("File"); // NOI18N

        jMenuItem2.setText("Check Schema"); // NOI18N
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        fileMenu2.add(jMenuItem2);

        menuItemUsers.setText("Users");
        menuItemUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemUsersActionPerformed(evt);
            }
        });
        fileMenu2.add(menuItemUsers);
        fileMenu2.add(jSeparator1);

        jMenuItem1.setText("Log Out"); // NOI18N
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed1(evt);
            }
        });
        fileMenu2.add(jMenuItem1);

        exitMenuItem2.setText("Exit"); // NOI18N
        exitMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu2.add(exitMenuItem2);

        menuBar2.add(fileMenu2);

        workMenu.setText("Menu Work");

        jmwMIpres.setText("Prescales");
        jmwMIpres.setEnabled(false);
        jmwMIpres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPrescalesActionPerformed(evt);
            }
        });
        workMenu.add(jmwMIpres);

        jmwMIasps.setText("Assign Prescale Sets");
        jmwMIasps.setEnabled(false);
        jmwMIasps.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAssignPSActionPerformed(evt);
            }
        });
        workMenu.add(jmwMIasps);

        jmwMIcomm.setText("Add Comment");
        jmwMIcomm.setEnabled(false);
        jmwMIcomm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCommentActionPerformed(evt);
            }
        });
        workMenu.add(jmwMIcomm);

        jmwMIalia.setText("Set Alias");
        jmwMIalia.setEnabled(false);
        jmwMIalia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAliasNewActionPerformed(evt);
            }
        });
        workMenu.add(jmwMIalia);

        jmwMIasalia.setText("Assign Alias");
        jmwMIasalia.setEnabled(false);
        jmwMIasalia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmwMIasaliaActionPerformed(evt);
            }
        });
        workMenu.add(jmwMIasalia);
        workMenu.add(jSeparator3);

        jmwMIstrm.setText("View Streams");
        jmwMIstrm.setEnabled(false);
        jmwMIstrm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAliasActionPerformed(evt);
            }
        });
        workMenu.add(jmwMIstrm);

        jmwMIalgos.setText("View Algorithms");
        jmwMIalgos.setEnabled(false);
        jmwMIalgos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmwMIalgosActionPerformed(evt);
            }
        });
        workMenu.add(jmwMIalgos);

        jmwMIdiff.setText("Diff Menus");
        jmwMIdiff.setEnabled(false);
        jmwMIdiff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDiffActionPerformed(evt);
            }
        });
        workMenu.add(jmwMIdiff);

        menuBar2.add(workMenu);

        l1Menu.setText("Level 1");
        l1Menu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jl1MIrTMC.setText("Run TMC");
        jl1MIrTMC.setEnabled(false);
        jl1MIrTMC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonTMCActionPerformed(evt);
            }
        });
        l1Menu.add(jl1MIrTMC);

        jl1MIlBG.setText("Bunch Group Editor");
        jl1MIlBG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonBunchGroupActionPerformed(evt);
            }
        });
        l1Menu.add(jl1MIlBG);

        DumpSmxFiles.setText("Dump Smx Files");
        DumpSmxFiles.setToolTipText("");
        DumpSmxFiles.setEnabled(false);
        DumpSmxFiles.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DumpSmxFilesActionPerformed(evt);
            }
        });
        l1Menu.add(DumpSmxFiles);

        Monitors.setText("Monitoring Counters");
        Monitors.setToolTipText("");
        Monitors.setEnabled(false);
        Monitors.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MonitorsActionPerformed(evt);
            }
        });
        l1Menu.add(Monitors);

        ItemMonitors.setText("Per-Bunch Monitoring Counters");
        ItemMonitors.setEnabled(false);
        ItemMonitors.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ItemMonitorsActionPerformed(evt);
            }
        });
        l1Menu.add(ItemMonitors);

        CTPCables.setText("CTP Cables");
        CTPCables.setEnabled(false);
        CTPCables.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CTPCablesActionPerformed(evt);
            }
        });
        l1Menu.add(CTPCables);
        l1Menu.add(jSeparator5);

        jMenu2.setText("Multi-Partition Editors");

        jMenuPartitionPSP1.setText("Prescale Editor P1");
        jMenuPartitionPSP1.setToolTipText("");
        jMenuPartitionPSP1.setEnabled(false);
        jMenuPartitionPSP1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuPartitionPSP1ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuPartitionPSP1);

        jMenuPartitionPSP2.setText("Prescale Editor P2");
        jMenuPartitionPSP2.setToolTipText("");
        jMenuPartitionPSP2.setEnabled(false);
        jMenuPartitionPSP2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuPartitionPSP2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuPartitionPSP2);

        jMenuPartitionPSP3.setText("Prescale Editor P3");
        jMenuPartitionPSP3.setToolTipText("");
        jMenuPartitionPSP3.setEnabled(false);
        jMenuPartitionPSP3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuPartitionPSP3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuPartitionPSP3);
        jMenu2.add(jSeparator4);

        jMenuPartitionBGP1.setText("Bunch Group Editor P1");
        jMenuPartitionBGP1.setEnabled(false);
        jMenuPartitionBGP1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuPartitionBGP1ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuPartitionBGP1);

        jMenuPartitionBGP2.setText("Bunch Group Editor P2");
        jMenuPartitionBGP2.setEnabled(false);
        jMenuPartitionBGP2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuPartitionBGP2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuPartitionBGP2);

        jMenuPartitionBGP3.setText("Bunch Group Editor P3");
        jMenuPartitionBGP3.setEnabled(false);
        jMenuPartitionBGP3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuPartitionBGP3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuPartitionBGP3);

        l1Menu.add(jMenu2);

        menuBar2.add(l1Menu);

        hltMenu.setText("HLT"); // NOI18N

        jmwMIAlgoEdit.setText("Algorithm Parameter Editor"); // NOI18N
        jmwMIAlgoEdit.setEnabled(false);
        jmwMIAlgoEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmwMIAlgoEditActionPerformed(evt);
            }
        });
        hltMenu.add(jmwMIAlgoEdit);

        menuBar2.add(hltMenu);

        loadSaveMenu.setText("Load/Save");

        jlsMIreadXML.setText("Upload from XML");
        jlsMIreadXML.setEnabled(false);
        jlsMIreadXML.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonReadXMLActionPerformed(evt);
            }
        });
        loadSaveMenu.add(jlsMIreadXML);

        jlsMIwriteXML.setText("Download as XML");
        jlsMIwriteXML.setEnabled(false);
        jlsMIwriteXML.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonWriteXMLActionPerformed(evt);
            }
        });
        loadSaveMenu.add(jlsMIwriteXML);

        menuBar2.add(loadSaveMenu);

        jMenu1.setText("View"); // NOI18N

        menuItemViewLogs.setText("View Logs"); // NOI18N
        menuItemViewLogs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemViewLogsActionPerformed(evt);
            }
        });
        jMenu1.add(menuItemViewLogs);

        menuItemInternalWriteLock.setText("Write Lock"); // NOI18N
        menuItemInternalWriteLock.setMinimumSize(new java.awt.Dimension(115, 24));
        menuItemInternalWriteLock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemInternalWriteLockActionPerformed(evt);
            }
        });
        jMenu1.add(menuItemInternalWriteLock);

        LoggerMenuItem.setText("Log Window");
        LoggerMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LoggerMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(LoggerMenuItem);

        menuBar2.add(jMenu1);

        helpMenu2.setText("Help"); // NOI18N

        aboutMenuItem2.setText("About"); // NOI18N
        aboutMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItem2ActionPerformed(evt);
            }
        });
        helpMenu2.add(aboutMenuItem2);

        menuBar2.add(helpMenu2);

        setJMenuBar(menuBar2);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jToolBar3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(jSplitPane1)
            .add(jToolBar1, 0, 0, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(jToolBar1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 33, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jSplitPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 833, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jToolBar3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 25, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        dispose();
    }//GEN-LAST:event_exitMenuItemActionPerformed

    /**
     * The search button has been pressed. Look at the combo box and work out
     * what field and table the user wants to search. Then we also need to know
     * what they want to search for from the text box. Create the SQL Query,
     * send it off and collect the results.
     *
     * @param evt The event that fires this action.
     */
    private void buttonSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSearchActionPerformed
        //long start = System.nanoTime();
        try {
            resultsModel.clear();

            //PJB - This doesn't work, even Simon couldn't fix it, hence the mess in the FieldCombo
            //AbstractTable table = (AbstractTable) comboTable.getSelectedItem();
            //String query = ((AbstractTable) comboTable.getSelectedItem()).getQueryString();
            String finalquery = ConnectionManager.getInstance().fix_schema_name(query);

            String search = null;

            if (comboFieldSelect.getSelectedIndex() != 0) {
                finalquery += " WHERE " + table.tab_prefix() + comboFieldSelect.get_db_name() + " LIKE ? ";
                search = editSearchText.getText();
            }

            //if we search for hidden smks:
            if (table.tab_prefix().contains("SMT")) {
                if (showHidden) {
                    if (comboFieldSelect.getSelectedIndex() != 0) {
                        finalquery += " ORDER BY " + table.tablePrefix + "ID DESC";
                    } else {
                        finalquery += " ORDER BY " + table.tablePrefix + "ID DESC";
                    }
                } else if (comboFieldSelect.getSelectedIndex() != 0) {
                    finalquery += " AND " + table.tablePrefix + "USED=0 ORDER BY " + table.tablePrefix + "ID DESC";
                } else {
                    finalquery += " WHERE " + table.tablePrefix + "USED=0 ORDER BY " + table.tablePrefix + "ID DESC";
                }
            } else {
                finalquery += " ORDER BY " + table.tablePrefix + "ID DESC";
            }
            try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(finalquery)) {
                if (search != null) {
                    ps.setString(1, search);
                }

                try (ResultSet rset = ps.executeQuery()) {
                    while (rset.next()) {
                        AbstractTable copy = (AbstractTable) table.clone();
                        copy.loadFromRset(rset);
                        resultsModel.add(copy);
                    }
                    rset.close();
                }
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Exception: {0}", ex);
        }

        //Long time = System.nanoTime() - start;
        ////System.out.println("Time: " + (1. * time / (1000 * 1000 * 1000)) + " seconds");
        Runtime r = Runtime.getRuntime();
        ////System.out.println("Free memory 1: " + r.freeMemory());
        r.gc();
        ////System.out.println("Free memory 2: " + r.freeMemory());

    }//GEN-LAST:event_buttonSearchActionPerformed

    /**
     * Function to perform the search as if the user clicked on the search
     * button. Optionally selects the first item in the results table.
     *
     * @param updatefocus - if true selects the first item in the results table.
     */
    public void doSearch(boolean updatefocus) {
        buttonSearch.doClick();
        if (updatefocus) {
            tableSearchResults.changeSelection(0, 0, false, false);
            reload(tableSearchResults.getSelectedRow());
        }
    }

    /**
     * The user wants to switch between the tables in the database using the
     * selection box in the top left.
     *
     * @param evt The event that fires this action.
     */
    private void comboTableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboTableActionPerformed

        jToggleButtonHidden.setEnabled(false);
        jButtonHide.setEnabled(false);

        selectedTable = null;
        treeTrigger.clear();
        resultsModel.clear();
        rowModel.clear();

        query = "";
        if (comboTable.getSelectedItem().equals("L1Master")) {
            table = new L1Master();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1PrescaleSet")) {
            table = new L1Prescale();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1MuonThresholdSet")) {
            table = new L1MuonThresholdSet();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1BunchGroupSet")) {
            table = new L1BunchGroupSet();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1BunchGroup")) {
            table = new L1BunchGroup();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1Menu")) {
            table = new L1Menu();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1Item")) {
            table = new L1Item();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1Threshold")) {
            table = new L1Threshold();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1ThresholdValue")) {
            table = new L1ThresholdValue();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1CaloInfo")) {
            table = new L1CaloInfo();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1CaloSinCos")) {
            table = new L1CaloSinCos();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1CaloMinTob")) {
            table = new L1CaloMinTob();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1CaloIsolation")) {
            table = new L1CaloIsolation();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1CaloIsoParam")) {
            table = new L1CaloIsoParam();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1CTPFiles")) {
            table = new L1CtpFiles();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1CTPSMX")) {
            table = new L1CtpSmx();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1Random")) {
            table = new L1Random();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1MuctpiInfo")) {
            table = new L1MuctpiInfo();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1PrescaledClock")) {
            table = new L1PrescaledClock();
            query = table.getQueryString();
        }
        ///////////////////////////////////////////////////////////        
        if (comboTable.getSelectedItem().equals("TopoMaster")) {
            table = new TopoMaster();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("TopoAlgoInput")) {
            table = new TopoAlgoInput();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("TopoAlgo")) {
            table = new TopoAlgo();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("TopoAlgoOutput")) {
            table = new TopoAlgoOutput();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("TopoConfig")) {
            table = new TopoConfig();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("TopoGeneric")) {
            table = new TopoGeneric();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("TopoMenu")) {
            table = new TopoMenu();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("TopoOutputLine")) {
            table = new TopoOutputLine();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("TopoOutputList")) {
            table = new TopoOutputList();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("TopoParameter")) {
            table = new TopoParameter();
            query = table.getQueryString();
        }
        ///////////////////////////////////////////////////////////
        if (comboTable.getSelectedItem().equals("HLTMaster")) {
            table = new HLTMaster();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("HLTPrescale")) {
            table = new HLTPrescale();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("HLTPrescaleSet")) {
            table = new HLTPrescaleSet();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("HLTTriggerMenu")) {
            table = new HLTTriggerMenu();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("HLTTriggerChain")) {
            table = new HLTTriggerChain();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("HLTTriggerSignature")) {
            table = new HLTTriggerSignature();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("HLTTriggerElement")) {
            table = new HLTTriggerElement();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("HLTTriggerGroup")) {
            table = new HLTTriggerGroup();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("HLTTriggerStream")) {
            table = new HLTTriggerStream();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("HLTTriggerType")) {
            table = new HLTTriggerType();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("HLTRelease")) {
            table = new HLTRelease();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("HLTSetup")) {
            table = new HLTSetup();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("HLTComponent")) {
            table = new HLTComponent();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("HLTParameter")) {
            table = new HLTParameter();
            query = table.getQueryString();
        }
    
        /////////////////////////////////////////////////////////
        if (comboTable.getSelectedItem().equals("SuperMaster")) {
            table = new SuperMasterTable();
            query = table.getQueryString();
            //see hidden sets
            jToggleButtonHidden.setEnabled(true);
            //expert
            if (userMode == UserMode.L1EXPERT || userMode.ordinal() >= UserMode.EXPERT.ordinal()) {
                jButtonHide.setEnabled(true);
            }

        }
        comboFieldSelect.removeAllItems();
        comboFieldSelect.getFields(table);
        editSearchText.setText("");
    }//GEN-LAST:event_comboTableActionPerformed

    /**
     * Call the trigger menu compiler if it's in the path and compiled. Note
     * that the TM_TT link table must be properly filled first. And that this
     * will create some files, including the L1 Menu as XML.
     *
     * @param evt The event that fires this action.
     */
    /**
     * When the user clicks on the search box to enter a term, clear it.
     *
     * @param evt The event that fires this action.
     */
    private void searchFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_searchFocusGained
        if (editSearchText.getText().equals("Search")) {
            editSearchText.setText("");
        }
    }//GEN-LAST:event_searchFocusGained

    /**
     * When the user exits the search box, and it contains nothing helpful, put
     * the word search there.
     *
     * @param evt The event that fires this action.
     */
    private void searchFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_searchFocusLost
        if (editSearchText.getText().isEmpty()) {
            editSearchText.setText("Search");
        }
    }//GEN-LAST:event_searchFocusLost

    /**
     * The user wants to see what's defined in the schema they're currently
     * connected to.
     *
     * @param evt The event that fires this action.
     */
    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        SchemaCheck schemaCheck = new SchemaCheck(this);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    /**
     * The user wants to view or fiddle with prescales.
     *
     * @param evt The event that fires this action.
     */
    private void buttonPrescalesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPrescalesActionPerformed
        if (selectedTable instanceof SuperMasterTable) {
            try {
                this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                PrescaleEditMasterPanel pe = new PrescaleEditMasterPanel((SuperMasterTable) selectedTable, this, getOpenWindowCount(0));
                pe.setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.setCursor(Cursor.getDefaultCursor());
            }
        }
    }//GEN-LAST:event_buttonPrescalesActionPerformed

    /**
     *
     * @param partition
     */
    public void incrementpswindows(int partition) {
        psWindows[partition]++;
    }

    /**
     *
     * @param partition
     */
    public void decrementpswindows(int partition) {
        psWindows[partition]--;
    }

    private int getOpenWindowCount(int partition) {
        int totalWindows = 0;
        if (partition == 0) {
            for (int counter = 0; counter < 4; counter++) {
                totalWindows += psWindows[counter];
            }
        } else {
            totalWindows = psWindows[0] + psWindows[partition];
        }
        return totalWindows;
    }

    /**
     * Show some information about the TriggerTool and the connection.
     *
     * @param evt The event that fires this action.
     */
    private void aboutMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItem2ActionPerformed
        AboutBox aboutBox = new AboutBox(this);
    }//GEN-LAST:event_aboutMenuItem2ActionPerformed

    /**
     * View the contents of the log table in the schema.
     *
     * @param evt The event that fires this action.
     */
    private void menuItemViewLogsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemViewLogsActionPerformed
        LogViewer logViewer = new LogViewer(this);
    }//GEN-LAST:event_menuItemViewLogsActionPerformed

    /**
     * Monitor the Internal lock mechanism
     *
     * @param evt The event that fires this action.
     */
    private void menuItemInternalWriteLockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemInternalWriteLockActionPerformed
        InternalWriteLockDialog internalWriteLockDialog = new InternalWriteLockDialog(this,true);
    }//GEN-LAST:event_menuItemInternalWriteLockActionPerformed

    private void jMenuItem1ActionPerformed1(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed1
        ConnectionManager mgr = ConnectionManager.getInstance();
        boolean online = mgr.getInitInfo().getOnline();
        boolean onlineDB = mgr.getInitInfo().getOnlineDB();
        try {
            // disconnect form db.
            mgr.disconnect();
        } catch (SQLException ex) {
            String msg = " Problems disconnecting form DB.\n"
                    + ex.getLocalizedMessage();
            JOptionPane.showMessageDialog(null, ex.getMessage(),
                    msg, JOptionPane.WARNING_MESSAGE);
            logger.severe(msg);
        }
        dispose();
        InitDialog.loadFromGui(online, onlineDB);
    }//GEN-LAST:event_jMenuItem1ActionPerformed1

    private void menuItemUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemUsersActionPerformed
        //for shifter
        if (userMode == UserMode.DBA) {
            new UsersDialogExpert(this);
        } else {
            //for expert
            new UsersDialogShifter(this);
        }
        //for user the button is anyway diabled.
    }//GEN-LAST:event_menuItemUsersActionPerformed

    private void buttonAssignPSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAssignPSActionPerformed
        // option to assign any ps to any menu
        try {
            AssignPrescales ap = new AssignPrescales((SuperMasterTable) selectedTable, this);
            ap.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_buttonAssignPSActionPerformed

    private void buttonCommentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCommentActionPerformed
        if (selectedTable instanceof SuperMasterTable) {
            try {
                SuperMasterTableInteractor smt = new SuperMasterTableInteractor(new SuperMasterTable(selectedTable.get_id()));
                smt.editComment(this);
            } catch (SQLException ex) {
                TTExceptionHandler.HandleSqlException(ex, rootPane, "Error while trying to save the comment in the super master table.");
            }
        }
    }//GEN-LAST:event_buttonCommentActionPerformed

    /**
     * The user is on the SuperMaster screen and wants to write XML files for
     * this configuration.
     *
     * @param evt The event that fires this action.
     */
    private void buttonWriteXMLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonWriteXMLActionPerformed
        if (selectedTable instanceof SuperMasterTable) {
            DownloadXML downloadXML = new DownloadXML(this, selectedTable.get_id());
        }
    }//GEN-LAST:event_buttonWriteXMLActionPerformed

    private void buttonAliasNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAliasNewActionPerformed
        //PJB new alias dialog
        if (selectedTable instanceof SuperMasterTable) {
            AliasDialog aliasDialog = new AliasDialog(this, (SuperMasterTable) selectedTable, aliasWindowActive);
        }
    }//GEN-LAST:event_buttonAliasNewActionPerformed

    private void buttonAliasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAliasActionPerformed
        //PJB we don't have an alias button yet, hijack this for the stream displayer
        //new AliasDialog(this);
        try {
            StreamsDialog st = new StreamsDialog((SuperMasterTable) selectedTable, this);
//            st.setModal(true);
        } catch (SQLException ex) {
            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_buttonAliasActionPerformed

    /**
     * The user wants to diff some configurations.
     *
     * @param evt The event that fires this action.
     */
    private void buttonDiffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDiffActionPerformed
        Object[] options = {"Diff supermaster tables", "Diff individual objects", "Cancel"};
        int n = JOptionPane.showOptionDialog(this, "Select diffing mode:", "Diff mode selection", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[2]);
        if (n == 0) {
            try {
                DiffDisplayDialog diffDisplayDialog = new DiffDisplayDialog(this);
            } catch (SQLException ex) {
                TTExceptionHandler.HandleSqlException(ex, rootPane, "Error while trying to save the comment in the super master table.");
            }
        } else if (n == 1) {
            DiffObjectSel diffObjectSel = new DiffObjectSel(this);
        }
    }//GEN-LAST:event_buttonDiffActionPerformed

    /**
     * The user is on the SuperMaster screen and wants to read XML files into a
     * new key (or if it already exists an existing one).
     *
     * @param evt The event that fires this action.
     */
    private void buttonReadXMLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonReadXMLActionPerformed
        if (comboTable.getSelectedItem().equals("SuperMaster")) {
            UploadXML uploader = new UploadXML(this);

            if (uploader.getSuperMasterTable() != null && uploader.getSuperMasterTable().get_id() > 0) {
                if (!resultsModel.contains(uploader.getSuperMasterTable())) {
                    resultsModel.add(uploader.getSuperMasterTable());
                }
            }
            doSearch(true); // update the main panel with the new SMK
        }
    }//GEN-LAST:event_buttonReadXMLActionPerformed
    

    /**
     * The user wants to create a copy of this configuration in another
     * database. Keeps all key numbers
     *
     * @param evt The event that fires this action.
     */
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

    }//GEN-LAST:event_jButton2ActionPerformed

private void buttonTMCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonTMCActionPerformed
        try {
            DoTMCActionPerformed();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, rootPane, "Error while performing TMC action in main panel.");
        }
    }

    private void DoTMCActionPerformed() throws SQLException {

        if (!(selectedTable instanceof SuperMasterTable)) {
            return;
        }

        labelStatus.setText("Trigger Menu Compiler Called");

        SuperMasterTable smt = (SuperMasterTable) selectedTable;

        if (smt.get_l1_master_table().get_menu().getTMTT().isEmpty()) {
            JOptionPane.showMessageDialog(this, "You must assign cables first", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (smt.get_l1_master_table().get_menu().getMonitors().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Please create Monitors first", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        CTPFileCreatorDialog cfcd = new CTPFileCreatorDialog(this, smt);
        cfcd.setVisible(true);
}//GEN-LAST:event_buttonTMCActionPerformed

private void buttonBunchGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonBunchGroupActionPerformed
    //open BG editor
    //BunchGroupEdit bge = new BunchGroupEdit(this);
    BunchGroupEditMasterPanel bge = new BunchGroupEditMasterPanel(this);
}//GEN-LAST:event_buttonBunchGroupActionPerformed

private void jmwMIalgosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmwMIalgosActionPerformed
    try {
        AlgosDialog ag = new AlgosDialog((SuperMasterTable) selectedTable, this);
    } catch (SQLException ex) {
        Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
    }
}//GEN-LAST:event_jmwMIalgosActionPerformed

private void jButtonHideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHideActionPerformed
        try {
            DoButtonHideActionPerformed();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, rootPane, "Error after while performing Hide action in Main panel.");
        } catch (NumberFormatException ex) {
            String message = "One of the string is not a number.";
            logger.log(Level.SEVERE, message, ex);
            JOptionPane.showMessageDialog(rootPane, "NumberFormatException", message
                    + " '\n Please report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, JOptionPane.ERROR_MESSAGE);
        }
    }

    private void DoButtonHideActionPerformed() throws SQLException, NumberFormatException {
        int length = tableSearchResults.getSelectedRows().length;
        int[] Rows = tableSearchResults.getSelectedRows();
        resultsModel = (ResultsTableModel) tableSearchResults.getModel();
        if (length == 1) {//allows us to deal with error state entries
            int smt_id = ((SuperMasterTable) selectedTable).get_id();
            //if the text says hide, we hide it:
            int value;
            if (jButtonHide.getText().equals("Hide")) {
                value = 1;
            } else {
                value = 0;
            }
            ((SuperMasterTable) selectedTable).UpdateSmtUsed(value, smt_id);
        }
        if (length > 0) {//multi selected rows
            for (int i = Rows[0]; i < Rows[0] + length; ++i) {
                String smt_id_s = resultsModel.getValueAt(i, 0).toString();
                if (smt_id_s.toLowerCase().equals("error")) {
                    continue;
                }
                int smt_id = Integer.parseInt(smt_id_s);
                //same as for the prescale sets - toggle the used flag to see if hidden or not!
                int value;
                //if the text says hide, we hide it:
                if (jButtonHide.getText().equals("Hide")) {
                    value = 1;
                } else {
                    value = 0;
                }
                ((SuperMasterTable) selectedTable).UpdateSmtUsed(value, smt_id);
            }
            doSearch(false);
        }

}//GEN-LAST:event_jButtonHideActionPerformed

private void jToggleButtonHiddenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButtonHiddenActionPerformed
    if (jToggleButtonHidden.isSelected()) {
        showHidden = true;
        doSearch(false);
        jButtonHide.setText("Show");
    } else {
        showHidden = false;
        doSearch(false);
        jButtonHide.setText("Hide");
    }
}//GEN-LAST:event_jToggleButtonHiddenActionPerformed
    /*
     * When the Menu Item 'View-Logger' has an action performed upon it the
     * loggerwindow created by the windowhandler is made visible. @param evt The
     * event that fires this action
     */
private void LoggerMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LoggerMenuItemActionPerformed
    triggertool.Panels.LoggerWindow logWindow = TriggerTool.getWindowhandler().getWindow();
    logWindow.setLocationRelativeTo(this);
    logWindow.setVisible(true);
    logger.finest("Logger Window Opened");
}//GEN-LAST:event_LoggerMenuItemActionPerformed

    private void MonitorsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MonitorsActionPerformed
        if (selectedTable instanceof SuperMasterTable) {
            L1Monitoring l1Monitoring = new L1Monitoring(this, (SuperMasterTable) selectedTable);
            this.buttonSearch.doClick();
        }
    }//GEN-LAST:event_MonitorsActionPerformed

    private void ItemMonitorsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ItemMonitorsActionPerformed
        if (selectedTable instanceof SuperMasterTable) {
            L1ItemMonitoring l1ItemMonitoring = new L1ItemMonitoring(this, (SuperMasterTable) selectedTable);
            this.buttonSearch.doClick();
        }
    }//GEN-LAST:event_ItemMonitorsActionPerformed


    private void jMenuPartitionPSP1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuPartitionPSP1ActionPerformed
        if (selectedTable instanceof SuperMasterTable) {
            L1PartitionPrescaleEdit pe = new L1PartitionPrescaleEdit((SuperMasterTable) selectedTable, this, getOpenWindowCount(1), 1);
        }
	    }//GEN-LAST:event_jMenuPartitionPSP1ActionPerformed

	    private void jMenuPartitionPSP2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuPartitionPSP2ActionPerformed
                if (selectedTable instanceof SuperMasterTable) {
                    L1PartitionPrescaleEdit pe = new L1PartitionPrescaleEdit((SuperMasterTable) selectedTable, this, getOpenWindowCount(2), 2);
                }
	    }//GEN-LAST:event_jMenuPartitionPSP2ActionPerformed

	    private void jMenuPartitionPSP3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuPartitionPSP3ActionPerformed
                if (selectedTable instanceof SuperMasterTable) {
                    L1PartitionPrescaleEdit pe = new L1PartitionPrescaleEdit((SuperMasterTable) selectedTable, this, getOpenWindowCount(3), 3);
                }
	    }//GEN-LAST:event_jMenuPartitionPSP3ActionPerformed


    private void jmwMIAlgoEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmwMIAlgoEditActionPerformed
        if (selectedTable instanceof SuperMasterTable) {
            try {
                HLTEditSetup hltEditSetup = new HLTEditSetup(this, (SuperMasterTable) selectedTable);
            } catch (SQLException ex) {
                TTExceptionHandler.HandleSqlException(ex, rootPane, "Error loading HLt Setup editors from the Main panel.");
            }
            this.buttonSearch.doClick();
        }
    }//GEN-LAST:event_jmwMIAlgoEditActionPerformed

     private void CTPCablesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CTPCablesActionPerformed
         try {
             if (selectedTable instanceof SuperMasterTable) {
                 L1Cable l1Cable = new L1Cable(this, (SuperMasterTable) selectedTable);
             }
         } catch (SQLException ex) {
             Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
         } catch (BadLocationException ex) {
             Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
         }
 		    }//GEN-LAST:event_CTPCablesActionPerformed

    private void jMenuPartitionBGP1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuPartitionBGP1ActionPerformed
        if (selectedTable instanceof SuperMasterTable) {
            BunchGroupEditMasterPanel bge = new BunchGroupEditMasterPanel(this, selectedTable.get_id(), 1);
            bge.setVisible(true);
        }
    }//GEN-LAST:event_jMenuPartitionBGP1ActionPerformed

    private void jMenuPartitionBGP2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuPartitionBGP2ActionPerformed
        if (selectedTable instanceof SuperMasterTable) {
            BunchGroupEditMasterPanel bge = new BunchGroupEditMasterPanel(this, selectedTable.get_id(), 2);
            bge.setVisible(true);
        }
    }//GEN-LAST:event_jMenuPartitionBGP2ActionPerformed

    private void jMenuPartitionBGP3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuPartitionBGP3ActionPerformed
        if (selectedTable instanceof SuperMasterTable) {
            BunchGroupEditMasterPanel bge = new BunchGroupEditMasterPanel(this, selectedTable.get_id(), 3);
            bge.setVisible(true);
        }
    }//GEN-LAST:event_jMenuPartitionBGP3ActionPerformed

    private void tabStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_tabStateChanged
        if (tab.getSelectedIndex() == 1 && selectedTable instanceof SuperMasterTable) {
            SuperMasterTable smt = (SuperMasterTable) selectedTable;
            quickMenuView.drawSuperMaster(smt);
        }
    }//GEN-LAST:event_tabStateChanged

    private void jmwMIasaliaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmwMIasaliaActionPerformed
           if (selectedTable instanceof SuperMasterTable) {
               try {
                   AssignAliasDialog aliasAssignmentDialog = new AssignAliasDialog((SuperMasterTable) selectedTable, this, aliasAssignWindowActive);
                   aliasAssignmentDialog.setVisible(true);
               } catch (SQLException ex) {
                   Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
               }
        }

    }//GEN-LAST:event_jmwMIasaliaActionPerformed

    private void DumpSmxFilesActionPerformed(java.awt.event.ActionEvent evt) {                                             
        if (selectedTable instanceof SuperMasterTable) {
            L1DumpSmxFiles l1DumpSmxFiles = new L1DumpSmxFiles(null, (SuperMasterTable) selectedTable);
            this.buttonSearch.doClick();
        }
    }         
    
    public void lockAlias(){
        aliasWindowActive = true;
    }

    public void unlockAlias(){
        aliasWindowActive = false;
    }

        
    public void lockAliasAssign(){
        aliasAssignWindowActive = true;
    }

    public void unlockAliasAssign(){
        aliasAssignWindowActive = false;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem CTPCables;
    private javax.swing.JMenuItem DumpSmxFiles;
    private javax.swing.JMenuItem ItemMonitors;
    private javax.swing.JMenuItem LoggerMenuItem;
    private javax.swing.JMenuItem Monitors;
    private javax.swing.JMenuItem aboutMenuItem2;
    private javax.swing.JButton buttonSearch;
    public triggertool.Components.FieldComboBox comboFieldSelect;
    public javax.swing.JComboBox<String> comboTable;
    public javax.swing.JTextField editSearchText;
    private javax.swing.JMenuItem exitMenuItem2;
    private javax.swing.JMenu fileMenu2;
    private javax.swing.JMenu helpMenu2;
    private javax.swing.JMenu hltMenu;
    private javax.swing.JButton jButtonHide;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuPartitionBGP1;
    private javax.swing.JMenuItem jMenuPartitionBGP2;
    private javax.swing.JMenuItem jMenuPartitionBGP3;
    private javax.swing.JMenuItem jMenuPartitionPSP1;
    private javax.swing.JMenuItem jMenuPartitionPSP2;
    private javax.swing.JMenuItem jMenuPartitionPSP3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JToggleButton jToggleButtonHidden;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar3;
    private javax.swing.JMenuItem jl1MIlBG;
    private javax.swing.JMenuItem jl1MIrTMC;
    private javax.swing.JMenuItem jlsMIreadXML;
    private javax.swing.JMenuItem jlsMIwriteXML;
    private javax.swing.JMenuItem jmwMIAlgoEdit;
    private javax.swing.JMenuItem jmwMIalgos;
    private javax.swing.JMenuItem jmwMIalia;
    private javax.swing.JMenuItem jmwMIasalia;
    private javax.swing.JMenuItem jmwMIasps;
    private javax.swing.JMenuItem jmwMIcomm;
    private javax.swing.JMenuItem jmwMIdiff;
    private javax.swing.JMenuItem jmwMIpres;
    private javax.swing.JMenuItem jmwMIstrm;
    private javax.swing.JMenu l1Menu;
    private javax.swing.JLabel labelStatus;
    private javax.swing.JMenu loadSaveMenu;
    private javax.swing.JMenuBar menuBar2;
    private javax.swing.JMenuItem menuItemInternalWriteLock;
    private javax.swing.JMenuItem menuItemUsers;
    private javax.swing.JMenuItem menuItemViewLogs;
    private triggertool.Panels.QuickMenuView quickMenuView;
    private javax.swing.JTabbedPane tab;
    private javax.swing.JScrollPane tabMenu;
    private javax.swing.JPanel tabTree;
    private javax.swing.JTable tableSearchResults;
    private javax.swing.JMenu workMenu;
    // End of variables declaration//GEN-END:variables

    private void setup() {
        InitInfo theInitInfo = ConnectionManager.getInstance().getInitInfo();

        String server = theInitInfo.getdbServer();

        if (server.indexOf('@') > 0) {
            server = server.substring(server.indexOf('@') + 1);
        }

        if (server.indexOf(':') > 0) {
            server = server.substring(0, server.indexOf(':'));
        }

        String version = triggertool.TriggerTool.TT_TAG;

        String titleString = "TriggerTool " + version + " : " + theInitInfo.getUserMode() + " " + theInitInfo.getSystemUsername() + " using " + theInitInfo.getTechnology() + " (" + theInitInfo.getUserName() + "@" + server + ")";

        userMode = theInitInfo.getUserMode();

        setTitle(titleString);
    }

    private void setupComboBox() {

        comboTable.addItem("SuperMaster");
        comboTable.addItem("L1PrescaleSet");
        comboTable.addItem("HLTPrescaleSet");
        comboTable.addItem("HLTRelease");

        if (userMode == UserMode.USER || userMode.ordinal() >= UserMode.EXPERT.ordinal()) {

            comboTable.addItem("L1Master");
            comboTable.addItem("L1BunchGroupSet");
            comboTable.addItem("L1MuonThresholdSet");
            comboTable.addItem("L1BunchGroup");

            comboTable.addItem("L1Menu");
            comboTable.addItem("L1Item");
            comboTable.addItem("L1Threshold");
            comboTable.addItem("L1ThresholdValue");

            comboTable.addItem("L1CaloInfo");
            comboTable.addItem("L1CaloSinCos");
            comboTable.addItem("L1CaloMinTob");                 
            comboTable.addItem("L1CaloIsolation");                   
            comboTable.addItem("L1CaloIsoParam");   

            comboTable.addItem("L1CTPFiles");
            comboTable.addItem("L1CTPSMX");
            comboTable.addItem("L1Random");
            comboTable.addItem("L1MuctpiInfo");
            comboTable.addItem("L1PrescaledClock");

            comboTable.addItem("TopoMaster");
            comboTable.addItem("TopoAlgoInput");  
            comboTable.addItem("TopoAlgo");  
            comboTable.addItem("TopoAlgoOutput");  
            comboTable.addItem("TopoConfig");  
            comboTable.addItem("TopoGeneric");   
            comboTable.addItem("TopoMenu");  
            comboTable.addItem("TopoOutputLine");  
            comboTable.addItem("TopoOutputList");  
            comboTable.addItem("TopoParameter");
        
            comboTable.addItem("HLTMaster");

            comboTable.addItem("HLTTriggerMenu");
            comboTable.addItem("HLTTriggerChain");
            comboTable.addItem("HLTPrescale");
            comboTable.addItem("HLTTriggerSignature");
            comboTable.addItem("HLTTriggerElement");
            comboTable.addItem("HLTTriggerGroup");
            comboTable.addItem("HLTTriggerStream");
            comboTable.addItem("HLTTriggerType");

            comboTable.addItem("HLTSetup");
            comboTable.addItem("HLTComponent");
            comboTable.addItem("HLTParameter");

            comboTable.addItem("HLTRuleSet");
            comboTable.addItem("HLTRule");
            comboTable.addItem("HLTRuleComponent");
            comboTable.addItem("HLTRuleParameter");
        }
    }
}
