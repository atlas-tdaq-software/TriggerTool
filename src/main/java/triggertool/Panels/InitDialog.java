package triggertool.Panels;

import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.PopupMenuListener;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.DBConnections;
import triggerdb.Connections.DBTechnology;
import triggerdb.Connections.DotTriggerToolEntry;
import triggerdb.Connections.InitInfo;
import triggerdb.Connections.UserMode;
import triggertool.TriggerTool;

/**
 * Login Dialog. Sets up the connection to DB. It takes connection info from
 * user (via GUI) and from several config files (~/.triggertool,
 * ~tdaq/.triggertool...).
 *
 * @author Simon Head
 */
public final class InitDialog extends javax.swing.JDialog {

    /**
     * Required by Java.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Message Log.
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    /**
     * Constants with the different user roles on DB. This is used by the
     * JComboBox comboSelectConn
     */
    private static final String ROLE_USER = "User";
    private static final String ROLE_SHIFTER = "Shifter";
    private static final String ROLE_EXPERT = "Expert";
    private static final String ROLE_DBA = "DBA";
    private static final ArrayList<String> ROLES = new ArrayList<>();

    static {
        ROLES.add(ROLE_USER);
        ROLES.add(ROLE_SHIFTER);
        ROLES.add(ROLE_EXPERT);
        ROLES.add(ROLE_DBA);
    }
    /**
     * Store the connection information here.
     */
    private InitInfo theInitInfo;
    /**
     * File chooser
     */
    private final JFileChooser fc = new JFileChooser(System.getProperty("user.dir"));

    /**
     *
     * @param online
     * @param onlineDB
     */
    public InitDialog(final boolean online, final boolean onlineDB) {
        super((java.awt.Dialog)null);
        this.theInitInfo = new InitInfo();
        this.theInitInfo.setUserMode(UserMode.EXIT);
        this.theInitInfo.setOnline(online);
        this.theInitInfo.setOnlineDB(onlineDB);
        this.theInitInfo.setDbMinimunSchema(TriggerTool.TRIGGER_DB_MIN_SCHEMA);
        this.theInitInfo.SetIsCommandLine(TriggerTool.isCommandLineMode());

        initComponents();

        initPanel();
        
    }

    private void initPanel() throws HeadlessException {
        MainPanel.SetIconImage(this);
        
        labelInfo.setText("");
       
        initializeDatabaseDropdown();

        panelConnParam.setVisible(false);
        getRootPane().setDefaultButton(buttonLogin);
        myComboSelectConnActionPerformed();
        labelVersion.setText("<html><B>TriggerTool " + triggertool.TriggerTool.TT_TAG + "</B></html>");

        if (theInitInfo.getOnline()) {
            labelDatabaseInfo.setText("Online Trigger Database");
        } else {
            labelDatabaseInfo.setText("Replica of the Online Trigger Database");
            Object userItem = comboSelectConn.getItemAt(0);
            comboSelectConn.removeAllItems();
            comboSelectConn.addItem(userItem.toString());
        }
        setModal(true);

        //MessageDialog.showReportDialog("Test Title", "Test message");

        fc.setCurrentDirectory(new File("."));

        // start in the center of the screen
        Point screenCenter = java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
        this.setLocation(screenCenter.x-getWidth()/2,screenCenter.y-getHeight()/2);
        
        setVisible(true);
    }

    private void initializeDatabaseDropdown() {

        ActionListener al = new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                DotTriggerToolEntry entry = (DotTriggerToolEntry) e.getSource();
                if (entry.getTechnology() == DBTechnology.DBLOOKUP) {
                    try {
                        theInitInfo.setConnectionFromCommandLine(entry.getDbhost());
                    } catch (Exception ex) {
                        logger.log(Level.SEVERE, "Can not interpret alias: {0}[{1}]", new Object[]{entry.getDbhost(), ex.getMessage()});
                        System.exit(-1);
                    }
                    theInitInfo.setUserMode(entry.isReadonly() ? UserMode.USER : UserMode.EXPERT);
                    theInitInfo.setgoodInfo(true);
                    logger.log(Level.INFO, "\n{0}", theInitInfo);
                    dispose();
                    return;
                } else {
                    theInitInfo.setTechnology(entry.getTechnology());
                    theInitInfo.setUserName(entry.getDbaccount());
                    theInitInfo.setdbHost(entry.getDbhost());
                    theInitInfo.setSchemaName(entry.getDbschema());
                    theInitInfo.setPassWord(entry.getDbpasswd());
                    theInitInfo.setUserMode(UserMode.EXPERT);
                }
                theInitInfo.setSystemUsername(System.getProperty("user.name")); // we have to take whoever is running the TriggerTool

                String connection = theInitInfo.getSchemaName() + "<br>" + theInitInfo.getUserName() + "<br>" + theInitInfo.getdbServer();

                labelDatabaseInfo.setText("<html><p align=\"center\">" + connection + "</p></html>");
                jPanel1.setVisible(true);
                comboSelectConn.setSelectedIndex(-1);
                comboSelectConn.setVisible(false);
                textPassword.setEnabled(true);
                labelPassword.setText("Password:");
                textPassword.setText(entry.getDbpasswd());
                textAtonrUsername.setVisible(false);
                labelAtonrUsername.setVisible(false);
                panelConnParam.setVisible(false);
                if (theInitInfo.getPassWord().length() != 0) {
                    buttonLogin.setEnabled(true);
                    buttonLogin.doClick();
                }
            }
        };


        DBConnections.setOnline(this.theInitInfo.getOnline());

        // the entries for the drop-down menus
        ArrayList<DotTriggerToolEntry> oracle_dbl = new ArrayList<>();
        ArrayList<DotTriggerToolEntry> oracle_dtt = new ArrayList<>(20);

        DBConnections.createDropDownEntriesFromDBLookup(oracle_dbl);
        DBConnections.createDropDownEntriesFromDotTriggerTool(oracle_dtt);

        for (DotTriggerToolEntry e : oracle_dbl) {
            e.addActionListener(al);
            menuOracle2.add(e);
        }
        if (oracle_dtt.size() > 0) {
            menuOracle2.addSeparator();
        }
        for (DotTriggerToolEntry e : oracle_dtt) {
            e.addActionListener(al);
            menuOracle2.add(e);
        }
        
        if(theInitInfo.getPassWord()==null || theInitInfo.getPassWord().isEmpty()) {
            for(DotTriggerToolEntry e : oracle_dbl) {
                if(e.isReadonly() && e.getAlias().equals("TRIGGERDB")) {
                    try {
                        theInitInfo.setPassWord( e.getInitInfo().getPassWord() );
                    } catch (Exception ex) {
                        Logger.getLogger(InitDialog.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    /**
     * Returns an object that contains the full configuration information.
     *
     * @return the initInfo object with the connection parameters.
     */
    public InitInfo getInitInfo() {
        return theInitInfo;
    }

    /**
     * Runs the connection procedure: prompts user for login data, creates a new
     * InitInfo object, connect to DB and launches a new MainPanel.
     *
     * @param online whether we are at P1.
     * @param onlineDB connecting to P1 DB or Replica.
     * @return
     * <code>true</code> if everything OK.
     */
    public static boolean loadFromGui(final boolean online,
            final boolean onlineDB) {
        boolean ok = false;
        InitInfo initInfo = null;
        InitDialog theInitDialog = null;

        while (!ok) {
            boolean exThrown = false;
            if (theInitDialog == null) {
                theInitDialog = new InitDialog(online, onlineDB);
            } else {
                theInitDialog.setVisible(true);
            }

            initInfo = theInitDialog.getInitInfo();

            if (!initInfo.getgoodInfo() || initInfo.getUserMode() == UserMode.EXIT) {
                initInfo.setUserMode(UserMode.EXIT);
                break;
            }

            try {
                ConnectionManager mgr = ConnectionManager.getInstance();
                ok = mgr.connect(initInfo);
            } catch (SQLException ex) {
                exThrown = true;
                logger.log(Level.SEVERE, "{0} Connection info:\n{1}System user: {2}", new Object[]{ex.getMessage(), initInfo.toString(), initInfo.getSystemUsername()});
                String msg = ex.getMessage();

                // Handling of "Account is Locked Exception", it happens if the
                // user tries more than 4/5 times a wrong pass
                // Note: This is the ORACLE code. 
                int err = ex.getErrorCode();
                if (err == 28000) {
                    msg += "\n You typed a wrong pwd more than 4 times.";
                    msg += "\n Close the TriggerTool and wait 2-3 minutes"
                            + "\n until the account unlocks.";

                }
                JOptionPane.showMessageDialog(null, msg,
                        "Connection Error", JOptionPane.WARNING_MESSAGE);
            }
            // This is redundant if there was already an exception.
            if (!ok && !exThrown) {
                JOptionPane.showMessageDialog(null, "Could not connect",
                        "Connection Error", JOptionPane.WARNING_MESSAGE);
            }

        }

        if (initInfo != null && theInitDialog != null &&   initInfo.getUserMode() != UserMode.EXIT) {
            theInitDialog.dispose();
            MainPanel mp = new MainPanel();
            mp.setLocationRelativeTo(theInitDialog);
            mp.setVisible(true);
        }

        return ok;
    }

    ///Belongs to Netbeans.
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelDatabaseInfo = new javax.swing.JLabel();
        panelConnParam = new javax.swing.JPanel();
        textHost = new javax.swing.JTextField();
        textDB = new javax.swing.JTextField();
        labelHost = new javax.swing.JLabel();
        labelDbName = new javax.swing.JLabel();
        labelUsername = new javax.swing.JLabel();
        labelTechnology = new javax.swing.JLabel();
        textUsername = new javax.swing.JTextField();
        labelTechnologySelected = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        labelInfo = new javax.swing.JLabel();
        labelVersion = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        labelPassword = new javax.swing.JLabel();
        textPassword = new javax.swing.JPasswordField();
        labelAtonrUsername = new javax.swing.JLabel();
        textAtonrUsername = new javax.swing.JTextField();
        browseButton = new javax.swing.JButton();
        comboSelectConn = new javax.swing.JComboBox<Object>(ROLES.toArray());
        buttonExit = new javax.swing.JButton();
        buttonLogin = new javax.swing.JButton();
        jMenuBar3 = new javax.swing.JMenuBar();
        jMenuSelectConnections = new javax.swing.JMenu();
        menuOracle2 = new javax.swing.JMenu();
        menuAddOraConnection1 = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Trigger Tool Login");
        setResizable(false);

        labelDatabaseInfo.setText("Online Trigger Database"); // NOI18N
        labelDatabaseInfo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        labelHost.setText("Host:"); // NOI18N

        labelDbName.setText("Database name:"); // NOI18N

        labelUsername.setText("Username:"); // NOI18N

        labelTechnology.setText("Technology:"); // NOI18N

        labelTechnologySelected.setText("techno");

        org.jdesktop.layout.GroupLayout panelConnParamLayout = new org.jdesktop.layout.GroupLayout(panelConnParam);
        panelConnParam.setLayout(panelConnParamLayout);
        panelConnParamLayout.setHorizontalGroup(
            panelConnParamLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panelConnParamLayout.createSequentialGroup()
                .addContainerGap()
                .add(panelConnParamLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(labelHost)
                    .add(labelDbName)
                    .add(labelUsername)
                    .add(labelTechnology))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panelConnParamLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, textHost, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 535, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, textDB, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 535, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, textUsername, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 535, Short.MAX_VALUE)
                    .add(labelTechnologySelected))
                .addContainerGap())
        );
        panelConnParamLayout.setVerticalGroup(
            panelConnParamLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panelConnParamLayout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .add(panelConnParamLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(labelTechnology)
                    .add(labelTechnologySelected))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panelConnParamLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(labelHost)
                    .add(textHost, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(12, 12, 12)
                .add(panelConnParamLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(labelDbName)
                    .add(textDB, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panelConnParamLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(labelUsername)
                    .add(textUsername, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
        );

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        labelInfo.setText("Info");
        jToolBar1.add(labelInfo);

        labelVersion.setText("jLabel2");
        labelVersion.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        labelPassword.setText("Password:"); // NOI18N

        textPassword.setEnabled(false);
        textPassword.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                textPasswordFocusGained(evt);
            }
        });
        textPassword.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                textPasswordMouseReleased(evt);
            }
        });
        textPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textPasswordActionPerformed(evt);
            }
        });
        textPassword.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                textPasswordKeyReleased(evt);
            }
        });

        labelAtonrUsername.setText("Username:");

        browseButton.setText("Browse");
        browseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(55, 55, 55)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(labelPassword)
                    .add(labelAtonrUsername))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(textAtonrUsername)
                    .add(textPassword, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE))
                .add(18, 18, 18)
                .add(browseButton)
                .addContainerGap(185, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(23, 23, 23)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(labelAtonrUsername)
                    .add(textAtonrUsername, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(labelPassword)
                    .add(textPassword, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(browseButton))
                .addContainerGap(32, Short.MAX_VALUE))
        );

        comboSelectConn.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                comboSelectConnPopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });

        buttonExit.setText("Exit");
        buttonExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonExitActionPerformed(evt);
            }
        });

        buttonLogin.setText("Login");
        buttonLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonLoginActionPerformed(evt);
            }
        });

        jMenuSelectConnections.setText("Databases");
        jMenuSelectConnections.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jMenu1MouseMoved(evt);
            }
        });

        menuOracle2.setText("Oracle"); // NOI18N

        menuAddOraConnection1.setText("Connection Parameters");
        menuAddOraConnection1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddOraConnectionActionPerformed(evt);
            }
        });
        menuOracle2.add(menuAddOraConnection1);
        menuOracle2.add(jSeparator4);

        jMenuSelectConnections.add(menuOracle2);

        jMenuBar3.add(jMenuSelectConnections);

        setJMenuBar(jMenuBar3);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jToolBar1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 685, Short.MAX_VALUE)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, panelConnParam, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(buttonLogin)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(buttonExit)
                        .add(5, 5, 5))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(comboSelectConn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 263, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                .add(labelDatabaseInfo, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 518, Short.MAX_VALUE)
                                .add(labelVersion, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(labelVersion)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(labelDatabaseInfo)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(comboSelectConn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panelConnParam, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(buttonExit)
                    .add(buttonLogin))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jToolBar1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 25, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * The action to take when one of the items in comboSelectConn is selected.
     */
    private void myComboSelectConnActionPerformed() {
        textPassword.setEnabled(false);
        buttonLogin.setEnabled(false);
        textPassword.setText("");
        textPassword.setVisible(true);
        labelPassword.setVisible(true);

        if (comboSelectConn.getSelectedIndex() != -1) {

            String mode = comboSelectConn.getSelectedItem().toString();

            if (mode.contains(ROLE_USER)) {
                if (this.theInitInfo.getOnline()) {
                    labelInfo.setText("Online Trigger DB, readonly");
                    theInitInfo = DBConnections.createAtonrReader();
                } else {
                    labelInfo.setText("Online Trigger DB replica, readonly");
                    theInitInfo = DBConnections.createAtlrReader();
                }
                theInitInfo.setUserMode(UserMode.USER);
                buttonLogin.setEnabled(true);
                textPassword.setVisible(false);
                labelPassword.setVisible(false);
                browseButton.setVisible(false);
                labelAtonrUsername.setVisible(false);
                textAtonrUsername.setVisible(false);
                textAtonrUsername.setText("trigger shifter");
            } else if (mode.contains(ROLE_SHIFTER)
                    || mode.contains(ROLE_EXPERT)
                    || mode.contains(ROLE_DBA)) {

                String systemUser = System.getProperty("user.name");
                if (systemUser.equals("crtrg")) {
                    if (mode.contains(ROLE_SHIFTER)) {
                        textAtonrUsername.setText("trgshifter");
                        textPassword.requestFocus();
                    } else {
                        textAtonrUsername.setText("");
                        textAtonrUsername.requestFocus();
                    }
                } else {
                    textAtonrUsername.setText(systemUser);
                    textPassword.requestFocus();
                }

                labelInfo.setText("Online Trigger DB with write access");
                theInitInfo = DBConnections.createAtonrWriter();
                textPassword.setEnabled(true);
                labelAtonrUsername.setVisible(true);
                textAtonrUsername.setVisible(true);

                if (mode.contains(ROLE_SHIFTER)) {
                    theInitInfo.setUserMode(UserMode.SHIFTER);
                } else if (mode.contains(ROLE_EXPERT)) {
                    theInitInfo.setUserMode(UserMode.EXPERT);
                } else if (mode.contains(ROLE_DBA)) {
                    theInitInfo.setUserMode(UserMode.DBA);
                }
            }
        }
    }

    /**
     * When the password box gets the focus.
     *
     * @param evt
     */
    private void textPasswordFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_textPasswordFocusGained
        buttonLogin.setEnabled(true);
    }//GEN-LAST:event_textPasswordFocusGained


    private void textPasswordKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textPasswordKeyReleased
        labelInfo.setText("");
    }//GEN-LAST:event_textPasswordKeyReleased

    private void jMenu1MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu1MouseMoved
        labelInfo.setText("Open or define your own databases here");
    }//GEN-LAST:event_jMenu1MouseMoved

    private void menuAddOraConnectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddOraConnectionActionPerformed
        panelConnParam.setVisible(true);
        textPassword.setVisible(false);
        labelPassword.setVisible(false);
        labelAtonrUsername.setVisible(false);
        textAtonrUsername.setVisible(false);
        comboSelectConn.setVisible(false);
        labelTechnologySelected.setText("Oracle");
        labelDatabaseInfo.setText("");
}//GEN-LAST:event_menuAddOraConnectionActionPerformed

    private void buttonExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonExitActionPerformed
        theInitInfo.setUserMode(UserMode.EXIT);
        dispose();
}//GEN-LAST:event_buttonExitActionPerformed

    private void buttonLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonLoginActionPerformed

        //at p1, need to configure an env variable for this in the script!
        String filename = InitInfo.getSystemHome() + "/.triggertool";
        if (theInitInfo.getOnline()) {
            filename = "/det/tdaq/scripts/.triggertool";
        }

        theInitInfo.setSystemUsername(textAtonrUsername.getText());
        if (!theInitInfo.getOnline()) {
            // option to create new connection
            if (panelConnParam.isVisible()) {
                theInitInfo.setTechnologyFromString(labelTechnologySelected.getText());
                theInitInfo.setUserName(textUsername.getText());
                theInitInfo.setdbHost(textHost.getText());
                theInitInfo.setSchemaName(textDB.getText());
                String selected = labelTechnologySelected.getText();

                try (FileWriter outputStream = new FileWriter(filename, true)) {
                    if (selected.equals("Oracle") || selected.equals("MySQL")) {
                        outputStream.write(labelTechnologySelected.getText() + ","
                                + textHost.getText() + ","
                                + textUsername.getText() + ","
                                + textDB.getText() + ",\n");
                    } else {
                        outputStream.write(labelTechnologySelected.getText() + ","
                                + new String(textPassword.getPassword()) + ",\n");
                    }
                } 
                catch (IOException e) {
                    logger.severe(e.getLocalizedMessage());
                }
            }
        }

        //get the pwd from the box if its there
        //String pwd = new String(textPassword.getPassword());
        if (theInitInfo.getdbServer().contains("atonr")) {

            if (theInitInfo.getAtonrRPassword().length() == 0) {
                JOptionPane.showMessageDialog(this,
                        "I don't know the password for the reader account on atonr. \n"
                        + "Please enter a line \"atonr_R_Password=password\" in the file " + filename + " under the section \"Passwords for ATLAS databases\"",
                        "ATONR Reader Password Required", JOptionPane.INFORMATION_MESSAGE);
                return;
            } else if (theInitInfo.getAtonrWPassword().length() == 0) {
                JOptionPane.showMessageDialog(this,
                        "I don't know the password for the writer account on atonr. \n"
                        + "Please enter a line \"atonr_W_Password=password\" in the file " + filename + " under the section \"Passwords for ATLAS databases\"",
                        "ATONR Writer Password Required", JOptionPane.INFORMATION_MESSAGE);
                return;
            } else {
                theInitInfo.setSystemUsername(textAtonrUsername.getText());
                theInitInfo.setOnlineUserPassword(new String(textPassword.getPassword()));
            }
        } else if (theInitInfo.getdbServer().contains("atlr") && (!theInitInfo.getUserName().toLowerCase().contains("mc"))) {
            if ( theInitInfo.getPassWord() == null || theInitInfo.getPassWord().length() == 0) {
                String coral_dblookup_path = System.getProperty("CORAL_DBLOOKUP_PATH");

                MessageDialog.showReportDialog("Login Problem", 
                        "The connection parameters for alias TRIGGERDB in read-only mode need to be added to the CORAL authentation in " + coral_dblookup_path);
                return;
            }
            //             else {
            //                 theInitInfo.setPassWord(theInitInfo.getPassWord());
            //             }
        } else {
            theInitInfo.setPassWord(new String(textPassword.getPassword()));
        }

        theInitInfo.setgoodInfo(true);
        if (comboSelectConn.isVisible()) {
            theInitInfo.setModeString(comboSelectConn.getSelectedItem().toString().toUpperCase());
        }

        //        dispose();
        this.setVisible(false);
}//GEN-LAST:event_buttonLoginActionPerformed

    /**
     * Activate login when user hits enter in the passwd field.
     *
     * @param evt
     */
    private void textPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textPasswordActionPerformed
        buttonLogin.doClick();
    }//GEN-LAST:event_textPasswordActionPerformed

    private void textPasswordMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_textPasswordMouseReleased
        if (evt.getButton() != MouseEvent.BUTTON2) {
            return;
        }
        //Clipboard systemClipboard = textPassword.getToolkit().getSystemClipboard();
        Clipboard systemSelection = textPassword.getToolkit().getSystemSelection();
        if (systemSelection != null) {
            try {
                String clip = systemSelection.getData(DataFlavor.stringFlavor).toString();
                systemSelection.setContents(new StringSelection(""), null);
                logger.log(Level.INFO, "System Selection: {0}", clip);
                textPassword.setText(clip);
            } catch ( UnsupportedFlavorException | IOException ex) {
                Logger.getLogger(InitDialog.class.getName()).log(Level.SEVERE, null, ex);
            }
            labelInfo.setText("");
        }
    }//GEN-LAST:event_textPasswordMouseReleased
    private void browseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_browseButtonActionPerformed
        int returnVal = fc.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = fc.getSelectedFile();
            textPassword.setText(f.toString());
        }
    }//GEN-LAST:event_browseButtonActionPerformed

    private void comboSelectConnPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboSelectConnPopupMenuWillBecomeInvisible
        this.myComboSelectConnActionPerformed();
    }//GEN-LAST:event_comboSelectConnPopupMenuWillBecomeInvisible
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton browseButton;
    private javax.swing.JButton buttonExit;
    private javax.swing.JButton buttonLogin;
    private javax.swing.JComboBox<Object> comboSelectConn;
    private javax.swing.JMenuBar jMenuBar3;
    private javax.swing.JMenu jMenuSelectConnections;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JLabel labelAtonrUsername;
    private javax.swing.JLabel labelDatabaseInfo;
    private javax.swing.JLabel labelDbName;
    private javax.swing.JLabel labelHost;
    private javax.swing.JLabel labelInfo;
    private javax.swing.JLabel labelPassword;
    private javax.swing.JLabel labelTechnology;
    private javax.swing.JLabel labelTechnologySelected;
    private javax.swing.JLabel labelUsername;
    private javax.swing.JLabel labelVersion;
    private javax.swing.JMenuItem menuAddOraConnection1;
    private javax.swing.JMenu menuOracle2;
    private javax.swing.JPanel panelConnParam;
    private javax.swing.JTextField textAtonrUsername;
    private javax.swing.JTextField textDB;
    private javax.swing.JTextField textHost;
    private javax.swing.JPasswordField textPassword;
    private javax.swing.JTextField textUsername;
    // End of variables declaration//GEN-END:variables
}
