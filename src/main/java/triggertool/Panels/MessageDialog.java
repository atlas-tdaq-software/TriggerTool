/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package triggertool.Panels;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;

/**
 *
 * @author stelzer
 */
@SuppressWarnings("serial")
public class MessageDialog extends javax.swing.JDialog {

    private URI target;
	
    private Color standardColor = new Color(0,0,255);
    private Color hoverColor = new Color(255,0,0);
    private Color activeColor = new Color(128,0,128);
    private Color transparent = new Color(0,0,0,0);
	
    private boolean underlineVisible = true;
	
    private Border activeBorder;
    private Border hoverBorder;
    private Border standardBorder;

    /**
     * Creates new form MessageDialog
     * @param parent
     * @param message
     * @param modal
     * @param title
     * @param target
     */
    public MessageDialog(java.awt.Frame parent, boolean modal, final String title, final String message, final String target) {
        this(parent, modal, title, message, target, target );
    }

    /**
     *
     * @param parent
     * @param modal
     * @param title
     * @param message
     * @param target
     * @param text
     */
    public MessageDialog(java.awt.Frame parent, boolean modal, final String title, final String message, final String target, final String text) {
        super(parent, modal);
        try {
            this.target = new URI(target);
        }
        catch (URISyntaxException ex) {
            this.target = null;
            Logger.getLogger(MessageDialog.class.getName()).log(Level.SEVERE, null, ex);
        }
        initComponents();

        url2.addMouseListener(new MouseListener() {
                /** Browse to the target. */
                @Override
                public void mouseClicked(MouseEvent me) {
                    browse();
                }
		
                /** Set the color to the hover color. */
                @Override
                    public void mouseEntered(MouseEvent me) {
                    url2.setForeground(hoverColor);
                    url2.setBorder(hoverBorder);
                }
		
                /** Set the color to the standard color. */
                @Override
                    public void mouseExited(MouseEvent me) {
                    url2.setForeground(standardColor);
                    url2.setBorder(standardBorder);
                }
		
                @Override
                public void mouseReleased(MouseEvent me) {}
		
                @Override
                public void mousePressed(MouseEvent me) {}

            });

        url2.addFocusListener(new FocusListener(){
                @Override
                public void focusLost(FocusEvent fe) {
                    url2.setForeground(standardColor);
                    url2.setBorder(standardBorder);
                }
                
                /** Set the color to the hover color. */
                @Override
                public void focusGained(FocusEvent fe) {
                    url2.setForeground(hoverColor);
                    url2.setBorder(hoverBorder);
                }
            });

        setTitle(title);
        messageField.setText(message);
        //messageField.setBackground(UIManager.getColor("TextField.inactiveBackground"));
        messageField.setBorder(BorderFactory.createEmptyBorder());
        //scrollPane.setBorder(BorderFactory.createEmptyBorder());
        url2.setText(text);
        url2.setToolTipText(target);
		
        if (underlineVisible) {
            activeBorder = new MatteBorder(0,0,1,0,activeColor);
            hoverBorder = new MatteBorder(0,0,1,0,hoverColor);
            standardBorder = new MatteBorder(0,0,1,0,transparent);
        } else {
            activeBorder = new MatteBorder(0,0,0,0,activeColor);
            hoverBorder = new MatteBorder(0,0,0,0,hoverColor);
            standardBorder = new MatteBorder(0,0,0,0,transparent);
        }
        url2.setForeground(standardColor);
        url2.setBorder(standardBorder);
        url2.setCursor( new Cursor(Cursor.HAND_CURSOR) );

        setLocationRelativeTo(parent);
        MainPanel.SetIconImage(this);
        setVisible(true);
    }

    /**
     *
     */
    public void browse() {
        setForeground(activeColor);
        url2.setBorder(activeBorder);
        try {
            Desktop.getDesktop().browse(target);
        } catch(Exception e) {
            e.printStackTrace();
        }
        setForeground(standardColor);
        url2.setBorder(standardBorder);
    }

    /**
     *
     * @param title
     * @param message
     */
    public static void showReportDialog(final String title, final String message) {
        new MessageDialog(null, true, title, message, "https://its.cern.ch/jira/issues/?jql=project+%3D+ATR+AND+component+%3D+%22TriggerTool%2FPanel%22", "JIRA ATR/TriggerTool");
    }

    /**
     *
     * @param ex
     */
    public static void showReportDialog(final Exception ex) {
        boolean skipped=false;
        StringBuilder msg = new StringBuilder(ex.getMessage());
        msg.append("\n");
        StackTraceElement[] stack = ex.getStackTrace();
        for(StackTraceElement line : stack) {
            if(line.toString().startsWith("triggerdb") ||
                    line.toString().startsWith("triggertool")) {
                msg.append(line.toString());
                msg.append("\n");
                skipped=false;
            } else {
                if(!skipped) {
                    msg.append("...\n");
                    skipped=true;
                }
            }
        }
        showReportDialog(ex.getClass().getCanonicalName(), msg.toString());
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scrollPane = new javax.swing.JScrollPane();
        messageField = new javax.swing.JTextArea();
        hint = new javax.swing.JLabel();
        url2 = new javax.swing.JTextField();
        okButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        messageField.setEditable(false);
        messageField.setColumns(20);
        messageField.setLineWrap(true);
        messageField.setRows(13);
        messageField.setWrapStyleWord(true);
        messageField.setBorder(null);
        scrollPane.setViewportView(messageField);

        hint.setText("Please report at");

        url2.setEditable(false);
        url2.setText("url");

        okButton.setText("OK");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 558, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(hint)
                        .addGap(6, 6, 6)
                        .addComponent(url2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hint)
                    .addComponent(url2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(okButton))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        setVisible(false);
        dispose();
    }//GEN-LAST:event_okButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel hint;
    private javax.swing.JTextArea messageField;
    private javax.swing.JButton okButton;
    private javax.swing.JScrollPane scrollPane;
    private javax.swing.JTextField url2;
    // End of variables declaration//GEN-END:variables
}
