package triggertool.Panels.PrescaleEditPanel;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdesktop.swingx.treetable.AbstractTreeTableModel;
import org.jdesktop.swingx.treetable.TreeTableModel;
import triggerdb.Entities.L1.L1Prescale;

/**
 * TreeTable model to hold the L1 Prescales Table. This table can display more
 * than one prescale set at once in a tree.
 *
 *
 * @author Tiago Perez
 * @date 2010-06-01
 */
// TODO: whenn saving and new set is equal to some old not going there.
public class L1PrescaleTreeTableModel extends AbstractTreeTableModel
        implements TreeTableModel {

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    // Names of the columns.

    /**
     *
     */
    static protected String[] columnNames = {"CTP Item", "Prescale Value", "NMD", "In/Out"};
    // Types of the columns.

    /**
     *
     */
    static protected Class[] types = {TreeTableModel.class,
        Double.class,
        String.class,
        Boolean.class};
    boolean[] canEdit = new boolean[]{false, true, false, true};

    /**
     *
     */
    public static int COLVALUE = 1;

    /**
     *
     */
    public static int COLNMD = 2;

    /**
     *
     */
    public static int COLBOOL = 3;

    /**
     * Only for testing
     */
    @Deprecated
    public L1PrescaleTreeTableModel() {
        this(new L1PrescaleEditItemNode("Prescale Sets", false));
    }

    /**
     * Default constructor.
     * @param top a ROOT item node.
     */
    public L1PrescaleTreeTableModel(L1PrescaleEditItemNode top) {
        super(top);
        // If l1psInt remove NMD column.
        String[] headInt = {"CTP Item", "Prescale Value", "In/Out"};
        L1PrescaleTreeTableModel.columnNames = headInt;
        L1PrescaleTreeTableModel.COLNMD = 3;
        L1PrescaleTreeTableModel.COLBOOL = 2;
        types[COLNMD] = java.lang.String.class;
        types[COLBOOL] = java.lang.Boolean.class;
        canEdit[2] = true;
    }

    private List<L1PrescaleEditItemNode> getChildren(Object node) {
        L1PrescaleEditItemNode itemNode = ((L1PrescaleEditItemNode) node);
        return itemNode.getChildren();
    }

    @Override
    public int getChildCount(Object node) {
        List children = this.getChildren(node);
        return (children == null) ? 0 : children.size();
    }

    @Override
    public Object getChild(Object node, int i) {
        return this.getChildren(node).get(i);
    }

    @Override
    public boolean isLeaf(Object node) {
        L1PrescaleEditItemNode item = (L1PrescaleEditItemNode) node;
        return item.isLeaf();
    }

    /**
     *
     * @return
     */
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    /**
     *
     * @param column
     * @return
     */
    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    /**
     *
     * @param column
     * @return
     */
    @Override
    public Class getColumnClass(int column) {
        return types[column];
    }

    /**
     *
     * @param node
     * @param column
     * @return
     */
    @Override
    public Object getValueAt(Object node, int column) {
        L1PrescaleEditItemNode itemNode = ((L1PrescaleEditItemNode) node);
        if (column == 0) {
            return itemNode.getLabel();
        } else if (column == 1) {
            return itemNode.getPrescaleValue();
        } else if (column == L1PrescaleTreeTableModel.COLBOOL) {
            return itemNode.getFlag();
        }
        return null;
    }

    /**
     * Checks whether a certain col in node can be edited by the user.
     * @param node the L1PrescaleEditNode to edit.
     * @param column the column number.
     * @return <code>true</code> if we can edit, else <code>false</code>.
     */
    @Override
    public final boolean isCellEditable(Object node, int column) {
        if (column == 0) {
            L1PrescaleEditItemNode n = (L1PrescaleEditItemNode) node;
            if (n.isLeaf() && !n.getLabelFlag()) {
                return true;
            } else {
                // Need especial case if for COL 0; i.e. the JTree column.
                return getColumnClass(column) == TreeTableModel.class;
            }
        } else {
            return canEdit[column];
        }
    }

    /**
     *
     * @param aValue
     * @param node
     * @param column
     */
    @Override
    public final void setValueAt(Object aValue, Object node, int column) {
        L1PrescaleEditItemNode item = (L1PrescaleEditItemNode) node;
        Class c = types[column];
        if (aValue != null && item.getInput() != null) {
            if (aValue.getClass().equals(c)) {
                if (column == COLVALUE) {
                    item.setValue((Double) aValue);
                } 
                else  if (column == COLBOOL) {
                    if(((Boolean) aValue && item.getInput() < 0 )|| (!(Boolean) aValue && item.getInput() > 0 ) ){
                        item.setValue(-1 * item.getInput());
                    }
                }
            } else if (column == 0) {
                item.setLabel(aValue.toString());

            } else {
                logger.log(Level.FINE, "Wrong class found while editing col{0}. Expected: {1} found: {2}", new Object[]{column, c, aValue.getClass()});
            }
        }
    }

    @Override
    public final int getIndexOfChild(Object parent, Object child) {
        if (parent == null || child == null) {
            return -1;
        }
        L1PrescaleEditItemNode p = (L1PrescaleEditItemNode) parent;
        L1PrescaleEditItemNode ch = (L1PrescaleEditItemNode) child;
        return p.getChildren().indexOf(ch);
    }

    /**
     * Checks whether the Table is empty. The non-empty condition implies that
     * we have more than one CTP ITEM which have more than one ITEMs.
     * @return empty
     */
    public boolean isEmpty() {
        L1PrescaleEditItemNode myRoot = (L1PrescaleEditItemNode) this.getRoot();
        return myRoot == null || myRoot.getChildCount() <= 0 || myRoot.getChildAt(0).getChildCount() <= 0;
    }

    /**
     * Removes a list of prescale sets from the table.
     * @param ids a vector with the ids of the prescales to remove.
     */
    public void clearPrescales(List<Integer> ids) {
        L1PrescaleEditItemNode myRoot = (L1PrescaleEditItemNode) this.getRoot();
        HelperFunctionsTreeTable.clearPrescales(myRoot, ids);
    }

    /**
     * clears the TreeTable but keeps the values given.
     * @param keepIds the id of the prescale sets to keep.
     */
    public void clearPrescalesButKeep(List<Integer> keepIds) {
        L1PrescaleEditItemNode myRoot = (L1PrescaleEditItemNode) this.getRoot();
        HelperFunctionsTreeTable.clearPrescalesButKeep(myRoot, keepIds);
    }

    /**
     * Add a list of L1Prescale to the table. Already existing prescale sets will
     * be skipped.
     * @param listOfSelectedDBPrescales the vector of L1Prescales to add.
     * @param ctpItems
     */
    public void addPrescales(List listOfSelectedDBPrescales, List<L1PrescaleEditItemNode> ctpItems) {
        L1PrescaleEditItemNode _root = (L1PrescaleEditItemNode) this.getRoot();
        for (Object l1p : listOfSelectedDBPrescales) {
            HelperFunctionsTreeTable.addPrescaleSet(_root, (L1Prescale) l1p);
        }
        // Update ctpItems vector
        for (int itemNumber = 0; itemNumber < ctpItems.size(); itemNumber++) {
            ctpItems.get(itemNumber).addChildren(_root.getChildAt(itemNumber).getChildren());
        }
    }

    /**
     * Reads all prescales available on the TreeTable.
     * @return the vector with the prescale nodes on hte TreeTable.
     */
    public List<L1PrescaleEditItemNode> getPrescales() {
        L1PrescaleEditItemNode _root = (L1PrescaleEditItemNode) this.getRoot();
        List<L1PrescaleEditItemNode> prescales = HelperFunctionsTreeTable.getPrescales(_root);
        return prescales;
    }
    
    /**
     *
     * @return
     */
    public List<L1PrescaleEditItemNode> getCTPitems() {
        L1PrescaleEditItemNode _root = (L1PrescaleEditItemNode) this.getRoot();
        return _root.getChildren();

    }

    /**
     * Adds new ctpitem to this root node of this table ones.
     * @param ctpItems
     */
    public void updateItems(List<L1PrescaleEditItemNode> ctpItems) {
        L1PrescaleEditItemNode _root = (L1PrescaleEditItemNode) this.getRoot();
        _root.getChildren().clear();
        _root.addChildren(ctpItems);
    }

    /**
     *
     */
    public final void removeEquals() {
        List<L1PrescaleEditItemNode> ctpItems =
                ((L1PrescaleEditItemNode) this.getRoot()).getChildren();
        List<L1PrescaleEditItemNode> removeItems = new ArrayList<>();
        for (L1PrescaleEditItemNode item : ctpItems) {
            List<L1PrescaleEditItemNode> values = item.getChildren();
            // makes no sense to do this with less than 2 prescale sets.
            if (values.size() < 2) {
                logger.log(Level.WARNING, "trying to compare {0} prescale sets.", values.size());
                return;
            }
            //float refval = values.get(0).getValue().floatValue();
            Double refval = values.get(0).getPrescaleValue();
            boolean remove = true;
            for (L1PrescaleEditItemNode val : values) {
                if (refval == null ? val.getPrescaleValue() == null : refval.equals(val.getPrescaleValue())) {
                    remove = false;
                }
            }
            if (remove) {
                removeItems.add(item);
            }
        }
        ((L1PrescaleEditItemNode) this.getRoot()).getChildren().removeAll(removeItems);
    }
}
