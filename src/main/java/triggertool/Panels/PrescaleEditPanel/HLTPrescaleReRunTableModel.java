/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.Panels.PrescaleEditPanel;

import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Michele
 */
public class HLTPrescaleReRunTableModel extends AbstractTableModel{

    private final List<HLTPrescaleReRunRow> dataVector;
    
    /**
     *
     * @param list
     */
    public HLTPrescaleReRunTableModel(List<HLTPrescaleReRunRow> list){
        dataVector = list;
    }
    
    /**
     * Stores the names of the columns for usage in getColumnName
     */
    private final String[] columnNames = {
        "Value", 
        "Condition", 
    };
    /**
     * Stored the class of the columns for usage in getColumnClass
     */
    Class[] types = new Class[]{
        java.lang.String.class, 
        java.lang.String.class, 
    };
 

    @Override
    public int getRowCount() {
        return dataVector.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    /**
     * @param col
     * @return 
     * @{@inheritDoc}
     */
    @Override
    public String getColumnName(final int col) {
        return columnNames[col];
    }
    
     /**
     * Returns the type of column passed to it.
     * @param columnIndex the column number.
     * @return the class of the column.
     */
    @Override
    public Class getColumnClass(final int columnIndex) {
        return types[columnIndex];
    }

    /**
     * Gets whether a cell is editable or not.
     * @param rowIdx the row number.
     * @param colIdx the column number.
     * @return <code>true</code> if cell is editable, else <code>false</code>.
     */
    @Override
    public boolean isCellEditable(final int rowIdx, final int colIdx) {
        return true;
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        HLTPrescaleReRunRow value = dataVector.get(rowIndex);
        return value.get(columnIndex);
    } 
    
    @Override
    public void setValueAt(final Object value, final int row,
            final int col) {
        dataVector.get(row).setElementAt(value, col);
    }
}
