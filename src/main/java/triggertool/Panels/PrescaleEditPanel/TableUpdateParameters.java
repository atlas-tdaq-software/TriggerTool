/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.Panels.PrescaleEditPanel;

import java.util.List;

/**
 *
 * @author Michele
 */
public class TableUpdateParameters {

    /**
     *
     */
    public String filterText;

    /**
     *
     */
    public String seedFilterText;

    /**
     *
     */
    public List<String> chainNames;

    /**
     *
     */
    public List<String> streamNames;

    /**
     *
     */
    public Boolean applyFilter;
   

    TableUpdateParameters(String filterText, String seedFilterText, List<String> groupChain, List<String> streamChain, boolean applyFilter) {
        this.filterText = filterText;
        this.seedFilterText = seedFilterText;
        this.chainNames = groupChain;
        this.streamNames = streamChain;
        this.applyFilter = applyFilter;
    }
}
