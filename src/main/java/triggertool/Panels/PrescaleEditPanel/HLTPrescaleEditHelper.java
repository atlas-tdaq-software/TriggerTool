package triggertool.Panels.PrescaleEditPanel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import triggerdb.Entities.HLT.HLTPrescale;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.HLT.HLTPrescaleType;

/**
 * Set of helper functions to manipulate Prescale Sets.
 *
 * @author tiago perez <tiago.perez@desy.de>
 */
public final class HLTPrescaleEditHelper {

    private static HLTPrescaleSet newHLTPrescaleSet;
    private static ArrayList<HLTPrescale> HLTPrescaleList;
    private static int chaincounter;

    /**
     * Creates a new HLTPrescale Set form a linkedHashMap as used in
     * PrescaleEdit panel. TODO this is not very well done. It produces a
     * prescaleSet that can be used for diff in the presca edit panel, but prob
     * it won't save. CHECK!
     *
     * @param lhm the map containing prescale info/
     * @param setName the name of the set
     * @return a new HLTPrescaleset.
     * @throws java.sql.SQLException
     */
    public static HLTPrescaleSet createHLTPrescaleSet(
            final LinkedHashMap<String, HLTPrescaleRow> lhm,
            final String setName) throws SQLException {

        //like save as, create a new record with a new name
        newHLTPrescaleSet = new HLTPrescaleSet();
        HLTPrescaleList = new ArrayList<>();
        newHLTPrescaleSet.set_name(setName);
        //we read from the map not the table - but we want to change this set as we go!
        for (HLTPrescaleRow hltrowdata : lhm.values()) {
            chaincounter = hltrowdata.getCounter();
            //System.out.println("counter " + chaincounter);

            CreateAndAddPrescale(HLTPrescaleType.Prescale, hltrowdata.getPrescale(), "");
            CreateAndAddPrescale(HLTPrescaleType.Pass_Through, hltrowdata.getPassThrough(), "");
            if (hltrowdata.getCondition().equals("na")) {
                CreateAndAddPrescale(HLTPrescaleType.Stream, hltrowdata.getStream(), "");
            } else {
                CreateAndAddPrescale(HLTPrescaleType.Stream, hltrowdata.getStream(), hltrowdata.getCondition());
            }
            CreateAndAddReRunPrescale(hltrowdata.getReRun());
        }

        newHLTPrescaleSet.setPrescales(HLTPrescaleList);
        return newHLTPrescaleSet;
    }

    private static void CreateAndAddPrescale(HLTPrescaleType type, String value, String condition) throws SQLException {
        if (!value.equals("na")) {
            HLTPrescale newHLTPS = new HLTPrescale();
            newHLTPS.set_chain_counter(chaincounter);
            newHLTPS.set_type(type);
            newHLTPS.set_value(Double.valueOf(value));
            newHLTPS.set_condition(condition);
            HLTPrescaleList.add(newHLTPS);
        }
    }

    private static void CreateAndAddReRunPrescale(String reRun) throws SQLException {
        List<String> prescales = Arrays.asList(reRun.split(";"));
        for (String p : prescales) {
            String[] reReun = p.split(":");
            if (reReun.length == 2) {
                CreateAndAddPrescale(HLTPrescaleType.ReRun, reReun[0], reReun[1]);
            }
        }
    }
}
