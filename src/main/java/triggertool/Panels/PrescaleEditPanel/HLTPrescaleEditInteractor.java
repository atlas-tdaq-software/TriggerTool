/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggertool.Panels.PrescaleEditPanel;

import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.swing.JOptionPane;
import triggerdb.Entities.HLT.HLTPrescale;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.HLT.HLTPrescaleType;
import triggerdb.Entities.HLT.HLTTriggerChain;
import triggerdb.Entities.HLT.HLTTriggerMenu;
import triggerdb.Entities.HLTLinks.HLTTM_PS;
import triggertool.XML.XMLprescales;

/**
 *
 * @author Michele
 */
public class HLTPrescaleEditInteractor {

    private static final Logger logger = Logger.getLogger("TriggerTool");

    /**
     *
     */
    public final HLTPrescaleTableModel hltMainModel;
    private HLTPrescaleSet theHLTPrescaleSet;
    private List<HLTPrescale> theHLTPrescales_v;

    /**
     *
     */
    public final LinkedHashMap<String, HLTPrescaleRow> lhm;
    private boolean regexFail = false;

    /**
     *
     */
    public final HLTPrescaleDiffTableModel hltDiffModel;
    int PS1 = 0;
    int PS2 = 1;
    int XS1 = 2;
    int XS2 = 3;

    /**
     *
     */
    public HLTPrescaleSet uploadedHLTPrescaleSet;

    private HLTTriggerMenu menu;

    /**
     *
     */
    public ArrayList<String> sets;
    private final ArrayList<HLTTriggerChain> chains;
    private final HashMap<Integer, HLTTriggerChain> chainMap;

    /**
     *
     * @param menu
     * @param chains
     */
    public HLTPrescaleEditInteractor(HLTTriggerMenu menu, ArrayList<HLTTriggerChain> chains) {
        this.menu = menu;
        this.chains = chains;
        chainMap = new HashMap<>();
        if (chains != null) {
            for (HLTTriggerChain chain : this.chains) {
                chainMap.put(chain.get_chain_counter(), chain);
            }
        }
        hltMainModel = new HLTPrescaleTableModel();
        lhm = new LinkedHashMap<>();
        sets = new ArrayList<>();
        theHLTPrescales_v = new ArrayList<>();
        hltDiffModel = new HLTPrescaleDiffTableModel();

    }

    /////////////////////////////////
    // Private Methods
    /**
     * TODO this method need around 9sec to complete!!!
     *
     * Queries the DB for to get the HLTPrescale belonging to the selected
     * HLTPrescaleSet. Then queries DB to retrieve the ExpressStream. Query
     * getRegularPrescaleSets(1.6sec)
     *
     * @param parameters
     * @throws java.sql.SQLException
     */
    public void updateHLTPrescalesTable(final TableUpdateParameters parameters) throws SQLException {
        if (!parameters.applyFilter) {

            hltMainModel.clearNumRows(); //clear data vector
            theHLTPrescales_v = theHLTPrescaleSet.getPrescales();

            Map<Integer, ArrayList<HLTPrescale>> map = new HashMap<>();
            Iterator<HLTPrescale> it = theHLTPrescales_v.iterator();
            while (it.hasNext()) {
                ArrayList<HLTPrescale> list = new ArrayList<>();
                HLTPrescale ps = (HLTPrescale) it.next();
                /*if(ps.get_chain_counter() == 883){
                    System.out.println(ps.get_type());
                }*/
                if (map.containsKey(ps.get_chain_counter())) {
                    list = map.get(ps.get_chain_counter());
                }
                list.add(ps);

                //System.out.println("putting " + ps.get_chain_counter());
                map.put(ps.get_chain_counter(), list);
            }

            Set<Integer> set = map.keySet();
            Iterator<Integer> it2 = set.iterator();
            while (it2.hasNext()) {
                Integer counter = it2.next();

                //HLTTriggerChain chain = chains.get(idx);
                HLTTriggerChain chain = chainMap.get(counter);

                if (chain == null) {
                    logger.log(Level.SEVERE, " HLT Trigger Chain with counter: {0} is present in HLT Prescale Set (PSK: {1}) BUT NOT in this Trigger Menu.", new Object[]{counter, this.theHLTPrescaleSet.get_id()});
                    continue;
                }

                HLTPrescaleRow hltPrescaleRow = new HLTPrescaleRow(counter, chain, map.get(counter));

                // Add this row to tablemodel
                hltMainModel.add(hltPrescaleRow);
                //System.out.println("setting " + chain.get_name() + " counter " + counter + " "  + hltPrescaleRow.getPrescale());
                // Add this row to the memory copy of the table
                lhm.put(chain.get_name(), hltPrescaleRow);
            }
        } else {
            // Apply filter.
            //model already filled... -do not clear it!
            List<HLTPrescaleRow> tableContent = doFilter(parameters);
            hltMainModel.clearNumRows(); //clear data vector

            for (int selrow = 0; selrow < tableContent.size(); selrow++) {
                HLTPrescaleRow rowdata = tableContent.get(selrow);
                String cn = rowdata.getName();
                hltMainModel.add(rowdata);
                //update the reference data
                lhm.put(cn, rowdata);
            }
        }
    }

    /**
     * Regenerates a new table with the filtered entries form lhm.
     *
     * @param map the likendHashedMap containing a prescale set.
     * @return The filtered data to be displayed on the table.
     */
    private List<HLTPrescaleRow> doFilter(final TableUpdateParameters parameters) {
        List<HLTPrescaleRow> tableContent = new ArrayList<>();
        Set<Map.Entry<String, HLTPrescaleRow>> set = lhm.entrySet();
        Iterator<Map.Entry<String, HLTPrescaleRow>> i = set.iterator();

        while (i.hasNext() && !regexFail) {
            Map.Entry<String, HLTPrescaleRow> me = i.next();
            HLTPrescaleRow rowdata = me.getValue();
            String chainName = rowdata.getName();
            if (parameters.filterText.length() != 0) {
                if (!filter(chainName, parameters.filterText)) {
                    continue;
                }
            }

            String seed = rowdata.getSeed();
            if (parameters.seedFilterText.length() != 0) {
                if (!filter(seed, parameters.seedFilterText)) {
                    continue;
                }
            }
            if (!parameters.chainNames.isEmpty()) {
                if (!parameters.chainNames.contains(chainName)) {
                    continue;
                }
            }
            if (!parameters.streamNames.isEmpty()) {
                if (!parameters.streamNames.contains(chainName)) {
                    continue;
                }
            }

            tableContent.add(rowdata);

        } //now clear old data vector and add new one
        regexFail = false;
        return tableContent;
    }

    private boolean filter(String input, String text) {
        String pattern = "";
        if (!regexFail) {
            pattern = pattern + text;
        }
        try {
            Pattern reg = Pattern.compile(pattern);
            Matcher Matcher = reg.matcher(input.toLowerCase());
            boolean Match = Matcher.find();

            if (Match) {
                return true;
            }
        } catch (PatternSyntaxException ex) {
            logger.log(Level.INFO, "Bad regex: {0}", ex);
            logger.info("Breaking out of filtering");
            regexFail = true;
        }
        return false;
    }

    /**
     *
     * @param incomingMenu
     */
    public void setMenu(HLTTriggerMenu incomingMenu) {
        menu = incomingMenu;
    }

    /**
     *
     * @param incomingSetInfo
     * @throws SQLException
     */
    public void setHLTPrescaleSet(String incomingSetInfo) throws SQLException {
        String[] data = incomingSetInfo.split(":");
        HLTPrescaleSet incomingSet = new HLTPrescaleSet(Integer.parseInt(data[0]));
        theHLTPrescaleSet = incomingSet;
    }

    /**
     *
     * @return
     */
    public HLTPrescaleSet getHLTPrescaleSet() {
        return theHLTPrescaleSet;
    }

    /**
     *
     * @param parameters
     * @param diffTableView
     * @param HLTPrescaleSet1Info
     * @param HLTPrescaleSet2Info
     * @throws SQLException
     */
    public void updateDiffTable(TableUpdateParameters parameters, boolean diffTableView, String HLTPrescaleSet1Info, String HLTPrescaleSet2Info) throws SQLException {
        if (diffTableView) {
            boolean setSelected = false;
            HLTPrescaleSet HLTPrescaleSet1;
            HLTPrescaleSet HLTPrescaleSet2;
            if (HLTPrescaleSet1Info.split(":")[0].equals("~")) {
                HLTPrescaleSet1 = uploadedHLTPrescaleSet;
            } else {
                HLTPrescaleSet1 = new HLTPrescaleSet(Integer.parseInt(HLTPrescaleSet1Info.split(":")[0]));
            }
            if (HLTPrescaleSet2Info.split(":")[0].equals("~")) {
                HLTPrescaleSet2 = uploadedHLTPrescaleSet;
            } else {
                HLTPrescaleSet2 = new HLTPrescaleSet(Integer.parseInt(HLTPrescaleSet2Info.split(":")[0]));
            }
            String diffName1 = HLTPrescaleSet1.get_name();
            String diffName2 = HLTPrescaleSet2.get_name();
            if (diffName1.equals("Prescale Set 1")
                    || diffName2.equals("Prescale Set 2")) {
                setSelected = true;
            } else if (diffName1.equals(diffName2) && (HLTPrescaleSet1.get_version().equals(HLTPrescaleSet2.get_version()))) {
                setSelected = true;
            }
            if (setSelected) {
                logger.info("No sets selected");
                String[] choices = {"Ok"};
                JOptionPane.showOptionDialog(null,
                        "Please select two different prescale sets to compare",
                        "",
                        0,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        choices,
                        choices[0]);
                updateDiffTable(parameters, false, HLTPrescaleSet1Info, HLTPrescaleSet2Info);
            } else {
                hltDiffModel.clearNumRows();

                // Get the map with the diff prescales.
                HashMap<Integer, HashMap<HLTPrescaleType, AbstractMap.SimpleEntry<HLTPrescale, HLTPrescale>>> diffMap = this.diffPrescaleSets(HLTPrescaleSet1, HLTPrescaleSet2);
                // Iterate over map and render table.
                Iterator<Integer> it = diffMap.keySet().iterator();
                while (it.hasNext() && !regexFail) {
                    Integer k = it.next();
                    HashMap<HLTPrescaleType, AbstractMap.SimpleEntry<HLTPrescale, HLTPrescale>> row = diffMap.get(k);
                    Iterator<HLTPrescaleType> iter = row.keySet().iterator();
                    //data object for table of diffs
                    ArrayList<Object> HLTprescalesdiff_v = new ArrayList<>();
                    for (int i = 0; i < HLTPrescaleDiffTableModel.NumberOfColumns; i++) {
                        HLTprescalesdiff_v.add("");
                    }

                    while (iter.hasNext()) {
                        HLTPrescaleType type = iter.next();
                        AbstractMap.SimpleEntry<HLTPrescale, HLTPrescale> element = row.get(type);
                        HLTPrescale ps1 = element.getKey();
                        HLTPrescale ps2 = element.getValue();

                        if (ps1 != null) {
                            HLTprescalesdiff_v.set(0, ps1.get_chain_name(menu));
                            HLTprescalesdiff_v.set(1, ps1.get_chain_counter());

                            if (type == HLTPrescaleType.Prescale) {
                                HLTprescalesdiff_v.set(2, ps1.get_value());
                            }
                            if (type == HLTPrescaleType.Pass_Through) {
                                HLTprescalesdiff_v.set(4, ps1.get_value());
                            }
                            if (type == HLTPrescaleType.Stream) {
                                HLTprescalesdiff_v.set(6, ps1.get_value());
                                HLTprescalesdiff_v.set(8, ps1.get_condition());
                            }
                            if (type == HLTPrescaleType.ReRun) {
                                HLTprescalesdiff_v.set(10, ps1.get_value() + ":" + ps1.get_condition() + ";");
                            }
                        } else if (ps2 != null) {
                            HLTprescalesdiff_v.set(0, ps2.get_chain_name(menu));
                        }
                        if (ps2 != null) {
                            if (type == HLTPrescaleType.Prescale) {
                                HLTprescalesdiff_v.set(3, ps2.get_value());
                            }
                            if (type == HLTPrescaleType.Pass_Through) {
                                HLTprescalesdiff_v.set(5, ps2.get_value());
                            }
                            if (type == HLTPrescaleType.Stream) {
                                HLTprescalesdiff_v.set(7, ps2.get_value());
                                HLTprescalesdiff_v.set(9, ps2.get_condition());
                            }
                            if (type == HLTPrescaleType.ReRun) {
                                HLTprescalesdiff_v.set(11, ps2.get_value() + ":" + ps2.get_condition() + ";");
                            }

                        }
                    }

                    if (HLTprescalesdiff_v.size() == 12) {

                        if (parameters.applyFilter) {
                            boolean filter = filter(HLTprescalesdiff_v.get(0).toString(), parameters.filterText);
                            if (filter) {
                                hltDiffModel.add(HLTprescalesdiff_v);
                            }
                        } else {
                            hltDiffModel.add(HLTprescalesdiff_v);
                        }
                    }
                }// END WHILE diffMap
                regexFail = false;
                hltDiffModel.fireTableDataChanged();
            } // setSelected
            // DIFF button disable ->  Reset tabel to defaults.
        } else {
            hltDiffModel.clearNumRows();
            hltMainModel.fireTableDataChanged();
        }
    }

    /**
     * Creates a map with the 2 prescale sets set up as: Key="L2/EF_COUNTER"
     * List= | PS1 | PS2 |ExpStr1 | ExpStr2 |
     *
     * @param set1 the first PrescaleSet to diff.
     * @param set2 the second PrescaleSet.
     * @return the map with the different prescales.
     */
    private HashMap<Integer, HashMap<HLTPrescaleType, AbstractMap.SimpleEntry<HLTPrescale, HLTPrescale>>> diffPrescaleSets(HLTPrescaleSet set1, HLTPrescaleSet set2) throws SQLException {
        // Create the map
        HashMap<Integer, HashMap<HLTPrescaleType, AbstractMap.SimpleEntry<HLTPrescale, HLTPrescale>>> diffMap = new HashMap<>();

        ArrayList<Integer> hasStreamPrescale1 = new ArrayList<>();
        ArrayList<Integer> hasStreamPrescale2 = new ArrayList<>();

        //temporarily pad prescale sets that are missing empty stream prescales
        if (!set1.getPrescales().isEmpty()) {
            for (HLTPrescale ps : set1.getPrescales()) {
                if (ps.get_type() == HLTPrescaleType.Stream) {
                    hasStreamPrescale1.add(ps.get_chain_counter());
                }
            }
        }
        if (!set2.getPrescales().isEmpty()) {
            for (HLTPrescale ps : set2.getPrescales()) {
                if (ps.get_type() == HLTPrescaleType.Stream) {
                    hasStreamPrescale2.add(ps.get_chain_counter());
                }
            }
        }

        ArrayList<HLTPrescale> streamsToAdd1 = new ArrayList<>();
        ArrayList<HLTPrescale> streamsToAdd2 = new ArrayList<>();

        for (HLTPrescale ps : set1.getPrescales()) {
            if (ps.get_type() == HLTPrescaleType.Prescale) {
                if (!hasStreamPrescale1.contains(ps.get_chain_counter())) {
                    HLTPrescale newps = new HLTPrescale();
                    newps.set_type(HLTPrescaleType.Stream);
                    newps.set_chain_counter(ps.get_chain_counter());
                    //add to set
                    streamsToAdd1.add(newps);
                }
            }
        }

        for (HLTPrescale ps : set2.getPrescales()) {
            if (ps.get_type() == HLTPrescaleType.Prescale) {
                if (!hasStreamPrescale2.contains(ps.get_chain_counter())) {
                    HLTPrescale newps = new HLTPrescale();
                    newps.set_type(HLTPrescaleType.Stream);
                    newps.set_chain_counter(ps.get_chain_counter());
                    //add to set
                    streamsToAdd2.add(newps);
                }
            }
        }
        set1.addPrescales(streamsToAdd1);
        set2.addPrescales(streamsToAdd2);

        //System.out.println("set1 " + set1.getNonReRunPrescales().size() + " set 2 " + set2.getNonReRunPrescales().size());
        // add set 1
        for (HLTPrescale ps : set1.getPrescales()) {
            if (diffMap.containsKey(ps.get_chain_counter())) {
                diffMap.get(ps.get_chain_counter()).put(ps.get_type(), new AbstractMap.SimpleEntry<HLTPrescale, HLTPrescale>(ps, null));
            } else {
                HashMap<HLTPrescaleType, AbstractMap.SimpleEntry<HLTPrescale, HLTPrescale>> psRow = new HashMap<>();
                psRow.put(ps.get_type(), new AbstractMap.SimpleEntry<HLTPrescale, HLTPrescale>(ps, null));
                diffMap.put(ps.get_chain_counter(), psRow);
            }
        }
        // Add set 2
        for (HLTPrescale ps : set2.getPrescales()) {
            // This shouldn't happen but... (#83514)            
            if (diffMap.get(ps.get_chain_counter()) == null) {
                 JOptionPane.showMessageDialog(null,
                    "Prescale sets " + set1.get_name() + " and " + set2.get_name() + " contain different numbers of chains!",
                    "Prescale set chain mismatch!",
                    JOptionPane.WARNING_MESSAGE);
                logger.log(Level.WARNING, " Prescale sets: {0}({1}) and {2} ({3}) don''t have the same number of chains!", new Object[]{set1.get_name(), set1.get_id(), set2.get_name(), set2.get_id()});
                HashMap<HLTPrescaleType, AbstractMap.SimpleEntry<HLTPrescale, HLTPrescale>> psRow = new HashMap<>();
                psRow.put(ps.get_type(), new AbstractMap.SimpleEntry<HLTPrescale, HLTPrescale>(null, ps));
                diffMap.put(ps.get_chain_counter(), psRow);
            } else {
                /*System.out.println("here");
                System.out.println(ps.get_chain_counter());
                System.out.println(ps.get_type());*/

                diffMap.get(ps.get_chain_counter()).get(ps.get_type()).setValue(ps);
                //System.out.println("there");

            }
        }

        // DO DIFF
        Iterator<Integer> it = diffMap.keySet().iterator();
        while (it.hasNext()) {
            Integer k = it.next();
            HashMap<HLTPrescaleType, AbstractMap.SimpleEntry<HLTPrescale, HLTPrescale>> rows = diffMap.get(k);
            boolean remo = this.diffRow(rows);
            if (remo) {
                it.remove();
            }
        }
        return diffMap;
    }

    /**
     * Check whether a row in the diffMap has differences or not.
     *
     * @param row a row in the diffMap as a list of HLTPrescales.
     * @return      <code>true<code> if equal.
     */
    private boolean diffRow(final HashMap<HLTPrescaleType, AbstractMap.SimpleEntry<HLTPrescale, HLTPrescale>> row) {
        boolean remove = true;
        /*if (row.size() != 4) {
         return false;
         }*/
        Iterator<HLTPrescaleType> iter = row.keySet().iterator();
        while (iter.hasNext()) {
            HLTPrescaleType key = iter.next();
            AbstractMap.SimpleEntry<HLTPrescale, HLTPrescale> pair = row.get(key);
            HLTPrescale ps1 = pair.getKey();
            HLTPrescale ps2 = pair.getValue();
            if (ps1 != null && ps2 != null) {
                //compare regular ps
                if (!(ps1.get_value().equals(ps2.get_value()))) {
                    remove = false;
                }
                if (key == HLTPrescaleType.Stream) {
                    if (!(ps1.get_condition().isEmpty() && ps2.get_condition().equals("~") || ps2.get_condition().isEmpty() && ps1.get_condition().equals("~")) && !(ps1.get_condition().equals(ps2.get_condition()))) {
                        remove = false;
                    }
                }
                if (key == HLTPrescaleType.ReRun) {
                    if (!(ps1.get_condition().equals(ps2.get_condition()))) {
                        remove = false;
                    }
                }

            } else if (!(ps1 == null && ps2 == null)) {
                remove = false;
            }

        }
        return remove;
    }

    /**
     * Sets the String <code>comment</code> as a comment in the prescale set
     * with id <code>Id</code>.
     *
     * @param comment the new comment to set.
     */
    public void updateHLTComment(final String comment) {
        if (comment != null) {
            theHLTPrescaleSet.set_comment(comment);
        }
    }

    /**
     *
     */
    public void CleanHLTPrescaleRowMap() {
        lhm.clear();
    }

    /**
     * Save the prescale set data on the table.
     *
     * @param newsetname
     * @param isHLTHidden
     * @return
     * @throws java.sql.SQLException
     */
    public final HLTPrescaleSet saveHLTAs(String newsetname, Boolean isHLTHidden) throws SQLException {
        HLTPrescaleSet newHLTPrescaleSet = HLTPrescaleEditHelper.createHLTPrescaleSet(lhm, newsetname);
        newHLTPrescaleSet.set_id(newHLTPrescaleSet.save(menu.get_id()));
        if(newHLTPrescaleSet.get_id() == -1){
            return newHLTPrescaleSet;
        }

        if (!IsPrescaleSetDuplicate(newHLTPrescaleSet)) {
            CreateLinkMenuPrescaleSet(newHLTPrescaleSet);
        }

        lhm.clear();

        return newHLTPrescaleSet;
    }

    /**
     *
     * @param newHLTPrescaleSet
     * @return
     * @throws SQLException
     */
    public boolean IsPrescaleSetDuplicate(HLTPrescaleSet newHLTPrescaleSet) throws SQLException {
        boolean alreadyexists = false;
        for (HLTPrescaleSet pss : menu.getPrescaleSets()) {
            if (newHLTPrescaleSet.get_id() == pss.get_id()) {
                alreadyexists = true;
                break;
            }
        }

        return alreadyexists;
    }

    private void CreateLinkMenuPrescaleSet(HLTPrescaleSet newHLTPrescaleSet) throws SQLException {
        HLTTM_PS newlink = new HLTTM_PS();
        newlink.set_menu_id(menu.get_id());
        newlink.set_prescale_set_id(newHLTPrescaleSet.get_id());
        newlink.save();
    }

    /**
     * Compares the prescale set values on the main table with the values on the
     * table. If differences are found return true.
     *
     * @return <true> if the prescale set table was edited.
     * @throws java.sql.SQLException
     */
    public boolean checkIfPrescaleSetEdited() throws SQLException {
        boolean hltPrescalesEdited = false;
        // loop over the prescales in the db and see if one in the data
        // vector different!
        for (HLTPrescale dbps : theHLTPrescaleSet.getRegularPrescales()) {
            Double oldPS = dbps.get_value();
            String chName = dbps.get_chain_name(menu);
            if (lhm.containsKey(chName)) {
                //System.out.println("checking " + chName);
                HLTPrescaleRow hltrowdata = lhm.get(chName);
                //PS
                String newPS = hltrowdata.getPrescale();
                try {
                    // Be carefull with numbers,  string(1)!=String(1.0)...
                    // If number we do Float compare.
                    Double newPsF = Double.parseDouble(newPS);
                    if (!newPsF.equals(oldPS)) {
                        //System.out.println(newPsF + " " + oldPS);
                        hltPrescalesEdited = true;
                        break;
                    }

                } catch (NumberFormatException e) {
                    logger.log(Level.WARNING," Prescales in Chain {0}" + " are not"
                            + " numeric. " + "\n DB    values: PS={1}\n TABLE values: PS={2}", new Object[]{chName, oldPS, newPS});
                }
            }
        }

        return hltPrescalesEdited;
    }

    /**
     *
     * @param path
     * @return
     * @throws SQLException
     */
    public String LoadHLTPrescalesFromXML(String path) throws SQLException {
        final int selectedLevel = 1;

        boolean usedThisPrescalesAsReference = false;

        XMLprescales xmlPrescales = new XMLprescales(path);
        xmlPrescales.checkFile();

        if (selectedLevel != xmlPrescales.getLevel()) {
            logger.warning(" PrescaleSet Level in XML and Table do not match.\n"
                    + " Please select an adequate XML file.");
            return "";
        }

        // If No name present break.
        String psSetName = xmlPrescales.getPrescaleSetName();
        if (psSetName.isEmpty()) {
            //psSetName = dbTrigMenuName;
            logger.severe(" The prescale set you are trying to upload has no"
                    + " name.\n Please provide a name in the field:\n"
                    + " <HLT_MENU prescale_set_name= >");
            return psSetName;
        }

        // Name of the current set, for logger info
        String oldSetName = "";

        if (!usedThisPrescalesAsReference) {
            logger.log(Level.INFO, " Creating a new prescale set wiht name: \"{0}\".", psSetName);
        } else {
            logger.log(Level.INFO, " Updating HLT prescale set: {0}", oldSetName);
        }

        // Update the values at the selected table.
        List<String> notPresent = xmlPrescales.updatePrescaleSetHLT(lhm);

        // If there are not present chains in the xml file, pop up a warning.
        if (!notPresent.isEmpty()) {

            String ps = HLTPrescale.DefaultPrescaleValue.toString();
            String setName = xmlPrescales.getPrescaleSetName();
            String msg = "<html><body>\n"
                    + "WARNING: You are uploading prescale set \"" + setName
                    + "\"\nbut " + notPresent.size()
                    + " elements are not present in the XML file.\n"
                    + "Default values will be used for these chains.\n"
                    + "[DEFAULTS PS=" + ps
                    + "]";
            msg += "\nList of non present chains: \n";
            for (int i = 0; i < notPresent.size(); i++) {
                String name = notPresent.get(i);
                msg += name + "\n";
                if (i >= 18) {
                    msg += "... and " + (notPresent.size() - 10)
                            + " others (Check the log FINE for more details).";
                    break;
                }
            }
            JOptionPane.showMessageDialog(null, msg, " Empty chains in XML file.", JOptionPane.WARNING_MESSAGE);
        }

        uploadedHLTPrescaleSet = CreateHLTPrescaleSet(psSetName + " *edit*");

        return psSetName;
    }

    /**
     *
     * @param psSetName
     * @return
     * @throws SQLException
     */
    public HLTPrescaleSet CreateHLTPrescaleSet(String psSetName) throws SQLException {
        return HLTPrescaleEditHelper.createHLTPrescaleSet(lhm, psSetName);
    }

    /**
     *
     * @param rowindex
     * @param columnIndex
     * @return
     */
    public HLTPrescaleRerunPanelInteractor HandleOpenReRunTable(int rowindex, int columnIndex) {
        if (hltMainModel.getRowCount() > 0 && columnIndex == HLTPrescaleRow.RERUN) {
            return new HLTPrescaleRerunPanelInteractor(hltMainModel.getRowAt(rowindex));
        }

        return null;
    }
}
