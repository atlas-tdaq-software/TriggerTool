package triggertool.Panels.PrescaleEditPanel;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.*;

/**
 * Tablemodel to hold the L1Prescales. Contains functions to link the boolean column to the prescales.
 * Allows for easy turning on or off of prescales.
 * To enable sorting will be decorated by the table sorter after being called.
 * @author Alex Martyniuk
 */
public class HLTPrescaleDiffTableModel extends AbstractTableModel {

    /**
     * Stores the data that the table is drawn from.
     * The first vector holds the row position information
     * The second vector holds the Object (name of prescale), Integer(Prescale value), Bool(Prescale On/Off)
     * Can be filled with rows using add and passing a vector< object > with Object, integer, Bool in it.
     * Can edit specific positions using setValueAt
     * Can return values stored at any position with getValueAt 
     */
    protected List<ArrayList<Object>> dataVector = new ArrayList<>();
    
    /**
     *
     */
    public static final int NumberOfColumns = 12;
    
    //
    // COLUMN INDEX CONSTANTS
    /**
     * Chain name.
     */
    public static final int NAME = 0;
    /**
     * An integer representing the chain counter.
     */
    public static final int COUNTER = 1;
    /**
     * The prescale value 1.
     */
    public static final int PS_1 = 2;
    /**
     * The prescale value 2.
     */
    public static final int PS_2 = 3;
    /**
     * The pass-through value 1.
     */
    public static final int PT_1 = 4;
    /**
     * The pass-through value 2.
     */
    public static final int PT_2 = 5;    
    /**
     * The stream value 1.
     */
    public static final int STREAM_1 = 6;
    /**
     * The stream value 2.
     */
    public static final int STREAM_2 = 7;
    /**
     * The stream value 1.
     */
    public static final int CONDITION_1 = 8;
    /**
     * The stream value 2.
     */
    public static final int CONDITION_2 = 9;
     /**
     * The stream value 1.
     */
    public static final int RERUN_1 = 10;
    /**
     * The stream value 2.
     */
    public static final int RERUN_2 = 11;
    /**
     * Stores the names of the columns for usage in getColumnName
     */
    private final String[] columnNames = {
        "Chain Name", 
        "Counter", 
        "PS: 1", 
        "PS: 2",
        "PT: 1", 
        "PT: 2", 
        "Stream: 1", 
        "Stream: 2", 
        "Cond: 1", 
        "Cond: 2",
        "ReRun: 1",
        "ReRun: 2"
    };
    /**
     * Stored the class of the columns for usage in getColumnClass
     */
    Class[] types = new Class[]{
        java.lang.Object.class, 
        java.lang.Object.class, 
        java.lang.Object.class,
        java.lang.String.class, 
        java.lang.String.class, 
        java.lang.String.class, 
        java.lang.String.class, 
        java.lang.String.class, 
        java.lang.String.class, 
        java.lang.String.class,
        java.lang.String.class,
        java.lang.String.class
    };
    /**
     * Stores the edit info to be passed to is CellEditable
     */
    boolean[] canEdit = new boolean[]{
        false, false, false, false, false, false, false, false, false, false, false, false
    };

    /**
     * Constructor for the tablemodel
     */
    public HLTPrescaleDiffTableModel() {
    }

    /**
     * Returns the number of columns
     * @return
     */
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    /**
     * Returns the columns name
     * @param col
     * @return
     */
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    /**
     * Returns the type of column passed to it.
     * @param columnIndex
     * @return
     */
    @Override
    public Class getColumnClass(int columnIndex) {
        return types[columnIndex];
    }

    /**
     * Sets whether a cell is editable or not.
     * @param rowIndex
     * @param columnIndex
     * @return
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return canEdit[columnIndex];
    }

    /**
     * Clears the datavector so a new prescale set can be loaded.
     */
    public final void clearNumRows() {
        dataVector.clear();
    }

    /**
     * Gets the current size of the table by finding the size of the datavector.
     * @return
     */
    @Override
    public int getRowCount() {
        return dataVector.size();
    }

    /**
     * Gets the value at specified row/col position from the datavector where the table data is stored.
     * @param row
     * @param col
     * @return
     */
    @Override
    public Object getValueAt(int row, int col) {
        List<Object> value = dataVector.get(row);
        return value.get(col);
    }

    /**
     * Adds a row to the datavector. 
     * Then tells the table that the data has been updated so needs to be redrawn.
     * @param HLTprescalesdata_v
     */
    public void add(ArrayList<Object> HLTprescalesdata_v) {
        dataVector.add(HLTprescalesdata_v);
        //System.out.println("test " + getRowCount());
        fireTableDataChanged();
    }
}