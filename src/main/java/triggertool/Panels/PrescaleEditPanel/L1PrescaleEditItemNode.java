/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package triggertool.Panels.PrescaleEditPanel;

import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggertool.L1Records.CutToValueConverter;

/**
 * A class to hold an item in the TreeTable. This represents a row in the Table.
 * 
 * TODO implement the interface instead of extend DefaultMutableTreeNode to avoid
 * cast warnings OR extend/implement org.swingx..node.
 *
 * @author Tiago Perez 
 * @date 2010-05-27
 */
public final class L1PrescaleEditItemNode extends DefaultMutableTreeNode {

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    // Defines whether we show the CTP item name (TRUE) or the setName (FALSE)
    private Boolean _label;
    private Double input;
    private Double prescaleValue;
    private int _cut;
    // Non-vissible field. For control and data manioulation.
    private String _nodeName;
    private Integer _id;
    // The Node with the info of the CTP itemm this node belongs to.
    private L1PrescaleEditItemNode _ctpItem;
    // is This item a CTP item
    private Boolean _isCTPitem;
    // The Prescale Set this node belongs to.
    private L1PrescaleEditItemNode _prescaleSet;
    private Boolean _isPrescaleSet;
    private Integer _version;

    /**
     * Create an empty instance to hold data of one row only. this will not work
     * if you need to build up a Tree.
     */
    public L1PrescaleEditItemNode() {
        super();
        _label = true;
        _nodeName = "";
        _id = -1;
        _version = 0;
        _ctpItem = null;
        _isCTPitem = false;
        _prescaleSet = null;
        _isPrescaleSet = false;
    }

    /**
     * Constructor of a CTP ITEM.
     * @param itemName the name of this item
     * @param id the CTP ID of this item
     */
    public L1PrescaleEditItemNode(String itemName, Integer id) {
        this();
        this.children = new Vector<>();
        this._nodeName = itemName;
        this._id = id;
        // Control
        this._isCTPitem = true;
    }

    /**
     * Constructor of default TOP node.
     * @param name
     */
    public L1PrescaleEditItemNode(String name) {
        this(name, false);
        //this.children = new Vector<L1PrescaleEditItemNode>();
    }

    /**
     * Constructor of a PrescaleSet item.
     * @param itemName the name of this set.
     * @param isPrescaleSet boolean flag that set whether this is a prescale set
     * or not.
     *
     * TODO set id here.
     */
    public L1PrescaleEditItemNode(String itemName, Boolean isPrescaleSet) {
        this();
        this._label = !(isPrescaleSet);
        this._nodeName = itemName;
        this._id = null;
        // Control
        this._isPrescaleSet = isPrescaleSet;
    }

    /**
     * Constructor of PrescaleSet node,
     * @param setName the name of this set
     * @param setId
     * @param setVersion
     * @param isPrescaleSet
     */
    public L1PrescaleEditItemNode(String setName, Integer setId, Integer setVersion, Boolean isPrescaleSet) {
        this();
        this._nodeName = setName;
        this._id = setId;
        this._version = setVersion;
        this._isPrescaleSet = isPrescaleSet;
        this._label = !(isPrescaleSet);
    }

    /**
     * Constructor for the leaf items.
     *
     * @param setNode the prescale set node
     * @param ctpItem the ctp item node
     * @param valueInIntFormat the prescale value
     */
    public L1PrescaleEditItemNode(L1PrescaleEditItemNode setNode, L1PrescaleEditItemNode ctpItem, String valueInIntFormat) {
        //this();
        if (setNode == null || ctpItem == null) {
            logger.warning("trying to create an empty L1PrescaleEditItemNode.");
        }
        this._label = true;
        this.setPrescaleSet(setNode);
        this.setCtpItem(ctpItem);
        this._cut = Integer.parseInt(valueInIntFormat);
        this.prescaleValue = CutToValueConverter.calculatePrescaleFromCut(Integer.parseInt(valueInIntFormat));
        this.input = prescaleValue;
    }

    /**
     * Adds a node as a CTP father. 
     * @param ctpItem the CTP item node.
     */
    public void setCtpItem(L1PrescaleEditItemNode ctpItem) {
        this._isCTPitem = false;
        this._ctpItem = ctpItem;
        this._ctpItem.add(this);
    }

    /**
     * Gets the CTP item father node.
     * @return the CTP node.
     */
    public final L1PrescaleEditItemNode getCtpItem() {
        return this._ctpItem;
    }

    /**
     * Returns the id of this node. For CTP Items this is the CTP ID, for leafs
     * will try to return the CTP parent's id, for others return this._id.
     * @return this id or CTP item fathers id.
     */
    public final Integer getId() {
        if (this.isLeaf() && this._ctpItem != null) {
            return this._ctpItem.getId();
        } else {
            return this._id;
        }
    }

    /**
     * Sets the ctp id of this node. For children of CTP items will try to set
     * the id of the parent.
     * @param id
     */
    public void setId(Integer id) {
        if (this._ctpItem != null) {
            this._ctpItem.setId(id);
        } else {
            this._id = id;
        }
    }

    /**     
     * @return <code>true</code> if this is a CTP item.
     */
    public final Boolean getIsCTPitem() {
        return this._isCTPitem;
    }

    /**
     *
     * @param isCTPitem
     */
    public void setIsCTPitem(Boolean isCTPitem) {
        this._isCTPitem = isCTPitem;
    }

    /**
     * Retruns the nodename of this node.
     * @return
     */
    public String getNodeName() {
        return _nodeName;
    }

    /**
     * Sets the name of this node.
     * @param nodeName
     */
    public void setNodeName(String nodeName) {
        this._nodeName = nodeName;
    }

    /**
     * Gets the PrescaleSet father node.
     * @return
     */
    public L1PrescaleEditItemNode getPrescaleSet() {
        return _prescaleSet;
    }

    /**
     *
     * @param prescaleSet
     */
    public void setPrescaleSet(final L1PrescaleEditItemNode prescaleSet) {
        this._isPrescaleSet = false;
        this._prescaleSet = prescaleSet;
        this._prescaleSet.add(this);
    }

    /**
     *
     * @return
     */
    public final Boolean getIsPrescaleSet() {
        return this._isPrescaleSet;
    }

    /**
     *
     * @param isPrescaleSet
     */
    public final void setIsPrescaleSet(final Boolean isPrescaleSet) {
        this._isPrescaleSet = isPrescaleSet;
    }

    /**
     * Createas a Label based on whether this is a CTP ITEM node, a PrescaleSet node
     * or a leaf. For leafs, it displays CTP ITEM or prescaleSet's info based on
     * this._label.
     * @return a label for the first colum.
     */
    public final String getLabel() {
        String label_pref = "CTP ID: ";
        String label = label_pref + this._id + " / " + this.getNodeName();
        try {
            if (_isCTPitem) {
                label = label_pref + this._id + " / " + this.getNodeName();
            } else if (_isPrescaleSet) {
                // Return the name as formatted in the prescale set list.
                if (_id > 0) {
                    if (_version != null && _version > 0) {
                        label = _id + ": " + _nodeName + " v" + _version;
                    } else {
                        label = _id + ": " + this.getNodeName();
                    }
                } else {
                    label = this.getNodeName();
                }
            } else if (this.isLeaf()) {
                if (_label) {
                    if (this._ctpItem != null) {
                        label = this.getCtpItem().getLabel();
                    }
                } else {
                    if (this._prescaleSet != null) {
                        label = this.getPrescaleSet().getLabel();
                    }
                }
            }
        } catch (Exception e) {
            logger.severe(" error getting label from this node.");
        }
        return label;


    }

    /**
     * Allow editing of prescale Set Name:
     * @param setName set name
     */
    public final void setLabel(String setName) {
        if (this.isLeaf() && !(_label)) {
            this._prescaleSet.setNodeName(setName);
            this._prescaleSet.setVersion(-1);
        }
    }

    /**
     * Force that this node shows the PrescaleSet name as Label.
     */
    public final void setPrescaleAsLabel() {
        this._label = false;
    }

    /**
     * Returns the <code>float</code> prescale value of this item.
     * @return the prescale.
     */
    public final Double getPrescaleValue() {
        return prescaleValue;
    }

    /**
     * Returns the <code>float</code> prescale value of this item.
     * @return the prescale.
     */
    public final Double getInput() {
        return input;
    }
    
    /**
     * Boolean flag, <code>true</code> if value > 0, else <code>false</code>.
     * @return the In/Out boolean flag.
     */
    public final Boolean getFlag() {
        if (prescaleValue == null) {
            return null;
        }
        return prescaleValue > 0;
    }

    /**
     * Loads the children or and new vector.
     * @return 
     */
    @SuppressWarnings(value = "unchecked")
    public final List<L1PrescaleEditItemNode> getChildren() {
        if (children == null) {
            children = new Vector<>();
        }
        return (List<L1PrescaleEditItemNode>) children;
    }

    @Override
    public final L1PrescaleEditItemNode getChildAt(int idx) {
        return (L1PrescaleEditItemNode) children.get(idx);
    }

    /**
     * Adds a vector to the list of children of this node. Ensures that no item
     * is repeated.
     * @param chldrn
     */
    public final void addChildren(final List<L1PrescaleEditItemNode> chldrn) {
        for (L1PrescaleEditItemNode item : chldrn) {
            this.add(item);
        }
    }

    /**
     * Add a child to the children vector if the child is not already present.
     * @param child
     */
    @SuppressWarnings(value = "unchecked")
    public void add(L1PrescaleEditItemNode child) {
        // If no vector, create a new children's vector.
        if (this.children == null) {
            this.children = new Vector<>();
        }
        // Only allow different children.
        if (!this.children.contains(child)) {
            this.children.add((Object) child);
        }
    }

    /**
     * Replaces the children list by <code>chldrn</code>
     * @param chldrn the vector of children.
     */
    public final void setChildren(final Vector<L1PrescaleEditItemNode> chldrn) {
        this.children = chldrn;
    }

    /**
     * This is recursive. It would work for more levels.
     * TODO: Take care that TOP is not editable. Set a FLAG, is TOP.
     */
    private void setChildrenValues() {
        for (Object child : children) {
            ((L1PrescaleEditItemNode) child).setInput(this.input);
        }
    }

    /**
     * Set  the prescale value of this item. It call the children's
     * setters to propagate down to all elements on a CTP ITEM.
     * @param cut the prescale value.
     */
    public final void setCutInHexFormat(String cut) {
        prescaleValue = CutToValueConverter.getPrescaleFromHexCut(cut);
        input = prescaleValue;
        if (!(this.isLeaf())) {
            this.setChildrenValues();
        }
    }

    /**
     * Set  the prescale value of this item. It call the children's
     * setters to propagate down to all elements on a CTP ITEM.
     * @param cut the prescale value.
     */
    public final void setCutInIntFormat(int cut) {
        _cut = cut;
        prescaleValue = CutToValueConverter.calculatePrescaleFromCut(cut);
        input = prescaleValue;        
        if (!(this.isLeaf())) {
            this.setChildrenValues();
        }
    }    
    
    /**
     * Set  the prescale value of this item. It call the children's
     * setters to propagate down to all elements on a CTP ITEM.
     * @param value the prescale value.
     */
    public final void setValue(Double value) {
        prescaleValue = value;
        input = prescaleValue;
        if (!(this.isLeaf())) {
            this.setChildrenValues();
        }
    }
    
    /**
     * Set  the prescale value of this item. It call the children's
     * setters to propagate down to all elements on a CTP ITEM.
     * @param value the prescale value.
     */
    public final void setInput(Double value) {
        input = value;
        int cutInHexFormat = CutToValueConverter.calculateCutFromPrescale(value);
        prescaleValue = CutToValueConverter.calculatePrescaleFromCut(cutInHexFormat);
        if (!(this.isLeaf())) {
            this.setChildrenValues();
        }
    }
    /**   
     * @return returns either the PrescaleSEt name or the "CTP ID : " label for
     * displaying in the first col of the table.
     */
    @Override
    public final String toString() {
        return this.getLabel();
    }

    /**
     * @return the boolean flag, <code>true</code> if positive or <code>false</code>
     * if negative.
     */
    public final boolean getLabelFlag() {
        return this._label;
    }

    /**
     * Set version number, only relevant for prescaleset nodes.
     * @param version the version of this prescale
     */
    public void setVersion(int version) {
        this._version = version;
    }

    /**
     * Clear the values (value, nmd & flag) of this item non recursively.
     */
    void clearValue() {
        this.input = null;
        this.prescaleValue = null;
    }

    /**
     * Get the ctp item name or "".
     * @return the ctp name of this item.
     */
    public final String getItemName() {
        String name = "";
        if (!this._isCTPitem && this._ctpItem != null) {
            name = this._ctpItem._nodeName;
        } else if (this._nodeName != null) {
            name = this._nodeName;
        }
        return name;
    }

    /**
     * Get the ctp item name or "".
     * @return the ctp name of this item.
     */
    public final String getPrescaleSetName() {
        String name = "";
        if (this._isPrescaleSet) {
            name = this._nodeName;
        } else if (this._prescaleSet != null) {
            name = this._prescaleSet._nodeName;
        }
        return name;
    }
}
