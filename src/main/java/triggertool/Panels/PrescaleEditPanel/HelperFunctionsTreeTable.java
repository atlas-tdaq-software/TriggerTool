/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package triggertool.Panels.PrescaleEditPanel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.L1.L1Prescale;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggertool.L1Records.CutToValueConverter;

/**
 * Set of helper functions to simplify the handling of TreeTables in PrescaleEdit.java.
 *
 * @author Tiago Perez
 */
public class HelperFunctionsTreeTable {

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");

    /**
     * Create an L1PrescaleEditItemNode containing one prescaleSet, with its
     * children being CTP items
     * @param l1p
     * @param ctpItems
     * @return
     */
    public static final L1PrescaleEditItemNode createNodeFromPrescale(L1Prescale l1p, List<L1PrescaleEditItemNode> ctpItems) {
        if (l1p == null || ctpItems == null) {
            logger.warning("input is  null");
            return null;
        }
        L1PrescaleEditItemNode prescaleNode = new L1PrescaleEditItemNode(l1p.get_name(), l1p.get_id(), l1p.get_version(), true);
        for (int i = 0; i < ctpItems.size(); i++) {
            L1PrescaleEditItemNode itemNode = new L1PrescaleEditItemNode(prescaleNode, ctpItems.get(i), l1p.get_val(ctpItems.get(i).getId() + 1).toString());
            itemNode.setPrescaleAsLabel();
            prescaleNode.add(itemNode);
        }
        return prescaleNode;
    }

    /**
     * Creates the tree from the given L1Prescale and adds it to the ROOT node.
     * @param root the root node of the TreeTable.
     * @param l1ps a L1Prescale.
     */
    public static final void addPrescaleSet(L1PrescaleEditItemNode root, L1Prescale l1ps) {
        // Break if not root node.
        if (root == null || !root.isRoot() || l1ps == null) {
            logger.warning("input is  null or node is not ROOT.");
            return;
        } else if (getPrescale(root, l1ps.get_id()) != null) {
            logger.log(Level.FINEST, "prescale already present: {0}: {1} v{2}, skipping", new Object[]{l1ps.get_id(), l1ps.get_name(), l1ps.get_version()});
            return;
        }
        // Create New Prescale Set Node
        L1PrescaleEditItemNode prescaleSetNode =
                new L1PrescaleEditItemNode(l1ps.get_name(), l1ps.get_id(), l1ps.get_version(), true);
        List<L1PrescaleEditItemNode> ctpItems = root.getChildren();

        for (L1PrescaleEditItemNode item : ctpItems) {
            // NOTE: get_val(ctpid+1)! 
            L1PrescaleEditItemNode node = new L1PrescaleEditItemNode(prescaleSetNode, item, l1ps.get_val(item.getId() + 1).toString());
            node.setPrescaleAsLabel();

            item.clearValue(); // Clear the data on the CTP ITEM.
        }
    }

    /**
     *
     * @param root
     * @param listOfPrescales
     */
    public static final void addPrescaleSets(L1PrescaleEditItemNode root, List<L1Prescale> listOfPrescales) {
        if (listOfPrescales == null) {
            return;
        }
        for (L1Prescale l1Prescale : listOfPrescales) {
            addPrescaleSet(root, l1Prescale);
        }
    }

    /**
     * Removes the prescaleSet from a root node. Iterate through all ctp items in
     * root and removes all children pointing to this prescale set. After this
     * removes all remainig children in the prescale set.
     * @param root
     * @param prescaleSet
     */
    public static final void flushPrescaleSet(final L1PrescaleEditItemNode root, final L1PrescaleEditItemNode prescaleSet) {
        List<L1PrescaleEditItemNode> ctpItems = root.getChildren();
        for (L1PrescaleEditItemNode ctpItem : ctpItems) {
            List<L1PrescaleEditItemNode> items = ctpItem.getChildren();
            ctpItem.clearValue(); // Clear the data on the CTP ITEM.
            List<Integer> removeIdx = new ArrayList<>();
            for (L1PrescaleEditItemNode item : items) {
                if (item.getPrescaleSet().equals(prescaleSet)) {
                    //items.remove(item);
                    removeIdx.add(items.indexOf(item));
                }
            }
            for (Integer idx : removeIdx) {
                items.remove(idx.intValue());
            }
        }
        prescaleSet.removeAllChildren();
    }

    /**
     * Remove prescale sets from a root node. If no ids are given, all sets are
     * removed.
     * @param root a L1PrescaleEditRoot node/
     * @param psIds
     */
    public static final void clearPrescales(L1PrescaleEditItemNode root, List<Integer> psIds) {
        if (root.isRoot()) {
            if (psIds == null || psIds.size() < 1) {
                List<L1PrescaleEditItemNode> prescaleSets = HelperFunctionsTreeTable.getPrescales(root);
                for (L1PrescaleEditItemNode prescale : prescaleSets) {
                    HelperFunctionsTreeTable.flushPrescaleSet(root, prescale);
                }
            } else {
                for (Integer psId : psIds) {
                    L1PrescaleEditItemNode prescale = HelperFunctionsTreeTable.getPrescale(root, psId);
                    HelperFunctionsTreeTable.flushPrescaleSet(root, prescale);
                }
            }
        }
    }

    /**
     * Clear the JTreeTable keeping hte prescales sets give by the vector of ids.
     * @param root the root node of the table.
     * @param keepIds a vector with the ids to keep.
     */
    static public void clearPrescalesButKeep(L1PrescaleEditItemNode root, List<Integer> keepIds) {
        if (root.isRoot()) {
            if (keepIds == null || keepIds.size() < 1) {
            clearPrescales(root, null);
            } else {
                // TODO this returns null if there are no differences between tables.
                List<L1PrescaleEditItemNode> prescales = HelperFunctionsTreeTable.getPrescales(root);

                if (prescales != null) {
                    for (L1PrescaleEditItemNode ps : prescales) {
                        if (!keepIds.contains(ps.getId())) {
                            HelperFunctionsTreeTable.flushPrescaleSet(root, ps);
                        }
                    }
                }
            }
        }
    }

    /**
     *
     * @param root
     * @return
     */
    public static List<L1PrescaleEditItemNode> getPrescales(L1PrescaleEditItemNode root) {
        if (!root.isRoot() || root.getChildCount() == 0) {
            return null;
        }

        List<L1PrescaleEditItemNode> prescaleSets = new ArrayList<>();
        L1PrescaleEditItemNode ctp0 = root.getChildAt(0);
        List<L1PrescaleEditItemNode> items = ctp0.getChildren();
        for (L1PrescaleEditItemNode item : items) {
            prescaleSets.add(item.getPrescaleSet());
        }
        return prescaleSets;
    }
    
    /**
     * Find the itemNode of the prescale set with id psId.
     * @param root the root node.
     * @param psId the id of the prescale.
     * @return the node of the prescale with id=psId.
     */
    private static L1PrescaleEditItemNode getPrescale(L1PrescaleEditItemNode root, int psId) {
        if (!root.isRoot() || root.getChildCount() == 0) {
            return null;
        }
        L1PrescaleEditItemNode ctp0 = root.getChildAt(0);
        List<L1PrescaleEditItemNode> items = ctp0.getChildren();
        for (L1PrescaleEditItemNode item : items) {
            if (item.getPrescaleSet().getId().equals(psId)) {
                return item.getPrescaleSet();
            }
        }
        return null;
    }

    /**
     * Creates a L1Prescale, i.e. a prescale set from the values on the table
     * @param l1m the linkedHashMap visible on the main PrescaleEdit panel.
     * @param setName the name of the set.
     * @return a L1Prescale object containing the info at l1m.
     */
    public static L1Prescale createPsSetFromNodeMap(final LinkedHashMap<Integer, L1PrescaleEditItemNode> l1m,
            final String setName) {
        L1Prescale newL1PrescaleSet = new L1Prescale();
        newL1PrescaleSet.set_name(setName);
        // never used but set them anyway for consistency
        // newL1PrescaleSet.set_lumi("0.0");
        newL1PrescaleSet.set_shift_safe(0);
        newL1PrescaleSet.set_default(0);

        // read the prescales from the map
        Set<Map.Entry<Integer, L1PrescaleEditItemNode>> set = l1m.entrySet();
        Iterator<Map.Entry<Integer, L1PrescaleEditItemNode>> itr = set.iterator();
        while (itr.hasNext()) {
            Map.Entry<Integer, L1PrescaleEditItemNode> me = itr.next();
            L1PrescaleEditItemNode l1rowdata = me.getValue();

            newL1PrescaleSet.set_val(l1rowdata.getId() + 1, CutToValueConverter.calculateCutFromPrescale(l1rowdata.getPrescaleValue()));
        }
        return newL1PrescaleSet;
    }

    /**
     * Creates a root node with all the prescale sets given.
     * @param super_master the super master table, from here we get the Item names.
     * @param prescales the list of selected prescales.
     * @param root
     * @return the root node to add to a tree.
     * @throws java.sql.SQLException
     */
    public static final L1PrescaleEditItemNode generateL1Tree(
            SuperMasterTable super_master,
            List<L1Prescale> prescales,
            L1PrescaleEditItemNode root) throws SQLException {

        if (!root.isRoot()) {
            return null;
        }
        List<L1PrescaleEditItemNode> ctpItems = new ArrayList<>();

        List<L1Item> items = super_master.get_l1_master_table().get_menu().getItems();
        int ctpid = 0;
        for (int ii = 0; ii < items.size(); ii++) {
            L1Item item = items.get(ii);
            int itemCTPId = item.get_ctp_id();
            // Fill the tree with dummy items if item is not present.
            while (itemCTPId > ctpid) {
                L1PrescaleEditItemNode ctpItem = new L1PrescaleEditItemNode("", ctpid);
                ctpItems.add(ctpItem);
                ctpid++;
            }
            L1PrescaleEditItemNode ctpItem = new L1PrescaleEditItemNode(item.get_name(), item.get_ctp_id());
            ctpItems.add(ctpItem);
            ctpid++;
        }

        // Add all ctp items to the root node.
        root.addChildren(ctpItems);
        // iterate thru all selected prescaleSets and create prescale set nodes.
        // link the nodes with the ctp item
        int numOfPrescales = prescales.size();
        for (int i = 0; i < numOfPrescales; i++) {
            L1Prescale l1Prescale = prescales.get(i);
            //System.err.println(i + " " + l1Prescale.get_name());
            L1PrescaleEditItemNode psSetNode = HelperFunctionsTreeTable.createNodeFromPrescale(l1Prescale, ctpItems);
        }
        // Now the this.ROOT node contains all ctp items with all prescales as
        // children.
        return root;
    }
}
