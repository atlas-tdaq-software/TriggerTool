package triggertool.Panels.PrescaleEditPanel;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * Tablemodel to hold the L1Prescales. Contains functions to link the boolean
 * column to the prescales. Allows for easy turning on or off of prescales. To
 * enable sorting will be decorated by the table sorter after being called.
 *
 * @author Alex Martyniuk
 */
@SuppressWarnings("serial")
public final class L1PrescaleTableModel extends AbstractTableModel {

    /**
     * Stores the data that the table is drawn from. The first vector holds the
     * row position information The second vector holds the Object (name of
     * prescale), Integer(Prescale value), Bool(Prescale On/Off) Can be filled
     * with rows using add and passing a vector< object > with Object, integer,
     * Bool in it. Can edit specific positions using setValueAt Can return
     * values stored at any position with getValueAt
     */
    protected List<L1PrescaleEditItemNode> dataVector = new ArrayList<>();
    /**
     * Stores the names of the columns for usage in getColumnName
     */
    private final String[] columnNames = {"CTP Item", "Input Prescale", "Actual Prescale", "In/Out"};
    /**
     * Stored the class of the columns for usage in getColumnClass
     */
    private final Class[] types = new Class[]{
        java.lang.String.class,
        java.lang.String.class,
        java.lang.String.class,
        java.lang.Boolean.class};
    /**
     * Stores the edit info to be passed to is CellEditable
     */
    boolean[] canEdit = new boolean[]{false, true, false, true};

    /**
     *
     */
    public static int COLINPUT = 1;

    /**
     *
     */
    public static int COLVALUE = 2;

    /**
     *
     */
    public static int COLBOOL = 3;
    private int SortingFlag;

    /**
     * Constructor for the tablemodel
     */
    public L1PrescaleTableModel() {
    }

    /**
     * @return 
     * @{@inheritDoc}
     */
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    /**
     * @param col
     * @return 
     * @{@inheritDoc}
     */
    @Override
    public String getColumnName(final int col) {
        return columnNames[col];
    }

    /**
     * @return 
     * @inheritDoc
     */
    @Override
    public Class getColumnClass(int columnIndex) {
        return types[columnIndex];
    }

    /**
     * @return 
     * @inheritDoc
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return this.canEdit[columnIndex];
    }

    /**
     * Clears the datavector so a new prescale set can be loaded.
     */
    public void clearNumRows() {
        dataVector.clear();
        fireTableDataChanged();
    }

    /**
     * @return 
     * @inheritDoc
     */
    @Override
    public int getRowCount() {
        return dataVector.size();
    }

    /**
     * @param row 
     * @param col 
     * @return 
     * @inheritDoc
     */

    @Override
    public Object getValueAt(int row, int col) {
        L1PrescaleEditItemNode _row = dataVector.get(row);
        if (col == 0) {
            return _row.getLabel();
        } else if (col == COLINPUT) {
            return _row.getInput();
        } else if (col == COLVALUE) {
            return _row.getPrescaleValue();
        } else if (col == COLBOOL) {
            return _row.getFlag();
        } else {
            return null;
        }
    }

    /**
     * Adds a row to the datavector. Then tells the table that the data has been
     * updated so needs to be redrawn.
     * @param L1prescalesdata_v
     */
    public void add(L1PrescaleEditItemNode L1prescalesdata_v) {
        dataVector.add(L1prescalesdata_v);
        fireTableDataChanged();


    }

    /**
     * Allows values on the table to be changed after being initially filled.
     * Implements boolean/integer linking. Tick box on = positive. Tick box off
     * = negative. Updates table after a cell has been changed, so it can be
     * redrawn. Also allows alternative setvalueat to be called if sorting flag
     * is on.
     *
     * @param value
     * @param row
     * @param col
     */
    @Override
    public void setValueAt(Object value, int row, int col) {
       
        if (col == L1PrescaleTableModel.COLINPUT) {
            dataVector.get(row).setInput(Double.valueOf((String) value));

        } else if (col == L1PrescaleTableModel.COLBOOL) {
            boolean bool = Boolean.parseBoolean(value.toString());
            Object data2 = getValueAt(row,  L1PrescaleTableModel.COLVALUE);
            double prescale = Double.parseDouble(data2.toString());
            if(!bool && prescale > 0){
                flipSign(row);
            } else if(bool && prescale < 0){
                flipSign(row);
            }
        }
        if (getFlag() == 1) {
            return;
        }
        fireTableDataChanged();
        fireTableCellUpdated(row, col);
    }

    
    private void flipSign(int row){
        double prescaleValue = (Double) getValueAt(row,  L1PrescaleTableModel.COLVALUE);
        dataVector.get(row).setValue(-prescaleValue);
    }
    
    /**
     * Returns the current sorting flag.
     *
     * @return
     */
    public int getFlag() {
        return SortingFlag;
    }

    /**
     * Sets the sorting flag.
     *
     * @param Flag
     */
    public void setFlag(int Flag) {
        SortingFlag = Flag;
    }

}
