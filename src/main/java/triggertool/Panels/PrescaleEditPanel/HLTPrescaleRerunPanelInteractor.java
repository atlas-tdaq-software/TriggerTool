/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.Panels.PrescaleEditPanel;

import java.util.ArrayList;
import java.util.List;
import triggerdb.Entities.HLT.HLTPrescale;

/**
 *
 * @author Michele
 */
public class HLTPrescaleRerunPanelInteractor {
    
    HLTPrescaleRow mainTableRow;
    
    /**
     *
     * @param mainTableRow
     */
    public HLTPrescaleRerunPanelInteractor(HLTPrescaleRow mainTableRow) {
        this.mainTableRow = mainTableRow; 
    }
    
    /**
     *
     * @return
     */
    public List<HLTPrescaleReRunRow> GetReRunRows(){
        List<HLTPrescaleReRunRow> list = new ArrayList<>();
        for(HLTPrescale p : mainTableRow.reRunList){
            list.add(new HLTPrescaleReRunRow(p, mainTableRow));
        }
        return list;
    }
    
    /**
     *
     * @return
     */
    public int GetChainId(){
        return mainTableRow.getCounter();
    }
    
}
