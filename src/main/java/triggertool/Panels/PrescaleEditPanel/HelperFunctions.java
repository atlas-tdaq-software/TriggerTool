/*
 */
package triggertool.Panels.PrescaleEditPanel;

import java.util.List;
import java.util.Iterator;
import java.util.LinkedHashMap;
import javax.swing.JOptionPane;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.L1.L1Prescale;
import triggertool.L1Records.CutToValueConverter;




/**
 * Set of helper functions to simplify PrescaleEdit.java.
 *
 * @author tiago perez <tiago.perez'@'desy.de>
 */
public final class HelperFunctions {

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    /**
     * Iterate over a list of prescales and compares if the five psset is
     * already there.
     *
     * @param l1ps the newly created prescale set.
     * @param psVector the list of prescale sets in DB.
     * @return The index of the prescale if exist, else -1.
     */
    public static int checkPrescaleSetExist(final L1Prescale l1ps,
            final List<L1Prescale> psVector) {
        int returnPsIndex = -1;
        Iterator<L1Prescale> pssIt = psVector.iterator();
        int counter = 0;
        while (pssIt.hasNext() && returnPsIndex < 0) {
            L1Prescale pss = pssIt.next();
            // Compare with existing prescalesets.
            if (l1ps.get_id() == pss.get_id()) {
                String msg = "New L1 prescale set is identical to \n" + pss;
                JOptionPane.showMessageDialog(null, msg);
                logger.warning(msg);
                returnPsIndex = counter;
                break;

            }
            counter++;
        }
        return returnPsIndex;
    }

    /**
     * Update L1_TM_TO_PS. set USED to 0.
     * @param linkid 
     * @return 0 ok, -1 bad.
     */
    public static final int updateL1TMPS(int linkid) {
        String query = "UPDATE L1_TM_TO_PS SET L1TM2PS_USED=0 WHERE L1TM2PS_ID=?";
        query = ConnectionManager.getInstance().fix_schema_name(query);
        try {
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ps.setInt(1, linkid);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, " Error updating  L1_TM_TO_PS \n{0}", e);
            return -1;
        }
        return 0;
    }

    /**
     *
     * @param potentialyModifiedPrescaleSet
     * @param originalPrescaleSet
     * @return
     */
    public final static boolean checkIfPrescaleSetEdited(final LinkedHashMap<Integer, L1PrescaleEditItemNode> potentialyModifiedPrescaleSet,
            L1Prescale originalPrescaleSet) {
        // If we have already looked at one set, there will be some entries in the map
        // Compare whats there now with the original - prompt to save if different
        
        // we compare the cut values
        boolean l1prescalesedited = false;
        if (potentialyModifiedPrescaleSet.size() > 0) {
            for (int ctpId = 0; ctpId < L1Prescale.LENGTH; ctpId++) {

                int originalCut = originalPrescaleSet.get_val(ctpId + 1); // the original cut
                
                if (potentialyModifiedPrescaleSet.containsKey(ctpId)) { // ctp is key
                    L1PrescaleEditItemNode l1rowdata = potentialyModifiedPrescaleSet.get(ctpId);
                    //float newPS = l1rowdata.getValue();
                    int newCut = CutToValueConverter.calculateCutFromPrescale(l1rowdata.getPrescaleValue());
                    if (newCut != originalCut) {
                        logger.log(Level.INFO, "Found modified L1 PS cut value! Now: {0}, original: {1}", new Object[]{newCut, originalCut});
                        l1prescalesedited = true;
                        break;
                    }
                }
            }
        }
        return l1prescalesedited;
    }

    /**
     * Iterates thru all items belonging to the frst CTP item of a root node and
     * finds the first PrescaleSet with the give id.
     * @param tableRootNode the root node.
     * @param id the DB id of the prescale set.
     * @return the prescale set node with id=<code>id</code>
     */
    public static L1PrescaleEditItemNode getPrescaleSetWithId(L1PrescaleEditItemNode tableRootNode, int id) {
        if (tableRootNode == null || !tableRootNode.isRoot()) {
            logger.severe("getPrescaleSetWithId:  bad node.");
            return null;
        }
        // Get First CTP ITEM, all leafs.
        List<L1PrescaleEditItemNode> items = tableRootNode.getChildAt(0).getChildren();
        for (L1PrescaleEditItemNode item : items) {
            L1PrescaleEditItemNode psSet = item.getPrescaleSet();
//            System.out.println("LABEL: " + psSet.getLabel() + "  ID: " + psSet.getId());
            if (psSet != null && psSet.getId().equals(id)) {
                return item.getPrescaleSet();
            }
        }
        //      System.err.println("getPrescaleSetWithId:  prescale with id: " + id + " not present.");
        return null;
    }

    /**
     *
     * @param tablePrescaleSet
     * @return
     */
    public static LinkedHashMap<Integer, L1PrescaleEditItemNode> PrescaleSetNode2Map(L1PrescaleEditItemNode tablePrescaleSet) {
        LinkedHashMap<Integer, L1PrescaleEditItemNode> map = new LinkedHashMap<>();
        for (L1PrescaleEditItemNode item : tablePrescaleSet.getChildren()) {
            L1PrescaleEditItemNode L1prescalesdata_v = item;
//            L1PrescaleEditItemNode L1prescalesdata_v = new L1PrescaleEditItemNode();
//            L1prescalesdata_v.setId(item.getCtpItem().getId());
//            //L1prescalesdata_v.setNodeName(item.getCtpItem().getNodeName());
//            L1prescalesdata_v.setNodeName(item.getPrescaleSet().getNodeName());
//            L1prescalesdata_v.setValue(item.getValue());
            map.put(item.getCtpItem().getId(), L1prescalesdata_v);
        }
        // Warning: the map has less than 256 items.
        return map;
    }

    /**
     * Create a pop up message asking to save some sets.
     * @param modifiedSetNames.
     * @return true (save) or false (discard).
     */
    public static boolean promptToSave(List<String> modifiedSetNames) {
        if (modifiedSetNames.size() > 0) {
            logger.info("L1 prescales have been edited");
            String[] choices = {"Save", "Discard"};
            String msg = "You edited the following L1 Prescale sets: \n";
            for (String names : modifiedSetNames) {
                msg += "\t" + names + "\n";
            }
            msg += "\nDo you wish to save the changes as new sets or discard them?";
            Integer response = JOptionPane.showOptionDialog(null,
                    msg, "", 0,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    choices,
                    choices[0]);
            // if user press ESC response==-1.
            if (response >= 0 && choices[response].equals("Save")) {
                logger.info("Saving changes");
                return true;
            } else {
                logger.info("Discarding changes");
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Creates a pop up message to say that two prescales sets are equal.
     * @param namesEqual the name of the 2 prescale sets diff'd.
     */
    public static void promptEqual(List<String> namesEqual) {
        String msg = "";
        if (namesEqual.size() == 2) {
            msg += namesEqual.get(0) + " and " + namesEqual.get(1) + " are equal.";
            logger.fine(msg);
            String[] choices = {"Close"};
            JOptionPane.showOptionDialog(null,
                    msg, "", 0,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    choices,
                    choices[0]);

        } else {
            msg += "\n problems diffing prescale sets:\n";
            for (String name : namesEqual) {
                msg += name + "\n";
            }
            logger.info(msg);
        }
    }
}
