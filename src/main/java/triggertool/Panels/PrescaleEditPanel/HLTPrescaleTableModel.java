package triggertool.Panels.PrescaleEditPanel;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 * Tablemodel to hold the L1Prescales. Contains functions to link the boolean
 * column to the prescales. Allows for easy turning on or off of prescales. To
 * enable sorting will be decorated by the table sorter after being called.
 *
 * @author Alex Martyniuk
 * @author Paul Bell
 * @author Tiago Perez
 */
public final class HLTPrescaleTableModel extends AbstractTableModel {

    /**
     * Stores the data that the table is drawn from. The first vector holds the
     * row position information The second vector holds the Object (name of
     * prescale), String(Prescale value), Bool(Prescale On/Off). Can be filled
     * with rows using add and passing a vector< object > with Object, string,
     * Bool in it. Can edit specific positions using setValueAt Can return
     * values stored at any position with getValueAt.
     */
    private final ArrayList<HLTPrescaleRow> dataVector;
    /**
     * Stores the names of the columns for usage in getColumnName.
     */

    private final String[] columnNames = {
        "Chain",
        "L1 seed",
        "Counter",
        "In/Out",
        "PS",
        "PT",
        "Stream",
        "Condition",
        "ReRun"
    };
    /**
     * Stored the class of the columns for usage in getColumnClass.
     */
    private final Class[] types = new Class[]{
        java.lang.String.class, // Object.class,
        java.lang.String.class, // Object.class,
        java.lang.Integer.class, // Counter is an int - java.lang.Object.class,
        java.lang.Boolean.class,
        java.lang.String.class, // Object.class,  - L2/EF string
        java.lang.String.class,
        java.lang.String.class,
        java.lang.String.class,
        java.lang.String.class
    };
    /**
     * Stores the edit info to be passed to is CellEditable.
     */
    private final boolean[] canEdit = new boolean[]{
        false, false, false, true, true, true, true, true, true
    };
    private int SortingFlag = 0;

    /**
     * Constructor.
     */
    public HLTPrescaleTableModel() {
        this.dataVector = new ArrayList<>();
    }

    /**
     * Returns the number of columns.
     *
     * @return number of columns.
     */
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    /**
     * @param col
     * @return
     * @{@inheritDoc}
     */
    @Override
    public String getColumnName(final int col) {
        return columnNames[col];
    }

    /**
     * Returns the type of column passed to it.
     *
     * @param columnIndex the column number.
     * @return the class of the column.
     */
    @Override
    public Class getColumnClass(final int columnIndex) {
        return types[columnIndex];
    }

    /**
     * Gets whether a cell is editable or not.
     *
     * @param rowIdx the row number.
     * @param colIdx the column number.
     * @return <code>true</code> if cell is editable, else <code>false</code>.
     */
    @Override
    public boolean isCellEditable(final int rowIdx, final int colIdx) {
        return canEdit[colIdx];
    }

    /**
     * Clears the datavector so a new prescale set can be loaded.
     */
    public void clearNumRows() {
        dataVector.clear();
        fireTableDataChanged();
    }

    /**
     * Gets the current size of the table by finding the size of the datavector.
     *
     * @return the number of rows.
     */
    @Override
    public int getRowCount() {
        return dataVector.size();
    }

    /**
     * @param rowIndex
     * @param colIndex
     * @return
     */
    @Override
    public Object getValueAt(final int rowIndex, final int colIndex) {
        HLTPrescaleRow value = getRowAt(rowIndex);
        return value.get(colIndex);
    }

    /**
     * @param rowIndex
     * @return
     */
    public HLTPrescaleRow getRowAt(final int rowIndex) {
        return dataVector.get(rowIndex);
    }

    /**
     * Adds a row to the datavector. Then tells the table that the data has been
     * updated so needs to be redrawn.
     *
     * @param row a HLTPrescaleRow object with data of a row.
     */
    public void add(final HLTPrescaleRow row) {
        dataVector.add(row);
        fireTableDataChanged();
    }

    /**
     * Allows values on the table to be changed after being initially filled.
     * Implements boolean/integer linking. Tick box on = positive. Tick box off
     * = negative. Updates table after a cell has been changed, so it can be
     * redrawn. Also allows alternative setValueAt to be called if sorting flag
     * is on.
     *
     * @param value the value to set.
     * @param row the row number.
     * @param col the column number.
     */
    @Override
    public void setValueAt(final Object value, final int row,
            final int col) {
        if(value.equals("")){
            return;
        }
        dataVector.get(row).setElementAt(value, col);
        if (col == HLTPrescaleRow.IN_OUT) {
            boolean bool = Boolean.parseBoolean(value.toString());
            Object data2 = getValueAt(row, HLTPrescaleRow.PS);
            String prescale = data2.toString();
            String rerunprescale = (String) getValueAt(row, HLTPrescaleRow.RERUN);
            if (!bool) {
                if (!prescale.startsWith("-")) {
                    if (!prescale.equals("0")) {
                        // Add a "-" to prescale.
                        dataVector.get(row).setPrescale("-" + prescale);
                    } else {
                        dataVector.get(row).setPrescale("-1");
                    }
                }
            } else {
                if (prescale.startsWith("-")) {
                    if (prescale.equals("-1") && (Double.parseDouble((rerunprescale).split(":")[0]) > 0)) {
                        dataVector.get(row).setPrescale("0");

                    } else {
                        // Remove the "-".
                        dataVector.get(row).setPrescale(prescale.substring(1));
                    }
                }
            }
            //this.fireTableCellUpdated(row, HLTPrescaleRow.PS);
        } else {
            if (col == HLTPrescaleRow.PS) {
                String prescale = dataVector.get(row).get(col).toString();
                if (prescale.startsWith("-")) {
                    dataVector.get(row).setElementAt(false, HLTPrescaleRow.IN_OUT);
                } else {
                    dataVector.get(row).setElementAt(true, HLTPrescaleRow.IN_OUT);
                }
            }
        }
        if (getFlag() == 1) {
            // If the sorting flag is set to 1 do not fire table data changed.
            // This is important for the enable/disable all, things go wrong if
            // the table data is updated at every setValueAt call when table is
            // sorting and enabling/disabling multiple entries.
            // Still need to fire table changed after calling
            // this setValueAtSort, or table will not update.
            return;
        }
        this.fireTableDataChanged();
        this.fireTableCellUpdated(row, col);
    }

    /**
     * Returns the current sorting flag.
     *
     * @return the sorting flag.
     */
    public int getFlag() {
        return this.SortingFlag;
    }

    /**
     * Sets the sorting flag.
     *
     * @param flag the new value of the sorting flag.
     */
    public void setFlag(final int flag) {
        this.SortingFlag = flag;
    }
}
