package triggertool.Panels.PrescaleEditPanel;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.*;

/**
 * Tablemodel to hold the L1Prescales. Contains functions to link the boolean
 * column to the prescales.
 * Allows for easy turning on or off of prescales.
 * To enable sorting will be decorated by the table sorter after being called.
 * @author Alex Martyniuk
 */
public final class L1PrescaleDiffTableModel extends AbstractTableModel {

    /**
     * Stores the data that the table is drawn from.
     * The first vector holds the row position information
     * The second vector holds the Object (name of prescale), Integer(Prescale
     * value), Bool(Prescale On/Off). Can be filled with rows using add and
     * passing a vector< object > with Object, integer, Bool in it.
     * Can edit specific positions using setValueAt
     * Can return values stored at any position with getValueAt.
     */
    protected List<ArrayList<Object>> dataVector = new ArrayList<>();
    /**
     * Stores the names of the columns for usage in getColumnName.
     */
    private final String[] columnNames = {
        "CTP Item", "Prescale: Set 1", "Prescale: Set 2"
    };
    /**
     * Stored the class of the columns for usage in getColumnClass.
     */
    private final Class[] types = new Class[]{
        java.lang.String.class, java.lang.String.class, java.lang.String.class
    };
    /**
     * Stores the edit info to be passed to is CellEditable.
     */
    private final boolean[] canEdit = new boolean[]{
        false, false, false
    };
    private int SortingFlag;

    /**
     * Constructor for the tablemodel.
     */
    public L1PrescaleDiffTableModel() {
    }

    /**
     * Returns the number of columns
     * @return
     */
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    /**
     * Returns the columns name
     * @param col
     * @return
     */
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    /**
     * Returns the type of column passed to it.
     * @param columnIndex
     * @return
     */
    @Override
    public Class getColumnClass(int columnIndex) {
        return types[columnIndex];
    }

    /**
     * Sets whether a cell is editable or not.
     * @param rowIndex
     * @param columnIndex
     * @return
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return canEdit[columnIndex];
    }

    /**
     * Clears the datavector so a new prescale set can be loaded.
     */
    public void clearNumRows() {
        dataVector.clear();
    }

    /**
     * Gets the current size of the table by finding the size of the datavector.
     * @return
     */
    @Override
    public int getRowCount() {
        return dataVector.size();
    }

    /**
     * Gets the value at specified row/col position from the datavector where the table data is stored.
     * @param row
     * @param col
     * @return
     */
    @Override
    public Object getValueAt(int row, int col) {
        List<Object> value = dataVector.get(row);
        return value.get(col);
    }

    /**
     * Adds a row to the datavector. 
     * Then tells the table that the data has been updated so needs to be redrawn.
     * @param L1prescalesdata_v
     */
    public void add(ArrayList<Object> L1prescalesdata_v) {
        dataVector.add(L1prescalesdata_v);
        fireTableDataChanged();
    }
}