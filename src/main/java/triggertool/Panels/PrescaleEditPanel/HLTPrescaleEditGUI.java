/*
 * HLTPrescaleEditGUI.java
 *
 * Created on Jul 19, 2010, 3:56:10 PM
 */
package triggertool.Panels.PrescaleEditPanel;

import java.awt.event.MouseEvent;
import java.io.File;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import triggertool.Auxiliary.TriggerToolCellRenderer;
import triggertool.BackEnd.HLTPrescaleEdit;
import triggerdb.Entities.InternalWriteLock;
import triggertool.Components.InternalWriteLockDialog;
import triggertool.Components.TableSorter;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.UserMode;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.HLT.HLTTriggerChain;
import triggerdb.Entities.HLT.HLTTriggerMenu;
import triggerdb.Entities.HLT.HLTTriggerStream;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.UploadException;
import triggertool.Panels.MessageDialog;
import triggertool.TTExceptionHandler;

/**
 * The graphical panel to display and edit hlt prescale sets. Interface to
 * Database is delegated to HLTPrescaleEdit.java, this should allow an easier
 * implementation of a command line interface if needed.
 *
 * @author tiago perez.
 */
public final class HLTPrescaleEditGUI extends javax.swing.JPanel
        implements Serializable {

    /**
     * Needed by Serializable.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The TriggerTool logger.
     */
    private static final Logger logger = Logger.getLogger("TriggerTool");
    /**
     * The id of the current hlt menu.
     */
    private int hltMenuId = -1;
    /**
     * The current hlt menu.
     */
    private HLTTriggerMenu hltMenu = null;
    /**
     * An integer representing the role of the user (see ConnectionManager).
     */
    private UserMode userMode = UserMode.UNKNOWN;
    /**
     * An string holding the previous filter text.
     */
    private String filterHold = "";
    /**
     * An string holding the previous regex filter text.
     */
    private final String regexFilterHold = "";
    /**
     * An string holding the previous regex filter text.
     */
    private final String regexSeedFilterHold = "";
    /**
     * The selected prescale set.
     */
    private ArrayList<ArrayList<Object>> groups = null;
    private HashMap<Integer, HLTTriggerChain> chainsById = null;
    private ArrayList<HLTTriggerChain> chains = null;
    /**
     * The list of all streams.
     */
    //private List<HLTTriggerStream> streams = null;
    private final DefaultListModel<Object> HLTitemsUnUsed = new DefaultListModel<>();
    private String filterText = new String();
    private String seedFilterText = new String();
    private final List<String> GroupChain = new ArrayList<>();
    private final List<String> StreamChain = new ArrayList<>();
    /**
     * bool if filtering with regex fails!
     */

    /**
     * <code>true</code> if we want to see all prescales, else only non hidden
     * ps sets are shown.
     */
    private boolean showHidden = false;
    /**
     * A box to display and write the prescale set comment.
     */
    //private PrescaleCommentBox hltCommentBox;
    /**
     * Prescale set created after an xml upload. Used for diffing.
     */
    private HLTPrescaleEdit hltPrescaleEdit = null;
    /**
     * Conversion factor milliseconds to seconds.
     */
    private static final float MIL2SEC = 1000f;
    // TODO
    /**
     * File chooser
     */
    private final JFileChooser fc = new JFileChooser();

    HLTPrescaleEditInteractor interactor;

    /**
     * Dummy constructor. This should not be used. Only needed for the GUI IDE
     * to be able to initialize this as a component in the GUI builder palette.
     */
    public HLTPrescaleEditGUI() {
        super();
        this.initPanel();
    }

    /**
     * Default constructor. TODO SMT is not needed here. Change constructor to
     * MasterTable or Menu.
     *
     * @param smt the supermasterTable
     * @param isEditable whether we can edit this object or not.
     */
    public HLTPrescaleEditGUI(final SuperMasterTable smt, final boolean isEditable) {
        super();
        long time0 = System.currentTimeMillis();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("XML files", "xml"); // in java 1.6
        fc.setFileFilter(filter);
        fc.setCurrentDirectory(new File("."));
        this.initPanel();
        try {
            this.initDB(smt, isEditable);
            long time1 = System.currentTimeMillis() - time0;
            logger.log(Level.FINER, "\n Time HLTPrscalEditGUI : {0}\n", time1 / MIL2SEC);
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this.getParent(), "Error while creating PrescaleEdit panel.");
        }
    }

    /**
     * Initialises several graphical component and tables.
     */
    private void initPanel() {
        // Initilize Panel
        initComponents();

        tableDetailsHLT.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        //
        listTablesHLTUnUsed.addListSelectionListener(new PrescaleSetListListener());
        listTablesHLTUnUsed.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        /*tableDetailsHLT.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    JTable target = (JTable) e.getSource();
                    int row = target.rowAtPoint(e.getPoint());
                    int column = target.columnAtPoint(e.getPoint());
                    HLTPrescaleRerunPanelInteractor reRunInteractor = interactor.HandleOpenReRunTable(row, column);
                    if (reRunInteractor != null) {
                        HLTPrescaleReRunGUI hltPrescaleReRunPanel = new HLTPrescaleReRunGUI(reRunInteractor);
                    }
                }
            }
        });*/
    }

    /**
     * Runs the initial DB queries and loads all needed objects to memory.
     *
     * @param smt the SuperMasterKey.
     * @param isEditable the boolean flag with the info of whether this panel is
     * editable or not.
     */
    private void initDB(final SuperMasterTable smt, final boolean isEditable) throws SQLException {
        hltMenu = smt.get_hlt_master_table().get_menu();
        hltMenuId = hltMenu.get_id();
        chains = hltMenu.getChains();
        hltPrescaleEdit = new HLTPrescaleEdit(hltMenuId);
        interactor = new HLTPrescaleEditInteractor(hltMenu, chains);
        tableDetailsHLT.setModel(interactor.hltMainModel);
        JTableHeader headerHLT = tableDetailsHLT.getTableHeader();
        TableSorter sorterHLT = new TableSorter(interactor.hltMainModel);
        tableDetailsHLT.setModel(sorterHLT);
        sorterHLT.setTableHeader(headerHLT);
        TableColumnModel tcm = tableDetailsHLT.getColumnModel();
        // Remove vertical lines.
        tcm.setColumnMargin(0);
        tableDetailsHLT.setDragEnabled(true);

        //first column is bigger
        tcm.getColumn(HLTPrescaleRow.NAME).setPreferredWidth(250);
        //first column is bigger
        tcm.getColumn(HLTPrescaleRow.SEED).setPreferredWidth(200);
        // Use cutom renderer. Align RIGHT.
        TriggerToolCellRenderer defRenderer = new TriggerToolCellRenderer();
        TriggerToolCellRenderer stringRend = new TriggerToolCellRenderer();
        stringRend.setHorizontalAlignment(SwingConstants.LEFT);
        tableDetailsHLT.setDefaultRenderer(Integer.class, defRenderer);
        tcm.getColumn(HLTPrescaleRow.NAME).setCellRenderer(stringRend);
        tcm.getColumn(HLTPrescaleRow.SEED).setCellRenderer(stringRend);
        tcm.getColumn(HLTPrescaleRow.IN_OUT).setCellRenderer(defRenderer);
        tcm.getColumn(HLTPrescaleRow.PS).setCellRenderer(defRenderer);
        tcm.getColumn(HLTPrescaleRow.PT).setCellRenderer(defRenderer);
        tcm.getColumn(HLTPrescaleRow.STREAM).setCellRenderer(defRenderer);
        tcm.getColumn(HLTPrescaleRow.CONDITION).setCellRenderer(defRenderer);
        tcm.getColumn(HLTPrescaleRow.RERUN).setCellRenderer(defRenderer);

        PopulateHLTPrescaleSetLists();

        // Create a Map with all chains indexed my DB ID
        chainsById = new HashMap<>();
        for (HLTTriggerChain hltChain : chains) {
            chainsById.put(hltChain.get_id(), hltChain);
        }

        logger.info("Loading groups");
        this.loadGroups();
        logger.info("Loading streams");
        this.loadStreams();

        if (HLTitemsUnUsed.size() > 0) {
            listTablesHLTUnUsed.setSelectedIndex(0);
        }

        //shifter/user differences
        userMode = ConnectionManager.getInstance().getInitInfo().getUserMode();
        if (userMode.ordinal() < UserMode.SHIFTER.ordinal() || isEditable == false) {
            tableDetailsHLT.setEnabled(false);
            SetNameHLT.setEditable(false);
            jButtonEnableHLT.setEnabled(false);
            jButtonDisableHLT.setEnabled(false);
            jButtonHLTComment.setEnabled(false);
        }

        if (userMode.ordinal() <= UserMode.SHIFTER.ordinal() || isEditable == false) {
            //cannot hide or show sets
            jButtonHLTHide.setEnabled(false);
            jButtonLoadXML.setEnabled(false);
            //jButtonLoadXML.setVisible(false);
            XMLpath.setEnabled(false);
            jButtonBrowse.setEnabled(false);
            //XMLpath.setVisible(false);
        }
        setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelHLTPrescales = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tableDetailsHLT = new javax.swing.JTable(){
            public String getToolTipText(MouseEvent e) {
                String tip = null;
                java.awt.Point p = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                int realColumnIndex = convertColumnIndexToModel(colIndex);

                if (realColumnIndex == 1 && getValueAt(rowIndex, colIndex).toString().contains(",")){
                    String seeds[];
                    seeds = getValueAt(rowIndex, colIndex).toString().split(",");
                    tip = "<html>";
                    for(String s: seeds){
                        tip = tip + s + "<br>";
                    }
                    tip = tip + "</html>";
                }
                return tip;
            }
        };
        jLabel5 = new javax.swing.JLabel();
        SetNameHLT = new javax.swing.JTextField();
        jButtonHLTSA = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        listTablesHLTUnUsed = new javax.swing.JList();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaHLTComment = new javax.swing.JTextArea();
        jLabelHLT = new javax.swing.JLabel();
        jButtonHLTComment = new javax.swing.JButton();
        jButtonEnableHLT = new javax.swing.JButton();
        jButtonDisableHLT = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        filterTextField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jComboHLTGroups = new javax.swing.JComboBox<>();
        jComboHLTStreams = new javax.swing.JComboBox<>();
        ResetFilter = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        seedFilterTextField = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        DiffHLT_PrescaleSet1 = new javax.swing.JComboBox<>();
        DiffHLT_PrescaleSet2 = new javax.swing.JComboBox<>();
        jToggleButtonDiffHLT = new javax.swing.JToggleButton();
        jToggleHLTHide = new javax.swing.JToggleButton();
        jButtonHLTHide = new javax.swing.JButton();
        XMLpath = new javax.swing.JTextField();
        jButtonLoadXML = new javax.swing.JButton();
        jButtonBrowse = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        jButtonHLTRefresh = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 204, 204));

        jPanelHLTPrescales.setMinimumSize(new java.awt.Dimension(400, 600));

        tableDetailsHLT.setModel(new HLTPrescaleTableModel());
        tableDetailsHLT.setUpdateSelectionOnSort(false);
        tableDetailsHLT.setVerifyInputWhenFocusTarget(false);
        tableDetailsHLT.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tableDetailsHLTFocusGained(evt);
            }
        });
        jScrollPane4.setViewportView(tableDetailsHLT);

        jLabel5.setText("Prescale Set Name:");

        SetNameHLT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SetNameHLTActionPerformed(evt);
            }
        });

        jButtonHLTSA.setText("Save");
        jButtonHLTSA.setEnabled(false);
        jButtonHLTSA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHLTSAActionPerformed(evt);
            }
        });

        listTablesHLTUnUsed.setModel(HLTitemsUnUsed);
        jScrollPane6.setViewportView(listTablesHLTUnUsed);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setText("Available Sets for this SMK");

        jTextAreaHLTComment.setColumns(20);
        jTextAreaHLTComment.setLineWrap(true);
        jTextAreaHLTComment.setRows(5);
        jTextAreaHLTComment.setAutoscrolls(false);
        jScrollPane2.setViewportView(jTextAreaHLTComment);

        jLabelHLT.setText("Comment Field");

        jButtonHLTComment.setText("Update Comment");
        jButtonHLTComment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHLTCommentActionPerformed(evt);
            }
        });

        jButtonEnableHLT.setText("Enable All/Selection");
        jButtonEnableHLT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEnableHLTActionPerformed(evt);
            }
        });

        jButtonDisableHLT.setText("Disable All/Selection");
        jButtonDisableHLT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDisableHLTActionPerformed(evt);
            }
        });

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Filters"));

        filterTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                filterTextFieldKeyReleased(evt);
            }
        });

        jLabel6.setText("Chain name:");

        jComboHLTGroups.setModel(new javax.swing.DefaultComboBoxModel<String>(new String[] { "All Groups" }));
        jComboHLTGroups.setMaximumSize(new java.awt.Dimension(59, 20));
        jComboHLTGroups.setPreferredSize(new java.awt.Dimension(98, 20));
        jComboHLTGroups.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboHLTGroupsActionPerformed(evt);
            }
        });

        jComboHLTStreams.setModel(new javax.swing.DefaultComboBoxModel<String>(new String[] { "All Streams" }));
        jComboHLTStreams.setMaximumSize(new java.awt.Dimension(59, 20));
        jComboHLTStreams.setPreferredSize(new java.awt.Dimension(59, 20));
        jComboHLTStreams.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboHLTStreamsActionPerformed(evt);
            }
        });

        ResetFilter.setText("Reset");
        ResetFilter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ResetFilterActionPerformed(evt);
            }
        });

        jLabel9.setText("Groups:");

        jLabel10.setText("Streams:");

        seedFilterTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                seedFilterTextFieldKeyReleased(evt);
            }
        });

        jLabel11.setText("L1 seed:");

        org.jdesktop.layout.GroupLayout jPanel5Layout = new org.jdesktop.layout.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel6)
                            .add(jLabel11))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, filterTextField)
                            .add(seedFilterTextField)))
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel10)
                            .add(jLabel9))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jComboHLTGroups, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jComboHLTStreams, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(ResetFilter)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(filterTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel6))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(seedFilterTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel11))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jComboHLTGroups, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel9))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jComboHLTStreams, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel10))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(ResetFilter))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("Compare sets"));

        DiffHLT_PrescaleSet1.setModel(new javax.swing.DefaultComboBoxModel<Object>(new String[] { "Prescale Set 1" }));
        DiffHLT_PrescaleSet1.setMaximumSize(new java.awt.Dimension(93, 20));
        DiffHLT_PrescaleSet1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DiffHLT_PrescaleSet1ActionPerformed(evt);
            }
        });

        DiffHLT_PrescaleSet2.setModel(new javax.swing.DefaultComboBoxModel<Object>(new String[] { "Prescale Set 2" }));
        DiffHLT_PrescaleSet2.setMaximumSize(new java.awt.Dimension(93, 20));
        DiffHLT_PrescaleSet2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DiffHLT_PrescaleSet2ActionPerformed(evt);
            }
        });

        jToggleButtonDiffHLT.setText("Diff Selected Sets");
        jToggleButtonDiffHLT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButtonDiffHLTActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel6Layout = new org.jdesktop.layout.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, DiffHLT_PrescaleSet2, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(DiffHLT_PrescaleSet1, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jToggleButtonDiffHLT, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 142, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createSequentialGroup()
                .add(DiffHLT_PrescaleSet1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(DiffHLT_PrescaleSet2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jToggleButtonDiffHLT)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jToggleHLTHide.setText("See hidden sets");
        jToggleHLTHide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleHLTHideActionPerformed(evt);
            }
        });

        jButtonHLTHide.setText("Hide");
        jButtonHLTHide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHLTHideActionPerformed(evt);
            }
        });

        XMLpath.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        jButtonLoadXML.setText("Load XML Prescales");
        jButtonLoadXML.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonLoadXMLMouseClicked(evt);
            }
        });
        jButtonLoadXML.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonLoadXMLActionPerformed(evt);
            }
        });

        jButtonBrowse.setText("Browse");
        jButtonBrowse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBrowseActionPerformed(evt);
            }
        });

        jButtonHLTRefresh.setText("Refresh");
        jButtonHLTRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHLTRefreshActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanelHLTPrescalesLayout = new org.jdesktop.layout.GroupLayout(jPanelHLTPrescales);
        jPanelHLTPrescales.setLayout(jPanelHLTPrescalesLayout);
        jPanelHLTPrescalesLayout.setHorizontalGroup(
            jPanelHLTPrescalesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelHLTPrescalesLayout.createSequentialGroup()
                .addContainerGap()
                .add(jPanelHLTPrescalesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelHLTPrescalesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelHLTPrescalesLayout.createSequentialGroup()
                            .add(jButtonLoadXML)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(jButtonBrowse))
                        .add(jPanelHLTPrescalesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jButtonHLTComment)
                            .add(jPanelHLTPrescalesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(jScrollPane2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 316, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(jLabelHLT)
                                .add(jPanelHLTPrescalesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                    .add(jPanelHLTPrescalesLayout.createSequentialGroup()
                                        .add(jButtonHLTRefresh, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jButtonHLTHide)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jToggleHLTHide, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 134, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                    .add(jScrollPane6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 316, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))))
                    .add(jLabel7))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanelHLTPrescalesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelHLTPrescalesLayout.createSequentialGroup()
                        .add(XMLpath)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButtonHLTSA, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 190, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelHLTPrescalesLayout.createSequentialGroup()
                        .add(0, 0, Short.MAX_VALUE)
                        .add(jButtonEnableHLT)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButtonDisableHLT))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelHLTPrescalesLayout.createSequentialGroup()
                        .add(jPanel5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .add(jPanelHLTPrescalesLayout.createSequentialGroup()
                        .add(jLabel5)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(SetNameHLT))
                    .add(jPanelHLTPrescalesLayout.createSequentialGroup()
                        .add(filler1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jScrollPane4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 803, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanelHLTPrescalesLayout.setVerticalGroup(
            jPanelHLTPrescalesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelHLTPrescalesLayout.createSequentialGroup()
                .addContainerGap()
                .add(jPanelHLTPrescalesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanelHLTPrescalesLayout.createSequentialGroup()
                        .add(jPanelHLTPrescalesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                            .add(jLabel5)
                            .add(SetNameHLT, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanelHLTPrescalesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(jPanel5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jPanel6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .add(jPanelHLTPrescalesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanelHLTPrescalesLayout.createSequentialGroup()
                                .add(99, 99, 99)
                                .add(filler1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .add(jPanelHLTPrescalesLayout.createSequentialGroup()
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jScrollPane4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED))))
                    .add(jPanelHLTPrescalesLayout.createSequentialGroup()
                        .add(jLabel7)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(jPanelHLTPrescalesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jToggleHLTHide)
                            .add(jButtonHLTHide)
                            .add(jButtonHLTRefresh))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jScrollPane6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 333, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabelHLT)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jScrollPane2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 156, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(13, 13, 13)))
                .add(jPanelHLTPrescalesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jButtonHLTComment, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButtonDisableHLT)
                    .add(jButtonEnableHLT))
                .add(11, 11, 11)
                .add(jPanelHLTPrescalesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(XMLpath, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButtonLoadXML)
                    .add(jButtonHLTSA)
                    .add(jButtonBrowse))
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelHLTPrescales, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelHLTPrescales, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tableDetailsHLTFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tableDetailsHLTFocusGained
        this._tableDetailsHLTFocusGained(evt);
}//GEN-LAST:event_tableDetailsHLTFocusGained

    private void SetNameHLTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SetNameHLTActionPerformed
        this._SetNameHLTActionPerformed(evt);
}//GEN-LAST:event_SetNameHLTActionPerformed

    private void jButtonHLTSAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHLTSAActionPerformed
        try {
            this._jButtonHLTSAActionPerformed(evt);
        } catch (SQLException ex) {
            Logger.getLogger(HLTPrescaleEditGUI.class.getName()).log(Level.SEVERE, null, ex);
            MessageDialog.showReportDialog(ex);
        }
}//GEN-LAST:event_jButtonHLTSAActionPerformed

    private void jButtonHLTCommentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHLTCommentActionPerformed
        this._jButtonHLTCommentActionPerformed(evt);
}//GEN-LAST:event_jButtonHLTCommentActionPerformed

    private void jButtonEnableHLTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEnableHLTActionPerformed
        this._jButtonEnableHLTActionPerformed(evt);
}//GEN-LAST:event_jButtonEnableHLTActionPerformed

    private void jButtonDisableHLTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDisableHLTActionPerformed
        _jButtonDisableHLTActionPerformed(evt);
}//GEN-LAST:event_jButtonDisableHLTActionPerformed

    private void filterTextFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_filterTextFieldKeyReleased
        this._FilterTextKeyReleased(evt);
}//GEN-LAST:event_filterTextFieldKeyReleased

    private void jComboHLTGroupsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboHLTGroupsActionPerformed
        this._jComboHLTGroupsActionPerformed(evt);
}//GEN-LAST:event_jComboHLTGroupsActionPerformed

    private void jComboHLTStreamsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboHLTStreamsActionPerformed
        this._jComboHLTStreamsActionPerformed(evt);
}//GEN-LAST:event_jComboHLTStreamsActionPerformed

    private void ResetFilterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ResetFilterActionPerformed
        try {
            this.resetHLT();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while Resetting filters in HLTPrescaleEdit panel.");
        }
}//GEN-LAST:event_ResetFilterActionPerformed

    private void seedFilterTextFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_seedFilterTextFieldKeyReleased
        this.seedFilterText = seedFilterTextField.getText().toLowerCase();
        if (seedFilterText.endsWith("\\")) {
            StringBuilder b = new StringBuilder(seedFilterText);
            b.replace(seedFilterText.lastIndexOf("\\"), seedFilterText.lastIndexOf("\\") + 1, "");
            seedFilterText = b.toString();
        }
        if (!testfilter(false)) {
            seedFilterText = regexSeedFilterHold;
        }
        try {
            interactor.updateHLTPrescalesTable(new TableUpdateParameters(filterText, seedFilterText, GroupChain, StreamChain, true));
            tableDetailsHLT.updateUI();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while filtering HLT prescales in HLTPrescaleEdit panel.");
        }
}//GEN-LAST:event_seedFilterTextFieldKeyReleased

    private void jToggleButtonDiffHLTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButtonDiffHLTActionPerformed
        performDiffButtonAction();
}//GEN-LAST:event_jToggleButtonDiffHLTActionPerformed

    private void performDiffButtonAction() {
        //logger.finest("jToggleButtonDiffHLTActionPerformed");
        //        long t0 = System.currentTimeMillis();
        if (jToggleButtonDiffHLT.isSelected() == true) {
            filterHold = filterText;
            seedFilterTextField.setEnabled(false);
            jComboHLTGroups.setEnabled(false);
            jComboHLTStreams.setEnabled(false);
            tableDetailsHLT.removeAll();
            tableDetailsHLT.setModel(interactor.hltDiffModel);
/*            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment( JLabel.CENTER );
            for(int column = 0; column < HLTPrescaleDiffTableModel.NumberOfColumns; column++){
                tableDetailsHLT.getColumnModel().getColumn(column).setCellRenderer( centerRenderer );
            }*/
            updateTableGraphics(true);
            tableDetailsHLT.updateUI();
            TableSorter sorterDiffHLT = new TableSorter(tableDetailsHLT.getModel());
            tableDetailsHLT.setModel(sorterDiffHLT);
            JTableHeader headerHLT = tableDetailsHLT.getTableHeader();
            sorterDiffHLT.setTableHeader(headerHLT);

        } else {
            seedFilterTextField.setEnabled(true);
            jComboHLTGroups.setEnabled(true);
            jComboHLTStreams.setEnabled(true);
            tableDetailsHLT.removeAll();
            tableDetailsHLT.setModel(interactor.hltMainModel);
            updateTableGraphics(false);
            tableDetailsHLT.updateUI();

        }
        boolean filter = false;
        if (!filterText.isEmpty()) {
            filter = true;
        }
        try {
            if (jToggleButtonDiffHLT.isSelected() == true) {
                updateTableGraphics(true);
            } else {
                updateTableGraphics(false);
            }
            interactor.updateDiffTable(new TableUpdateParameters(filterText, seedFilterText, GroupChain, StreamChain, filter), jToggleButtonDiffHLT.isSelected(),
                    (String) DiffHLT_PrescaleSet1.getSelectedItem(), (String) DiffHLT_PrescaleSet2.getSelectedItem());

            tableDetailsHLT.updateUI();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while comparing HLT tables in HLTPrescaleEdit panel.");
        }
    }

    private void updateTableGraphics(boolean toggle) {
        TableSorter sorterHLT = new TableSorter(tableDetailsHLT.getModel());
        tableDetailsHLT.setModel(sorterHLT);
        JTableHeader headerHLT = tableDetailsHLT.getTableHeader();
        sorterHLT.setTableHeader(headerHLT);

        // Use custom renderer. Align RIGHT.
        TriggerToolCellRenderer defRenderer = new TriggerToolCellRenderer();
        TriggerToolCellRenderer stringRend = new TriggerToolCellRenderer();
        stringRend.setHorizontalAlignment(SwingConstants.LEFT);

        //tableDetailsHLT.setDefaultRenderer(String.class, stringRenderer);
        tableDetailsHLT.setDefaultRenderer(Integer.class, defRenderer);
        TableColumnModel tcm = tableDetailsHLT.getColumnModel();
        if (!toggle) {
            tcm.getColumn(HLTPrescaleRow.NAME).setCellRenderer(stringRend);
            //Name column is bigger
            tcm.getColumn(HLTPrescaleRow.NAME).setPreferredWidth(250);
            tcm.getColumn(HLTPrescaleRow.SEED).setCellRenderer(stringRend);
            // SEED col is bigger.
            tcm.getColumn(HLTPrescaleRow.SEED).setPreferredWidth(250);
            tcm.getColumn(HLTPrescaleRow.IN_OUT).setCellRenderer(defRenderer);
            tcm.getColumn(HLTPrescaleRow.PS).setCellRenderer(defRenderer);
            tcm.getColumn(HLTPrescaleRow.PT).setCellRenderer(defRenderer);
            tcm.getColumn(HLTPrescaleRow.STREAM).setCellRenderer(defRenderer);
            tcm.getColumn(HLTPrescaleRow.CONDITION).setCellRenderer(defRenderer);
            tcm.getColumn(HLTPrescaleRow.RERUN).setCellRenderer(defRenderer);
        } else {
            tcm.getColumn(HLTPrescaleDiffTableModel.NAME).setCellRenderer(stringRend);
            //Name column is bigger
            tcm.getColumn(HLTPrescaleDiffTableModel.NAME).setPreferredWidth(250);
            tcm.getColumn(HLTPrescaleDiffTableModel.COUNTER).setCellRenderer(defRenderer);
            tcm.getColumn(HLTPrescaleDiffTableModel.COUNTER).setPreferredWidth(50);
            tcm.getColumn(HLTPrescaleDiffTableModel.PS_1).setCellRenderer(defRenderer);
            tcm.getColumn(HLTPrescaleDiffTableModel.PS_1).setPreferredWidth(50);
            tcm.getColumn(HLTPrescaleDiffTableModel.PS_2).setCellRenderer(defRenderer);
            tcm.getColumn(HLTPrescaleDiffTableModel.PS_2).setPreferredWidth(50);
            tcm.getColumn(HLTPrescaleDiffTableModel.PT_1).setCellRenderer(defRenderer);
            tcm.getColumn(HLTPrescaleDiffTableModel.PT_1).setPreferredWidth(50);
            tcm.getColumn(HLTPrescaleDiffTableModel.PT_2).setCellRenderer(defRenderer);
            tcm.getColumn(HLTPrescaleDiffTableModel.PT_2).setPreferredWidth(50);
            tcm.getColumn(HLTPrescaleDiffTableModel.STREAM_1).setCellRenderer(defRenderer);
            tcm.getColumn(HLTPrescaleDiffTableModel.STREAM_1).setPreferredWidth(50);
            tcm.getColumn(HLTPrescaleDiffTableModel.STREAM_2).setCellRenderer(defRenderer);
            tcm.getColumn(HLTPrescaleDiffTableModel.STREAM_2).setPreferredWidth(50);
            tcm.getColumn(HLTPrescaleDiffTableModel.CONDITION_1).setCellRenderer(defRenderer);
            tcm.getColumn(HLTPrescaleDiffTableModel.CONDITION_1).setPreferredWidth(50);
            tcm.getColumn(HLTPrescaleDiffTableModel.CONDITION_2).setCellRenderer(defRenderer);
            tcm.getColumn(HLTPrescaleDiffTableModel.CONDITION_2).setPreferredWidth(50);
            tcm.getColumn(HLTPrescaleDiffTableModel.RERUN_1).setCellRenderer(defRenderer);
            tcm.getColumn(HLTPrescaleDiffTableModel.RERUN_1).setPreferredWidth(50);
            tcm.getColumn(HLTPrescaleDiffTableModel.RERUN_2).setCellRenderer(defRenderer);
            tcm.getColumn(HLTPrescaleDiffTableModel.RERUN_2).setPreferredWidth(50);
    
        }
    }

    private void jButtonHLTHideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHLTHideActionPerformed
        jButtonHLTHideAction(evt);
}//GEN-LAST:event_jButtonHLTHideActionPerformed

    private void jToggleHLTHideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleHLTHideActionPerformed
        jToggleHLTHideAction(evt);
}//GEN-LAST:event_jToggleHLTHideActionPerformed

    /**
     * Load a prescale set from a XML file in to the current table.
     *
     * @param evt
     * @return 0 if OK, else 1.
     */
    private void jButtonLoadXMLMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonLoadXMLMouseClicked
        try {
            this.loadXMLMouseClickedHLT();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while loading XML in HLTPrescaleEdit panel.");
        }
        interactor.hltMainModel.fireTableDataChanged();
        tableDetailsHLT.updateUI();
    }//GEN-LAST:event_jButtonLoadXMLMouseClicked

    private void jButtonBrowseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonBrowseActionPerformed
        int returnVal = fc.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = fc.getSelectedFile();
            XMLpath.setText(f.toString());
        }
    }//GEN-LAST:event_jButtonBrowseActionPerformed

    private void DiffHLT_PrescaleSet1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DiffHLT_PrescaleSet1ActionPerformed
        if (jToggleButtonDiffHLT.isSelected() == true) {
            performDiffButtonAction();
        }
    }//GEN-LAST:event_DiffHLT_PrescaleSet1ActionPerformed

    private void DiffHLT_PrescaleSet2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DiffHLT_PrescaleSet2ActionPerformed
        if (jToggleButtonDiffHLT.isSelected() == true) {
            performDiffButtonAction();
        }
    }//GEN-LAST:event_DiffHLT_PrescaleSet2ActionPerformed

    private void jButtonHLTRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHLTRefreshActionPerformed
        refreshPanel();
    }//GEN-LAST:event_jButtonHLTRefreshActionPerformed

    private void jButtonLoadXMLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLoadXMLActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonLoadXMLActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<Object> DiffHLT_PrescaleSet1;
    private javax.swing.JComboBox<Object> DiffHLT_PrescaleSet2;
    private javax.swing.JButton ResetFilter;
    private javax.swing.JTextField SetNameHLT;
    private javax.swing.JTextField XMLpath;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JTextField filterTextField;
    private javax.swing.JButton jButtonBrowse;
    private javax.swing.JButton jButtonDisableHLT;
    private javax.swing.JButton jButtonEnableHLT;
    private javax.swing.JButton jButtonHLTComment;
    private javax.swing.JButton jButtonHLTHide;
    private javax.swing.JButton jButtonHLTRefresh;
    private javax.swing.JButton jButtonHLTSA;
    private javax.swing.JButton jButtonLoadXML;
    private javax.swing.JComboBox<String> jComboHLTGroups;
    private javax.swing.JComboBox<String> jComboHLTStreams;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelHLT;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanelHLTPrescales;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTextArea jTextAreaHLTComment;
    private javax.swing.JToggleButton jToggleButtonDiffHLT;
    private javax.swing.JToggleButton jToggleHLTHide;
    private javax.swing.JList listTablesHLTUnUsed;
    private javax.swing.JTextField seedFilterTextField;
    private javax.swing.JTable tableDetailsHLT;
    // End of variables declaration//GEN-END:variables

    //////////////
    /// ACTION CUSTOM CODE
    private void _SetNameHLTActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_SetNameHLTActionPerformed
        if (userMode.ordinal() >= UserMode.SHIFTER.ordinal()) {
            jButtonHLTSA.setEnabled(true);
        }
    }

    private void _jButtonHLTSAActionPerformed(java.awt.event.ActionEvent evt) throws SQLException {
        // Lock the DB.
        InternalWriteLock writeLock = InternalWriteLock.getInstance();
        writeLock.setDescription("HLT Prescale Save");
        try {
            writeLock.setLock();
        } catch (UploadException ex) {
            InternalWriteLockDialog writeLockDialog = new InternalWriteLockDialog(null);
            if (writeLockDialog.get_lock() != null) {
                logger.warning("Can not write - lock is still set");
                return;
            }
        }
        try {
            // Save PS
            this.saveHLTAs();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while saving the HLT prescales in HLTPrescaleEdit panel.");
        }
        // Remove Lock
        writeLock.removeLock();
    }

    /**
     * Saves the HLT prescale comment for the selected prescale set.
     *
     * @param evt
     */
    private void _jButtonHLTCommentActionPerformed(java.awt.event.ActionEvent evt) {
        // update the HLT comment here! direct to db.
        String comment = jTextAreaHLTComment.getText();
        try {
            interactor.updateHLTComment(comment);
            hltPrescaleEdit.HLTComment(comment, interactor.getHLTPrescaleSet().get_id());
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while updating the HLT comment in HLTPrescaleEdit panel.");
        }
    }

    /**
     * A button to enable all prescales for an entire HLT set/selected
     * prescales. When clicked sets all prescales positive and sets check boxes
     * to true Setting flag to 1 enables the alternative setValueAt in the
     * model. If the normal setValueAt is used whilst altering multiple rows
     * when sorting then rows move whilst changing them and the wrong ones get
     * changed. This way they stay still whilst changing and firetabledata comes
     * at the end.
     *
     * @param evt
     */
    private void _jButtonEnableHLTActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButtonEnableHLTActionPerformed
        int length = tableDetailsHLT.getSelectedRows().length;
        int[] Rows = tableDetailsHLT.getSelectedRows();
        TableModel model = tableDetailsHLT.getModel();
        int length2 = model.getRowCount();
        interactor.hltMainModel.setFlag(1);
        if (length == 0) {
            for (int i = 0; i < length2; ++i) {
                model.setValueAt(true, i, HLTPrescaleRow.IN_OUT);
            }
        } else if (length > 0) {
            for (int i = Rows[0]; i < Rows[0] + length; ++i) {
                model.setValueAt(true, i, HLTPrescaleRow.IN_OUT);
            }
            tableDetailsHLT.clearSelection();
        }
        interactor.hltMainModel.setFlag(0);
        interactor.hltMainModel.fireTableDataChanged();
        tableDetailsHLT.updateUI();
        if (userMode.ordinal() >= UserMode.SHIFTER.ordinal()) {
            jButtonHLTSA.setEnabled(true);
        }
    }

    /**
     * A button to disable all prescales for an entire HLT set/selected
     * prescales. When clicked sets all prescales negative and sets check boxes
     * to false Setting flag to 1 enables the alternative setValueAt in the
     * model. If the normal setValueAt is used whilst altering multiple rows
     * when sorting then rows move whilst changing them and the wrong ones get
     * changed. This way they stay still whilst changing and firetabledata comes
     * at the end.
     *
     * @param evt
     */
    private void _jButtonDisableHLTActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButtonDisableHLTActionPerformed
        int length = tableDetailsHLT.getSelectedRows().length;
        int[] Rows = tableDetailsHLT.getSelectedRows();
        TableModel model = tableDetailsHLT.getModel();
        int length2 = model.getRowCount();
        interactor.hltMainModel.setFlag(1);
        if (length == 0) {
            for (int i = 0; i < length2; ++i) {
                model.setValueAt(false, i, HLTPrescaleRow.IN_OUT);
            }
        } else if (length > 0) {
            for (int i = Rows[0]; i < Rows[0] + length; ++i) {
                model.setValueAt(false, i, HLTPrescaleRow.IN_OUT);
            }
            tableDetailsHLT.clearSelection();
        }
        interactor.hltMainModel.setFlag(0);
        interactor.hltMainModel.fireTableDataChanged();
        tableDetailsHLT.updateUI();

        if (userMode.ordinal() >= UserMode.SHIFTER.ordinal()) {
            jButtonHLTSA.setEnabled(true);
        }
    }

    /**
     * A button to enable all prescales for the selected HLT group. When clicked
     * sets all prescales positive and sets check boxes to true Setting flag to
     * 1 enables the alternative setValueAt in the model. If the normal
     * setValueAt is used whilst altering multiple rows when sorting then rows
     * move whilst changing them and the wrong ones get changed. This way they
     * stay still whilst changing and firetabledata comes at the end.
     *
     * @param evt
     */
    private void _tableDetailsHLTFocusGained(java.awt.event.FocusEvent evt) {// GEN-FIRST:event_tableDetailsHLTFocusGained
        if (evt.getSource() == tableDetailsHLT) {
            if (userMode.ordinal() >= UserMode.SHIFTER.ordinal()) {
                jButtonHLTSA.setEnabled(true);
            }
        }
    }

    private void _FilterTextKeyReleased(final java.awt.event.KeyEvent evt) {
        //logger.finest("___itemfilterKeyReleased\n");
        filterText = filterTextField.getText().toLowerCase();
        if (filterText.endsWith("\\")) {
            StringBuilder b = new StringBuilder(filterText);
            b.replace(filterText.lastIndexOf("\\"), filterText.lastIndexOf("\\") + 1, "");
            filterText = b.toString();
        }
        if (!testfilter(true)) {
            filterText = regexFilterHold;
        }
        try {
            if (tableDetailsHLT.getModel().getColumnName(HLTPrescaleRow.COUNTER).equals("Counter")) {
                interactor.updateHLTPrescalesTable(new TableUpdateParameters(filterText, seedFilterText, GroupChain, StreamChain, true));
            }

            if (tableDetailsHLT.getModel().getColumnName(HLTPrescaleDiffTableModel.COUNTER).equals("Counter")) {
                tableDetailsHLT.removeAll();
                boolean filter = false;
                if (!filterText.isEmpty()) {
                    filter = true;
                }
                interactor.updateDiffTable(new TableUpdateParameters(filterText, seedFilterText, GroupChain, StreamChain, filter),
                        jToggleButtonDiffHLT.isSelected(), (String) DiffHLT_PrescaleSet1.getSelectedItem(), (String) DiffHLT_PrescaleSet2.getSelectedItem());
            }
            tableDetailsHLT.updateUI();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while filtering in HLTPrescaleEdit panel.");
        }
    }

    /**
     * Obtain the ID (PrimaryKey in DB) of chains belongin to a certain group.
     * Fire updateHLTPrescaleTable(true) afterwards.
     *
     * @param evt
     */
    private void _jComboHLTGroupsActionPerformed(java.awt.event.ActionEvent evt) {
        //logger.finest("_jComboHLTGroupsActionPerformed\n");
        GroupChain.clear();

        if (!jComboHLTGroups.getSelectedItem().equals("All Groups")) {
            String SelectedGroup = jComboHLTGroups.getSelectedItem().toString();
            ArrayList<Integer> ChainIds = new ArrayList<>();
            for (ArrayList<Object> group : groups) {
                if (group.get(0).equals(SelectedGroup)) {
                    ChainIds.add(Integer.parseInt(group.get(1).toString()));
                }
            }
            for (int ID : ChainIds) {
                GroupChain.add(chainsById.get(ID).get_name());
            }
        }
        try {
            interactor.updateHLTPrescalesTable(new TableUpdateParameters(filterText, seedFilterText, GroupChain, StreamChain, true));
            tableDetailsHLT.updateUI();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while performing HLTGroup combobox action in HLTPrescaleEdit panel.");
        }
    }

    /**
     * Select the chains matching the name in the jComboBox and filter anything
     * else out.
     *
     * @param evt
     */
    private void _jComboHLTStreamsActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            //logger.finest("_jComboHLTGroupsActionPerformed\n");
            StreamChain.clear();
            if (!jComboHLTStreams.getSelectedItem().equals("All Streams")) {
                String SelectedStream = jComboHLTStreams.getSelectedItem().toString();
                for (HLTTriggerChain chain : chainsById.values()) {
                    for (HLTTriggerStream stream : chain.getStreams()) {
                        if (stream.get_name().equals(SelectedStream)) {
                            StreamChain.add(chain.get_name());
                        }
                    }
                }
            }

            interactor.updateHLTPrescalesTable(new TableUpdateParameters(filterText, seedFilterText, GroupChain, StreamChain, true));
            tableDetailsHLT.updateUI();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while performing HLTStreams combobox action in HLTPrescaleEdit panel.");
        }
    }

    /**
     * Action of the HIDE/SHOW prescale sets button.
     *
     * @param evt the actionEvent.
     */
    private void jButtonHLTHideAction(final java.awt.event.ActionEvent evt) {
        try {
            Object[] rows = this.listTablesHLTUnUsed.getSelectedValues();
            // By def hide, if button not "HIDE" then show.
            boolean hide = true;
            if (!jButtonHLTHide.getText().equals("Hide")) {
                hide = false;
            }
            for (Object row : rows) {
                HLTPrescaleSet psset = new HLTPrescaleSet(Integer.parseInt(((String) row).split(":")[0]));
                if (hide) {
                    hltPrescaleEdit.hidePrescaleSet(psset);
                } else {
                    hltPrescaleEdit.showPrescaleSet(psset);
                }
            }
            // We hide ps set i, jump to first item in on the list.
            int selIdx = 0;
            // We tell ps set i should "show", keep this as sel item.
            if (!hide) {
                selIdx = listTablesHLTUnUsed.getSelectedIndex();
            }
            if (jToggleButtonDiffHLT.isSelected() == true) {
                jToggleButtonDiffHLT.doClick();
            }

            interactor.CleanHLTPrescaleRowMap();
            PopulateHLTPrescaleSetLists();
            listTablesHLTUnUsed.setSelectedIndex(selIdx);
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while performing Hide action in HLTPrescaleEdit panel.");
        }

    }

    /**
     * Display or not the hidden prescale sets.
     *
     * @param e the ActionEvent.
     */
    private void jToggleHLTHideAction(final java.awt.event.ActionEvent e) {

        if (jToggleHLTHide.isSelected()) {
            showHidden = true;
            jButtonHLTHide.setText("Show");

        } else {
            showHidden = false;
            jButtonHLTHide.setText("Hide");
        }

        refreshPanel();
    }

    private void refreshPanel() {
        Object sel = listTablesHLTUnUsed.getSelectedValue();
        Boolean diffInProgress = false;
        try {

            Object store1 = null;
            Object store2 = null;
            if (jToggleButtonDiffHLT.isSelected() == true) {
                store1 = DiffHLT_PrescaleSet1.getSelectedItem();
                store2 = DiffHLT_PrescaleSet2.getSelectedItem();
                jToggleButtonDiffHLT.setSelected(false);
                diffInProgress = true;
            }
            interactor.CleanHLTPrescaleRowMap();
            PopulateHLTPrescaleSetLists();
            if (diffInProgress) {
                int var1 = ((DefaultComboBoxModel) (DiffHLT_PrescaleSet1.getModel())).getIndexOf(store1);
                int var2 = ((DefaultComboBoxModel) (DiffHLT_PrescaleSet2.getModel())).getIndexOf(store2);
                if (var1 != -1 && var2 != -1) {
                    DiffHLT_PrescaleSet1.setSelectedItem(store1);
                    DiffHLT_PrescaleSet2.setSelectedItem(store2);
                    jToggleButtonDiffHLT.setSelected(true);
                }

            }
            tableDetailsHLT.updateUI();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while performing Toggle action in HLTPrescaleEdit panel.");
        }

        if (!diffInProgress) {
            // select an object on the JList. this fires the refresh of table.
            if (HLTitemsUnUsed.contains(sel)) {
                listTablesHLTUnUsed.setSelectedValue(sel, true);
            } else {
                // The selected item is not present anymore -> hidden.
                // thus select first item.
                listTablesHLTUnUsed.setSelectedIndex(0);
            }
        } else {
            performDiffButtonAction();
        }
    }

    // TODO this to backend class.
    // TODO modularize & simplify this method.
    /**
     * Save the prescale set data on the table.
     *
     * @throws java.sql.SQLException
     */
    public final void saveHLTAs() throws SQLException {
        HLTPrescaleSet newHLTPrescaleSet = interactor.saveHLTAs(SetNameHLT.getText(), jToggleHLTHide.isSelected());
        if(newHLTPrescaleSet.get_id() == -1){
            return;
        }
        boolean alreadyexists = interactor.IsPrescaleSetDuplicate(newHLTPrescaleSet);
        int selIdx = 0;

        if (alreadyexists) {
            ListModel model = listTablesHLTUnUsed.getModel();
            for (int i = 0; i < model.getSize(); i++) {
                String setInfo = (String) model.getElementAt(i);
                Integer set = Integer.parseInt(setInfo.split(":")[0]);
                if (set.equals(newHLTPrescaleSet.get_id())) {
                    listTablesHLTUnUsed.setSelectedValue(setInfo, true);
                    selIdx = listTablesHLTUnUsed.getSelectedIndex();
                    break;
                }
            }

            JOptionPane.showMessageDialog(null,
                    "Prescale set being saved duplicates set " + newHLTPrescaleSet.get_id() + ": " + newHLTPrescaleSet.get_name(),
                    "Equivalent set already in database",
                    JOptionPane.INFORMATION_MESSAGE);
        } else {
            //SaveCommentOfNewHLTPrescaleSet(newHLTPrescaleSet);
            hltMenu = new HLTTriggerMenu(hltMenuId);
            interactor.setMenu(hltMenu);
        }

        if (jToggleHLTHide.isSelected()) {
            jToggleHLTHide.doClick();
        } else {
            interactor.CleanHLTPrescaleRowMap();
            PopulateHLTPrescaleSetLists();
        }

        listTablesHLTUnUsed.setSelectedIndex(selIdx);
        interactor.setHLTPrescaleSet((String) listTablesHLTUnUsed.getSelectedValue());

    }

    /*private void SaveCommentOfNewHLTPrescaleSet(HLTPrescaleSet newHLTPrescaleSet) throws SQLException {
        if (ConnectionManager.getInstance().getInitInfo().getOnline()) {
            String arg = "" + newHLTPrescaleSet.get_id();
            hltCommentBox = new PrescaleCommentBox("HLT", arg);
            String comment = hltCommentBox.getNewval();
            hltCommentBox.clearVal();
            interactor.updateHLTComment(comment);
            hltPrescaleEdit.HLTComment(comment, newHLTPrescaleSet.get_id());
        }
    }*/
    /**
     * Queries the DB to get all HLTPrescaleSets belonging to this HLT Trigger
     * Menu. Along with the PrescaleSet also the Boolean value, representing
     * whether this set is hidden or not in this menu.
     */
    private void PopulateHLTPrescaleSetLists() throws SQLException {
        ArrayList<String> sets = hltPrescaleEdit.getHLTSetInfo(showHidden);
        // Clean all values form lists.
        HLTitemsUnUsed.removeAllElements();
        DiffHLT_PrescaleSet1.removeAllItems();
        DiffHLT_PrescaleSet2.removeAllItems();
        for (String psSetC : sets) {
            // Add all elements to Diff
            DiffHLT_PrescaleSet1.addItem(psSetC);
            DiffHLT_PrescaleSet2.addItem(psSetC);
            HLTitemsUnUsed.addElement(psSetC);
        }

        interactor.sets = sets;
    }

    /**
     * Queries the DB to obtain group and stream names. These are used in the
     * jComboBoxes to filter prescale items.
     */
    private void loadGroups() throws SQLException {
        groups = hltPrescaleEdit.loadGroups();

        // TreeSet keeps the Set of names ordered.
        // Using custom case insensitive comparator
        TreeSet<String> groupNames
                = new TreeSet<>(new CaseInsensitiveComp());
        for (ArrayList<Object> group : groups) {
            groupNames.add(group.get(0).toString());
        }
        for (String groupName : groupNames) {
            jComboHLTGroups.addItem(groupName);
        }
    }

    private void loadStreams() throws SQLException {
        // TreeSet keeps the Set of names ordered.
        // Using custom case insensitive comparator
        TreeSet<String> streamNames = new TreeSet<>(new CaseInsensitiveComp());
        //streams = new ArrayList<>();
        //List<HLTTriggerChain> allChains = new ArrayList<>();
        //allChains.addAll(chains);

        // Initial load, streams not yet loaded from DB, hence try to optimize
        // the loading of the whole lot.
        // Broken, fix later.
//        String streamQuery = HLTTriggerChain.QUERY_STREAMS;
//        ConnectionManager cmgr = ConnectionManager.getInstance();
//        streamQuery = cmgr.fix_schema_name(streamQuery);
//        try {
//            Connection con = cmgr.getConnection();
//            PreparedStatement ps = con.prepareStatement(streamQuery);
        for (HLTTriggerChain chain : chains) {
            ArrayList<HLTTriggerStream> tmpStreams = chain.getStreams();
            for (HLTTriggerStream stream : tmpStreams) {
                //streams.add(stream);
                streamNames.add(stream.get_name());
            }
        }
//        } catch (SQLException ex) {
//            logger.severe(ex.toString());
//        }

        for (String stream : streamNames) {
            jComboHLTStreams.addItem(stream);
        }

    }

    /**
     * Resets the panel. Removes all filters, selects item 0 and fires table
     * refresh.
     */
    private void resetHLT() throws SQLException {
        //logger.finest("resetHLT\n");
        filterTextField.setText("");
        seedFilterTextField.setText("");
        filterText = "";
        seedFilterText = "";
        jComboHLTGroups.setSelectedIndex(0);
        jComboHLTStreams.setSelectedIndex(0);
        interactor.updateHLTPrescalesTable(new TableUpdateParameters(filterText, seedFilterText, GroupChain, StreamChain, true));
        if (jToggleButtonDiffHLT.isSelected() == true) {
            interactor.updateDiffTable(new TableUpdateParameters(filterText, seedFilterText, GroupChain, StreamChain, true), jToggleButtonDiffHLT.isSelected(),
                    (String) DiffHLT_PrescaleSet1.getSelectedItem(), (String) DiffHLT_PrescaleSet2.getSelectedItem());
        }
        tableDetailsHLT.updateUI();
    }

    /**
     * Check conditions and Upload HLT Prescales.
     *
     * @return 0 if OK, 1 otherwise.
     */
    private int loadXMLMouseClickedHLT() throws SQLException {
        String psSetName = interactor.LoadHLTPrescalesFromXML(XMLpath.getText());
        if (jToggleButtonDiffHLT.isSelected() == true) {
            jToggleButtonDiffHLT.setSelected(false);
            performDiffButtonAction();
        }

        this.SetNameHLT.setText(psSetName);
        String psInfo = "~: " + psSetName + " *edit*";
        // remove old uploadedSet from the jCombos if present.
        if (interactor.uploadedHLTPrescaleSet != null) {
            this.DiffHLT_PrescaleSet1.removeItem(psInfo);
            this.DiffHLT_PrescaleSet2.removeItem(psInfo);
        }
        // Create a new prescale set map for diffing.
        // Trick to overcome toStrin -1 condition.
        this.DiffHLT_PrescaleSet1.addItem(psInfo);
        this.DiffHLT_PrescaleSet2.addItem(psInfo);
        this.DiffHLT_PrescaleSet1.setSelectedItem(psInfo);
        this.jButtonHLTSA.setEnabled(true);
        return 0;
    }

    /**
     * When the user clicks on a prescale set in the list display its details,
     * i.e. the prescales.
     *
     * @param e an event.
     */
    public final void valueChanged(final ListSelectionEvent e) {
        try {
            DoValueChanged(e);
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while value changed in HLTPrescaleEdit panel.");
        }
    }

    private void DoValueChanged(final ListSelectionEvent e) throws SQLException {
        // ///////////////////////////////
        // clicked on HLT prescale set list
        if (e.getSource() != listTablesHLTUnUsed || e.getValueIsAdjusting()) {
            return;
        }
        if (listTablesHLTUnUsed.getSelectedValue() == null) {
            return;
        }

        Boolean activeFilters = false;
        String savedNameFilter = "";
        String savedSeedFilter = "";
        String savedNameFilterText = "";
        String savedSeedFilterText = "";
        Integer savedGroupFilter = 0;
        Integer savedStreamFilter = 0;

        if (!filterText.isEmpty()) {
            savedNameFilter = filterText;
            savedNameFilterText = filterTextField.getText();
            activeFilters = true;
        }

        if (!seedFilterText.isEmpty()) {
            savedSeedFilter = seedFilterText;
            savedSeedFilterText = seedFilterTextField.getText();
            activeFilters = true;
        }

        if (!jComboHLTGroups.getSelectedItem().equals("All Groups")) {
            savedGroupFilter = jComboHLTGroups.getSelectedIndex();
            activeFilters = true;
        }

        if (!jComboHLTStreams.getSelectedItem().equals("All Streams")) {
            savedStreamFilter = jComboHLTStreams.getSelectedIndex();
            activeFilters = true;
        }

        // stop filtering
        resetHLT();
        // stop diffing if we are doing so
        if (jToggleButtonDiffHLT.isSelected()) {
            jToggleButtonDiffHLT.doClick();
        }
        // Remove temp prescale set created while xml upload from the JCombos
        // for diff.
        if (interactor.uploadedHLTPrescaleSet != null) {
            this.DiffHLT_PrescaleSet1.removeItem(interactor.uploadedHLTPrescaleSet);
            this.DiffHLT_PrescaleSet2.removeItem(interactor.uploadedHLTPrescaleSet);
            this.DiffHLT_PrescaleSet1.setSelectedItem(interactor.getHLTPrescaleSet());
            this.DiffHLT_PrescaleSet2.setSelectedItem(interactor.getHLTPrescaleSet());
        }
        if (interactor.lhm.size() > 0) {
            // If we have already looked at one set, there will be some entries in
            // the map. Compare what is there now with the original - prompt to
            // save if different
            boolean hltprescalesedited = interactor.checkIfPrescaleSetEdited();

            if (hltprescalesedited) {
                logger.info("HLT prescales have been edited");
                String[] choices = {"Save", "Discard"};
                Integer response = JOptionPane.showOptionDialog(null,
                        "The HLT Prescale set " + interactor.getHLTPrescaleSet()
                        + " has been edited.\n Do you wish to save the changes"
                        + " as a new set or discard?",
                        "",
                        0,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        choices,
                        choices[0]);
                String answer = choices[response];

                if (answer.equals("Discard")) {
                    logger.info("Discarding changes");
                } else {
                    logger.info("Saving changes");
                    this.saveHLTAs();
                }
            }
        }

        interactor.setHLTPrescaleSet((String) listTablesHLTUnUsed.getSelectedValue());
        if (interactor.getHLTPrescaleSet() == null) {
            return;
        }
        //logger.finest("Selected HLT prescale set: " + theHLTPrescaleSet);
        // update the comment label
        jLabelHLT.setText("Comment Field for Set ID "
                + interactor.getHLTPrescaleSet().get_id());
        // show the comment
        jTextAreaHLTComment.setText(interactor.getHLTPrescaleSet().get_comment());
        // set name
        SetNameHLT.setText(interactor.getHLTPrescaleSet().get_name());
        long t7 = System.currentTimeMillis();
        // update the hlt prescales table
        interactor.updateHLTPrescalesTable(new TableUpdateParameters(filterText, seedFilterText, GroupChain, StreamChain, false));
        tableDetailsHLT.updateUI();

        if (activeFilters) {
            filterText = savedNameFilter;
            filterTextField.setText(savedNameFilterText);
            seedFilterText = savedSeedFilter;
            seedFilterTextField.setText(savedSeedFilterText);
            jComboHLTGroups.setSelectedIndex(savedGroupFilter);
            jComboHLTStreams.setSelectedIndex(savedStreamFilter);
            interactor.updateHLTPrescalesTable(new TableUpdateParameters(filterText, seedFilterText, GroupChain, StreamChain, true));
        }
        long t8 = System.currentTimeMillis() - t7;
    }

    /**
     * Case insensitive String comparator.
     */
    private class CaseInsensitiveComp
            implements Comparator<String> {

        /**
         * {@inheritDoc}
         */
        @Override
        public final int compare(final String s1, final String s2) {
            String str1 = s1.toUpperCase();
            String str2 = s2.toUpperCase();
            return str1.compareTo(str2);
        }
    }

    private boolean testfilter(boolean filter) {
        String pattern = "";
        if (filter) {
            pattern = pattern + filterText;
        } else {
            pattern = pattern + seedFilterText;
        }
        try {
            Pattern reg = Pattern.compile(pattern);
            Matcher Matcher = reg.matcher("Lorem ipsum");
            boolean Match = Matcher.find();

            if (Match) {
                return true;
            }
        } catch (PatternSyntaxException ex) {
            logger.log(Level.INFO, "Bad regex: {0}", ex);
            return false;
        }
        return true;
    }

    private class PrescaleSetListListener implements ListSelectionListener {

        /**
         * When the user clicks on a prescale set in the list display its
         * details, i.e. the prescales.
         *
         * @param e an event.
         */
        @Override
        public final void valueChanged(final ListSelectionEvent e) {
            try {
                DoValueChanged(e);
            } catch (SQLException ex) {
                TTExceptionHandler.HandleSqlException(ex, triggertool.Panels.MainPanel.getMainPanel(), "Error while value changed in HLTPrescaleEdit panel.");
            }
        }
    }

}
