/*
 * PrescaleEditMasterPanel.java
 *
 * Created on March 4 , 2008, 2:15 PM
 */
package triggertool.Panels.PrescaleEditPanel;
import java.net.URL;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggertool.Panels.MainPanel;

/**
 * @author William Panduro Vazquez
 */
public final class PrescaleEditMasterPanel extends JDialog {

    /**
     * Required by Java
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");

    private SuperMasterTable super_master = null;
    private MainPanel mp = null;

    private boolean isEditable = true;

    /**
     * complete list of HLT data (vector of vectors - all rows and cols).
     */

  
    /**
     * Creates new form PrescaleEditMasterPanel
     * @param smt
     * @param pmp
     * @param numwindows
     * @throws java.sql.SQLException
     */
    public PrescaleEditMasterPanel(final SuperMasterTable smt, final MainPanel pmp,
            final int numwindows) throws SQLException {

        super((JDialog)null);

        super_master = smt;
        mp = pmp;
        
        URL iconUrl = PrescaleEditMasterPanel.class.getResource("/triggertool/image/TTIcon2.gif");
        if(iconUrl != null) {
            ImageIcon icon = new ImageIcon(iconUrl);
            setIconImage( icon.getImage() );
        }

        
        long tPEdit = System.currentTimeMillis();
         
        if ((numwindows) > 0) {
            isEditable = false;
           
        } else {
            isEditable = true;           
        }

        initComponents();
        
        if(isEditable){
            ViewModeWarningLabel.setText("");
        }
        else{
            ViewModeWarningLabel.setText("VIEWER ONLY (SCREEN " + (numwindows + 1) + ")");
        }
        
        setTitle("Prescale Editor: SMK " + smt + " Editable = " + isEditable);
       
        this.setLocationRelativeTo(pmp);
        mp.incrementpswindows(0);
        tPEdit = System.currentTimeMillis() - tPEdit;
        logger.log(Level.FINER, " Time to run PrescaleEdit() {0}", tPEdit / 1000f);
    }

     private void formWindowClosed(java.awt.event.WindowEvent evt) {
        mp.decrementpswindows(0);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonOK = new javax.swing.JButton();
        TabbedPrescalePane = new javax.swing.JTabbedPane();
        L1PrescalePanel = new javax.swing.JPanel();
        l1PrescaleEditGUI1 = new triggertool.Panels.PrescaleEditPanel.L1PrescaleEditGUI(super_master,isEditable);
        HLTPrescalePanel = new javax.swing.JPanel();
        hLTPrescaleEditGUI = new triggertool.Panels.PrescaleEditPanel.HLTPrescaleEditGUI(super_master, isEditable);
        ViewModeWarningLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1330, 790));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        buttonOK.setText("Close");
        buttonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonOKActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout L1PrescalePanelLayout = new org.jdesktop.layout.GroupLayout(L1PrescalePanel);
        L1PrescalePanel.setLayout(L1PrescalePanelLayout);
        L1PrescalePanelLayout.setHorizontalGroup(
            L1PrescalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 1257, Short.MAX_VALUE)
            .add(L1PrescalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(org.jdesktop.layout.GroupLayout.TRAILING, L1PrescalePanelLayout.createSequentialGroup()
                    .add(l1PrescaleEditGUI1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 1251, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        L1PrescalePanelLayout.setVerticalGroup(
            L1PrescalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 653, Short.MAX_VALUE)
            .add(L1PrescalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(l1PrescaleEditGUI1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        TabbedPrescalePane.addTab("L1 Prescales", L1PrescalePanel);

        org.jdesktop.layout.GroupLayout HLTPrescalePanelLayout = new org.jdesktop.layout.GroupLayout(HLTPrescalePanel);
        HLTPrescalePanel.setLayout(HLTPrescalePanelLayout);
        HLTPrescalePanelLayout.setHorizontalGroup(
            HLTPrescalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(hLTPrescaleEditGUI, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 1257, Short.MAX_VALUE)
        );
        HLTPrescalePanelLayout.setVerticalGroup(
            HLTPrescalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(hLTPrescaleEditGUI, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 653, Short.MAX_VALUE)
        );

        TabbedPrescalePane.addTab("HLT Prescales", HLTPrescalePanel);

        ViewModeWarningLabel.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        ViewModeWarningLabel.setText("jLabel8");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(0, 1133, Short.MAX_VALUE)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, ViewModeWarningLabel)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                                .add(buttonOK, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 145, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(TabbedPrescalePane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(ViewModeWarningLabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(TabbedPrescalePane)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(buttonOK)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonOKActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_buttonOKActionPerformed
        dispose();// GEN-LAST:event_buttonOKActionPerformed
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel HLTPrescalePanel;
    private javax.swing.JPanel L1PrescalePanel;
    private javax.swing.JTabbedPane TabbedPrescalePane;
    private javax.swing.JLabel ViewModeWarningLabel;
    private javax.swing.JButton buttonOK;
    private triggertool.Panels.PrescaleEditPanel.HLTPrescaleEditGUI hLTPrescaleEditGUI;
    private triggertool.Panels.PrescaleEditPanel.L1PrescaleEditGUI l1PrescaleEditGUI1;
    // End of variables declaration//GEN-END:variables
    
}
