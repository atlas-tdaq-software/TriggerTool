/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggertool.Panels.PrescaleEditPanel;

import java.awt.Color;
import java.io.File;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.jdesktop.swingx.JXTreeTable;
import external.ColorUtil;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.renderer.IconValue;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.UserMode;
import triggerdb.Entities.InternalWriteLock;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.L1.L1Menu;
import triggerdb.Entities.L1.L1Prescale;
import triggerdb.Entities.L1Links.L1TM_PS;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.UploadException;
import triggertool.Auxiliary.TriggerToolCellRenderer;
import triggertool.Components.InternalWriteLockDialog;
//import triggertool.Components.PrescaleCommentBox;
import triggertool.Components.TableSorter;
import triggertool.L1Records.CutToValueConverter;
import triggertool.TTExceptionHandler;
import triggertool.XML.XMLprescales;

/**
 *
 * @author wvazquez
 */
public class L1PrescaleEditGUI extends javax.swing.JPanel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    /**
     * The selected prescale set.
     */
    private L1Prescale theL1PrescaleSet;
    private L1Prescale PrescaleSet1;
    private L1Prescale PrescaleSet2;
    private Integer PSSPartition = 0;
    /**
     * The L1 menu.
     */
    private L1Menu theL1Menu = null;
    private UserMode userMode = UserMode.UNKNOWN;
    private HashSet<String> types = null;
    private HashSet<String> type = new HashSet<>();
    private final DefaultListModel<String> L1itemsUnUsed = new DefaultListModel<>();
    private String L1Filter = new String();
    private final List<String> L1Filters = new ArrayList<>();
    private boolean show_hidden_l1 = false;
    private SuperMasterTable super_master = null;
    private L1PrescaleTableModel L1MainModel;
    private final L1PrescaleDiffTableModel L1DiffModel = new L1PrescaleDiffTableModel();
    //private PrescaleCommentBox L1CommentBox;
    private boolean isEditable = true;
    private final List<L1Prescale> listOfSelected = new ArrayList<>();
    private boolean saving = false;

    /**
     * bool if filtering with regex fails!
     */
    private boolean regexFail = false;
    /**
     * complete list of HLT data (vector of vectors - all rows and cols).
     */
    LinkedHashMap<Integer, L1PrescaleEditItemNode> l1m;
    L1PrescaleEditItemNode ctpItems;
    /**
     * New Prescale objects from table.
     */
    private L1Prescale uploadedL1PrescaleSet;
    /**
     * An string holding the previous filter text.
     */
    private String filterHold = "";
    /**
     * An string holding the previous regex filter text.
     */
    private String regexFilterHold = "";
    /*
     Defines whether the panel will be locked to a particular CTP partition or not
     */
    Boolean isPartitionLocked = false;
    Integer lockPartition = 0;
    /**
     * File chooser
     */
    private final JFileChooser fc = new JFileChooser();

    /**
     * Creates new form L1PSEditGUI
     */
    public L1PrescaleEditGUI() {
        initPanel();
    }

    /**
     *
     * @param smt
     * @param partition
     */
    public L1PrescaleEditGUI(final SuperMasterTable smt, int partition) {

        super_master = smt;
        isPartitionLocked = true;
        lockPartition = partition;

        initPanel();

        initDB();
        lockToPartition();

    }

    /**
     *
     * @param smt
     * @param partition
     * @param Editable
     */
    public L1PrescaleEditGUI(final SuperMasterTable smt, int partition, Boolean Editable) {

        isEditable = Editable;
        super_master = smt;
        isPartitionLocked = true;
        lockPartition = partition;

        initPanel();

        initDB();
        lockToPartition();

    }

    /**
     * Creates new form L1PrescaleEditGUI
     *
     * @param smt
     * @param Editable
     */
    public L1PrescaleEditGUI(final SuperMasterTable smt, Boolean Editable) {

        isEditable = Editable;
        super_master = smt;

        initPanel();
        initDB();
    }

    private void initDB() {
        try {
            theL1Menu = super_master.get_l1_master_table().get_menu();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this.getParent(), "Error accessing L1 menu.");
        }

        try {
            this.getL1Sets(getPartitionFilterSelectorValue());
            this.getTypes();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this.getParent(), "Error getting Prescale Sets.");
        }

        if (this.L1itemsUnUsed.size() > 0) {
            this.PrescaleSetListContents.setSelectedIndex(0);
        }

        userMode = ConnectionManager.getInstance().getInitInfo().getUserMode();
        //shifter/user differences - user is effectively uneditable
        if (userMode.ordinal() < UserMode.SHIFTER.ordinal() || isEditable == false) {
            tableDetailsL1.setEnabled(false);
            SetName.setEditable(false);
            EnableSelectionButton.setEnabled(false);
            DisableSelectionButton.setEnabled(false);
            UpdateCommentButton.setEnabled(false);
            CommentAllButton.setEnabled(false);
            L1SaveButton.setEnabled(false);

            PartitionChangeSelector.setVisible(false);
            PrescaleSetChangePartitionLabel.setVisible(false);
            CommentTextField.setEditable(false);
        }

        if (userMode.ordinal() <= UserMode.SHIFTER.ordinal() || isEditable == false) {
            //cannot hide or show sets or perform xml uploads
            HideSetsToggleButton.setEnabled(false);
            XMLLoadButton.setEnabled(false);
            XMLpath.setEnabled(false);
            FileSystemXMLBrowseButton.setEnabled(false);
            //XMLLoadButton.setVisible(false);
            //XMLpath.setVisible(false);
            //FileSystemXMLBrowseButton.setVisible(false);
        }

        L1SaveButton.setEnabled(false);
        setVisible(true);

    }

    private void initPanel() {
        FileNameExtensionFilter filter = new FileNameExtensionFilter("XML files", "xml"); // in java 1.6
        fc.setFileFilter(filter);
        fc.setCurrentDirectory(new File("."));
        //check which mode

        initComponents();
        this.l1m = new LinkedHashMap<>();
        this.ctpItems = new L1PrescaleEditItemNode("LOCAL_ROOT");
        //set the text areas visible and editable
        CommentTextField.setVisible(true);

        this.L1MainModel = (L1PrescaleTableModel) tableDetailsL1.getModel();
        this.recreatePrescalesJTable();
        this.PrescaleSetListContents.addListSelectionListener(new L1PrescaleEditGUI.PrescaleSetListListener());
        this.tableDetailsL1.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    }

    private void lockToPartition() {
        MultiplePrescalesSelectBox.setVisible(false);
        PartitionFilterSelector.setModel(new javax.swing.DefaultComboBoxModel<String>(new String[]{"0", lockPartition.toString()}));
        PartitionChangeSelector.setVisible(false);
        PSSPartition = lockPartition;
        PrescaleSetChangePartitionLabel.setText("PANEL LOCKED TO PARTITION " + lockPartition.toString());
        PartitionFilterSelector.setSelectedIndex(1);
        PartitionFilterSelector.updateUI();
        L1SaveButton.setEnabled(false);
    }

    /**
     * Queries the DB and sets the lists - L1itemsUnUsed -
     * PrescaleSet1DiffSelector - PrescaleSet2DiffSelector
     */
    private void getL1Sets(Integer partition) throws SQLException {
        L1itemsUnUsed.removeAllElements();
        //get L1 prescales
        L1Menu l1menu = this.theL1Menu;
        /*List<L1Prescale> sets = new ArrayList<>();
        if (show_hidden_l1) {
            for (L1Prescale set : l1menu.getPrescaleSets()) {
                if (set.get_partition().equals(partition)) {
                    sets.add(set);
                }
            }
        } else {
            for (L1Prescale set : l1menu.getPrescalesSetsNonHidden()) {
                if (set.get_partition().equals(partition)) {
                    sets.add(set);
                }
            }
	    }*/
        List<String> sets = l1menu.getPrescaleSetInfoPartition(show_hidden_l1, new ArrayList<>(Arrays.asList(partition)));
        for (String l1ps : sets) {
            L1itemsUnUsed.addElement(l1ps);
        }
//        // Add all prescale sets to the diff boxes.
//        PrescaleSet1DiffSelector.removeAllItems();
//        PrescaleSet2DiffSelector.removeAllItems();
//        sets = l1menu.getPrescaleSets();
//        for (L1Prescale l1psd : sets) {
//            PrescaleSet1DiffSelector.addItem(l1psd);
//            PrescaleSet2DiffSelector.addItem(l1psd);
//        }
        this.updateDiffPrescaleSets();
    }

    /**
     * TODO documentation TODO db query to PrescaleEditBackEndL1 TODO db query
     * simplify "select L1TM2TI_TRIGGER_ITEM_ID from..."
     */
    private void getTypes() throws SQLException {
        types = new HashSet<>();
        String query = "SELECT L1TI_TRIGGER_TYPE, L1TM2TI_TRIGGER_ITEM_ID, L1TM2TI_TRIGGER_MENU_ID, L1TI_ID "
                + "FROM "
                + "L1_TRIGGER_MENU, L1_TM_TO_TI, L1_TRIGGER_ITEM "
                + "WHERE "
                + "L1TM_ID = ? "
                + "AND "
                + "L1TM2TI_TRIGGER_MENU_ID=L1TM_ID "
                + "AND "
                + "L1TM2TI_TRIGGER_ITEM_ID=L1TI_ID "
                + "ORDER BY L1TI_ID DESC";

        query = ConnectionManager.getInstance().fix_schema_name(query);

        PreparedStatement ps;
        ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, theL1Menu.get_id());
        ResultSet rset;
        rset = ps.executeQuery();
        while (rset.next()) {
            types.add(rset.getString("L1TI_TRIGGER_TYPE"));
        }
        rset.close();
        ps.close();

        //Hard coding the l1 types to display in human readable form. WARNING MAY CHANGE!!!
        // TODO This should go to a properties file or even better humnan
        // readable names should be included in a db table.
        for (String t : types) {
            try {
                int tint = Integer.parseInt(t);
                switch (tint) {
                    case 1:
                        TypeSelector.addItem("RNDM (1)");
                        break;
                    case 48:
                        TypeSelector.addItem("CalReq0 (48)");
                        break;
                    case 50:
                        TypeSelector.addItem("CalReq1 (50)");
                        break;
                    case 52:
                        TypeSelector.addItem("CalReq2 (52)");
                        break;                    
                    case 128:
                        TypeSelector.addItem("ID Cosmic (128)");
                        break;
                    case 129:
                        TypeSelector.addItem("RNDM (129)");
                        break;
                    case 130:
                        TypeSelector.addItem("ZeroBias (130)");
                        break;
                    case 132:
                        TypeSelector.addItem("L1Calo (132)");
                        break;
                    case 136:
                        TypeSelector.addItem("Muon (136)");
                        break;
                    case 144:
                        TypeSelector.addItem("LArDemo (144)");
                        break;
                    case 160:
                        TypeSelector.addItem("MinBias/FTK (160)");
                        break;
                    case 192:
                        TypeSelector.addItem("ALFA (192)");
                        break;
                    default:
                        TypeSelector.addItem("(" + tint + ")");
                        break;
                }
            } catch (NumberFormatException ex) {
                logger.log(Level.SEVERE, "Exception: {0}", ex);
            }
        }
    }

    /**
     * Get all items belonging to a triggerType
     *
     * TODO this go to PrescaleEditBackEndL1. TODO triggerTypeId needs cast to
     * int here.
     *
     * @param TriggerTypeId The trigger type id as a string.
     */
    private void getItem(String triggerTypeId) throws SQLException {
        type = new HashSet<>();
        String query = "SELECT L1TI_TRIGGER_TYPE, L1TI_NAME, L1TM_ID, L1TM2TI_TRIGGER_ITEM_ID, L1TM2TI_TRIGGER_MENU_ID, L1TI_ID "
                + "FROM "
                + "L1_TRIGGER_MENU, L1_TM_TO_TI, L1_TRIGGER_ITEM "
                + "WHERE "
                + "L1TM_ID = ? "
                + "AND "
                + "L1TM2TI_TRIGGER_MENU_ID=L1TM_ID "
                + "AND "
                + "L1TM2TI_TRIGGER_ITEM_ID=L1TI_ID "
                + "AND "
                + "L1TI_TRIGGER_TYPE = ? "
                + "ORDER BY L1TI_ID DESC";

        query = ConnectionManager.getInstance().fix_schema_name(query);

        PreparedStatement ps;
        ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, theL1Menu.get_id());
        ps.setString(2, triggerTypeId);
        ResultSet rset;
        rset = ps.executeQuery();
        while (rset.next()) {
            type.add(rset.getString("L1TI_NAME").trim());
        }
        rset.close();
        ps.close();
    }

    private void DoL1ActionPerformed() throws SQLException {
        // Implementation of locking mechanism
        InternalWriteLock writeLock = InternalWriteLock.getInstance();
        writeLock.setDescription("LVL1 Prescale Save");
        try {
            writeLock.setLock();
        } catch (UploadException ex) {
            InternalWriteLockDialog writeLockDialog = new InternalWriteLockDialog(null);
            if (writeLockDialog.get_lock() != null) {
                logger.fine("Can not write - lock is still set");
                return;
            }
        }
        if (!MultiplePrescalesSelectBox.isSelected()) {
            this.saveL1As();
        } else {
            // save prescales on table
            List<Integer> savedIdxs = null;
            try {
                savedIdxs = this.saveL1Multiple();
                L1SaveButton.setEnabled(false);
            } catch (java.lang.NullPointerException ex) {
                TTExceptionHandler.HandleNullPointerException(ex, this, "Error while saving performing multiple L1 prescale set save action in Prescale Edit panel.");
            }
            //clear table
            L1PrescaleTreeTableModel tableModel = (L1PrescaleTreeTableModel) ((JXTreeTable) tableDetailsL1).getTreeTableModel();
            tableModel.clearPrescalesButKeep(null);
            //tableModel.getchi
            // get indices of saved sets
            if (savedIdxs != null) {
                int[] selIdxs = new int[savedIdxs.size()];
                int j = 0;
                for (int i = 0; i < this.L1itemsUnUsed.getSize(); i++) {
                    if (savedIdxs.contains(getIDFromInfo((String) this.L1itemsUnUsed.get(i)))) {
                        selIdxs[j] = i;
                        j++;
                    }
                }
                // select saved sets.
                PrescaleSetListContents.setSelectedIndices(selIdxs);
            }
        }
        writeLock.removeLock();
    }

    /**
     * Updates the comment to the prescale set with given id.
     *
     * @param _comment the new comment.
     * @param Id the Id of the prescale set to upload or -1 for the current one.
     */
    private void L1Comment(final String _comment, final Integer Id) throws SQLException {
        if (_comment != null) {
            if (Id == -1) {
                theL1PrescaleSet.update_comment(_comment);
            } else {
                L1Prescale l1ps = new L1Prescale(Id);
                l1ps.update_comment(_comment);
            }

            this.updateDiffPrescaleSets();
        }
    }

    private void updateDiffTable(boolean applyFilter) throws SQLException {
        if (PrescaleSetDoDiffButton.isSelected() == true) {
            boolean setSelected = false;
            if (getNameFromInfo((String) PrescaleSet1DiffSelector.getSelectedItem()).equals("Prescale Set 1") || getNameFromInfo((String) PrescaleSet2DiffSelector.getSelectedItem()).equals("Prescale Set 2")) {
                setSelected = true;
            } else if (PrescaleSet1DiffSelector.getSelectedItem().toString().equals(PrescaleSet2DiffSelector.getSelectedItem().toString())) {
                setSelected = true;
            }
            if (setSelected) {
                logger.info("No sets selected");
                PrescaleSetDoDiffButton.setSelected(false);
                String[] choices = {"Ok"};
                JOptionPane.showOptionDialog(null,
                        "Please select two different prescale sets to diff.",
                        "",
                        0,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        choices,
                        choices[0]);
                PrescaleSetDoDiffButton.setSelected(false);
                updateDiffTable(false);
            } else {
                updateDiff(applyFilter);
            }
        } else {
            if (tableDetailsL1 instanceof JXTreeTable) {
                this.updateJXtable((JXTreeTable) tableDetailsL1, listOfSelected, false);
                return;
            }
            L1DiffModel.clearNumRows();
            tableDetailsL1.removeAll();
            tableDetailsL1.setModel(L1MainModel);
            TableSorter sorterL1 = new TableSorter(tableDetailsL1.getModel());
            tableDetailsL1.setModel(sorterL1);
            JTableHeader headerL1 = tableDetailsL1.getTableHeader();
            sorterL1.setTableHeader(headerL1);
            if (filterHold.equals(L1Filter)) {
                updateL1prescalestable(false);
            } else {
                updateL1prescalestable(true);
            }
            L1MainModel.fireTableDataChanged();
        }
    }

    private void updateDiff(boolean applyFilter) throws SQLException {
        if (tableDetailsL1 instanceof JXTreeTable) {
            if (!applyFilter) {
                L1Prescale diff1;
                L1Prescale diff2;
                if (isNullIDinInfo((String) PrescaleSet1DiffSelector.getSelectedItem())) {
                    diff1 = uploadedL1PrescaleSet;
                } else {
                    diff1 = new L1Prescale(getIDFromInfo((String) PrescaleSet1DiffSelector.getSelectedItem()));
                }
                if (isNullIDinInfo((String) PrescaleSet2DiffSelector.getSelectedItem())) {
                    diff2 = uploadedL1PrescaleSet;
                } else {
                    diff2 = new L1Prescale(getIDFromInfo((String) PrescaleSet2DiffSelector.getSelectedItem()));
                }
                this.diffL1JXtreeTable(diff1, diff2);
                return;
            }
        }
        int length = 0;
        String items[] = new String[L1Prescale.LENGTH];
        for (int i = 0; i < L1Prescale.LENGTH; i++) {
            items[i] = "";
            length++;
        }

        for (L1Item item : super_master.get_l1_master_table().get_menu().getItems()) {
            items[item.get_ctp_id()] = item.get_name() + " / " + item.get_comment();
        }
        tableDetailsL1.removeAll();
        tableDetailsL1.setModel(L1DiffModel);
        L1DiffModel.clearNumRows();
        TableSorter sorterDiffL1 = new TableSorter(tableDetailsL1.getModel());
        tableDetailsL1.setModel(sorterDiffL1);
        JTableHeader headerL1 = tableDetailsL1.getTableHeader();
        sorterDiffL1.setTableHeader(headerL1);
        // swap!
        if (isNullIDinInfo((String) PrescaleSet1DiffSelector.getSelectedItem())) {
            PrescaleSet2 = uploadedL1PrescaleSet;
        } else {
            PrescaleSet2 = new L1Prescale(getIDFromInfo((String) PrescaleSet1DiffSelector.getSelectedItem()));
        }
        if (isNullIDinInfo((String) PrescaleSet2DiffSelector.getSelectedItem())) {
            PrescaleSet1 = uploadedL1PrescaleSet;
        } else {
            PrescaleSet1 = new L1Prescale(getIDFromInfo((String) PrescaleSet2DiffSelector.getSelectedItem()));
        }
        for (int i = 0; i < length; ++i) {
            Integer v1 = PrescaleSet1.get_val(i + 1);
            Integer v2 = PrescaleSet2.get_val(i + 1);
            if (!v1.equals(v2)) {
                ArrayList<Object> L1prescalesdiff_v = new ArrayList<>();
                L1prescalesdiff_v.add("CTP ID: " + i + " / " + items[i]);
                L1prescalesdiff_v.add(CutToValueConverter.calculatePrescaleFromCut(v1));
                L1prescalesdiff_v.add(CutToValueConverter.calculatePrescaleFromCut(v2));
                if (!applyFilter) {
                    L1DiffModel.add(L1prescalesdiff_v);
                } else if (filter(items[i])) {
                    L1DiffModel.add(L1prescalesdiff_v);
                }
            }
        }
        regexFail = false;
        L1DiffModel.fireTableDataChanged();
    }

    private boolean filter(String input) {
        String pattern = "";
        if (!regexFail) {
            pattern = pattern + L1Filter;
        }
        try {
            Pattern reg = Pattern.compile(pattern);
            Matcher Matcher = reg.matcher(input.toLowerCase());
            boolean Match = Matcher.find();
            if (Match) {
                regexFilterHold = L1Filter;
                return true;
            }
        } catch (PatternSyntaxException ex) {
            logger.log(Level.INFO, "Bad regex: {0}", ex);
            logger.info("Breaking out of filtering");
            regexFail = true;
        }
        return false;
    }

    /**
     * Diffing function to diff 2 HLT prescale sets. Compares the two selected
     * sets. Currently assumes that if there is a prescale in one position in on
     * set the same one will be in the smae position in the other. May need to
     * be altered if this is not always the case.
     *
     * @param evt
     */
    private void DoFilterText1OnKeyReleased() throws SQLException {
        // GEN-FIRST:event_FilterText1KeyReleased
        L1Filter = NameFilterText.getText().toLowerCase();
        if (L1Filter.endsWith("\\")) {
            StringBuilder b = new StringBuilder(L1Filter);
            b.replace(L1Filter.lastIndexOf("\\"), L1Filter.lastIndexOf("\\") + 1, "");
            L1Filter = b.toString();
        }
        if (!testfilter()) {
            L1Filter = regexFilterHold;
        }
        if (tableDetailsL1.getModel().getColumnCount() == 4) {
            if (tableDetailsL1 instanceof JXTreeTable) {
                this.updateJXtable((JXTreeTable) tableDetailsL1, null, true);
            } else {
                updateL1prescalestable(true);
            }
        }
        if (tableDetailsL1.getModel().getColumnCount() == 3) {
            boolean filter = false;
            if (!L1Filter.isEmpty()) {
                filter = true;
            }
            updateDiffTable(filter);
        }
    }

    private void DoTypesActionPerformedOnComboBox() throws SQLException {
        // GEN-FIRST:event_jComboTypesActionPerformed
        type.clear();
        if (!TypeSelector.getSelectedItem().equals("All Types")) {
            String SelectedType = TypeSelector.getSelectedItem().toString();
            SelectedType = SelectedType.substring(SelectedType.indexOf("(") + 1, SelectedType.indexOf(")")).trim();
            getItem(SelectedType);
            updateL1prescalestable(true);
        } else {
            updateL1prescalestable(true);
        }
    }

    private void DoL1HideActionPerformed() throws SQLException {
        PrescaleSetListContents.setValueIsAdjusting(true);
        Object[] sel = PrescaleSetListContents.getSelectedValues();
        for (Object sel1 : sel) {
            L1Prescale toHide = new L1Prescale(getIDFromInfo((String) sel1));
            // if the text says hide, we hide it:
            if (HideSetsToggleButton.getText().equals("Hide")) {
                L1TM_PS.hidePS(this.theL1Menu.get_id(), toHide.get_id());
            } else {
                L1TM_PS.unhidePS(this.theL1Menu.get_id(), toHide.get_id());
            }
        }
        l1m.clear();
        getL1Sets(getPartitionFilterSelectorValue());
        getTypes();
        // allow refreshing table.
        PrescaleSetListContents.setValueIsAdjusting(false);
        // select first item.
        if (L1itemsUnUsed.size() > 0) {
            PrescaleSetListContents.setSelectedIndex(0);
        }
    }

    private void changePrescaleSetPartitionActionPerformed() {
        if (userMode.ordinal() >= UserMode.SHIFTER.ordinal() && isEditable) {
            L1SaveButton.setEnabled(true);
        }
    }

    private int getPartitionFilterSelectorValue() {
        if (!isPartitionLocked) {
            return PartitionFilterSelector.getSelectedIndex();
        } else if (PartitionFilterSelector.getSelectedIndex() == 0) {
            return 0;
        } else {
            return lockPartition;
        }
    }

    private void DoPartitionActionPerformedOnComboBox() {
        try {
            this.l1m.clear();
            // Disble valueChanged before retrieving the new PrescaleSets. Avoid
            // repeated refreshes.
            PrescaleSetListContents.setValueIsAdjusting(true);
            logger.log(Level.INFO, "Selected Partition :{0}", getPartitionFilterSelectorValue());
            getL1Sets(getPartitionFilterSelectorValue());
            getTypes();
            PrescaleSetListContents.setValueIsAdjusting(false);
            if (L1itemsUnUsed.size() > 0) {
                PrescaleSetListContents.setSelectedIndex(0);
            }
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while applying partition filter in PrescaleEdit panel.");
        }
    }

    /**
     * Load a prescale set form a XML file into the table with the selected set.
     *
     * @return 0 if OK, else 1.
     */
    private int HandleXMLLoadMouseButtonClick() {
        // 1. Check selected level
        // 2  call L1 load of HLT load

        // The Trigger Menu Level Selected
        // 0 = L1 /  1 = HLT
        int retCode = this.loadL1PrescaleSetsFromXML();

        // if update ok, fire table refresh.
        if (retCode == 0) {
            if (tableDetailsL1 instanceof org.jdesktop.swingx.JXTreeTable) {
                ((JXTreeTable) tableDetailsL1).updateUI();
            } else if (tableDetailsL1 instanceof javax.swing.JTable) {
                this.L1MainModel.fireTableDataChanged();
            }
        }
//        else {
//            logger.warning("Problems updating prescale table.");
//        }
        return retCode;
    }

    /**
     * Check conditions and Upload l1 prescales.
     *
     * @return 0 if ok, 1 otherwise.
     */
    private int loadL1PrescaleSetsFromXML() {
        final int selectedLevel = 0;
        //
        // Read XML file and get basic info.
        XMLprescales xmlPrescales = new XMLprescales(XMLpath.getText());
        xmlPrescales.ReadPrescaleValuesL1();
        int xmlLevel = xmlPrescales.getLevel();
        // Check Level of XML and Selcted table.
        if (xmlLevel < 0) {
            logger.severe(" PrescaleSet Level in XML file not set. \n"
                    + " Please select an adequate XML file.");
            return 1;
        } else if (selectedLevel != xmlLevel) {
            logger.severe(" PrescaleSet Level in XML and Table do not match. \n"
                    + " Please select an adequate XML file.");
            return 1;
        }

        // If No name present, use Menu Name.
        //Object[] selectedSets = listTablesL1UnUsed.getSelectedValues();
        String xmlSetName = xmlPrescales.getPrescaleSetName();
        Integer xmlSetPartition = xmlPrescales.getPrescaleSetPartition();
        if (isPartitionLocked) {
            if (!xmlSetPartition.equals(lockPartition)) {
                JOptionPane.showMessageDialog(this,
                        "Unable to save prescale sets from partition - " + xmlSetPartition + " this panel is locked to partition " + lockPartition,
                        "Error", JOptionPane.ERROR_MESSAGE);
                logger.log(Level.SEVERE, "Can't save L1 Prescale Set with partition {0}", xmlSetPartition);
                return 1;
            }
        }
        // Update the selected L1 prescale Set with the values from
        // the XML file.
        this.PartitionChangeSelector.setSelectedIndex(xmlSetPartition);
        this.PartitionChangeSelector.updateUI();
        if (isPartitionLocked) {
            this.PartitionFilterSelector.setSelectedIndex(1);
        } else {
            this.PartitionFilterSelector.setSelectedIndex(xmlSetPartition);
        }
        this.PartitionFilterSelector.updateUI();
        DoPartitionActionPerformedOnComboBox();
        if (theL1PrescaleSet == null) {
            theL1PrescaleSet = new L1Prescale();
        }

        try {
            this.updateL1prescalestable(false);
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, triggertool.Panels.MainPanel.getMainPanel(), "Error while loading prescale table.");
        }

        LinkedHashMap<Integer, L1PrescaleEditItemNode> map2Update;
        //HelperFunctions.createDefaultMap(theL1Menu);
        L1PrescaleEditItemNode l1ps = null;
        if (MultiplePrescalesSelectBox.isSelected()) {
            JXTreeTable tab = (JXTreeTable) tableDetailsL1;
            L1PrescaleTreeTableModel model;
            model = (L1PrescaleTreeTableModel) tab.getTreeTableModel();
            // Set the name of the edited prescale set.
            l1ps = model.getPrescales().get(0);
            // create a Map (similar to the table) for saving. This is to reuse
            // the already existing saving facilities.
            map2Update = HelperFunctions.PrescaleSetNode2Map(l1ps);
        } else {
            map2Update = this.l1m;
        }
        if (!xmlPrescales.updatePrescaleSetL1(map2Update, false)) {
            logger.severe(" No L1 Items Present in loaded menu, unable to attach prescales");
            return 1;
        }
    
        if (l1ps != null) {
            l1ps.setNodeName(xmlSetName);
            l1ps.setVersion(-1);
        } else if (!this.SetName.getText().equalsIgnoreCase(xmlSetName)) {
            this.SetName.setText(xmlSetName);
            /*try {
                this.getL1Sets(getPartitionFilterSelectorValue());
                this.getTypes();
            } catch (SQLException ex) {
                TTExceptionHandler.HandleSqlException(ex, triggertool.Panels.MainPanel.getMainPanel(), "Error while loading prescale sets table.");
            }*/
        }
 
        if (this.uploadedL1PrescaleSet != null) {
            this.PrescaleSet1DiffSelector.removeItem("~: " + uploadedL1PrescaleSet.get_name());
            this.PrescaleSet2DiffSelector.removeItem("~: " + uploadedL1PrescaleSet.get_name());
        }
        // New L1 Prescale object from table
        this.uploadedL1PrescaleSet
                = HelperFunctionsTreeTable.createPsSetFromNodeMap(map2Update,
                        xmlSetName + " *");
        // Set id to -id.
        this.uploadedL1PrescaleSet.set_id(-1 * (getIDFromInfo((String) PrescaleSetListContents.getSelectedValue())));
        if (xmlSetPartition == getPartitionFilterSelectorValue() || getPartitionFilterSelectorValue() == 0) {
            this.PrescaleSet1DiffSelector.addItem("~: " + uploadedL1PrescaleSet.get_name());
            this.PrescaleSet2DiffSelector.addItem("~: " + uploadedL1PrescaleSet.get_name());
            this.PrescaleSet2DiffSelector.setSelectedItem("~: " + uploadedL1PrescaleSet.get_name());
        }

        if (userMode.ordinal() >= UserMode.SHIFTER.ordinal() && isEditable) {
            this.L1SaveButton.setEnabled(true);
        }
        return 0;
    }

    private Integer getIDFromInfo(String info) {
        return Integer.parseInt(info.split(":")[0]);
    }

    private Boolean isNullIDinInfo(String info) {
        if (info.split(":")[0].equals("~")) {
            return true;
        }
        return false;
    }

    private String getNameFromInfo(String info) {
        return info.split(":", 2)[1];
    }

    /**
     * Clear the filtering text field and reloads table.
     */
    private void resetL1() throws SQLException {
        NameFilterText.setText("");
        L1Filter = "";
        L1Filters.clear();
        TypeSelector.setSelectedIndex(0);
        this.updateL1prescalestable(true);
        if (PrescaleSetDoDiffButton.isSelected() == true) {
            this.updateDiffTable(true);
        }
    }

    private void DoValueChanged(ListSelectionEvent e) throws SQLException {
        if (e.getValueIsAdjusting()) {
            return;
        }

        Boolean activeFilters = false;
        String savedNameFilter = "";
        String savedNameFilterLabel = "";
        Integer savedTypeFilterIndex = 0;

        // Check Multiple Prescales Tree.
        if (MultiplePrescalesSelectBox.isSelected()) {
            this.actionL1PrescalesList();
            //return;
        } else {
            if (!NameFilterText.getText().equals("")) {
                savedNameFilterLabel = NameFilterText.getText();
                savedNameFilter = L1Filter;
                activeFilters = true;
            }
            if (!TypeSelector.getSelectedItem().equals("All Types")) {
                savedTypeFilterIndex = TypeSelector.getSelectedIndex();
                activeFilters = true;
            }
            this.actionL1PrescalesListSingleSelection();
        }

        if (PrescaleSetListContents.getSelectedValue() == null) {
            tableDetailsL1.clearSelection();
            return;
        }
        theL1PrescaleSet = new L1Prescale(getIDFromInfo((String) PrescaleSetListContents.getSelectedValue()));
        if (theL1PrescaleSet == null) {
            tableDetailsL1.clearSelection();
            return;
        }
        // update the comment label
        L1CommentFieldLabel.setText("Comment Field for Set ID " + theL1PrescaleSet.get_id());
        // show the comment
        CommentTextField.setText(theL1PrescaleSet.get_comment());
        // update the comment button
        UpdateCommentButton.setText(" Update comment for set ID " + theL1PrescaleSet.get_id());
        // show the name, lumi, default, shiftsafe
        SetName.setText(theL1PrescaleSet.get_name());
        if (isPartitionLocked) {
            PartitionChangeSelector.setSelectedIndex(lockPartition);
        } else {
            PartitionChangeSelector.setSelectedIndex(theL1PrescaleSet.get_partition());
            PartitionChangeSelector.updateUI();
        }

        // update the l1 prescales table
        updateL1prescalestable(false);
        if (activeFilters) {
            NameFilterText.setText(savedNameFilterLabel);
            L1Filter = savedNameFilter;
            TypeSelector.setSelectedIndex(savedTypeFilterIndex);
            updateL1prescalestable(true);
        }
    }

    /**
     * If the selection on listTablesL1UnUsed changes, recreate the map/tree
     * holding the table data.
     */
    private void actionL1PrescalesList() throws SQLException {
        if (tableDetailsL1 == null || !(tableDetailsL1 instanceof JXTreeTable)) {
            Object[] sel = PrescaleSetListContents.getSelectedValues();
            listOfSelected.clear();
            for (Object sel1 : sel) {
                listOfSelected.add(new L1Prescale(getIDFromInfo((String) sel1)));
            }
            this.createPrescalesTreeTable(listOfSelected, super_master);
        } else if (!saving) {
            // Get The TreeTableModel
            L1PrescaleTreeTableModel tableModel = (L1PrescaleTreeTableModel) ((JXTreeTable) tableDetailsL1).getTreeTableModel();
            // Get The RootNode
            L1PrescaleEditItemNode tableRootNode = (L1PrescaleEditItemNode) tableModel.getRoot();

            Object[] sel = PrescaleSetListContents.getSelectedValues();
            List<L1Prescale> listOfSelectedDBPrescales = new ArrayList<>();
            for (Object sel1 : sel) {
                listOfSelectedDBPrescales.add(new L1Prescale(getIDFromInfo((String) sel1)));
            }

            if (tableModel.isEmpty()) {
                listOfSelected.clear();
                listOfSelected.addAll(listOfSelectedDBPrescales);
                this.updateJXtable((JXTreeTable) tableDetailsL1, listOfSelectedDBPrescales, false);
            } else if (listOfSelectedDBPrescales.containsAll(listOfSelected)) {
                // User add one set to the selected ones on the top right list.
                // add the selected set to the listOfselected and reload the table.
                listOfSelected.clear();
                listOfSelected.addAll(listOfSelectedDBPrescales);
                updateJXtable((JXTreeTable) tableDetailsL1, listOfSelectedDBPrescales, false);
            } else {
                // User deselect one item.
                List<L1Prescale> listOfDeselected = new ArrayList<>();
                List<L1Prescale> listOfChanged = new ArrayList<>();
                List<LinkedHashMap<Integer, L1PrescaleEditItemNode>> listOfChangedMaps = new ArrayList<>();
                List<String> listOfChangedNames = new ArrayList<>();
                for (L1Prescale oldDBPrescale : listOfSelected) {
                    if (!listOfSelectedDBPrescales.contains(oldDBPrescale)) {
                        listOfDeselected.add(oldDBPrescale);
                    }
                }
                // Remove all deselected items from list.
                listOfSelectedDBPrescales.removeAll(listOfDeselected);
                // Check whether table edited
                for (L1Prescale des : listOfDeselected) {
                    LinkedHashMap<Integer, L1PrescaleEditItemNode> aMap = checkChanged(des, tableRootNode);
                    if (aMap != null) {
                        listOfChanged.add(des);
                        listOfChangedMaps.add(aMap);
                        listOfChangedNames.add(des.get_id() + ": " + des.get_name() + " v" + des.get_version());
                    }
                }
                // if edited promp for save
                boolean save = false;
                if (listOfChanged.size() > 0) {
                    // Remove created original prescale set for Diffing.
                    if (this.uploadedL1PrescaleSet != null) {
                        this.PrescaleSet1DiffSelector.removeItem("~: " + uploadedL1PrescaleSet.get_name());
                        this.PrescaleSet2DiffSelector.removeItem("~: " + uploadedL1PrescaleSet.get_name());
                    }
                    this.uploadedL1PrescaleSet = null;
                    save = HelperFunctions.promptToSave(listOfChangedNames);
                    saving = save;
                }
                // save
                if (save) {
                    List<Integer> savedIdxs = new ArrayList<>();
                    for (int i = 0; i < listOfChangedNames.size(); i++) {
                        LinkedHashMap<Integer, L1PrescaleEditItemNode> savingMap = listOfChangedMaps.get(i);
                        savedIdxs.add(this.saveL1Map(savingMap, null));
                    }
                    saving = false;
                    listOfSelected.clear();
                    listOfSelected.addAll(listOfSelectedDBPrescales);

                    List<Integer> idxs = new ArrayList<>();
                    for (L1Prescale ps : listOfSelected) {
                        idxs.add(L1itemsUnUsed.indexOf(ps));
                    }
                    for (int i = 0; i < this.L1itemsUnUsed.getSize(); i++) {
                        if (savedIdxs.contains(getIDFromInfo((String) this.L1itemsUnUsed.get(i)))) {
                            if (!idxs.contains(i)) {
                                idxs.add(i);
                            }
                            listOfSelectedDBPrescales.add(new L1Prescale(getIDFromInfo((String) this.L1itemsUnUsed.get(i))));
                        }
                    }
                    int[] arrIdx = new int[idxs.size()];
                    for (int i = 0; i < idxs.size(); i++) {
                        arrIdx[i] = idxs.get(i);
                    }
                    PrescaleSetListContents.setSelectedIndices(arrIdx);
                }

                listOfSelected.clear();
                listOfSelected.addAll(listOfSelectedDBPrescales);
                updateJXtable((JXTreeTable) tableDetailsL1, listOfSelectedDBPrescales, false);
            }
        }
    }

    /**
     * Do all we need to do after clicking in the L1 Prescale set list.
     */
    private void actionL1PrescalesListSingleSelection() throws SQLException {
        // stop filtering
        this.resetL1();

        // stop diffing if we are doing so
        if (PrescaleSetDoDiffButton.isSelected()) {
            PrescaleSetDoDiffButton.doClick();
        }

        // If we have already looked at one set, there will be some entries in the map
        // Compare whats there now with the original - prompt to save if different
        boolean l1prescalesedited = HelperFunctions.checkIfPrescaleSetEdited(l1m, theL1PrescaleSet);

        //If the values on the table map chaged, prompt to save.
        if (l1prescalesedited) {
            // Remove created original prescale set for Diffing.
            if (this.uploadedL1PrescaleSet != null) {
                this.PrescaleSet1DiffSelector.removeItem("~: " + uploadedL1PrescaleSet.get_name());
                this.PrescaleSet2DiffSelector.removeItem("~: " + uploadedL1PrescaleSet.get_name());
            }
            this.uploadedL1PrescaleSet = null;

            logger.info("L1 prescales have been edited");
            String[] choices = {"Save", "Discard"};
            Integer response = JOptionPane.showOptionDialog(null,
                    "The L1 Prescale set " + theL1PrescaleSet
                    + " has been edited.\n Do "
                    + "you wish to save the changes as a new set or discard?",
                    "",
                    0,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    choices,
                    choices[0]);
            // if user press ESC response==-1.
            if (response >= 0 && choices[response].equals("Save")) {
                logger.info("Saving changes");
                saveL1As();
            } else {
                logger.info("Discarding changes");
            }
        }
    }

    /**
     * TODO add comments
     *
     * @param applyfilter
     * @throws java.sql.SQLException
     */
    public void updateL1prescalestable(boolean applyfilter) throws SQLException {
        // The length to items and prescales.
        int psLength = triggerdb.Entities.L1.L1Prescale.LENGTH;
        // if we click on the list of sets, we draw from the db
        // if we are filtering, the datavector has entries and we work on them!
        tableDetailsL1.clearSelection();

        // Delegate to JXTreeTable update
        if (tableDetailsL1 instanceof JXTreeTable) {
            listOfSelected.clear();
            Object[] sel = PrescaleSetListContents.getSelectedValues();
            for (Object sel1 : sel) {
                listOfSelected.add(new L1Prescale(getIDFromInfo((String) sel1)));
            }
            this.updateJXtable(((JXTreeTable) tableDetailsL1), listOfSelected, applyfilter);
            ((JXTreeTable) tableDetailsL1).expandAll();

        }
        // Write prescales in table
        // if from db
        if (!applyfilter) {
            L1MainModel.clearNumRows(); // clear data vector
            String items[] = new String[psLength];
            for (int i = 0; i < psLength; i++) {
                items[i] = "";
            }

            for (L1Item item : super_master.get_l1_master_table().get_menu().getItems()) {
                if (!isPartitionLocked) {
                    if (item.get_partition() == getPartitionFilterSelectorValue() || getPartitionFilterSelectorValue() == 0) {
                        items[item.get_ctp_id()] = item.get_name();
                    }
                } else if (item.get_partition().equals(lockPartition)) {
                    items[item.get_ctp_id()] = item.get_name();
                }
            }

            for (int i = 0; i < psLength; i++) {
                if (!items[i].isEmpty()) {
                    L1PrescaleEditItemNode L1prescalesdata_v = new L1PrescaleEditItemNode();
                    L1prescalesdata_v.setId(i);
                    L1prescalesdata_v.setNodeName(items[i]);
                    L1prescalesdata_v.setCutInIntFormat(theL1PrescaleSet.get_val(i + 1));
                    L1MainModel.add(L1prescalesdata_v);
                    l1m.put(i, L1prescalesdata_v);

                    // validate the insertion
                    int origCut = theL1PrescaleSet.get_val(i + 1);
                    int insertedCut = CutToValueConverter.calculateCutFromPrescale(l1m.get(i).getPrescaleValue());
                    if (origCut != insertedCut) {
                        logger.log(Level.SEVERE, "Found modified L1 PS cut value! original: {0}, inserted: {1}", new Object[]{origCut, insertedCut});
                    }
                }
            }
        } else {
            List<L1PrescaleEditItemNode> contents_v = new ArrayList<>();
            Set<Map.Entry<Integer, L1PrescaleEditItemNode>> set = l1m.entrySet();
            Iterator<Map.Entry<Integer, L1PrescaleEditItemNode>> i = set.iterator();
            while (i.hasNext()) {
                Map.Entry<Integer, L1PrescaleEditItemNode> me = i.next();
                L1PrescaleEditItemNode item = me.getValue();
                String ctpitem = item.getNodeName();
                if (applyfilter == true && L1Filter.length() != 0) {
                    if (!filter(ctpitem.toLowerCase())) {
                        continue;
                    }
                }
                if (applyfilter == true && !type.isEmpty()) {
                    if (!type.contains(ctpitem)) {
                        continue;
                    }
                }
                contents_v.add(item);
            } // now clear old data vector and add new one
            regexFail = false;
            L1MainModel.clearNumRows(); // clear data vector

            for (L1PrescaleEditItemNode l1rowdata : contents_v) {
                L1MainModel.add(l1rowdata);
                int idx = l1rowdata.getId();
                l1m.put(idx, l1rowdata);
            }
        }
    }

    /**
     * Tries to save this.l1m Map containing the data on the current table. We
     * should try to avoid this method but used the ones with parameters.
     */
    @Deprecated
    private void saveL1As() throws SQLException {
        int id = this.saveL1Map(l1m, SetName.getText());
        Boolean matched = false;
        String info = "";
        for (int i = 0; i < this.L1itemsUnUsed.getSize(); i++) {
            info = (String) this.L1itemsUnUsed.get(i);
            if (getIDFromInfo(info).equals(id)) {
                matched = true;
                break;
            }
        }
        if (matched) {
            PrescaleSetListContents.setSelectedValue(info, true);
        } else {
            PrescaleSetListContents.setSelectedIndex(0);
        }
    }

    /**
     * Creates a new L1Prescale from the LinkedHashedMap on the edit panel,
     * checks whether this L1Prescale already exists, if so updates that one,
     * else creates a new one a saves it to DB.
     *
     * @param tableMap a linked hash map containing a prescale set.
     * @param setName the name of the set to save.
     * @return the id of the saved prescale set.
     */
    private int saveL1Map(
            final LinkedHashMap<Integer, L1PrescaleEditItemNode> tableMap,
            final String setName) throws SQLException {
        int savedId;
        String name;
        if (setName == null) {
            name = tableMap.get(0).getPrescaleSetName();
        } else {
            name = setName;
        }
        // create a new prescale set record with a new name
        L1Prescale newL1PrescaleSet = HelperFunctionsTreeTable.createPsSetFromNodeMap(tableMap, name);
        newL1PrescaleSet.set_partition(PSSPartition);
        // try to save the set.
        try {
            savedId = newL1PrescaleSet.save();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this,
                    "Can't save! Correct and try again - " + e.getMessage(),
                    "Error", JOptionPane.ERROR_MESSAGE);
            logger.log(Level.SEVERE, " Can't save L1 Prescale Set with name "
                    + name, e);

            return -1;
        }
        if (isPartitionLocked && getPartitionFilterSelectorValue() == 0) {
            JOptionPane.showMessageDialog(this,
                    "Saved Prescale Set to Partition in other than the one displayed - " + lockPartition,
                    "Info", JOptionPane.INFORMATION_MESSAGE);
            logger.log(Level.INFO, "Saved Prescale Set to Partition in other than the one displayed {0}", lockPartition);
        }

        // see if saved set already in linked ones
        L1Menu _l1Menu = this.theL1Menu;
        boolean exists = _l1Menu.addPrescaleSet(newL1PrescaleSet, true);
        if (exists) {
            L1Prescale set = new L1Prescale(savedId);
            JOptionPane.showMessageDialog(null,
                    "Prescale set being saved duplicates set " + set.get_id() + ": " + set.get_name(),
                    "Equivalent set already in database",
                    JOptionPane.INFORMATION_MESSAGE);
        }

        if (ViewHiddenSetsToggleButton.isSelected()) {
            ViewHiddenSetsToggleButton.doClick();
        }
        // the above achieves these 3 lines:
        tableMap.clear();
        this.getL1Sets(getPartitionFilterSelectorValue());
        this.getTypes();

        return savedId;
    }

    /**
     * Save all prescale sets on the tableDetailsL1 JXTreeTable.
     *
     * @return a vector with the ids of the saved prescales.
     */
    private List<Integer> saveL1Multiple() throws SQLException {
        if (!(tableDetailsL1 instanceof JXTreeTable)) {
            logger.warning("Trying to save a wrong table. skipping");
            return null;
        }
        L1PrescaleTreeTableModel tableModel = (L1PrescaleTreeTableModel) ((JXTreeTable) tableDetailsL1).getTreeTableModel();
        List<L1PrescaleEditItemNode> tablePrescales = tableModel.getPrescales();
        List<Integer> savedIdx = new ArrayList<>();
        this.saving = true;
        for (L1PrescaleEditItemNode prescale : tablePrescales) {
            LinkedHashMap<Integer, L1PrescaleEditItemNode> psMap
                    = HelperFunctions.PrescaleSetNode2Map(prescale);
            savedIdx.add(this.saveL1Map(psMap, null));
        }
        this.saving = false;

        return savedIdx;
    }

    /**
     * recreates <code>tableDetailsL1</code> as a new JXTable. It adds a
     * TreeTable data model to it.
     *
     * @param prescaleSets the list of prescale sets to add to the table.
     * @param smt the super master table.
     */
    private void createPrescalesTreeTable(final List<L1Prescale> prescaleSets,
            final SuperMasterTable smt) throws SQLException {
        // Disable non used fields.
        this.SetName.setText("");
        this.SetName.setEnabled(false);

        // New ROOT node.
        L1PrescaleEditItemNode root = new L1PrescaleEditItemNode("top");
        // Generate the tree with the given prescales
        HelperFunctionsTreeTable.generateL1Tree(smt, prescaleSets, root);

        L1PrescaleTreeTableModel model = new L1PrescaleTreeTableModel(root);

        this.ctpItems.removeAllChildren();

        this.ctpItems.addChildren(root.getChildren());

        tableDetailsL1 = new JXTreeTable(model);
        ((JXTreeTable) tableDetailsL1).setRootVisible(false);
        //((JXTreeTable) tableDetailsL1).expandAll();
        tableDetailsL1.addFocusListener(new java.awt.event.FocusAdapter() {

            @Override
            public final void focusGained(final java.awt.event.FocusEvent evt) {
                tableDetailsL1FocusGained(evt);
            }
        });
        ((JXTreeTable) tableDetailsL1).setClosedIcon(IconValue.NULL_ICON);
        ((JXTreeTable) tableDetailsL1).setOpenIcon(IconValue.NULL_ICON);
        ((JXTreeTable) tableDetailsL1).setLeafIcon(IconValue.NULL_ICON);

        // Highlight Rows
        Color cellBack = TriggerToolCellRenderer.CELLBKG;
        Color cellFor = ColorUtil.setSaturation(Color.BLUE, 0.9f);
        ColorHighlighter hLeaf = new ColorHighlighter(HighlightPredicate.IS_LEAF, null, cellFor);
        ColorHighlighter hLeafEven = new ColorHighlighter(HighlightPredicate.EVEN, cellBack, null);
        ((JXTreeTable) tableDetailsL1).addHighlighter(hLeafEven);
        ((JXTreeTable) tableDetailsL1).addHighlighter(hLeaf);

        // Custom Renderers, formating of number in cells
        TriggerToolCellRenderer rend = new TriggerToolCellRenderer();
        // Skip color rows by renderer, this somwhow doesn't work on
        // JXtreeTables (20110628 - Tiago).
        rend.setUseColors(false);
        ((JXTreeTable) tableDetailsL1).setDefaultRenderer(java.lang.Float.class, rend);
        ((JXTreeTable) tableDetailsL1).setDefaultRenderer(java.lang.Boolean.class, rend);
        ((JXTreeTable) tableDetailsL1).setDefaultRenderer(java.lang.String.class, rend);
        //
        ((JXTreeTable) tableDetailsL1).expandAll();
        PrescaleListPane.setViewportView(tableDetailsL1);
    }

    /**
     * recreates tableDetailsL1 as a JTable. Called after unchecking "multiple
     * prescales" check box.
     */
    private void recreatePrescalesJTable() {
        logger.finest("recreating prescales JTable");
        // Block refreshing of table until we are done.
        PrescaleSetListContents.setValueIsAdjusting(true);
        // set first selected idx als only sel idx
        int idx = PrescaleSetListContents.getSelectedIndex();
        this.PrescaleSetListContents.setSelectedIndex(idx);

        // Enable fields.
        this.SetName.setEnabled(true);
        this.UpdateCommentButton.setEnabled(true);
        this.CommentTextField.setEnabled(true);
        //release treetable nodes from mem.
        this.ctpItems.removeAllChildren();
        this.listOfSelected.clear();
        this.tableDetailsL1 = new javax.swing.JTable();

        tableDetailsL1 = new javax.swing.JTable();
        tableDetailsL1.setModel(this.L1MainModel);
        // Remove grid lines
        TableColumnModel tcm = tableDetailsL1.getColumnModel();
        tcm.setColumnMargin(0);
        // Cell Renderer
        TriggerToolCellRenderer rend = new TriggerToolCellRenderer();
        rend.setHorizontalAlignment(SwingConstants.LEFT);
        // force text to be black...
        rend.setForeground(Color.black);
        tableDetailsL1.setDefaultRenderer(java.lang.Float.class, rend);
        tableDetailsL1.setDefaultRenderer(java.lang.String.class, rend);
        tableDetailsL1.setDefaultRenderer(java.lang.Boolean.class, rend);
        tableDetailsL1.addFocusListener(new java.awt.event.FocusAdapter() {

            @Override
            public final void focusGained(final java.awt.event.FocusEvent evt) {
                tableDetailsL1FocusGained(evt);
            }
        });
        // Add sorting capabilities.
        TableSorter sorterL1 = new TableSorter(tableDetailsL1.getModel());
        tableDetailsL1.setModel(sorterL1);
        tableDetailsL1.putClientProperty("terminateEditOnFocusLost",
                Boolean.TRUE);

        JTableHeader headerL1 = tableDetailsL1.getTableHeader();
        sorterL1.setTableHeader(headerL1);

        // allow refresh of table.
        PrescaleSetListContents.setValueIsAdjusting(false);

        PrescaleListPane.setViewportView(tableDetailsL1);
    }

    /**
     *
     * Load new Prescales onto table. Already existing prescales are skiped.
     * Remove deselected prescales.
     *
     * @param table the table.
     * @param listOfSelectedDBPrescales the list of selected prescales that will
     * be added.
     * @param applyFilter if <code>true</code> items are filered before being
     * added to the table.
     */
    // TODO reduce passes thru this method - Be more careful with the fire table changes...
    private void updateJXtable(JXTreeTable table, List<L1Prescale> listOfSelectedDBPrescales, boolean applyFilter) {
        L1PrescaleTreeTableModel tableModel = (L1PrescaleTreeTableModel) table.getTreeTableModel();
        // Read the selected Prescales and load them to the Table.
        if (!applyFilter) {
            List<Integer> ids = new ArrayList<>();
            for (L1Prescale l1ps : listOfSelectedDBPrescales) {
                ids.add(l1ps.get_id());
            }
            // Clear Table and local Copy
            HelperFunctionsTreeTable.clearPrescalesButKeep(this.ctpItems, ids);
            // add new prescales to table.c
            // TODO move this to tablemodelMethod.
            HelperFunctionsTreeTable.addPrescaleSets(this.ctpItems, listOfSelectedDBPrescales);
            tableModel.updateItems(this.ctpItems.getChildren());

        } else {
            // DO FILTER
            List<L1PrescaleEditItemNode> filteredItems = new ArrayList<>();
            for (L1PrescaleEditItemNode item : this.ctpItems.getChildren()) {
                String ctpItemName = item.getNodeName();
                if (!item.getNodeName().toLowerCase().contains(L1Filter)) {
                    continue;
                }
                if (!type.isEmpty()) {
                    if (!type.contains(ctpItemName)) {
                        continue;
                    }
                }
                filteredItems.add(item);
            }
            tableModel.updateItems(filteredItems);
        }// refresh TREE

        ((JXTreeTable) tableDetailsL1).updateUI();
    }

    /**
     * Checks whether a L1Prescale was changed on the table.
     *
     * @param deselected a L1Prescale.
     * @param tableRoot the root node of the JXTreeTable.
     * @return a LinkedHashedMap with the data from the edited table.
     */
    private LinkedHashMap<Integer, L1PrescaleEditItemNode> checkChanged(L1Prescale deselected, L1PrescaleEditItemNode tableRoot) {
        int setId = deselected.get_id();
        L1PrescaleEditItemNode tableOldPrescaleNode = HelperFunctions.getPrescaleSetWithId(tableRoot, setId);
        LinkedHashMap<Integer, L1PrescaleEditItemNode> aMap
                = HelperFunctions.PrescaleSetNode2Map(tableOldPrescaleNode);
        // check if this prescale NODE Changed on TreeTable;
        boolean l1prescalesedited = HelperFunctions.checkIfPrescaleSetEdited(aMap, deselected);
        if (l1prescalesedited) {
            return aMap;
        } else {
            return null;
        }
    }

    /**
     * Removes equal items from the table and refresh.
     *
     * @param ps1 a L1Prescale.
     * @param ps2 another L1Prescale.
     */
    private void diffL1JXtreeTable(L1Prescale ps1, L1Prescale ps2) throws SQLException {
        // Check whether we are trying to diff a hidden set.
        boolean ps1hidden = ps1.getHidden(theL1Menu.get_id());
        boolean ps2hidden = ps2.getHidden(theL1Menu.get_id());

        // If something goes wrong, will reset the currently selected indexes.
        int[] oldSelection = PrescaleSetListContents.getSelectedIndices();

        // get index of selected prescales.
        int[] selIdx = new int[2];
        selIdx[0] = ((DefaultListModel) PrescaleSetListContents.getModel()).indexOf(ps1);
        selIdx[1] = ((DefaultListModel) PrescaleSetListContents.getModel()).indexOf(ps2);

        // Block the updating of the table until we are done.
        PrescaleSetListContents.setValueIsAdjusting(true);
        // Check if we are trying to compare after xml upload.
        int ps1Id = ps1.get_id();
        int ps2Id = ps2.get_id();

        L1Prescale originalSet = null;

        // ps1 not present in the list
        if (selIdx[0] < 0 && !ps1hidden) {
            // op 1 we've just xml upload
            if (ps1.get_name().contains("*")) {
                // Try to compare old with new.
                if (selIdx[1] >= 0 && ps1Id == ps2Id) {
                    // Get untouched prescale set from list
                    originalSet = (L1Prescale) ((DefaultListModel) PrescaleSetListContents.getModel()).get(selIdx[1]);
                } else {
                    // Use selected set with the changed Values as reference.
                    selIdx[0] = PrescaleSetListContents.getSelectedIndex();
                }
            }
        }
        if (ps2.get_name().contains("*")) {
            // Try to compare old with new.
            if (selIdx[1] < 0) {
                if (selIdx[0] >= 0 && ps1Id == -ps2Id) {
                    // Get untouched prescale set from list
                    originalSet = (L1Prescale) ((DefaultListModel) PrescaleSetListContents.getModel()).get(selIdx[0]);
                } else {
                    selIdx[1] = PrescaleSetListContents.getSelectedIndex();
                }

            }
        }

        List<L1Prescale> listOfOldPrescales = new ArrayList<>();
        if (originalSet != null) {
            L1Prescale ps = (L1Prescale) originalSet.clone();
            ps.set_name(originalSet.get_name() + " in DB ");
            ps.set_version(originalSet.get_version());
            int id = ((L1Prescale) originalSet).get_id();
            ps.set_id(-1 * id);
            for (int i = 1; i <= L1Prescale.LENGTH; i++) {
                ps.set_val(i, originalSet.get_val(i));
            }
            listOfOldPrescales.add(ps);
        }

        PrescaleSetListContents.setSelectedIndices(selIdx);

        // update table here.
        PrescaleSetListContents.setValueIsAdjusting(false);
        // remove equal items
        L1PrescaleTreeTableModel model = (L1PrescaleTreeTableModel) ((JXTreeTable) tableDetailsL1).getTreeTableModel();
        model.addPrescales(listOfOldPrescales, this.ctpItems.getChildren());
        // try to add hidden ps sets
        listOfOldPrescales.clear();
        if (ps1hidden) {
            listOfOldPrescales.add(ps1);
        }
        if (ps2hidden) {
            listOfOldPrescales.add(ps2);
        }
        model.addPrescales(listOfOldPrescales, this.ctpItems.getChildren());

        model.removeEquals();
        List<L1PrescaleEditItemNode> pslist = model.getPrescales();
        if (pslist == null || pslist.isEmpty()) {
            List<String> names = new ArrayList<>();
            names.add(ps1.get_id() + ": " + ps1.get_name() + " v" + ps1.get_version());
            names.add(ps2.get_id() + ": " + ps2.get_name() + " v" + ps2.get_version());
            if (this.uploadedL1PrescaleSet != null) {
                this.PrescaleSet1DiffSelector.removeItem("~: " + uploadedL1PrescaleSet.get_name());
                this.PrescaleSet2DiffSelector.removeItem("~: " + uploadedL1PrescaleSet.get_name());
            }
            this.uploadedL1PrescaleSet = null;
            HelperFunctions.promptEqual(names);
            PrescaleSetListContents.setSelectedIndices(oldSelection);
        }
        // Do diff and disable "diff" button.
        PrescaleSetDoDiffButton.setSelected(false);
        ((JXTreeTable) tableDetailsL1).updateUI();

    }

    /**
     * Refresh the diff comboBoxes.
     */
    private void updateDiffPrescaleSets() throws SQLException {
        Boolean diffInProgress = false;
        Object store1 = null;
        Object store2 = null;
        if (PrescaleSetDoDiffButton.isSelected() == true) {
            diffInProgress = true;
            PrescaleSetDoDiffButton.setSelected(false);
            store1 = PrescaleSet1DiffSelector.getSelectedItem();
            store2 = PrescaleSet2DiffSelector.getSelectedItem();
        }
        this.PrescaleSet1DiffSelector.removeAllItems();
        this.PrescaleSet2DiffSelector.removeAllItems();
        ArrayList<Integer> partitionsToQuery = new ArrayList<>();
        partitionsToQuery.add(0);
        partitionsToQuery.add(getPartitionFilterSelectorValue());
        List<String> sets = this.theL1Menu.getPrescaleSetInfoPartition(show_hidden_l1, partitionsToQuery);
        for (String l1ps : sets) {
            this.PrescaleSet1DiffSelector.addItem(l1ps);
            this.PrescaleSet2DiffSelector.addItem(l1ps);
        }
        if (diffInProgress) {
            int var1 = ((DefaultComboBoxModel) (PrescaleSet1DiffSelector.getModel())).getIndexOf(store1);
            int var2 = ((DefaultComboBoxModel) (PrescaleSet2DiffSelector.getModel())).getIndexOf(store2);
            if (var1 != -1 && var2 != -1) {
                PrescaleSet1DiffSelector.setSelectedItem(store1);
                PrescaleSet2DiffSelector.setSelectedItem(store2);
                PrescaleSetDoDiffButton.setSelected(true);
            }
        }
    }

    private boolean testfilter() {
        String pattern = L1Filter;
        try {
            Pattern reg = Pattern.compile(pattern);
            Matcher Matcher = reg.matcher("Lorem ipsum");
            boolean Match = Matcher.find();

            if (Match) {
                return true;
            }
        } catch (PatternSyntaxException ex) {
            logger.log(Level.INFO, "Bad regex: {0}", ex);
            return false;
        }
        return true;
    }

    private class PrescaleSetListListener implements ListSelectionListener {

        /**
         * When the user clicks on a prescale set in the list display its
         * details, i.e. the prescales.
         */
        @Override
        public void valueChanged(ListSelectionEvent e) {

            // ///////////////////////////////
            // clicked on l1 prescale set list
            if (e.getSource() == PrescaleSetListContents) {
                try {
                    DoValueChanged(e);
                } catch (SQLException ex) {
                    TTExceptionHandler.HandleSqlException(ex, triggertool.Panels.MainPanel.getMainPanel(), "Error while value change in PrescaleEdit panel.");
                }
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        L1PrescalePanel = new javax.swing.JPanel();
        PartitionFilterSelector = new javax.swing.JComboBox();
        PrescaleListPane = new javax.swing.JScrollPane();
        tableDetailsL1 = new javax.swing.JTable();
        PartitionFilterLabel = new javax.swing.JLabel();
        L1CommentFieldLabel = new javax.swing.JLabel();
        SetNameLabel = new javax.swing.JLabel();
        SetName = new javax.swing.JTextField();
        CommentPane = new javax.swing.JScrollPane();
        CommentTextField = new javax.swing.JTextArea();
        UpdateCommentButton = new javax.swing.JButton();
        PrescaleSetListPane = new javax.swing.JScrollPane();
        PrescaleSetListContents = new javax.swing.JList<>();
        ItemNameFilterPanel = new javax.swing.JPanel();
        NameFilterText = new javax.swing.JTextField();
        TypeSelector = new javax.swing.JComboBox<>();
        NameFilterLabel = new javax.swing.JLabel();
        TypeSelectorLabel = new javax.swing.JLabel();
        FilterResetButton = new javax.swing.JButton();
        CompareSetPanel = new javax.swing.JPanel();
        PrescaleSet2DiffSelector = new javax.swing.JComboBox<>();
        PrescaleSet1DiffSelector = new javax.swing.JComboBox<>();
        PrescaleSetDoDiffButton = new javax.swing.JToggleButton();
        ViewHiddenSetsToggleButton = new javax.swing.JToggleButton();
        HideSetsToggleButton = new javax.swing.JButton();
        MultiplePrescalesSelectBox = new javax.swing.JCheckBox();
        CommentAllButton = new javax.swing.JButton();
        XMLLoadButton = new javax.swing.JButton();
        FileSystemXMLBrowseButton = new javax.swing.JButton();
        PrescaleSetChangePartitionLabel = new javax.swing.JLabel();
        PartitionChangeSelector = new javax.swing.JComboBox();
        RefreshSetsButton = new javax.swing.JButton();
        EnableSelectionButton = new javax.swing.JButton();
        DisableSelectionButton = new javax.swing.JButton();
        L1SaveButton = new javax.swing.JButton();
        XMLpath = new javax.swing.JTextField();

        setPreferredSize(new java.awt.Dimension(1248, 653));

        L1PrescalePanel.setPreferredSize(new java.awt.Dimension(1260, 814));

        PartitionFilterSelector.setFont(new java.awt.Font("Ubuntu", 0, 10)); // NOI18N
        PartitionFilterSelector.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "1", "2", "3" }));
        PartitionFilterSelector.setToolTipText("select partition");
        PartitionFilterSelector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PartitionFilterSelectorActionPerformed(evt);
            }
        });

        tableDetailsL1.setModel(new L1PrescaleTableModel());
        tableDetailsL1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tableDetailsL1FocusGained(evt);
            }
        });
        PrescaleListPane.setViewportView(tableDetailsL1);

        PartitionFilterLabel.setFont(new java.awt.Font("Ubuntu", 0, 10)); // NOI18N
        PartitionFilterLabel.setText("Available Sets For Partition:");

        L1CommentFieldLabel.setText("Comment:");

        SetNameLabel.setText("Prescale Set Name:");

        SetName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SetNameActionPerformed(evt);
            }
        });

        CommentTextField.setColumns(20);
        CommentTextField.setLineWrap(true);
        CommentTextField.setRows(5);
        CommentTextField.setAutoscrolls(false);
        CommentPane.setViewportView(CommentTextField);

        UpdateCommentButton.setText("Update Comment");
        UpdateCommentButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UpdateCommentButtonActionPerformed(evt);
            }
        });

        PrescaleSetListContents.setModel(L1itemsUnUsed);
        PrescaleSetListPane.setViewportView(PrescaleSetListContents);

        ItemNameFilterPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Item name filtering"));

        NameFilterText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                NameFilterTextKeyReleased(evt);
            }
        });

        TypeSelector.setModel(new javax.swing.DefaultComboBoxModel<String>(new String[] { "All Types" }));
        TypeSelector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TypeSelectorActionPerformed(evt);
            }
        });

        NameFilterLabel.setText("Item name:");

        TypeSelectorLabel.setText("Trigger type:");

        FilterResetButton.setText("Reset Filter");
        FilterResetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FilterResetButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout ItemNameFilterPanelLayout = new javax.swing.GroupLayout(ItemNameFilterPanel);
        ItemNameFilterPanel.setLayout(ItemNameFilterPanelLayout);
        ItemNameFilterPanelLayout.setHorizontalGroup(
            ItemNameFilterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ItemNameFilterPanelLayout.createSequentialGroup()
                .addGroup(ItemNameFilterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ItemNameFilterPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(ItemNameFilterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(TypeSelectorLabel)
                            .addComponent(NameFilterLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(ItemNameFilterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(NameFilterText)
                            .addComponent(TypeSelector, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(FilterResetButton, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        ItemNameFilterPanelLayout.setVerticalGroup(
            ItemNameFilterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ItemNameFilterPanelLayout.createSequentialGroup()
                .addGroup(ItemNameFilterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(NameFilterLabel)
                    .addComponent(NameFilterText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ItemNameFilterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TypeSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TypeSelectorLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FilterResetButton))
        );

        CompareSetPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Compare sets"));

        PrescaleSet2DiffSelector.setModel(new javax.swing.DefaultComboBoxModel<Object>(new String[] { "Prescale Set 1" }));
        PrescaleSet2DiffSelector.setMaximumSize(new java.awt.Dimension(93, 20));
        PrescaleSet2DiffSelector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PrescaleSet2DiffActionPerformed(evt);
            }
        });

        PrescaleSet1DiffSelector.setModel(new javax.swing.DefaultComboBoxModel<Object>(new String[] { "Prescale Set 2" }));
        PrescaleSet1DiffSelector.setMaximumSize(new java.awt.Dimension(93, 20));
        PrescaleSet1DiffSelector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PrescaleSet1DiffActionPerformed(evt);
            }
        });

        PrescaleSetDoDiffButton.setText("Diff Selected Sets");
        PrescaleSetDoDiffButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PrescaleSetDoDiffButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout CompareSetPanelLayout = new javax.swing.GroupLayout(CompareSetPanel);
        CompareSetPanel.setLayout(CompareSetPanelLayout);
        CompareSetPanelLayout.setHorizontalGroup(
            CompareSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CompareSetPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(CompareSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PrescaleSet2DiffSelector, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PrescaleSet1DiffSelector, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PrescaleSetDoDiffButton, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        CompareSetPanelLayout.setVerticalGroup(
            CompareSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CompareSetPanelLayout.createSequentialGroup()
                .addComponent(PrescaleSet2DiffSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PrescaleSet1DiffSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PrescaleSetDoDiffButton)
                .addGap(17, 17, 17))
        );

        ViewHiddenSetsToggleButton.setText("See hidden sets");
        ViewHiddenSetsToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ViewHiddenSetsToggleButtonActionPerformed(evt);
            }
        });

        HideSetsToggleButton.setText("Hide");
        HideSetsToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                HideSetsToggleButtonActionPerformed(evt);
            }
        });

        MultiplePrescalesSelectBox.setText("Multiple Prescales");
        MultiplePrescalesSelectBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MultiplePrescalesSelectBoxActionPerformed(evt);
            }
        });

        CommentAllButton.setText("Update all");
        CommentAllButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CommentAllButtonActionPerformed(evt);
            }
        });

        XMLLoadButton.setText("Load Prescales XML");
        XMLLoadButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                XMLLoadButtonMouseClicked(evt);
            }
        });

        FileSystemXMLBrowseButton.setText("Browse");
        FileSystemXMLBrowseButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                FileSystemXMLBrowseButtonMouseClicked(evt);
            }
        });

        PrescaleSetChangePartitionLabel.setText("Prescale Set Partition:");

        PartitionChangeSelector.setFont(new java.awt.Font("Ubuntu", 0, 10)); // NOI18N
        PartitionChangeSelector.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "1", "2", "3" }));
        PartitionChangeSelector.setToolTipText("select partition");
        PartitionChangeSelector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PartitionChangeSelectorActionPerformed(evt);
            }
        });

        RefreshSetsButton.setText("Refresh");
        RefreshSetsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RefreshSetsButtonActionPerformed(evt);
            }
        });

        EnableSelectionButton.setText("Enable All/Selection");
        EnableSelectionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EnableSelectionButtonActionPerformed(evt);
            }
        });

        DisableSelectionButton.setText("Disable All/Selection");
        DisableSelectionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DisableSelectionButtonActionPerformed(evt);
            }
        });

        L1SaveButton.setText("Save");
        L1SaveButton.setEnabled(false);
        L1SaveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                L1SaveButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout L1PrescalePanelLayout = new javax.swing.GroupLayout(L1PrescalePanel);
        L1PrescalePanel.setLayout(L1PrescalePanelLayout);
        L1PrescalePanelLayout.setHorizontalGroup(
            L1PrescalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(L1PrescalePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(L1PrescalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(L1PrescalePanelLayout.createSequentialGroup()
                        .addComponent(CommentAllButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(UpdateCommentButton))
                    .addComponent(CommentPane, javax.swing.GroupLayout.DEFAULT_SIZE, 314, Short.MAX_VALUE)
                    .addComponent(PrescaleSetListPane)
                    .addGroup(L1PrescalePanelLayout.createSequentialGroup()
                        .addGroup(L1PrescalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(L1CommentFieldLabel)
                            .addGroup(L1PrescalePanelLayout.createSequentialGroup()
                                .addComponent(PartitionFilterLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(PartitionFilterSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, L1PrescalePanelLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(XMLLoadButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(FileSystemXMLBrowseButton))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, L1PrescalePanelLayout.createSequentialGroup()
                        .addComponent(RefreshSetsButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(HideSetsToggleButton, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ViewHiddenSetsToggleButton, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(L1PrescalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(L1PrescalePanelLayout.createSequentialGroup()
                        .addComponent(ItemNameFilterPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CompareSetPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(PrescaleListPane, javax.swing.GroupLayout.DEFAULT_SIZE, 898, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, L1PrescalePanelLayout.createSequentialGroup()
                        .addComponent(SetNameLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(SetName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(PrescaleSetChangePartitionLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(PartitionChangeSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(L1PrescalePanelLayout.createSequentialGroup()
                        .addComponent(MultiplePrescalesSelectBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(EnableSelectionButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(DisableSelectionButton))
                    .addGroup(L1PrescalePanelLayout.createSequentialGroup()
                        .addComponent(XMLpath)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(L1SaveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        L1PrescalePanelLayout.setVerticalGroup(
            L1PrescalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(L1PrescalePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(L1PrescalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(L1PrescalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(SetNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(SetName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(PrescaleSetChangePartitionLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(PartitionChangeSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(L1PrescalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(PartitionFilterLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(PartitionFilterSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(L1PrescalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(L1PrescalePanelLayout.createSequentialGroup()
                        .addGroup(L1PrescalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(CompareSetPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ItemNameFilterPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(PrescaleListPane, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addGroup(L1PrescalePanelLayout.createSequentialGroup()
                        .addGroup(L1PrescalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(HideSetsToggleButton, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ViewHiddenSetsToggleButton, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(RefreshSetsButton, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(PrescaleSetListPane, javax.swing.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(L1CommentFieldLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CommentPane, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(L1PrescalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, L1PrescalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(MultiplePrescalesSelectBox, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(DisableSelectionButton)
                        .addComponent(EnableSelectionButton))
                    .addGroup(L1PrescalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(UpdateCommentButton, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(CommentAllButton, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(L1PrescalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(XMLLoadButton)
                    .addComponent(FileSystemXMLBrowseButton)
                    .addComponent(L1SaveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(XMLpath, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(L1PrescalePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 1236, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(L1PrescalePanel, javax.swing.GroupLayout.PREFERRED_SIZE, 655, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void PartitionFilterSelectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PartitionFilterSelectorActionPerformed
        DoPartitionActionPerformedOnComboBox();
    }//GEN-LAST:event_PartitionFilterSelectorActionPerformed

    private void tableDetailsL1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tableDetailsL1FocusGained
        if (evt.getSource() == tableDetailsL1) {
            if (userMode.ordinal() >= UserMode.SHIFTER.ordinal() && isEditable) {
                L1SaveButton.setEnabled(true);
            }
        }
    }//GEN-LAST:event_tableDetailsL1FocusGained

    private void L1SaveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_L1SaveButtonActionPerformed
        try {
            DoL1ActionPerformed();
            L1SaveButton.setEnabled(false);
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while performing L1 action in Prescale Edit panel.");
        }
    }//GEN-LAST:event_L1SaveButtonActionPerformed

    private void SetNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SetNameActionPerformed
        if (userMode.ordinal() >= UserMode.SHIFTER.ordinal() && isEditable) {
            L1SaveButton.setEnabled(true);
        }
    }//GEN-LAST:event_SetNameActionPerformed

    private void EnableSelectionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EnableSelectionButtonActionPerformed
        int length = tableDetailsL1.getSelectedRows().length;
        int[] Rows = tableDetailsL1.getSelectedRows();
        TableModel model = tableDetailsL1.getModel();
        int length2 = model.getRowCount();
        L1MainModel.setFlag(1);
        if (length == 0) {
            for (int i = 0; i < length2; ++i) {
                model.setValueAt(true, i, L1PrescaleTableModel.COLBOOL);
            }
        } else if (length > 0) {
            for (int i = Rows[0]; i < Rows[0] + length; ++i) {
                model.setValueAt(true, i, L1PrescaleTableModel.COLBOOL);
            }
            tableDetailsL1.clearSelection();
        }
        L1MainModel.setFlag(0);
        L1MainModel.fireTableDataChanged();
        if (userMode.ordinal() >= UserMode.SHIFTER.ordinal() && isEditable) {
            L1SaveButton.setEnabled(true);
        }
    }//GEN-LAST:event_EnableSelectionButtonActionPerformed

    private void DisableSelectionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DisableSelectionButtonActionPerformed
        int length = tableDetailsL1.getSelectedRows().length;
        int[] Rows = tableDetailsL1.getSelectedRows();
        TableModel model = tableDetailsL1.getModel();
        int length2 = model.getRowCount();
        L1MainModel.setFlag(1);
        if (length == 0) {
            for (int i = 0; i < length2; ++i) {
                model.setValueAt(false, i, L1PrescaleTableModel.COLBOOL);
            }
        } else if (length > 0) {
            for (int i = Rows[0]; i < Rows[0] + length; ++i) {
                model.setValueAt(false, i, L1PrescaleTableModel.COLBOOL);
            }
            tableDetailsL1.clearSelection();
        }
        L1MainModel.setFlag(0);
        L1MainModel.fireTableDataChanged();
        if (userMode.ordinal() >= UserMode.SHIFTER.ordinal() && isEditable) {
            L1SaveButton.setEnabled(true);
        }
    }//GEN-LAST:event_DisableSelectionButtonActionPerformed

    private void UpdateCommentButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UpdateCommentButtonActionPerformed
        // update the L1 comment here! direct to db.
        String comment = CommentTextField.getText();
        try {
            L1Comment(comment, -1);
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while performing L1 comment action in PrescaleEdit panel.");
        }
    }//GEN-LAST:event_UpdateCommentButtonActionPerformed

    private void NameFilterTextKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NameFilterTextKeyReleased

        try {
            DoFilterText1OnKeyReleased();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while filtering text after key pressed in PrescaleEdit panel.");
        }
    }//GEN-LAST:event_NameFilterTextKeyReleased

    private void TypeSelectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TypeSelectorActionPerformed
        try {
            DoTypesActionPerformedOnComboBox();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while action was performed in combobox of types in PrescaleEdit panel.");
        }
    }//GEN-LAST:event_TypeSelectorActionPerformed

    private void FilterResetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FilterResetButtonActionPerformed
        try {
            resetL1();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while resetting L1 in PrescaleEdit panel.");
        }
    }//GEN-LAST:event_FilterResetButtonActionPerformed

    private void PrescaleSetDoDiffButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PrescaleSetDoDiffButtonActionPerformed
        L1DiffAction();
    }//GEN-LAST:event_PrescaleSetDoDiffButtonActionPerformed

    private void L1DiffAction() {
        if (PrescaleSetDoDiffButton.isSelected() == true) {
            filterHold = L1Filter;
            TypeSelector.setEnabled(false);
        } else {
            TypeSelector.setEnabled(true);
        }
        boolean filter = false;
        if (!L1Filter.isEmpty()) {
            filter = true;
        }
        try {
            updateDiffTable(filter);
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while toggling between L1 diff in PrescaleEdit panel.");
        }
    }

    private void ViewHiddenSetsToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ViewHiddenSetsToggleButtonActionPerformed
   
        if (ViewHiddenSetsToggleButton.isSelected()) {
            show_hidden_l1 = true;
            HideSetsToggleButton.setText("Show");
        } else {
            show_hidden_l1 = false;
            HideSetsToggleButton.setText("Hide");
        }
        refreshPanel();
    }//GEN-LAST:event_ViewHiddenSetsToggleButtonActionPerformed

    private void refreshPanel() {
        try {
            this.l1m.clear();
            // Disble valueChanged before retrieving the new PrescaleSets. Avoid
            // repeated refreshes.
            PrescaleSetListContents.setValueIsAdjusting(true);
            getL1Sets(getPartitionFilterSelectorValue());
            getTypes();
            if (PrescaleSetDoDiffButton.isSelected() == false) {
                PrescaleSetListContents.setValueIsAdjusting(false);
                if (L1itemsUnUsed.size() > 0) {
                    PrescaleSetListContents.setSelectedIndex(0);
                }
            }
            L1DiffAction();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while Toggling L1Hide in PrescaleEdit panel.");
        }
    }

    private void HideSetsToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_HideSetsToggleButtonActionPerformed
        try {
            DoL1HideActionPerformed();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while L1Hide action performed after button pressed in PrescaleEdit panel.");
        }
    }//GEN-LAST:event_HideSetsToggleButtonActionPerformed

    private void MultiplePrescalesSelectBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MultiplePrescalesSelectBoxActionPerformed
        if (MultiplePrescalesSelectBox.isSelected()) {
            try {
                EnableSelectionButton.setEnabled(false);
                DisableSelectionButton.setEnabled(false);
                PrescaleSetListContents.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
                this.actionL1PrescalesList();
            } catch (SQLException ex) {
                TTExceptionHandler.HandleSqlException(ex, this, "Error while MultiplePrescale checkbox pressed in PrescaleEdit panel.");
            }
        } else {
            EnableSelectionButton.setEnabled(true);
            DisableSelectionButton.setEnabled(true);
            PrescaleSetListContents.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            this.recreatePrescalesJTable();
        }
    }//GEN-LAST:event_MultiplePrescalesSelectBoxActionPerformed

    private void CommentAllButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CommentAllButtonActionPerformed
        String comment = CommentTextField.getText();
        Object[] sel = this.PrescaleSetListContents.getSelectedValues();
        for (Object object : sel) {
            try {
                L1Prescale l1ps = new L1Prescale(getIDFromInfo((String) object));

                int id = l1ps.get_id();
                // update the L1 comment in db.
                L1Comment(comment, id);
                // update the comment in the memory object
                l1ps.set_comment(comment);
                // after doing the objects changed, therefore
            } catch (SQLException ex) {
                TTExceptionHandler.HandleSqlException(ex, this, "Error while performing CommentAll action after button pressed in PrescaleEdit panel.");
            }
        }
    }//GEN-LAST:event_CommentAllButtonActionPerformed

    private void XMLLoadButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_XMLLoadButtonMouseClicked
        this.HandleXMLLoadMouseButtonClick();
    }//GEN-LAST:event_XMLLoadButtonMouseClicked

    private void PrescaleSet1DiffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PrescaleSet1DiffActionPerformed
        if (PrescaleSetDoDiffButton.isSelected() == true) {
            L1DiffAction();
        }
    }//GEN-LAST:event_PrescaleSet1DiffActionPerformed

    private void PrescaleSet2DiffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PrescaleSet2DiffActionPerformed
        if (PrescaleSetDoDiffButton.isSelected() == true) {
            L1DiffAction();
        }
    }//GEN-LAST:event_PrescaleSet2DiffActionPerformed

    private void FileSystemXMLBrowseButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_FileSystemXMLBrowseButtonMouseClicked
        int returnVal = fc.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = fc.getSelectedFile();
            XMLpath.setText(f.toString());
        }
    }//GEN-LAST:event_FileSystemXMLBrowseButtonMouseClicked

    private void PartitionChangeSelectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PartitionChangeSelectorActionPerformed
        PSSPartition = PartitionChangeSelector.getSelectedIndex();
        this.changePrescaleSetPartitionActionPerformed();
    }//GEN-LAST:event_PartitionChangeSelectorActionPerformed

    private void RefreshSetsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RefreshSetsButtonActionPerformed
        refreshPanel();
    }//GEN-LAST:event_RefreshSetsButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton CommentAllButton;
    private javax.swing.JScrollPane CommentPane;
    private javax.swing.JTextArea CommentTextField;
    private javax.swing.JPanel CompareSetPanel;
    private javax.swing.JButton DisableSelectionButton;
    private javax.swing.JButton EnableSelectionButton;
    private javax.swing.JButton FileSystemXMLBrowseButton;
    private javax.swing.JButton FilterResetButton;
    private javax.swing.JButton HideSetsToggleButton;
    private javax.swing.JPanel ItemNameFilterPanel;
    private javax.swing.JLabel L1CommentFieldLabel;
    private javax.swing.JPanel L1PrescalePanel;
    private javax.swing.JButton L1SaveButton;
    private javax.swing.JCheckBox MultiplePrescalesSelectBox;
    private javax.swing.JLabel NameFilterLabel;
    private javax.swing.JTextField NameFilterText;
    private javax.swing.JComboBox PartitionChangeSelector;
    private javax.swing.JLabel PartitionFilterLabel;
    private javax.swing.JComboBox PartitionFilterSelector;
    private javax.swing.JScrollPane PrescaleListPane;
    private javax.swing.JComboBox<Object> PrescaleSet1DiffSelector;
    private javax.swing.JComboBox<Object> PrescaleSet2DiffSelector;
    private javax.swing.JLabel PrescaleSetChangePartitionLabel;
    private javax.swing.JToggleButton PrescaleSetDoDiffButton;
    private javax.swing.JList<String> PrescaleSetListContents;
    private javax.swing.JScrollPane PrescaleSetListPane;
    private javax.swing.JButton RefreshSetsButton;
    private javax.swing.JTextField SetName;
    private javax.swing.JLabel SetNameLabel;
    private javax.swing.JComboBox<String> TypeSelector;
    private javax.swing.JLabel TypeSelectorLabel;
    private javax.swing.JButton UpdateCommentButton;
    private javax.swing.JToggleButton ViewHiddenSetsToggleButton;
    private javax.swing.JButton XMLLoadButton;
    private javax.swing.JTextField XMLpath;
    private javax.swing.JTable tableDetailsL1;
    // End of variables declaration//GEN-END:variables
}
