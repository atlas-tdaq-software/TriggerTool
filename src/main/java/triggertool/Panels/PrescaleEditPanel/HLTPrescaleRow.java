/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package triggertool.Panels.PrescaleEditPanel;

import java.util.ArrayList;
import java.util.List;
import triggerdb.Entities.HLT.HLTPrescale;
import triggerdb.Entities.HLT.HLTTriggerChain;

/**
 * Container of a row in the HLTPrescaleEditGUI table. It contains the prescale
 * information of one chain: 0 name 1 seed 2 counter 3 l2 or ef 4 rerun 5 bool
 * in out 6 ps 7 pt 8 express stream ps
 *
 * @author tiago
 */
public final class HLTPrescaleRow {

    /**
     *
     */
    public static final int NumberoOfColumns = 9;
    //
    // COLUMN INDEX CONSTANTS
    /**
     * Chain name.
     */
    public static final int NAME = 0;
    /**
     * Name of the seed of this chain.
     */
    public static final int SEED = 1;
    /**
     * An integers representing the chain counter.
     */
    public static final int COUNTER = 2;
    /**
     * Boolean value enabled/disabled chain.
     */
    public static final int IN_OUT = 3;
    /**
     * The prescale value.
     */
    public static final int PS = 4;
    /**
     * The Pass-through value.
     */
    public static final int PT = 5;
    /**
     * The Express Stream value.
     */
    public static final int STREAM = 6;
    /**
     * The rerun value of this chain.
     */
    public static final int CONDITION = 7;
    /**
     * The rerun value of this chain.
     */
    public static final int RERUN = 8;

    //
    // Data fields (Columns) of one row.
    /**
     * The name of this trigger chain.
     */
    private String name;
    /**
     * The name of the chain this item seeds form.
     */
    private String seed;
    /**
     * the chain counter.
     */
    private Integer counter;
    /**
     * The rerun value.
     */
    private String condition;
    /**
     * Active or inactive (ps value greater or less than 0).
     */
    private Boolean inOut;
    /**
     * The prescale value.
     */
    private String ps;
    /**
     * The pass-through value.
     */
    private String pt;
    /**
     * The express stream value.
     */
    private String stream;
    /**
     * The rerun values and conditions concatenated in a string.
     */
    private String reRun;

    /**
     *
     */
    public List<HLTPrescale> allPrescaleList;

    /**
     *
     */
    public List<HLTPrescale> reRunList;

    /**
     *
     * @param counter
     * @param chain
     * @param list
     */
    public HLTPrescaleRow(Integer counter, HLTTriggerChain chain, List<HLTPrescale> list) {
        allPrescaleList = list;

        setName(chain.get_name());
        setSeed(chain.get_lower_chain_name());
        setCounter(counter);

        reRunList = new ArrayList<>();
        String reRunString = "";
        for (HLTPrescale p : list) {
            if (null != p.get_type()) switch (p.get_type()) {
                case Prescale:
                    setPrescale(fmt(p.get_value()));
                    break;
                case Pass_Through:
                    setPassThrough(fmt(p.get_value()));
                    break;
                case Stream:
                    /* if (p.get_value() == 0) {
                    setStream("na");
                    } else {*/
                    setStream(fmt(p.get_value()));
                    //}
                    if (p.get_condition().equals("~")) {
                        setCondition("na");
                    } else {
                        setCondition(p.get_condition());
                    }   break;
                case ReRun:
                    reRunList.add(p);
                    reRunString += fmt(p.get_value()) + ":" + p.get_condition() + ";";
                    break;
                default:
                    break;
            }
        }
        if (getStream() == null) {
            setStream("na");
            setCondition("na");
        }

        setReRun(reRunString);
    }

    /**
     * Creates and return a Vector of objects to easily set the values in the
     * HLTTableModel.
     *
     * @return a vector containing all the values in the right order needed by
     * the HLTTableModel.
     */
    public ArrayList<Object> getDataVector() {
        ArrayList<Object> dataVector = new ArrayList<>();
        dataVector.add(name);
        dataVector.add(seed);
        dataVector.add(counter);
        dataVector.add(condition);
        dataVector.add(inOut);
        dataVector.add(ps);
        dataVector.add(pt);
        dataVector.add(stream);
        dataVector.add(reRun);
        return dataVector;
    }

    /**
     * Method access the data by "column" number.
     *
     * @param idx the col number.
     * @return the object at column idx.
     */
    public Object get(final int idx) {
        switch (idx) {
            case NAME:
                return this.name;
            case SEED:
                return this.seed;
            case COUNTER:
                return this.counter;
            case CONDITION:
                return this.condition;
            case IN_OUT:
                return this.inOut;
            case PS:
                return this.ps;
            case PT:
                return this.pt;
            case STREAM:
                return this.stream;
            case RERUN:
                return this.reRun;
            default:
                return null;
        }
    }

    /**
     * Method set the data by "column" number.
     *
     * @param obj the value to set
     * @param idx the col number
     */
    public void setElementAt(final Object obj, final int idx) {
        switch (idx) {
            case IN_OUT:
                this.inOut = (Boolean) obj;
                break;
            case NAME:
                this.name = obj.toString();
                break;
            case SEED:
                this.seed = obj.toString();
                break;
            case COUNTER:
                this.counter = (Integer) obj;
                break;
            case CONDITION:
                setCondition(obj.toString());
                break;
            case PS:
                setPrescale(obj.toString());
                break;
            case PT:
                setPassThrough(obj.toString());
                break;
            case STREAM:
                setStream(obj.toString());
                break;
            case RERUN:
                setReRun(obj.toString());
            default:
                break;
        }
    }

    /**
     *
     * @param condition
     */
    public void setCondition(final String condition) {
        if (this.condition == null || (!this.condition.trim().equalsIgnoreCase("na") && !this.condition.trim().equalsIgnoreCase("express"))) {
            this.condition = condition;
        }
    }

    /**
     * Get the chain counter.
     *
     * @return the chain counter.
     */
    public Integer getCounter() {
        return this.counter;
    }

    /**
     * Set the chain counter.
     *
     * @param newCounter the new chain counter.
     */
    public void setCounter(final Integer newCounter) {
        this.counter = newCounter;
    }

    /**
     * Gets the In/Out value.
     *
     * @return the <code>boolean</code> In/Out value.
     */
    public Boolean getInOut() {
        return this.inOut;
    }

    /**
     * Sets the In/Out value.
     *
     * @param inOutIn the in/out value.
     */
    public void setInOut(final Boolean inOutIn) {
        this.inOut = inOutIn;
    }

    /**
     * Get the name of this chain.
     *
     * @return the name of this chain.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Set the name of this chain.
     *
     * @param newName the name of this chain.
     */
    public void setName(final String newName) {
        this.name = newName;
    }

    /**
     * Get the prescale value.
     *
     * @return the prescale value.
     */
    public String getPrescale() {
        return this.ps;
    }

    /**
     * Get the pass-through value.
     *
     * @return the pass-through value.
     */
    public String getPassThrough() {
        return this.pt;
    }

    /**
     * Get the stream value.
     *
     * @return the stream value.
     */
    public String getStream() {
        return this.stream;
    }

    /**
     * Set the prescale value. Non numeric values are converted to "-1".
     *
     * @param ps the new prescale value.
     */
    public void setPrescale(final String ps) {
        try {

            this.ps = ps;
            this.inOut = Double.parseDouble(ps) >= 0;
        } catch (NumberFormatException e) {
            this.ps = "1.0";
            this.inOut = false;
        }
    }

    /**
     * Set the pass-through value. Non numeric values are converted to "-1".
     *
     * @param ps the new pass-through value.
     */
    public void setPassThrough(final String ps) {
        try {
            this.pt = ps;
        } catch (NumberFormatException e) {
            this.pt = "1.0";
        }
    }

    /**
     * Set the stream value. Non numeric values are converted to "-1".
     *
     * @param ps the new stream value.
     */
    public void setStream(final String ps) {
        try {
            if (stream == null || (!stream.trim().equalsIgnoreCase("na") && ps.matches("[0-9]+"))) {
                this.stream = ps;
            }
        } catch (NumberFormatException e) {
            this.stream = "1.0";
        }
    }

    /**
     * Gets the rerun value.
     *
     * @return the rerun vaue.
     */
    public String getCondition() {
        return this.condition;
    }

    /**
     * Gets the name of the seed of this chain.
     *
     * @return the name of the chain seeding this.
     */
    public String getSeed() {
        return this.seed;
    }

    /**
     * Sets the name of the seed of this chain.
     *
     * @param seedName the name of the seed to this chain.
     */
    public void setSeed(final String seedName) {
        this.seed = seedName;
    }

    /**
     *
     * @param s
     */
    public void setReRun(final String s) {
        this.reRun = s;
    }

    /**
     *
     * @return
     */
    public String getReRun() {
        return reRun;
    }

    /**
     *
     */
    public void ReCalculateReRunField() {
        String reRunString = "";
        for (HLTPrescale p : reRunList) {
            reRunString += fmt(p.get_value()) + ":" + p.get_condition() + ";";
        }

        reRun = reRunString;
    }

    /**
     *
     * @param d
     * @return
     */
    public static String fmt(double d) {
        if (d == (long) d) {
            return String.format("%d", (long) d);
        } else {
            return String.format("%s", d);
        }
    }
}
