/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.Panels.PrescaleEditPanel;

import triggerdb.Entities.HLT.HLTPrescale;

/**
 *
 * @author Michele
 */
public class HLTPrescaleReRunRow {    
    
    private final int ValuePosition = 0;
    private final int ConditionPosition = 1;
    
    private final HLTPrescale prescale;
    private final HLTPrescaleRow mainTableRow;
    
    HLTPrescaleReRunRow(HLTPrescale p, HLTPrescaleRow mainTableRow) {
        prescale = p;
        this.mainTableRow = mainTableRow;
    }
    
        /**
     * Method access the data by "column" number.
     *
     * @param idx the col number.
     * @return the object at column idx.
     */
    public Object get(final int idx) {
        switch (idx) {
            case ValuePosition:
                return fmt(prescale.get_value());
            case ConditionPosition:
                return prescale.get_condition();
            default:
                return null;
        }
    }
        /**
     * Method set the data by "column" number.
     *
     * @param obj the value to set
     * @param idx the col number
     */
    public void setElementAt(final Object obj, final int idx) {
        switch (idx) {
            case ValuePosition:
                prescale.set_value(Double.valueOf(obj.toString()));
                break;
            case ConditionPosition:
                prescale.set_condition(obj.toString());
                break;
            default:
                break;
        }
        
        mainTableRow.ReCalculateReRunField();
    }

    /**
     *
     * @param d
     * @return
     */
    public static String fmt(double d)
    {
        if(d == (long) d)
            return String.format("%d",(long)d);
        else
            return String.format("%s",d);
    }
}
