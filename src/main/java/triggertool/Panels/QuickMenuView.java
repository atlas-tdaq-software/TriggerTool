/*
 * QuickMenuView.java
 *
 * Created on February 25, 2008, 7:09 PM
 * Modified: on May 5, 2010, 14:09
 *          14-07-2011 tperez   speed up scrolling. Print prescales left of the trigger map.
 * 
 */
package triggertool.Panels;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggertool.Auxiliary.DrawingTriggerItem;

/**
 * Displays a tree with the L1 trigger items and HLT trigger chains form a given
 * super master key.
 * Uses minimum DB access, so should be a lot faster than getting
 * the full information from the DB.
 * 
 * @author Simon Head
 * @author Tiago Perez <tiago.perez@desy.de>
 */
public class QuickMenuView extends javax.swing.JPanel {

    /** Message Log */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    /** horizontal spacing. */
    private static final int XSPACING = 20;
    /** line spacing. */
    private static final int LINEHEIGTH = 15;
    /** Prescales Column Width. */
    public static final int COLWIDTH = 157;
    /** X position of the HLT Prescale Set list. */
    public static final int XREF_HLT = COLWIDTH + 2 * XSPACING;
    /** X Position of the Trigger Map. */
    public static final int XREF_MAP = XREF_HLT + COLWIDTH;
    /** Font Size. */
    private static final int FONTSIZE = 11;
    /** Prescale Title Font. */
    private static final Font PSTITLEFONT = new Font("Arial", Font.BOLD, FONTSIZE + 2);
    /** Prescale Font. */
    public static final Font PSFONT = new Font("Arial", Font.PLAIN, FONTSIZE);
    /** 
     * Vector of L1 Items.  L1 Items contain links to L2 chains and those 
     * contain links to EF chains.
     */
    private Vector<DrawingTriggerItem> items = new Vector<>();
    private Vector<String> l1PrescaleNames = new Vector<>();
    private Vector<String> hltPrescaleNames = new Vector<>();

    /** Creates new form QuickMenuView.  Form will be empty. */
    public QuickMenuView() {
        initComponents();
        this.setFont(PSFONT);

    }

    /**
     * Supply the super master key.  We then query the DB three times.  Once to
     * get the L1 Items, then L2 chains and finally EF chains.
     *
     * @param smt ID of the super master record that we want to display.
     */
    public final void drawSuperMaster(final SuperMasterTable smt) {
        items.clear();
        l1PrescaleNames.clear();
        hltPrescaleNames.clear();


        Vector<DrawingTriggerItem> itemsL2 = this.getL2Items(smt);
        // Create with L1 items.
        Vector<DrawingTriggerItem> itemsL1 = this.getL1Items(smt);
        // add L2 items to L1 items
        itemsL1 = this.match(itemsL1, itemsL2);

        this.items = itemsL1;

        //sort out dimensions
        int h = 10;
        for (DrawingTriggerItem obj : itemsL1) {
            obj.setX(10);
            obj.setY(h);
            h += obj.getHeight();
        }

        if (h != getPreferredSize().getHeight()) {
            setSize(new Dimension(800, h));
            setPreferredSize(new Dimension(800, h));
        }

        this.l1PrescaleNames = this.getL1PrescaleNames(smt);
        this.hltPrescaleNames = this.getHLTPrescaleNames(smt);

        repaint();
    }

    /**
     * Draw this to panel and display the menu for the super master key 
     * supplied.
     * 
     * @param g Graphics object.
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (items != null && items.size() > 0) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            //
            // Draw Prescales.
            int posY = 25;
            g2.setFont(PSTITLEFONT);
            g2.drawString("L1 Prescale Sets", XSPACING, posY);
            g2.drawString("HLT Prescale Sets", XREF_HLT, posY);

            // Draw L1 Prescales
            g2.setFont(PSFONT);
            posY += LINEHEIGTH;
            for (String prescale : l1PrescaleNames) {
                if (prescale.length() > 24) {
                    prescale = (prescale.substring(0, 24)) + "...";
                }
                g2.drawString(prescale, XSPACING, posY);
                posY += LINEHEIGTH;
            }

            // RESET Y pos and draw HLT Prescales.
            posY = 25 + LINEHEIGTH;
            for (String prescale : hltPrescaleNames) {
                if (prescale.length() > 24) {
                    prescale = (prescale.substring(0, 24)) + "...";
                }
                g2.drawString(prescale, XREF_HLT, posY);
                posY += LINEHEIGTH;
            }

            //calc the max width of the item name
            int max_w = 0;
            int max_w_2 = 0;
            int max_w_3 = 0;

            for (DrawingTriggerItem obj : items) {
                if (obj.getWidth(g2) > max_w) {
                    max_w = obj.getWidth(g2);
                }
                for (DrawingTriggerItem l2 : obj.getChildren()) {
                    if (l2.getWidth(g2) > max_w_2) {
                        max_w_2 = l2.getWidth(g2);
                    }
                    for (DrawingTriggerItem ef : l2.getChildren()) {
                        if (ef.getWidth(g2) > max_w_3) {
                            max_w_3 = ef.getWidth(g2);
                        }
                    }
                }
            }
            max_w += 10;
            max_w_2 += 10;
            max_w_3 += 10;
            for (DrawingTriggerItem obj : items) {
                obj.setW(max_w);

                for (DrawingTriggerItem l2 : obj.getChildren()) {
                    l2.setW(max_w_2);

                    for (DrawingTriggerItem ef : l2.getChildren()) {
                        ef.setW(max_w_3);
                    }
                }
                obj.setX(XREF_MAP);
                obj.draw(g2);
            }
        }
    }

    /**
     * Query the DB and create a vector of drawindTriggerItem for display.
     * @return the items to display.
     *
     */
    private Vector<DrawingTriggerItem> getL1Items(final SuperMasterTable smt) {
        Vector<DrawingTriggerItem> retItems = new Vector<>();
        String query = "SELECT L1TI_NAME FROM L1_TRIGGER_ITEM, L1_TM_TO_TI, L1_TRIGGER_MENU, L1_MASTER_TABLE WHERE L1TM2TI_TRIGGER_ITEM_ID=L1TI_ID AND L1TM2TI_TRIGGER_MENU_ID=L1TM_ID AND L1TM_ID=L1MT_TRIGGER_MENU_ID AND L1MT_ID=?";
        query = ConnectionManager.getInstance().fix_schema_name(query);
        try {
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ps.setInt(1, smt.get_l1_master_table_id());
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {
                DrawingTriggerItem obj = new DrawingTriggerItem(rset.getString("L1TI_NAME"));
                retItems.add(obj);
            }
            rset.close();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return retItems;
    }

    /**
     * Query the DB for L2Items.
     * @param smt The SuperMasterTable.
     * @return the items to display.
     */
    private Vector<DrawingTriggerItem> getL2Items(final SuperMasterTable smt) {
        Vector<DrawingTriggerItem> retItems = new Vector<>();
        String query = "SELECT HTC_NAME, HTC_LOWER_CHAIN_NAME FROM "
                + "HLT_TRIGGER_CHAIN, HLT_TM_TO_TC, HLT_TRIGGER_MENU, "
                + "HLT_MASTER_TABLE "
                + "WHERE HTM2TC_TRIGGER_CHAIN_ID=HTC_ID AND "
                + "HTM2TC_TRIGGER_MENU_ID=HTM_ID AND "
                + "HTM_ID=HMT_TRIGGER_MENU_ID AND "
                + "HMT_ID=? ";
        query = ConnectionManager.getInstance().fix_schema_name(query);
        // Launch the QUERY and put the values into the map.
        try {
            Connection con = ConnectionManager.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, smt.get_hlt_master_table_id());
            ResultSet rset = ps.executeQuery();
            int counter = 0;
            while (rset.next()) {
                String itemName = rset.getString("HTC_NAME").trim();
                DrawingTriggerItem item = new DrawingTriggerItem(itemName);
                item.setLowerName(rset.getString("HTC_LOWER_CHAIN_NAME").trim());
                retItems.add(item);
                counter++;
            }
            rset.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return retItems;
    }

    /**
     * Queries DB to get a list of EF trigger items.
     * @param smt super master table.
     * @return ef trigger items belonging to this SMT.
     */
    private Vector<DrawingTriggerItem> getEfItems(final SuperMasterTable smt) {
        ConnectionManager cmngr = ConnectionManager.getInstance();
        Vector<DrawingTriggerItem> efItems = new Vector<>();
        String query = "SELECT HTC_NAME, HTC_LOWER_CHAIN_NAME FROM "
                + "HLT_TRIGGER_CHAIN, HLT_TM_TO_TC, HLT_TRIGGER_MENU, "
                + "HLT_MASTER_TABLE "
                + "WHERE HTM2TC_TRIGGER_CHAIN_ID=HTC_ID AND "
                + "HTM2TC_TRIGGER_MENU_ID=HTM_ID AND "
                + "HTM_ID=HMT_TRIGGER_MENU_ID AND "
                + "HMT_ID=? ";
        query = cmngr.fix_schema_name(query);
        try {
            PreparedStatement ps = null;
            ps = cmngr.getConnection().prepareStatement(query);
            ps.setInt(1, smt.get_hlt_master_table_id());
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {
                String name = rset.getString("HTC_NAME").trim();
                String lowerName = rset.getString("HTC_LOWER_CHAIN_NAME").trim();
                if (lowerName.contains(",")) {
                    logger.log(Level.WARNING," EF Chain {0}" + " has more than one "
                            + "feed. Displaying only the first one.", name);
                    StringTokenizer tkzer = new StringTokenizer(lowerName, ",");
                    lowerName = tkzer.nextToken().trim();
                }

                DrawingTriggerItem efItem = new DrawingTriggerItem(name);
                efItem.setLowerName(lowerName);
                efItems.add(efItem);
            }
            rset.close();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return efItems;
    }

    /**
     * Loops on itemsLow and Hi and check the names. Adds every itemHi to its
     * corresponding father in itemLo
     * @param itemsLow lowe DrawingItem chain item vector (L1 or L2)
     * @param itemsHi higher DrawingTriggerItem vector (L2 or EF)
     * @return the low vector with all its children set.
     */
    private Vector<DrawingTriggerItem> match(final Vector<DrawingTriggerItem> itemsLow, final Vector<DrawingTriggerItem> itemsHi) {
        for (DrawingTriggerItem l2 : itemsLow) {
            for (DrawingTriggerItem ef : itemsHi) {
                Vector<String> lowerNames = new Vector<>();
                // Multi Seeded chains.
                if (ef.getLowerName().contains(",")) {
                    StringTokenizer tkzer = new StringTokenizer(ef.getLowerName(), ",");
                    while (tkzer.hasMoreTokens()) {
                        lowerNames.add(tkzer.nextToken().trim());
                    }
                } else { //Normal chains
                    lowerNames.add(ef.getLowerName().trim());
                }
                if (lowerNames.contains(l2.getName())) {
                    l2.getChildren().add(ef);
                    if (ef.getFather() == null) {
                        ef.setFather(l2);
                    }
                }
            }
        }
        return itemsLow;
    }

    /**
     * Get the list of L1 prescale sets for the given SMT.
     * @param _smt the super master table to query.
     * @return the list of L1 prescale sets.
     */
    private Vector<String> getL1PrescaleNames(SuperMasterTable _smt) {
        Vector<String> psNames = new Vector<>();
        String query = "SELECT L1PS_ID, L1PS_NAME, L1PS_VERSION, "
                + "L1TM2PS_TRIGGER_MENU_ID, L1TM2PS_PRESCALE_SET_ID, "
                + "L1TM2PS_USED FROM "
                + "L1_PRESCALE_SET, L1_MASTER_TABLE, L1_TM_TO_PS "
                + "WHERE "
                + "L1MT_ID=? AND "
                + "L1TM2PS_USED=0 AND "
                + "L1TM2PS_TRIGGER_MENU_ID=L1MT_TRIGGER_MENU_ID AND "
                + "L1TM2PS_PRESCALE_SET_ID=L1PS_ID "
                + "ORDER BY L1PS_ID DESC";
        query = ConnectionManager.getInstance().fix_schema_name(query);

        try {
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ps.setInt(1, _smt.get_l1_master_table_id());
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {
                psNames.add(rset.getInt("L1PS_ID") + " " + rset.getString("L1PS_NAME") + " / " + rset.getInt("L1PS_VERSION"));
            }

            rset.close();
            ps.close();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return psNames;
    }

    /**
     * Get the list of HLT prescale sets for the given SMT.
     * @param _smt the super master table to query.
     * @return the list of HLT prescale sets.
     */
    private Vector<String> getHLTPrescaleNames(SuperMasterTable _smt) {
        Vector<String> psNames = new Vector<>();

        String query = "SELECT HPS_ID, HPS_NAME, HPS_VERSION, HTM2PS_TRIGGER_MENU_ID, HTM2PS_PRESCALE_SET_ID, HTM2PS_USED FROM "
                + "HLT_PRESCALE_SET, HLT_MASTER_TABLE, HLT_TM_TO_PS "
                + "WHERE "
                + "HMT_ID=? AND "
                + "HTM2PS_USED=0 AND "
                + "HTM2PS_TRIGGER_MENU_ID=HMT_TRIGGER_MENU_ID AND "
                + "HTM2PS_PRESCALE_SET_ID=HPS_ID "
                + "ORDER BY HPS_ID DESC";
        query = ConnectionManager.getInstance().fix_schema_name(query);

        try {
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ps.setInt(1, _smt.get_hlt_master_table_id());
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {
                psNames.add(rset.getInt("HPS_ID") + " " + rset.getString("HPS_NAME") + " / " + rset.getInt("HPS_VERSION"));
            }
            rset.close();
            ps.close();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return psNames;
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setFont(new java.awt.Font("Krungthep", 0, 18)); // NOI18N
        setMaximumSize(new java.awt.Dimension(200, 200));
        setMinimumSize(new java.awt.Dimension(200, 200));

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
