/*
 * BunchGroupSetPanel.java
 * Created Jun 8, 2011, 12:44:07 PM
 */
package triggertool.L1BunchGroupPanels;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import javax.swing.table.TableModel;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.L1.L1BunchGroup;
import triggerdb.Entities.L1.L1BunchGroupSet;
import triggertool.TTExceptionHandler;

/**
 * Panel to show and edit BunchGroupSet information. This is meant to be
 * embedded into the main BunchGroupEditor.
 *
 * @author tiago perez <tperez@cern.ch>
 */
public final class BunchGroupSetPanel extends javax.swing.JPanel
        implements Serializable, BunchGroupSubpanel, ListSelectionListener {

    /**
     * shift up.
     */
    private static int UP = 1;
    /**
     * shift down.
     */
    private static int DOWN = -1;
    /**
     * The logger.
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    /**
     * The sibling panel to get objects and call update.
     */
    private BunchGroupSubpanel sibling;
    private boolean changeSel = true;
    Boolean partitionLocked;
    Integer superMasterKey;
    Integer ctpPartition;
    ArrayList<Integer> activeGroups;

    /**
     * Empty constructor, needed for the Netbeans palette.
     */
    public BunchGroupSetPanel() {
        initComponents();
    }

    /**
     * Default constructor.
     *
     * @param bgs The sibling panel of this. Information is transfer via this
     * object.
     */
    public BunchGroupSetPanel(final BunchGroupSubpanel bgs) {
        partitionLocked = false;
        ctpPartition = 0;
        initPanel(bgs);
    }

    /**
     *
     * @param bgs
     * @param smk
     * @param partition
     */
    public BunchGroupSetPanel(final BunchGroupSubpanel bgs, Integer smk, Integer partition) {
        partitionLocked = true;
        superMasterKey = smk;
        ctpPartition = partition;
        initPanel(bgs);
    }

    private void initPanel(final BunchGroupSubpanel bgs) {
        initComponents();
        activeGroups = new ArrayList<>();
        if (partitionLocked) {
            partitionStatusDisplay.setText("PANEL LOCKED TO PARTITION " + ctpPartition + " for SMK " + superMasterKey + " - Note: BCRVeto Locked");
            nameTextField1.setEnabled(false);
            nameTextField1.setVisible(false);
            titleLabel1.setVisible(false);
            try {
                List<String> groups = L1BunchGroupSet.getActiveBunchGroupsInPartition(superMasterKey, ctpPartition);

                for (String group : groups) {
                    Integer enabledGroup = Integer.parseInt(group.substring(4));
                    ((BunchGroupSetTableModel) bunchGroupsInSetTable.getModel()).setCellEditable(enabledGroup, 1, true);
                    activeGroups.add(enabledGroup);
                }

                //Group Zero should never be editabe in partition locked mode:
                 ((BunchGroupSetTableModel) bunchGroupsInSetTable.getModel()).setCellEditable(0, 1, false);
            } catch (SQLException ex) {
                TTExceptionHandler.HandleSqlException(ex, this, "Error while setting up partition locked bunch group panel.");
            }

        } else {
            partitionStatusDisplay.setVisible(false);
            for(int row = 0; row < L1BunchGroupSet.MAXGROUPS; row++){
                //In partition unlocked mode all rows should be editable
                ((BunchGroupSetTableModel) bunchGroupsInSetTable.getModel()).setCellEditable(row, 1, true);
            }
        }
        this.bunchGroupsInSetTable.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        //
        this.sibling = bgs;
        //
        this.leftPanel.BunchLeftPanelInit(this, BunchLeftPanel.BUNCHGROUPSET, ctpPartition);
        this.leftPanel.setSelectedObject();
        // Add listener to grouspInSet table
        this.bunchGroupsInSetTable.getSelectionModel().addListSelectionListener(this);
        // Select first row in bunchGroupsInSetTable
        this.bunchGroupsInSetTable.setRowSelectionInterval(0, 0);
        this.selectGroupInSibling();
        this.bunchGroupsInSetTable.putClientProperty("terminateEditOnFocusLost", true);
    }

    /**
     * Save the current Group Set definition on the table to the DB. Before
     * saving it will check some condition that the bunch groups in set should
     * fulfil.
     */
    private void saveSet() throws SQLException {
        //Get selected group
        //L1BunchGroupSet set = (L1BunchGroupSet) leftPanel.getSelectedRow();
        L1BunchGroupSet set = new L1BunchGroupSet(-1);
        String bunchGrSetName = this.nameTextField.getText().trim();
        int bunchGrPartition = 0;
        if (partitionLocked) {
            bunchGrPartition = ctpPartition;
        } else {
            bunchGrPartition = Integer.parseInt(this.nameTextField1.getText().trim());
        }
        // the group list in the right table (may contain blanks
        List<AbstractMap.SimpleEntry<L1BunchGroup, String>> groupList = this.getBunchGroupsInTable();

        // the group list to save
        //List<L1BunchGroup> newGroupList = new ArrayList<L1BunchGroup>();
        List<AbstractMap.SimpleEntry<L1BunchGroup, String>> newGroupList = new ArrayList<>();
        for (AbstractMap.SimpleEntry<L1BunchGroup, String> gr : groupList) {
            if (gr != null) {
                newGroupList.add(gr);
            }
        }

        Boolean pass = true;
        if (!newGroupList.isEmpty()) {
            if (newGroupList.size() != L1BunchGroupSet.MAXGROUPS) {
                String msg = "A set must have " + L1BunchGroupSet.MAXGROUPS + " groups."
                        + "\nContinuing will cause unpopulated groups to be saved as empty?"
                        + "\nAre you sure you want to save?";
                int saveOpt = 0;
                if (partitionLocked) {
                    saveOpt = JOptionPane.YES_OPTION;
                } else {
                    saveOpt = JOptionPane.showOptionDialog(this, msg, "save bunch group set", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
                }
                if (saveOpt == JOptionPane.YES_OPTION) {
                    if (partitionLocked) {
                        msg += "\nSaving of smaller groups allowed in partition locked mode";
                    } else {
                        msg += "\nUser requested saving";
                    }
                    newGroupList.clear();
                    for (AbstractMap.SimpleEntry<L1BunchGroup, String> gr : groupList) {
                        if (gr != null) {
                            newGroupList.add(gr);
                        } else {

                            L1BunchGroup bg = new L1BunchGroup();
                            bg.set_id(bg.save());
                            newGroupList.add(new AbstractMap.SimpleEntry<>(bg, "Auto-Generated Empty"));

                        }
                    }

                } else if (saveOpt == JOptionPane.NO_OPTION) {
                    pass = false;
                    msg += "\nUser cancelled saving";
                }
                logger.warning(msg);
            }
        } else {
            // Block error.
            String msg = "Unable to save an empty bunch group set ";
            JOptionPane.showMessageDialog(this, msg, "Warning", JOptionPane.WARNING_MESSAGE);
            msg += "\nCancelling save.";
            logger.warning(msg);
            pass = false;
        }

        // check whether group list is OK.
        if (pass) {
            // Set new internal number before save.
            for (int i = 0; i < newGroupList.size(); i++) {
                newGroupList.get(i).getKey().set_internal_number(i);
            }
            // 
            int savedId = -1;
            // set id to -1 i.e. creates a new group
            set.set_id(savedId);
            set.set_partition(bunchGrPartition);
            // new name
            set.set_name(bunchGrSetName);
            set.getBunchGroups().clear();
            set.getBunchGroups().addAll(newGroupList);
            savedId = set.save();
            if (savedId > 0) {
                this.updateLeft(savedId);
            }
        }
    }

    private void fillGroupsInSet(final List<AbstractMap.SimpleEntry<L1BunchGroup, String>> groups, final JTable groupsInSetTable) {
        BunchGroupSetTableModel model = (BunchGroupSetTableModel) groupsInSetTable.getModel();
        // Clear column
        for (int i = 0; i < L1BunchGroupSet.MAXGROUPS; i++) {
            model.setValueAt(null, i, 1);
            model.setValueAt(null, i, 2);
        }

        if (groups != null) {
            // add groups to col
            for (int i = 0; i < groups.size(); i++) {
                if (activeGroups.contains(i) || !partitionLocked) {
                    if (groups.get(i) != null) {
                        L1BunchGroup gr = groups.get(i).getKey();
                        //String desc = gr.get_name() + " (DBid=" + gr.get_id() + "/v." + gr.get_version() + ")";
                        String name = groups.get(i).getValue();
                        model.setValueAt(name, i, 1);
                        model.setValueAt(gr, i, 2);
                    }
                } else {
                    model.setValueAt("--", i, 1);
                    model.setValueAt("Unavailable in Partition", i, 2);
                }
            }
        }
        groupsInSetTable.updateUI();
    }

    /**
     * Get a list with all groups in the right table.
     *
     * @return the list of groups on the right table (groups in set).
     */
    private List<AbstractMap.SimpleEntry<L1BunchGroup, String>> getBunchGroupsInTable() {
        ArrayList<AbstractMap.SimpleEntry<L1BunchGroup, String>> groups = new ArrayList<>(L1BunchGroupSet.MAXGROUPS);
        TableModel model = this.bunchGroupsInSetTable.getModel();
        // Clear colum
        for (int i = 0; i < L1BunchGroupSet.MAXGROUPS; i++) {
            // add a new slot.
            groups.add(null);
            Object rowgrp = model.getValueAt(i, 2);
            Object rowname = model.getValueAt(i, 1);
            if (rowgrp != null && rowgrp instanceof L1BunchGroup && rowname != null && rowname instanceof String) {
                L1BunchGroup gr = (L1BunchGroup) rowgrp;
                String name = (String) rowname;
                groups.set(i, new AbstractMap.SimpleEntry<>(gr, name.trim()));
            }
        }
        return groups;

    }

    /**
     * Shift the selected group by the shift value.
     *
     * @param shift the shift step: UP=1, DOWN=-1.
     */
    private void shiftGroupsInSet(final int shift) {
        JTable table = this.bunchGroupsInSetTable;

        int rowIds[] = table.getSelectedRows();
        int rowFirst = rowIds[0];
        int rowLast = rowIds[rowIds.length - 1];
        int rowFirstNext = rowFirst - shift;
        int rowLastNext = rowLast - shift;
        if (rowFirstNext > -1 && rowLastNext < L1BunchGroupSet.MAXGROUPS) {
            // get selected rows.
            List<AbstractMap.SimpleEntry<Object, Object>> selRowObj = new ArrayList<>();
            for (int ii = rowFirst; ii <= rowLast; ii++) {
                selRowObj.add(new AbstractMap.SimpleEntry<>(table.getValueAt(ii, 2), table.getValueAt(ii, 1)));
            }
            /////////////////////////
            // SHIFT UP
            /////////////////////////
            if (shift > 0) {
                // object before first, will realocated at last.
                Object rNextObj = table.getValueAt(rowFirstNext, 2);
                table.setValueAt(rNextObj, rowLast, 2);
                rNextObj = table.getValueAt(rowFirstNext, 1);
                table.setValueAt(rNextObj, rowLast, 1);

                for (int ii = 0; ii < selRowObj.size(); ii++) {
                    table.setValueAt(selRowObj.get(ii).getKey(), rowFirstNext + ii, 2);
                    table.setValueAt(selRowObj.get(ii).getValue(), rowFirstNext + ii, 1);
                }
            } else {
                /////////////////////////
                // SHIFT DOWN
                /////////////////////////
                // object after last will be realocated at first                
                Object rNextObj = table.getValueAt(rowLastNext, 2);
                table.setValueAt(rNextObj, rowFirst, 2);
                rNextObj = table.getValueAt(rowLastNext, 1);
                table.setValueAt(rNextObj, rowFirst, 1);
                for (int ii = 0; ii < selRowObj.size(); ii++) {
                    int idx = rowFirstNext + ii;
                    table.setValueAt(selRowObj.get(ii).getKey(), idx, 2);
                    table.setValueAt(selRowObj.get(ii).getValue(), idx, 1);
                }
            }
            table.setRowSelectionInterval(rowFirstNext, rowLastNext);
        }

    }

    /**
     * Listen to changes in the selection of bunchGroupInSetTable. When a new
     * row is selected, the label of the addButton will change from "Add" in
     * empty rows to "Insert" if the row is not empty. Selecting a non empty row
     * here, will select the same bunch group in the sibling panel; the
     * BunchGroupPanel.
     *
     * @param e the selection event.
     */
    @Override
    public void valueChanged(final ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
            this.changeSel = true;
        }
    }

    private void selectGroupInSibling() {
        if (changeSel) {
            int rowId = this.bunchGroupsInSetTable.getSelectedRow();
            Object row = this.bunchGroupsInSetTable.getValueAt(rowId, 2);
            if (row != null && row instanceof L1BunchGroup) {
                L1BunchGroup gr = (L1BunchGroup) row;
                this.sibling.setSelectedRow(gr.get_id());
            }
        }
        this.changeSel = false;
    }

    /**
     * Add a bunch group to the table with the groups in this set.
     *
     * @param i "+1" add, -1 to remove.
     */
    private void addGroup(final int i) throws SQLException {
        List<AbstractMap.SimpleEntry<L1BunchGroup, String>> groups = this.getBunchGroupsInTable();
        int selrow = this.bunchGroupsInSetTable.getSelectedRow();
        if(!bunchGroupsInSetTable.getModel().isCellEditable(selrow, 1)){
            return;
        }
        // ADD - INSERT
        if (i > 0) {
            L1BunchGroup selGroup = (L1BunchGroup) this.sibling.getSelectedRow();
            if (selGroup != null && selGroup.get_id() > 0) {
                L1BunchGroup addGroup = new L1BunchGroup(selGroup.get_id());
                if (selrow < groups.size()) {
                    String groupname = groups.get(selrow).getValue();
                    groups.set(selrow, new AbstractMap.SimpleEntry<>(addGroup, groupname));
                } else {
                    selrow = groups.size();
                    groups.add(new AbstractMap.SimpleEntry<>(addGroup, "Manual Entry"));
                }
            }
            // REMOVE
        } else if (i < 0) {
            if (groups.size() > selrow) {
                groups.set(selrow, null);
                // If the targetRow is out of the groups, select the last group.
                if (selrow >= groups.size()) {
                    selrow = Math.max(groups.size() - 1, 0);
                }

            }
        }

        this.fillGroupsInSet(groups, this.bunchGroupsInSetTable);
        int targetRow = selrow + i;
        if (targetRow < 0 || targetRow > 15) {
            targetRow = selrow;
        }
        this.bunchGroupsInSetTable.setRowSelectionInterval(targetRow, targetRow);
    }

    /**
     * Remove the selected row s form bunchGroupInTable table.
     */
    private void removeGroups() {
        JTable table = this.bunchGroupsInSetTable;
        List<AbstractMap.SimpleEntry<L1BunchGroup, String>> groups = this.getBunchGroupsInTable();
        int selRows[] = table.getSelectedRows();
        int rowFirst = selRows[0];
        int rowLast = selRows[selRows.length - 1];
        for (int rIdx = rowFirst; rIdx <= rowLast; rIdx++) {
            if (rIdx > -1 && rIdx < L1BunchGroupSet.MAXGROUPS) {
                table.setValueAt(null, rIdx, 1);
                table.setValueAt(null, rIdx, 2);
            }
        }
        int targetRow = 0;

        if (rowFirst - 1 > 0) {
            targetRow = rowFirst - 1;
        }
        this.bunchGroupsInSetTable.setRowSelectionInterval(targetRow, targetRow);
    }

    private void updateLeft(final int id) throws SQLException {
        this.leftPanel.updatePanel(id);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void updateRight(final AbstractTable at) throws SQLException {
        List<AbstractMap.SimpleEntry<L1BunchGroup, String>> groups = null;
        L1BunchGroupSet set = null;
        if (at == null) {
            this.nameTextField.setText("");
            if (!partitionLocked) {
                this.nameTextField1.setText("");
            }
        } else if (at instanceof L1BunchGroupSet) {
            set = (L1BunchGroupSet) at;
            String setName = set.get_name();
            int setPartition = set.get_partition();
            groups = set.getBunchGroupsFromDb();
            this.nameTextField.setText(setName);
            if (!partitionLocked) {
                this.nameTextField1.setText("" + setPartition);
            }
        } else {
            logger.severe(" Problems in BunchGroupPanel.java.");
        }
        if (groups != null && set != null && groups.size() > L1BunchGroupSet.MAXGROUPS) {
            logger.log(Level.SEVERE, " Groups Set too long!! \n {0}\n Group size  {1}", new Object[]{set.toString(), groups.size()});
            groups = groups.subList(0, L1BunchGroupSet.MAXGROUPS);
        }
        this.fillGroupsInSet(groups, this.bunchGroupsInSetTable);

    }

    /**
     * @inheritDoc
     */
    @Override
    public AbstractTable getSelectedRow() {
        return this.leftPanel.getSelectedRow();
    }

    /**
     * @param id
     */
    @Override
    public void setSelectedRow(int id) {
        this.leftPanel.setSelectedId(id);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        bunchGroupsInSetTable = new BunchGroupSetTable();
        titleLabel = new javax.swing.JLabel();
        nameTextField = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        upButton = new javax.swing.JButton();
        downButton = new javax.swing.JButton();
        addButton = new javax.swing.JButton();
        removeButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        nameTextField1 = new javax.swing.JTextField();
        titleLabel1 = new javax.swing.JLabel();
        leftPanel = new triggertool.L1BunchGroupPanels.BunchLeftPanel();
        partitionStatusDisplay = new javax.swing.JLabel();

        jPanel1.setName("jPanel1"); // NOI18N

        jScrollPane4.setName("jScrollPane4"); // NOI18N

        bunchGroupsInSetTable.setModel(new BunchGroupSetTableModel());
        bunchGroupsInSetTable.setIntercellSpacing(new java.awt.Dimension(1, 0));
        bunchGroupsInSetTable.setName("bunchGroupsInSetTable"); // NOI18N
        bunchGroupsInSetTable.setShowHorizontalLines(false);
        bunchGroupsInSetTable.setShowVerticalLines(false);
        bunchGroupsInSetTable.getTableHeader().setReorderingAllowed(false);
        bunchGroupsInSetTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bunchGroupsInSetTableMouseClicked(evt);
            }
        });
        bunchGroupsInSetTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                bunchGroupsInSetTableKeyReleased(evt);
            }
        });
        jScrollPane4.setViewportView(bunchGroupsInSetTable);
        if (bunchGroupsInSetTable.getColumnModel().getColumnCount() > 0) {
            bunchGroupsInSetTable.getColumnModel().getColumn(0).setPreferredWidth(20);
        }

        titleLabel.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        titleLabel.setForeground(java.awt.Color.blue);
        titleLabel.setText("Name:");
        titleLabel.setName("titleLabel"); // NOI18N

        nameTextField.setName("nameTextField"); // NOI18N
        nameTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameTextFieldActionPerformed(evt);
            }
        });

        jPanel2.setName("jPanel2"); // NOI18N

        upButton.setText("up");
        upButton.setName("upButton"); // NOI18N
        upButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                upButtonActionPerformed(evt);
            }
        });

        downButton.setText("down");
        downButton.setName("downButton"); // NOI18N
        downButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                downButtonActionPerformed(evt);
            }
        });

        addButton.setText("add");
        addButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        addButton.setName("addButton"); // NOI18N
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        removeButton.setText("remove");
        removeButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        removeButton.setName("removeButton"); // NOI18N
        removeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeButtonActionPerformed(evt);
            }
        });

        saveButton.setText("Save Set");
        saveButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        saveButton.setName("saveButton"); // NOI18N
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(17, 17, 17)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(downButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(upButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(addButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(removeButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE)
                    .add(saveButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(upButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(downButton)
                .add(53, 53, 53)
                .add(addButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(removeButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(saveButton)
                .addContainerGap())
        );

        nameTextField1.setName("nameTextField1"); // NOI18N
        nameTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameTextField1ActionPerformed(evt);
            }
        });

        titleLabel1.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        titleLabel1.setForeground(java.awt.Color.blue);
        titleLabel1.setText("Partition:");
        titleLabel1.setName("titleLabel1"); // NOI18N

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel1Layout.createSequentialGroup()
                        .add(titleLabel)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(nameTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 166, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(24, 24, 24)
                        .add(titleLabel1)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(nameTextField1))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jScrollPane4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 419, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 95, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(nameTextField1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(titleLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(nameTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(titleLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED, 14, Short.MAX_VALUE)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(jScrollPane4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 289, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jPanel1Layout.linkSize(new java.awt.Component[] {nameTextField, nameTextField1, titleLabel, titleLabel1}, org.jdesktop.layout.GroupLayout.VERTICAL);

        leftPanel.setName("leftPanel"); // NOI18N

        partitionStatusDisplay.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        partitionStatusDisplay.setText("jLabel1");
        partitionStatusDisplay.setName("partitionStatusDisplay"); // NOI18N

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(leftPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(partitionStatusDisplay)
                        .add(225, 225, 225))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(leftPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 424, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createSequentialGroup()
                        .add(partitionStatusDisplay)
                        .add(29, 29, 29)
                        .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(0, 15, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void downButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_downButtonActionPerformed
        this.shiftGroupsInSet(BunchGroupSetPanel.DOWN);
}//GEN-LAST:event_downButtonActionPerformed

    private void upButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_upButtonActionPerformed
        this.shiftGroupsInSet(BunchGroupSetPanel.UP);
    }//GEN-LAST:event_upButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        try {
            this.saveSet();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while saving Bunch group set: ");
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        try {
            this.addGroup(1);
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while adding a Bunch group set: ");
        }
    }//GEN-LAST:event_addButtonActionPerformed

    private void removeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeButtonActionPerformed
        this.removeGroups();
    }//GEN-LAST:event_removeButtonActionPerformed

    private void bunchGroupsInSetTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bunchGroupsInSetTableMouseClicked
        this.selectGroupInSibling();
    }//GEN-LAST:event_bunchGroupsInSetTableMouseClicked

    private void bunchGroupsInSetTableKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bunchGroupsInSetTableKeyReleased
        this.selectGroupInSibling();
    }//GEN-LAST:event_bunchGroupsInSetTableKeyReleased

    private void nameTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nameTextField1ActionPerformed

    private void nameTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nameTextFieldActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JTable bunchGroupsInSetTable;
    private javax.swing.JButton downButton;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane4;
    private triggertool.L1BunchGroupPanels.BunchLeftPanel leftPanel;
    private javax.swing.JTextField nameTextField;
    private javax.swing.JTextField nameTextField1;
    private javax.swing.JLabel partitionStatusDisplay;
    private javax.swing.JButton removeButton;
    private javax.swing.JButton saveButton;
    private javax.swing.JLabel titleLabel;
    private javax.swing.JLabel titleLabel1;
    private javax.swing.JButton upButton;
    // End of variables declaration//GEN-END:variables
}
