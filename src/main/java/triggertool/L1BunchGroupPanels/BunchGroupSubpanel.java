/*
 * BunchGroupSubpanel.java
 *
 * Created on Jun 8, 2011, 4:19:44 PM
 */
package triggertool.L1BunchGroupPanels;

import java.sql.SQLException;
import triggerdb.Entities.AbstractTable;

/**
 * Common interface for the 2 panels in BunchGroupEditor. 
 * @author tiago perez <"tperez@cern.ch">
 */
public interface BunchGroupSubpanel {
    
    /**
     * Refresh the left table from DB.
     * @param id the db id of the abstract table selected/created in the right panel.
     */
    public void setSelectedRow(int id);
    
    /**
     * Refresh the right table with the values of an AbstractTable.
     * @param at the table to display in the right panel.
     * @throws java.sql.SQLException
     */
    public void updateRight(AbstractTable at) throws SQLException;

    /**
     * Get the selected AbstractTable in the left panel.
     * @return the selected AbstractTabel in the left panel.
     */
    public AbstractTable getSelectedRow();
    
}
