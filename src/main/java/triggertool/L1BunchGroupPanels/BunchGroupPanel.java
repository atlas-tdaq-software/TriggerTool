/*
 * BunchGroupPanel.java
 *
 * Created on Jun 8, 2011, 6:02:09 PM
 */
package triggertool.L1BunchGroupPanels;

import java.awt.Component;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.L1.L1Bunch;
import triggerdb.Entities.L1.L1BunchGroup;
import triggertool.TTExceptionHandler;

/**
 * Panel to show and edit BunchGroup information. This is meant to be embedded
 * into the main BunchGroupEditor.
 *
 * @author tiago perez <"tperez@cern.ch">
 */
public class BunchGroupPanel extends javax.swing.JPanel
        implements java.io.Serializable, BunchGroupSubpanel {

    /**
     * The logger.
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");

    /**
     * Empty constructor for the Netbeans palette.
     */
    public BunchGroupPanel() {
        initComponents();
    }

    /**
     * Default Constructor
     *
     * @param sibling
     */
    public BunchGroupPanel(final BunchGroupSubpanel sibling) {
        initComponents();
        initPanel();
    }

    private void initPanel() {
        this.leftPanel.BunchLeftPanelInit(this, BunchLeftPanel.BUNCHGROUP);
        this.leftPanel.idlabel.setText("BG ID");
        this.leftPanel.filterLabel.setText("Size filter");
        // Disabel load panel
        for (Component c : this.bgLoadPanel.getComponents()) {
            c.setEnabled(false);
        }
        // 
        this.bgSizeTextField.setEnabled(true);
        this.bgSizeTextField.setVisible(true);
        this.bgIdTextField.setEnabled(true);
        this.bgIdTextField.setVisible(true);
    }

    private void updateLiveCounter() {
        String bunchDef = this.bgDefinitionText.getText();
        //String bunchGrName = this.bgNameTextField.getText().trim();
        List<Integer> bunchNumbers = this.readBunchDefinition(bunchDef);
        updateCounter(-1, bunchNumbers.size());
    }

    /**
     * Reads the definition of the bunch group form the text area, creates a
     * bunch group and tries to save.
     */
    private void saveBunchGroupDefinition() throws SQLException {
        String bunchDef = this.bgDefinitionText.getText();
        //String bunchGrName = this.bgNameTextField.getText().trim();
        List<Integer> bunchNumbers = this.readBunchDefinition(bunchDef);
        if (bunchNumbers.isEmpty()) {
            Object[] options = {"Yes, please", "No, thanks"};
            int showOptionDialog = JOptionPane.showOptionDialog(this,
                    "Bunchgroup is empty. Are you sure you want to save it?",
                    "Empty BG upload confirmation ", JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
            if (showOptionDialog == 1) {
                return;
            }
        }
        L1BunchGroup gr = new L1BunchGroup();

        int savedId = -1;
        gr.set_id(savedId);
        //gr.set_name(bunchGrName);
        gr.get_Bunches().clear();
        ArrayList<L1Bunch> bunchList = new ArrayList<>();
        for (Integer i : bunchNumbers) {
            L1Bunch bunch = new L1Bunch();
            bunch.set_bunch_number(i);
            bunchList.add(bunch);
            //System.out.println(i);
        }

        gr.set_Bunches(bunchList);
        savedId = gr.save();

        if (savedId > 0) {
            this.updateLeft(savedId);
        }
        //      }
    }

    /**
     * Parses the given text as a list of bunch numbers.
     *
     * @param bunchDef the Text as in the bunch group definition.
     * @return the list of bunches.
     */
    private List<Integer> readBunchDefinition(final String bunchDef) {
        List<Integer> bunchNumbers = new ArrayList<>();
        StringTokenizer tkn = new StringTokenizer(bunchDef, ",");
        while (tkn.hasMoreTokens()) {
            String token = tkn.nextToken().trim();
            // Range of bunches: expand before save.
            if (token.contains("-")) {
                String bunchrange[] = token.split("-");
                //now bunchrange[0] is lower and [1] is upper
                Integer lower = Integer.parseInt(bunchrange[0].trim());
                Integer upper = Integer.parseInt(bunchrange[1].trim());
                List<Integer> seqbunches = new ArrayList<>();
                for (Integer j = lower; j <= upper; j++) {
                    seqbunches.add(j);
                }
                bunchNumbers.addAll(seqbunches);
            } else {
                // Single bunch number.
                bunchNumbers.add(Integer.parseInt(token));
            }
        }
        return bunchNumbers;
    }

    @Override
    public void updateRight(AbstractTable at) throws SQLException {
        if (at == null) {
            this.bgDefinitionText.setText("");
        } else if (at instanceof L1BunchGroup) {
            L1BunchGroup group = (L1BunchGroup) at;

            String bunchText = "";
            ArrayList<L1Bunch> bunches = group.getBunchesFromDb();

            for (int i = 0; i < bunches.size(); i++) {
                L1Bunch b = bunches.get(i);
                bunchText += b.get_bunch_number() + ", ";
            }
            bunchText = bunchText.trim();
            if (bunchText.endsWith(",")) {
                bunchText = bunchText.substring(0, bunchText.lastIndexOf(","));
            }
            this.bgDefinitionText.setText(bunchText);
            updateCounter(group.get_id(), bunches.size());
        } else {
            logger.severe(" Problems in BunchGroupPanel.java.");
        }
    }

    /**
     *
     * @param id
     * @param count
     */
    public void updateCounter(int id, int count) {
        if (id == -1) {
            this.bgIdTextField.setText("Unsaved");     
        } else {
            this.bgIdTextField.setText("" + id);
        }
         this.bgSizeTextField.setText("" + count);
    }

    private void updateLeft(final int id) throws SQLException {
        this.leftPanel.updatePanel(id);
    }

    @Override
    public void setSelectedRow(final int tableId) {
        this.leftPanel.setSelectedId(tableId);

    }

    @Override
    public AbstractTable getSelectedRow() {
        L1BunchGroup g = new L1BunchGroup();
        return this.leftPanel.getSelectedRow();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        leftPanel = new triggertool.L1BunchGroupPanels.BunchLeftPanel();
        bgDefinitionPanel = new javax.swing.JPanel();
        bgLoadPanel = new javax.swing.JPanel();
        bgLoadButton = new javax.swing.JButton();
        bgLoadTextField = new javax.swing.JTextField();
        bgLoadBrowseButton = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        bgDefinitionText = new javax.swing.JTextArea();
        bgSaveButton = new javax.swing.JButton();
        bgDefinitionLabel = new javax.swing.JLabel();
        bgSizeTextField = new javax.swing.JTextField();
        bgIdTextField = new javax.swing.JTextField();
        bgIdLabel = new javax.swing.JLabel();
        bgSizeLabel = new javax.swing.JLabel();

        bgDefinitionPanel.setName("bgDefinitionPanel"); // NOI18N

        bgLoadPanel.setName("bgLoadPanel"); // NOI18N

        bgLoadButton.setText("Load");
        bgLoadButton.setName("bgLoadButton"); // NOI18N
        bgLoadButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bgLoadButtonActionPerformed(evt);
            }
        });

        bgLoadTextField.setName("bgLoadTextField"); // NOI18N

        bgLoadBrowseButton.setText("Browse");
        bgLoadBrowseButton.setName("bgLoadBrowseButton"); // NOI18N

        org.jdesktop.layout.GroupLayout bgLoadPanelLayout = new org.jdesktop.layout.GroupLayout(bgLoadPanel);
        bgLoadPanel.setLayout(bgLoadPanelLayout);
        bgLoadPanelLayout.setHorizontalGroup(
            bgLoadPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(bgLoadPanelLayout.createSequentialGroup()
                .add(bgLoadButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(bgLoadTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 217, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(bgLoadBrowseButton))
        );
        bgLoadPanelLayout.setVerticalGroup(
            bgLoadPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(bgLoadBrowseButton)
            .add(bgLoadButton)
            .add(bgLoadTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );

        bgLoadPanelLayout.linkSize(new java.awt.Component[] {bgLoadBrowseButton, bgLoadButton, bgLoadTextField}, org.jdesktop.layout.GroupLayout.VERTICAL);

        jScrollPane5.setName("jScrollPane5"); // NOI18N

        bgDefinitionText.setColumns(20);
        bgDefinitionText.setLineWrap(true);
        bgDefinitionText.setRows(5);
        bgDefinitionText.setWrapStyleWord(true);
        bgDefinitionText.setName("bgDefinitionText"); // NOI18N
        bgDefinitionText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                bgDefinitionTextKeyReleased(evt);
            }
        });
        jScrollPane5.setViewportView(bgDefinitionText);

        bgSaveButton.setText("Save Bunch Group");
        bgSaveButton.setName("bgSaveButton"); // NOI18N
        bgSaveButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bgSaveButtonMouseClicked(evt);
            }
        });

        bgDefinitionLabel.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        bgDefinitionLabel.setForeground(java.awt.Color.blue);
        bgDefinitionLabel.setText("Bunch Group Definition");
        bgDefinitionLabel.setName("bgDefinitionLabel"); // NOI18N

        bgSizeTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        bgSizeTextField.setMinimumSize(new java.awt.Dimension(20, 25));
        bgSizeTextField.setName("bgSizeTextField"); // NOI18N

        bgIdTextField.setEditable(false);
        bgIdTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        bgIdTextField.setMinimumSize(new java.awt.Dimension(100, 25));
        bgIdTextField.setName("bgIdTextField"); // NOI18N

        bgIdLabel.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        bgIdLabel.setForeground(java.awt.Color.blue);
        bgIdLabel.setText("ID");
        bgIdLabel.setName("bgIdLabel"); // NOI18N

        bgSizeLabel.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        bgSizeLabel.setForeground(java.awt.Color.blue);
        bgSizeLabel.setText("Size:");
        bgSizeLabel.setName("bgSizeLabel"); // NOI18N

        org.jdesktop.layout.GroupLayout bgDefinitionPanelLayout = new org.jdesktop.layout.GroupLayout(bgDefinitionPanel);
        bgDefinitionPanel.setLayout(bgDefinitionPanelLayout);
        bgDefinitionPanelLayout.setHorizontalGroup(
            bgDefinitionPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane5)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, bgDefinitionPanelLayout.createSequentialGroup()
                .add(bgDefinitionLabel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE)
                .add(41, 41, 41)
                .add(bgIdLabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(bgIdTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 64, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(32, 32, 32)
                .add(bgSizeLabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(bgSizeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 58, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
            .add(bgDefinitionPanelLayout.createSequentialGroup()
                .add(bgLoadPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(bgSaveButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        bgDefinitionPanelLayout.setVerticalGroup(
            bgDefinitionPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, bgDefinitionPanelLayout.createSequentialGroup()
                .add(30, 30, 30)
                .add(bgDefinitionPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(bgIdLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(bgIdTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 25, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(bgSizeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 25, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(bgSizeLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(bgDefinitionLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(6, 6, 6)
                .add(jScrollPane5)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(bgDefinitionPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(bgLoadPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(bgSaveButton))
                .addContainerGap())
        );

        bgDefinitionPanelLayout.linkSize(new java.awt.Component[] {bgDefinitionLabel, bgIdLabel, bgIdTextField, bgSizeLabel, bgSizeTextField}, org.jdesktop.layout.GroupLayout.VERTICAL);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(leftPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(bgDefinitionPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(leftPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 410, Short.MAX_VALUE)
            .add(layout.createSequentialGroup()
                .add(20, 20, 20)
                .add(bgDefinitionPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void bgLoadButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bgLoadButtonActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_bgLoadButtonActionPerformed

    private void bgSaveButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bgSaveButtonMouseClicked
        try {
            this.saveBunchGroupDefinition();
        } catch (SQLException ex) {
            TTExceptionHandler.HandleSqlException(ex, this, "Error while saving Bunch group definition: ");
        } catch (NumberFormatException ex) {
            TTExceptionHandler.HandleNumberFormatException(ex, this, "Error while saving Bunch group definition: ");
        }
}//GEN-LAST:event_bgSaveButtonMouseClicked

    private void bgDefinitionTextKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bgDefinitionTextKeyReleased
        try {
            updateLiveCounter();
        } catch (NumberFormatException ex) {

        }
    }//GEN-LAST:event_bgDefinitionTextKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel bgDefinitionLabel;
    private javax.swing.JPanel bgDefinitionPanel;
    private javax.swing.JTextArea bgDefinitionText;
    private javax.swing.JLabel bgIdLabel;
    private javax.swing.JTextField bgIdTextField;
    private javax.swing.JButton bgLoadBrowseButton;
    private javax.swing.JButton bgLoadButton;
    private javax.swing.JPanel bgLoadPanel;
    private javax.swing.JTextField bgLoadTextField;
    private javax.swing.JButton bgSaveButton;
    private javax.swing.JLabel bgSizeLabel;
    private javax.swing.JTextField bgSizeTextField;
    private javax.swing.JScrollPane jScrollPane5;
    private triggertool.L1BunchGroupPanels.BunchLeftPanel leftPanel;
    // End of variables declaration//GEN-END:variables
}
