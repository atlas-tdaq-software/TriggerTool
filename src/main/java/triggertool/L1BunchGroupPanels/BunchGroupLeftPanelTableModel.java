package triggertool.L1BunchGroupPanels;

/**
 *
 * Table model used in BunchGroupSetEditor panel.
 * 
 * @author Tiago Perez <tperez@cern.ch>
 * @date 2011-05-16, 11:21:43 AM
 */
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.L1.L1BunchGroup;

/**
 *
 * @author William
 */
public class BunchGroupLeftPanelTableModel extends AbstractTableModel {

    /**
     * Message Logger setup here.
     */
    public static final Logger logger = Logger.getLogger("TriggerTool");
    /**
     * Stores the data that the table is drawn from. The first vector holds the
     * row position information The second vector holds the Object (name of
     * prescale), Integer(Prescale value), Bool(Prescale On/Off) Can be filled
     * with rows using add and passing a vector< object > with Object, integer,
     * Bool in it. Can edit specific positions using setValueAt Can return
     * values stored at any position with getValueAt
     */
    private ArrayList<AbstractTable> dataVector = new ArrayList<>();
    /**
     * Stores the names of the columns for usage in getColumnName
     */
    private final String[] columnNames = {"ID", "Size"};
    /**
     * Stored the class of the columns for usage in getColumnClass
     */
    private final Class[] types = new Class[]{
        java.lang.Integer.class,
        java.lang.String.class,
        java.lang.Integer.class};
    /**
     * Stores the edit info to be passed to is CellEditable
     */
    boolean[] canEdit = new boolean[]{false, false, false};

    /**
     *
     */
    public static int COLID = 0;

    /**
     *
     */
    public static int COLSIZE = 1;
    private int SortingFlag;

    /**
     *
     */
    public BunchGroupLeftPanelTableModel() {
        super();
        dataVector = new ArrayList<>();
    }

    /**
     * @inheritDoc
     */
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    /**
     * @param col
     * @return 
     * @{@inheritDoc}
     */
    @Override
    public String getColumnName(final int col) {
        return columnNames[col];
    }

    /**
     * @inheritDoc
     */
    @Override
    public Class getColumnClass(int columnIndex) {
        return types[columnIndex];
    }

    /**
     * @inheritDoc
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return this.canEdit[columnIndex];
    }

    /**
     * Clears the datavector so a new prescale set can be loaded.
     */
    public void clearRows() {
        dataVector.clear();
        fireTableDataChanged();
    }

    /**
     * @inheritDoc
     */
    @Override
    public int getRowCount() {
        return dataVector.size();
    }

    /**
     * @inheritDoc
     */
    @Override
    public Object getValueAt(int row, int col) {
        L1BunchGroup _row = (L1BunchGroup)dataVector.get(row);
        if (col == COLID) {
            return _row.get_id();
        }  else if (col == COLSIZE) {
            return _row.get_size();
        } else {
            return null;
        }
    }

    /**
     * Gets the ID of one row.
     * @param row the number of the row.
     * @return the value of the Id column in row.
     */
    public Integer getRowId(int row) {
        return (Integer) this.getValueAt(row, COLID);
    }

    /**
     * Get from the JTable the object with the given ID.
     * @param tableId the id of the object.
     * @return the Object with id tableId.
     */
    public AbstractTable getTable(final int tableId) {
        AbstractTable retTab = null;
        for (AbstractTable tab : dataVector) {
            if (tab.get_id() == tableId) {
                retTab = tab;
            }
        }
        return retTab;
    }

    /**
     * Adds a row to the datavector. Then tells the table that the data has been
     * updated so needs to be redrawn.
     * @param row a AbstractTable
     */
    public void add(AbstractTable row) {
        dataVector.add(row);
        fireTableDataChanged();


    }

    /**
     * Adds a row to the datavector. Then tells the table that the data has been
     * updated so needs to be redrawn.
     *
     * @param L1prescalesdata_v
     */
    @Deprecated
    private void addRow(AbstractTable row) {
        this.add(row);


    }

    /**
     * Allows values on the table to be changed after being initially filled.
     * Implements boolean/integer linking. Tick box on = positive. Tick box off
     * = negative. Updates table after a cell has been changed, so it can be
     * redrawn. Also allows alternative setvalueat to be called if sorting flag
     * is on.
     *
     * Should not be used.
     * 
     * @param value
     * @param row
     * @param col
     */
    @Override
    @Deprecated
    public void setValueAt(Object value, int row, int col) {
//        if (getFlag() == 1) {
//            setValueAtSort(value, row, col);
//            return;
//        }
//        if (col == COLID) {
//            this.dataVector.get(row).setValue(new L1PSNumber((Float) value));
//            fireTableDataChanged();
//        } else if (col == L1PrescaleTableModel.COLBOOL) {
//            this.dataVector.get(row).setFlag((Boolean) value);
//            fireTableDataChanged();
//        }
//        fireTableCellUpdated(row, col);
        logger.warning(" problems");
    }

    /**
     * Returns the current sorting flag.
     *
     * @return
     */
    public int getFlag() {
        return SortingFlag;


    }

    /**
     * Sets the sorting flag.
     *
     * @param Flag
     */
    public void setFlag(int Flag) {
        SortingFlag = Flag;


    }

    /**
     * A second set value at. Only accessed when the sorting flag is set to 1.
     * This is identical to the first set value at but does not fire table data
     * changed. It is accessed from the enable/disable all, things go wrong if
     * the table data is updated at every setValueAt call when table is sorting
     * and enabling/disabling multiple entries. This splitting gets around this.
     * Still need to fire table changed after calling this setValueAtSort, or
     * table will not update.
     *
     * @param value
     * @param row
     * @param col
     */
    @Deprecated
    public void setValueAtSort(Object value, int row, int col) {
//        //dataVector.get(row).setElementAt(value, col);
//        if (col == L1PrescaleTableModel.COLVALUE) {
//            this.dataVector.get(row).setValue(new L1PSNumber((Float) value));
//        } else if (col == L1PrescaleTableModel.COLBOOL) {
//            this.dataVector.get(row).setFlag((Boolean) value);
//        }
        logger.warning(" probl");
    }

    /**
     * Get the list with all the lements in the table.
     * @return the DataVector.
     */
    public List<AbstractTable> getDataVector() {
        return dataVector;
    }

    /**
     *
     * @param _vector
     */
    public void addDataVector(List<AbstractTable> _vector) {
        this.dataVector.addAll(_vector);
        fireTableDataChanged();
    }

    /**
     * Get an element of the table.
     * @param rowId the id of the row to get.
     * @return the Astract Table with id = rowId.
     */
    public AbstractTable getRow(final int rowId) {
        return this.dataVector.get(rowId);
    }
}
