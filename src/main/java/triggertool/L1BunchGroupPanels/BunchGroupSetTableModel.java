/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggertool.L1BunchGroupPanels;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import triggerdb.Entities.L1.L1BunchGroupSet;

/**
 *
 * @author William
 */
public class BunchGroupSetTableModel extends AbstractTableModel {

    private final String[] columnNames = {"Pos", "Name", "Description"};
    private ArrayList<Object[]> dataVector;
    boolean[][] canEdit = new boolean[L1BunchGroupSet.MAXGROUPS][];

    private final Class[] types = new Class[]{
        java.lang.Integer.class,
        java.lang.String.class,
        java.lang.String.class
    };

    /**
     *
     */
    public BunchGroupSetTableModel() {
        super();
        this.dataVector = new ArrayList<>();
        initModel();
    }

    private void initModel() {
        dataVector = new ArrayList<>();
        for (int row = 0; row < L1BunchGroupSet.MAXGROUPS; row++) {
            dataVector.add(new Object[]{row, new String(), null});
            canEdit[row] = new boolean[3];
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return canEdit[rowIndex][columnIndex];
    }

    /**
     *
     * @param rowIndex
     * @param columnIndex
     * @param isEditable
     */
    public void setCellEditable(int rowIndex, int columnIndex, boolean isEditable) {
        canEdit[rowIndex][columnIndex] = isEditable;
    }

    @Override
    public Class getColumnClass(int columnIndex) {
        return types[columnIndex];
    }
    
    @Override
    public int getRowCount() {
        return dataVector.size();
    }

    
    @Override
    public Object getValueAt(int row, int col) {
        Object[] element = dataVector.get(row);
        switch (col) {
            case 0:
                return element[0];
            case 1:
                return (String)element[1];
            case 2:
                return element[2];
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object obj,int row, int col) {
        dataVector.get(row)[col] = obj;
    }

    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    /**
     * @param col
     * @return 
     * @{@inheritDoc}
     */
    @Override
    public String getColumnName(final int col) {
        return columnNames[col];
    }
    
}