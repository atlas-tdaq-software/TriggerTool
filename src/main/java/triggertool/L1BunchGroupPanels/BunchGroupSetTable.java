/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggertool.L1BunchGroupPanels;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author William
 */
public class BunchGroupSetTable extends javax.swing.JTable {

    @Override
    public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
        Component comp = super.prepareRenderer(renderer, row, col);
        Object value = getModel().isCellEditable(row, col);
          if (col == 1 && row == 0 && !isCellEditable(row, col)) {
            comp.setBackground(Color.white);
            comp.setForeground(Color.black);
            comp.setFont(new Font("Helvetica", Font.ITALIC, 13));
        } else if (col != 1) {
            if (isCellEditable(row, 1)) {
                comp.setBackground(Color.white);
                comp.setForeground(Color.black);
            } else if (row == 0) {
                comp.setBackground(Color.white);
                comp.setForeground(Color.black);
                comp.setFont(new Font("Helvetica", Font.ITALIC, 13));

            } else {
                comp.setBackground(Color.gray);
                comp.setForeground(Color.white);
                comp.setFont(new Font("Helvetica", Font.ITALIC, 13));
            }
        } else if (value.equals(false)) {
            comp.setBackground(Color.gray);
            comp.setForeground(Color.white);
            comp.setFont(new Font("Helvetica", Font.ITALIC, 13));
        } else {
            comp.setBackground(Color.white);
            comp.setForeground(Color.black);
        }
            if (isCellSelected(row,col))
            {
                comp.setBackground(getSelectionBackground());
            }
        return comp;
    }
}
