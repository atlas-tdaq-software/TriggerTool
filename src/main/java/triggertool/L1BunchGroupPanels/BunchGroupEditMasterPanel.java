    /*
 * BunchGroupEditMasterPanel.java
 *
 * Created on Jun 8, 2011, 4:19:44 PM
 */
package triggertool.L1BunchGroupPanels;

import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 * A JDialog to view and edit bunch groups and bunch group sets.
 * This replaces <code>triggertool.Components.BunchGroupEdit<code>
 * @author tiago perez <tperez@cern.ch">
 */
public class BunchGroupEditMasterPanel extends JDialog {

    Boolean partitionLocked = false;
    Integer superMasterKey;
    Integer ctpPartition;

    /** Creates new form BunchGroupEdit2
     * @param parent
     */
    public BunchGroupEditMasterPanel(JFrame parent) {
        super((JDialog)null);
        initComponents();        
        setLocationRelativeTo(parent);
        setVisible(true);
    }

    /**
     *
     * @param parent
     * @param smk
     * @param partition
     */
    public BunchGroupEditMasterPanel(JFrame parent, Integer smk, Integer partition) {
        super((JDialog)null);
        partitionLocked = true;        
        superMasterKey = smk;
        ctpPartition = partition;
        initComponents();       
 	setLocationRelativeTo(parent);
 	setVisible(true);
    }

    
    /** This method is called from within the constructor to
     * initialise the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator2 = new javax.swing.JSeparator();
        jSeparator1 = new javax.swing.JSeparator();
        bunchGroupPanel = new triggertool.L1BunchGroupPanels.BunchGroupPanel(null);
        if(partitionLocked){
            bunchGroupSetPanel = new triggertool.L1BunchGroupPanels.BunchGroupSetPanel(this.bunchGroupPanel,superMasterKey,ctpPartition);
        }
        else{
            bunchGroupSetPanel = new triggertool.L1BunchGroupPanels.BunchGroupSetPanel(this.bunchGroupPanel);
        }
        closeButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1180, 950));

        jSeparator2.setName("jSeparator2"); // NOI18N

        jSeparator1.setName("jSeparator1"); // NOI18N

        bunchGroupPanel.setName("bunchGroupPanel"); // NOI18N

        bunchGroupSetPanel.setName("BUNCH GROUP SET EDITOR");

        closeButton.setText("Close");
        closeButton.setName("closeButton"); // NOI18N
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(bunchGroupPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, bunchGroupSetPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(0, 0, Short.MAX_VALUE)
                        .add(closeButton))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jSeparator2)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jSeparator1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(bunchGroupSetPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(3, 3, 3)
                .add(jSeparator2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(bunchGroupPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(closeButton)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_closeButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private triggertool.L1BunchGroupPanels.BunchGroupPanel bunchGroupPanel;
    private triggertool.L1BunchGroupPanels.BunchGroupSetPanel bunchGroupSetPanel;
    private javax.swing.JButton closeButton;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    // End of variables declaration//GEN-END:variables
}
