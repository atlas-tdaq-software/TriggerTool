package triggertool.XML;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Entities.L1.L1Threshold;

/**
 * A tree node to hold the logic tree-like structure of L1 Item descriptions.
 *
 * @author tiago perez.
 * @author joerg stelzer.
 */
public final class TriggerDescriptionNode extends DefaultMutableTreeNode {

    /** The TriggerTool Message Log */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    /** Holds the logic String. */
    private String value;
    /** Unknowns trigger type. */
    private final static int UNKNOWN_TYPE = -1;
    /** Threshold type. */
    private final static int THRESHOLD_TYPE = 0;
    /** Not type. */
    private final static int NOT_TYPE = 1;
    /** AND type. */
    private final static int AND_TYPE = 2;
    /** OR type. */
    private final static int OR_TYPE = 3;
    private int type;
    /** Display debug information. */
    private static boolean verbose = false;

    /** Default constructor. */
    public TriggerDescriptionNode() {
        this.type = TriggerDescriptionNode.UNKNOWN_TYPE;
    }

    /**
     *
     * @param verb
     */
    public static void setVerbose(final boolean verb) {
        TriggerDescriptionNode.verbose = verb;
    }

    /**
     *
     * @param logic
     * @return
     * @throws ParseException
     */
    public static TriggerDescriptionNode buildTriggerDescriptionNode(
            final String logic) throws ParseException {
        TriggerDescriptionNode node = new TriggerDescriptionNode();
        node.parse(logic);
        return node;
    }

    private static TriggerDescriptionNode buildNOTTriggerDescriptionNode(
            final String logic) throws ParseException {
        TriggerDescriptionNode node = new TriggerDescriptionNode();
        node.type = 1;
        if (verbose) {
            logger.log(Level.INFO, "build ! of ''{0}''", logic);
        }
        TriggerDescriptionNode child = new TriggerDescriptionNode();
        child.parse(logic);
        node.add(child);
        return node;
    }

    /**
     * Returns the String label of the logical operator operator.
     * @return the operator as a String.
     */
    public final String operator() {
        switch (this.type) {
            case UNKNOWN_TYPE:
                return "UNKNOWN";
            case THRESHOLD_TYPE:
                return "THR";
            case NOT_TYPE:
                return "NOT";
            case AND_TYPE:
                return "AND";
            case OR_TYPE:
                return "OR";
            default:
                return "UNKNOWN";
        }
    }

    /**
     * Recursives printing.
     * @param indent A string to display before every line, typically an indent.
     * @return the formatted XML output.
     */
    public String writeXML(final String indent) {
        String printStr;
        if (this.isLeaf()) {
            printStr = indent + this.value;
        } else {
            printStr = "";
            // This is to avoid problems with descriptions starting with "!".
            if (this.type != UNKNOWN_TYPE) {
                printStr += indent + "<" + this.operator() + ">";

            }
            for (Object child : children) {
                TriggerDescriptionNode node = (TriggerDescriptionNode) child;
                printStr += "\n" + node.writeXML(indent + "  ");
            }
            if (this.type != UNKNOWN_TYPE) {
                printStr += "\n" + indent + "</" + this.operator() + ">";
            }
        }
        return printStr;
    }

    /**
     * Recursives printing with not indent.
     * @return the formatted XML output.
     */
    public String writeXML() {
        return this.writeXML("");
    }

    /**
     * Recursives setting of the threshold description.
     * @param thresholds
     */
    public void setThresholdDescription(ArrayList<L1Threshold> thresholds) {
        if (this.isLeaf()) {
            int num = Integer.parseInt(this.value);
            for (L1Threshold thr : thresholds) {
                if (!thr.get_position().equals(num)) {
                    continue;
                }
                if (L1Threshold.isInternalThreshold(thr)) {
                    this.value = "<InternalTrigger name=\"" + thr.get_name()
                            + "\"/>";
                } else {
                    this.value = "<TriggerCondition multi=\""
                            + thr.get_multiplicity() + "\" name=\""
                            + thr.get_name() + "_x" + thr.get_multiplicity()
                            + "\" triggerthreshold=\"" + thr.get_name()
                            + "\"/>";
                }
            }
        } else {
            for (Object child : children) {
                TriggerDescriptionNode node = (TriggerDescriptionNode) child;
                node.setThresholdDescription(thresholds);
            }
        }
    }

    /**
     * number of left brackets and right brackets.
     *
     * @param logic a string representing logic.
     * @throws ParseException Exceptions if brackets do not match.
     */
    private void checkBalancedParentheses(final String logic)
            throws ParseException {
        int lb = 0, rb = 0;
        for (int i = 0; i < logic.length(); i++) {
            char ch = logic.charAt(i);
            if (ch == '(') {
                lb++;
            }
            if (ch == ')') {
                rb++;
            }
        }
        if (rb != lb) {
            throw new ParseException("Left and Right brakets do not match.", 0);
        }
    }

    /**
     * Removes the outermost brackets.
     * @param logic a string representing logic.
     * @return the same string without meaningless outer brackets.
     * @throws ParseException
     */
    private String removeOuterParetheses(final String logic)
            throws ParseException {
        String outLogic = logic;
        while (outLogic.charAt(0) == '('
                && findClosingParenthesis(outLogic, 0) == outLogic.length() - 1) {
            outLogic = outLogic.substring(1, outLogic.length() - 1);
        }
        return outLogic;
    }

    /**
     *
     * @param logic
     * @param startOfSearch
     * @return
     * @throws ParseException
     */
    //private
    public int findClosingParenthesis(final String logic,
            final int startOfSearch)
            throws ParseException {
        int pos = startOfSearch;
        int lastpos = logic.length() - 1;
        int balance = 1;
        while (balance > 0) {
            pos++;
            if (pos > lastpos) {
                throw new ParseException("Found no closing bracket", pos);
            }
            char ch = logic.charAt(pos);
            if (ch == '(') {
                balance++;
            }
            if (ch == ')') {
                balance--;
            }
            if (balance == 0) {
                return pos;
            }
        }
        return pos;
    }

    private int findGroupEnd(final String logic, int startOfSearch) throws ParseException {
        int pos = startOfSearch;
        int lastpos = logic.length() - 1;
        char ch = logic.charAt(pos);
        if (ch == '&' || ch == '|' || ch == ')') {
            throw new ParseException("'" + ch + "' must not follow a '!'", startOfSearch);
        }

        if (ch == '!') {
            throw new ParseException("Found double negation '!!'", startOfSearch - 1);
        }

        if (ch == '(') {
            return findClosingParenthesis(logic, pos);
        }

        while (pos < lastpos && Character.isDigit(logic.charAt(pos + 1))) {
            pos++;
        }

        return pos;
    }

    private void parse(final String log) throws ParseException {
        if (verbose) {
            logger.log(Level.INFO, "parse ''{0}''", log);
        }
        // Remove spaces
        String logInt = log.trim();
        String replaceAll = logInt.replaceAll(" ", "");

        checkBalancedParentheses(logInt);

        logInt = removeOuterParetheses(logInt);

        // is a child/leaf if there is no '!', '&', '|' inside
        if (!logInt.contains("!")
                && !logInt.contains("&")
                && !logInt.contains("|")) {
            this.type = 0;
            this.value = logInt;
            return;
        }

        // Find the groups one level below this.
        // int leftOpen = 0;
        String group = "";
        int lastpos = logInt.length() - 1;

        for (int pos = 0; pos <= lastpos; pos++) {
            char ch = logInt.charAt(pos);

            if (ch == '!') {
                if (pos == logInt.length() - 1) {
                    throw new ParseException("NOT-sign in unexpected position", pos);
                }
                int groupStart = pos + 1;
                int groupEnd = findGroupEnd(logInt, groupStart);
                String negatedExpression = logInt.substring(groupStart, groupEnd + 1);
                TriggerDescriptionNode child = buildNOTTriggerDescriptionNode(negatedExpression);
                this.add(child);
                pos = groupEnd;
                continue;
            }

            if (ch == '(') {
                int groupStart = pos;
                int groupEnd = findClosingParenthesis(logInt, groupStart);
                String subExpression = logInt.substring(groupStart, groupEnd + 1);
                TriggerDescriptionNode child = buildTriggerDescriptionNode(subExpression);
                this.add(child);
                pos = groupEnd;
                continue;
            }

            if (ch == '&' || ch == '|') {
                if (pos == 0 || logInt.charAt(pos - 1) == '&' || logInt.charAt(pos - 1) == '|') {
                    throw new ParseException("No '&' or '|' allowed at this position", pos);
                }

                if (this.type != -1) { // then there was already a '&' or '|' at this level
                    if (this.type == ((ch == '&') ? 2 : 3)) {
                        continue; // same type, so it is fine
                    } else {
                        // opposite type (e.g. 1&2|3) we can't handle right now, so let's throw an error until somebody complains
                        throw new ParseException("Parsing mixed AND/OR logics like '" + logInt + "' is currently not supported, please use parentheses like "
                                + "'(" + logInt.substring(0, pos) + ")" + ch + logInt.substring(pos + 1, logInt.length()) + "' "
                                + "or complain to the TriggerTool people", pos);
                    }

                } else {
                    this.type = (ch == '&') ? 2 : 3;
                    continue;
                }
            }

            if (Character.isDigit(ch)) {
                int groupStart = pos;
                while (pos < lastpos && Character.isDigit(logInt.charAt(pos + 1))) {
                    pos++;
                }
                int groupEnd = pos;
                String subExpression = logInt.substring(groupStart, groupEnd + 1);
                TriggerDescriptionNode child = buildTriggerDescriptionNode(subExpression);
                this.add(child);
                pos = groupEnd;
            }

        }

    }  // parse(final String logic)
}
