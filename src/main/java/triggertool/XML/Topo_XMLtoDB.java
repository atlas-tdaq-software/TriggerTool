package triggertool.XML;

import triggerdb.UploadException;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import triggerdb.Connections.ConnectionManager;

import triggerdb.Entities.L1Links.L1TM_TT;

//Import all of the topological tables. We are going to need them....
import triggerdb.Entities.Topo.*;
import triggerdb.Entities.TopoLinks.*;

/**
 * Read Topo XML file and save to database.
 * Functionality to read a Topo XML file and convert, and save it to the database.
 * 
 * @author Alex Martyniuk
 */
public final class Topo_XMLtoDB {

    // /Message Log.

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    // /Used when parsing XML.
    private NodeList sections;
    // /Used by XML parser.
    private Document document;
    // /Vector of L1 TM to TT links - required for cables.
    private final ArrayList<L1TM_TT> cables_to_save = new ArrayList<>();
    // /A vector of the problems encountered before the upload.
    private final ArrayList<String> faults = new ArrayList<>();
    //A bool to say the OutputLine has been checked against the outputs.
    private boolean OutCheck = false;
    //A collection of loaded algo inputs
    private ArrayList<TopoAlgoInput> algoInputs = new ArrayList<>();
    //A collection of loaded algo outputs
    private ArrayList<TopoAlgoOutput> algoOutputs = new ArrayList<>();
    //A collection of loaded algo paramters
    private ArrayList<TopoParameter> algoParameters = new ArrayList<>();
    //A collection of loaded algo gnerics
    private ArrayList<TopoGeneric> algoGenerics = new ArrayList<>();

    // /to store the new LVL1 bunch group key
    // private int newLVL1BGKey = 0;
    // /Constructor takes the filename of the L1 XML file to read only.
    /**
     * Constructor takes the filename. The XML file is parsed and the
     * information contained within it is written to the database
     * 
     * @param filename
     *            Path and filename of the L1 XML file to read in
     * @param configname
     * @return 
     * @throws triggerdb.UploadException 
     * @throws triggertool.XML.ConfigurationException 
     */
    public int readXML(final String filename, final String configname) throws UploadException, ConfigurationException {
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringComments(true);
        factory.setCoalescing(true);
        factory.setNamespaceAware(false);
        factory.setValidating(false);

        DocumentBuilder parser;

        int topo_id = -1;

        try {
            parser = factory.newDocumentBuilder();
            document = parser.parse(new File(filename));

            ValidateXml();
            if (IsXmlValid()) {
                topo_id = HandleValidXml(configname, filename); 
            }
            else {
                HandleInvalidXml();
            }                       
        } catch (ParserConfigurationException | SAXException | IOException | SQLException e) {
            logger.severe(e.toString());
        }
        return topo_id;
    }

    private int HandleValidXml(final String configname, final String filename) throws SQLException, UploadException, ConfigurationException {
        //if name is "USEXML" then find menu from inside!
        String configname_ = configname;
        if (configname.equals("USEXML")) {
            Element menu = (Element) document.getElementsByTagName("TOPO_MENU").item(0);
            configname_ = menu.getAttribute("menu_name");
        }
        StringBuilder sb = new StringBuilder(200);
        sb.append("Will upload Topo XML file: ").append(filename)
            .append("\nMenu name will be:         ").append(configname_);
        logger.log(Level.INFO,sb.toString());
        logger.fine("Passed all checks, will start upload");
        logger.fine("Reading Output List");
        ArrayList<TopoOutputLine> outputLines = read_output_list();
        logger.fine("Reading Configs");
        ArrayList<TopoConfig> outputConfigs = read_configs();
        logger.fine("Reading Algorithms");
        ArrayList<TopoAlgo> outputAlgos = read_algos();
        logger.fine("Checking output consistency");
        OutCheck = OutputCheck(outputLines, outputAlgos);
        
        if( ! OutCheck){
            logger.warning("Output list check failed. Outputs do not match the output list");
            throw new ConfigurationException("L1Topo algorithm outputs don't match output list");
        }
        //Save the output lines
        outputLines = saveOutputLine(outputLines, configname_);
            
        //Make the outputs list and its links
        //Only need to save if we have new lines?
        
        TopoOutputList outputList = linkOutputLines(outputLines, configname_);
            
        outputConfigs = saveConfig(outputConfigs);
        //This goes away and checks then saves the algoInputs.
        //Returns a linked map of TopoAlgoInputs and thier ids.
        //Could swap to return AlgoInputs with the Ids set?
        algoInputs = saveAlgoInputs();

        //This goes away and checks then saves the algoOutputs
        algoOutputs = saveAlgoOutputs();

        //This goes away and checks then saves the generics
        algoGenerics = saveGenerics();

        //This goes away and checks then saves the parameters
        algoParameters = saveParameters();

        //Save the algos and all the links.
        outputAlgos = saveAlgos(outputAlgos);
            
        //We now have everything in hand so.....
        //Now lets save the menu. This will link the configs and the algos to the menu.
        //It will also make the link to attach the topo menu to a l1 menu???
        logger.fine("Saving Menu");
        TopoMenu outputMenu = saveMenu(configname_, outputList, outputConfigs, outputAlgos);
        //Then lets save a Master table
        TopoMaster outputMaster = saveMaster(outputMenu);
        
        int topo_id = outputMaster.get_id();
        int topo_menu_id = outputMenu.get_id();
        
        logger.log(Level.INFO,"Finished uploading Topo" + "\n\tTopo Master ID =     {0}\n\tTopo Menu ID =      {1}",
                new Object[]{topo_id, topo_menu_id});
                
        return topo_id;

    }

    /**
     * List where the XML had errors if the validation routine returns any.
     * Do not try to upload an invalid XML.
     */
    private void HandleInvalidXml() {
        logger.warning("XML file has errors");
        for (String line : faults) {
            logger.warning(line);
        }
    }
    
    /**
     * Function reads the contents of the output list from the XML and returns an ArrayList
     * containing the elements. Needs to be cross-checked with the outputs from the decision
     * algos before saving.
     * @return
     * @throws SQLException 
     */
    private ArrayList<TopoOutputLine> read_output_list()
            throws SQLException {
        sections = document.getElementsByTagName("OutputList");
        ArrayList<TopoOutputLine> outputLines = new ArrayList<>();
        if (sections.getLength() == 1) {
            Element section = (Element) sections.item(0);
            NodeList List_outputs = section.getElementsByTagName("Output");
            
            int Length = List_outputs.getLength();
            for (int i = 0; i < Length; ++i) {
                Element output_node = (Element) List_outputs.item(i);
                TopoOutputLine newOutputLine = new TopoOutputLine(-1);
                newOutputLine.set_algo_name(output_node.getAttribute("algname"));
                newOutputLine.set_triggerline(output_node.getAttribute("triggerline"));
                newOutputLine.set_algo_id(Integer.parseInt(output_node.getAttribute("algoId")));
                newOutputLine.set_module(Integer.parseInt(output_node.getAttribute("module")));
                newOutputLine.set_fpga(Integer.parseInt(output_node.getAttribute("fpga")));
                newOutputLine.set_first_bit(Integer.parseInt(output_node.getAttribute("firstbit")));
                newOutputLine.set_clock(Integer.parseInt(output_node.getAttribute("clock")));
                outputLines.add(newOutputLine);
            } 
        } else {
            logger.log(Level.WARNING, "Found {0} Output List nodes in XML, expected exactly 1.", sections.getLength());
        }
        return outputLines;
    }
   
    
    private ArrayList<TopoOutputLine> saveOutputLine(ArrayList<TopoOutputLine> outputLines, String configname) throws SQLException{
        TopoOutputLine saveOutputLine = new TopoOutputLine(-1);
        ArrayList<TopoOutputLine> toSaveOutputLines = new ArrayList<>();
        //remove duplicated inputs. i.e. the same input is used twice. We might want to
        //save them but we can use the same table and link it twice
        for(TopoOutputLine current:outputLines){
            if(!toSaveOutputLines.contains(current)){
                toSaveOutputLines.add(current);
            }
        }
        int i = 0;
        //Loop over the outputs we read in and form the strings to go into the check query.
        ArrayList<String> Check = new ArrayList<>();
        for(TopoOutputLine current:outputLines){
            Check.add(String.valueOf(i));
            Check.add(current.get_algo_name()+"."+current.get_triggerline()+"."+current.get_algo_id()+"."+current.get_module()
                    +"."+current.get_fpga()+"."+current.get_first_bit()+"."+current.get_clock());
            i++;
        }
        //What columns do we need to match?
        String Names = "tol_algo_name||'.'||tol_triggerline||'.'||tol_algo_id||'.'||tol_module||'.'||tol_fpga||'.'||tol_first_bit||'.'||tol_clock";
        //Fire off the query
        ArrayList<AbstractMap.SimpleEntry<Integer, Integer>> Matched = ConnectionManager.getInstance().checkTopoExisting(Check, Names,saveOutputLine.tablePrefix,saveOutputLine.getTableName());
        for(AbstractMap.SimpleEntry<Integer, Integer> key : Matched){
            //trying out a new method. Now we attach the id to the object.
            outputLines.get(key.getKey()).set_id(key.getValue());
            toSaveOutputLines.remove(outputLines.get(key.getKey()));
        }
        
        //Save all the outputs we did not find in the DB in one shot.
        TreeMap<Integer,Integer> outAlgoOutputLines = saveOutputLine.batchsave(toSaveOutputLines);
        logger.log(Level.INFO, "Saving {0} output lines not already in DB", outAlgoOutputLines.size());
        for(Integer Key:outAlgoOutputLines.keySet()){
            if(outAlgoOutputLines.get(Key) > 0){
                toSaveOutputLines.get(Key).set_id(outAlgoOutputLines.get(Key));
                for(int j =0; j<outputLines.size();j++){
                    if(outputLines.get(j).equals(toSaveOutputLines.get(Key))){
                       outputLines.get(j).set_id(outAlgoOutputLines.get(Key));
                    }
                }
            }
        }
            for(TopoOutputLine line:outputLines){
        }
 
        return outputLines;
    }

    private TopoOutputList linkOutputLines(ArrayList<TopoOutputLine> outputLines, String configname) throws SQLException, UploadException{
        //Need to check if we already have a link with all the correct lines attached
        ArrayList<String> outputLineIDs = new ArrayList<>();
        for(TopoOutputLine line:outputLines){
            outputLineIDs.add(String.valueOf(line.get_id()));
        }
        ArrayList<Integer> Matched = ConnectionManager.getInstance().checkLinkExisting(outputLineIDs, "TL_LINK_ID","TOPO_OUTPUT_LINK", "TL_OUTPUT_ID");
        
        //Should only ever match one ID. Function is more general to work for tables with multiple
        //linking objects.
        TopoOutputList saveOutputList = new TopoOutputList(-1);
        if(Matched.size() == 1){
            saveOutputList = new TopoOutputList(Matched.get(0));
        } else if(Matched.isEmpty()){
            saveOutputList.set_name(configname);
            int list = saveOutputList.save();
            saveOutputList.set_id(list);
            ArrayList<TopoOuputLink> links = new ArrayList<>();
            for(TopoOutputLine line:outputLines){
                TopoOuputLink link = new TopoOuputLink(-1);
                link.set_link_id(list);
                link.set_output_id(line.get_id());
                links.add(link);
            }
            TreeMap<Integer,Integer> algoOuputLinks = links.get(0).batchsave(links);
        } else {
            // Alex, perhaps it is better to allow for more than 1 match and take the last
            throw new UploadException("L1Topo: more than one matching output list found");
        }
        return saveOutputList;
    }

    
    /**
     * Function reads the contents of the config from the XML and returns an ArrayList
     * of TopoConfg items.
     * @return
     * @throws SQLException 
     */
    private ArrayList<TopoConfig> read_configs()
            throws SQLException {
        sections = document.getElementsByTagName("TopoConfig");

        ArrayList<TopoConfig> configurations = new ArrayList<>();
        
        if (sections.getLength() == 1) {
            Element section = (Element) sections.item(0);
            NodeList List_configs = section.getElementsByTagName("Entry");
            
            int Length = List_configs.getLength();
            for (int i = 0; i < Length; ++i) {
                Element output_node = (Element) List_configs.item(i);
                TopoConfig newConfig = new TopoConfig(-1);
                newConfig.set_name(output_node.getAttribute("name"));
                newConfig.set_value(Integer.parseInt(output_node.getAttribute("value")));
                configurations.add(newConfig);
            } 
        } else {
            logger.log(Level.WARNING, "Found {0} Output List nodes in XML, expected exactly 1.", sections.getLength());
        }

        return configurations;
    }
    
    private ArrayList<TopoConfig> saveConfig(ArrayList<TopoConfig> outputConfigs) throws SQLException{
        TopoConfig saveConfig = new TopoConfig(-1);
        ArrayList<TopoConfig> toSaveConfigs = new ArrayList<>();
        //remove duplicated inputs. i.e. the same input is used twice. We might want to
        //save them but we can use the same table and link it twice
        for(TopoConfig current:outputConfigs){
            if(!toSaveConfigs.contains(current)){
                toSaveConfigs.add(current);
            }
        }
        int i = 0;
        //Loop over the outputs we read in and form the strings to go into the check query.
        ArrayList<String> Check = new ArrayList<>();
        for(TopoConfig current:outputConfigs){
            Check.add(String.valueOf(i));
            Check.add(current.get_name()+"."+current.get_value());
            i++;
        }
        //What columns do we need to match?
        String Names = "tc_name||'.'||tc_value";
        //Fire off the query
        ArrayList<AbstractMap.SimpleEntry<Integer, Integer>> Matched = new ArrayList<>();
        if(!Check.isEmpty()){
             Matched = ConnectionManager.getInstance().checkTopoExisting(Check, Names,saveConfig.tablePrefix,saveConfig.getTableName());
        }
        if(!Matched.isEmpty()){
            for(AbstractMap.SimpleEntry<Integer, Integer> key : Matched){
                //trying out a new method. Now we attach the id to the object.
                //Dont need a 
                outputConfigs.get(key.getKey()).set_id(key.getValue());
                toSaveConfigs.remove(outputConfigs.get(key.getKey()));
            }
        }
        
        //Save all the outputs we did not find in the DB in one shot.
        TreeMap<Integer,Integer> outAlgoConfigs = saveConfig.batchsave(toSaveConfigs);
        logger.log(Level.INFO, "Saving {0} algo configs not already in DB", outAlgoConfigs.size());
        for(Integer Key:outAlgoConfigs.keySet()){
            if(outAlgoConfigs.get(Key) > 0){
                for(int j =0; j<outputConfigs.size();j++){
                    if(outputConfigs.get(j).equals(toSaveConfigs.get(Key))){
                        outputConfigs.get(j).set_id(outAlgoConfigs.get(Key));
                    }
                }
            }
        }
            for(TopoConfig line:outputConfigs){
        }
        return outputConfigs;
    }
    
    /**
     * Function reads the algorithms from the XML and returns an ArrayList
     * of algorithms including their attached inputs/outputs/paras......
     * @return
     * @throws SQLException 
     */
    private ArrayList<TopoAlgo> read_algos() throws SQLException{
        
        ArrayList<TopoAlgo> algos = new ArrayList<>();
        sections = document.getElementsByTagName("SortAlgo");
        algos.addAll(read_algos("Sort"));
        sections = document.getElementsByTagName("DecisionAlgo");
        algos.addAll(read_algos("Decision"));
        
        return algos;
    }
    
    private ArrayList<TopoAlgo> read_algos(String type) throws SQLException{
        
        ArrayList<TopoAlgo> algos = new ArrayList<>();

        int AlgoLength = sections.getLength();
        for (int i = 0; i < AlgoLength; ++i) {
            Element algo_node = (Element) sections.item(i);
            TopoAlgo newAlgo = new TopoAlgo();
            newAlgo.set_name(algo_node.getAttribute("name"));
            newAlgo.set_type(algo_node.getAttribute("type"));
            newAlgo.set_output(algo_node.getAttribute("output"));
            if(type.equals("Sort")){
                newAlgo.set_sort_deci("Sort");
            } else if(type.equals("Decision")) {
                newAlgo.set_sort_deci("Decision");
            }
            newAlgo.set_algo_id(Integer.parseInt(algo_node.getAttribute("algoId")));
            //Now need to grab the sub parts of the algos.
            //First the inputs
            NodeList inputs = algo_node.getElementsByTagName("Input");
            newAlgo.add_input(read_inputs(inputs, type));
            algoInputs.addAll(newAlgo.get_inputs());
            //Now the outputs. Should we put a check in here that the bits match the #bit?
            NodeList outputs = algo_node.getElementsByTagName("Output");
            newAlgo.add_output(read_outputs(outputs, type));
            algoOutputs.addAll(newAlgo.get_outputs());
            NodeList generics = algo_node.getElementsByTagName("Generic");
            newAlgo.add_generic(read_generics(generics));
            algoGenerics.addAll(newAlgo.get_generics());
            NodeList parameters = algo_node.getElementsByTagName("Parameter");
            newAlgo.add_parameter(read_parameters(parameters, type));
            algoParameters.addAll(newAlgo.get_parameters());
            algos.add(newAlgo);
        }
          
        return algos;
    }
    
    //This is goign to be complicated. Need to save the algos and all
    //of the links.... Well maybe not......
    //First save the algos, then take the lists of inputs, outputs, generics and parameters,
    //These should all have the IDs associated with them now. Try and save the links between
    //the algos and the attached parameters.
    private ArrayList<TopoAlgo> saveAlgos(ArrayList<TopoAlgo> outputAlgos) throws SQLException{
        TopoAlgo saveAlgo = new TopoAlgo(-1);
        ArrayList<TopoAlgo> toSaveAlgos = new ArrayList<>();
        //remove duplicated inputs. i.e. the same input is used twice. We might want to
        //save them but we can use the same table and link it twice
        for(TopoAlgo current:outputAlgos){
            if(!toSaveAlgos.contains(current)){
                toSaveAlgos.add(current);
            }
        }
        int i = 0;
        //Loop over the outputs we read in and form the strings to go into the check query.
        ArrayList<String> Check = new ArrayList<>();
        for(TopoAlgo current:outputAlgos){
            Check.add(String.valueOf(i));
            String output = current.get_output();
            if(output.isEmpty()) output = "~";
            Check.add(current.get_name()+"."+output+"."+current.get_type()+"."+current.get_bits()
                    +"."+current.get_sort_deci()+"."+current.get_algo_id());
            i++;
        }
        //What columns do we need to match?
        String Names = "ta_name||'.'||ta_output||'.'||ta_type||'.'||ta_bits||'.'||ta_sort_deci||'.'||ta_algo_id";
        //Fire off the query
        TreeMap<Integer, ArrayList<Integer>> Matched = ConnectionManager.getInstance().checkTopoExistingMulti(Check, Names,saveAlgo.tablePrefix,saveAlgo.getTableName());
        for(int key:Matched.keySet()){
            //Now check if all the input links are the same?
            ArrayList<String> inputIDs = new ArrayList<>();
            for(TopoAlgoInput input:outputAlgos.get(key).get_inputs()){
                inputIDs.add(String.valueOf(input.get_id()));
            }
            
            //So incase there are no attached inputs we need to say we match. Otherwise check the link is correct.
            ArrayList<Integer> matchedInputs = new ArrayList<>();
            if(!inputIDs.isEmpty()){
                matchedInputs = ConnectionManager.getInstance().checkLinkExisting(inputIDs, "TA2TI_ALGO_ID","TA_TO_TI", "TA2TI_INPUT_ID");
            } else {
                //matchedInputs.add(Matched.get(key));
                matchedInputs.addAll(Matched.get(key));
            }
            //Now check if all the output links are the same?
            ArrayList<String> outputIDs = new ArrayList<>();
            for(TopoAlgoOutput output:outputAlgos.get(key).get_outputs()){
                outputIDs.add(String.valueOf(output.get_id()));
            }
            
            ArrayList<Integer> matchedOutputs = new ArrayList<>();
            if(!outputIDs.isEmpty()){
                matchedOutputs = ConnectionManager.getInstance().checkLinkExisting(outputIDs, "TA2TO_ALGO_ID","TA_TO_TO", "TA2TO_OUTPUT_ID");
            } else {
                //matchedOutputs.add(Matched.get(key));
                matchedOutputs.addAll(Matched.get(key));
            }
            
            //Now check if all the generic links are the same?
            ArrayList<String> genericIDs = new ArrayList<>();
            for(TopoGeneric generic:outputAlgos.get(key).get_generics()){
                genericIDs.add(String.valueOf(generic.get_id()));
            }
            
            ArrayList<Integer> matchedGenerics = new ArrayList<>();        
            if(!genericIDs.isEmpty()){
                matchedGenerics = ConnectionManager.getInstance().checkLinkExisting(genericIDs, "TA2TG_ALGO_ID","TA_TO_TG", "TA2TG_GENERIC_ID");
            } else {
                //matchedGenerics.add(Matched.get(key));
                matchedGenerics.addAll(Matched.get(key));
            }
            
            //Now check if all the parameter links are the same?
            ArrayList<String> parameterIDs = new ArrayList<>();
            for(TopoParameter parameter:outputAlgos.get(key).get_parameters()){
                parameterIDs.add(String.valueOf(parameter.get_id()));
            }
            ArrayList<Integer> matchedParameters = new ArrayList<>();
            if(!parameterIDs.isEmpty()){
                matchedParameters = ConnectionManager.getInstance().checkLinkExisting(parameterIDs, "TA2TP_ALGO_ID","TA_TO_TP", "TA2TP_PARAM_ID");
            } else {
                //matchedParameters.add(Matched.get(key));
                matchedParameters.addAll(Matched.get(key));
            }
            //Make sure all of the attached objects are the same as the ones we are trying to save.
            //Do not try to match if there were nothing of a type (i.e. no paramters to match).
            //If we try this then we try to see if nothing equals nothing and get false.
            for(int k = 0; k<Matched.get(key).size();k++){
                if(matchedInputs.contains(Matched.get(key).get(k)) && matchedOutputs.contains(Matched.get(key).get(k))
                        && matchedGenerics.contains(Matched.get(key).get(k)) && matchedParameters.contains(Matched.get(key).get(k))){
                    outputAlgos.get(key).set_id(Matched.get(key).get(k));
                    toSaveAlgos.remove(outputAlgos.get(key));
                }
            }
        }
        
        
        //Save all the outputs we did not find in the DB in one shot.
        TreeMap<Integer,Integer> outAlgoAlgos = saveAlgo.batchsave(toSaveAlgos);
        for(Integer Key:outAlgoAlgos.keySet()){
            if(outAlgoAlgos.get(Key) > 0){
                toSaveAlgos.get(Key).set_id(outAlgoAlgos.get(Key));
                for(int j =0; j<outputAlgos.size();j++){
                    if(outputAlgos.get(j).equals(toSaveAlgos.get(Key))){
                        outputAlgos.get(j).set_id(outAlgoAlgos.get(Key));
                    }
                }
            }
        }
        
        //At this point we should save all of the lovely links to any new Algos.
        //If we returned an existing algo then it will already have the links.
        //Lets make some arraylists to hold the links
        ArrayList<TA_TI> inputLinks = new ArrayList<>();
        ArrayList<TA_TO> outputLinks = new ArrayList<>();
        ArrayList<TA_TG> genericLinks = new ArrayList<>();
        ArrayList<TA_TP> parameterLinks = new ArrayList<>();
        
        //Loop over the algos and for each algo fill the list of links for
        //each link type.
        for(TopoAlgo saved:toSaveAlgos){
            for(TopoAlgoInput input:saved.get_inputs()){
                TA_TI link = new TA_TI(-1);
                link.set_algo_id(saved.get_id());
                link.set_input_id(input.get_id());
                inputLinks.add(link);
            }
            for(TopoAlgoOutput output:saved.get_outputs()){
                TA_TO link = new TA_TO(-1);
                link.set_algo_id(saved.get_id());
                link.set_output_id(output.get_id());
                outputLinks.add(link);
            }
            for(TopoGeneric generic:saved.get_generics()){
                TA_TG link = new TA_TG(-1);
                link.set_algo_id(saved.get_id());
                link.set_generic_id(generic.get_id());
                genericLinks.add(link);
            }
            for(TopoParameter parameter:saved.get_parameters()){
                TA_TP link = new TA_TP(-1);
                link.set_algo_id(saved.get_id());
                link.set_parameter_id(parameter.get_id());
                parameterLinks.add(link);
            }
        }
        
        //Save all the links we just made.
        if(!inputLinks.isEmpty()) {
            TreeMap<Integer,Integer> algoInputLinks = inputLinks.get(0).batchsave(inputLinks);
        }
        if(!outputLinks.isEmpty()) {
            TreeMap<Integer,Integer> algoOutputLinks = outputLinks.get(0).batchsave(outputLinks);
        }
        if(!genericLinks.isEmpty()) {
            TreeMap<Integer,Integer> algoGenericLinks = genericLinks.get(0).batchsave(genericLinks);
        }
        if(!parameterLinks.isEmpty()) {
            TreeMap<Integer,Integer> algoParameterLinks = parameterLinks.get(0).batchsave(parameterLinks);
        }
        
        //Pretty sure we don't need to do anything else now we saved the links. The algos themselves
        //will be linked to the menu so need their ids  and that is all.
        //Please add it if we do need something....
        
        return outputAlgos;
    }
    
    private ArrayList<TopoAlgoInput> read_inputs(NodeList input_list, String type){
        
        ArrayList<TopoAlgoInput> inputs = new ArrayList<>();

        int InputLength = input_list.getLength();
        for (int i = 0; i < InputLength; ++i) {
            Element input_node = (Element) input_list.item(i);
            TopoAlgoInput newInput = new TopoAlgoInput();
            newInput.set_name(input_node.getAttribute("name"));
            newInput.set_value(input_node.getAttribute("value"));
            if(type.equals("Decision") && !(input_node.getAttribute("position").isEmpty())){
                newInput.set_position(Integer.parseInt(input_node.getAttribute("position")));
            }
            inputs.add(newInput);
        }
          
        return inputs;
    }
    
    private ArrayList<TopoAlgoInput> saveAlgoInputs() throws SQLException{
        TopoAlgoInput saveAlgoInput = new TopoAlgoInput(-1);
        ArrayList<TopoAlgoInput> toSaveInputs = new ArrayList<>();
        //remove duplicated inputs. i.e. the same input is used twice. We might want to
        //save them but we can use the same table and link it twice
        for(TopoAlgoInput current:algoInputs){
            if(!toSaveInputs.contains(current)){
                toSaveInputs.add(current);
            }
        }
        //If the input has already been saved then we want to attach it to the id and remove it from 
        //those that need to be saved. If not then leave it in to be saved.
        int i = 0;
        //Loop over the inputs we read in and form the strings to go into the check query.
        ArrayList<String> Check = new ArrayList<>();
        for(TopoAlgoInput current:algoInputs){
            Check.add(String.valueOf(i));
            Check.add(current.get_name()+"."+current.get_value()+"."+current.get_position());
            i++;
        }
        //What columns do we need to match?
        String Names = "tai_name||'.'||tai_value||'.'||tai_position";
        //Fire off the query
        ArrayList<AbstractMap.SimpleEntry<Integer, Integer>> Matched = ConnectionManager.getInstance().checkTopoExisting(Check, Names,saveAlgoInput.tablePrefix,saveAlgoInput.getTableName());
        for(AbstractMap.SimpleEntry<Integer, Integer> key : Matched){
            //From the output we will only get those inputs that have an Id already.
            //Save this Id and remove the input from the list of those to save.
            algoInputs.get(key.getKey()).set_id(key.getValue());
            toSaveInputs.remove(algoInputs.get(key.getKey()));
        }
        

        //Save all the inputs we did not find in the DB in one shot.
        TreeMap<Integer,Integer> outAlgoInputs = saveAlgoInput.batchsave(toSaveInputs);
        logger.log(Level.INFO, "Saving {0} algo inputs not already in DB", outAlgoInputs.size());
        for(Integer Key:outAlgoInputs.keySet()){
            if(outAlgoInputs.get(Key) > 0){
                for(int j=0; j<algoInputs.size();j++){
                    if(algoInputs.get(j).equals(toSaveInputs.get(Key))){
                        algoInputs.get(j).set_id(outAlgoInputs.get(Key));
                    }
                }
            }
        }
        return algoInputs;
    }
    
    private ArrayList<TopoAlgoOutput> read_outputs(NodeList output_list, String type){
        
        ArrayList<TopoAlgoOutput> outputs = new ArrayList<>();
        int OutputLength = output_list.getLength();
        if(type.equals("Sort")){
            if(OutputLength == 1){
                Element output_node = (Element) output_list.item(0);
                TopoAlgoOutput newOutput = new TopoAlgoOutput();
                newOutput.set_name(output_node.getAttribute("name"));
                newOutput.set_value(output_node.getAttribute("value"));
                outputs.add(newOutput);
            } else {
                logger.log(Level.WARNING, "Found {0} Output nodes in XML for this, sorting algo, expected exactly 1.", sections.getLength());
            }
        }
        
        if(type.equals("Decision")){
            if(OutputLength == 1){
                Element output_node = (Element) output_list.item(0);
                
                String name = output_node.getAttribute("name");
                int bits = Integer.parseInt(output_node.getAttribute("bits"));
                NodeList List_bits = output_node.getElementsByTagName("Bit");
                int bitLength = List_bits.getLength();
                if(bits != bitLength){
                    logger.log(Level.WARNING, "Expecting {0} output bits", bits);
                    logger.log(Level.WARNING, "in the algorithm called {0}", name);
                    logger.log(Level.WARNING, "I actually found {0}. Fix this.", bitLength);
                }
                for (int i = 0; i < bitLength; ++i) {
                    Element bit_node = (Element) List_bits.item(i);
                    TopoAlgoOutput newOutput = new TopoAlgoOutput();
                    newOutput.set_name(name);
                    newOutput.set_selection(Integer.parseInt(bit_node.getAttribute("selection")));
                    newOutput.set_bitname(bit_node.getAttribute("name"));
                    outputs.add(newOutput);
                }
            } else {
                logger.log(Level.WARNING, "Found {0} Output nodes in XML for this, sorting algo, expected exactly 1.", sections.getLength());
            }
        }
          
        return outputs;
    }
    
    private ArrayList<TopoAlgoOutput> saveAlgoOutputs() throws SQLException{
        TopoAlgoOutput saveAlgoOutput = new TopoAlgoOutput(-1);
        ArrayList<TopoAlgoOutput> toSaveOutputs = new ArrayList<>();
        //remove duplicated inputs. i.e. the same input is used twice. We might want to
        //save them but we can use the same table and link it twice
        for(TopoAlgoOutput current:algoOutputs){
            if(!toSaveOutputs.contains(current)){
                toSaveOutputs.add(current);
            }
        }
        int i = 0;
        //Loop over the outputs we read in and form the strings to go into the check query.
        ArrayList<String> Check = new ArrayList<>();
        for(TopoAlgoOutput current:algoOutputs){
            Check.add(String.valueOf(i));
            String value = current.get_value();
            if(value.isEmpty()) value = "~";
            String bitname = current.get_bitname();
            if(bitname.isEmpty()) bitname = "~";
            Check.add(current.get_name()+"."+value+"."+current.get_selection()+"."+bitname);
            i++;
        }
        //What columns do we need to match?
        String Names = "tao_name||'.'||tao_value||'.'||tao_selection||'.'||tao_bitname";
        //Fire off the query
        ArrayList<AbstractMap.SimpleEntry<Integer, Integer>> Matched = ConnectionManager.getInstance().checkTopoExisting(Check, Names,saveAlgoOutput.tablePrefix,saveAlgoOutput.getTableName());
        for(AbstractMap.SimpleEntry<Integer, Integer> key : Matched){
            //From the output we will only get those outputs that have an Id already.
            //Save this Id and remove the output from the list of those to save.
            algoOutputs.get(key.getKey()).set_id(key.getValue());
            toSaveOutputs.remove(algoOutputs.get(key.getKey()));
        }
        
        //Save all the outputs we did not find in the DB in one shot.
        TreeMap<Integer,Integer> outAlgoOutputs = saveAlgoOutput.batchsave(toSaveOutputs);
        logger.log(Level.INFO, "Saving {0} alog outputs not already in DB", outAlgoOutputs.size());
        for(Integer Key:outAlgoOutputs.keySet()){
            if(outAlgoOutputs.get(Key) > 0){
                for(int j =0; j<algoOutputs.size();j++){
                    if(algoOutputs.get(j).equals(toSaveOutputs.get(Key))){
                        algoOutputs.get(j).set_id(outAlgoOutputs.get(Key));
                    }
                }
            }
        }
        return algoOutputs;
    }
    
    private ArrayList<TopoGeneric> read_generics(NodeList generic_list){
        
        ArrayList<TopoGeneric> generics = new ArrayList<>();

        int GenericLength = generic_list.getLength();
        for (int i = 0; i < GenericLength; ++i) {
            Element generic_node = (Element) generic_list.item(i);
            TopoGeneric newGeneric = new TopoGeneric();
            newGeneric.set_name(generic_node.getAttribute("name"));
            newGeneric.set_value(generic_node.getAttribute("value"));
            generics.add(newGeneric);
        }
          
        return generics;
    }
    
    private ArrayList<TopoGeneric> saveGenerics() throws SQLException{
        TopoGeneric saveGeneric = new TopoGeneric(-1);
        ArrayList<TopoGeneric> toSaveGenerics = new ArrayList<>();
        //remove duplicated outputs. i.e. the same output is used twice. We might want to
        //save them but we can use the same table and link it twice
        for(TopoGeneric current:algoGenerics){
            if(!toSaveGenerics.contains(current)){
                toSaveGenerics.add(current);
            }
        }
        
        int i = 0;
        //Loop over the outputs we read in and form the strings to go into the check query.
        ArrayList<String> Check = new ArrayList<>();
        for(TopoGeneric current:algoGenerics){
            Check.add(String.valueOf(i));
            Check.add(current.get_name()+"."+current.get_value());
            i++;
        }
        //What columns do we need to match?
        String Names = "tg_name||'.'||tg_value";
        //Fire off the query
        ArrayList<AbstractMap.SimpleEntry<Integer, Integer>> Matched = ConnectionManager.getInstance().checkTopoExisting(Check, Names,saveGeneric.tablePrefix,saveGeneric.getTableName());
        for(AbstractMap.SimpleEntry<Integer, Integer> key : Matched){
            //From the output we will only get those outputs that have an Id already.
            //Save this Id and remove the output from the list of those to save.
            algoGenerics.get(key.getKey()).set_id(key.getValue());
            toSaveGenerics.remove(algoGenerics.get(key.getKey()));
        }
        
        //Save all the inputs we did not find in the DB in one shot.
        TreeMap<Integer,Integer> outGenerics = saveGeneric.batchsave(toSaveGenerics);
        logger.log(Level.INFO, "Saving {0} generics not already in DB", outGenerics.size());
        for(Integer Key:outGenerics.keySet()){
            if(outGenerics.get(Key) > 0){
                for(int j=0; j<algoGenerics.size();j++){
                    if(algoGenerics.get(j).equals(toSaveGenerics.get(Key))){
                        algoGenerics.get(j).set_id(outGenerics.get(Key));
                    }
                }
            }
        }
        return algoGenerics;
    }
    
    private ArrayList<TopoParameter> read_parameters(NodeList parameter_list, String type){
        
        ArrayList<TopoParameter> parameters = new ArrayList<>();

        int ParameterLength = parameter_list.getLength();
        for (int i = 0; i < ParameterLength; ++i) {
            Element parameter_node = (Element) parameter_list.item(i);
            TopoParameter newParameter = new TopoParameter();
            newParameter.set_name(parameter_node.getAttribute("name"));
            newParameter.set_value(parameter_node.getAttribute("value"));
            newParameter.set_position(Integer.parseInt(parameter_node.getAttribute("pos")));
            if(!(parameter_node.getAttribute("selection").isEmpty())){
                newParameter.set_selection(Integer.parseInt(parameter_node.getAttribute("selection")));
            }
            parameters.add(newParameter);
        }
          
        return parameters;
    }
    
    private ArrayList<TopoParameter> saveParameters() throws SQLException{
        TopoParameter saveParameter = new TopoParameter(-1);
        ArrayList<TopoParameter> toSaveParameters = new ArrayList<>();
        //remove duplicated inputs. i.e. the same input is used twice. We might want to
        //save them but we can use the same table and link it twice
        for(TopoParameter current:algoParameters){
            if(!toSaveParameters.contains(current)){
                toSaveParameters.add(current);
            }
        }

        int i = 0;
        //Loop over the outputs we read in and form the strings to go into the check query.
        ArrayList<String> Check = new ArrayList<>();
        for(TopoParameter current:algoParameters){
            Check.add(String.valueOf(i));
            String position = current.get_position().toString();
            if(position.isEmpty()) position = "~";
            String selection = current.get_selection().toString();
            if(selection.isEmpty()) selection = "~";
            Check.add(current.get_name()+"."+current.get_value()+"."+position+"."+selection);
            i++;
        }
        //What columns do we need to match?
        String Names = "tp_name||'.'||tp_value||'.'||tp_position||'.'||tp_selection";
        //Fire off the query
        ArrayList<AbstractMap.SimpleEntry<Integer, Integer>> Matched = ConnectionManager.getInstance().checkTopoExisting(Check, Names,saveParameter.tablePrefix,saveParameter.getTableName());
        for(AbstractMap.SimpleEntry<Integer, Integer> key : Matched){
            //From the output we will only get those outputs that have an Id already.
            //Save this Id and remove the output from the list of those to save.
            algoParameters.get(key.getKey()).set_id(key.getValue());
            toSaveParameters.remove(algoParameters.get(key.getKey()));
        }
        
        //Save all the inputs we did not find in the DB in one shot.
        TreeMap<Integer,Integer> outParameters = saveParameter.batchsave(toSaveParameters);
        logger.log(Level.INFO, "Saving {0} parameters not already in DB", outParameters.size());
        for(Integer Key:outParameters.keySet()){
            if(outParameters.get(Key) > 0){
                for(int j =0; j<algoParameters.size();j++){
                    if(algoParameters.get(j).equals(toSaveParameters.get(Key))){
                        algoParameters.get(j).set_id(outParameters.get(Key));
                    }
                }
            }
        }
        return algoParameters;
    }        
    
     /**
     * Need to cross-check algo outputs for decision algos with the output list needs.
     * Aims to loop over the trigger lines in the outputlist. Match these to an id/algoname.
     * Then match the outputs of that algo to the triggerline. Counts all the matches.
     * If != to the output lines then something went wrong....
     * Takes into account the position of the bits on the module/fpga/clock and bit
     */
    private boolean OutputCheck(ArrayList<TopoOutputLine> topo_outputList, ArrayList<TopoAlgo> topo_algoList) throws SQLException, ConfigurationException{
        //Start the count of matched bits
        int matches = 0;
        logger.log(Level.FINE, "Size is {0}", topo_outputList.size());
        //A key to look up the module/fpga/clock position
        ArrayList<Integer> mapKey = new ArrayList<>();
        //Map to remember the bits used for each module/fpga/clock
        HashMap<ArrayList<Integer>,ArrayList<Integer>> mapping = new HashMap<>();
        //Loop over the output list we want to match
        for(int i = 0; i < topo_outputList.size(); i++){
            //Get the output line
            TopoOutputLine currentOutput = topo_outputList.get(i);
            //Get info for matching
            int id = currentOutput.get_algo_id();
            String name = currentOutput.get_algo_name();
            String triggerline = currentOutput.get_triggerline();
            //Need to check we match the bit we are looking at
            boolean matched = false;
            //Loop over the algos (better way to do this?)
            for(TopoAlgo currentAlgo:topo_algoList){
                //logger.info(currentAlgo.toString());
                //If it is not the right algo continue
                //logger.info("Sort "+currentAlgo.get_sort_deci() +" id " + currentAlgo.get_algo_id()+" " +id +" name " +currentAlgo.get_name() + " " +name);
                if(!currentAlgo.get_sort_deci().equals("Decision")) continue;
                if(!currentAlgo.get_algo_id().equals(id)) continue;
                if(!currentAlgo.get_name().equals(name)) continue;
                //logger.info("Is a decision");
                //Loop over the outputs of this bit
                for(TopoAlgoOutput output:currentAlgo.get_outputs()){
                    //Check the bitname (needs modification for outputlines with two attached bits)
                    if(triggerline.contains(",")){
                        String[] split = triggerline.split(",");
                        boolean snap = false;
                        for(String part:split){
                            if(part.equals(output.get_bitname())) snap = true;
                        }
                        if(!snap) continue;
                    } else if(!triggerline.equals(output.get_bitname())) continue;
                    //logger.info("Algo is "+currentAlgo.get_name());
                    //Go get the key for this output (module/fpga/clock)
                    mapKey = setMapKey(currentOutput);
                    //Get the current bits for this key. If it is null then return a new list of 16 0's.
                    ArrayList<Integer> placement = mapping.get(mapKey);
                    if(placement == null){
                        placement = cleanPlacement();
                        //logger.info("Start from scratch " +placement);
                    }
                    //Check if the bit has been taken. If not then fill it.
                    //This can handle two output bits to one triggerline by taking the selection into account
                    int firstbit = currentOutput.get_first_bit();
                    int selection = output.get_selection();
                    //For each module/fpga/clock option there are only 16 bits (0-15). Throw if trying to use one past this point.
                    if(firstbit+selection >= 16){
                        logger.log(Level.SEVERE, "We tried to put a bit beyond the number of possible bits for L1Topo module/fpga/clock = {0}. The bad position was {1}.", new Object[]{mapKey, firstbit+selection});
                        throw new ConfigurationException("We tried to put a bit beyond the number of possible bits for L1Topo module/fpga/clock = "+mapKey+". The bad position was "+(firstbit+selection)+".");
                    }
                    logger.log(Level.FINE, "Firstbit {0} selection {1}", new Object[]{firstbit, selection});
                    if(placement.get(firstbit+selection) == 0){
                        placement.set(firstbit+selection, 1);
                        mapping.put(mapKey, placement);
                        matched = true;
                        if(selection == 0) matches++;
                        logger.log(Level.FINEST, "Map position is {0}", mapKey);
                        logger.log(Level.FINEST, "Size is {0} size currently {1}", new Object[]{topo_outputList.size(), matches});
                        logger.log(Level.FINEST, "Map is {0}", placement);
                    } else {
                        logger.log(Level.SEVERE, "We tried to put a bit on top of an already used bit. The module/fpga/clock was {0}. The position was {1}.", new Object[]{mapKey, firstbit+selection});
                        throw new ConfigurationException("We tried to put a bit on top of an already used bit. The module/fpga/clock was "+mapKey+". The position was "+(firstbit+selection)+".");
                    }
                }
            } 
            if(matched == false){
                logger.log(Level.SEVERE, "We failed to match an output trigger line to an algorithm in the L1Topo Menu. The output line in question was called {0}", triggerline);
                throw new ConfigurationException("We failed to match an output trigger line to an algorithm in the L1Topo Menu. The output line in question was called "+triggerline);
            }
        }
        
        return matches == topo_outputList.size();
    }
    /*
     * Sets the key for the module/fpga/clock we are looking at.
     */
    private ArrayList<Integer> setMapKey(TopoOutputLine current){
        ArrayList<Integer> mapKey = new ArrayList<>();
        mapKey.add(0, current.get_module());
        mapKey.add(1, current.get_fpga());
        mapKey.add(2, current.get_clock());
        return mapKey;
    }
    /*
     * Returns a list of 0's for a fresh module/fpga/clock combo
     */
    private ArrayList<Integer> cleanPlacement(){
        ArrayList<Integer> content = new ArrayList<>();
        for(int i = 0; i<16; i++){
            content.add(i, 0);
        }
        return content;
    }

    // /Read the trigger menu from the XML file.
    /**
     * Load a trigger menu from XML.
     * 
     * @return ID of the trigger menu.
     */
    private TopoMenu saveMenu(final String configname, TopoOutputList outputs,
                              ArrayList<TopoConfig> configList, ArrayList<TopoAlgo> algoList) throws SQLException{

        sections = document.getElementsByTagName(TopoMenu.XML_MENU_TAG);

        TopoMenu needthis = new TopoMenu(-1);
        if (sections.getLength() == 1) {
            Element menu = (Element) this.sections.item(0);

            // set the name as supplied
            needthis.set_name(configname);
            //needthis.set_version(Integer.parseInt(menu.getAttribute("menu_version")));
            needthis.set_ctplink(outputs.get_id());
            needthis.add_algo(algoList);
            needthis.add_config(configList);
            needthis.add_outputlist(outputs);
        }

        //Loop over the outputs we read in and form the strings to go into the check query.
        ArrayList<String> Check = new ArrayList<>();
        Check.add(String.valueOf(0));//+"."+needthis.get_version()
        Check.add(needthis.get_name()+"."+needthis.get_ctplink());

        //Wha columns do we need to match?'.'||ttm_version||
        String Names = "ttm_name||'.'||ttm_ctplink_id";
        //Fire off the query
        //System.out.println("here!?!");
        ArrayList<AbstractMap.SimpleEntry<Integer,Integer>> Matched = ConnectionManager.getInstance().checkTopoExisting(Check, Names,needthis.tablePrefix,needthis.getTableName());
             
        //Now need to check the one we matched was linked to all the correct things
        //First do we link to all the right algos?
        ArrayList<String> algoIDs = new ArrayList<>();
        for(TopoAlgo algo:algoList){
            algoIDs.add(String.valueOf(algo.get_id()));
        }
        ArrayList<Integer> matchedAlgos = ConnectionManager.getInstance().checkLinkExisting(algoIDs, "TTM2TA_MENU_ID","TTM_TO_TA", "TTM2TA_ALGO_ID");
/*for(Integer algo : matchedAlgos){
    System.out.println("matched algo " + algo);
} */       
        //Second do we link to all the right configs?
        ArrayList<String> configIDs = new ArrayList<>();
        for(TopoConfig config:configList){
            configIDs.add(String.valueOf(config.get_id()));
        }
        ArrayList<Integer> matchedConfigs = new ArrayList<>();
        if(!configIDs.isEmpty()){
            matchedConfigs = ConnectionManager.getInstance().checkLinkExisting(configIDs, "TTM2TC_MENU_ID","TTM_TO_TC", "TTM2TC_CONFIG_ID");
            //System.out.println("here???");

        } /*else {
            //matchedConfigs.add(Matched.get(0).getValue());
            System.out.println("here?");
        }*/
        /*for(Integer config : matchedConfigs){
            System.out.println("matched configs " + config);
        } */       
        
        for (AbstractMap.SimpleEntry<Integer, Integer> key : Matched) {
            //System.out.println("topo menu matching key " + key.getKey() + " value " + key.getValue());
            if (!matchedConfigs.isEmpty()) {
                if (matchedAlgos.contains(key.getValue()) && (matchedConfigs.contains(key.getValue()))) {
                    needthis.set_id(key.getValue());
                }
            } else if (matchedAlgos.contains(key.getValue())) {
                needthis.set_id(key.getValue());
            }
        }
        
        if(needthis.get_id() < 0){
            int id = needthis.save();
            needthis.set_id(id);
            ArrayList<TTM_TA> algoLinks = new ArrayList<>();
            for(TopoAlgo algo:needthis.get_algos()){
                TTM_TA link = new TTM_TA(-1);
                link.set_algo_id(algo.get_id());
                link.set_menu_id(needthis.get_id());
                algoLinks.add(link);
            }
            ArrayList<TTM_TC> configLinks = new ArrayList<>();
            for(TopoConfig config:needthis.get_configs()){
                TTM_TC link = new TTM_TC(-1);
                link.set_config_id(config.get_id());
                link.set_menu_id(needthis.get_id());
                configLinks.add(link);
            }
            if(!algoLinks.isEmpty()){
                TreeMap<Integer,Integer> algoFinalLinks = algoLinks.get(0).batchsave(algoLinks);
            } else {
                logger.warning("We are saving an L1Topo menu with no algos?");
            }
            if(!configLinks.isEmpty()){
                TreeMap<Integer,Integer> configFinalLinks = configLinks.get(0).batchsave(configLinks);
            }
        }
        else{
            logger.info("This topo menu already exists");
        }
        
        return needthis;
    }

    /**
     * write the Topo Master Table
     * 
     * @param menu_id
     *            menu ID
     */    
    private TopoMaster saveMaster(TopoMenu outputMenu) throws SQLException {
        TopoMaster outputMaster = new TopoMaster(-1);
        outputMaster.set_name(outputMenu.get_name());
        outputMaster.set_version(1);
        outputMaster.set_comment("");
        outputMaster.set_menu_id(outputMenu.get_id());
        //Temporary default.
        outputMaster.set_hash(99);
        
        //Loop over the outputs we read in and form the strings to go into the check query.
        ArrayList<String> Check = new ArrayList<>();
        Check.add(String.valueOf(0));
        Check.add(outputMaster.get_name()+"."+outputMaster.get_version()+"."+outputMaster.get_menu_id()+"."+outputMaster.get_hash());

        //Wha columns do we need to match?
        String Names = "tmt_name||'.'||tmt_version||'.'||tmt_trigger_menu_id||'.'||tmt_hash";
        //Fire off the query
        ArrayList<AbstractMap.SimpleEntry<Integer, Integer>> Matched = ConnectionManager.getInstance().checkTopoExisting(Check, Names,outputMaster.tablePrefix,outputMaster.getTableName());
        
        switch (Matched.size()) {
            case 1:
                logger.info("This topo master table already exists");
                for(AbstractMap.SimpleEntry<Integer, Integer> key : Matched) {
                    outputMaster.set_id(key.getValue());
                } break;
            case 0:
                logger.info("Saving new topo master table");
                int id = outputMaster.save();
                outputMaster.set_id(id);
                break;
            default:
                logger.severe("Panic, we returned multiple master tables?!?");
                break;
        }
        
        return outputMaster;
    }
    
    /**
     * Perform a check on a file to see if the basic xml is valid.
     */
    private Boolean IsXmlValid() {
        return faults.isEmpty();
    }

    //Check the topo XML has everything as you would expect it to 
    private void ValidateXml() {
        CheckOutputLine();
        CheckConfig();
    }

    private void CheckOutputLine() {
        //Output List
        int count = document.getElementsByTagName("OutputList").getLength();
        NodeList OutputLine = document.getElementsByTagName("OutputList");
        if (count != 1) {
            if (count < 1) {
                faults.add("OutputList XML node missing");
            } else {
                faults.add("Too many OutputList XML nodes.  Should have 1, found " + count);
            }
        } else {
            Element section = (Element) OutputLine.item(0);
            NodeList List_outputs = section.getElementsByTagName("Output");

            if(List_outputs.getLength() == 0){
                faults.add("There are no output lines in the OutputList. I think you might want to add some.");
            }
            for(int i = 0; i < List_outputs.getLength(); i++){
                Element output_node = (Element) List_outputs.item(i);
                if (output_node.getAttribute("algname").length() == 0) {
                    faults.add("An Output line node is missing an attribute algname=\"a_string\"");
                }
                if (output_node.getAttribute("triggerline").length() == 0) {
                    faults.add("An Output line node is missing an attribute triggerline=\"a_string\"");
                }
                if (output_node.getAttribute("algoId").length() == 0) {
                    faults.add("An Output line node is missing an attribute algoId=\"a_number\"");
                }
                if (output_node.getAttribute("module").length() == 0) {
                    faults.add("An Output line node is missing an attribute module=\"a_number\"");
                }
                if (output_node.getAttribute("fpga").length() == 0) {
                    faults.add("An Output line  node is missing an attribute fpga=\"a_number\"");
                }
                if (output_node.getAttribute("firstbit").length() == 0) {
                    faults.add("An Output line node is missing an attribute firstbit=\"a_number\"");
                }
                if (output_node.getAttribute("clock").length() == 0) {
                    faults.add("An Output line node is missing an attribute clock=\"a_number\"");
                }
            }
        }
    }
    
    private void CheckConfig() {
        // Config
        int count = document.getElementsByTagName("TopoConfig").getLength();
        NodeList OutputLine = document.getElementsByTagName("TopoConfig");
        if (count != 1) {
            if (count < 1) {
                faults.add("Config XML node missing");
            } else {
                faults.add("Too many Config XML nodes. Should have 1, found " + count);
            }
        } else {
            Element section = (Element) OutputLine.item(0);
            NodeList List_configs = section.getElementsByTagName("Entry");
            //I think it might be possible to have no config nodes.....
            /*if(List_configs.getLength() == 0){
                faults.add("There are no configs in the list. I think you might want to add some.");
            }
            for(int i = 0; i < List_configs.getLength(); i++){
                Element output_node = (Element) List_configs.item(i);
                if (output_node.getAttribute("name").length() == 0) {
                    faults.add("A Config node is missing an attribute name=\"a_string\"");
                }
                if (output_node.getAttribute("value").length() == 0) {
                    faults.add("A Config node is missing an attribute value=\"a_number\"");
                }
            }*/
        }
    }
    
    boolean tryParseInt(String value)
    {
        try{
            Integer.parseInt(value);
            return true;
        }catch(NumberFormatException ex)
        {
            return false;
        }
    }
}
