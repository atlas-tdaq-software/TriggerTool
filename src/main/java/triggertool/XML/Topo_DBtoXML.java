package triggertool.XML;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.logging.Level;



import triggerdb.Connections.ConnectionManager;

//Import all of the topological tables. We are going to need them....
import triggerdb.Entities.Topo.*;

/**
 * This class contains all the code to convert the Database into a Topo XML file
 * Write a Topo XML file from a Topo Master key.
 * 
 * @author Alex Martyniuk
 */
public final class Topo_DBtoXML {

    /** Message Log */
    protected static final Logger logger = Logger.getLogger("TriggerTool");

    /**
     * Function to create the dtd file. This is required by the Trigger Menu
     * Compiler, so that it accepts the XML file as input. It describes the XML
     * schema we use.
     * 
     * @param filename
     *            Output filename including full path
     */
    private void writeDTD(String filename) {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(filename));

            out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n");
            out.write("<!ENTITY % ctpin_name '(SLOT7 | SLOT8 | SLOT9) \"SLOT9\"'>\n");
            out.write("<!ENTITY % cable_name '(CON0 | CON1 | CON2 | CON3) \"CON0\"'>\n");
            out.write("<!ENTITY % module_name '(CTPIN | CTPMON) \"CTPIN\"'>\n");
            out.write("<!ENTITY % trigger_name '(BGRP0 | BGRP1 | BGRP2 | BGRP3 | BGRP4 | BGRP5 | BGRP6 | BGRP7 | BGRP8 | BGRP9 | BGRP10 | BGRP11 | BGRP12 | BGRP13 | BGRP14 | BGRP15  RNDM0 | RNDM1 | RNDM2 | RNDM3 | PCLK0 | PCLK1) '>\n\n");

            out.write("<!ELEMENT LVL1Config (TriggerMenu,PrescaleSet,PrioritySet,TriggerCounterList?,Random," + "BunchGroupSet,PrescaledClock,MuctpiInfo,CaloInfo)>\n\n");

            out.write("<!ATTLIST LVL1Config\n" + "id                 CDATA           #IMPLIED\n" + "name               CDATA           #REQUIRED\n" + "version            CDATA           #IMPLIED>\n\n");

            out.write("<!ELEMENT TriggerMenu (TriggerItem)+>\n" + "<!ATTLIST TriggerMenu\n" + "id                 CDATA           #REQUIRED\n" + "name               CDATA           #REQUIRED\n" + "version            CDATA           #IMPLIED\n" + "phase              CDATA           #IMPLIED>\n\n");

            out.write("<!ELEMENT  TriggerItem (AND|OR|NOT|TriggerCondition|InternalTrigger)>\n" + "<!ATTLIST  TriggerItem\n" + "id                 CDATA           #IMPLIED\n" + "name               CDATA           #REQUIRED\n" + "version            CDATA           #IMPLIED\n" + "ctpid              CDATA           #REQUIRED\n" + "definition         CDATA           #REQUIRED\n" + "trigger_type       CDATA           #REQUIRED\n" + "comment            CDATA           #REQUIRED\n" + "group              CDATA           #REQUIRED>\n\n");

            out.write("<!ELEMENT AND (AND|OR|NOT|TriggerCondition|InternalTrigger)+>\n");
            out.write("<!ELEMENT OR (AND|OR|NOT|TriggerCondition|InternalTrigger)+>\n");
            out.write("<!ELEMENT NOT (AND|OR|NOT|TriggerCondition|InternalTrigger)>\n\n");

            out.write("<!ELEMENT TriggerCondition EMPTY>\n" + "<!ATTLIST TriggerCondition\n" + "triggerthreshold   IDREF           #REQUIRED\n" + "name               CDATA           #REQUIRED\n" + "multi              CDATA           #REQUIRED>\n\n");

            out.write("<!ELEMENT InternalTrigger EMPTY>\n" + "<!ATTLIST InternalTrigger\n" + "id                 CDATA           #IMPLIED\n" + "name               %trigger_name;  #REQUIRED>\n\n");

            out.write("<!ELEMENT TriggerCounter (AND|OR|NOT|TriggerCondition)>\n" + "<!ATTLIST TriggerCounter\n" + "name               CDATA           #REQUIRED\n" + "type               %module_name;>\n\n");

            out.write("<!ELEMENT TriggerThresholdList (TriggerThreshold*)>\n\n");

            out.write("<!ELEMENT TriggerThreshold (TriggerThresholdValue*, Cable)>\n" + "<!ATTLIST TriggerThreshold\n" + "id                 CDATA           #IMPLIED\n" + "name               ID              #REQUIRED\n" + "version            CDATA           #IMPLIED\n" + "type               CDATA           #REQUIRED\n" + "bitnum             CDATA           #REQUIRED\n" + "OPL                CDATA           \"NO\"\n" + "confirm            CDATA           \"0\"\n" + "active             CDATA           #REQUIRED\n" + "bcdelay            CDATA           \"\"\n" + "seed               CDATA           \"\"\n" + "seed_multi         CDATA           \"\" \n" + "mapping            CDATA           #REQUIRED>\n\n");

            out.write("<!ELEMENT TriggerThresholdValue EMPTY>\n" + "<!ATTLIST TriggerThresholdValue\n" + "id                 CDATA           #IMPLIED\n" + "name               CDATA           #REQUIRED\n" + "version            CDATA           #IMPLIED\n" + "type               CDATA           #REQUIRED\n" + "thresholdval       CDATA           #REQUIRED\n" + "em_isolation       CDATA           \"5\"\n" + "had_isolation      CDATA           \"10\"\n" + "had_veto           CDATA           \"20\"\n" + "window             CDATA           \"2\"\n" + "phimin             CDATA           #REQUIRED\n" + "phimax             CDATA           #REQUIRED\n" + "etamin             CDATA           #REQUIRED\n" + "etamax             CDATA           #REQUIRED\n" + "priority           CDATA           #REQUIRED>\n\n");

            out.write("<!ELEMENT Cable (Signal+)>\n" + "<!ATTLIST Cable\n" + "name               CDATA           #REQUIRED\n" + "ctpin              %ctpin_name;\n" + "connector          %cable_name;>\n\n");

            out.write("<!ELEMENT Signal EMPTY>\n" + "<!ATTLIST Signal \n" + "range_begin        CDATA           #REQUIRED\n" + "range_end          CDATA           #REQUIRED>\n\n");

            out.write("<!ELEMENT MuctpiInfo (low_pt+ ,high_pt+, max_cand+) >\n" + "<!ATTLIST MuctpiInfo\n" + "name               CDATA            #REQUIRED\n" + "version            CDATA            \"\">\n\n");

            out.write("<!ELEMENT low_pt (#PCDATA)>\n");
            out.write("<!ELEMENT high_pt (#PCDATA)>\n");
            out.write("<!ELEMENT max_cand (#PCDATA)>\n\n");

            out.write("<!ELEMENT Deadtime EMPTY>\n" + "<!ATTLIST Deadtime\n" + "name               CDATA           #REQUIRED\n" + "version            CDATA           \"\"\n" + "complex1_level     CDATA           #REQUIRED\n" + "complex1_rate      CDATA           #REQUIRED\n" + "complex2_level     CDATA           #REQUIRED\n" + "complex2_rate      CDATA           #REQUIRED\n" + "simple             CDATA           #REQUIRED>\n\n");

            out.write("<!ELEMENT CaloInfo (JetWeight* , METSignificance*)>\n" + "<!ATTLIST CaloInfo\n" + "name              CDATA            #REQUIRED\n" + "version           CDATA            \"\"\n" + "global_scale      CDATA            #REQUIRED>\n\n");

            out.write("<!ELEMENT JetWeight (#PCDATA)>\n" + "<!ATTLIST JetWeight\n" + "num               CDATA            #REQUIRED>\n\n");

            out.write("<!ELEMENT METSignificance EMPTY>\n<!ATTLIST METSignificance\nteSqrtMax         CDATA            #REQUIRED\nteSqrtMin         CDATA            #REQUIRED\nxeMax             CDATA            #REQUIRED\nxeMin             CDATA            #REQUIRED\nxsSigmaOffset     CDATA            #REQUIRED\nxsSigmaScale      CDATA            #REQUIRED>\n\n");

            out.write("<!ELEMENT BunchGroupSet (BunchGroup*,Bunch*)>\n" + "<!ATTLIST BunchGroupSet\n" + "name              CDATA            #REQUIRED\n" + "version           CDATA            \"\">\n\n");

            out.write("<!ELEMENT BunchGroup (Bunch*)>\n" + "<!ATTLIST BunchGroup\n" + "internalNumber    CDATA            #REQUIRED\n" + "name              CDATA            #REQUIRED\n" + "version           CDATA            \"\">\n\n");

            out.write("<!ELEMENT Bunch EMPTY>\n" + "<!ATTLIST Bunch\n" + "bunchNumber       CDATA            #REQUIRED>\n\n");

            out.write("<!ELEMENT PrescaledClock EMPTY>\n" + "<!ATTLIST PrescaledClock\n" + "name              CDATA            #REQUIRED\n" + "version           CDATA            \"\"\n" + "clock1            CDATA            #REQUIRED\n" + "clock2            CDATA            #REQUIRED>\n\n");

            out.write("<!ELEMENT Random EMPTY>\n" + "<!ATTLIST Random\n" + "name               CDATA           #REQUIRED\n" + "version            CDATA           #IMPLIED\n" + "rate1              CDATA           #REQUIRED\n" + "seed1              CDATA           #REQUIRED\n" + "rate2              CDATA           #REQUIRED\n" + "seed2              CDATA           #REQUIRED>\n\n");

            out.write("<!ELEMENT PrescaleSet (Prescale*)>\n" + "<!ATTLIST PrescaleSet \n" + "name               CDATA           #REQUIRED\n" + "version            CDATA           #IMPLIED>\n\n");

            out.write("<!ELEMENT Prescale (#PCDATA)>\n" + "<!ATTLIST Prescale\n" + "ctpid              CDATA           #REQUIRED>\n\n");

            out.write("<!ELEMENT PrioritySet (Priority*)>\n<!ATTLIST PrioritySet\nname              CDATA        \"\"\nversion           CDATA        \"\">\n\n");

            out.write("<!ELEMENT Priority (#PCDATA)>\n" + "<!ATTLIST Priority\n" + "ctpid              CDATA           #REQUIRED>\n\n");

            out.close();

        } catch (IOException e) {
            logger.log(Level.WARNING, "Failed to write dtd - {0}", e.getMessage());
        }
    }

    /**
     * Write the XML. This is the main code to save the file. XML tags are
     * written by hand. If a prescale_id of less than or equal to 0 is supplied
     * then the prescales are not written to the file. TODO: This method writes
     * out L1PrescaleValues as FLOATs (L1_XMLtoDB reads floats in). this has to
     * change: In the XML we want have FLOAT and NMD info, either as 3 integers
     * or as a HEX.
     * 
     * @param topomt_id
     *            ID for the L1 Master table we want to save to a file
     * @param filename
     *            Filename that we're going to save to
     * @param dtdFlag
     *            Flag to turn on printing dtd or not. 0 off 1 on.
     * @throws java.sql.SQLException
     */
    public void writeXML(int topomt_id, String filename, boolean dtdFlag) throws SQLException {
        try {
            if (dtdFlag) {
                writeDTD(filename.replace(".xml", "-dtd.dtd"));
            }

            logger.log(Level.INFO, "About to write Topo XML file: {0} for {1}", new Object[]{filename, topomt_id});
            BufferedWriter out = new BufferedWriter(new FileWriter(filename));

            // load the data
            TopoMaster topoMaster = new TopoMaster(topomt_id);
            
            Calendar cal = Calendar.getInstance(TimeZone.getDefault());

            String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT);

            sdf.setTimeZone(TimeZone.getDefault());

            logger.fine("Writing Topo level node");

            out.write("<?xml version=\"1.0\" ?>\n");
            if (dtdFlag) {
                String dtd = filename.replace(".xml", "-dtd.dtd");
                if (dtd.lastIndexOf("/") > 0) {
                    dtd = dtd.substring(1 + dtd.lastIndexOf("/"));
                }
                out.write("<!DOCTYPE LVL1Config SYSTEM \"" + dtd + "\">\n");
            }            
            
            TopoMenu topoMenu = topoMaster.get_menu();
            logger.log(Level.FINE, "Master Id {0}", topoMenu.get_id());
            TopoOutputList list = topoMenu.get_outputlist();
            //Need to get these lists sorted. Currently come out in the order the DB wants
            ArrayList<TopoAlgo> Algos = topoMenu.get_algos();
            ArrayList<TopoConfig> Configs = topoMenu.get_configs();
            
            //Write out the header of the XML
            String ver = triggertool.TriggerTool.TT_TAG;
            String usr = ConnectionManager.getInstance().getInitInfo().getUserName();
            out.write("<TOPO_MENU menu_name=\"" + topoMaster.get_name() + "\" menu_version=\"" + topoMaster.get_version() + "\">\n");
            String header = "  <!--This file was generated by the TriggerTool (ver. " + ver + ") -->\n"
                    + "  <!--By " + usr + " on " + sdf.format(cal.getTime()) + "-->\n"
                    + "  <!--No. L1Topo algos defined: "+Algos.size()+"-->\n";
            out.write(header);
            
            //Write out the outputlist node
            logger.fine("Writing Topo OutputList node");
            out.write("  <OutputList>\n");
            ArrayList<TopoOutputLine> Lines = list.get_output_lines();
            for(TopoOutputLine line:Lines){
                out.write("    <Output algname=\"" + line.get_algo_name() + "\" triggerline=\"" + line.get_triggerline() + "\" algoId=\"" + line.get_algo_id() 
                    + "\" module=\"" + line.get_module() + "\" fpga=\"" + line.get_fpga() + "\" firstbit=\"" + line.get_first_bit() + "\" clock=\"" + line.get_clock() + "\"/>\n");
            }
            out.write("  </OutputList>\n\n");
            
            //Write out the congif node
            logger.fine("Writing Topo Config node");
            out.write("  <TopoConfig>\n");
            for(TopoConfig config:Configs){
                out.write("    <Entry name=\"" + config.get_name() + "\" value=\"" + config.get_value() +"\"/>\n");
            }
            out.write("  </TopoConfig>\n\n");
            //Now write out all the Algos
            //First do the Sort algos. The go 0,1,2... in the Algo_id, decision start again from 0
            for(TopoAlgo algo:Algos){
                if(algo.get_sort_deci().equals("Sort")){
                    out = writeAlgos(out,algo);
                }
            }
            //First do the Sort algos. The go 0,1,2... in the Algo_id, decision start again from 0
            for(TopoAlgo algo:Algos){
                if(algo.get_sort_deci().equals("Decision")){
                    out = writeAlgos(out,algo);
                }
            }
            out.write("</TOPO_MENU>");
            out.close();

            logger.log(Level.INFO, "Finished writing L1 XML {0}", filename);
        } catch (IOException e) {
            logger.log(Level.WARNING,"Some error while writing L1 XML" + " {0}", e.getStackTrace());
        }
    }
    
    private BufferedWriter writeAlgos(BufferedWriter out, TopoAlgo algo) throws IOException, SQLException{
            //Need to open a node depending on what type of algorithm it is
            if(algo.get_sort_deci().equals("Sort")){
                out.write("  <SortAlgo type=\""+algo.get_type()+"\" name=\""+algo.get_name()+"\" output=\""+algo.get_output()+"\" algoId=\""+algo.get_algo_id()+"\">\n");
            } else if(algo.get_sort_deci().equals("Decision")) {
                out.write("  <DecisionAlgo type=\""+algo.get_type()+"\" name=\""+algo.get_name()+"\" algoId=\""+algo.get_algo_id()+"\">\n");
            }
            //Now do the fixed section
            out.write("    <Fixed>\n");
                ArrayList<TopoAlgoInput> Inputs = algo.get_inputs();
                logger.log(Level.FINER, "Inputs size {0}", Inputs.size());
                ArrayList<TopoAlgoOutput> Outputs = algo.get_outputs();
                logger.log(Level.FINER, "Outputs size {0}", Outputs.size());
                ArrayList<TopoGeneric> Generics = algo.get_generics();
                logger.log(Level.FINER, "Generics size {0}", Generics.size());
                //Write the inputs. These have a slightly different layout depending on the algo type
                for(TopoAlgoInput input:Inputs){
                    if(algo.get_sort_deci().equals("Sort")){
                        out.write("      <Input name=\"" + input.get_name() + "\" value=\"" + input.get_value() +"\"/>\n");
                    } else if(algo.get_sort_deci().equals("Decision")){
                        if(input.get_position() < 0){
                            out.write("      <Input name=\"" + input.get_name() + "\" value=\"" + input.get_value()+"\"/>\n");
                        } else {
                            out.write("      <Input name=\"" + input.get_name() + "\" value=\"" + input.get_value() 
                                            + "\" position=\"" + input.get_position() +"\"/>\n");
                        }
                    }
                }
                //Write the outputs. These are completely different depending on algo type!
                if(algo.get_sort_deci().equals("Decision")){
                    //Dangerous to hard code this? What if it is not Results?!
                    out.write("      <Output name=\"" + Outputs.get(0).get_name() + "\" bits=\"" + Outputs.size() +"\">\n");
                }
                for(TopoAlgoOutput output:Outputs){
                    if(algo.get_sort_deci().equals("Sort")){
                        out.write("      <Output name=\"" + output.get_name() + "\" value=\"" + output.get_value() +"\"/>\n");
                    } else if(algo.get_sort_deci().equals("Decision")){
                        out.write("        <Bit selection=\"" + output.get_selection() + "\" name=\"" + output.get_bitname()+"\"/>\n");
                    }
                }
                if(algo.get_sort_deci().equals("Decision")){
                    //Dangerous to hard code this? What if it is not Results?!
                    out.write("      </Output>\n");
                }
                //Write the generics. These are non-awkward.
                for(TopoGeneric generic:Generics){
                    out.write("      <Generic name=\"" + generic.get_name() + "\" value=\"" + generic.get_value() +"\"/>\n");
                }

            out.write("    </Fixed>\n");
            //Now do the variable section
            out.write("    <Variable>\n");
            ArrayList<TopoParameter> Parameters = algo.get_parameters();
            logger.log(Level.FINE, "Generics size {0}", Generics.size());
            //Write the parameters. These are slightly non-awkward.
                for(TopoParameter parameter:Parameters){
                    if(algo.get_sort_deci().equals("Sort")){
                        out.write("      <Parameter pos=\""+parameter.get_position()+"\" name=\"" + parameter.get_name() + "\" value=\"" + parameter.get_value() +"\"/>\n");
                    } else if (algo.get_sort_deci().equals("Decision")){
                        if(parameter.get_selection() < 0){
                            out.write("      <Parameter pos=\""+parameter.get_position()+"\" name=\"" + parameter.get_name() + "\" value=\"" + parameter.get_value() +"\"/>\n");
                        } else {
                            out.write("      <Parameter pos=\""+parameter.get_position()+"\" name=\"" + parameter.get_name() 
                                     +"\" selection=\"" + parameter.get_selection()+ "\" value=\"" + parameter.get_value() +"\"/>\n");                                
                        }
                    }
                }
            out.write("    </Variable>\n");

            //And now just need to close the node depending on the algorithm type
            if(algo.get_sort_deci().equals("Sort")){
                out.write("  </SortAlgo>\n\n");
            } else if(algo.get_sort_deci().equals("Decision")) {
                out.write("  </DecisionAlgo>\n\n");
            }
        return out;
    }
}
