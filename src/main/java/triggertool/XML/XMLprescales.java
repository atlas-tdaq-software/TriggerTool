package triggertool.XML;

import java.util.List;
import java.io.File;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import triggertool.Panels.PrescaleEditPanel.HLTPrescaleRow;
import triggertool.Panels.PrescaleEditPanel.L1PrescaleEditItemNode;

/**
 *
 * Reads a "simple" XML file containing only prescales, finds out whether it is
 * an L1 or HLT file, and creates Map similar to that used by the table on the
 * prescale editor.
 *
 * @author tiago perez
 */
public final class XMLprescales {

    /**
     * the logger.
     */
    private static final Logger logger = Logger.getLogger("TriggerTool");
    // Constants
    /**
     * The LVL1 .
     */
    public static final int LEVEL1 = 0;
    /**
     * The HLT .
     */
    public static final int HLT = 1;
    // Variables    
    private Document document;
    private final String filename;
    private int level = -1;
    private String prescaleSetName;
    Integer prescaleSetPartition = 0;

    /**
     *
     */
    public LinkedHashMap<String, ArrayList<Object>> prescaleSetMap;

    /**
     *
     * @param file
     */
    public XMLprescales(final String file) {
        this.filename = file;
        this.prescaleSetMap = new LinkedHashMap<>();
    }

    /**
     * Get the prescale set name.
     *
     * @return the name of the prescale set in the XML file.
     */
    public String getPrescaleSetName() {
        return this.prescaleSetName;
    }

    /**
     *
     * @return
     */
    public Integer getPrescaleSetPartition() {
        return this.prescaleSetPartition;
    }

    /**
     *
     * @return
     */
    public int getLevel() {
        return this.level;
    }

    /**
     * Returns the name of the XML file.
     *
     * @return the original file name.
     */
    public String getFileName() {
        return this.filename;
    }

    /**
     * Updates the given LinkedMap (representing the L1 prescale table in
     * PrescaleEdit) and updates it with the values from the XML file.
     *
     * Value for items not present in XML are taken from existing Map or or if
     * useTableAsRef==false are set to -1.
     *
     * @param l1m a HashMap containing a L1 prescale set.
     * @param useTableAsRef A boolean flag, if true, values of the current table
     * are updated, else it sets everything not present to -1.
     * @return <code>true</code> if ok, else <code>false</code>.
     */
    public final boolean updatePrescaleSetL1(
            final LinkedHashMap<Integer, L1PrescaleEditItemNode> l1m,
            final boolean useTableAsRef) {
        // Read prescales from XML.
        ArrayList<String> notPresent = new ArrayList<>();
        ArrayList<String> present = new ArrayList<>();

        if (l1m.isEmpty()) {
            return false;
        }

        // Loop once over the rows of l1m to check that names match.
        // if not breaking.
        boolean match = true;

        for (Map.Entry<Integer, L1PrescaleEditItemNode> entry : l1m.entrySet()) {
            Integer mapCtpId = entry.getValue().getId();
            if (prescaleSetMap.containsKey(mapCtpId.toString())) {
                String mapName = entry.getValue().getItemName();
                String xmlName = prescaleSetMap.get(mapCtpId.toString()).
                        get(1).toString();
                // Check the item name
                if (mapName == null || xmlName == null
                        || !(mapName.equalsIgnoreCase(xmlName))) {
                    logger.log(Level.SEVERE, " Trying to upload prescale value with "
                            + "different names!\n" + "\t CTP ITEM on DB      : {0}\n\t CTP ITEM in XML file: {1}", new Object[]{mapName, xmlName});
                    match = false;
                }
            }
        }
        if (!match) {
            logger.severe(" CTP Items in XML file and DB do not match,"
                    + " breaking.");
            return false;
        }

        // The default prescale value.
        for (Map.Entry<Integer, L1PrescaleEditItemNode> entry : l1m.entrySet()) {
            Integer mapCtpId = entry.getValue().getId();
            // Compare CTP IDs
            if (prescaleSetMap.containsKey(mapCtpId.toString())) {
                Object value = prescaleSetMap.get(mapCtpId.toString()).get(0);
                present.add(mapCtpId.toString());
                if (value instanceof Number) {
                    double val = Float.parseFloat(value.toString());
                    entry.getValue().setInput(val);
                }

            } else {
                notPresent.add(mapCtpId.toString());
                // If we are creating a new PS set, set empty values to -1
                // Else keep the old ones (update old menu)
                if (!useTableAsRef) {
                    entry.getValue().setCutInHexFormat("-ffffff");
                }
            }
        }

        // Info message.
        if (notPresent.size() > 0) {
            String msg = " Uploading L1PrescaleSet.\n Reading " + present.size()
                    + " items from xmlfile. ";
            if (useTableAsRef) {
                msg += "keeping " + notPresent.size()
                        + " items from the existing PrescaleSet.";
            } else {
                msg += "Applying default value (-1) for " + notPresent.size() + " not present items.";
            }
            logger.info(msg);
        }
        return true;
    }

    /**
     * Iterates the given lhm Map and compares the values with the items in
     * this.prescaleMap, ie. the Map read form the XML file.
     *
     * @param lhm a Map contaning HLT trigger items as in PrescaleEdit HLT
     * table.
     * @return
     *
     */
    public final List<String> updatePrescaleSetHLT(
            final LinkedHashMap<String, HLTPrescaleRow> lhm) {
        // Read prescales from XML.
        LinkedHashMap<String, ArrayList<Object>> xmlMap
                = this.ReadPrescaleValuesHLT();
        // These two arrays are for debuging and logging info.
        ArrayList<String> notPresent = new ArrayList<>();
        ArrayList<String> present = new ArrayList<>();
        Set<Map.Entry<String, HLTPrescaleRow>> set = lhm.entrySet();
        Iterator<Map.Entry<String, HLTPrescaleRow>> itr = set.iterator();

        while (itr.hasNext()) {
            Map.Entry<String, HLTPrescaleRow> me = itr.next();
            // Vector containing the chain info as seen on the prescale editor.
            HLTPrescaleRow hltRow = me.getValue();

            String chainName = (String) hltRow.get(HLTPrescaleRow.NAME);
            // If the chain is available in the XML file, use XML data.
            if (xmlMap.containsKey(chainName)) {
                // for Logging/Debugging.
                present.add(chainName);
                // The XML data.
                ArrayList<Object> xmlData = xmlMap.get(chainName);
                // check counter match
                Integer xmlCounter = Integer.parseInt(xmlData.get(0).toString());
                if (hltRow.getCounter().equals(xmlCounter)) {
                    hltRow.setPrescale(xmlData.get(3).toString());
                    hltRow.setPassThrough(xmlData.get(4).toString());
                    hltRow.setStream(xmlData.get(5).toString());
                    //logger.info("rerun again "+xmlData.get(6).toString());
                    hltRow.setReRun(xmlData.get(6).toString());
                } else {
                    //counterMismatch.add(new AbstractMap.SimpleEntry<>(hltRow,xmlCounter));
                    logger.log(Level.WARNING, "Chain counter mismatch for chain {0}: Counter from SMK {1} - Counter from Input File {2}.", new Object[]{chainName, hltRow.getCounter(), xmlCounter});
                }
            } else {
                notPresent.add(chainName);
            }
        }
      /*  if(!counterMismatch.isEmpty()){
            String mismatchMsg = "List of chains set to default values:\n";
            for (AbstractMap.SimpleEntry<HLTPrescaleRow,Integer> mismatch : counterMismatch) {
                mismatchMsg += " Chain " + mismatch.getKey().getName() + " has SMK counter " + mismatch.getKey().getCounter() + " and Input file has " + mismatch.getValue() + "\n";
            }
            
            JOptionPane.showMessageDialog(null,
                    mismatchMsg,
                    "Chain Counter Mismatch",
                    JOptionPane.WARNING_MESSAGE);
        
        }*/
        // Log info.
        if (!notPresent.isEmpty()) {
            logger.log(Level.WARNING, " Uploading HLTPrescaleSet with name : {0}\n{1} items read from xml file.\n Using default values for {2} not present items.", new Object[]{this.getPrescaleSetName(), present.size(), notPresent.size()});
            String fineMsg = "List of chains set to default values:\n";
            for (String np : notPresent) {
                fineMsg += np + "\n";
            }
            logger.warning(fineMsg);
        }
        return notPresent;
    }

    /**
     * Opens the file in XMLpath Calls readHeadInfo() to read Header
     * Information.
     *
     * @return
     */
    public boolean checkFile() {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setIgnoringComments(true);
            factory.setCoalescing(true);
            factory.setNamespaceAware(false);
            factory.setValidating(false);

            DocumentBuilder parser = factory.newDocumentBuilder();
            document = parser.parse(new File(filename));
            this.level = this.ReadHeaderInfo();

        } catch (ParserConfigurationException ex) {
            logger.log(Level.SEVERE, " Error Parsing XML file: {0}. \n{1}", new Object[]{this.filename, ex});
        } catch (SAXException ex) {
            logger.log(Level.SEVERE, " XML file:{0} is badly formatted. \n{1}", new Object[]{this.filename, ex});
        } catch (IOException ex) {
            logger.log(Level.SEVERE, " Error opening XML file: {0}. \n{1}", new Object[]{this.filename, ex});
        }
        return this.level >= 0;

    }

    /**
     * Reads the header of the file and finds out whether the file contains a L1
     * ps set or a HLT ps set.
     *
     * @return the level of the prescale set.
     */
    private int ReadHeaderInfo() {

        int _level;
        NodeList sections;

        String firstChild = document.getFirstChild().getNodeName();

        switch (firstChild) {
            case "HLT_MENU":
                sections = document.getElementsByTagName("HLT_MENU");
                _level = XMLprescales.HLT;
                this.prescaleSetName = XMLprescales.readPrescaleNameHLT(sections);
                this.prescaleSetPartition = XMLprescales.readPrescalePartitionL1(sections);
                break;
            case "LVL1Config":
                sections = document.getElementsByTagName("LVL1Config");
                _level = XMLprescales.LEVEL1;
                this.prescaleSetName = XMLprescales.readPrescaleNameL1(sections);
                break;
            default:
                logger.severe(" XML file is not properly formatted.");
                return -1;
        }
        return _level;
    }

    /**
     * Read the name of the L1 Prescale Set XML file.
     *
     * @param sections a node list containing a L1 Prescale set.
     * @return the name of the prescale set.
     */
    private static String readPrescaleNameL1(final NodeList sections) {
        String setName = "";
        Element section = (Element) sections.item(0);
        NodeList prescaleSet = section.getElementsByTagName("PrescaleSet");
        if (prescaleSet.getLength() == 1) {
            Element element = (Element) prescaleSet.item(0);
            setName = element.getAttribute("name").trim();
        }
        return setName;
    }

    private static Integer readPrescalePartitionL1(final NodeList sections) {
        Integer partition = 0;
        Element section = (Element) sections.item(0);
        NodeList prescaleSet = section.getElementsByTagName("PrescaleSet");
        if (prescaleSet.getLength() == 1) {
            Element element = (Element) prescaleSet.item(0);
            partition = Integer.parseInt(element.getAttribute("menuPartition"));
        }
        return partition;
    }

    /**
     * Read the name of the HLT Prescale Set XML file.
     *
     * @param sections a node list containing a HLT Prescale set.
     * @return the name of the prescale set.
     */
    private static String readPrescaleNameHLT(final NodeList sections) {
        Element section = (Element) sections.item(0);
        String setName = section.getAttribute("prescale_set_name").trim();
        return setName;
    }

    /**
     * Reads the L1 Prescale Set values.
     *
     * @return <code>true</code> if everything was OK, else <code>false</code>.
     */
    public boolean ReadPrescaleValuesL1() {
        if (!checkFile()) {
            return false;
        }

        boolean ret = true;
        NodeList items = null;
        NodeList prescales = null;
        NodeList sections = document.getElementsByTagName("LVL1Config");
        if (sections.getLength() == 1) {
            Element section = (Element) sections.item(0);
            NodeList menus = section.getElementsByTagName("TriggerMenu");
            if (menus.getLength() == 1) {
                Element triggerMenu = (Element) menus.item(0);
                items = triggerMenu.getElementsByTagName("TriggerItem");
            }
            menus = section.getElementsByTagName("PrescaleSet");
            if (menus.getLength() == 1) {
                Element prescaleSet = (Element) menus.item(0);
                prescales = prescaleSet.getElementsByTagName("Prescale");
            }
        }
        // Read the NAMES
        if (items != null) {
            Element item;
            Integer ctpid;
            float psValue = -1;
            int psCut = -1;
            String psName;
            for (int i = 0; i < items.getLength(); i++) {
                item = (Element) items.item(i);
                ctpid = Integer.parseInt(item.getAttribute("ctpid").trim());
                psName = item.getAttribute("name").trim();
                logger.log(Level.FINEST, "Filling with psValue {0} psName {1} psCut {2} ID {3}", new Object[]{psValue, psName, psCut, ctpid});
                ArrayList<Object> psVector = new ArrayList<>(2);
                psVector.add(0, psValue);
                psVector.add(1, psName);
                psVector.add(2, psCut);
                prescaleSetMap.put(ctpid.toString(), psVector);
            }
        } else {
            ret = false;
        }
        // Read the VALUES
        if (prescales != null) {
            Element prescale;
            Integer ctpid;
            float psValue;
            int psCut;
            for (int i = 0; i < prescales.getLength(); i++) {
                prescale = (Element) prescales.item(i);
                ctpid = Integer.parseInt(prescale.getAttribute("ctpid").trim());
                psValue = Float.parseFloat(prescale.getAttribute("value").trim());
                psCut = Integer.parseInt(prescale.getAttribute("cut").trim());

                if (prescaleSetMap.containsKey(ctpid.toString())) {
                    prescaleSetMap.get(ctpid.toString()).set(0, psValue);
                    prescaleSetMap.get(ctpid.toString()).set(2, psCut);
                    logger.log(Level.FINEST, "Filling with psValue {0} psCut {1} ID {2}", new Object[]{psValue, psCut, ctpid});
                } else {
                    ret = false;
                    logger.log(Level.FINE, " No TriggerItem with CPTID : {0}", ctpid);
                }
            }
        } else {
            ret = false;
        }
        return ret;
    }

    /**
     * <p>
     * Reads HLT prescale item form XML file and fills and returns a
     * prescaleSetMap FORMAT:
     * <code>HashMap<String, ArrayList<Object>></code><br> Stri
     * ng : chainName<br>
     * vector :<br>
     * 0 Counter<br>
     * 1 Name<br>
     * 2 pass_thru<br>
     * 3 prescale<br>
     * 4 extpress_stream<br>
     * </p>
     *
     * @return retMap a LinkedHashedMap with all info contained in the XML file
     * formatted as described above.
     */
    private LinkedHashMap<String, ArrayList<Object>> ReadPrescaleValuesHLT() {
        LinkedHashMap<String, ArrayList<Object>> retMap
                = new LinkedHashMap<>();

        NodeList items = null;
        NodeList sections = document.getElementsByTagName("HLT_MENU");
        if (sections.getLength() == 1) {
            Element section = (Element) sections.item(0);
            NodeList menus = section.getElementsByTagName("CHAIN_LIST");
            if (menus.getLength() == 1) {
                Element triggerMenu = (Element) menus.item(0);
                items = triggerMenu.getElementsByTagName("CHAIN");
            }
        }
        // Read the NAMES
        if (items != null) {
            Element item;
            int chainCounter = -1;
            String chainName = "";
            Double prescale = 1.0;
            Double pass_through = 1.0;
            Double express_stream = 1.0;
            String rerun_prescale = "";

            for (int i = 0; i < items.getLength(); i++) {
                item = (Element) items.item(i);
                // Whatch out with the \"!!!
                chainCounter = Integer.parseInt(item.getAttribute(
                        "chain_counter").trim());
                chainName = item.getAttribute("chain_name").trim();
                try {
                    String ps = item.getAttribute("prescale").trim();
                    //System.out.println("Prescale " + ps);
                    prescale = Double.parseDouble(ps);
                } catch (NumberFormatException e) {
                    logger.log(Level.WARNING,  e.toString() + " when attempting to load prescale attribute for Chain {0} Counter {1}. Reverting to default value {2}.", new Object[]{chainName, chainCounter, 1});
                    prescale = 1.0;
                }
                try {
                    String xs = item.getAttribute("express_stream").trim();
                    express_stream = Double.parseDouble(xs);
                } catch (NumberFormatException e) {
                    logger.log(Level.WARNING,  e.toString() + " when attempting to load express_stream attribute for Chain {0} Counter {1}. Reverting to default value {2}.", new Object[]{chainName, chainCounter, 0});
                    express_stream = 0.0;
                }
                
                try {
                    String pt = item.getAttribute("pass_through").trim();
                    //System.out.println("Passthrough " + pt);
                    pass_through = Double.parseDouble(pt);
                } catch (NumberFormatException e) {
                    logger.log(Level.WARNING, e.toString() + " when attempting to load pass_through for Chain {0} Counter {1}. Reverting to default value {2}.", new Object[]{chainName, chainCounter, 0});
                    pass_through = 0.0;
                }
                String rr = item.getAttribute("rerun_prescale").trim();
                //logger.info("Rerun "+rr);
                rerun_prescale = rr;
                if (!rerun_prescale.contains(":")) {
                    rerun_prescale = rerun_prescale + ":1;";
                }

                ArrayList<Object> psVector = new ArrayList<>();
                psVector.add(0, chainCounter);
                psVector.add(1, chainName);
                psVector.add(2, "HLT");
                psVector.add(3, prescale);
                psVector.add(4, pass_through);
                psVector.add(5, express_stream);
                //System.out.println("chain name " + chainName + " couter " + chainCounter + " confirm prescale write " + rerun_prescale);
                psVector.add(6, rerun_prescale);

                retMap.put(chainName, psVector);
            }
        }
        this.prescaleSetMap = retMap;
        return retMap;
    }

    /**
     *
     * @return
     */
    public LinkedHashMap<String, ArrayList<Object>> createPrescaleSetL1() {
        this.ReadPrescaleValuesL1();
        return prescaleSetMap;
    }

    /**
     *
     * @return
     */
    public LinkedHashMap<String, ArrayList<Object>> createPrescaleSetHLT() {
        this.ReadPrescaleValuesHLT();
        return prescaleSetMap;
    }
}
