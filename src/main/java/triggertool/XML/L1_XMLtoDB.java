package triggertool.XML;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.L1.L1Bunch;
import triggerdb.Entities.L1.L1BunchGroup;
import triggerdb.Entities.L1.L1BunchGroupSet;
import triggerdb.Entities.L1.L1CaloInfo;
import triggerdb.Entities.L1.L1CaloIsoParam;
import triggerdb.Entities.L1.L1CaloIsolation;
import triggerdb.Entities.L1.L1CaloMinTob;
import triggerdb.Entities.L1.L1CtpFiles;
import triggerdb.Entities.L1.L1CtpSmx;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.L1.L1Menu;
import triggerdb.Entities.L1.L1MuctpiInfo;
import triggerdb.Entities.L1.L1MuonThresholdSet;
import triggerdb.Entities.L1.L1Prescale;
import triggerdb.Entities.L1.L1PrescaledClock;
import triggerdb.Entities.L1.L1Random;
import triggerdb.Entities.L1.L1Threshold;
import triggerdb.Entities.L1.L1ThresholdValue;
import triggerdb.Entities.L1Links.L1TM_PS;
import triggerdb.Entities.L1Links.L1TM_TT;
import triggerdb.Entities.L1Links.L1TM_TTM;
import triggerdb.Entities.Topo.TopoOutputLine;
import triggerdb.Entities.Topo.TopoOutputList;
import triggerdb.Entities.TopoLinks.TopoL1Link;
import triggertool.L1Records.CutToValueConverter;
import triggertool.XML.Readers.XMLReader_L1Prescale;

/**
 * Read L1 XML file and save to database.
 * Functionality to read a L1 XML file and convert, and save it to the database.
 * 
 * @author Simon Head
 * @author Alex Martyniuk
 */
public final class L1_XMLtoDB {

    // /Message Log.

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    // /Used when parsing XML.
    private NodeList sections;
    // /Used by XML parser.
    private Document document;
    // /These are all the thresholds
    private final HashMap<String, Integer> threshold_ids = new HashMap<>();
    // /forced thresholds
    private final HashMap<String, Integer> forcedthreshold_ids = new HashMap<>();
    // /Map of Thresholds that are "InternalTriggers" and have been loaded &
    // saved
    private Map<String, L1Threshold> internaltrigthresholdsLoaded = null;
    // /Vector of L1 TM to TT links - required for cables.
    private final ArrayList<L1TM_TT> cables_to_save = new ArrayList<>();
    // /Threshold counter for when we combine thresholds into an item.
    private int mThresholdCounter = 0;
    // /A vector of the problems encountered before the upload.
    private final ArrayList<String> faults = new ArrayList<>();
    // /to store the new LVL1 prescale key.
    private int newLVL1PrescaleKey = 0;
    //Import the topoId this menu should be linked to
    private int topoId = 0;
    //List of the L1Thresholds with Topo inputs
    private final ArrayList<L1Threshold> topoInputs = new ArrayList<>();

    // /to store the new LVL1 bunch group key
    // private int newLVL1BGKey = 0;
    // /Constructor takes the filename of the L1 XML file to read only.
    /**
     * Constructor takes the filename. The XML file is parsed and the
     * information contained within it is written to the database
     * 
     * @param filename
     *            Path and filename of the L1 XML file to read in
     * @param configname
     * @param pssname
     * @param _topoId
     * @return 
     * @throws java.sql.SQLException 
     * @throws triggertool.XML.ConfigurationException 
     */
    public int readXML(final String filename, final String configname, final String pssname, final int _topoId) throws SQLException, ConfigurationException {
        
        if(filename.equalsIgnoreCase("default")) {
            int activeId = L1Master.getActiveId();
            if(activeId<0) {
                logger.log(Level.SEVERE, "No active L1 master ID is set on this database.");
            } else {
                logger.log(Level.INFO, "Will use L1 master ID {0}", activeId);            
            }
            return activeId;
        }
        
        internaltrigthresholdsLoaded = new HashMap<>(100);
        topoId = _topoId;
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringComments(true);
        factory.setCoalescing(true);
        factory.setNamespaceAware(false);
        factory.setValidating(false);

        DocumentBuilder parser;

        int l1_id = -1;

        try {
            parser = factory.newDocumentBuilder();
            document = parser.parse(new File(filename));

            ValidateXml();
            if (IsXmlValid()) {
                l1_id = HandleValidXml(configname, pssname, filename); 
            }
            else {
                HandleInvalidXml();
            }                       
        } catch (ParserConfigurationException | SAXException | IOException e) {
            logger.severe(e.toString());
        }
        return l1_id;
    }

    private int HandleValidXml(final String configname, final String pssname, final String filename) throws SQLException, ConfigurationException {
        //if name is "USEXML" then find menu and pss names from inside!
        String configname_ = configname;
        if (configname.equals("USEXML")) {
            Element menu = (Element) document.getElementsByTagName("TriggerMenu").item(0);
            configname_ = menu.getAttribute("name");
        }
        String pssname_ = pssname;
        if (pssname.equals("USEXML")) {
            Element pss = (Element) document.getElementsByTagName("PrescaleSet").item(0);
            pssname_ = pss.getAttribute("name");
        }
        StringBuilder sb = new StringBuilder(200);
        sb.append("Will upload LVL1 XML file: ").append(filename)
                .append("\nMenu name will be:         ").append(configname_)
                .append("\nLVL1 PSK name will be:     ").append(pssname_);
        logger.log(Level.INFO,sb.toString());
        logger.fine("Passed all checks, will start upload");
        logger.fine("Reading Threshold List");
        trigger_threshold_list();
        logger.fine("Checking thresholds with Topo inputs");
        boolean inputsfine = checkTopoInputs();
        if (!inputsfine) {
            logger.severe("We have a L1 Threshold seeded from a non-existant Topo trigger line!");
            throw new ConfigurationException("L1 threshold seeded from non-existant topo trigger line");
        }
        logger.fine("Reading Random");
        int rand_id = random();
        logger.fine("Reading Prescaled Clock");
        int pc_id = prescaled_clock();
        logger.fine("Reading MUCTPI info");
        int mu_id = muctpi_info();
        logger.fine("Reading Calorimeter Info");
        int ci_id = calo_info();
        // PJB need to read PS and BGS before menu
        logger.fine("Reading Bunch Groups");
        bunch_group_set();
        logger.fine("Reading Prescale Set");
        newLVL1PrescaleKey = prescale_set();
        logger.fine("Reading Trigger Menu");
        int menu_id = trigger_menu(configname_);
        logger.fine("Linking menu to Topo Menu");
        if(inputsfine){
            linkTopo(menu_id);
        }
        logger.fine("Making a muon threshold set table");
        int mts_id = muon_threshold_set();
        logger.log(Level.FINE, "Making a L1 master {0} {1} {2} {3} {4} {5} {6}",
                new Object[]{menu_id, mu_id, rand_id, pc_id, ci_id, mts_id});
        int l1_id = trigger_master(menu_id, mu_id, rand_id, pc_id, ci_id, mts_id, configname_);
        logger.log(Level.INFO,"Finished uploading L1" + "\n\tL1 Master ID =     {0}\n\tL1 Menu ID =      {1}",
                new Object[]{l1_id, menu_id});
        // link the prescales to the menu
        logger.log(Level.INFO, "***Adding to L1 Menu {0} the L1 PSS {1}",
                new Object[]{menu_id, newLVL1PrescaleKey});
        L1TM_PS newpslink = new L1TM_PS();
        newpslink.set_menu_id(menu_id);
        newpslink.set_prescale_set_id(newLVL1PrescaleKey);
        try {
            newpslink.save();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        } 
        return l1_id;
    }

    private void HandleInvalidXml() {
        logger.warning("XML file has errors");
        for (String line : faults) {
            logger.warning(line);
        }
    }

    // /Get the prescale ID.
    /**
     * If this has made a new key, return the ID with this.
     * 
     * @return Integer ID of the prescale key.
     */
    public int getLVLPrescaleSetKey() {
        return newLVL1PrescaleKey;
    }

    // /Read a threshold value.
    /**
     * Reads the L1 threshold value node and converts it into the object used by
     * the trigger tool. Also saves to the database.
     * 
     * @param ttv_node
     *            The XML TriggerThresholdValue node.
     * @return The ID of the trigger threshold value.
     */
    @SuppressWarnings("LoggerStringConcat")
    private L1ThresholdValue threshold_value(Element ttv_node)
            throws SQLException {
        logger.finest("Threshold value name " + ttv_node.getAttribute("name"));
        L1ThresholdValue newThresholdV = new L1ThresholdValue();
        newThresholdV.set_name(ttv_node.getAttribute("name"));
        newThresholdV.set_eta_max(Integer.parseInt(ttv_node.getAttribute("etamax")));
        newThresholdV.set_eta_min(Integer.parseInt(ttv_node.getAttribute("etamin")));
        newThresholdV.set_phi_min(Integer.parseInt(ttv_node.getAttribute("phimin")));
        newThresholdV.set_phi_max(Integer.parseInt(ttv_node.getAttribute("phimax")));
        newThresholdV.set_priority(Integer.parseInt(ttv_node.getAttribute("priority")));
        newThresholdV.set_window(Integer.parseInt(ttv_node.getAttribute("window")));
        if(ttv_node.hasAttribute("isobits")) {
            newThresholdV.set_em_isolation(ttv_node.getAttribute("isobits"));
            newThresholdV.set_had_isolation("0");
            newThresholdV.set_had_veto("99");
        } else {
            newThresholdV.set_em_isolation(ttv_node.getAttribute("em_isolation"));
            newThresholdV.set_had_isolation(ttv_node.getAttribute("had_isolation"));
            newThresholdV.set_had_veto(ttv_node.getAttribute("had_veto"));
        }
        newThresholdV.set_pt_cut(ttv_node.getAttribute("thresholdval"));
        newThresholdV.set_type(ttv_node.getAttribute("type"));

        if (newThresholdV.get_type().equals("BG")) {
            newThresholdV.set_type(L1Threshold.BG_TYPE);
        }

        if (newThresholdV.get_type().equals("RND")) {
            newThresholdV.set_type(L1Threshold.RND_TYPE);
        }

        newThresholdV.save();

        return newThresholdV;
    }

    // /Read a threhold.
    /**
     * Read a threshold from the XML file. If the old style XML notation is used
     * for Bunch Group or Random (type=BG or RND) replace it with the new style
     * notation as defined in L1Threshold.
     * 
     * @param element
     *            XML node for the element.
     * @return ID of the trigger threshold.
     * @throws java.sql.SQLException
     *             Stop on SQL problems.
     */
    private int trigger_threshold(Element element) throws SQLException {
        L1Threshold threshold = new L1Threshold();
        threshold.set_name(element.getAttribute("name"));
        threshold.set_active(Integer.parseInt(element.getAttribute("active")));
        threshold.set_bitnum(Integer.parseInt(element.getAttribute("bitnum")));
        threshold.set_type(element.getAttribute("type"));

        if (threshold.get_type().equals("ZB")) {
            threshold.set_bcdelay(Integer.parseInt(element.getAttribute("bcdelay")));
            threshold.set_seed(element.getAttribute("seed"));
            threshold.set_seed_multi(Integer.parseInt(element.getAttribute("seed_multi")));
        }

        // CATCH when we've used the old style notation and change to the
        // official threshold types
        if (threshold.get_type().equals("BG")) {
            threshold.set_type(L1Threshold.BG_TYPE);
        }

        if (threshold.get_type().equals("RND")) {
            threshold.set_type(L1Threshold.RND_TYPE);
        }

        int map;
        try {
            map = Integer.parseInt(element.getAttribute("mapping"));
        } catch (NumberFormatException ex) {
            map = 0;
        }

        threshold.set_mapping(map);

        NodeList List_thresholds = element.getElementsByTagName("TriggerThresholdValue");

        logger.log(Level.FINER, "Threshold {0}", threshold.get_name());

        for (int j = 0; j < List_thresholds.getLength(); ++j) {
            Element ttv_node = (Element) List_thresholds.item(j);
            L1ThresholdValue ttv = threshold_value(ttv_node);
            logger.log(Level.FINEST, "  Adding threshold value {0} ? ", ttv.get_name());
            threshold.getThresholdValues().add(ttv);
        }

        int id = threshold.save();
        threshold_ids.put(threshold.get_name(), id);
        if(threshold.get_type().equals("TOPO")){
            topoInputs.add(threshold);
        }
        logger.log(Level.FINER, "Saved L1 Threshold name {0} with id {1}", new Object[]{threshold.get_name(), id});

        NodeList cables = element.getElementsByTagName("Cable");

        for (int j = 0; j < cables.getLength(); ++j) {
            Element cable_node = (Element) cables.item(j);
            cable(cable_node, id);
        }

        return id;
    }

    /**
     * Read the trigger threshold list.
     * Go through the trigger threshold list in the XML and write to the
     * database.
     */
    private void trigger_threshold_list() throws SQLException {
        sections = document.getElementsByTagName("TriggerThresholdList");

        if (sections.getLength() == 1) {
            Element thresholdlist = (Element) sections.item(0);

            NodeList thresholds = thresholdlist.getElementsByTagName("TriggerThreshold");
            for (int i = 0; i < thresholds.getLength(); ++i) {
                Element threshold = (Element) thresholds.item(i);
                logger.log(Level.FINER, "  {0}- bitnum={1}, id={2}, name={3}, type={4}, version={5}, active={6}, mapping={7}", new Object[]{threshold.getNodeName(), threshold.getAttribute("bitnum"), threshold.getAttribute("id"), threshold.getAttribute("name"), threshold.getAttribute("type"), threshold.getAttribute("version"), threshold.getAttribute("active"), threshold.getAttribute("mapping")});

                trigger_threshold(threshold);
            }
            // now set forced thresholds to contain all thresholds
            // forcedthreshold_ids = threshold_ids; cant do this!
        } else {
            logger.warning("TOO MANY TRIGGERTHRESHOLD NODES IN XML");
        }
    }

    // /Read the cable information for a single cable.
    private void cable(Element cable_node, int threshold_id) {
        String name = cable_node.getAttribute("name");
        String slot = cable_node.getAttribute("input");
        String connector = cable_node.getAttribute("connector");

        int start = 0;
        int end = 0;
        int clock = -1; // for non-overclocked cables use -1

        NodeList signals = cable_node.getElementsByTagName("Signal");
        for (int k = 0; k < signals.getLength(); ++k) {
            Element signal = (Element) signals.item(k);
            logger.log(Level.FINEST, "      {0}- range_begin={1}, range_end={2}", new Object[]{signal.getNodeName(), signal.getAttribute("range_begin"), signal.getAttribute("range_end")});
            start = Integer.parseInt(signal.getAttribute("range_begin"));
            end = Integer.parseInt(signal.getAttribute("range_end"));
            if(signal.hasAttribute("clock")) {
                clock = Integer.parseInt(signal.getAttribute("clock"));
            }
        }

        logger.log(Level.FINEST, "cable part 1 {0}, {1}, {2}", new Object[]{name, slot, connector});
        logger.log(Level.FINEST, "cable part 2 {0}, {1}, {2}", new Object[]{start, end, clock});


        L1TM_TT link = new L1TM_TT();
        link.set_threshold_id(threshold_id);
        link.set_cable_name(name);
        link.set_cable_ctpin(slot);
        link.set_cable_connector(connector);
        link.set_cable_start(start);
        link.set_cable_end(end);
        link.set_cable_clock(clock);

        cables_to_save.add(link);
    }
    
    /** Read the CTP monitoring information from the XML file.*/
    //This method reads from the new L1Menu style. Here the counters
    //are stored as a TriggerCounter with the posibility of multiple
    //TriggerConditions.
    private void mon_counters(final int menu_id, L1Menu newmenu)
            throws SQLException {
        sections = document.getElementsByTagName(L1TM_TTM.XML_COUNTER_LIST_TAG);//"TriggerCounterList"

        if (sections.getLength() == 1) {
            Element section = (Element) sections.item(0);

            logger.fine(section.getNodeName());

            NodeList counterlist = section.getElementsByTagName(L1TM_TTM.XML_COUNTER_TAG);//"TriggerCounter"
            for (int i = 0; i < counterlist.getLength(); ++i) {
                Element thiscounter = (Element) counterlist.item(i);

                //String threshold = thiscounter.getAttribute(L1TM_TTM.XML_THRESHOLD_ATTR);
                logger.log(Level.FINER, "Counter {0}, {1}",
                        new Object[]{thiscounter.getAttribute(L1TM_TTM.XML_NAME_ATTR), thiscounter.getAttribute("type")});
                String _name = thiscounter.getAttribute(L1TM_TTM.XML_NAME_ATTR);
                
                NodeList List_Conditions = thiscounter.getElementsByTagName("TriggerCondition");
                for (int cond_i = 0; cond_i < List_Conditions.getLength(); ++cond_i) {
                    Element condition_node = (Element) List_Conditions.item(cond_i);
                    //logger.fine("Condition for counter " +thiscounter.getAttribute("name")+ " has condition " + cond_i +" called "
                    //        +condition_node.getAttribute("name") +" and a multiplicity of " +condition_node.getAttribute("multi"));
                    int node_threshold_id = threshold_ids.get(condition_node.getAttribute("triggerthreshold"));
                    L1TM_TTM db_counter = new L1TM_TTM(-1);
                    db_counter.set_menu_id(menu_id);
                    db_counter.set_threshold_id(node_threshold_id);
                    db_counter.set_bunch_group_id(1);
                    db_counter.set_counter_name(_name);
                    db_counter.set_multiplicity(Integer.parseInt(condition_node.getAttribute(L1TM_TTM.XML_MULTI_ATTR)));
                    db_counter.set_type(thiscounter.getAttribute(L1TM_TTM.XML_TYPE_ATTR));
                    newmenu.getMonitors().add(db_counter);
                    logger.finer("Counter save ok");
                }
            }
        } else {
            logger.info("Probably no TRIGGERCOUNTERLIST NODE in this xml");
        }
    }


    // /Read Bunch Group information.
    /**
     * Check and insert a Bunch Group (and bunches) if required. Called by
     * BunchGroupSet \verbatim <BunchGroup internalNumber="1" name="col_Bunch"
     * version="1"> which contains <Bunch> </BunchGroup> \endverbatim
     * 
     * @param bg_node
     *            the XML node we want to parse
     * @return the id for the bunch group
     */
    private L1BunchGroup bunch_group(Element bg_node) throws SQLException {
        String bg_name = bg_node.getAttribute("name");
        int num = Integer.parseInt(bg_node.getAttribute("internalNumber"));
        // logger.info("bunch group: " + bg_name);

        NodeList List_bunches = bg_node.getElementsByTagName("Bunch");

        L1BunchGroup bg = new L1BunchGroup();
        //bg.set_name(bg_name);
        bg.set_internal_number(num);
        //bg.set_partition(Integer.parseInt(bg_node.getAttribute("menuPartition")));
        ArrayList<L1Bunch> bunches = new ArrayList<>();
        for (int i = 0; i < List_bunches.getLength(); ++i) {
            Element b_node = (Element) List_bunches.item(i);
            int bn = Integer.parseInt(b_node.getAttribute("bunchNumber"));

            L1Bunch b = new L1Bunch();
            b.set_bunch_number(bn);
            bunches.add(b);
        }
        
        bg.set_Bunches(bunches);
        bg.save();

        return bg;
    }

    // /Read a bunch group set.
    /**
     * Check for the bunch group set and insert one if required. \verbatim
     * <BunchGroupSet name="bg_set1" version="bg_set1"> Contains <BunchGroup>
     * which contains <Bunch> </BunchGroupSet> \endverbatim
     * 
     * @return id of the bunch group set
     */
    private int bunch_group_set() throws SQLException {
        sections = document.getElementsByTagName("BunchGroupSet");

        int bgs_id = -1;

        if (sections.getLength() == 1) {
            Element section = (Element) sections.item(0);

            NodeList List_groups = section.getElementsByTagName("BunchGroup");

            L1BunchGroupSet needthis = new L1BunchGroupSet();
            needthis.set_name(section.getAttribute("name"));
            needthis.set_partition(Integer.parseInt(section.getAttribute("menuPartition")));
            int bgLength = List_groups.getLength();
            for (int i = 0; i < bgLength; ++i) {
                Element bg_node = (Element) List_groups.item(i);
                needthis.getBunchGroups().add(new AbstractMap.SimpleEntry<>(bunch_group(bg_node),bg_node.getAttribute("name").trim()));
            }

            bgs_id = needthis.save();
        }
        return bgs_id;
    }

    private int random() throws SQLException {
        sections = document.getElementsByTagName("Random");
        int id = -1;

        if (sections.getLength() == 1) {
            Element section = (Element) sections.item(0);

            L1Random random = new L1Random(-1);

            random.set_cut0(CutToValueConverter.getIntegerFromHexString(section.getAttribute("cut0")));
            random.set_cut1(CutToValueConverter.getIntegerFromHexString(section.getAttribute("cut1")));
            random.set_cut2(CutToValueConverter.getIntegerFromHexString(section.getAttribute("cut2")));
            random.set_cut3(CutToValueConverter.getIntegerFromHexString(section.getAttribute("cut3")));

            id = random.save();
        } else {
            logger.log(Level.WARNING, "Found {0} Random nodes in XML, expected exactly 1.", sections.getLength());
        }

        return id;
    }


    // /Read Muctpi Info.
    /**
     * Read the MuctpiInfo node. \verbatim <MuctpiInfo name="muctpi01"
     * version="1"> <low_pt>2</low_pt> <high_pt>6</high_pt>
     * <max_cand>13</max_cand> </MuctpiInfo> \endverbatim
     * 
     * @return id for the muctpi info table (uploaded or existing)
     */
    private int muctpi_info() throws SQLException {
        sections = document.getElementsByTagName("MuctpiInfo");
        int id = -1;

        if (sections.getLength() == 1) {
            Element section = (Element) sections.item(0);

            L1MuctpiInfo newInfo = new L1MuctpiInfo(-1);
            newInfo.set_name(section.getAttribute("name"));
            newInfo.set_low_pt(Integer.parseInt(section.getAttribute("low_pt")));
            newInfo.set_high_pt(Integer.parseInt(section.getAttribute("high_pt")));
            newInfo.set_max_cand(Integer.parseInt(section.getAttribute("max_cand")));
            id = newInfo.save();
        } else {
            logger.warning("TOO MANY MUCTPI NODES IN XML");
        }

        return id;
    }

    // /Read the prescaled clock node.
    /**
     * Read the prescaled clock node from the XML. It is expected to look like
     * this, and there is only one: \verbatim <PrescaledClock name="psc01"
     * version="1" clock1="10" clock2="100"/> \endverbatim
     * 
     * @return id for the prescaled clock table (uploaded or existing)
     */
    private int prescaled_clock() throws SQLException {
        sections = document.getElementsByTagName("PrescaledClock");
        int id = -1;

        if (sections.getLength() == 1) {
            Element section = (Element) sections.item(0);

            L1PrescaledClock pc = new L1PrescaledClock(-1);
            pc.set_name(section.getAttribute("name"));
            pc.set_clock1(Integer.parseInt(section.getAttribute("clock1")));
            pc.set_clock2(Integer.parseInt(section.getAttribute("clock2")));
            id = pc.save();
        } else {
            logger.warning("TOO MANY PRESCALED CLOCK NODES IN XML");
        }

        return id;
    }

    // /Read the Calo Info node.
    /**
     * Read a CaloInfo node \verbatim <CaloInfo name="standard" version="1"
     * global_scale="4.0"> <JetWeight num="1"> 10 </JetWeight> ... <JetWeight
     * num="12"> 10 </JetWeight> </CaloInfo> \endverbatim
     */
    private int calo_info() throws SQLException {
        sections = document.getElementsByTagName("CaloInfo");
        int id = -1;

        if (sections.getLength() == 1) {
            Element section = (Element) sections.item(0);

            
            L1CaloInfo newInfo = new L1CaloInfo(-1);
            newInfo.set_name(section.getAttribute("name"));
            //add global scales if present - if not skip as by defualt they are 1
            if(section.hasAttribute("global_em_scale")) {
              newInfo.set_global_em_scale(Double.valueOf(section.getAttribute("global_em_scale")));
            } else {
                logger.log(Level.FINER, "EM Global scale missing, but set to = {0}", newInfo.get_global_em_scale());
            }
            if(section.hasAttribute("global_jet_scale")){
              newInfo.set_global_jet_scale(Double.valueOf(section.getAttribute("global_jet_scale")));
            } else {
                logger.log(Level.FINER, "Jet Global scale missing, but set to = {0}", newInfo.get_global_jet_scale());
            }
            // get MET significance part
            NodeList metsigeles = section.getElementsByTagName("METSignificance");
            if (metsigeles.getLength() != 1) {
                logger.severe("METSignificance does not have length 1, cannot import it.");
            } else {
                Element ele = (Element) metsigeles.item(0);
                try {
                    newInfo.set_xs_tesqrt_max(Integer.parseInt(ele.getAttribute("teSqrtMax")));
                    logger.log(Level.FINE, "METSignificance teSqrtMax = {0}", newInfo.get_xs_tesqrt_max());
                    newInfo.set_xs_tesqrt_min(Integer.parseInt(ele.getAttribute("teSqrtMin")));
                    logger.log(Level.FINE, "METSignificance teSqrtMin = {0}", newInfo.get_xs_tesqrt_min());
                    newInfo.set_xs_xe_max(Integer.parseInt(ele.getAttribute("xeMax")));
                    logger.log(Level.FINE, "METSignificance xeMax = {0}", newInfo.get_xs_xe_max());
                    newInfo.set_xs_xe_min(Integer.parseInt(ele.getAttribute("xeMin")));
                    logger.log(Level.FINE, "METSignificance xeMin = {0}", newInfo.get_xs_xe_min());
                    newInfo.set_xs_sigma_offset(Integer.parseInt(ele.getAttribute("xsSigmaOffset")));
                    logger.log(Level.FINE, "METSignificance xsSigmaOffset = {0}", newInfo.get_xs_sigma_offset());
                    newInfo.set_xs_sigma_scale(Integer.parseInt(ele.getAttribute("xsSigmaScale")));
                    logger.log(Level.FINE, "METSignificance xsSigmaScale = {0}", newInfo.get_xs_sigma_scale());
                } catch (NumberFormatException ex) {
                    logger.severe("NumberFormatException when reading METSignificance properties, cannot import it");
                }

            }
            
            // get Isolation part
            NodeList isoeles = section.getElementsByTagName("Isolation");
            //there should be three types of isolation listed
            if (isoeles.getLength() != 3 ) {
                logger.severe("Isolation does not have length 3, cannot import it.");
            } else {
                //TODO move to new function
                //loop over the three isolation types
                for(int iso_i=0; iso_i<isoeles.getLength(); iso_i++){
                    Element ele = (Element) isoeles.item(iso_i);
                
                    L1CaloIsolation l1cis = new L1CaloIsolation(-1);
                  
                    l1cis.set_thr_type(ele.getAttribute("thrtype"));
                    logger.log(Level.FINER, "CaloIsolation thrtype = {0}", l1cis.get_thr_type());
                    
                    //loop over the up to 5 parametrizations
                    //TODO certainly move to new function
                    NodeList param_node = ele.getElementsByTagName("Parametrization");
                    if(param_node.getLength()<5){
                        logger.log(Level.FINER, "Mark: Only {0} isolation parametrizations to inport for {1}", new Object[]{param_node.getLength(), l1cis.get_thr_type()});
                    }
                    
                    L1CaloIsoParam[] parameterizations = new L1CaloIsoParam[5];

                    for(int i = 0; i<5; i++) {
                        parameterizations[i] = new L1CaloIsoParam(-1);
                    }

                    for(int param_i = 0; param_i < param_node.getLength(); param_i++) {
                                            
                        L1CaloIsoParam l1cip = new L1CaloIsoParam(-1);

                        //if have a parameterization retrieve its details
                        //otherwise the blank version will be stored
                        //TODO check above is correct policy!
                        
                        //TODO MARK add try/catch
                        Element param_ele = (Element) param_node.item(param_i);
                        l1cip.set_iso_bit(Integer.parseInt(param_ele.getAttribute("isobit")));
                        logger.log(Level.FINER, "CaloIsoParam isobit = {0}", l1cip.get_iso_bit());
                        l1cip.set_offset(Integer.parseInt(param_ele.getAttribute("offset")));
                        logger.log(Level.FINER, "CaloIsoParam offset = {0}", l1cip.get_offset());
                        l1cip.set_slope(Integer.parseInt(param_ele.getAttribute("slope")));
                        logger.log(Level.FINER, "CaloIsoParam slope = {0}", l1cip.get_slope());
                        l1cip.set_min_cut(Integer.parseInt(param_ele.getAttribute("mincut")));
                        logger.log(Level.FINER, "CaloIsoParam mincut = {0}", l1cip.get_min_cut());
                        l1cip.set_upper_limit(Integer.parseInt(param_ele.getAttribute("upperlimit")));
                        logger.log(Level.FINER, "CaloIsoParam upperlimit = {0}", l1cip.get_upper_limit());
                        l1cip.set_eta_min(Integer.parseInt(param_ele.getAttribute("etamin")));
                        logger.log(Level.FINER, "CaloIsoParam etamin = {0}", l1cip.get_eta_min());
                        l1cip.set_eta_max(Integer.parseInt(param_ele.getAttribute("etamax")));
                        logger.log(Level.FINER, "CaloIsoParam etamax = {0}", l1cip.get_eta_max());
                        l1cip.set_priority(Integer.parseInt(param_ele.getAttribute("priority")));
                        logger.log(Level.FINER, "CaloIsoParam priority = {0}", l1cip.get_priority());

                        
                        int isobit = l1cip.get_iso_bit();
                        if(isobit<1 && isobit>5) {
                            logger.log(Level.SEVERE, "CaloIsoParameteriztion has wrong isobit {0}", isobit);
                        }

                        parameterizations[isobit-1] = l1cip;
                    }

                    for(int i = 0; i<5; i++) {
                        //now save the parametrization
                        parameterizations[i].save();

                        //now store the link between paramtrization and isolation
                        l1cis.set_par( i+1, parameterizations[i].get_id() );
                    }
                    logger.log(Level.FINE, "Saving with PARIDs {0}, {1}, {2}, {3}, {4}", new Object[]{l1cis.get_par(1), l1cis.get_par(2), l1cis.get_par(3), l1cis.get_par(4), l1cis.get_par(5)});
                    l1cis.save();
                    switch (l1cis.get_thr_type()) {
                        case "HAIsoForEMthr":
                            newInfo.set_iso_ha_em(l1cis.get_id());
                            break;
                        case "EMIsoForEMthr":
                            newInfo.set_iso_em_em(l1cis.get_id());
                            break;
                        case "EMIsoForTAUthr":
                            newInfo.set_iso_em_tau(l1cis.get_id());
                            break;
                        default:
                            //TODO what to do if wrong?
                            logger.log(Level.WARNING, "Incorrect CaloIsolation thrtype = {0}", l1cis.get_thr_type());
                            break;
                    }

                }

            }
            
            //now add the minimumTOB
            NodeList mineles = section.getElementsByTagName("MinimumTOBPt");
            if (mineles.getLength() != 4 ) {
                logger.severe("MinimumTOBPt does not have length 4, cannot import it.");
            } else {
                //TODO MARK add try/catch
                for(int min_i=0; min_i<mineles.getLength(); min_i++){
                    Element ele = (Element) mineles.item(min_i);
                
                    L1CaloMinTob l1cmt = new L1CaloMinTob(-1);
                    l1cmt.set_thr_type(ele.getAttribute("thrtype"));
                    logger.log(Level.FINER, "MinimumTOBPt thr_type = {0}", l1cmt.get_thr_type());
                    if(ele.hasAttribute("window")){
                        l1cmt.set_window(Integer.parseInt(ele.getAttribute("window")));
                        logger.log(Level.FINER, "MinimumTOBPt window = {0}", l1cmt.get_window());
                    } else {
                       logger.log(Level.FINER, "MinimumTOBPt window missing, but set to = {0}", l1cmt.get_window());
                    }
                    l1cmt.set_pt_min(Integer.parseInt(ele.getAttribute("ptmin")));
                    logger.log(Level.FINER, "MinimumTOBPt pt_min = {0}", l1cmt.get_pt_min());
                    l1cmt.set_eta_min(Integer.parseInt(ele.getAttribute("etamin")));
                    logger.log(Level.FINER, "MinimumTOBPt eta_min = {0}", l1cmt.get_eta_min());
                    l1cmt.set_eta_max(Integer.parseInt(ele.getAttribute("etamax")));
                    logger.log(Level.FINER, "MinimumTOBPt eta_max = {0}", l1cmt.get_eta_max());
                    l1cmt.set_priority(Integer.parseInt(ele.getAttribute("priority")));
                    logger.log(Level.FINER, "MinimumTOBPt  = {0}", l1cmt.get_priority());
                    l1cmt.save();
                    
                    //given the above ID save the link to the calo info table
                    switch (l1cmt.get_thr_type()) {
                        case "EM":
                            newInfo.set_min_tob_em(l1cmt.get_id());
                            break;
                        case "TAU":
                            newInfo.set_min_tob_tau(l1cmt.get_id());
                            break;
                        case "JETS":
                            newInfo.set_min_tob_jets(l1cmt.get_id()); 
                            break;
                        case "JETL":
                            newInfo.set_min_tob_jetl(l1cmt.get_id());
                            break;
                    }
                }
                
            }


            id = newInfo.save();
        } else {
            logger.warning("NOT ONE AND ONLY ONE CALOINFO NODE IN XML");
        }

        return id;
    }

    // /Read a prescale set.
    /**
     * Read a prescale_set from XML and write to the database.
     * 
     * @return ID of the Prescale Set
     */
    private int prescale_set() throws SQLException {
        XMLReader_L1Prescale writer = new XMLReader_L1Prescale();
        L1Prescale prescaleSet = writer.CreateL1Prescale(document);
        return prescaleSet.save();
    }

    // /Read all the trigger items.
    /**
     * Write the trigger item table to the database.
     * 
     * @param itemNode
     *            the node in the XML for the trigger item.
     * @return ID for the trigger item in the database.
     */
    private L1Item trigger_item(Element item_node) throws SQLException {
        L1Item ti = new L1Item();
        ti.set_ctp_id(Integer.parseInt(item_node.getAttribute("ctpid")));
        ti.set_name(item_node.getAttribute("name"));

        logger.log(Level.FINEST, "Current TriggerItem: {0}", ti.get_name());

        String ttype = item_node.getAttribute("trigger_type");
        ti.set_trigger_type(ttype);

        if (item_node.getAttribute("comment").length() == 0) {
            ti.set_comment("~");
        } else {
            ti.set_comment(item_node.getAttribute("comment"));
        }

        int group;
        try {
            group = Integer.parseInt(item_node.getAttribute("group"));
        } catch (NumberFormatException e) {
            group = 0;
        }
        
        ti.set_group(group);
         
        String priority = item_node.getAttribute("complex_deadtime");
        ti.set_priority(priority);

        int partition;
        try {
            partition = Integer.parseInt(item_node.getAttribute("partition"));
        } catch (NumberFormatException e) {
            partition = 0;
        }
        ti.set_partition(partition);
        
        //Check that the DB can handle the item monitors
        if(ConnectionManager.L1ItemMonitorColumnExists == 1){
            //Check the XML contains the monitors?
            if(item_node.hasAttribute("monitor")){
                String monitor;
                monitor = item_node.getAttribute("monitor");

                ti.set_monitor(monitor);
            }
        }
        
        // work out the logic
        mThresholdCounter = 1;
        String def_string = read(item_node);
        //String def_string = item_node.getAttribute("definition");
        if (def_string.length() == 0) {
            def_string = "(1)";
        }
        ti.set_definition(def_string);

        NodeList List_conditions = item_node.getElementsByTagName("*");
        int position_counter = 1;
        ArrayList<L1Threshold> thList = new ArrayList<>();
        
        for (int i = 0; i < List_conditions.getLength(); ++i) {
            Element condition_node = (Element) List_conditions.item(i);

            logger.log(Level.FINER, "{0} TagName: {1}", new Object[]{i + 1, condition_node.getTagName()});

            if (condition_node.getTagName().equals("InternalTrigger")) {
                String name = condition_node.getAttribute("name");
                L1Threshold threshold;
                // / See if we already loaded this threshold
                String test = name;
                test += "_p";
                test += position_counter;
                if (internaltrigthresholdsLoaded.containsKey(test)) {
                    threshold = internaltrigthresholdsLoaded.get(test); // get
                    // from
                    // the
                    // map
                } else { // / need to load from xml
                    threshold = new L1Threshold();
                    threshold.set_position(position_counter);
                    threshold.set_multiplicity(1);
                    threshold.set_name(name);

                    String type = "";

                    if (name.contains(L1Threshold.BG_TYPE)) {
                        type = L1Threshold.BG_TYPE;
                    } else if (name.contains(L1Threshold.RND_TYPE)) {
                        type = L1Threshold.RND_TYPE;
                    } else if (name.contains(L1Threshold.PSC_TYPE)) {
                        type = L1Threshold.PSC_TYPE;
                    }

                    threshold.set_type(type);
                    threshold.compactsave();
                    // add threshold to the loaded list
                    internaltrigthresholdsLoaded.put(test, threshold);
                }

                // //System.out.println("  " + threshold.get_position() +
                // " Internal  " + threshold.get_multiplicity() + "x " +
                // threshold.get_name());

                thList.add(threshold);
                ++position_counter;
            } else if (condition_node.getTagName().equals("TriggerCondition")) {
                String me = condition_node.getAttribute("triggerthreshold");
                int multi = Integer.parseInt(condition_node.getAttribute("multi"));

                Integer thresholdID = threshold_ids.get(me);
                if (thresholdID == null) {
                    throw new RuntimeException("Threshold '" + me + "', used by item '" + ti.get_name() + "' is not defined in L1 XML file. Abort loading!");
                }
                L1Threshold threshold = new L1Threshold(thresholdID);
                logger.log(Level.FINER, "Recovering threshold {0} name={1}", new Object[]{threshold_ids.get(me), threshold.get_name()});
                threshold.set_multiplicity(multi);
                threshold.set_position(position_counter);

                thList.add(threshold);
                forcedthreshold_ids.remove(threshold.get_name());

                ++position_counter;
            }
        }

        ti.setThresholds(thList);
        ti.save();
        return ti;
    }

    // /Master key needs a L1 muon prescale set to point at
    /**
     * Load empty L1 muon prescale set
     * 
     * @return ID of the l1 muon prescale set
     */
    private int muon_threshold_set() throws SQLException {
        L1MuonThresholdSet l1mts = new L1MuonThresholdSet(-1);
        l1mts.set_name("Unassigned");
        return l1mts.save();
    }

    // /Read the trigger menu from the XML file.
    /**
     * Load a trigger menu from XML.
     * 
     * @return ID of the trigger menu.
     */
    private int trigger_menu(final String configname) throws SQLException {
        int id = -1;

        this.sections = this.document.getElementsByTagName(L1Menu.XML_MENU_TAG);

        L1Menu needthis = new L1Menu();

        if (this.sections.getLength() == 1) {

            // put all thresholds into forced thresholds
            this.forcedthreshold_ids.putAll(this.threshold_ids);

            Element menu = (Element) this.sections.item(0);

            // set the name as supplied
            needthis.set_name(configname);
            needthis.set_phase(menu.getAttribute(L1Menu.XML_PHASE_ATTRIBUTE));

            L1CtpSmx l1CtpSmx = new L1CtpSmx(-1);
            l1CtpSmx.set_name("Empty");
            needthis.set_ctp_smx(l1CtpSmx);
            needthis.set_ctp_smx_id(l1CtpSmx.save());

            L1CtpFiles l1CtpFiles = new L1CtpFiles(-1);
            l1CtpFiles.set_name("Empty");
            needthis.set_ctp_files(l1CtpFiles);
            needthis.set_ctp_files_id(l1CtpFiles.save());

            NodeList List_items = menu.getElementsByTagName("TriggerItem");
            for (int i = 0; i < List_items.getLength(); ++i) {
                Element item_node = (Element) List_items.item(i);
                needthis.getItems().add(trigger_item(item_node));
            }

            // Now forced thresholds should contain all thresholds not in items
            // ...if there are any forced thresholds we need to save them!
            if (forcedthreshold_ids.size() > 0) {
                for (String key : forcedthreshold_ids.keySet()) {
                    Integer ftid = forcedthreshold_ids.get(key);
                    needthis.getForcedThresholds().add(new L1Threshold(ftid));
                }
            }
        }

        for (L1TM_TT cable : cables_to_save) {
            cable.set_menu_id(id);
            needthis.getTMTT().add(cable);
        }
        
        mon_counters(id, needthis);
        
        id = needthis.save();

        return id;
    }

    /**
     * write the L1 Master Table
     * 
     * @param menu_id
     *            menu ID
     * @param ps_id
     *            Prescale Set ID
     * @param mu_id
     *            MU CTP ID
     * @param pc_id
     *            Prescaled Clock ID
     * @param bgs_id
     *            Bunch Group Set ID
     * @param ci_id
     *            Calo Info ID
     */
    private int trigger_master(int menu_id, int mu_id, int random_id, int pc_id, int ci_id, int mts_id, String configname)
            throws SQLException {
        sections = document.getElementsByTagName("LVL1Config");

        int l1m_id = -1;
        if (sections.getLength() == 1) {
        
            // Set the name as supplied
            Element config = (Element) sections.item(0);
            //String name = config.getAttribute("name");
            String name = configname;

            L1Master master = new L1Master();
            master.set_name(name);
            master.set_menu_id(menu_id);
            master.set_muon_threshold_set_id(mts_id);
            master.set_muctpi_info_id(mu_id);
            master.set_prescaled_clock_id(pc_id);
            master.set_calo_info_id(ci_id);
            master.set_random_id(random_id);
            master.set_ctpVersion(Integer.parseInt(config.getAttribute("ctpVersion")));
            master.set_l1Version(Integer.parseInt(config.getAttribute("l1Version")));
            logger.fine("Calling save of L1 master...");
            l1m_id = master.save();
            logger.fine("Save of L1 master finished");
        }
        return l1m_id;
    }

    // /Logic between thresholds from the old trigger tool.

    /**
     *
     * @param e
     * @return
     */
    public String read(Element e) {
        String mLogicString = read(e, "");
//        if (mLogicString.length() > 0 && !mLogicString.contains("(")) {
//            mLogicString = "(" + mLogicString + ")";
//        }
        return mLogicString;
    }

    // /Logic class taken from the old trigger tool.
    private String read(Element e, String insert_symbol) {
        String OPEN = "(";
        String CLOSE = ")";

        String logic_string = "";
        Node node;
        Element e_next;
        String tag;
        NodeList nodes = e.getChildNodes();
        int elem_counter = 0;
        // String s_multi;

        for (int i = 0; i < nodes.getLength(); ++i) {
            node = nodes.item(i);
            if (node.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            e_next = (Element) node;
            tag = node.getNodeName();

            if (elem_counter > 0) {
                logic_string += insert_symbol;
            }
            switch (tag) {
                case "AND":
                    logic_string += OPEN;
                    logic_string += read(e_next, "&");
                    logic_string += CLOSE;
                    break;
                case "OR":
                    logic_string += OPEN;
                    logic_string += read(e_next, "|");
                    logic_string += CLOSE;
                    break;
                case "NOT":
                    logic_string += "!";
                    logic_string += read(e_next, "");
                    break;
                case "TriggerCondition":
                case "InternalTrigger":
                    logic_string += String.valueOf(mThresholdCounter++);
                    // mThresholdNames.add(e_next.getAttribute(LVL1ConfigModel.ktriggerthreshold));
                    // s_multi = e_next.getAttribute(LVL1ConfigModel.kmulti);
                    // mMultiplicities.add(Integer.valueOf(s_multi));
                    break;
            }
            elem_counter++;
        }
        return logic_string;
    }

    /**
     * Perform a check on a file to see if the basic xml is valid.
     */
    private Boolean IsXmlValid() {
  
        return faults.isEmpty();
    }

    private void ValidateXml() {
        //CheckDeadTime();
        CheckRandom();
        CheckPrescaledClock();
        CheckMuctpiInfo();
        CheckCaloInfo();
        CheckLVL1Config();
    }

    private void CheckDeadTime() {
        int count = document.getElementsByTagName("Deadtime").getLength();
        if (count != 1) {
            if (count < 1) {
                faults.add("Deadtime XML node missing");
            } else {
                faults.add("Too many Deadtime XML nodes.  Should have 1, found " + count);
            }
        } else {
            Element dt = (Element) document.getElementsByTagName("Deadtime").item(0);

            if (dt.getAttribute("complex1_level").length() == 0) {
                faults.add("Deadtime node needs complex1_level=\"a_number\"");
            }

            if (dt.getAttribute("complex1_rate").length() == 0) {
                faults.add("Deadtime node needs complex1_rate=\"a_number\"");
            }

            if (dt.getAttribute("complex2_level").length() == 0) {
                faults.add("Deadtime node needs complex2_level=\"a_number\"");
            }

            if (dt.getAttribute("complex2_rate").length() == 0) {
                faults.add("Deadtime node needs complex2_rate=\"a_number\"");
            }

            if (dt.getAttribute("simple").length() == 0) {
                faults.add("Deadtime node needs simple=\"a_number\"");
            }

            if (dt.getAttribute("name").length() == 0) {
                faults.add("Deadtime node needs name=\"a_string\"");
            }
        }
    }

    private void CheckRandom() {
        int count = document.getElementsByTagName("Random").getLength();
        if (count != 1) {
                faults.add("Wrong number of Random XML nodes.  Should have 1, found " + count);
        } else {
            for (int index = 0; index < 4; index++){
                CheckAttributesForRandom(index);
            }
        }
    }

    private void CheckAttributesForRandom(int index) {
        Element dt = (Element) document.getElementsByTagName("Random").item(0);
        
        //Removed constraint as we are not even saving the names?
//        if (dt.getAttribute("name" + index).length() == 0) {
//            faults.add("Random node needs name" + index + " =\"a_string\"");
//        }
        
        if (dt.getAttribute("cut" + index).length() == 0) {
            faults.add("Random node needs cut" + index + " =\"a_number\"");
        }
    }

    private void CheckPrescaledClock() {
        int count;
        // PrescaledClock
        count = document.getElementsByTagName("PrescaledClock").getLength();
        if (count != 1) {
            if (count < 1) {
                faults.add("PrescaledClock XML node missing");
            } else {
                faults.add("Too many PrescaledClock XML nodes.  Should have 1, found " + count);
            }
        } else {
            Element dt = (Element) document.getElementsByTagName("PrescaledClock").item(0);

            if (dt.getAttribute("name").length() == 0) {
                faults.add("PrescaledClock node needs name=\"a_string\"");
            }

            if (dt.getAttribute("clock1").length() == 0) {
                faults.add("PrescaledClock node needs clock1=\"a_number\"");
            }

            if (dt.getAttribute("clock2").length() == 0) {
                faults.add("PrescaledClock node needs clock2=\"a_number\"");
            }
        }
    }

    private void CheckMuctpiInfo() {
        int count;
        count = document.getElementsByTagName("MuctpiInfo").getLength();
        if (count != 1) {
            if (count < 1) {
                faults.add("MuctpiInfo XML node missing");
            } else {
                faults.add("Too many MuctpiInfo XML nodes.  Should have 1, found " + count);
            }
        } else {
            Element mi = (Element) document.getElementsByTagName("MuctpiInfo").item(0);

            if (mi.getAttribute("name").length() == 0) {
                faults.add("MuctpiInfo node needs name=\"a_string\"");
            }
            if (mi.getAttribute("low_pt").length() == 0) {
                faults.add("Need 1 low_pt attribute in <MuctpiInfo></MuctpiInfo>");
            }
            if (mi.getAttribute("high_pt").length() == 0) {
                faults.add("Need 1 high_pt attribute in <MuctpiInfo></MuctpiInfo>");
            }
            if (!tryParseInt(mi.getAttribute("max_cand"))) {
                faults.add("Need 1 max_cand attribute in <MuctpiInfo></MuctpiInfo>");
            }
        }
    }

    private void CheckCaloInfo() {
        int count;
        count = document.getElementsByTagName("CaloInfo").getLength();
        if (count != 1) {
            if (count < 1) {
                faults.add("CaloInfo XML node missing");
            } else {
                faults.add("Too many CaloInfo XML nodes.  Should have 1, found " + count);
            }
        } else {
            Element section = (Element) document.getElementsByTagName("CaloInfo").item(0);

            if (section.getAttribute("name").length() == 0) {
                faults.add("CaloInfo node needs name=\"a_string\"");
            }

            if (section.getAttribute("global_em_scale").length() == 0 && section.getAttribute("global_jet_scale").length() == 0 ) {
                //issue a warning rather than fault as uses default value if missing 
                logger.warning("CaloInfo node missing global_em_scale=\"an_integer\" and global_jet_scale=\"an_integer\" ");
            }
            
            //MARK TODO: add in checks here for new isolation - missing metsig?
            

        }
    }

    private void CheckLVL1Config() {
        sections = document.getElementsByTagName("LVL1Config");
        if (sections.getLength() == 1) {
            Element menu = (Element) sections.item(0);

            if (menu.getAttribute("name").length() == 0) {
                faults.add("<LVL1Config> tag needs name=\"a_string\" adding");
            }
        }               
    }
    
    boolean tryParseInt(String value)
    {
        try{
            Integer.parseInt(value);
            return true;
        }catch(NumberFormatException ex)
        {
            return false;
        }
    }

    //Check to see if the inputs from topo we have match the topo menu
    //we are loading.
    private boolean checkTopoInputs() throws SQLException {
        ArrayList<TopoOutputLine> outputlines = TopoOutputList.get_DB_output_lines_from_master(topoId);
        ArrayList<String> triggerLines = new ArrayList<>();
        for (TopoOutputLine line : outputlines) {
            //logger.log(Level.FINE, "We have line {0}", line.get_triggerline());
            triggerLines.add(line.get_triggerline());
        }
        boolean foundAll = true;
        //System.out.println("test");
        for (L1Threshold L1 : topoInputs) {
            //logger.log(Level.FINE, "We have threshold {0}", L1.get_name());
            boolean snap = false;
            for (String line : triggerLines) {
                if (line.contains(",")) {
                    String[] split = line.split(",");
                    for (String part : split) {
                        if (part.contains("[")) {
                            String[] subpart = part.split("\\[");
                            if (subpart[0].equals(L1.get_name())) {
                                snap = true;
                            }
                        } else if (part.equals(L1.get_name())) {
                            snap = true;
                        }
                    }
                } else if (line.contains("[")) {
                    String[] part = line.split("\\[");
                    if (part[0].equals(L1.get_name())) {
                        snap = true;
                    }
                } else if (line.equals(L1.get_name())) {
                    snap = true;
                }
            }

            if (!snap) {
                logger.log(Level.FINE, "We have not found threshold {0}", L1.get_name());
                foundAll = false;
            }
        }
        return foundAll;
    }

    private void linkTopo(int menu_id) throws SQLException {
        int links = TopoOutputList.get_DB_output_list_from_master(topoId);
        TopoL1Link link = new TopoL1Link(-1);
        link.set_topo_id(links);
        link.set_L1_id(menu_id);
        link.save();
    }
}
