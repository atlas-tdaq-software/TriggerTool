package triggertool.XML;

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 *
 * @author davethomas
 */
public class XMLCompressor {
    
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    
    
    public static void main(String[] args) throws SQLException, IOException, FileNotFoundException, SAXException, TransformerException {
        
    }
    
    
    public static void ZIPcompress(ArrayList<String> inputXMLs) throws IOException, FileNotFoundException {
        try {
            FileOutputStream fos = new FileOutputStream(inputXMLs.get(0).substring(0, inputXMLs.get(0).length() - 4) + "_compressed.zip");
            System.out.println(inputXMLs.get(0).substring(0, inputXMLs.get(0).length() - 4) + "_compressed.zip");
            ZipOutputStream zos = new ZipOutputStream(fos);
            System.out.print(inputXMLs);
            for (String xml : inputXMLs) {
                File XMLToZip = new File(xml);
                FileInputStream fis = new FileInputStream(XMLToZip);
                ZipEntry zipEntry = new ZipEntry(XMLToZip.getName());
                zos.putNextEntry(zipEntry);
                byte[] bytes = new byte[1024];
                int length;
                while ((length = fis.read(bytes)) >= 0) {
                   zos.write(bytes, 0, length);
                }
                fis.close();
            }
            zos.close();
            fos.close();
        }
        catch (IOException ex) {
            logger.log(Level.SEVERE, "Exception: {0}", ex);
        }
    }
    
    public void XMLCompressUpload (ArrayList<String> inputXMLs) throws IOException {
        FileOutputStream fos = new FileOutputStream("inputXMLs_compressed.zip");
        //System.out.println(inputXMLs.get(0).substring(0, inputXMLs.get(0).length() - 4) + "_compressed.zip");
        ZipOutputStream zos = new ZipOutputStream(fos);
        System.out.print(inputXMLs);
        for (String xml : inputXMLs) {
            File XMLToZip = new File(xml);
            FileInputStream fis = new FileInputStream(XMLToZip);
            ZipEntry zipEntry = new ZipEntry(XMLToZip.getName());
            zos.putNextEntry(zipEntry);
            byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                 zos.write(bytes, 0, length);
            }
            fis.close();
        }
        zos.close();
        fos.close();
    }
   
    public static void ZIPuncompress(ArrayList<String> originalXMLs) throws IOException {
        try {
            byte[] buffer = new byte[1024];
            System.out.println(originalXMLs);
            String inputZIP = "inputXMLs_compressed.zip";
            FileInputStream fis = new FileInputStream(inputZIP);
            ZipInputStream zis = new ZipInputStream(fis);
            ZipEntry zipEntry = zis.getNextEntry();
            Integer i = 0;
            while(zipEntry != null){
                String output = originalXMLs.get(i);
                output = output.substring(0, output.length() - 4) + "_uncompressed.xml";
                System.out.println(output);
                File ZipToXML = new File(output);
                FileOutputStream fos = new FileOutputStream(ZipToXML);
                int length;
                while ((length = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, length);
                }
                fos.close();
                zipEntry = zis.getNextEntry();
                i++;
            }
            zis.closeEntry();
            zis.close();
        }
        catch (IOException ex) {
            logger.log(Level.SEVERE, "Exception: {0}", ex);
        }
    }
}