/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.XML.Writers;

import java.sql.SQLException;
import java.util.ArrayList;
import triggerdb.Entities.HLT.HLTPrescale;
import triggerdb.Entities.HLT.HLTPrescaleType;
import triggerdb.Entities.HLT.HLTTriggerChain;
import triggerdb.Entities.HLT.HLTTriggerElement;
import triggerdb.Entities.HLT.HLTTriggerGroup;
import triggerdb.Entities.HLT.HLTTriggerSignature;
import triggerdb.Entities.HLT.HLTTriggerStream;
import triggerdb.Entities.HLT.HLTTriggerType;

/**
 *
 * @author Michele
 */
public class XMLWriter_HLTChain {

    Double prescalevalue = 1.0, streamPrescale = 0.0, passthroughvalue = 0.0, rerunValue = -1.0;

    private HLTTriggerChain chain;
    
    /**
     *
     * @param chain
     * @param prescales
     * @return
     * @throws SQLException
     */
    public String WriteChain(HLTTriggerChain chain, ArrayList<HLTPrescale> prescales) throws SQLException {        
        this.chain = chain;
        SetPrescaleValues(prescales);
                
        return WritePrescaleAttributes() +
                WriteTriggerTypeList() +
                WriteStreams() +
                WriteGroups() +
                WriteSignatureList() + 
                "\t\t</CHAIN>\n";
    }

    private String WritePrescaleAttributes() {
        String chainString = "\t\t<CHAIN "
                + "EBstep=\"" + chain.get_eb_point() + "\" "
                + "chain_counter=\"" + chain.get_chain_counter() + "\" "
                + "chain_name=\"" + chain.get_name() + "\" "
                + "level=\"HLT\" "
                + "lower_chain_name=\"" + FormatLowerChainName() + "\" "
                + "pass_through=\"" + FormatDoubles(passthroughvalue) + "\" "
                + "prescale=\"" + FormatDoubles(prescalevalue) + "\" "
                + "rerun_prescale=\"" + FormatDoubles(rerunValue) + "\">\n";
        return chainString;
    }


    private String FormatLowerChainName() {
        String lcn = chain.get_lower_chain_name();
        if (lcn.equals("~")) {
            lcn = "";
        }
        return lcn;
    }
    
    /**
     *
     * @param d
     * @return
     */
    public static String FormatDoubles(double d)
    {
        if(d == (long) d)
            return String.format("%d",(long)d);
        else
            return String.format("%s",d);
    }
    
    private void SetPrescaleValues(ArrayList<HLTPrescale> psList) {
        for (HLTPrescale p : psList)
        {
            HLTPrescaleType type = p.get_type();
            switch (type){
                case Pass_Through :
                    passthroughvalue = p.get_value();
                    break;
                case ReRun :
                    rerunValue = p.get_value();
                    break;
                case Prescale :
                    prescalevalue = p.get_value();
                    break;
                case Stream :
                    streamPrescale= p.get_value();
                    break;
            }
        }
    }

    private String WriteTriggerTypeList() throws SQLException  {        
        
        if (chain.getTriggerTypes().isEmpty()){
            return "\t\t\t<TRIGGERTYPE_LIST/>\n";
        }
        
        String returnString = "\t\t\t<TRIGGERTYPE_LIST>\n";
        for (HLTTriggerType type : chain.getTriggerTypes()) {
            returnString+="\t\t\t<TRIGGERTYPE bit=\"" + type.get_typebit() + "\"/>\n";
        }
        return returnString;
    }


    private String WriteStreams() {        
        String streamString ="\t\t\t<STREAMTAG_LIST>\n";
        
        for (HLTTriggerStream stream : chain.getStreams()) {            
            streamString += WriteStream(stream);
        }
        
        streamString+="\t\t\t</STREAMTAG_LIST>\n";
        return streamString;
    }

    private String WriteStream(HLTTriggerStream stream) {
        return "\t\t\t\t<STREAMTAG obeyLB=\"" + FormatObeyLB(stream) + "\" "
                + "prescale=\"" + FormatStreamPrescaleValue(stream) + "\" "
                + "stream=\"" + stream.get_name() + "\" "
                + "type=\"" + stream.get_type() + "\"/>\n";
    }

    private String FormatStreamPrescaleValue(HLTTriggerStream stream) {
        String prescale;
        if (!stream.get_name().toLowerCase().equals("express")) {
            prescale = stream.get_stream_prescale();
        } else {
            prescale = FormatDoubles(streamPrescale);
        }
        return prescale;
    }

    private String FormatObeyLB(HLTTriggerStream stream) {
        //PJB convert between string and int for obeyLB
        String obeyLBstr = "";
        if (stream.get_obeyLB() == 1) {
            obeyLBstr = "yes";
        }
        if (stream.get_obeyLB() == 0) {
            obeyLBstr = "no";
        }
        return obeyLBstr;
    }

    private String WriteGroups() {
        String groupsString = "\t\t\t<GROUP_LIST>\n";
        for (HLTTriggerGroup group : chain.getTriggerGroups()) {
            groupsString+="\t\t\t\t<GROUP name=\"" + group.get_name() + "\"/>\n";
        }
        groupsString+="\t\t\t</GROUP_LIST>\n";
        return groupsString;
    }

    private String WriteSignatureList() {
        String signtureListString = "";
        
        if(chain.getSignatures().isEmpty()){
            signtureListString = "\t\t\t<SIGNATURE_LIST/>\n";      
        }
        else{
            signtureListString = "\t\t\t<SIGNATURE_LIST>\n";
            for (HLTTriggerSignature signature : chain.getSignatures()) {
                signtureListString += WriteSignature(signature);
            }
            signtureListString+="\t\t\t</SIGNATURE_LIST>\n";
        }
        return signtureListString;
    }

    private String WriteSignature(HLTTriggerSignature signature) {
        String signtureString ="\t\t\t\t<SIGNATURE logic=\"" + signature.get_logic() + "\" " + "signature_counter=\"" + signature.get_signature_counter() + "\">\n";
        for (HLTTriggerElement element : signature.getElements()) {
            /// only write out elements with element counter > -1
            if (element.get_element_counter() > -1) {
                signtureString+="\t\t\t\t\t<TRIGGERELEMENT te_name=\"" + element.get_name() + "\"/>\n";
            }
        }
        signtureString+="\t\t\t\t</SIGNATURE>\n";
        return signtureString;
    }
}
