/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.XML.Writers;

import triggerdb.Entities.L1.L1Random;

/**
 *
 * @author giannell
 */
public class XMLWriter_L1Random {

    private L1Random random;
    
    /**
     *
     * @param random
     */
    public XMLWriter_L1Random(L1Random random){
        this.random = random;
    }
    
    /**
     *
     * @return
     */
    public String Write() {
        return "  <Random " + 
                    "name0=\"Random0\" cut0=\"" + random.get_cut0() + "\" " + 
                    "name1=\"Random1\" cut1=\"" + random.get_cut1() + "\" " + 
                    "name2=\"Random2\" cut2=\"" + random.get_cut2() + "\" " + 
                    "name3=\"Random3\" cut3=\"" + random.get_cut3() + "\" " + 
                    "/>\n";
    }
    
}
