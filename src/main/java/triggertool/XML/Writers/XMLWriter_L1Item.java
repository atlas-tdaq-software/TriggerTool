package triggertool.XML.Writers;


import triggerdb.Entities.L1.L1Item;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import triggerdb.Connections.ConnectionManager;
import triggertool.L1Records.L1ItemDefinitionBuilder;
import triggertool.XML.TriggerDescriptionNode;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author William
 */
public final class XMLWriter_L1Item {

    private final L1Item item;
    private final TriggerDescriptionNode topnode;

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");

    /**
     *
     * @param it
     * @throws ParseException
     * @throws SQLException
     */
    public XMLWriter_L1Item(final L1Item it) throws ParseException, SQLException {
        item = it;
        topnode = TriggerDescriptionNode.buildTriggerDescriptionNode(item.get_definition());
        topnode.setThresholdDescription(item.getThresholds());
    }

    /**
     *
     * @return
     * @throws IOException
     */
    public String writeXML() throws IOException {
        ConnectionManager mrg = ConnectionManager.getInstance();
        String binary = item.get_trigger_type();
        String def = L1ItemDefinitionBuilder.GetDefinition(item);
        String output = "    <TriggerItem ctpid=\"" + item.get_ctp_id()
                + "\" partition=\"" + item.get_partition()
                + "\" name=\"" + item.get_name()
                + "\" complex_deadtime=\"" + item.get_priority()
                + "\" definition=\"" + def
                + "\" trigger_type=\"" + binary;
        //Check if the monitor column exists in the DB
        if(mrg.L1ItemMonitorColumnExists == 1 && !item.get_monitor().equals("None")){
            logger.log(Level.FINE, "Writing monitor{0}", item.get_monitor());
            output += "\" monitor=\"" + item.get_monitor();
        }
                //+ "\" version=\"" + item.get_version()
        output += "\">\n";

        output += this.topnode.writeXML("      ");
        output += "\n    </TriggerItem>\n";
        
        return output;
    }
}


