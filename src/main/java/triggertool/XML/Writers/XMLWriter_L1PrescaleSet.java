/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.XML.Writers;

import triggerdb.Entities.L1.L1Prescale;
import triggertool.L1Records.CutToValueConverter;

/**
 *
 * @author giannell
 */
public class XMLWriter_L1PrescaleSet {
    
    final private L1Prescale prescaleSet;
    
    /**
     *
     * @param prescaleSet
     */
    public XMLWriter_L1PrescaleSet(L1Prescale prescaleSet){
        this.prescaleSet = prescaleSet;        
    }

    /**
     *
     * @return
     */
    public String Write() {        
        if (prescaleSet.get_id() >= 0) {             
            return FirstLine() + Prescales() + FinalLine();
        }
        else{
            return "  <PrescaleSet name=\"Blank\" version=\"1\">\n  </PrescaleSet>";
        }
    }

    private String FirstLine() {

        return "  <PrescaleSet name=\"" + prescaleSet.get_name() + "\" type=\"" + prescaleSet.get_type() + "\" menuPartition=\""+ prescaleSet.get_partition() + "\">\n";
    }
    
    private String Prescales() {
        StringBuilder out = new StringBuilder(10000);
        for (int i = 0; i < L1Prescale.LENGTH; i++)
        {
            int dbValue = prescaleSet.get_val(i+1);
            String cut = CutToValueConverter.getHexStringFromInteger(dbValue);
            Integer rounded = CutToValueConverter.calculatePrescaleFromCut(dbValue).intValue();            
            out.append("    <Prescale ctpid=\"").append(i).append("\" cut=\"").append(cut).append("\" value=\"").append(rounded).append("\"/>\n");
        }
        
        return out.toString();
    }
    
    private String FinalLine() {
        return "  </PrescaleSet>\n";
    }

}
