/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggertool.XML.Writers;

import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import triggerdb.Entities.L1.L1Bunch;
import triggerdb.Entities.L1.L1BunchGroup;
import triggerdb.Entities.L1.L1BunchGroupSet;

/**
 *
 * @author Michele
 */
public class XMLWriter_BunchGroupSet {

    private L1BunchGroupSet bgSet;

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");

    /**
     *
     * @param set
     */
    public XMLWriter_BunchGroupSet(final L1BunchGroupSet set) {
        bgSet = set;
    }

    /**
     *
     * @return
     * @throws SQLException
     */
    public String Write() throws SQLException {
        if (bgSet.get_id() == -1) {

            L1Bunch dummyBunch = new L1Bunch();
            dummyBunch.set_bunch_number(1);

            ArrayList<L1Bunch> bunches = new ArrayList<>();
            bunches.add(dummyBunch);

            L1BunchGroup group0 = new L1BunchGroup();
            group0.set_internal_number(0);
            group0.set_partition(0);
            group0.set_Bunches(bunches);

            L1BunchGroup group1 = new L1BunchGroup();
            group1.set_internal_number(1);
            group1.set_partition(0);
            group1.set_Bunches(bunches);

            ArrayList<SimpleEntry<L1BunchGroup, String>> groupList = new ArrayList<>();

            List<L1BunchGroupSet> sets = L1BunchGroupSet.getBunchGroupSets();
            if (!sets.isEmpty()) {
                L1BunchGroupSet sampleSet = new L1BunchGroupSet(sets.get(0).get_id());
                bgSet.set_name(sampleSet.get_name());
                ArrayList<SimpleEntry<L1BunchGroup, String>> groupRefList = sampleSet.getBunchGroups();
                groupList.add(new SimpleEntry<>(group0, groupRefList.get(0).getValue()));
                groupList.add(new SimpleEntry<>(group1, groupRefList.get(1).getValue()));
                for (int groupIndex = 2; groupIndex < groupRefList.size(); groupIndex++) {
                    L1BunchGroup dummyGroup = new L1BunchGroup();
                    dummyGroup.set_internal_number(groupIndex);
                    groupList.add(new SimpleEntry<>(dummyGroup, groupRefList.get(groupIndex).getValue()));
                }

            }
            else{
                logger.warning("No Bunch Group Sets in Database - cannot generate default set in XML without one to use as a template!");
                return "";
            }
            bgSet.setBunchGroups(groupList);
            //return "";
        }
        return OpenLine() + BunchGroups() + CloseLine();
    }

    private String OpenLine() {
        return "  <BunchGroupSet name=\"" + bgSet.get_name() + "\" menuPartition=\"" + bgSet.get_partition() + "\">\n";
    }

    private String BunchGroups() {
        String returnString = "";
        for (AbstractMap.SimpleEntry<L1BunchGroup, String> bg : bgSet.getBunchGroups()) {
            returnString += "    <BunchGroup internalNumber=\"" + bg.getKey().get_internal_number() + "\" name=\"" + bg.getValue();
            if (bg.getKey().get_Bunches().isEmpty()) {
                returnString += "\"/>\n";
            } else {
                returnString += "\">\n";
                returnString += Bunches(bg.getKey());
                returnString += "    </BunchGroup>\n";
            }
        }
        return returnString;
    }

    private String Bunches(L1BunchGroup bg) {
        String returnString = "";
        for (L1Bunch b : bg.get_Bunches()) {
            returnString += "      <Bunch bunchNumber=\"" + b.get_bunch_number() + "\"/>\n";
        }
        return returnString;
    }

    private String CloseLine() {
        return "  </BunchGroupSet>\n";
    }
}
