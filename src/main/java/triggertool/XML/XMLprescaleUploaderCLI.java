package triggertool.XML;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import static java.lang.Math.round;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import triggerdb.PrescaleSetAliasLumi;
import triggerdb.Entities.HLTLinks.HLTTM_PS;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTPrescale;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.HLT.HLTPrescaleType;
import triggerdb.Entities.HLT.HLTTriggerChain;
import triggerdb.Entities.HLT.HLTTriggerMenu;
import triggerdb.Entities.L1Links.L1TM_PS;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.L1.L1Menu;
import triggerdb.Entities.L1.L1Prescale;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.Entities.alias.PrescaleSetAlias;
import triggertool.Components.AliasTwikiDisplay;
import triggertool.Components.AliasTypeRef;
import triggertool.Components.AliasUploadDialog;

/**
 * Reads the prescale sets in all files in the path and tries to save them and
 * link them to the L1 menu id given in the xml file. This class checks that
 * trigger item names match those on DB for the given menu id. If the names do
 * not match it will abort. The path can be either the path to a specific file
 * or to a directory containing several .xml files, in this case all .xml files
 * in the dir will be uploaded.
 *
 * TODO use delegated classes for L1 and HLT.
 *
 * @author tiago perez
 */
public final class XMLprescaleUploaderCLI {

    /**
     * Message Logger.
     */
    public static final Logger logger = Logger.getLogger("TriggerTool");

    /**
     * Load the prescale set from the XML on the path to the give super master
     * table.
     *
     * @param path the path to the XML file or directory.
     * @param smk the super master key.
     * @throws java.sql.SQLException
     */
    public static void load(final String path, final int smk) throws SQLException {
        XMLprescaleUploaderCLI loader = new XMLprescaleUploaderCLI(path, null);
        loader.load(smk);
    }
    /**
     * Vector with the name of the XML files.
     */
    public List<String> xmlFiles;
    private final JTextArea theText = new JTextArea();
    private final java.awt.Frame parent;
    /**
     * Default constructor. Checks the path, looks for files in path and
     * generates the list of XML files to load.
     *
     * @param path Path to XML files or directory.
     * @param parent
     */
    public XMLprescaleUploaderCLI(final String path, java.awt.Frame parent) {
        this.parent = parent;
        this.LoadXMLFileNames(path, null);
    }

    /**
     * Iterates over a list of file names, reads the prescale sets and saves
     * them to DB.
     *
     * @param smk The Super Master Key of the menu to load the prescale set to.
     * @throws java.sql.SQLException
     */
    public void load(final int smk) throws SQLException {
        if (this.xmlFiles == null) {
            return;
        }
        List<String> files = this.xmlFiles;
        // Load SMT
        // TODO this need a exception catcher.
        SuperMasterTable smt = new SuperMasterTable(smk);
        // Load L1
        L1Master l1Master = smt.get_l1_master_table();
        L1Menu l1Menu = l1Master.get_menu();
        // Load HLT
        HLTMaster hltMaster = smt.get_hlt_master_table();
        HLTTriggerMenu hltMenu = hltMaster.get_menu();

        String listoffiles = "Number of files " + files.size();
        for (String s : files) {
            listoffiles += "\n   " + s;
        }
        logger.info(listoffiles);

        for (String fileName : files) {
            try {
                if (!isAliasSummaryFileThenLoad(fileName, l1Menu, hltMenu, smt, new ArrayList<String>())) {
                    int[] tmpids = new int[2];
                    loadSingleFile(fileName, l1Menu, hltMenu, smk, tmpids);
                }
            } catch (ParserConfigurationException ex) {
                logger.log(Level.SEVERE, "Could not configure parser: {0}", ex.toString());
            } catch (SAXException ex) {
                logger.log(Level.SEVERE, "Could not parse xml file ''{0}'':{1}", new Object[]{fileName, ex.toString()});
            } catch (IOException ex) {
                logger.log(Level.SEVERE, "Could not read the file ''{0}'':{1}", new Object[]{fileName, ex.toString()});
            } 
        }
    }

    /**
     * Grabs and checks the XML files passed to it. Then tries to save them
     * If a old ps link is returned unhide it so that it is viewable.
     *
     * @param fileName the file name....
     * @param l1Menu the l1 menu
     * @param hltMenu the hlt menu
     * @param smk the smk
     * @param newId the ids returned for the  l1/hlt ps keys after the save.
     */
    private void loadSingleFile(final String fileName, final L1Menu l1Menu, final HLTTriggerMenu hltMenu, final int smk, int[] newId) throws SQLException {
        logger.log(Level.INFO, " Uploading prescale set from {0}", fileName);
        XMLprescales xmlF = new XMLprescales(fileName);
        newId[0] = -1;
        newId[1] = -1;
        if (xmlF.checkFile()) {
            if (xmlF.getLevel() == XMLprescales.LEVEL1) {
                if (l1Menu != null) {
                    newId[0] = this.doWorkL1(xmlF, l1Menu, fileName);
                    if(newId[0] != -1){
                        setUnhidden(l1Menu.get_id(), newId[0], false);
                    }
                } else {
                    logger.log(Level.SEVERE, " No L1 Menu asociated to SMK: {0}. Skipping file {1}", new Object[]{smk, fileName});
                }
            } else if (xmlF.getLevel() == XMLprescales.HLT) {
                if (hltMenu != null) {
		    newId[1] = this.doWorkHLT(xmlF, hltMenu, fileName);
                    if(newId[1] != -1){
                        setUnhidden(hltMenu.get_id(), newId[1], true);
                    }
                } else {
                    logger.log(Level.SEVERE, " No HLT Menu asociated to SMK: {0}. Skipping file {1}", new Object[]{smk, fileName});
                }
            }
        }
    }

    private void loadSingleFileTest(final String fileName, final L1Menu l1Menu, final HLTTriggerMenu hltMenu, final int smk, int[] newId) {
        newId[0] = 15;
        newId[1] = 27;
    }

    /**
     *
     * @param fileName
     * @param l1Menu
     * @param hltMenu
     * @param smt
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws SQLException
     */
    public boolean isAliasSummaryFileThenLoad(final String fileName,
            final L1Menu l1Menu, final HLTTriggerMenu hltMenu, final SuperMasterTable smt, ArrayList<String> results)
            throws ParserConfigurationException, SAXException, IOException, SQLException {

        File f = new File(fileName);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(f);
        doc.getDocumentElement().normalize();
        if (!doc.getDocumentElement().getNodeName().equals("ALIAS_DEFINITION")) {
            return false; // not an Alias-File
        }
        String aliasName = doc.getDocumentElement().getAttribute("alias");
        String freeName = doc.getDocumentElement().getAttribute("description");
        String comment = doc.getDocumentElement().getAttribute("comment");
        boolean isDefault = doc.getDocumentElement().getAttribute("default").equals("1");
        logger.log(Level.INFO, "Starting upload of ALIAS file ''{0}''", fileName);
        
        if(!AliasTypeRef.types.contains(aliasName)){
              JOptionPane.showMessageDialog(null,
                    "Selected File Contains Invalid Alias Type, should one of: " + AliasTypeRef.types + ", stopping.", "Set has no effect.",
                    JOptionPane.WARNING_MESSAGE);
              return false;
        }
        
        AliasUploadDialog dialog = new AliasUploadDialog(null, true, aliasName, freeName, comment, smt.get_id());
        //dialog.pack();
        dialog.setLocationRelativeTo(this.parent);
        ArrayList<Object> result = dialog.showDialog();
        
        if(!(Boolean)result.get(3)){
            return false;
        }
        aliasName = (String)result.get(0);
        freeName = (String)result.get(1);
        comment = (String)result.get(2);
        
        String canonicalPathFileName = f.getCanonicalPath();
        String canonicalPath;
        if(canonicalPathFileName.contains("\\")){
            canonicalPath = canonicalPathFileName.substring(0, canonicalPathFileName.lastIndexOf("\\")) + "\\";
        }
        else{
            canonicalPath = canonicalPathFileName.substring(0, canonicalPathFileName.lastIndexOf("/")) + "/";    
        }
        // save the alias (just the alias name e.g. PHYSICS and the free form)
        PrescaleSetAlias psAlias = new PrescaleSetAlias(-1);
        psAlias.set_name(aliasName);
        psAlias.set_name_free(freeName);
        psAlias.set_comment(comment);
        psAlias.set_default(isDefault);
        psAlias.save();
        //psAlias.clearAliases(l1Menu.get_id(), hltMenu.get_id());

        int aliasId = psAlias.get_id();

        results.add(aliasName);
        results.add(freeName);
        
        Boolean errors = false;
        //SuperMasterTable _smt = new SuperMasterTable(smk);
        NodeList nList = doc.getElementsByTagName("SET");
        ArrayList<ArrayList<Object>> savedPSKs = new ArrayList<>();
        for (int i = 0; i < nList.getLength(); i++) {
            Element e = (Element) nList.item(i);

            isDefault = (e.getAttribute("default").equals("1"));
            String l1file = canonicalPath + e.getAttribute("l1file");
            String hltfile = canonicalPath + e.getAttribute("hltfile");
            String lowlumi = e.getAttribute("lowlumi");
            String highlumi = e.getAttribute("highlumi");
            String lowercomment = e.getAttribute("comment");
            logger.log(Level.INFO, "l1file={0}\nhltfile={1}\nlowlumi={2}, highlumi={3}, isdefault={4}", new Object[]{l1file, hltfile, lowlumi, highlumi, isDefault});

            int[] tmpids = new int[2];
            loadSingleFile(l1file, l1Menu, hltMenu, smt.get_id(), tmpids);

            int l1psid = tmpids[0];
            loadSingleFile(hltfile, l1Menu, hltMenu, smt.get_id(), tmpids);

            int hltpsid = tmpids[1];
            ArrayList<Object> tempStore = new ArrayList<>();
            if (l1psid > 0 && hltpsid > 0) {
                logger.log(Level.INFO, "Prescale uploads successful: l1 psk={0}, hlt psk={1}", new Object[]{l1psid, hltpsid});
                PrescaleSetAliasLumi lumi = new PrescaleSetAliasLumi(smt, aliasId, l1psid, hltpsid);
                lumi.set_lum_min_bulk(lowlumi);
                lumi.set_lum_max_bulk(highlumi);
                lumi.set_comment(lowercomment);
                lumi.set_default(isDefault);
                lumi.save();


                logger.log(Level.INFO, "Saving alias {0}\n   {1}\n   {2}", new Object[]{psAlias, lumi.get_l1_prescale_set_alias(), lumi.get_hlt_prescale_set_alias()});
            } else {
                if (l1psid <= 0) {
                    logger.severe("L1 Prescale upload not successful");
                }
                if (hltpsid <= 0) {
                    logger.severe("HLT Prescale upload not successful");
                }
                errors = true;
                logger.log(Level.SEVERE, "Will not create an alias for lumi range {0} - {1}", new Object[]{lowlumi, highlumi});
                //return false;
            }
            
            String release = smt.getReleases().get(0).get_name();
            tempStore.add(release);
            tempStore.add(smt.get_id());
            tempStore.add(l1psid);
            tempStore.add(hltpsid);
            tempStore.add(round(Double.parseDouble(lowlumi) / 1e30));
            tempStore.add(round(Double.parseDouble(highlumi) / 1e30));
            savedPSKs.add(tempStore);
        }
        
        Collections.sort(savedPSKs, new Comparator<List<Object>>() {
            @Override
            public int compare(List<Object> a, List<Object> b) {
                Double lhs = Double.parseDouble(a.get(5).toString());
                 Double rhs = Double.parseDouble(b.get(5).toString());
                 return rhs.compareTo(lhs); //reverse order for descending sort
                 
            }
        });
        
        if(errors){
            JOptionPane.showMessageDialog(null,
                    "Unable to correctly upload alias components for all lumi points due to missing prescales.\nPlease fix resulting alias!", "Missing alias components!",
                    JOptionPane.ERROR_MESSAGE);
        }
        else{
            AliasTwikiDisplay display = new AliasTwikiDisplay(aliasName + ": " + freeName);
            display.drawDisplay(savedPSKs);
        }
        return true;
    }

    /**
     * Checks pathName+fileName and tries to load all files there. It will check
     * the format off all files and keep only the one which are readable and
     * properly formatted.
     *
     * @param fileName the name of file (can be full path)
     * @param pathName
     * @return the number of correct files or -1;
     */
    private int LoadXMLFileNames(final String fileName, final String pathName) {
        int numberOfFiles = -1;
        File f;
        // resolve wildcard and re-call
        if (fileName.contains("*") || fileName.contains("?")) {
            File[] files = this.filterFileNames(fileName);
            for (File file : files) {
                String name = file.getAbsolutePath();
                this.LoadXMLFileNames(name, null);
            }
            return files.length;
        }

        // open file
        if (pathName != null && !(pathName.isEmpty())) {
            f = new File(pathName, fileName);
        } else {
            f = new File(fileName);
        }
        // and check readability
        if (!f.exists() || !f.canRead()) {
            logger.log(Level.SEVERE, " Can''t read from: {0}. \n skipping.", fileName);
            return numberOfFiles;
        }

        if (f.isFile()) {
            if (f.isHidden()) {
                logger.log(Level.SEVERE, " You are trying to load the hidden file: {0}. \n skipping.", fileName);
                return numberOfFiles;
            }
            if (fileName.startsWith("#") || fileName.endsWith("~")) {
                logger.log(Level.SEVERE," You are probably trying to load the back up" + " file: {0}. \n skipping.", fileName);
                return numberOfFiles;
            }
            if (!fileName.substring(fileName.lastIndexOf(".")).equalsIgnoreCase(".xml")) {
                String ext = fileName.substring(fileName.lastIndexOf("."));
                logger.log(Level.SEVERE, " Please provide only xml files. Skipping file: {0}\n Extension = {1}", new Object[]{fileName, ext});
                return numberOfFiles;
            }
            String _fileName = f.getAbsolutePath();

            if (this.xmlFiles == null) {
                this.xmlFiles = new ArrayList<>();
            }
            this.xmlFiles.add(_fileName);
            numberOfFiles = 1;
        } else if (f.isDirectory()) {
            String _canPath = null;
            try {
                _canPath = f.getCanonicalPath();
            } catch (IOException ex) {
                logger.log(Level.SEVERE, " {0}", ex);
            }
            String[] _fNames = f.list();
            if (_fNames.length > 0) {
                for (String _fName : _fNames) {
                    this.LoadXMLFileNames(_fName, _canPath);
                }
                numberOfFiles = this.xmlFiles.size();
            }
        }
        return numberOfFiles;
    }

    /**
     * Compare trigger item's names and CTP ids of a Map and a L1Menu. If they
     * match, it fill the values of the map in a L1Prescale object.
     *
     * @param theL1Menu a L1Menu.
     * @param myMap a LinkedHashMap with l1 prescale info.
     * @return a L1Prescale with the prescale information from the Map.
     */
    private L1Prescale compareAndFillL1(final L1Menu theL1Menu,
            final L1Prescale l1ps,
            final LinkedHashMap<String, ArrayList<Object>> myMap,
            final String setName) throws SQLException {
        L1Prescale match = new L1Prescale(-1);
        // Copy old prescale set.
        if (l1ps != null) {
            logger.log(Level.INFO," Found prescale set in DB matching the name in the xml"
                    + " file.\n L1Prescale set {0} will be updated.", l1ps.toString());
            //match.set_lumi(l1ps.get_lumi());
            match.set_shift_safe(l1ps.get_shift_safe());
            for (int i = 1; i <= L1Prescale.LENGTH; i++) {
                match.set_val(i, l1ps.get_val(i));
            }
        } else {
            logger.log(Level.INFO, " Creating a new L1Prescale with name {0}", setName);
        }
        List<L1Item> items = theL1Menu.getItems();
        List<ArrayList<String>> notPresentItems = new ArrayList<>();

        for (L1Item l1i : items) {
            String ctpId = l1i.get_ctp_id().toString();
            if (myMap.containsKey(ctpId)) {
                String xmlName = myMap.get(ctpId).get(1).toString().trim();
                String dbName = l1i.get_name().trim();
                if (xmlName.equalsIgnoreCase(dbName)) {
                    //Added in the cut into the vector..... hopefully
                    String cutval = myMap.get(ctpId).get(2).toString().trim();
                    // The ctp id in the L1Prescale runs from 1 to 256.
                    int ctpIdp1 = Integer.parseInt(ctpId) + 1;
                    match.set_val(ctpIdp1, Integer.parseInt(cutval));
                } else {
                    logger.log(Level.SEVERE, " Item  in XML file doesn''t match the item in DB. CTP ID= {0} dbName: {1}  xmlName: {2}", new Object[]{ctpId, dbName, xmlName});
                    return null;
                }
            } else {
                ArrayList<String> v = new ArrayList<>();
                v.add(ctpId);
                v.add(l1i.get_name());
                notPresentItems.add(v);
            }
        }
        if (notPresentItems.size() > 0) {
            String msg = notPresentItems.size() + " not present in XML file. \n";
            if (l1ps == null) {
                msg += "The following items are set to -1\n";
            } else {
                msg += "The following items are set to the values in L1Prescale "
                        + l1ps.get_id() + ":" + l1ps.get_name() + " v"
                        + l1ps.get_version() + ".\n";
            }
            for (ArrayList<String> vector : notPresentItems) {
                msg += "CTP ID:" + vector.get(0) + "  " + vector.get(1) + "\n";
            }
            // Details output for debug and development.
            logger.fine(msg);
            String infoMsg = notPresentItems.size() + " trigger items are not "
                    + "present in the XML file.\n";
            if (l1ps != null) {
                infoMsg += " These items are set to the values on the set " + l1ps.get_id() + ":" + l1ps.get_name() + " v"
                        + l1ps.get_version() + ".\n";
            } else {
                infoMsg += " These items are set to the default value (-1).\n";
            }
            infoMsg += " Check log file (level fine) for more details.";
            // normal info.
            logger.info(infoMsg);
        }
        match.set_name(setName);
        return match;
    }

    /**
     * Compare trigger item's names and counter of a Map and a L1Menu. If they
     * match, it fills the values of the map in a HLTPrescale object.
     *
     * @param hltMenu the HLT menu
     * @param myMap a LinkedHashMap with the prescale info.
     * @return a HLTPrescale with the prescale information from the Map.
     */
    private HLTPrescaleSet compareAndFillHLT(final HLTTriggerMenu hltMenu,
            final LinkedHashMap<String, ArrayList<Object>> myMap,
            final String prescaleSetName) throws SQLException {
        // The Prescale set.
        HLTPrescaleSet prescaleSet = new HLTPrescaleSet(-1);
        // The regular prescales
        ArrayList<HLTPrescale> prescales = new ArrayList<>();
        ArrayList<HLTPrescale> passthroughs = new ArrayList<>();
        ArrayList<HLTPrescale> reruns = new ArrayList<>();
        ArrayList<HLTTriggerChain> chains = hltMenu.getChains();
        if (chains.isEmpty()) {
            logger.log(Level.WARNING, " HLTMenu: {0} has not trigger chains.", hltMenu.toString());
            return null;
        }

        //Map with the existing express streams.
        LinkedHashMap<Integer, HLTPrescale> xpressChains = new LinkedHashMap<>();
        //hltMenu.getChains();
        ArrayList<Integer> xpressCounters = hltMenu.loadExpressStreams();

        List<ArrayList<String>> notPresentItems = new ArrayList<>();
        for (HLTTriggerChain chain : chains) {
            String chainName = chain.get_name();
            int dbChainCounter = chain.get_chain_counter();
            // this create a new prescale with deffault values.
            HLTPrescale prescale = new HLTPrescale();
            prescale.set_chain_counter(dbChainCounter);
            prescale.set_type(HLTPrescaleType.Prescale);
            HLTPrescale passthrough = new HLTPrescale();
            passthrough.set_chain_counter(dbChainCounter);
            passthrough.set_type(HLTPrescaleType.Pass_Through);
            HLTPrescale rerun = new HLTPrescale();
            rerun.set_chain_counter(dbChainCounter);
            rerun.set_type(HLTPrescaleType.ReRun);
            
            boolean hasExpress = false;
            HLTPrescale xpressChain = new HLTPrescale();
            if(xpressCounters.contains(dbChainCounter)){
                xpressChain.set_chain_counter(dbChainCounter);
                xpressChain.set_type(HLTPrescaleType.Stream);
                xpressChain.set_condition("express");
                hasExpress = true;
            }
	    
            // set defaults.
            if (myMap.containsKey(chainName)) {
                Integer xmlChainCounter = (Integer) myMap.get(chainName).get(0);

                // Normal case: we fill the new prescale with the values from
                // xml file.
                if (xmlChainCounter.equals(dbChainCounter)) {
                    String psVal = myMap.get(chainName).get(3).toString().trim();
                    String ptVal = myMap.get(chainName).get(4).toString().trim();
                    String rrString = myMap.get(chainName).get(6).toString().trim();
                    String[] rrSplit = rrString.split(":");
                    prescale.set_value(Double.parseDouble(psVal));
                    passthrough.set_value(Double.parseDouble(ptVal));
                    rerun.set_value(Double.parseDouble(rrSplit[0]));
                    rerun.set_condition(rrSplit[1]);
                    // Add new prescale to Set.
                    prescales.add(prescale);
                    passthroughs.add(passthrough);
                    reruns.add(rerun);
                    if(hasExpress){
                        String xpVal = myMap.get(chainName).get(5).toString().trim();
                        xpressChain.set_value(Double.parseDouble(xpVal));
                        xpressChains.put(dbChainCounter, xpressChain);
                    }
                } else {
                    logger.log(Level.SEVERE," Chain counter in XML file doesn''t match the" + " counter in DB. Name = {0} dbCounter: {1}  xmlCounter: {2}", new Object[]{chainName, dbChainCounter, xmlChainCounter});
                    return null;
                }
            } else {
                // Debug info about chains not included in xml file
                ArrayList<String> v = new ArrayList<>();
                v.add(chain.get_chain_counter().toString());
                v.add(chain.get_name());
                notPresentItems.add(v);
            }
        }

        // There are items in the map not present in the chains vector. 
        // look for them and write out an error mgs.
        if (prescales.size() != notPresentItems.size() + myMap.size()) {
            List<String> chainNames = new ArrayList<>();
            List<String> badChainNames = new ArrayList<>();
            for (HLTTriggerChain chain : chains) {
                chainNames.add(chain.get_name());
            }
            for (Entry<String, ArrayList<Object>> mapName : myMap.entrySet()) {
                String n = mapName.getKey();
                if (!chainNames.contains(n)) {
                    badChainNames.add(n);
                }
            }
            String msg = "";
            for (String bad : badChainNames) {
                msg += bad + "\n";
            }
            logger.log(Level.SEVERE," Chain names in XML file and HLT Menu {0}"
                    + " do not match.\n" + " The following chains are not present in the menu:\n{1} \n", new Object[]{hltMenu.toString(), msg});
            return null;
        }
        prescaleSet.getPrescales().addAll(prescales);
        prescaleSet.getPrescales().addAll(passthroughs);
        prescaleSet.getPrescales().addAll(reruns);
        // Add express stream prescales to PrescaleSet.
        Set<Entry<Integer, HLTPrescale>> set = xpressChains.entrySet();
        Iterator<Entry<Integer, HLTPrescale>> iter = set.iterator();
        while (iter.hasNext()) {
            Entry<Integer, HLTPrescale> me = iter.next();
            HLTPrescale xprStream = me.getValue();
            prescaleSet.getPrescales().add(xprStream);
        }

        if (notPresentItems.size() > 0) {
            String msg = notPresentItems.size() + " not present in XML file. \n";
            msg += "The folowing items are set to -1\n";
            for (List<String> vector : notPresentItems) {
                msg += "CTP ID:" + vector.get(0) + "  " + vector.get(1) + "\n";
            }
            // Details output for debug and development.
            logger.fine(msg);
            String infoMsg = notPresentItems.size() + " trigger items are not "
                    + "present in the XML file, these items are set to the "
                    + "default values. Check log file (level fine) for more "
                    + "details.";
            // normal info.
            logger.info(infoMsg);
        }
        prescaleSet.set_name(prescaleSetName);
        return prescaleSet;
    }

    /**
     * Saves the given L1Prescale and links it to the L1 menu with id
     * <code>menuId</code>
     *
     * @param l1ps the L1Prescale to save.
     * @param menuId the id of the menu to link l1ps to.
     * @param fileName The name of the original xml file (only for logger).
     * @return the id of the saved prescale set or -1;
     */
    private int saveAndLinkPrescales(L1Prescale l1ps, int menuId, String fileName) {
        int psId;
        try {
            psId = l1ps.save();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, " Error saving prescales from file {0}", fileName);
            return -1;
        }

        //
        L1TM_PS l1TmPs = new L1TM_PS();
        l1TmPs.set_menu_id(menuId);
        l1TmPs.set_prescale_set_id(psId);
        int l1TmPsId;
        try {
            l1TmPsId = l1TmPs.save();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, " Error linking prescale set {0} to menu {1}", new Object[]{psId, menuId});
            return -1;
        }
        logger.log(Level.INFO, "Prescale set saved with id: {0}.\n\tSet linked to L1 Menu id: {1}.\n\tLink id (L1TM2PS_ID): {2}", new Object[]{psId, menuId, l1TmPsId});
        return psId;
    }

    /**
     * Saves the given HLTPrescaleSet and links it to the L1 menu with id
     * <code>menuId</code>
     *
     * @param psSet the HLTPrescaleSet to save.
     * @param menuId the id of the menu to link l1ps to.
     * @param fileName The name of the original xml file (only for logger).
     * @return the id of the saved prescale set or -1;
     */
    private int saveAndLinkPrescales(HLTPrescaleSet psSet, int menuId, String fileName) {
        int psId;
        try {
            psId = psSet.save(menuId);
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, " Error saving prescales from file {0}\n{1}", new Object[]{fileName, ex});
            return -1;
        }
        if (psId < 0) {
            logger.log(Level.SEVERE, " Error saving prescales from file {0}", fileName);
            return -1;
        }
        // Link
        HLTTM_PS tm2ps = new HLTTM_PS();
        tm2ps.set_menu_id(menuId);
        tm2ps.set_prescale_set_id(psId);
        int tm2psId;
        try {
            tm2psId = tm2ps.save();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, " Error linking prescale set {0} to menu {1}", new Object[]{psId, menuId});
            return -1;
        }
        logger.log(Level.INFO, "HLT Prescale set saved with id: {0}.\n\tSet linked to HLT Menu id: {1}.\n\tLink id (HLTTM2PS_ID): {2}", new Object[]{psId, menuId, tm2psId});
        return psId;
    }

    /**
     * Finds existing file names using unix style wildcards: '*' and '?'.
     *
     * @param path the path containing wildcards
     * @return an array of filenames.
     */
    private File[] filterFileNames(String path) {
        File f = new File(path);
        String dir = f.getParent();
        final String name = f.getName();
        final String pre;
        final String post;
        final int l;
        if (name.indexOf("*") > -1) {
            pre = name.substring(0, name.indexOf("*"));
            post = name.substring(name.indexOf("*") + 1);
            l = 0;
        } else if (name.indexOf("?") > -1) {
            l = 1;
            pre = name.substring(0, name.indexOf("?"));
            post = name.substring(name.indexOf("?") + 1);
        } else {
            pre = name;
            post = name;
            l = 0;
        }
        FilenameFilter myFilter = new FilenameFilter() {

            @Override
            public boolean accept(File dir, String inName) {

                if (inName.startsWith(pre) && inName.endsWith(post)) {
                    if (l == 1) {
                        int prei = pre.length();
                        int posti = post.length();
                        String len = inName.substring(prei, inName.length() - posti);

                        if (len.length() != 1) {
                            return false;
                        }
                    }
                    return true;
                } else {
                    return false;
                }
            }
        };
        File fDir = new File(dir);
        File[] files = fDir.listFiles(myFilter);
        return files;
    }

    /**
     * Do the upload of the l1 prescale set.
     *
     * @param xmlF the XMLPrescales.
     * @param menu the L1 menu to link the prescale to.
     * @param fileName the name of the file.
     */
    private int doWorkL1(final XMLprescales xmlF,
            final L1Menu menu, final String fileName) throws SQLException {
        if (menu == null) {
            logger.severe(" Error loading menu ");
            return -1;
        }
        // Create a map (similar to the table in the GUI) and fill it with the 
        // values from the XML file.
        LinkedHashMap<String, ArrayList<Object>> myMap = xmlF.createPrescaleSetL1();
        // Check that XML and DB prescale items match
        L1Prescale uploadedL1Ps = this.compareAndFillL1(menu, null, myMap, xmlF.getPrescaleSetName());
        if (uploadedL1Ps == null) {
            return -1;
        }

        // Save
        return this.saveAndLinkPrescales(uploadedL1Ps, menu.get_id(), fileName);
    }

    /**
     * Checks the give HLT menu, if menu exist and matches the menu id from xml
     * file it will reuse it. Creates a Map with prescale info from xml file.
     * Check the db for a prescaleSet with the same name as the name give in xml
     * file if one set is found, it will be retrieved from db and updated, else
     * it will create a new psset will all prescales set to -1
     *
     * @param xmlF the xml file
     * @param hltMenu the menu to link the prescale set to.
     * @param fileName the name of the input file.
     */
    private int doWorkHLT(final XMLprescales xmlF,
            final HLTTriggerMenu hltMenu, final String fileName) throws SQLException {
        // Menu not present, bad SMK.
        if (hltMenu == null) {
            logger.log(Level.SEVERE," You are trying to load a prescale set to an empty"
                    + " HLT MENU. Did you forgot to give a SMK?" + "or maybe the SMK you tried doesn''t exist in this DB." + "\n File {0}.\n", fileName);
            return -1;
        }
        LinkedHashMap<String, ArrayList<Object>> myMap = xmlF.createPrescaleSetHLT();
        // Check that XML and DB prescale items match
        String setName = xmlF.getPrescaleSetName();
        HLTPrescaleSet psSet = this.compareAndFillHLT(hltMenu, myMap, setName);

        if (psSet == null) {
            return -1;
        }

        // Save
        return this.saveAndLinkPrescales(psSet, hltMenu.get_id(), fileName);
    }
    
    /**
     * If a set is returned from the XML upload make sure it is set to
     * be unhidden so it can be seen in the ps viewer.
     *
     * @param menuID the menu the ps is linked to
     * @param psID the ID of the prescale you want to unhide
     * @param level, l1 = 0 hlt = 1
     */
    private void setUnhidden(int menuId, int psId, boolean level) throws SQLException{
        //unhide the l1
        if(!level){
            L1TM_PS.unhidePS(menuId, psId);
        } else {
            HLTTM_PS.unhidePS(menuId, psId);
        }
    }
}
