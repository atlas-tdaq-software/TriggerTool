package triggertool.XML;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.collections.map.MultiValueMap;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.L1.*;
import triggerdb.Entities.L1Links.L1TM_TT;
import triggerdb.Entities.L1Links.L1TM_TTM;
import triggertool.XML.Writers.XMLWriter_BunchGroupSet;
import triggertool.XML.Writers.XMLWriter_L1Item;
import triggertool.XML.Writers.XMLWriter_L1PrescaleSet;
import triggertool.XML.Writers.XMLWriter_L1Random;

/**
 * This class contains all the code to convert the Database into a L1 XML file
 * Write a L1 XML file from a L1 Master key.
 * 
 * @author Simon Head
 * @author Alex Martyniuk
 */
public final class L1_DBtoXML {

    /** Message Log */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    /** List of internal triggers that are not associated with thresholds */
    private final String internal_trig_list[] = { "RNDM", "PCLK", "BGRP" };

    /**
     * Function to create the dtd file. This is required by the Trigger Menu
     * Compiler, so that it accepts the XML file as input. It describes the XML
     * schema we use.
     * 
     * @param filename
     *            Output filename including full path
     */
    private void writeDTD(String filename) {
        try {
            try (BufferedWriter out = new BufferedWriter(new FileWriter(filename))) {
                out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                out.write("\n");
                out.write("<!ENTITY % input_name '(SLOT7 | SLOT8 | SLOT9 | CTPCORE) \"SLOT9\"'>\n");
                out.write("<!ENTITY % type_name '(ELECTRICAL | OPTICAL) \"ELECTRICAL\"'>\n");
                out.write("<!ENTITY % cable_name '(CON0 | CON1 | CON2 | CON3) \"CON0\"'>\n");
                out.write("<!ENTITY % module_name '(CTPIN | CTPMON) \"CTPIN\"'>\n");
                out.write("<!ENTITY % trigger_name '(BGRP0 | BGRP1 | BGRP2 | BGRP3 | BGRP4 | BGRP5 | BGRP6 | BGRP7 | BGRP8 | BGRP9 | BGRP10 | BGRP11 | BGRP12 | BGRP13 | BGRP14 | BGRP15 | RNDM0 | RNDM1 | RNDM2 | RNDM3) '>\n");
                out.write("\n");
                out.write("<!ELEMENT LVL1Config (TriggerMenu,TriggerCounterList?,TriggerThresholdList)>\n");
                out.write("<!ATTLIST LVL1Config\n");
                out.write("     name               CDATA           #IMPLIED\n");
                out.write("     ctpVersion         CDATA           #IMPLIED\n");
                out.write("     l1Version          CDATA           #IMPLIED\n");
                out.write("     id                 CDATA           #IMPLIED>\n");
                out.write("\n");
                out.write("<!ELEMENT TriggerMenu (TriggerItem)*>\n");
                out.write("<!ATTLIST TriggerMenu\n");
                //out.write("     id                 CDATA           #REQUIRED\n");
                out.write("     name               ID              #REQUIRED\n");
                out.write("     version            CDATA           #IMPLIED\n");
                out.write("     phase              CDATA           #IMPLIED>\n");
                out.write("\n");
                out.write("<!ELEMENT  TriggerItem (AND|OR|NOT|TriggerCondition|InternalTrigger)>\n");
                out.write("<!ATTLIST  TriggerItem\n");
                out.write("     id                 CDATA           #IMPLIED\n");
                out.write("     name               CDATA           #REQUIRED\n");
                out.write("     version            CDATA           #IMPLIED\n");
                out.write("     ctpid              CDATA           #REQUIRED\n");
                out.write("     partition          CDATA           #IMPLIED\n");
                out.write("     complex_deadtime   CDATA           #IMPLIED\n");
                out.write("     definition         CDATA           #IMPLIED\n");
                out.write("     trigger_type       CDATA           #IMPLIED\n");
                out.write("     monitor            CDATA           #IMPLIED>\n");
                out.write("\n");
                out.write("<!ELEMENT AND (AND|OR|NOT|TriggerCondition|InternalTrigger)+>\n");
                out.write("\n");
                out.write("<!ELEMENT OR (AND|OR|NOT|TriggerCondition|InternalTrigger)+>\n");
                out.write("\n");
                out.write("<!ELEMENT NOT (AND|OR|NOT|TriggerCondition|InternalTrigger)>\n");
                out.write("\n");
                out.write("<!ELEMENT TriggerCondition EMPTY>\n");
                out.write("<!ATTLIST TriggerCondition\n");
                out.write("     id                 CDATA           #IMPLIED\n");
                out.write("     name               CDATA           #REQUIRED\n");
                out.write("     version            CDATA           #IMPLIED\n");
                out.write("     triggerthreshold   CDATA           #REQUIRED\n");
                out.write("     multi              CDATA           #REQUIRED>\n");
                out.write("\n");
                out.write("<!ELEMENT InternalTrigger EMPTY>\n");
                out.write("<!ATTLIST InternalTrigger\n");
                out.write("     id                 CDATA           #IMPLIED\n");
                out.write("     name               %trigger_name;  #REQUIRED>\n");
                out.write("\n");
                out.write("<!ELEMENT TriggerThresholdList (TriggerThreshold*)>\n");
                out.write("\n");
                out.write("<!ELEMENT TriggerThreshold (TriggerThresholdValue*, Cable)>\n");
                out.write("<!ATTLIST TriggerThreshold\n");
                out.write("     id                 CDATA           #IMPLIED\n");
                out.write("     name               CDATA           #REQUIRED\n");
                out.write("     version            CDATA           #IMPLIED\n");
                out.write("     type               CDATA           #REQUIRED\n");
                out.write("     bitnum             CDATA           #REQUIRED\n");
                out.write("     OPL                CDATA           \"NO\"\n");
                out.write("     confirm            CDATA           \"0\"\n");
                out.write("     active             CDATA           #IMPLIED\n");
                out.write("     mapping            CDATA           #IMPLIED\n");
                out.write("     seed               CDATA           #IMPLIED\n");
                out.write("     bcdelay            CDATA           #IMPLIED\n");
                out.write("     seed_multi         CDATA           #IMPLIED\n");
                out.write("     input              CDATA           #IMPLIED>\n");
                out.write("\n");
                out.write("<!ELEMENT TriggerThresholdValue EMPTY>\n");
                out.write("<!ATTLIST TriggerThresholdValue\n");
                out.write("     id                 CDATA           #IMPLIED\n");
                out.write("     name               CDATA           #REQUIRED\n");
                out.write("     version            CDATA           #IMPLIED\n");
                out.write("     type               CDATA           #REQUIRED\n");
                out.write("     thresholdval       CDATA           #REQUIRED\n");
                out.write("     em_isolation       CDATA           \"5\"\n");
                out.write("     had_isolation      CDATA           \"10\"\n");
                out.write("     had_veto           CDATA           \"20\"\n");
                out.write("     window             CDATA           \"2\"\n");
                out.write("     phimin             CDATA           #REQUIRED\n");
                out.write("     phimax             CDATA           #REQUIRED\n");
                out.write("     etamin             CDATA           #REQUIRED\n");
                out.write("     etamax             CDATA           #REQUIRED\n");
                out.write("     priority           CDATA           #IMPLIED>\n");
                out.write("\n");
                out.write("<!ELEMENT Cable (Signal+)>\n");
                out.write("<!ATTLIST Cable\n");
                out.write("     name               CDATA           #REQUIRED\n");
                out.write("     input              %input_name;\n");
                out.write("     ctpin              CDATA           #IMPLIED\n");
                out.write("     type               %type_name;\n");
                out.write("     connector          %cable_name;>\n");
                out.write("\n");
                out.write("<!ELEMENT Signal EMPTY>\n");
                out.write("<!ATTLIST Signal\n");
                out.write("     range_begin        CDATA           #REQUIRED\n");
                out.write("     range_end          CDATA           #REQUIRED\n");
                out.write("     clock              CDATA           #IMPLIED>\n");
                out.write("\n");
                out.write("<!ELEMENT TriggerCounterList (TriggerCounter*)>\n");
                out.write("\n");
                out.write("<!ELEMENT TriggerCounter (AND|OR|NOT|TriggerCondition)>\n");
                out.write("<!ATTLIST TriggerCounter\n");
                out.write("     id                 CDATA           #IMPLIED\n");
                out.write("     name               CDATA           #REQUIRED\n");
                out.write("     version            CDATA           #IMPLIED\n");
                out.write("     type               %module_name;>\n");
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, "Failed to write dtd - {0}", e.getMessage());
        }
    }

    /**
     * Write the XML. This is the main code to save the file. XML tags are
     * written by hand. If a prescale_id of less than or equal to 0 is supplied
     * then the prescales are not written to the file. TODO: This method writes
     * out L1PrescaleValues as FLOATs (L1_XMLtoDB reads floats in). this has to
     * change: In the XML we want have FLOAT and NMD info, either as 3 integers
     * or as a HEX.
     * 
     * @param l1mt_id
     *            ID for the L1 Master table we want to save to a file
     * @param prescale_id
     *            ID for the prescale set
     * @param filename
     *            Filename that we're going to save to
     * @param bunch_group_set_id
     * @param forTriggerMenuCompiler
     *            Flag to turn on printing dtd or not. 0 off 1 on.
     * @throws java.sql.SQLException
     */
    public void writeXML(int l1mt_id, int prescale_id, String filename, int bunch_group_set_id,
                         boolean forTriggerMenuCompiler) throws SQLException {
        try {
            if (forTriggerMenuCompiler) {
                String pathname = "";
                if(filename.contains("/")) {
                    pathname = filename.substring(0, filename.lastIndexOf('/'));
                }
                writeDTD(pathname + "/LVL1MenuDDR.dtd");
            }

            logger.log(Level.INFO, "About to write L1 XML file: {0}", filename);
            try(BufferedWriter out = new BufferedWriter(new FileWriter(filename))) {

                // load the data
                L1Master l1Master = new L1Master(l1mt_id);
            
                Calendar cal = Calendar.getInstance(TimeZone.getDefault());

                String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
                java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT);

                sdf.setTimeZone(TimeZone.getDefault());

                logger.fine("Writing LVL1Config node");

                out.write("<?xml version=\"1.0\"?>\n");
                if (forTriggerMenuCompiler) {
                    out.write("<!DOCTYPE LVL1Config SYSTEM \"LVL1MenuDDR.dtd\">\n");
                }            
                String ver = triggertool.TriggerTool.TT_TAG;
                String usr = ConnectionManager.getInstance().getInitInfo().getUserName();
                out.write("<LVL1Config name=\"" + l1Master.get_name() + "\" ctpVersion=\"" + l1Master.get_ctpVersion() + "\" l1Version=\"" + l1Master.get_l1Version() + "\">\n");
                String comment = "  <!--This file was generated by the TriggerTool (ver. " + ver + ") -->\n"
                    + "  <!--By " + usr + " on " + sdf.format(cal.getTime()) + "-->\n";
                out.write(comment);

                // dump l1 menu
                L1Menu l1Menu = l1Master.get_menu();

                out.write("  <!--No. L1 thresholds defined: "+l1Menu.getTMTT().size() +"-->\n");
                out.write("  <!--No. L1 items defined: "+l1Menu.getItems().size()+"-->\n");

                logger.fine("Writing TriggerMenu node");
                out.write("  <TriggerMenu name=\"" + l1Menu.get_name() + "\" phase=\"" + l1Menu.get_phase() + "\">\n");
                for (L1Item item : l1Menu.getItems()) {
                    try {
                        out.write(new XMLWriter_L1Item(item).writeXML());
                    } catch (ParseException ex) {
                        logger.log(Level.WARNING, "Caught ParseException:{0} for item {1}", new Object[]{ex.getMessage(), item.get_name()});
                    }
                }

                out.write("  </TriggerMenu>\n");

                // Prescale Set
                if(! forTriggerMenuCompiler ) {
                    logger.fine("Writing PrescaleSet node");
                    out.write(new XMLWriter_L1PrescaleSet(new L1Prescale(prescale_id)).Write());
                }
                
                
                //Want to grab the monitors from the DB
                //The single counter monitors are stored one per entry
                //The combination counters are stored in multiple entries.
                //Need to infer them from the counter names.
                MultiValueMap CTPMON_Map = new MultiValueMap();
                ArrayList<L1TM_TTM> CTPMON_Vec = new ArrayList<>();
            
                if( (l1Menu.getMonitors().size() + CTPMON_Vec.size()) > 0) {
                    out.write("  <TriggerCounterList>\n");
                    //Loop over the monitors
                    for (L1TM_TTM monitor : l1Menu.getMonitors()) {
                        //Grab any that contain logical combinations...
                        if(monitor.get_counter_name().contains("_AND_") || monitor.get_counter_name().contains("_OR_")
                           || monitor.get_counter_name().contains("_NOT_")){
                            CTPMON_Map.put(monitor.get_counter_name(), monitor);
                            if(CTPMON_Vec.contains(monitor) == false){
                                CTPMON_Vec.add(monitor);
                            }
                            //Otherwise write out the simple single monitor
                        } else {
                            out.write("    <TriggerCounter name=\"" + monitor.get_counter_name() + "\" type=\"" + monitor.get_type() + "\">\n");
                            out.write("      <TriggerCondition name=\"" + monitor.get_counter_name() + "\" triggerthreshold=\"" + monitor.get_threshold().get_name() + "\" multi=\"" + monitor.get_multiplicity() +"\"/>\n");
                            out.write("    </TriggerCounter>\n");
                        }
                    }
                
                    for (L1TM_TTM current : CTPMON_Vec) {
                        out.write("    <TriggerCounter name=\"" + current.get_counter_name() + "\" type=\"" + current.get_type() + "\">\n");
                        //Currently can only do naive logical combinations.
                        //TODO: need to implement a solution for more complicated logical combinations.
                        if(current.get_counter_name().contains("_AND_")){
                            out.write("      <AND>\n");
                        } else if(current.get_counter_name().contains("_OR_")){
                            out.write("      <OR>\n");
                        } else if(current.get_counter_name().contains("_NOT_")){
                            out.write("      <NOT>\n");
                        }
                        @SuppressWarnings("unchecked")
                            Collection<L1TM_TTM> combMon = (Collection<L1TM_TTM>) CTPMON_Map.getCollection( current.get_counter_name() );
                        for (L1TM_TTM cond : combMon) {
                            out.write("        <TriggerCondition name=\"" + cond.get_counter_name() + "\" triggerthreshold=\"" + current.get_threshold().get_name() + "\" multi=\"" + current.get_multiplicity() +"\"/>\n");
                        }
                        if(current.get_counter_name().contains("_AND_")){
                            out.write("      </AND>\n");
                        } else if(current.get_counter_name().contains("_OR_")){
                            out.write("      </OR>\n");
                        } else if(current.get_counter_name().contains("_NOT_")){
                            out.write("      </NOT>\n");
                        }
                        out.write("    </TriggerCounter>\n");
                    }
                    out.write("  </TriggerCounterList>\n");
                }

                out.write("  <TriggerThresholdList>\n");

                for (L1TM_TT cable : l1Menu.getTMTT()) {
                    L1Threshold thresh = cable.get_threshold();

                    boolean internalTrigger = false;

                    for ( String inttrig : internal_trig_list) {
                        if( thresh.get_name().startsWith(inttrig)) {
                            internalTrigger = true;
                            break;
                        }
                    }

                    if (!internalTrigger) {

                        if (!thresh.get_type().equals("ZB")) {
                            out.write("    <TriggerThreshold active=\"" + thresh.get_active() + "\" bitnum=\"" + thresh.get_bitnum() + "\" id=\"" + thresh.get_id() 
                                      + "\" mapping=\"" + thresh.get_mapping() + "\" name=\"" + thresh.get_name() 
                                      + "\" type=\"" + thresh.get_type() + "\" version=\"" + thresh.get_version()
                                      + "\">\n");
                        } else {
                            out.write("    <TriggerThreshold active=\"" + thresh.get_active() + "\" bitnum=\"" + thresh.get_bitnum() + "\" id=\"" + thresh.get_id() 
                                      + "\" mapping=\"" + thresh.get_mapping() + "\" name=\"" + thresh.get_name() 
                                      + "\" type=\"" + thresh.get_type() + "\" version=\"" + thresh.get_version()
                                      + "\" seed=\"" + thresh.get_seed() + "\" bcdelay=\"" + thresh.get_bcdelay()
                                      + "\" seed_multi=\"" + thresh.get_seed_multi() + "\">\n");
                        }

                        for (L1ThresholdValue ttv : cable.get_threshold().getThresholdValues()) {

                            out.write("      <TriggerThresholdValue em_isolation=\"" + ttv.get_em_isolation() + "\" etamin=\"" + ttv.get_eta_min()
                                      + "\" etamax=\"" + ttv.get_eta_max() + "\" had_isolation=\"" + ttv.get_had_isolation() 
                                      + "\" had_veto=\"" + ttv.get_had_veto() + "\" name=\"" + ttv.get_name() 
                                      + "\" phimin=\"" + ttv.get_phi_min() + "\" phimax=\"" + ttv.get_phi_max()
                                      + "\" priority=\"" + ttv.get_priority() + "\" thresholdval=\"" + ttv.get_pt_cut() 
                                      + "\" type=\"" + ttv.get_type() + "\" window=\"" + ttv.get_window() + "\"/>\n");
                        }

                        out.write("      <Cable connector=\"" + cable.get_cable_connector() + "\" input=\"" + cable.get_cable_ctpin() + "\" name=\"" + cable.get_cable_name() + "\">\n");
                        String clock="";
                        if(cable.get_cable_clock()>=0) {
                            clock = " clock=\"" + cable.get_cable_clock() +"\"";
                        }
                        out.write("        <Signal range_begin=\"" + cable.get_cable_start() + "\" range_end=\"" + cable.get_cable_end() + "\"" + clock + "/>\n");
                        out.write("      </Cable>\n");

                        out.write("    </TriggerThreshold>\n");
                    }
                }

                out.write("  </TriggerThresholdList>\n");
                
                // Random
                if( ! forTriggerMenuCompiler ) {
                    logger.fine("Writing Random nodes");               
                    out.write(new XMLWriter_L1Random(l1Master.get_random()).Write());
                }
            
                // Bunch Group Set
                if( ! forTriggerMenuCompiler ) {
                    /*logger.fine("writing BunchGroupSet node using MC set");
                    int id = -1;
                    for (L1BunchGroupSet set : L1BunchGroupSet.getBunchGroupSets()){
                        if (set.get_name().equals("MC")){
                            id = set.get_id();
                        }
                    }
                    out.write(new XMLWriter_BunchGroupSet(new L1BunchGroupSet(id)).Write());*/
                    //Write BGS based on user selection
                    out.write(new XMLWriter_BunchGroupSet(new L1BunchGroupSet(bunch_group_set_id)).Write());

                }        

                // Prescale Clock
                if( ! forTriggerMenuCompiler ) {
                    logger.fine("Writing PrescaledClock node");
                    L1PrescaledClock pc = l1Master.get_prescaled_clock();
                    out.write("  <PrescaledClock clock1=\"" + pc.get_clock1() + "\" clock2=\"" + pc.get_clock2() + "\" name=\"" + pc.get_name() + "\"/>\n");
                }
                
                // Calo Info
                if( ! forTriggerMenuCompiler ) {
                    logger.fine("Write CaloInfo node");
                    L1CaloInfo ci = l1Master.get_calo_info();
                    out.write("  <CaloInfo name=\"" + ci.get_name() + "\" global_em_scale=\"" + ci.get_global_em_scale() + "\" global_jet_scale=\"" + ci.get_global_jet_scale() + "\">\n");
                    out.write("    <METSignificance  xeMin=\"" + ci.get_xs_xe_min() +
                              "\" xeMax=\"" + ci.get_xs_xe_max() +
                              "\" teSqrtMin=\"" + ci.get_xs_tesqrt_min() + 
                              "\" teSqrtMax=\"" + ci.get_xs_tesqrt_max() + 
                              "\" xsSigmaScale=\"" + ci.get_xs_sigma_scale() +
                              "\" xsSigmaOffset=\"" + ci.get_xs_sigma_offset() + "\"/>\n" );

                    //Mark TODO: here needs to have the new isolation
                    //Grab the isolations from the DB
                    ArrayList<L1CaloIsolation> IsoList = ci.get_all_isolations();
                    for(L1CaloIsolation isolation:IsoList){
                        //Just incase the threshold names change....
                        out.write("    <Isolation thrtype=\"" + isolation.get_thr_type() + "\">\n" );
                        for (int i = 1; i < 6; ++i) {
                            logger.log(Level.INFO, "{0}", i);
                            L1CaloIsoParam param = new L1CaloIsoParam(isolation.get_par(i));
                            if(param.get_iso_bit() == -1) continue;
                            out.write("      <Parametrization isobit=\"" + param.get_iso_bit() + 
                                      "\" offset=\"" + param.get_offset() + 
                                      "\" slope=\"" + param.get_slope() + 
                                      "\" mincut=\"" + param.get_min_cut() + 
                                      "\" upperlimit=\"" + param.get_upper_limit() +
                                      "\" etamin=\"" + param.get_eta_min() +
                                      "\" etamax=\"" + param.get_eta_max() +
                                      "\" priority=\"" + param.get_priority() + "\"/>\n" );
                        }
                        out.write("    </Isolation>\n" );
                    }
                    ArrayList<L1CaloMinTob> Tobs = ci.get_all_tobs();
                    for(L1CaloMinTob tob:Tobs){
                        if(tob.get_thr_type().equals("EM") || tob.get_thr_type().equals("TAU")){
                            out.write("    <MinimumTOBPt thrtype=\"" + tob.get_thr_type() + 
                                          "\" ptmin=\"" + tob.get_pt_min() + 
                                          "\" etamin=\"" + tob.get_eta_min() + 
                                          "\" etamax=\"" + tob.get_eta_max() +
                                          "\" priority=\"" + tob.get_priority() + "\"/>\n" );
                        } else {
                            out.write("    <MinimumTOBPt thrtype=\"" + tob.get_thr_type() +
                                          "\" window=\"" + tob.get_window()+
                                          "\" ptmin=\"" + tob.get_pt_min() +
                                          "\" etamin=\"" + tob.get_eta_min() + 
                                          "\" etamax=\"" + tob.get_eta_max() +
                                          "\" priority=\"" + tob.get_priority() + "\"/>\n" );
                        }
                    }
                    out.write("  </CaloInfo>\n" ); 
                }

                // Mucpti Info
                if( ! forTriggerMenuCompiler ) {
                    logger.fine("Writing MuctpiInfo node");
                    L1MuctpiInfo mi = l1Master.get_muctpi_info();
                    out.write("  <MuctpiInfo name=\"" + mi.get_name() 
                              + "\" low_pt=\"" + mi.get_low_pt() 
                              + "\" high_pt=\"" + mi.get_high_pt() 
                              + "\" max_cand=\"" + mi.get_max_cand() 
                              +"\"/>\n");
                }
            
                out.write("</LVL1Config>\n");
            }
            logger.log(Level.INFO, "Finished writing L1 XML {0}", filename);
        } catch (IOException e) {
            logger.log(Level.WARNING,"Some error while writing L1 XML" + " {0}", Arrays.toString(e.getStackTrace()));
        }
    }
}
