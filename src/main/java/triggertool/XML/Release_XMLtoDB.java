package triggertool.XML;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

import triggerdb.Entities.HLT.HLTRelease;

/**
 *
 * @author William
 */
public final class Release_XMLtoDB {

    /** Message Log */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    private static final long serialVersionUID = 1L;
    private Document release_doc = null;
    
    //map of loaded trigger elements
    Map<String, Integer> loadedTEs = null;
    
     /** 
     * Populate the HLTRelease table with one record from the release XML file
     * 
     * @param relfile
     * @return ID of the release record in the database
     * @throws java.sql.SQLException
     * @throws triggertool.XML.ConfigurationException
     */
    public int loadRelease(String relfile) throws SQLException, ConfigurationException {
        
        int hre_id = -1;
        
        //see if there is a file
        File f = new File(relfile);

        if (!f.exists()&&!f.getName().contains("xml")){
            logger.fine("No release file - maybe its a string?");
            return -2;
        }

        //parse the document
        logger.fine("Checking consistency of release xml file");
        release_doc = readRelease(relfile);
        if (release_doc == null) {
            logger.warning("Problem with release xml file");
            return hre_id;
        }
        
        //extract the information and save the release
        NodeList releaseInfo = release_doc.getElementsByTagName("RELEASE");
        
        if (releaseInfo.getLength() == 1) {
            Element releaseInfo_element = (Element) releaseInfo.item(0);
            HLTRelease release = new HLTRelease(-1);
            String release_name = releaseInfo_element.getAttribute("name");
            release.set_name(release_name);
            release.set_version(1);
                        
            hre_id = release.save();
        } else {
            String exMessage = "HLT Release file corrupted:";
            exMessage += "\t" + releaseInfo.getLength() + " releases in file";
            throw new ConfigurationException(exMessage);
        }

        logger.log(Level.INFO, "Finished uploading the release. ID = {0}", hre_id);

        return hre_id;    
    }

     
    /**
     * Take release info from string argument on command line.
     * 
     * @param rel the release name.
     * @return the release id in DB.
     * @throws java.sql.SQLException
     * @throws Exception 
     */
    public int loadReleaseString(String rel) throws SQLException {
    
        int hre_id;

        HLTRelease release = new HLTRelease(-1);
        release.set_name(rel);
        release.set_version(1);
        hre_id = release.save();

        logger.log(Level.INFO, "Finished uploading the release. ID = {0}", hre_id);

        return hre_id;    
    }

    
    ////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @param xmlfile
     * @return
     * @throws ConfigurationException
     */
    public Document readRelease(String xmlfile) throws ConfigurationException {
        logger.log(Level.FINE, "Creating xml document from {0}", xmlfile);
        // Unfortunately we must deep copy the document otherwise the problems arise
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        // dbf.setValidating(true);

        DocumentBuilder doc_builder;
        FileInputStream in_file;
        Document document;

        try {
            doc_builder = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            logger.warning("ERROR: Failed to create document builder");
            e.printStackTrace();
            throw new ConfigurationException(e.getMessage());
        }

        try {
            in_file = new FileInputStream(xmlfile);
        } catch (FileNotFoundException e) {
            logger.log(Level.WARNING, "ERROR: Cannot open file: {0}", xmlfile);
            e.printStackTrace();
            throw new ConfigurationException(e.getMessage());
        }

        try {
            document = doc_builder.newDocument();
            Document document_temp = doc_builder.parse(in_file);
            // Use the Clone with deep copy to avoid null pointer exception
            document =
                    (Document) document_temp.cloneNode(true);
            document.normalize();
            in_file.close();
        } catch (SAXException e) {
            logger.log(Level.WARNING, "ERROR: error while reading file: {0}", xmlfile);
            e.printStackTrace();
            throw new ConfigurationException(e.getMessage());
        } catch (IOException e) {
            logger.warning("ERROR: io exception while read file");
            e.printStackTrace();
            throw new ConfigurationException(e.getMessage());
        }

        return document;
    }
  
}
