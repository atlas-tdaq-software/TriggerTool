package triggertool.XML;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.HLT.*;
import triggerdb.Entities.HLTLinks.HLTTM_PS;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggertool.XML.Readers.XMLReader_HLTChain;
import triggertool.XML.Readers.XMLReader_HLTTriggerStream;

/**
 *
 * @author William
 */
public final class Hlt_XMLtoDB {

    /**
     * Message Log
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    private static final long serialVersionUID = 1L;
    private String menu_name = "";
    private String prescale_set_name = "";
    private String mastertable_name = "";
    //private Document release_doc = null;
    private Document hltSetup_doc = null, menu_doc = null;
    private final ArrayList<String> prodTEs = new ArrayList<>();
    /**
     * to store the new HLT prescale key
     */
    private int newHLTPrescaleKey = 0;
    private Element hlt_setup_element = null;
    private boolean dummySetup = false;
    private ArrayList<String> comp_alias_v = null;
    //map of loaded trigger elements
    Map<String, Integer> loadedTEs = null;
    /// Map of component name / alias combination to the loaded HLT Components
    Map<String, HLTComponent> loadedComps = null;
    /// Map of Stream names to loaded HLTTriggerStreams
    Map<Integer, HLTTriggerStream> loadedStreams = null;
    //List of the currently testing Components
    ArrayList<HLTComponent> currentComps = null;
    //List of the currently testing Paramters
    ArrayList<HLTParameter> currentParas = null;
    //To hold the element hashes....
    ArrayList<Integer> ElementHashes = new ArrayList<>();
    //List of the currently testing Components
    ArrayList<HLTTriggerElement> currentElems = null;

    /**
     * Reads the HLT xml configuration files, creates the TriggerMenu, L2 Setup
     * and HLT setup and compactsaves it to DB.
     *
     * @param hltSetupXML path to the L2 setup xml file.
     * @param menuXML
     * @param configName the configuration name
     * @param pssName
     * @return the id of the HLT master table.
     * @throws java.sql.SQLException
     * @throws triggertool.XML.ConfigurationException
     */
    public int readHltConfig(final String hltSetupXML,
            final String menuXML,
            final String configName,
            final String pssName) throws SQLException, ConfigurationException {

        loadedTEs = new HashMap<>(850);
        loadedComps = new HashMap<>(1000);
        loadedStreams = new HashMap<>(15);
        comp_alias_v = new ArrayList<>();
        currentComps = new ArrayList<>();

        // Initialize paths:
        String _hltxml = "";
        if (hltSetupXML != null
                && hltSetupXML.length() > 0) {
            File f = new File(hltSetupXML);
            if (f.isFile() && f.canRead()) {
                _hltxml = hltSetupXML;
            } else {
                logger.log(Level.SEVERE, "HLT setup XML file {0} does not exist or is not readable. Exiting!", hltSetupXML);
                System.exit(-1);
            }
        }
        String _menuxml = "";
        if (menuXML != null
                && menuXML.length() > 0) {
            File f = new File(menuXML);
            if (f.isFile() && f.canRead()) {
                _menuxml = menuXML;
            } else {
                logger.log(Level.SEVERE, "HLT menu XML file {0} does not exist or is not readable. Exiting!", menuXML);
                System.exit(-1);
            }
        }

        logger.fine("Checking consistency of HLT setup xml file");
        if (!_hltxml.isEmpty()) {
            hltSetup_doc = readHltConfig(_hltxml);
            if (hltSetup_doc == null) {
                logger.warning("Problem with HLT setup xml file");
            }
        } else {
            logger.fine("No HLT setup xml file supplied - will use dummy setup");
            dummySetup = true;
        }

        logger.fine("Checking consistency of menu xml file");
        menu_doc = readHltConfig(_menuxml);
        if (menu_doc == null) {
            logger.warning("Problem with menu xml file");
        }
        //now have a menu document in hand
        //if passed name is "USEXML" then find menu and pss names from inside file!
        String confNameTemp = configName;
        if (confNameTemp.equals("USEXML")) {
            Element menu = (Element) menu_doc.getElementsByTagName("HLT_MENU").item(0);
            if (menu.hasAttribute("menu_name")) {
                confNameTemp = menu.getAttribute("menu_name");
            } else {
                confNameTemp = menu.getAttribute("name");
            }
        }
        String pssNameTemp = pssName;
        if (pssName.equals("USEXML")) {
            Element pss = (Element) menu_doc.getElementsByTagName("HLT_MENU").item(0);
            pssNameTemp = pss.getAttribute("prescale_set_name");
        }
        mastertable_name = confNameTemp;
        menu_name = confNameTemp;
        prescale_set_name = pssNameTemp;

        String msg = "Will upload HLT XML file: " + _menuxml;
        msg += "\nMenu name will be:        " + menu_name;
        msg += "\nHLT PSK name will be:     " + prescale_set_name;
        msg += "\n(Master name will be):    " + mastertable_name;
        logger.info(msg);

        int id_HLTSetup = -1;
        if (hltSetup_doc == null && _hltxml.length() == 0) {
            try {
                id_HLTSetup = loadDummySetup("Dummy_HLT");
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, "Setup could not be loaded for this reason: {0}", ex.getMessage());
                throw ex;
            }
        } else {
            try {
                id_HLTSetup = loadSetup(hltSetup_doc);
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, "Setup could not be loaded for this reason: {0}", ex.getMessage());
                throw ex;
            }
        }

        int id_Menu = -1;
        if (menu_doc == null) {
            logger.severe("HLT Menu file corrupt");
            System.exit(-1);
        }

        try {
            id_Menu = loadMenu(menu_doc);
        } catch (ConfigurationException | SQLException ex) {
            logger.log(Level.SEVERE, "Menu could not be loaded for this reason: {0}", ex.getMessage());
            throw ex;
        }

        int id_Master = loadMaster(mastertable_name, id_Menu, id_HLTSetup);

        // Load prescale set information
        logger.fine("PJB calling load PSS");
         int id_PrescaleSet = loadPrescaleSet(prescale_set_name, menu_doc,id_Menu);
        
        logger.log(Level.INFO, "Finished uploading HLT"
                + "\n\tHLT Master ID =    " + "{0}\n\tHLT Menu ID =      {1}\n\tHLT Prescale Set ID =      {2}", new Object[]{id_Master, id_Menu, id_PrescaleSet});

        //link the prescales to the menu
        logger.log(Level.INFO, "***Adding to HLT Menu {0} the HLT PSS {1}", new Object[]{id_Menu, id_PrescaleSet});
        HLTTM_PS newpslink = new HLTTM_PS();
        newpslink.set_menu_id(id_Menu);
        newpslink.set_prescale_set_id(id_PrescaleSet);
        newpslink.save();

        logger.log(Level.FINE, "REMAINING COMPONENTS: {0}", comp_alias_v.size());
        for (String alias : comp_alias_v) {
            logger.log(Level.FINE, "unloaded comp: {0}", alias);
        }

        return id_Master;
    }

    /**
     *
     * @return
     */
    public int getHLTPrescaleSetKey() {
        return newHLTPrescaleKey;
    }

    /**
     * loadSetup. Reads setup xml file and gets comp elements into memory Also
     * loads the I-compts to the DB Algorithms and their tools are loaded with
     * the menu.
     *
     * @param setup_doc
     * @return integer id of the setup record in the database
     */
    private int loadSetup(Document setup_doc) throws SQLException, ConfigurationException {
        NodeList setup = setup_doc.getElementsByTagName("setup");
        if (setup.getLength() != 1) {
            String exMessage = "Setup file corrupted: " + setup.getLength() + " setup nodes in file";
            throw new ConfigurationException(exMessage);
        }

        Element setup_element = (Element) setup.item(0);

        hlt_setup_element = setup_element;

        HLTSetup new_setup = new HLTSetup();
        //new_setup.set_name(setup_element.getAttribute("name"));
        //PJB should be same as menu name
        new_setup.set_name(menu_name);

        //get all top-level component elements into memory
        // NodeList List_component = setup_element.getElementsByTagName("component");
        NodeList List_component = setup_element.getChildNodes();

        //now, can already add the I-setup components
        for (int iComp = 0; iComp < List_component.getLength(); ++iComp) {
            if (!List_component.item(iComp).getNodeName().equals("component")) {
                continue;
            }

            logger.log(Level.FINE, "Length of current Comp {0}", currentComps.size());

            Element component_element = (Element) List_component.item(iComp);
            //Lets start a test to grab a component and all of its parameters in one go
            int compCheck = checkInfraStructureComponentHash(component_element);

            HLTComponent comp;
            if (compCheck < 0) {
                clearContainers();
                comp = loadInfraStructureComponentHash(component_element, false, false, true);
            } else {
                comp = new HLTComponent(compCheck);
                //logger.info("We found a component! "+compCheck +" its name is "+comp.get_name());
            }

            //The components we add to the setup are the "parent" components.
            //Children components (tools) are linked from these.
            if (comp != null) {
                new_setup.getInfraStructureComponents().add(comp);
            } else {
                logger.log(Level.FINE, "{0}/{1}Component not loaded", new Object[]{component_element.getAttribute("name"), component_element.getAttribute("alias")});
            }

            // Exception for the ToolSvc - load the children of the ToolSvc as upper level algorithms
            String hcp_alias = "";
            if (component_element.hasAttribute("alias")) {
                hcp_alias = component_element.getAttribute("alias");
            }
            if (hcp_alias.equals("ToolSvc") && component_element.getElementsByTagName("component").getLength() > 0) {
                NodeList List_tool = component_element.getChildNodes();
                for (int iTool = 0; iTool < List_tool.getLength(); ++iTool) {
                    // Skip non component elements
                    if (!List_tool.item(iTool).getNodeName().equals("component")) {
                        continue;
                    }
                    Element tool_element = (Element) List_tool.item(iTool);
                    comp = loadInfraStructureComponentHash(tool_element, false, false, true);
                    //The components we add to the setup are the "parent" components.
                    //Children components (tools) are linked from these.
                    if (comp != null) {
                        new_setup.getInfraStructureComponents().add(comp);
                    } else {
                        logger.log(Level.FINE, "{0}/{1}Component not loaded", new Object[]{tool_element.getAttribute("name"), tool_element.getAttribute("alias")});
                    }
                }
            }
        }
        //what have we actually added to new setup?
        //for (HLTComponent comp : new_setup.getInfraStructureComponents()){
        //    logger.info("Removing from vector: " + comp.get_alias());
        //    comp_alias_v.remove(comp.get_alias());
        //    for (HLTComponent childcomp : comp.getAllChildComponents()){
        //        logger.info("Removing from vector: " + childcomp.get_alias());
        //        comp_alias_v.remove(childcomp.get_alias());
        //    }
        //}
        return new_setup.compactsave();
    }

    private void clearContainers() {
        currentParas = new ArrayList<>();
        currentParas.clear();
        currentComps.clear();
    }

    /**
     * Parameters are the children of a Component. They have no children so this
     * should be a fairly simple routine to check if it exists with these
     * parameters and if not compactsave to the database
     *
     * @param parameter_element node in the XML file
     * @return id of the parameter record in the database
     */
    private HLTParameter loadParameter(Element parameter_element) throws SQLException {
        HLTParameter new_param = new HLTParameter();
        new_param.set_name(parameter_element.getAttribute("name"));
        new_param.set_value(parameter_element.getAttribute("value"));
        new_param.set_op(parameter_element.getAttribute("op"));
        //xml file should mark those parameters which are important:
        new_param.set_chain_user_version(false);
        new_param.set_hash();
        // Don't save the parameter if the value length overflow 4000 characters -- should be splitted
        if (new_param.get_value().length() <= 4000) {
            new_param.save();
        } else {
            logger.log(Level.FINE, "Parameter {0} has to be split, more than 4000 characters long: {1}", new Object[]{new_param.get_name().length(), new_param.get_value().length()});
        }
        return new_param;
    }

    /**
     * Not yet checked at all
     *
     * @param menu_doc
     * @param id_L2Setup
     * @param id_EFSetup
     * @param id_Release
     * @return integer id of the menu in the database
     */
    private int loadMenu(Document menu_doc) throws ConfigurationException, SQLException {

        NodeList List_chainLists = menu_doc.getElementsByTagName("CHAIN_LIST");
        NodeList List_sequenceLists = menu_doc.getElementsByTagName("SEQUENCE_LIST");

        if ((List_chainLists.getLength() != 1) || (List_sequenceLists.getLength() != 1)) {
            String exMessage = "Menu file corrupted:";
            if (List_chainLists.getLength() != 1) {
                exMessage += "\t" + List_chainLists.getLength() + " chain lists";
            }
            if (List_sequenceLists.getLength() != 1) {
                exMessage += "\t" + List_sequenceLists.getLength() + " sequence lists";
            }
            throw new ConfigurationException(exMessage);
        }

        Element chainList_element = (Element) List_chainLists.item(0);
        Element sequenceList_element = (Element) List_sequenceLists.item(0);

        NodeList List_chains = chainList_element.getElementsByTagName("CHAIN");
        NodeList List_sequences = sequenceList_element.getElementsByTagName("SEQUENCE");

        HLTTriggerMenu menu = new HLTTriggerMenu();
        if (menu_doc.getDocumentElement().hasAttribute("name")) {
            menu.set_name(menu_doc.getDocumentElement().getAttribute("name"));
        } else {
            menu.set_name("HLTMenu");
        }

        //add chains
        ArrayList<HLTTriggerChain> chains = new ArrayList<>();
        for (int iChain = 0; iChain < List_chains.getLength(); iChain++) {
            Element chain_element = (Element) List_chains.item(iChain);
            HLTTriggerChain chain = loadChain(chain_element, List_sequences);
            if (chain != null) {
                chains.add(chain);
            }
        }
        menu.setChains(chains);

        menu.set_name(menu_name);

        int htm_id = menu.compactsave();

        return htm_id;
    }

    /**
     * Generate the prescale set in the database. If we're written a new Menu
     * then we have to do this
     *
     * @param prescale_set_name
     * @param prescale_set_version
     * @param menu_id
     * @return the id for the prescale set in the database
     */
    private int loadPrescaleSet(String prescale_set_name, Document menu_doc, int menuID) throws ConfigurationException, SQLException {

        //create new set
        HLTPrescaleSet prescaleSet = new HLTPrescaleSet();
        //prescaleSet.set_menu_id(menu_id);
        prescaleSet.set_name(prescale_set_name);

        //loop over the menu doc and assign prescales
        NodeList List_chainLists = menu_doc.getElementsByTagName("CHAIN_LIST");
        Element chainList_element = (Element) List_chainLists.item(0);
        NodeList List_chains = chainList_element.getElementsByTagName("CHAIN");

        for (int iChain = 0; iChain < List_chains.getLength(); iChain++) {

            Element chain_element = (Element) List_chains.item(iChain);
            int chain_counter = Integer.parseInt(chain_element.getAttribute("chain_counter"));
            ArrayList<HLTPrescale> list = new XMLReader_HLTChain().CreatePrescales(chain_element);
            prescaleSet.addPrescales(list);

            NodeList List_streamTagList = chain_element.getElementsByTagName("STREAMTAG_LIST");
            // Sanity check
            boolean allowNoStream = true;
            boolean noStream = List_streamTagList.getLength() == 0;
            if (allowNoStream && noStream) {
                logger.warning("Chain with no STREAMTAG_LIST: allowed");
            }
            if ((List_streamTagList.getLength() != 1) && !(allowNoStream && noStream)) {
                String exMessage = "Menu file corrupted: \t" + List_streamTagList.getLength() + " STREAMTAG_LISTs in file";
                throw new ConfigurationException(exMessage);
            }
            // Load trigger stream
            if (!(allowNoStream && noStream)) { // Test for no streamtags
                Element streamTagList_element = (Element) List_streamTagList.item(0);
                NodeList List_stream = streamTagList_element.getElementsByTagName("STREAMTAG");
                if (List_stream.getLength() == 0 && !allowNoStream) { // Test for no streams
                    String exMessage = "Menu file corrupted:\t No STREAMTAG specified in STREAMTAG_LIST of chain " + chain_element.getAttribute("chain_name");
                    throw new ConfigurationException(exMessage);
                }

                for (int iStream = 0; iStream < List_stream.getLength(); iStream++) {
                    Element stream_element = (Element) List_stream.item(iStream);
                    if (stream_element.getAttribute("stream").equals("express")) {
                        prescaleSet.addPrescale(XMLReader_HLTTriggerStream.CreatePrescales(stream_element, chain_counter));
                    }
                }//end loop over streams
            }//end stream sanity check

        }
        newHLTPrescaleKey = prescaleSet.save(menuID);
        return newHLTPrescaleKey;
    }

//    
//    private int checkChainHash(Element chain_element, NodeList List_sequences) throws Exception{
//        int check = -1;
//        HLTTriggerChain new_chain = loadChain(chain_element, List_sequences, false);
//        return check;
//    }
//    
    private void clearElems() {
        currentElems = new ArrayList<>();
        currentElems.clear();
    }

    /**
     * Load the master table
     *
     * @param mastertable_name name for the master table
     * @param htm_id id in the database for the menu
     * @param idPrescaleSet prescale set id
     * @param hre_id release id
     * @return id of the master table
     */
    private int loadMaster(String mastertable_name, int htm_id, int hst_id) throws SQLException {
        HLTMaster master = new HLTMaster();
        master.set_name(mastertable_name);
        master.set_menu_id(htm_id);
        master.set_setup_id(hst_id);

        return master.save(); // Throws an exception
    }

    /**
     * this is only called from the command line upload - similar code appears
     * in the TT upload dialog... Hence the flag is superfluous (sp)
     *
     * @param fromcommandline
     * @param l1_id
     * @param hlt_id
     * @param smComment the comment to the new SMT.
     * @param replicate
     * @param offline
     * @param configuration_name
     * @param online
     * @return the id of the uploaded SMt i.e. the SMK.
     * @throws java.lang.Exception
     */
    public int buildSMK(boolean fromcommandline, int l1_id, int hlt_id, String configuration_name, boolean online, boolean offline, boolean replicate, final String smComment) throws Exception {

        logger.log(Level.INFO, "fcl{0} {1} {2} {3} online: {4} offline {5} rep: {6}",
                new Object[]{fromcommandline, l1_id, hlt_id, configuration_name, online, offline, replicate});

        if (configuration_name.equals("USEXML")) {
            Element menu = (Element) menu_doc.getElementsByTagName("HLT_MENU").item(0);
            configuration_name = menu.getAttribute("menu_name");
        }

        //Create the SMT  
        SuperMasterTable smt = new SuperMasterTable();
        smt.set_name(configuration_name);

        //set origin and parent:
        smt.set_origin("XML UPLOAD");
        smt.set_parent_key(0);

        //set children including the release
        smt.set_l1_master_table_id(l1_id);
        smt.set_hlt_master_table_id(hlt_id);

        //set status online or offline or to replicate - use the functions in SuperMasterTable
        smt.setOnline(online);
        smt.setMC(offline);
        smt.setRep(replicate);

        //set from command line to suppress output
        logger.info("Calling smt.set_fromcommandline()");
        smt.set_fromcommandline();

        // set comment
        smt.set_comment(smComment);
        int smt_id = smt.save(); // Throws an exception

        return smt_id;
    }

    /**
     *
     * @param xmlfile
     * @return
     * @throws ConfigurationException
     */
    public Document readHltConfig(String xmlfile) throws ConfigurationException {
        logger.log(Level.FINE, "Creating xml document from {0}", xmlfile);
        // Unfortunately we must deep copy the document otherwise the problems arise
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        // dbf.setValidating(true);

        DocumentBuilder doc_builder;
        FileInputStream in_file;
        Document document;

        try {
            doc_builder = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            logger.warning("ERROR: Failed to create document builder");
            throw new ConfigurationException(e.getMessage());
        }

        try {
            in_file = new FileInputStream(xmlfile);
        } catch (FileNotFoundException e) {
            logger.log(Level.WARNING, "ERROR: Cannot open file: {0}\n{1}", new Object[]{xmlfile, e.getMessage()});
            //e.printStackTrace();
            return null;
        }

        try {
            Document document_temp = doc_builder.parse(in_file);
            // Use the Clone with deep copy to avoid null pointer exception
            document = (Document) document_temp.cloneNode(true);
            document.normalize();
            in_file.close();
        } catch (SAXException e) {
            logger.log(Level.WARNING, "ERROR: error while reading file: {0}\n {1}", new Object[]{xmlfile, e.getMessage()});
            //e.printStackTrace();
            throw new ConfigurationException(e.getMessage());
        } catch (IOException e) {
            logger.log(Level.WARNING, "ERROR: io exception while reading file: {0}\n {1}", new Object[]{xmlfile, e.getMessage()});
            //e.printStackTrace();
            throw new ConfigurationException(e.getMessage());
        }

        return document;
    }

    /**
     * Load dummy Setup
     *
     * @param setupName
     * @return id of the dummy setup
     * @throws java.sql.SQLException
     */
    public int loadDummySetup(String setupName) throws SQLException {
        HLTSetup setup = new HLTSetup();
        setup.set_name(setupName);
        return setup.compactsave();
    }

    /**
     *
     * @param chain_element
     * @param List_sequences
     * @return
     * @throws SQLException
     * @throws ConfigurationException
     */
    public HLTTriggerChain loadChain(Element chain_element, NodeList List_sequences) throws SQLException, ConfigurationException {
        HLTTriggerChain chain = new XMLReader_HLTChain().CreateBaseChain(chain_element);

        chain.setSignatures(GetSignatures(chain_element, List_sequences, chain));
        chain.setStreams(SetStreams(chain_element));
        chain.setTriggerTypes(SetTriggerTypes(chain_element, chain));
        chain.setTriggerGroups(SetGroups(chain_element, chain));

        logger.log(Level.FINE, "Saving chain {0}", chain.get_name());
        chain.save();
        return chain;
    }

    private ArrayList<HLTTriggerGroup> SetGroups(Element chain_element, HLTTriggerChain chain) throws ConfigurationException, SQLException {
        ArrayList<HLTTriggerGroup> groups = new ArrayList<>();
        // Check TRIGGERGROUP_LIST
        NodeList List_groupList = chain_element.getElementsByTagName("GROUP_LIST");
        boolean noGroup = List_groupList.getLength() == 0;
        if (noGroup) {
            logger.warning("Chain with no GROUP_LIST: allowed");
        }
        // Sanity check - there can be only 1 GROUP_LIST for a chain
        if ((List_groupList.getLength() != 1) && !noGroup) {
            String exMessage = "Menu file corrupted: \t" + List_groupList.getLength() + " GROUP_LISTs in file";
            throw new ConfigurationException(exMessage);
        }
        // Load Trigger Groups for the Chain Sanity of the group list tested above
        if (!noGroup) {
            Element groupList_element = (Element) List_groupList.item(0);
            NodeList List_group = groupList_element.getElementsByTagName("GROUP");

            //PJB 30/5 some chains have same group defined more than once - ignore multiples
            //else it screws up the compactsave (compares n+1 groups when only n will be compactsaved)
            List<Element> existing_group_elements_v = new ArrayList<>();
            logger.log(Level.FINER, "Chain {0} has this many groups: {1}", new Object[]{chain.toString(), List_group.getLength()});
            for (int igroup = 0; igroup < List_group.getLength(); igroup++) {
                boolean alreadyhavegroup = false;
                Element group_element = (Element) List_group.item(igroup);

                if (group_element.getAttribute("name").isEmpty()) {
                    String exMessage = "Group in chain: " + chain.get_name() + " has empty name which is not allowed.";
                    throw new ConfigurationException(exMessage);
                }

                logger.log(Level.FINER, "Looking at group {0}", group_element.getAttribute("name"));
                //do we have this already
                logger.log(Level.FINER, "Exiting groups: {0}", existing_group_elements_v.size());
                Iterator<Element> it = existing_group_elements_v.iterator();
                while (it.hasNext()) {
                    Element existing_group_element = it.next();
                    if (group_element.getAttribute("name").equals(existing_group_element.getAttribute("name"))) {
                        alreadyhavegroup = true;
                    }
                    logger.log(Level.FINER, "Comparing {0}-{1}-{2}", new Object[]{group_element.getAttribute("name"), existing_group_element.getAttribute("name"), alreadyhavegroup});
                }
                if (!alreadyhavegroup) {
                    groups.add(loadTriggerGroup(group_element));
                    existing_group_elements_v.add(group_element);
                }
            }
        }
        return groups;
    }

    private ArrayList<HLTTriggerType> SetTriggerTypes(Element chain_element, HLTTriggerChain chain) throws SQLException, ConfigurationException {
        ArrayList<HLTTriggerType> types = new ArrayList<>();
        NodeList List_typeList = chain_element.getElementsByTagName("TRIGGERTYPE_LIST");
        boolean noType = List_typeList.getLength() == 0;
        if (noType) {
            logger.warning("Chain with no TRIGGERTYPE_LIST: allowed");
        }
        Element typeList_element = (Element) List_typeList.item(0);
        NodeList List_type = typeList_element.getElementsByTagName("TRIGGERTYPE");
        if (List_type.getLength() == 0) { // Test for no triggertypes

            logger.log(Level.FINER, "No TRIGGERTYPE specified in TRIGGERTYPE_LIST of chain: using counter {0}", chain.get_chain_counter());
            chain.getTriggerTypes().add(loadTriggerType(chain.get_chain_counter()));
        }
        // Sanity check - there can be only 1 TRIGGERTYPE_LIST for a chain
        if ((List_typeList.getLength() != 1) && !noType) {
            String exMessage = "Menu file corrupted: t" + List_typeList.getLength() + " TRIGGERTYPE_LISTs in file";
            throw new ConfigurationException(exMessage);
        }
        if (!noType) {
            for (int iType = 0; iType < List_type.getLength(); ++iType) {
                Element type_element = (Element) List_type.item(iType);
                types.add(loadTriggerType(type_element));
            }
        }
        return types;
    }

    private ArrayList<HLTTriggerStream> SetStreams(Element chain_element) throws SQLException, ConfigurationException {
        ArrayList<HLTTriggerStream> streams = new ArrayList<>();
        NodeList List_streamTagList = chain_element.getElementsByTagName("STREAMTAG_LIST");
        Element streamTagList_element = (Element) List_streamTagList.item(0);
        NodeList List_stream = streamTagList_element.getElementsByTagName("STREAMTAG");
        for (int iStream = 0; iStream < List_stream.getLength(); iStream++) {
            Element stream_element = (Element) List_stream.item(iStream);
            HLTTriggerStream stream = loadTriggerStream(stream_element);
            streams.add(stream);
        }

        return streams;
    }

    private HLTTriggerStream loadTriggerStream(Element stream_element) throws SQLException, ConfigurationException {
        HLTTriggerStream stream = new XMLReader_HLTTriggerStream().CreateHLTTriggerStream(stream_element);

        int id = stream.save();
        if (!loadedStreams.containsKey(id)) { //already loaded this stream
            loadedStreams.put(id, stream); // store the compactsaved stream
        }

        return stream;
    }

    private HLTTriggerType loadTriggerType(int type_bit) {
        HLTTriggerType type = new HLTTriggerType();
        type.set_typebit(type_bit);
        return type;
    }

    private HLTTriggerType loadTriggerType(Element type_element) {
        HLTTriggerType type = new HLTTriggerType();
        type.set_typebit(Integer.parseInt(type_element.getAttribute("bit")));
        return type;
    }

    private HLTTriggerGroup loadTriggerGroup(Element group_element) {
        HLTTriggerGroup group = new HLTTriggerGroup();
        group.set_name(group_element.getAttribute("name"));
        return group;
    }

    /**
     *
     * @param chain_element
     * @param List_sequences
     * @param chain
     * @return
     * @throws ConfigurationException
     * @throws NumberFormatException
     * @throws SQLException
     */
    public ArrayList<HLTTriggerSignature> GetSignatures(Element chain_element, NodeList List_sequences, HLTTriggerChain chain) throws ConfigurationException, NumberFormatException, SQLException {
        NodeList List_signatureList = chain_element.getElementsByTagName("SIGNATURE_LIST");
        boolean emptyChain = List_signatureList.getLength() == 0;
        if (emptyChain) {
            logger.warning("Chain with no SIGNATURE_LIST (empty Chain): allowed");
        } else if (List_signatureList.getLength() != 1) {
            String exMessage = "Menu file corrupted: \t" + List_signatureList.getLength() + " SIGNATURE_LISTs in file";
            throw new ConfigurationException(exMessage);
        }

        return GetSignaturesFromNode(List_signatureList, List_sequences, chain);
    }

    private ArrayList<HLTTriggerSignature> GetSignaturesFromNode(NodeList List_signatureList, NodeList List_sequences, HLTTriggerChain chain) throws ConfigurationException, NumberFormatException, SQLException {
        ArrayList<HLTTriggerSignature> signatures = new ArrayList<>();
        Element signatureList_element = (Element) List_signatureList.item(0);
        NodeList List_signatures = signatureList_element.getElementsByTagName("SIGNATURE");

        for (int i = 0; i < List_signatures.getLength(); ++i) {
            signatures.add(GetSignature((Element) List_signatures.item(i), List_sequences, chain));
        }

        return signatures;
    }

    private HLTTriggerSignature GetSignature(Element signature_element, NodeList List_sequences, HLTTriggerChain chain) throws ConfigurationException, NumberFormatException, SQLException {
        HLTTriggerSignature sig = loadSignature(signature_element, List_sequences);
        int sig_counter = 0;
        try {
            sig_counter = Integer.parseInt(signature_element.getAttribute("signature_counter"));
        } catch (NumberFormatException ex) {
            String exMessage = ex.getMessage() + "\nInvalid format of signature_counter in Chain: " + chain.get_name() + ", lower chain name: " + chain.get_lower_chain_name();
            throw new NumberFormatException(exMessage);
        }
        sig.set_signature_counter(sig_counter);

        return sig;
    }

    private HLTTriggerSignature loadSignature(Element signature_element, NodeList List_sequences) throws ConfigurationException, SQLException {
        HLTTriggerSignature new_signature = new HLTTriggerSignature();
        new_signature.set_logic(Integer.parseInt(signature_element.getAttribute("logic")));

        NodeList List_TE = signature_element.getElementsByTagName("TRIGGERELEMENT");

        boolean emptySignature = List_TE.getLength() == 0;
        if (emptySignature) {
            logger.warning("Signature with no TRIGGERELEMENTs (empty Signature): allowed");
        }

        // Sanity check
        if ((List_TE.getLength() <= 0) && !emptySignature) {
            String exMessage = "Menu file corrupted: " + List_TE.getLength() + " TRIGGERELEMENTs in SIGNATURE";
            throw new ConfigurationException(exMessage);
        }
        clearElems();
        ElementHashes.clear();
        //System.out.println("num elements " + List_TE.getLength());
        for (int iTE = 0; iTE < List_TE.getLength(); iTE++) {
            String TE_element = ((Element) List_TE.item(iTE)).getAttribute("te_name");
            //System.out.println("name " + TE_element);
            //HLTTriggerElement element = GetElement(new_signature, (Element) List_TE.item(iTE), List_sequences);
            //HLTTriggerElement element = GetElement(new_signature, TE_element, List_sequences);
            HLTTriggerElement element = GetElement(prodTEs, new_signature, TE_element, List_sequences);
            element.set_element_counter(iTE);
            //System.out.println("element counter " + element.get_element_counter());
            currentElems.add(element);
        }

        ElementHashes.add(new_signature.hashCode());
        new_signature.set_hashfull(ElementHashes);
        int signatureID = checkSignature(new_signature);
        if (signatureID < 0) {
            new_signature.save();
        } else {
            new_signature.set_id(signatureID);
        }
        return new_signature;
    }

    private int checkSignature(HLTTriggerSignature sig) throws SQLException {
        int matchingIDCheck = sig.check_hash();
        if (matchingIDCheck > 0) {
            boolean check = sig.doublecheck(matchingIDCheck, currentElems);
            if (check) {
                sig.set_id(matchingIDCheck);
                logger.log(Level.FINE, "[HASH CHECK SIGNATURE :: returned id = {0} ] {1}", new Object[]{sig.get_id(), sig.get_name()});
                return matchingIDCheck;
            }
        }
        return -1;
    }

    /**
     *
     * @param prodTEs
     * @param new_signature
     * @param TE_element
     * @param List_sequences
     * @return
     * @throws SQLException
     * @throws ConfigurationException
     */
    public HLTTriggerElement GetElement(List<String> prodTEs, HLTTriggerSignature new_signature, final String TE_element, final NodeList List_sequences) throws SQLException, ConfigurationException {

        //String TE_element = TE.getAttribute("te_name");
        logger.log(Level.FINE, "Start loadTERecursive() of trigger element {0}", TE_element);
        // first we add the TE to the list of TE's that are already produced in this chain 
        prodTEs.add(TE_element);

        // next we load this TE into the DB (including algorithms etc.)
        HLTTriggerElement element = loadTE(TE_element, List_sequences);

        logger.log(Level.FINE, "{0}", element.getInputElements());

        // last we check if the input element is either
        // a) from the previous level
        // b) part of this chain
        // c) not part of this chain
        //
        // in the case c) it is a sequence to be loaded on demand
        // and needs to be attached to the signature but with
        // te_counter -1
        for (HLTTE_TE link : element.getInputElements()) {
            final String inputEl = link.get_element_inp_id();

            // if inputEl is outputTE of previous TE, then ok 
            if (prodTEs.contains(inputEl)) {
                continue;
            }

            if (!(inputEl.startsWith("HLT")
                    || inputEl.startsWith("L2")
                    || inputEl.startsWith("EF")
                    || inputEl.startsWith("MS")
                    || inputEl.startsWith("TB")
                    || inputEl.startsWith("SA"))) {
                continue;
            }

            //if we get to here, its a floating TE - have to find the xml element in the sequence 
            logger.log(Level.FINE, "Floating input TE found: {0}", inputEl);
            HLTTriggerElement inpElement = GetElement(prodTEs, new_signature, inputEl, List_sequences);
            inpElement.set_element_counter(-1);
            currentElems.add(inpElement);
        }
        logger.log(Level.FINE, "Adding TE {0} to signature {1}", new Object[]{element.get_name(), new_signature.get_name()});
        new_signature.getElements().add(element);
        //ElementHashes.add(element.get_hash());
        return element;
    }

    private HLTTriggerElement loadTE(String TE_element, NodeList List_sequences) throws ConfigurationException, SQLException {

        ArrayList<HLTComponent> currentAlgos = new ArrayList<>();

        //many TEs used more than once - already have algos linked
        //look if name is same as name we already have - in 1 xml file will be the same!
        //need a collection of tename and db ids - search this and return!
        //
        //loading a TE now means loading all its compts
        //better to see if already loaded as part of this upload!
        //fill a map of TEnames and their ids!
        //logger.fine("Map of loaded TEs has size " + loadedTEs.size());
        if (loadedTEs.containsKey(TE_element)) {
            //return this TE
            int existingTEid = loadedTEs.get(TE_element);
            //logger.fine("PJB --- returning existing TE id " + existingTEid);
            return new HLTTriggerElement(existingTEid);
        }

        HLTTriggerElement new_element = new HLTTriggerElement();
        new_element.set_name(TE_element);

        String teInput = "";

        // the list of components (alias+name=instanceName+className)
        List<String> component_alias_v = new ArrayList<>();
        List<String> component_name_v = new ArrayList<>();

        // analyse the sequences in the trigger menu 
        int nsequences = 0;
        for (int iSeq = 0; iSeq < List_sequences.getLength(); iSeq++) {
            Element sequence_element = (Element) List_sequences.item(iSeq);
            //find the sequence
            if (sequence_element.getAttribute("output").equals(new_element.get_name())) {
                ++nsequences;
                //Check that the DB can handle the topo_start_from
                if (ConnectionManager.TopoStartFromColumnExists == 1) {
                    //Check the XML contains the monitors?
                    if (sequence_element.hasAttribute("topo_start_from")) {
                        new_element.set_topo_start_from(sequence_element.getAttribute("topo_start_from"));
                    }
                }
                getAlgorithmsfromSequence(sequence_element, component_name_v, component_alias_v);
                teInput = sequence_element.getAttribute("input");
            }
        }

        if (nsequences != 1) {
            String exMessage = nsequences + " sequences with output: " + new_element.get_name() + " found";
            throw new ConfigurationException(exMessage);
        }
        if (component_alias_v.size() != component_name_v.size()) {
            String exMessage = "Different number of algorithm names " + component_name_v.size() + " and aliases " + component_alias_v.size();
            throw new ConfigurationException(exMessage);
        }

        String teInputsDummy[] = teInput.split("\\s+");

        ArrayList<HLTTE_TE> tete = new ArrayList<>();
        for (int itein = 0; itein < teInputsDummy.length; itein++) {
            // Formating for Oracle
            //put tildas in mysql as well
            if (teInputsDummy[itein].isEmpty()) {
                //if (oracle_db && teInputsDummy[itein].equals("")) {
                teInputsDummy[itein] = "~";
            }

            //System.out.println("PJB making TETE: " + teInputsDummy[itein]);
            HLTTE_TE link = new HLTTE_TE();
            link.set_element_inp_type("~");
            link.set_element_inp_id(teInputsDummy[itein]);
            link.set_element_counter(itein);
            tete.add(link);
        }
        new_element.setInputElements(tete);

        // Check for algorithms
        //PJB - all re written for new schema 17/07/08
        //load all the algo components and their private tools here
        //we have the component name and alias, need to look for these in the setup file!
        //loop over the algos
        for (int algo_counter = 0; algo_counter < component_alias_v.size(); algo_counter++) {
            String this_comp_alias = component_alias_v.get(algo_counter);
            String this_comp_name = component_name_v.get(algo_counter);
            //won't be loaded already - need to look in xml setup and load compts

            logger.log(Level.FINE, "{0} {1}", new Object[]{this_comp_alias, this_comp_name});

            HLTComponent algocomp = loadMenuComponent(this_comp_name, this_comp_alias, false);
            currentComps.add(algocomp);
            currentAlgos.add(algocomp);
            if (algocomp == null) {
                String message = "Missing algorithm in setup for TE " + new_element.get_name() + ":";
                throw new ConfigurationException(message);
            } else {
                algocomp.set_algorithm_counter(algo_counter);
                new_element.getAlgorithms().add(algocomp);
            }
        }

        new_element.set_hash();
        int newTEid = checkElement(new_element, currentAlgos);
        if (newTEid < 0) {
            newTEid = new_element.save();
        } else {
            new_element.set_id(newTEid);
        }
        //currentElems.add(new_element);

        loadedTEs.put(TE_element, newTEid);

        return new_element;
    }

    private int checkElement(HLTTriggerElement elem, ArrayList<HLTComponent> currentAlgos) throws SQLException {
        ArrayList<Integer> matchingIDCheck = elem.check_hash();
        int matchedID = -1;
 	Boolean check = false;
 	if (!matchingIDCheck.isEmpty()) {
            for(Integer checkID : matchingIDCheck){
                if(elem.doublecheck(checkID, currentAlgos,elem)){
                    check = true;
                    matchedID = checkID;
                }
            }
 	}
 	
        if (check){
            elem.set_id(matchedID);
            return matchedID;
 	}
        return -1;
    }

    /**
     *
     * @param sequence_element
     * @param algNames
     * @param algAliases
     * @throws ConfigurationException
     */
    public void getAlgorithmsfromSequence(Element sequence_element, List<String> algNames, List<String> algAliases) throws ConfigurationException {
        if (algNames == null) {
            algNames = new ArrayList<>();
        }

        if (algAliases == null) {
            algAliases = new ArrayList<>();
        }

        String algo = sequence_element.getAttribute("algorithm");
        // logger.info("-------------algorithms: " + algo);

        //PJB may be a list of algos: split at whitespace
        String algos[] = algo.split("\\s+");

        int numalgos = algos.length;
        // logger.info("-------------num algorithms: " + numalgos);

        for (int i = 0; i < numalgos; i++) {
            //split each part
            String namealias[] = algos[i].split("\\/");
            // Formating for Oracle DB
            //for mysql as well
            if (namealias[0].isEmpty()) {
                //if (oracle_db && namealias[0].equals("")) {
                namealias[0] = "~";
            }

            switch (namealias.length) {
                case 1:
                    algNames.add(namealias[0]);
                    algAliases.add(namealias[0]);
                    break;
                case 2: // example: PESA::dummyAlgo/Muon

                    algNames.add(namealias[0]);
                    // Formating for Oracle DB
                    //sql as well
                    if (namealias[1].isEmpty()) {
                        //if (oracle_db && namealias[1].equals("")) {
                        namealias[1] = "~";
                    }

                    algAliases.add(namealias[1]);
                    break;
                case 3: // example: PESA::dummyAlgo/EgammaAdv/g10

                    algNames.add(namealias[0]);
                    // Formating for Oracle DB
                    //mysql as well
                    if (namealias[1].isEmpty() && namealias[2].isEmpty()) {
                        //if (oracle_db && namealias[1].equals("") && namealias[2].equals("")) {
                        algAliases.add("~");
                    } else {
                        algAliases.add(namealias[1] + "_" + namealias[2]);
                    }

                    break;
                default:
                    String exMessage = "Unsupported format of algorithm: " + algos[i];
                    throw new ConfigurationException(exMessage);
            }
            // logger.info("-------------name alias: " + algNames.lastElement() + " " + algAliases.lastElement());

            //setup a map of comp name to counter
            //         compname_algcounter_map.put(algNames.lastElement(), i);
        }

        if (algAliases.size() != algNames.size()) {
            String exMessage = "Different number of algorithm names " + algNames.size() + " and aliases " + algAliases.size();
            throw new ConfigurationException(exMessage);
        }
    }

    private HLTComponent loadMenuComponent(final String name, final String alias, boolean child) throws SQLException {
        logger.log(Level.FINE, "loadMenuComponent: {0}/{1}\t", new Object[]{name, alias});

        // First see if this component has already been loaded
        String test = name + "_+_" + alias;
        if (loadedComps.containsKey(test)) { // already loaded this component
            logger.log(Level.FINE, "Already loaded {0}, so don't need to continue", test);
            return loadedComps.get(test); //return the already loaded component
        }

        // Component not in DB, creating a new one.
        HLTComponent comp;
        if (!dummySetup) {
            boolean foundalgo = false;
            Element component_element = null;
            if (hlt_setup_element != null) {

                NodeList List_component = hlt_setup_element.getElementsByTagName("component");

                //load M-setup components
                for (int iComp = 0; iComp < List_component.getLength(); ++iComp) {

                    component_element = (Element) List_component.item(iComp);

                    //look for the name/alias
                    if (component_element.getAttribute("name").equals(name)
                            && component_element.getAttribute("alias").equals(alias)) {

                        foundalgo = true;
                        break;
                    }
                }
            }

            if (!foundalgo) {
                //we did not find the algo in the setup!
                logger.log(Level.SEVERE, "Algorithm {0} not found in setup file!", alias);
                return null;
            }

            int matching_Id;
            clearContainers();
            comp = loadInfraStructureComponentHash(component_element, false, true, false);
            if (comp != null) {
                matching_Id = comp.check_hashfull();
                boolean checking = false;
                if (matching_Id > 0) {
                    checking = comp.doublecheck(matching_Id, currentComps, currentParas);
                    logger.log(Level.FINE, "Double check returns {0} for {1} with id {2}", new Object[]{checking, comp.toString(), matching_Id});
                    if (checking) {
                        comp.set_id(matching_Id);
                        logger.log(Level.FINE, "[HASH CHECK COMPONENT :: returned id = {0} ] {1}", new Object[]{comp.get_id(), comp.get_name()});
                    }
                    //else{
                                            //System.out.println("Double check returns false for " + comp.toString() + " with id " + matching_Id);

                    //}
                } else {
                    clearContainers();
                    comp = loadInfraStructureComponentHash(component_element, false, true, true);
                }
            } else {
                clearContainers();
                comp = loadInfraStructureComponentHash(component_element, false, true, true);
            }
            loadedComps.put(test, comp);
        } else {
            // if menu only, don't load algo properties
            comp = new HLTComponent();
            comp.set_name(name);
            comp.set_alias(alias);

            comp.set_hash();
            ArrayList<Integer> Hashes = new ArrayList<>();
            Hashes.add(comp.get_hash());
            comp.set_hashfull(Hashes);
            comp.compactsave();

            loadedComps.put(test, comp);

        }

        return comp;
    }

    private int checkInfraStructureComponentHash(Element component_element) throws SQLException {
        int matching_Id = -1;
        clearContainers();
        HLTComponent comp = loadInfraStructureComponentHash(component_element, false, false, false);
        //logger.info("How long are they? Comps " +currentComps.size()+" and Paras "+currentParas.size());
        if (comp != null) {
            matching_Id = comp.check_hashfull();
            //System.out.println("match? " + matching_Id);
            if (matching_Id > 0) {
                boolean check = comp.doublecheck(matching_Id, currentComps, currentParas);
                if (check) {
                    logger.log(Level.FINER, "[HASH CHECK COMPONENT :: returned id = {0} ] {1}", new Object[]{matching_Id, comp.get_name()});
                } else {
                    matching_Id = -1;
                    //System.out.println("False!");
                }
            }
        }
        return matching_Id;
    }

    private HLTComponent loadInfraStructureComponentHash(Element component_element, Boolean child, Boolean Menu, Boolean save) throws SQLException {
        HLTComponent new_component = new HLTComponent();

        logger.log(Level.FINE, "loadInfraStructureComponent for: {0}/{1}", new Object[]{component_element.getAttribute("name"), component_element.getAttribute("alias")});
        //logger.info("We have " +component_element.getAttribute("name") +" which is a child? "+child+" which we are saving? "+save);
        //check its not an algorithm
//        if (component_element.getAttribute("steeralg").equals("1") && !Menu) {
//            logger.fine("loadInfraStructureComponent: this is a steeralg so skipping");
//            //NB PJB removes support for strange TrigMoore subalgs which needed to be uploaded
//            //here in the past. TrigMoore replaced by TrigMuonEF now. See email from Brian 14.08.08
//            return null;
//        }
//
//        //only if its a child can it be a priv tool (and indeed it has to be)
//        if (child == false && component_element.getAttribute("privT").equals("1")) {
//            logger.fine("loadInfraStructureComponent: this is a privatetool but not a child, so skipping");
//            return null;
//        }
//
//        //if its a toolsvc. component it may appear as a child of the toolsvc, which we don't
//        //really want. We'll already have it as a non-parent, so flatten this out, i.e. dont set link
//        if (child == true
//                && component_element.getAttribute("alias").startsWith("ToolSvc.")
//                && component_element.getAttribute("alias").split("\\.").length <= 2) {
//            logger.fine("loadInfraStructureComponent: not setting component as a child");
//            return null;
//        }

        //set the type according to the xml selection:
        //e.g. topalg="0" alg="0" steeralg="0" svc="1" pubT="0" privT="0" aud="0
        //must be an infrastructure type:
        if (component_element.getAttribute("topalg").equals("1")) {
            new_component.set_type("TopAlg");
        }
        if (component_element.getAttribute("svc").equals("1")) {
            new_component.set_type("Service");
        }
        if (component_element.getAttribute("pubT").equals("1")) {
            new_component.set_type("PublicTool");
        }
        if (component_element.getAttribute("privT").equals("1")) {
            new_component.set_type("PrivateTool");
        }
        if (component_element.getAttribute("aud").equals("1")) {
            new_component.set_type("Auditor");
        }
        if (component_element.getAttribute("steeralg").equals("1")) {
            new_component.set_type("steeralg");
        }
        //fix if type not set for toolsvc. tools in xml - shouldn't be needed
        if (component_element.getAttribute("alias").startsWith("ToolSvc.")
                && component_element.getAttribute("pubT").equals("0")
                && component_element.getAttribute("alias").split("\\.").length <= 2) {
            logger.log(Level.FINE, "loadInfraStructureComponent: ToolSvc component has no type in xml, setting to pubT for {0}", component_element.getAttribute("alias"));
            new_component.set_type("PublicTool");
        }

        //set name and alias
        new_component.set_name(component_element.getAttribute("name"));
        new_component.set_alias(component_element.getAttribute("alias"));

        // set python package and name
        new_component.set_py_name("");
        new_component.set_py_package("");
        if (component_element.hasAttribute("py_name")) {
            String pyName = component_element.getAttribute("py_name");
            if(pyName.isEmpty()){
                pyName = "~";
            }
            new_component.set_py_name(pyName);
        }
        if (component_element.hasAttribute("py_package")) {
            String pyPackage = component_element.getAttribute("py_package");
            if(pyPackage.isEmpty()){
                pyPackage = "~";
            }
            new_component.set_py_package(pyPackage);
        }

        // Flag for exceptions
        boolean loadChildren = true;
        // Exception for ToolSvc
        if (component_element.getAttribute("alias").equals("ToolSvc")) {
            loadChildren = false;
        }

        //Get all nodes - may be parameters, may be components and loop 
        NodeList nodes = component_element.getChildNodes();
        ArrayList<Integer> Hashes = new ArrayList<>();
        for (int iPC = 0; iPC < nodes.getLength(); ++iPC) {

            Node para_or_comp = nodes.item(iPC);
            //if its a parameter, load the parameter
            if (para_or_comp.getNodeName().equals("parameter")) {
                Element parameter_element = (Element) para_or_comp;
                HLTParameter param;
                if (save) {
                    param = loadParameter(parameter_element);
                    if (param.get_value().length() > 4000) {
                        String msg = "Parameter " + param.get_name();
                        msg += " has to be split, more than 4000 characters long: ";
                        msg += param.get_value().length();
                        logger.fine(msg);
                        for (HLTParameter parShort : param.split()) {
                            parShort.save();
                        }
                    }
                } else {
                    param = loadParameterNoSave(parameter_element);
                }
                new_component.getParameters().add(param);
                currentParas.add(param);
                //logger.info("Parameter "+param.get_name() + " has the value "+param.get_value());
                //logger.info("Parameter "+param.get_name() + " has the hash "+param.get_hash());
                Hashes.add(param.get_hash());
            }

            //if its a comp, call the method again
            if (loadChildren && para_or_comp.getNodeName().equals("component")) {
                Element childcomponent_element = (Element) para_or_comp;
                HLTComponent childcomp;
                childcomp = loadInfraStructureComponentHash(childcomponent_element, true, Menu, save);

                if (childcomp != null) {
                    new_component.getChildComponents().add(childcomp);
                    Hashes.add(childcomp.get_hashfull());
                }
            }
        }

        new_component.set_hash();
        //logger.info("Component "+new_component.get_name() + " has the hash "+new_component.get_hash());
        Hashes.add(new_component.get_hash());
        new_component.set_hashfull(Hashes);
        //System.out.println("New Comp " + new_component);
        currentComps.add(new_component);
        //logger.info("Component "+new_component.get_name() + " has the hashfull "+new_component.get_hashfull());
        if (save) {
            new_component.compactsave();
        }
        return new_component;
    }

    private HLTParameter loadParameterNoSave(Element parameter_element) {
        HLTParameter new_param = new HLTParameter();
        new_param.set_name(parameter_element.getAttribute("name"));
        new_param.set_value(parameter_element.getAttribute("value"));
        new_param.set_op(parameter_element.getAttribute("op"));
        //xml file should mark those parameters which are important:
        new_param.set_chain_user_version(false);
        new_param.set_hash();

        return new_param;
    }

}
