/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggertool.XML.Readers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.w3c.dom.Element;
import triggerdb.Entities.HLT.HLTPrescale;
import triggerdb.Entities.HLT.HLTPrescaleType;
import triggerdb.Entities.HLT.HLTTriggerChain;

/**
 *
 * @author Michele
 */
public class XMLReader_HLTChain {

    private HLTTriggerChain chain;
    private int chain_counter;
    private String prescale_rate;
    private String pass_through_rate;
    private String rerun_rate;

    /**
     *
     * @param element
     * @return
     */
    public ArrayList<HLTPrescale> CreatePrescales(Element element) {
        GetInfoFromXML(element);

        ArrayList<HLTPrescale> list = new ArrayList<>();

        list.add(CreatePrescale(HLTPrescaleType.Prescale, prescale_rate, ""));
        list.add(CreatePrescale(HLTPrescaleType.Pass_Through, pass_through_rate, ""));
        //Default rerun condition to zero
        list.add(CreatePrescale(HLTPrescaleType.ReRun, rerun_rate, "1"));

        return list;
    }

    private void GetInfoFromXML(Element element) throws NumberFormatException {
        chain_counter = Integer.parseInt(element.getAttribute("chain_counter"));
        prescale_rate = element.getAttribute("prescale");
        pass_through_rate = element.getAttribute("pass_through");
        rerun_rate = element.getAttribute("rerun_prescale");
    }

    private HLTPrescale CreatePrescale(HLTPrescaleType type, String prescale_rate, String condition) throws NumberFormatException {
        HLTPrescale p = new HLTPrescale();
        p.set_chain_counter(chain_counter);
        p.set_type(type);
        p.set_value(Double.valueOf(prescale_rate));
        p.set_condition(condition);
        return p;
    }

    /**
     *
     * @param chain_element
     * @return
     * @throws NumberFormatException
     */
    public HLTTriggerChain CreateBaseChain(Element chain_element) throws NumberFormatException {
        chain = new HLTTriggerChain();
        chain.set_user_version(1);
        chain.set_name(chain_element.getAttribute("chain_name"));
        chain.set_eb_point(GetEbStep(chain_element));
        chain.set_lower_chain_name(loadLowerChains(chain_element));
        chain.set_chain_counter(GetChainCounter(chain_element));

        return chain;
    }

    private int GetEbStep(Element chain_element) throws NumberFormatException {
        int EBstep;
        if (chain_element.hasAttribute("EBstep")) {
            EBstep = Integer.parseInt(chain_element.getAttribute("EBstep"));
        } else {
            EBstep = 01;
        }
        return EBstep;
    }

    private String loadLowerChains(Element chain_element) {
        String lower_chains_list = chain_element.getAttribute("lower_chain_name").trim();
        List<String> lower_chains_v = Arrays.asList(lower_chains_list.split(","));

        //sorted list
        Collections.sort(lower_chains_v);

        //sorted string returned
        String lower_chains_ordered = "";

        Iterator<String> lcIter = lower_chains_v.iterator();
        while (lcIter.hasNext()) {
            lower_chains_ordered += lcIter.next();
            if (lcIter.hasNext()) {
                lower_chains_ordered += ",";
            }
        }

        return lower_chains_ordered;
    }

    private int GetChainCounter(Element chain_element) throws NumberFormatException {
        int counter;
        try {
            counter = Integer.parseInt(chain_element.getAttribute("chain_counter"));
        } catch (NumberFormatException ex) {
            String exMessage = ex.getMessage() + "\nInvalid format of chain_counter in Chain: " + chain.get_name() + ", lower chain name: " + chain.get_lower_chain_name();
            throw new NumberFormatException(exMessage);
        }
        return counter;
    }
}
