/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.XML.Readers;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import triggerdb.Entities.L1.L1Prescale;
import triggertool.L1Records.CutToValueConverter;

/**
 *
 * @author Michele
 */
public class XMLReader_L1Prescale {

    // Message Log.

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");

    private L1Prescale prescaleSet;
        
    /**
     *
     * @param document
     * @return
     */
    public L1Prescale CreateL1Prescale(Document document){
        prescaleSet = new L1Prescale();
        ReadInfoOfPrescaleSet(document);
        return prescaleSet;
    }

    private void ReadInfoOfPrescaleSet(Document document) {
        NodeList sections = document.getElementsByTagName("PrescaleSet");
        if (sections.getLength() == 1) {
            ReadInfoInPrescaleSet(sections, document);
        } else {
            logger.warning("TOO MANY PRESCALESET NODES IN XML");
        }
    }

    private void ReadInfoInPrescaleSet(NodeList sections, Document document) throws NumberFormatException {
        Element section = (Element) sections.item(0);       
        ArrayList<Integer> ctpids_v = GetListOfTriggerMenuIds(document);
        ArrayList<String> prescaleArray = CreatePrescaleList(section, ctpids_v);
        
        prescaleSet.set_name(section.getAttribute("name"));
        prescaleSet.set_type(section.getAttribute("type"));       
	prescaleSet.set_partition(Integer.parseInt(section.getAttribute("menuPartition")));
        for (int i = 0; i < L1Prescale.LENGTH; i++) {
            prescaleSet.set_val(i + 1, CutToValueConverter.getIntegerFromHexString(prescaleArray.get(i)));
        }
    }

    private ArrayList<Integer> GetListOfTriggerMenuIds(Document document) throws NumberFormatException {
        ArrayList<Integer> ctpids_v = new ArrayList<>();
        Element menu = (Element) document.getElementsByTagName("TriggerMenu").item(0);
        NodeList listItems = menu.getElementsByTagName("TriggerItem");
        for (int i = 0; i < listItems.getLength(); ++i) {
            Element itemNode = (Element) listItems.item(i);
            ctpids_v.add(Integer.parseInt(itemNode.getAttribute("ctpid")));
        }
        return ctpids_v;
    }

    private ArrayList<String> CreatePrescaleList(Element section, ArrayList<Integer> ctpids_v) throws NumberFormatException {
        Map<Integer, String> prescaleArray = new TreeMap<>();
        NodeList prescales = section.getElementsByTagName("Prescale");
        for (int i = 0; i < prescales.getLength(); i++) {
            SetPrescaleValueFromElement((Element) prescales.item(i), ctpids_v, prescaleArray);
        }
        AddNegativeValueInUnfilledColumns(prescaleArray);
        
        return new ArrayList<>(prescaleArray.values());
    }

    private void SetPrescaleValueFromElement(Element prescale, ArrayList<Integer> ctpids_v, Map<Integer, String> prescaleArray) throws NumberFormatException {
        // only those with items
        if (ctpids_v.contains(Integer.parseInt(prescale.getAttribute("ctpid").trim()))) {
            SetPrescaleForExistingItems(prescale, prescaleArray);
        } else {
            prescaleArray.put(Integer.parseInt(prescale.getAttribute("ctpid").trim()), "-1");
        }
    }

    private void AddNegativeValueInUnfilledColumns(Map<Integer, String> prescaleArray) {
        for (int i = 0; i < L1Prescale.LENGTH; i++) {
            if (!prescaleArray.containsKey(i))
            prescaleArray.put(i, "-1");
        }
    }

    private void SetPrescaleForExistingItems(Element prescale, Map<Integer, String> prescaleArray) throws NumberFormatException {
        Integer idx = Integer.parseInt(prescale.getAttribute("ctpid").trim());
        String cut = prescale.getAttribute("cut").trim();
        prescaleArray.put(idx, cut);
    }
}
