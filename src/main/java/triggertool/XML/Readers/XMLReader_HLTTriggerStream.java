/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.XML.Readers;

import org.w3c.dom.Element;
import triggerdb.Entities.HLT.HLTPrescale;
import triggerdb.Entities.HLT.HLTPrescaleType;
import triggerdb.Entities.HLT.HLTTriggerStream;
import triggertool.XML.ConfigurationException;

/**
 *
 * @author Michele
 */
public class XMLReader_HLTTriggerStream {
    
    /**
     *
     * @param stream_element
     * @return
     * @throws ConfigurationException
     */
    public HLTTriggerStream CreateHLTTriggerStream(Element stream_element) throws ConfigurationException {
        HLTTriggerStream stream = new HLTTriggerStream();
        stream.set_name(stream_element.getAttribute("stream"));
        stream.set_type(stream_element.getAttribute("type"));
        stream.set_obeyLB(ParseObeyLBAttribute(stream_element));
        stream.set_stream_prescale(SetPrescaleInStream(stream_element));
        stream.set_description(""); 
        return stream;
    }
      
    private Integer ParseObeyLBAttribute(Element stream_element) throws ConfigurationException {
        String stream_obeyLBstr = stream_element.getAttribute("obeyLB");
        //PJB in the DB this is an integer, maybe a string or integer in xml?
        Integer stream_obeyLB = -1;
        if (stream_obeyLBstr.toLowerCase().equals("yes") || stream_obeyLBstr.toLowerCase().equals("1")) {
            stream_obeyLB = 1;
        }
        if (stream_obeyLBstr.toLowerCase().equals("no") || stream_obeyLBstr.toLowerCase().equals("0")) {
            stream_obeyLB = 0;
        }
        if (stream_obeyLB < 0) { // Might set a default value instead and send the warning - What is the default value?
            String exMessage = "Menu file corrupted:\t obeyLB = " + stream_obeyLBstr + " - unknown format (accepts yes/no)";
            throw new ConfigurationException(exMessage);
        }
        return stream_obeyLB;
    }
 
    private String SetPrescaleInStream(Element stream_element) {
        //if (!stream_element.getAttribute("stream").equals("express")){
            return stream_element.getAttribute("prescale");
        //} 
        
        //return "~";
    }
   
    /**
     *
     * @param element
     * @param chain_counter
     * @return
     */
    public static HLTPrescale CreatePrescales(Element element, int chain_counter) {
        HLTPrescale p = new HLTPrescale();
        p.set_chain_counter(chain_counter);
        p.set_type(HLTPrescaleType.Stream);
        p.set_value(Double.valueOf(element.getAttribute("prescale")));
        p.set_condition(element.getAttribute("stream"));
        
        return p;
    }   
}
