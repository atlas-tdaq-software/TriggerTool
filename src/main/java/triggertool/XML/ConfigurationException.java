/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package triggertool.XML;

/**
 *
 * @author stelzer
 */
public class ConfigurationException extends Exception {

    /**
     *
     * @param what
     */
    public ConfigurationException(String what) {
        super("ConfigurationException: " + what);
    }
    
}
