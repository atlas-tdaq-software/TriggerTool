/*
 * DiffObjectSel.java
 *
 * Created on February 23, 2009, 2:14 PM
 */

package triggertool.Diff;

import java.awt.Frame;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;
import javax.swing.JDialog;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.HLT.HLTComponent;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTParameter;
import triggerdb.Entities.HLT.HLTPrescale;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.HLT.HLTSetup;
import triggerdb.Entities.HLT.HLTTriggerChain;
import triggerdb.Entities.HLT.HLTTriggerElement;
import triggerdb.Entities.HLT.HLTTriggerGroup;
import triggerdb.Entities.HLT.HLTTriggerMenu;
import triggerdb.Entities.HLT.HLTTriggerSignature;
import triggerdb.Entities.HLT.HLTTriggerStream;
import triggerdb.Entities.HLT.HLTTriggerType;
import triggerdb.Entities.L1.L1BunchGroup;
import triggerdb.Entities.L1.L1BunchGroupSet;
import triggerdb.Entities.L1.L1CaloInfo;
import triggerdb.Entities.L1.L1CaloIsoParam;
import triggerdb.Entities.L1.L1CaloIsolation;
import triggerdb.Entities.L1.L1CaloMinTob;
import triggerdb.Entities.L1.L1CaloSinCos;
import triggerdb.Entities.L1.L1CtpFiles;
import triggerdb.Entities.L1.L1CtpSmx;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.L1.L1Menu;
import triggerdb.Entities.L1.L1MuctpiInfo;
import triggerdb.Entities.L1.L1MuonThresholdSet;
import triggerdb.Entities.L1.L1Prescale;
import triggerdb.Entities.L1.L1PrescaledClock;
import triggerdb.Entities.L1.L1Random;
import triggerdb.Entities.L1.L1Threshold;
import triggerdb.Entities.L1.L1ThresholdValue;
import triggerdb.Entities.Topo.TopoAlgo;
import triggerdb.Entities.Topo.TopoAlgoInput;
import triggerdb.Entities.Topo.TopoAlgoOutput;
import triggerdb.Entities.Topo.TopoConfig;
import triggerdb.Entities.Topo.TopoGeneric;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.Entities.Topo.TopoMenu;
import triggerdb.Entities.Topo.TopoOutputLine;
import triggerdb.Entities.Topo.TopoOutputList;
import triggerdb.Entities.Topo.TopoParameter;
import triggertool.Components.ResultsTableModel;

/**
 * Dialog for selecting two objects to diff.
 * @author  markowen
 */
public class DiffObjectSel extends JDialog {

    /** Creates new form DiffObjectSel
     * @param parent
     * @param modal */
    public DiffObjectSel(Frame parent) {
        super((JDialog)null);
        initComponents();
        setupComboBox();
        setSize(600,600);
        searchField.setToolTipText("Use % for wildcards in searches");
        setLocationRelativeTo(parent);
        setVisible(true);
    }

    /** Table model to display the search results */
    private ResultsTableModel resultsModel = new ResultsTableModel();
    private ResultsTableModel resultsModel2 = new ResultsTableModel();
    
    // Table under study

    /**
     *
     */
    protected AbstractTable table = null;
    // query associated with that table

    /**
     *
     */
    protected String query = "";
    // results window
    //DiffObject results= new DiffObject(null,false)
    
    ///Message Log.

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    
    /**
     * Function to set up the combo box containing the object types
     */
    private void setupComboBox() {
 
            //comboTable.removeAllItems();
            comboTable.addItem("L1PrescaleSet");
            comboTable.addItem("HLTPrescaleSet");
            comboTable.addItem("HLTRelease");
            
            comboTable.addItem("L1Master");
            comboTable.addItem("L1BunchGroupSet");
            comboTable.addItem("L1MuonThresholdSet");
            comboTable.addItem("L1BunchGroup");
        
            comboTable.addItem("L1Menu");
            comboTable.addItem("L1Item");
            comboTable.addItem("L1Threshold");
            comboTable.addItem("L1ThresholdValue");
        
            comboTable.addItem("L1CaloInfo");
            comboTable.addItem("L1CaloSinCos");
            comboTable.addItem("L1CaloMinTob");                 
            comboTable.addItem("L1CaloIsolation");                   
            comboTable.addItem("L1CaloIsoParam");   

            comboTable.addItem("L1CTPFiles");
            comboTable.addItem("L1CTPSMX");
            comboTable.addItem("L1Random");
            comboTable.addItem("L1MuctpiInfo");
            comboTable.addItem("L1PrescaledClock");
        
            comboTable.addItem("TopoMaster");
            comboTable.addItem("TopoAlgoInput");  
            comboTable.addItem("TopoAlgo");  
            comboTable.addItem("TopoAlgoOutput");  
            comboTable.addItem("TopoConfig");  
            comboTable.addItem("TopoGeneric");   
            comboTable.addItem("TopoMenu");  
            comboTable.addItem("TopoOutputLine");  
            comboTable.addItem("TopoOutputList");  
            comboTable.addItem("TopoParameter");
            
            comboTable.addItem("HLTMaster");
        
            comboTable.addItem("HLTTriggerMenu");
            comboTable.addItem("HLTTriggerChain");
            comboTable.addItem("HLTPrescale");
            comboTable.addItem("HLTTriggerSignature");
            comboTable.addItem("HLTTriggerElement");
            comboTable.addItem("HLTTriggerGroup");
            comboTable.addItem("HLTTriggerStream");
            comboTable.addItem("HLTTriggerType");
    
            comboTable.addItem("HLTSetup");
            comboTable.addItem("HLTComponent");
            comboTable.addItem("HLTParameter");
                        
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        diffButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        comboTable = new javax.swing.JComboBox<>();
        searchField = new javax.swing.JTextField();
        searchButton = new javax.swing.JButton();
        comboFieldSelect = new triggertool.Components.FieldComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        searchField1 = new javax.swing.JTextField();
        searchButton1 = new javax.swing.JButton();
        bothPanelCheckBox = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(400, 600));

        diffButton.setText("Diff");
        diffButton.setEnabled(false);
        diffButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                diffButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        comboTable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboTableActionPerformed(evt);
            }
        });

        searchField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchFieldActionPerformed(evt);
            }
        });

        searchButton.setText("Search");
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt);
            }
        });

        jTable1.setModel(resultsModel);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jTable2.setModel(resultsModel2);
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable2MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTable2);

        searchField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchField1ActionPerformed(evt);
            }
        });

        searchButton1.setText("Search");
        searchButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButton1ActionPerformed(evt);
            }
        });

        bothPanelCheckBox.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        bothPanelCheckBox.setSelected(true);
        bothPanelCheckBox.setText("Use this search for both panels");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(bothPanelCheckBox)
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(cancelButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(diffButton))
                    .add(comboTable, 0, 361, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(comboFieldSelect, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(searchField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(searchButton))
                    .add(jScrollPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(searchField1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 281, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(searchButton1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(comboTable, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(searchButton)
                    .add(searchField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(comboFieldSelect, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(bothPanelCheckBox)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(searchButton1)
                    .add(searchField1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jScrollPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
                .add(14, 14, 14)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(cancelButton)
                    .add(diffButton))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void diffButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_diffButtonActionPerformed
    int r1 = jTable1.getSelectedRow();
    int r2 = jTable2.getSelectedRow();
    
    AbstractTable t1 = resultsModel.get(r1);
    AbstractTable t2 = resultsModel2.get(r2);
    if(t1.get_id() == t2.get_id()) { // don't diff the same item
        javax.swing.JOptionPane.showMessageDialog(null, "You selected the same component twice, please retry");
        return;
    }
    
    //results.diffObjects(t1, t2);
    DiffDisplayDialog res = new DiffDisplayDialog(null,false,t1,t2);
    res.setVisible(true);
    this.dispose();
}//GEN-LAST:event_diffButtonActionPerformed

private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
    //results.dispose();
    dispose();
}//GEN-LAST:event_cancelButtonActionPerformed

private void comboTableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboTableActionPerformed
        resultsModel.clear();
        resultsModel2.clear();
        
        if(comboTable.getSelectedItem().equals("L1Master")){
            table = new L1Master();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("L1PrescaleSet")){
            table = new L1Prescale();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("L1MuonThresholdSet")){
            table = new L1MuonThresholdSet();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("L1BunchGroupSet")){
            table = new L1BunchGroupSet();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("L1BunchGroup")){
            table = new L1BunchGroup();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("L1Menu")){
            table = new L1Menu();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("L1Item")){
            table = new L1Item();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("L1Threshold")){
            table = new L1Threshold();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("L1ThresholdValue")){
            table = new L1ThresholdValue();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("L1CaloInfo")){
            table = new L1CaloInfo();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("L1CaloSinCos")){
            table = new L1CaloSinCos();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1CaloMinTob")) {
            table = new L1CaloMinTob();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1CaloIsolation")) {
            table = new L1CaloIsolation();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("L1CaloIsoParam")) {
            table = new L1CaloIsoParam();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("L1CTPFiles")){
            table = new L1CtpFiles();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("L1CTPSMX")){
            table = new L1CtpSmx();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("L1Random")){
            table = new L1Random();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("L1MuctpiInfo")){
            table = new L1MuctpiInfo();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("L1PrescaledClock")){
            table = new L1PrescaledClock();
            query = table.getQueryString();
        }
        ///////////////////////////////////////////////////////////        
        if (comboTable.getSelectedItem().equals("TopoMaster")) {
            table = new TopoMaster();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("TopoAlgoInput")) {
            table = new TopoAlgoInput();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("TopoAlgo")) {
            table = new TopoAlgo();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("TopoAlgoOutput")) {
            table = new TopoAlgoOutput();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("TopoConfig")) {
            table = new TopoConfig();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("TopoGeneric")) {
            table = new TopoGeneric();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("TopoMenu")) {
            table = new TopoMenu();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("TopoOutputLine")) {
            table = new TopoOutputLine();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("TopoOutputList")) {
            table = new TopoOutputList();
            query = table.getQueryString();
        }
        if (comboTable.getSelectedItem().equals("TopoParameter")) {
            table = new TopoParameter();
            query = table.getQueryString();
        }
        ///////////////////////////////////////////////////////////
        if(comboTable.getSelectedItem().equals("HLTMaster")){
            table = new HLTMaster();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("HLTPrescale")){
            table = new HLTPrescale();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("HLTPrescaleSet")){
            table = new HLTPrescaleSet();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("HLTTriggerMenu")){
            table = new HLTTriggerMenu();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("HLTTriggerChain")){
            table = new HLTTriggerChain();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("HLTTriggerSignature")){
            table = new HLTTriggerSignature();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("HLTTriggerElement")){
            table = new HLTTriggerElement();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("HLTTriggerGroup")){
            table = new HLTTriggerGroup();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("HLTTriggerStream")){
            table = new HLTTriggerStream();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("HLTTriggerType")){
            table = new HLTTriggerType();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("HLTRelease")){
            table = new HLTRelease();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("HLTSetup")){
            table = new HLTSetup();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("HLTComponent")){
            table = new HLTComponent();
            query = table.getQueryString();
        }
        if(comboTable.getSelectedItem().equals("HLTParameter")){
            table = new HLTParameter();
            query = table.getQueryString();
        }
        
        comboFieldSelect.removeAllItems();
        comboFieldSelect.getFields(table);
}//GEN-LAST:event_comboTableActionPerformed

private void searchFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchFieldActionPerformed
    searchButton.doClick();
}//GEN-LAST:event_searchFieldActionPerformed

private void searchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchButtonActionPerformed
    try {
            resultsModel.clear();
            if(bothPanelCheckBox.isSelected()) resultsModel2.clear();
            String finalquery = ConnectionManager.getInstance().fix_schema_name(query);

            String search = null;
            if (comboFieldSelect.getSelectedIndex() != 0) {
                finalquery += " WHERE " + table.tab_prefix() + comboFieldSelect.get_db_name() + " LIKE ? ";
                search = searchField.getText();
            }
            finalquery += " ORDER BY " + table.tablePrefix + "ID DESC";
                        
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(finalquery);

            ////System.out.println("Query = " + finalquery);
            
            if (search != null) {
                ps.setString(1, search);
            }

            ////System.out.println(ps.toString());
            
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {
                AbstractTable copy = (AbstractTable) table.clone();
                copy.loadFromRset(rset);
                resultsModel.add(copy); 
                if(bothPanelCheckBox.isSelected()) resultsModel2.add(copy);
            }

            rset.close();
            ps.close();
        } catch (SQLException ex) {
            logger.severe("Caught exception while searching");
            logger.severe(ex.getMessage());
        }
}//GEN-LAST:event_searchButtonActionPerformed

private void searchField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchField1ActionPerformed
    searchButton1.doClick();
    setDiffButtonStatus();
}//GEN-LAST:event_searchField1ActionPerformed

private void searchButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchButton1ActionPerformed
    try {
            resultsModel2.clear();
            if(bothPanelCheckBox.isSelected()) resultsModel.clear();
            String finalquery = ConnectionManager.getInstance().fix_schema_name(query);

            String search = null;
            if (comboFieldSelect.getSelectedIndex() != 0) {
                finalquery += " WHERE " + table.tab_prefix() + comboFieldSelect.get_db_name() + " LIKE ? ";
                search = searchField1.getText();
            }
            finalquery += " ORDER BY " + table.tablePrefix + "ID DESC";
                        
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(finalquery);

            ////System.out.println("Query = " + finalquery);
            
            if (search != null) {
                ps.setString(1, search);
            }

            ////System.out.println(ps.toString());
            
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {
                AbstractTable copy = (AbstractTable) table.clone();
                copy.loadFromRset(rset);
                resultsModel2.add(copy);   
                if(bothPanelCheckBox.isSelected()) resultsModel.add(copy);
            }

            rset.close();
            ps.close();
        } catch (SQLException ex) {
            logger.severe("Caught exception while searching");
            logger.severe(ex.getMessage());
        }
    setDiffButtonStatus();
}//GEN-LAST:event_searchButton1ActionPerformed

private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
    setDiffButtonStatus();
}//GEN-LAST:event_jTable1MouseClicked

private void jTable2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MouseClicked
    setDiffButtonStatus();
}//GEN-LAST:event_jTable2MouseClicked

private void setDiffButtonStatus() {
    if(jTable1.getSelectedRow() > -1 && jTable2.getSelectedRow() > -1) diffButton.setEnabled(true);
    else diffButton.setEnabled(false);
}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox bothPanelCheckBox;
    private javax.swing.JButton cancelButton;
    private triggertool.Components.FieldComboBox comboFieldSelect;
    protected javax.swing.JComboBox<String> comboTable;
    private javax.swing.JButton diffButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JButton searchButton;
    private javax.swing.JButton searchButton1;
    private javax.swing.JTextField searchField;
    private javax.swing.JTextField searchField1;
    // End of variables declaration//GEN-END:variables

}
