package triggertool.Auxiliary;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.Vector;
import triggertool.Panels.QuickMenuView;

/**
 * Class to hold the graphical information of a trigger item for displaying the
 * trigger tree on the screen.
 * @author Tiago Perez <tiago.perez@desy.de>
 */
public class DrawingTriggerItem {

    /** Def screen height. */
    private static final int DEF_H2 = 25;
    /** def screen children side step.*/
    private static final int DEF_SIDE_STEP = 50;
    /** Gery fill colour. */
    private static final Color GREYFILL = new Color(200, 200, 200);
    /** The thin blue stroke.*/
    private static final BasicStroke thin = new BasicStroke(0.25f);
    /** The fat black stroke.*/
    private static final BasicStroke THICK = new BasicStroke(1.0f);
    /** The X position. */
    private int x = 10; // Pos X
    /** The Y position. */
    private int y = 0; // Pos Y
    /** The width of this. */
    private int w = 200; // Width
    /** The height of this. */
    private int h = 20; // Height
    /** The screen height: this + children). */
    private int h2 = DEF_H2; // Screen height: this + children
    private String name = "";
    // Main Father (for multi seeded items)
    // This item will be displayed only once after this.father. Other items this
    // may seed from will only draw a line to this position.
    private DrawingTriggerItem father = null;
    private String lowerName = null;
    /** Chains which seed form this.*/
    private Vector<DrawingTriggerItem> children = new Vector<>();

    /**
     * Default constructor.
     * @param itemName the name of this item (should be unique).
     */
    public DrawingTriggerItem(String itemName) {
        this.name = itemName;
    }

    /**
     * New Item with a name and a father.
     * @param itemName
     * @param fatherDTI
     */
    public DrawingTriggerItem(final String itemName, final DrawingTriggerItem fatherDTI) {
        this.name = itemName;
        this.father = fatherDTI;
    }

    /**
     *
     * @return
     */
    public DrawingTriggerItem getFather() {
        return this.father;
    }

    /**
     * Set the father.
     * @param pap
     */
    public void setFather(final DrawingTriggerItem pap) {
        this.father = pap;
    }

    /**
     * Set lowerChain names
     * @param lowerN
     */
    public void setLowerName(final String lowerN) {
        this.lowerName = lowerN;
    }

    /**
     *
     * @return
     */
    public String getLowerName() {
        return this.lowerName;
    }

    /** Recover the name
     * @return  */
    public String getName() {
        return name;
    }

    /**
     *
     * @return
     */
    public int getH() {
        return this.h;
    }

    /**
     *
     * @param h
     */
    public void setH(int h) {
        this.h = h;
    }

    /**
     *
     * @return
     */
    public int getH2() {
        return this.h2;
    }

    /**
     *
     * @param h2
     */
    public void setH2(int h2) {
        this.h2 = h2;
    }

    /** List of children (used to add also).
     * @return  */
    public final Vector<DrawingTriggerItem> getChildren() {
        return this.children;
    }

    /** Set the width of the box.
     * @param inp */
    public void setW(int inp) {
        w = inp;
    }

    /** X position on the screen.
     * @param inp */
    public void setX(int inp) {
        x = inp;
    }

    /** Y position on the screen.
     * @param inp */
    public void setY(int inp) {
        y = inp;
    }

    /** Calculate the height. Takes into accound the children also.
     * @return  */
    public int getHeight() {
        int i = 0;
        int heigth = 0;
        for (DrawingTriggerItem o : getChildren()) {
            // Only increase Heigth if we are the father of o, otherwise
            // we will just draw a line to the position of o.
            if (o.getFather() == null || o.getFather().equals(this)) {
                o.setX(x + w + DrawingTriggerItem.DEF_SIDE_STEP);
                o.setY(y + heigth);
                ++i;
                heigth += o.getHeight();
            }
        }

        if (i == 0) {
            h2 = DrawingTriggerItem.DEF_H2;
        } else {
            h2 = heigth;
        }

        return h2;
    }

    /**
     * Draw this to the screen.  You need to supply a g2 object.
     *
     * @param g2 Where this gets drawn.
     */
    public final void draw(Graphics2D g2) {
        for (DrawingTriggerItem o : getChildren()) {
            if (o.getFather() == null || o.getFather().equals(this)) {
                o.setX(x + w + DrawingTriggerItem.DEF_SIDE_STEP);
                o.draw(g2);
            } else {
                g2.setStroke(thin);
                g2.setColor(Color.BLUE);
            }
            g2.drawLine(x + w - 5, y + h / 2, o.x, o.y + o.h / 2);
        }
        g2.setStroke(THICK);
        g2.setColor(GREYFILL);
        g2.fill(new Rectangle2D.Double(x, y, w, h));
        g2.setColor(Color.BLACK);
        g2.draw(new Rectangle2D.Double(x, y, w, h));
        //
        g2.setFont(QuickMenuView.PSFONT);
        FontMetrics metrics = g2.getFontMetrics(g2.getFont());
        int length = metrics.stringWidth(name);
        g2.drawString(name, x + (w / 2) - length / 2, y + 16);
    }

    /**
     * Get the width of the name when rendered in the current font of g2.
     *
     * @param g2 The canvas with all the current font settings.
     * @return The width as an integer, in pixels.
     */
    public int getWidth(Graphics2D g2) {        
        FontMetrics metrics = g2.getFontMetrics(g2.getFont());
        return metrics.stringWidth(name);
    }
}
