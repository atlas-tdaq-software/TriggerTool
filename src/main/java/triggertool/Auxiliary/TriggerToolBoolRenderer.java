package triggertool.Auxiliary;

import java.awt.Component;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * Custom cell renderer. It set the a light gry background every 2nd row.
 * @author tiago perez
 */
public final class TriggerToolBoolRenderer extends JCheckBox
        implements TableCellRenderer {

    /** Default constructor. */
    public TriggerToolBoolRenderer() {
        setHorizontalAlignment(JLabel.CENTER);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Component getTableCellRendererComponent(final JTable table,
            final Object value, final boolean isSelected,
            final boolean hasFocus, final int row, final int column) {
        if (isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        } else {
            setForeground(table.getForeground());
            setBackground(table.getBackground());
        }
        setSelected((value != null && ((Boolean) value)));
        return this;
    }
}
