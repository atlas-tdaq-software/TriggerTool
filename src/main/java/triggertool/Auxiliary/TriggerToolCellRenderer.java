package triggertool.Auxiliary;

import java.awt.Color;
import java.awt.Component;
import java.text.NumberFormat;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import external.ColorUtil;

/**
 *
 * Custom renderer for the floating L1 prescales. This renderer removes the
 * LOCALE dependent thousands separator.
 * @author tiago perez (tiago.perez@desy.de)
 */
public final class TriggerToolCellRenderer extends DefaultTableCellRenderer {

    /** The default cell renderer to start with. */
    public static final DefaultTableCellRenderer DEFAULT_RENDERER =
            new DefaultTableCellRenderer();
    /** Default background color for even rows. */
    public static final Color CELLBKG =
            ColorUtil.setSaturation(Color.BLUE, 0.08f);

    /**
     *
     */
    public static final Color CELLBKG2 = Color.WHITE;
    //ColorUtil.setSaturation(Color.GREEN, 0.01f);
    /** The number format to modify. */
    private NumberFormat f;
    /** Alignement. */
    private int alignement = SwingConstants.RIGHT;
    /** Flag to control colorizing of even rows. */
    private boolean useColors = true;

    /** Default constructor. */
    public TriggerToolCellRenderer() {
        //super();
    }

    /**
     * Constructor with a specific number format.
     *
     * @param format the number format to use.
     */
    public TriggerToolCellRenderer(final NumberFormat format) {
        //super();
        f = format;
    }

    @Override
    public Component getTableCellRendererComponent(final JTable table,
            final Object value, final boolean isSelected,
            final boolean hasFocus, final int row, final int column) {
        Component label = DEFAULT_RENDERER.getTableCellRendererComponent(
                table, value, isSelected, hasFocus, row, column);
        if (this.useColors) {
            label.setForeground(this.getForeground());
        }
        String formattedValue = "";
        if (value instanceof Boolean) {
            TriggerToolBoolRenderer rend = new TriggerToolBoolRenderer();
            label = rend.getTableCellRendererComponent(table, value,
                    isSelected, hasFocus, row, column);
        } else {
            if (value == null) {
                formattedValue = "";
            } else if (value instanceof Float) {
                Float fValue = (Float) value;
                f = NumberFormat.getInstance();
                f.setGroupingUsed(false);
                formattedValue = f.format(fValue);
            } else {
                formattedValue = value.toString();
            }
            ((JLabel) label).setText(formattedValue);
            ((JLabel) label).setHorizontalAlignment(this.alignement);
        }
        // Set color to even rows.
        if (!isSelected && this.useColors) {
            if (row % 2 == 0) {
                label.setBackground(CELLBKG);
            } else {
                label.setBackground(CELLBKG2);
            }
        }

        return label;
    }

    @Override
    public void setHorizontalAlignment(final int a) {
        this.alignement = a;
    }

    /**
     * Enable/disable colourising even rows.
     * @param use <code>true</code> to enable colors.
     */
    public final void setUseColors(final boolean use) {
        this.useColors = use;
    }
}
