package triggertool.Auxiliary;



import java.util.logging.ErrorManager;
import java.util.logging.Filter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;
import triggertool.Panels.LoggerWindow;

/**
 * A handler which displays messages from the TriggerTool logger in a window
 *
 * @author Isabella Soldner-Rembold & Mark Owen
 */
public class LoggingWindowHandler extends Handler {

    private LoggerWindow loggerwindow;
    private static LoggingWindowHandler handler;

    private LoggingWindowHandler() {
        configure();
        loggerwindow = new LoggerWindow();
        
    }

    /**
     *
     * @return
     */
    public static synchronized LoggingWindowHandler getInstance() {
        if (handler == null) {
            handler = new LoggingWindowHandler();
        }
        return handler;
    }

    /**
     * Function to get the actual log window.
     * @return the logging window.
     */
    public LoggerWindow getWindow() {
        return loggerwindow;
    }

    private void configure() {
        LogManager manager = LogManager.getLogManager();
        String className = getClass().getName();
        String level = manager.getProperty(className + ".level");
        //Sets the default level at Finest
        setLevel((level == null) ? Level.FINEST : Level.parse(level));
        String filter = manager.getProperty(className + ".filter");
        setFilter(makeFilter(filter));
        String formatter =
                manager.getProperty(className + ".formatter");
        setFormatter(makeFormatter(formatter));
    }

    private Filter makeFilter(String name) {
        Filter f = null;
        try {
            Class c = Class.forName(name);
            f = (Filter) c.newInstance();


        } catch (Exception e) {
            if (name != null) {
                System.err.println("Unable to load filter: " + name);
            }
        }
        return f;
    }

    private SimpleFormatter makeFormatter(String name) {
        SimpleFormatter f = null;
        try {
            Class c = Class.forName(name);
            f = (SimpleFormatter) c.newInstance();
        } catch (Exception e) {
            f = new SimpleFormatter();
        }
        return f;
    }
    
    @Override
    public void close() {
    }

    @Override
    public void flush() {
    }

    /**
     * Publishes the data to the LoggerWindow
     * @param record The data from the TriggerTool logger
     */
    @Override
    public void publish(LogRecord record) {
        if (isLoggable(record)) {

            try {
                //Adds data to the loggerwindow
                loggerwindow.addLogInfo(record);
            } catch (Exception e) {
                reportError(null, e, ErrorManager.WRITE_FAILURE);
            }
        }
    }
}
