/*
 * ByteToHex
 *  
 */
package triggertool.Auxiliary;

/**
 * Aux class to convert Byte to Hex strings.
 * Create on 24.10.2010.
 * @author tiago perez
 */
public final class ByteToHex {

    /**
     *
     * @param b
     * @return
     */
    public static String getHexString(byte[] b) {
        String result = "";
        for (int i = 0; i < b.length; i++) {            
            result +=
                    Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }
}
