/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.L1Records;

/**
 *
 * @author giannell
 * 
 * The prescaling uses a pseudo-random binary sequence generator of 24 bit width (31 bit internal)
 * 
 *    - Prescales are defined by a cutoff (C).
 *- Each time a random number (R) is generated.
 *- C, R are between 0 and 2**24-1
 *
 *- The trigger item is accepted if R>=C. The prescale (PS) corresponding 
 * to a cutoff C is PS = (2**24-1)/(2**24-C)
 *
 */
public final class CutToValueConverter {
    
    /**
     *
     * @param dbValue
     * @return
     */
    public static Double calculatePrescaleFromCut(int dbValue)
    {
        // PS = (2**24-1)/(2**24-C)
        double sign = dbValue>=0 ? 1 : -1;
        int validCut = Math.abs(dbValue);
        if(validCut==0) {
            validCut = 1;
        }
        if(validCut>0xFFFFFF) {
            validCut=0xFFFFFF;
        }
        
        return (sign * 0xFFFFFF) / (0x1000000 - validCut);
    }
    
    /**
     *
     * @param frequency
     * @return
     */
    public static int calculateCutFromPrescale(double frequency)
    {
        // C = 2**24-(2**24-1)/PS

        int sign = frequency>=0 ? 1 : -1;
        
        double cut =  0x1000000 - 0xFFFFFF/Math.abs(frequency);
        cut = Math.round(cut);
        if(cut==0x1000000) {
            cut = 0xFFFFFF; // for prescale values that are larger than 2*24-1
        }
        
        return (int)(sign*cut);
    }
    
    /**
     *
     * @param cutOff
     * @return
     */
    public static Integer getIntegerFromHexString(String cutOff)
    {
        if (!cutOff.isEmpty()) 
        {
            return Integer.parseInt(cutOff, 16);
        }
        else {
            return -1;
        }
    }
    
    /**
     *
     * @param dbValue
     * @return
     */
    public static String getHexStringFromInteger(int dbValue)
    {
        String sign = dbValue>=0 ? "" : "-";       
        String hex = Integer.toHexString(Math.abs(dbValue));
        while (hex.length() < 6){
            hex = "0"+hex;
        }
        return sign + hex; //& 0xffffff);
    }
    
    /**
     *
     * @param value
     * @return
     */
    public static boolean tryParseInt(String value)  
    {  
        try  
        {  
            Integer.parseInt(value);  
            return true;  
        } catch(NumberFormatException nfe)  
        {  
            return false;  
        }  
    } 
    
    /**
     *
     * @param cut
     * @return
     */
    public static Double getPrescaleFromHexCut(String cut){
        int cutValue = getIntegerFromHexString(cut);
        return calculatePrescaleFromCut(cutValue);
    }

    /**
     *
     * @param value
     * @return
     */
    public static String getHexCutFromPrescale(Double value){
        int cutValue = calculateCutFromPrescale(value);
        return getHexStringFromInteger(cutValue);
    }
}
