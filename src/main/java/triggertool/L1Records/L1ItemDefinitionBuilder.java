/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.L1Records;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.L1.L1Threshold;

/**
 *
 * @author Michele
 */
public final class L1ItemDefinitionBuilder {

    /**
     * @param item contains a definition of style ((1&2)|2|3)
     * @return the corresponding string ((EM3[x1]&amp;MU4[x2])|BGRP0|BGRP1)
     */
    public static String GetDefinition(final L1Item item){
        ArrayList<String> names = GetThresholdNames(item);
        // needed to change the previous implementation since it
        // lead to results like this: MBTS_A[xMBTS_C[x2]]|2|MBTS_A[x1]
        String definition = item.get_definition();
        Pattern p = Pattern.compile("(\\d+)");
        Matcher m = p.matcher(definition);
        StringBuilder s = new StringBuilder(1000);
        int curpos = 0;
        while(m.find(curpos)) { // finds matches of numbers (\\d+) and puts them in group 1
            s.append(definition.substring(curpos,m.start(1)));
            s.append(names.get(Integer.parseInt(m.group(1))-1)); // replace the 
            curpos=m.end(1);
        }
        s.append(definition.substring(curpos,definition.length()));
        
        return s.toString().replace("&", "&amp;");
    }

    private static ArrayList<String> GetThresholdNames(L1Item item) {
        ArrayList<String> names = new  ArrayList<>();
        for(L1Threshold th : item.getThresholds()){
            if (L1Threshold.isInternalThreshold(th)){
                names.add(InternalTriggerDefinition(th));
            }
            else{
                names.add(TriggerConditionDefinition(th));
            }
        }
        return names;
    }
    
    private static String TriggerConditionDefinition(L1Threshold th){
        return th.get_name() + "[x" + th.get_multiplicity() + "]";
    }

    private static String InternalTriggerDefinition(L1Threshold th){
        return th.get_name();
    }
}
