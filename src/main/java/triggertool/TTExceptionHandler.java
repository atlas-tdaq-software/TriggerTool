/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool;

import java.awt.Component;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import triggerdb.UploadException;

/**
 *
 * @author Michele
 */
public class TTExceptionHandler {
    
    private static final Logger logger = Logger.getLogger("TriggerTool");
    
    /**
     *
     * @param ex
     * @param component
     * @param message
     */
    public static void HandleSqlException(SQLException ex, Component component, String message) {
    logger.log(Level.SEVERE, message, ex); 
    JOptionPane.showMessageDialog(component, message + "\nDetail: " + ex.getMessage() +
            " \nPlease report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, "SQL Error", JOptionPane.ERROR_MESSAGE);
    }
    
    /**
     *
     * @param ex
     * @param component
     * @param message
     */
    public static void HandleNumberFormatException(NumberFormatException ex, Component component, String message) {
    logger.log(Level.SEVERE, message, ex); 
    JOptionPane.showMessageDialog(component, message + "\nDetail: " +  ex.getMessage() +
            " \nPlease report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, "Number Format Exception", JOptionPane.ERROR_MESSAGE);
    }
    
    /**
     *
     * @param ex
     * @param component
     * @param message
     */
    public static void HandleNullPointerException(NullPointerException ex, Component component, String message) {
    logger.log(Level.SEVERE, message, ex); 
    JOptionPane.showMessageDialog(component, message + "\nDetail: " +  ex.getMessage() +
            " \nPlease report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, "Null Pointer Exception", JOptionPane.ERROR_MESSAGE);
    }
    
    /**
     *
     * @param ex
     * @param component
     * @param message
     */
    public static void HandleUploadException(UploadException ex, Component component, String message) {
    logger.log(Level.SEVERE, message, ex); 
    JOptionPane.showMessageDialog(component, message + "\nDetail: " +  ex.getMessage() +
            " \nPlease report this error attaching the log file. This can be found in " + triggertool.TriggerTool.logFileName, "Upload Exception", JOptionPane.ERROR_MESSAGE);
    }
}
