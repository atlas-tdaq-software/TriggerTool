package triggertool.BackEnd;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.HLT.HLTPrescaleSet;

/**
 * Class to deal with HLT prescale edit.
 * @author tiago perez <tperez@cern.ch>
 */
public class HLTPrescaleEdit {

    private static final long serialVersionUID = 1L;
    // The TriggerTool logger

    /**
     *
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    // The Menu Id
    int menuId = -1;
    //

    /**
     * Default constructor. Queries all HLT Prescale Sets belonging to this SMT 
     * form DB.
     * @param _menuId
     */
    public HLTPrescaleEdit(int _menuId) {
        this.menuId = _menuId;
    }

    /**
     *
     */
    public class PSSETCOMP {

        /**
         *
         */
        public HLTPrescaleSet set;
        //public HLTTM_PS set2menu;

        /**
         *
         */
        public Boolean setUsedInMenu;
    }

    /**
     * Query HLT Prescale Sets from DB.
     * @return 
     * @throws java.sql.SQLException 
     */
    public ArrayList<PSSETCOMP> getHLTSets() throws SQLException {
        ArrayList<PSSETCOMP> hltPsSets = new ArrayList<>();
        //get HLT prescales
        String query = "SELECT HPS_ID, HTM2PS_PRESCALE_SET_ID, HTM2PS_TRIGGER_MENU_ID, HTM2PS_USED "
                + "FROM "
                + "HLT_PRESCALE_SET, HLT_TM_TO_PS "
                + "WHERE "
                + "HTM2PS_TRIGGER_MENU_ID=? "
                + "AND "
                + "HTM2PS_PRESCALE_SET_ID=HPS_ID "
                + "ORDER BY HPS_ID DESC";
        query = ConnectionManager.getInstance().fix_schema_name(query);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, menuId);
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            //
            HLTPrescaleSet hltps = new HLTPrescaleSet(rset.getInt("HPS_ID"));
            Boolean used = rset.getBoolean("HTM2PS_USED");
            PSSETCOMP setComplex = new PSSETCOMP();
            setComplex.set = hltps;
            setComplex.setUsedInMenu = used;
            hltPsSets.add(setComplex);
        }
        rset.close();
        ps.close();

        return hltPsSets;
    }

       /**
     * Query HLT Prescale Sets from DB.
     * @param showHidden
     * @return 
     * @throws java.sql.SQLException 
     */
    public ArrayList<String> getHLTSetInfo(Boolean showHidden) throws SQLException {
         ArrayList<String> hltPsInfoSets = new ArrayList<>();
        //get HLT prescales
        String query = "SELECT HPS_ID, HPS_NAME, HPS_VERSION, HTM2PS_PRESCALE_SET_ID, HTM2PS_TRIGGER_MENU_ID, HTM2PS_USED "
                + "FROM "
                + "HLT_PRESCALE_SET, HLT_TM_TO_PS "
                + "WHERE "
                + "HTM2PS_TRIGGER_MENU_ID=? "
                + "AND "
                + "HTM2PS_PRESCALE_SET_ID=HPS_ID "
                + "ORDER BY HPS_ID DESC";
        query = ConnectionManager.getInstance().fix_schema_name(query);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, menuId);
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            String info = rset.getInt("HPS_ID") + ": " + rset.getString("HPS_NAME") + " v" + rset.getString("HPS_VERSION");
            if(showHidden){
                hltPsInfoSets.add(info);
            }
            else{
                if(!rset.getBoolean("HTM2PS_USED")){
                   hltPsInfoSets.add(info);  
                }
            }
        }
        rset.close();
        ps.close();

        return hltPsInfoSets;
    }
    
    /**
     * Grabs all the group names and associated chain IDs.
     * Used to enable/disable chains in specific groups
     * 0 GroupName
     * 1 ChainId
     * @return 
     * @throws java.sql.SQLException 
     */
    public final ArrayList<ArrayList<Object>> loadGroups() throws SQLException {
        ArrayList<ArrayList<Object>> groups = new ArrayList<>();
        String query = "SELECT HTG_NAME, HTG_TRIGGER_CHAIN_ID, HTM_ID, HTM2TC_TRIGGER_MENU_ID, HTM2TC_TRIGGER_CHAIN_ID "
                + "FROM "
                + "HLT_TM_TO_TC, HLT_TRIGGER_MENU, HLT_TRIGGER_GROUP "
                + "WHERE "
                + "HTM_ID=? "
                + "AND "
                + "HTM2TC_TRIGGER_MENU_ID=HTM_ID "
                + "AND "
                + "HTM2TC_TRIGGER_CHAIN_ID=HTG_TRIGGER_CHAIN_ID";

        query = ConnectionManager.getInstance().fix_schema_name(query);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, menuId);
        ResultSet rset = ps.executeQuery();

        while (rset.next()) {
            ArrayList<Object> group = new ArrayList<>();
            group.add(rset.getString("HTG_NAME"));
            group.add(rset.getInt("HTG_TRIGGER_CHAIN_ID"));
            groups.add(group);
        }

        rset.close();
        ps.close();

        return groups;
    }

    /**
     * Update comment of HLTPrescaleSet with id Id
     * @param comment the comment to set.
     * @param setId the id of the PrescaleSet to update.
     * @throws java.sql.SQLException
     */
    public final void HLTComment(String comment, Integer setId) throws SQLException {
        if (comment == null || setId < 1) {
            logger.log(Level.WARNING, "Error updating comment ({0})for HLTPrescale with  id {1}", new Object[]{comment, setId});
            return;
        }

        if (comment.isEmpty()) {
            comment = "~";
        }

        String query = "UPDATE HLT_PRESCALE_SET SET HPS_COMMENT=? WHERE HPS_ID=?";
        query = ConnectionManager.getInstance().fix_schema_name(query);
        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setString(1, comment);
        ps.setInt(2, setId);
        ps.executeUpdate();
        ps.close();
    }

    /**
     * Hide a set.
     * @param psSet
     * @throws java.sql.SQLException
     */
    public final void hidePrescaleSet(HLTPrescaleSet psSet) throws SQLException {
        int linkid = psSet.get_TMPSid(menuId);
        this.setHTM2PSused(linkid, 1);
    }

    /**
     * Show a set.
     * @param psSet
     * @throws java.sql.SQLException
     */
    public final void showPrescaleSet(HLTPrescaleSet psSet) throws SQLException {
        int linkid = psSet.get_TMPSid(menuId);
        this.setHTM2PSused(linkid, 0);
    }

    private void setHTM2PSused(int linkId, int value) throws SQLException {
        String query = "UPDATE HLT_TM_TO_PS SET HTM2PS_USED=? WHERE HTM2PS_ID=?";
        query = ConnectionManager.getInstance().fix_schema_name(query);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, value);
        ps.setInt(2, linkId);
        ps.executeUpdate();
        ps.close();
    }
}
