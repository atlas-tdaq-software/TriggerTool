/**
 * XMLUploader.java
 *
 * DATE: 2011-05-07
 */
package triggertool.BackEnd;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.HLT.HLTSetup;
import triggerdb.Entities.HLT.HLTTriggerMenu;
import triggerdb.Entities.InternalWriteLock;
import triggerdb.Entities.L1.*;
import triggerdb.Entities.SMT.SMK_TO_RE;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.Entities.Topo.*;
import triggertool.XML.ConfigurationException;
import triggertool.XML.Hlt_XMLtoDB;
import triggertool.XML.L1_XMLtoDB;
import triggertool.XML.Release_XMLtoDB;
import triggertool.XML.Topo_XMLtoDB;
import triggerdb.UploadException;

/**
 * Class that controls the uploading of XML files to DB. This should replace the
 * logic in TriggerTool.uploadConfiguration and
 * triggertool.Components.UploadXML.
 *
 * @author tiago perez <tperez@cern.ch>
 */
public final class XMLUploader {

    /**
     * Message Log.
     * This needs to be upgraded to Michele's bubbled logger.
     */
    protected static final Logger logger = Logger.getLogger("TriggerTool");
    /**
     * The super master table.
     */
    private SuperMasterTable smt;
    /**
     * The Configuration Name. If not given read it from XML files.
     */
    private String confName = "";
    /**
     * The XML with the L1 menu.
     */
    private String lvl1menuXML = "";
    /**
     * The XML with the Topo menu.
     */
    private String topomenuXML = "";
    /**
     * Path to the HLT release OR HLT release name.
     */
    private String release = "";
    /**
     * The db id of the release.
     */
    private int releaseId = -1;
    /**
     * The XML with the HLT Menu.
     */
    private String hltmenuXML = "";
    /**
     * The XML with the HLT setup info.
     */
    private String hltSetupXML = "";
     /**
     * writing of trigger levels (0-L1Topo only, 1-L1Topo + CTP, 2 - L1Topo, CTP, HLT)
     */
    private int maxUploadLevel = 2;

    /**
     * parameter for the SMT.
     */
    private Boolean onlineConfig = false;
    private Boolean offlineConfig = false;
    private Boolean markReplicate = false;
    private String smComment = "";
    /**
     * L1 Master Id
     */
    private int l1Id = -1;
    /**
     * L1 PSK.
     */
    private int l1Psk = -1;
    /**
     * Topo Master Id
     */
    private int topoId = -1;
    /**
     * HLT Master Id
     */
    private int hltId = -1;
    /**
     * HLT PSK.
     */
    private int hltPsk = -1;
    /**
     * set from command line.
     */
    private boolean fromcommandline = false;

    /**
     *
     */
    public XMLUploader() {
    }

    /**
     * Parses the command line arguments and sets up the uploader.
     *
     * @param cmd the command arguments.
     */
    public XMLUploader(final CommandLine cmd) {
        // Command Line Options.
        // if no name supplied, use what is in the xml file
        if (!cmd.hasOption("n")
                || cmd.getOptionValue("n").equalsIgnoreCase("XML")
                || cmd.getOptionValue("n").equalsIgnoreCase("USEXML")) {
            confName = "USEXML";
        } else {
            confName = cmd.getOptionValue("n");
        }

        maxUploadLevel = 2;

        // HLT
        release = "";
        if (cmd.hasOption("rel")) {
            release = cmd.getOptionValue("rel");
        } else {
            logger.severe(" No release given. Please provide a release with the"
                    + " option \"-rel\"");
        }

        hltSetupXML = "";
        if (cmd.hasOption("hlt_setup")) {
            hltSetupXML = cmd.getOptionValue("hlt_setup");
        }
        hltmenuXML = "";
        if (cmd.hasOption("hlt_menu")) {
            hltmenuXML = cmd.getOptionValue("hlt_menu");
        } else {
            maxUploadLevel = 1;
        }

        topomenuXML = "";
        if(cmd.hasOption("topo_menu")) {
            topomenuXML = cmd.getOptionValue("topo_menu");
        } else {
            logger.severe("No topo menu given, but one is needed. Please provide a release with the"
                    + " option \"-topo_menu\"");
        }

        lvl1menuXML = "";
        if(cmd.hasOption("l1_menu")) {
            lvl1menuXML = cmd.getOptionValue("l1_menu");
        } else {
            maxUploadLevel = 0;
        }

        //
        onlineConfig = cmd.hasOption("onl");
        offlineConfig = cmd.hasOption("off");
        markReplicate = cmd.hasOption("mr");
        smComment = "";
        if (cmd.hasOption("SMcomment")) {
            smComment = cmd.getOptionValue("SMcomment");
        }
        fromcommandline = true;
    }

    /**
     * Tries to upload a trigger configuration from the xml files.
     *
     * @return the id of the uploaded SMT.
     * @throws java.sql.SQLException
     * @throws triggerdb.UploadException
     * @throws triggertool.XML.ConfigurationException
     */
    public int upload() throws SQLException, UploadException, ConfigurationException {
        int smtId = -1;

        // validate input
        if(!this.checkUploadData()) {
            logger.severe("Can not upload configuration from XML.");
            return smtId;
        }
        
        // Lock DB
        boolean lockOK = this.lockDB();
        if (!lockOK) {
            logger.severe("Could not obtain write lock for uploading XML");
            return smtId;
        }

        int relId  = this.releaseId;
            
        // Start upload
            
        // release
        if (relId < 0) {
            relId = this.getRelease(release);
        }
        if (relId < 1) {
            logger.log(Level.SEVERE, " Problems saving release {0}", release);
            this.unlockDB();
            return smtId;
        }
        
        // upload Topo unless an ID exists already
        if (this.topoId < 0) {
            logger.log(Level.INFO, "Uploading L1Topo menu {0}", new Object[]{this.topomenuXML});
            this.uploadTopo();
        }
        // check that upload succeeded
        if (this.topoId < 1) {
            logger.log(Level.SEVERE, " Problems saving Topo Menu {0} ({1})", new Object[]{this.topomenuXML, this.topoId});
            this.unlockDB();
            return smtId;
        }

        
        // upload L1 unless an ID exists already
        if(maxUploadLevel>=1) {
            if (this.l1Id < 0) {
                logger.log(Level.INFO, "Uploading L1 menu {0}", new Object[]{this.lvl1menuXML});
                this.uploadL1();
            }
            // check that upload succeeded
            if (this.l1Id < 1) {
                logger.log(Level.SEVERE, " Problems saving L1 Menu {0} ({1})", new Object[]{lvl1menuXML, this.l1Id});
                this.unlockDB();
                return smtId;
            }
        } else {
            logger.fine("Uploading dummy L1 menu");
            this.saveDummyL1();
        }
        
        
        // upload HLT unless an ID exists already
        if(maxUploadLevel==2) {
            if (this.hltId < 0) {
                logger.log(Level.INFO, "Uploading HLT menu {0}", new Object[]{this.hltmenuXML});
                this.uploadHLT();
            }
            // check that upload succeeded
            if (this.hltId < 1) {
                logger.log(Level.SEVERE, " Problems saving HLT Menu {0} ({1})", new Object[]{hltmenuXML, this.hltId});
                this.unlockDB();
                return smtId;
            }
        } else {
            logger.fine("Uploading dummy HLT menu");
            this.saveDummyHLT();
        }

        // Create entry in SuperMasterTable
        logger.log(Level.INFO, "Calling SMK creation with L1Topo key {0}, CTP key {1}, and HLT key {2}", 
                   new Object[]{this.topoId, this.l1Id, relId, this.hltId});
        smtId = uploadSMT(this.topoId, this.l1Id, relId, this.hltId);

        this.logUpload(smtId, relId, this.l1Psk, this.getHltPsk());
        this.unlockDB();
        return smtId;
    }

    /**
     * Loads the release from db with name "rel".
     *
     * @param rel the name of the hlt release to load.
     * @return the id of the rel with name "rel".
     */
    private int getRelease(final String rel) throws SQLException, ConfigurationException {
        int relid = -1;
        //Release upload
        Release_XMLtoDB hre = new Release_XMLtoDB();
        try {
            relid = hre.loadRelease(rel);
        } catch (SQLException e) {
            logger.log(Level.FINE, "Loading of the release file failed: {0}", e.getMessage());
            throw e;
        }

        //if we get back -2 (file not found) assume string is the release
        if (relid == -2) {
            try {
                relid = hre.loadReleaseString(rel);
                logger.info("Set release as string");
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Loading of Release failed: {0}", e.getMessage());
                throw e;
            }
        }
        return relid;
    }

    /**
     * Lock the DB.
     *
     * @return
     * <code>true</code> if DB locked.
     */
    private boolean lockDB() throws UploadException, SQLException {
        boolean dbLocked;
        InternalWriteLock writeLock = InternalWriteLock.getInstance();
        try {
            dbLocked = writeLock.setLock("XML upload " + confName);
            if (!dbLocked) {
                logger.log(Level.SEVERE, " Database is already locked by {0}", writeLock.get_username());
            }
        } catch (SQLException e) {
            logger.severe(e.getMessage());
            dbLocked = false;
        }
        return dbLocked;
    }

    /**
     * Try to remove write lock.
     */
    private void unlockDB() throws SQLException {
        InternalWriteLock writeLock = InternalWriteLock.getInstance();
        writeLock.removeLock();
    }

    /**
     * Upload and save the L1 menu. It sets this.l1MasterId and this.l1psk if
     * the upload succeeds, else they are "-1".
     *
     * @return the L1 master key.
     */
    private void uploadL1() throws SQLException, ConfigurationException {
        L1_XMLtoDB reader = new L1_XMLtoDB();
        this.l1Id = reader.readXML(this.lvl1menuXML, this.confName, this.confName, this.topoId);
        if (this.l1Id < 1) {
            logger.severe("Problems uploading L1 XML file.");
        } else {
            this.l1Psk = reader.getLVLPrescaleSetKey();
        }
    }
    
    /**
     * Save a dummy L1 menu with the name of this super master table.
     * Now necessary due to the fact we could have a l1Topo only menu uploaded
     *
     * @param l1id the id of the L1Master table (to get the name).
     * @return the id of the HLT Master table.
     */
    private void saveDummyL1() {
        this.l1Id = -1; 
        L1Master l1mt = new L1Master();
        
        int dummy = l1mt.findDummy();
        int menu_id;
        if(dummy < 0){
            try {
                L1Menu menu = new L1Menu();
                L1CtpSmx smx = new L1CtpSmx(-1);
                smx.set_name("dummyL1");
                L1CtpFiles files = new L1CtpFiles(-1);
                files.set_name("dummyL1");
                smx.set_name("dummyL1");
                menu.set_name("dummyL1");
                int smx_id = smx.save();
                int files_id = files.save();
                menu.set_ctp_safe(0);
                menu.set_ctp_smx_id(smx_id);
                menu.set_ctp_files_id(files_id);
                menu_id = menu.save();



                L1PrescaledClock psClock = new L1PrescaledClock(-1);
                L1CaloInfo caloInfo = new L1CaloInfo(-1);
                L1CaloIsolation isolation = new L1CaloIsolation(-1);
                L1CaloIsoParam isoParam = new L1CaloIsoParam(-1);
                L1MuctpiInfo muInfo = new L1MuctpiInfo(-1);
                L1MuonThresholdSet muThresh = new L1MuonThresholdSet(-1);
                L1Random rand = new L1Random();

                psClock.set_name("dummyL1");
                psClock.set_clock1(-1);
                psClock.set_clock2(-1);
                int psId = psClock.save();
                l1mt.set_prescaled_clock_id(psId);

                caloInfo.set_name("dummyL1");

                isoParam.set_iso_bit(-1);
                int isoPId = isoParam.save();
                isolation.set_thr_type("dummyL1");
                for(int i = 1; i < 6; i++){
                    isolation.set_par(i, isoPId);
                }
                int isoId = isolation.save();    
                caloInfo.set_iso_ha_em(isoId);
                caloInfo.set_iso_em_em(isoId);
                caloInfo.set_iso_em_tau(isoId);

                L1CaloMinTob caloMin = new L1CaloMinTob(-1);
                caloMin.set_thr_type("dummyL1");
                caloMin.set_eta_max(-1);
                caloMin.set_eta_min(-1);
                caloMin.set_pt_min(-1);
                caloMin.set_priority(-1);
                int cMinId = caloMin.save();

                caloInfo.set_min_tob_em(cMinId);
                caloInfo.set_min_tob_tau(cMinId);
                caloInfo.set_min_tob_jets(cMinId);
                caloInfo.set_min_tob_jetl(cMinId);

                int caloId = caloInfo.save();
                l1mt.set_calo_info_id(caloId);

                muInfo.set_name("dummyL1");
                muInfo.set_low_pt(-1);
                muInfo.set_high_pt(-1);
                muInfo.set_max_cand(-1);
                int muInfId = muInfo.save();
                l1mt.set_muctpi_info_id(muInfId);

                muThresh.set_name("dummyL1");
                int muThrId = muThresh.save();
                l1mt.set_muon_threshold_set_id(muThrId);

                rand.set_cut0(-1);
                rand.set_cut1(-1);
                rand.set_cut2(-1);
                rand.set_cut3(-1);
                int randId = rand.save();
                l1mt.set_random_id(randId);

                l1mt.set_name("dummyL1");
                l1mt.set_menu_id(menu_id);
                this.l1Id = l1mt.save();
                l1mt.set_id(l1Id);
            } catch (SQLException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        } else {
            this.l1Id = dummy;
        }
    }
    
    /**
     * Upload and save the Topo menu. It sets this.topoMasterId
     * the upload succeeds, else they are "-1".
     *
     * @return the topo master key.
     */
    private void uploadTopo() throws UploadException, ConfigurationException {
        Topo_XMLtoDB reader = new Topo_XMLtoDB();
        this.topoId = reader.readXML(this.topomenuXML, this.confName);
        if (this.topoId < 1) {
            logger.severe("Problems uploading Topo XML file.");
        }
    }
    
    /**
     * Save a dummy Topo menu with the name of this super master table.
     *
     * @param l1id the id of the L1Master table (to get the name).
     * @return the id of the HLT Master table.
     */
    private void saveDummyTopo() throws SQLException {
        this.topoId = -1;
        TopoMaster topoMaster = new TopoMaster(-1);
        
        int dummy = topoMaster.findDummy();
        
        if(dummy < 0){
            try {
                TopoMenu menu = new TopoMenu(-1);
                menu.set_name("dummyTopo");

                TopoOutputList list = new TopoOutputList(-1);
                list.set_name("dummyTopo");
                int files_id = list.save();
                menu.set_ctplink(files_id);
                int menuId = menu.save();

                topoMaster.set_menu_id(menuId);
                //A dummy hash
                topoMaster.set_hash(-99);
                topoMaster.set_topo_menu(menu);
                topoMaster.set_name("dummyTopo");
                this.topoId = topoMaster.save();
            } catch (SQLException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        } else {
            this.topoId = dummy;
        }
    }

    /**
     * Uploads and the HLT configuration from XML files. It sets
     * this.hltMasterId and this.hltPsk.
     *
     * @return The id of the HLT master table.
     */
    private void uploadHLT() throws SQLException, ConfigurationException {
        Hlt_XMLtoDB hlt = new Hlt_XMLtoDB();
        try {
            this.hltId = hlt.readHltConfig(hltSetupXML, hltmenuXML, confName, confName);
            if (this.hltId < 0) {
                logger.severe(" Problems uploading HLT XML file.");
            } else {
                this.setHltPsk(hlt.getHLTPrescaleSetKey());
            }
        } catch (SQLException | ConfigurationException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    /**
     * Save a dummy HLT menu with the name of this super master table.
     *
     * @param l1id the id of the L1Master table (to get the name).
     * @return the id of the HLT Master table.
     */
    private void saveDummyHLT() throws SQLException {
        this.hltId = -1;
        HLTMaster hmt = new HLTMaster();
        
        int dummy = hmt.findDummy();
        
        int menu_id;
        if(dummy < 0){
            try {
                HLTTriggerMenu menu = new HLTTriggerMenu(-1);
                menu.set_name("dummyHLT");
                menu_id = menu.save();
                HLTSetup setup = new HLTSetup();
                setup.set_name("dummyHLT");
                int setupid = setup.save();
                
                hmt.set_name("dummyHLT");
                hmt.set_menu_id(menu_id);
                hmt.set_setup_id(setupid);
                hmt.set_status(0);
                this.hltId = hmt.save();
                hmt.set_id(hltId);
            } catch (SQLException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
                throw e;
            }
        } else {
            this.hltId = dummy;
        }
    }

    /**
     * Create a new SMT linking the given L1 Menu and HLT Menu and save it to
     * DB.
     *
     * @param l1Id the L1 Trigger Menu Id
     * @param relId the release db id.
     * @param hltId the HLT Master Table db id.
     * @return the super master key.
     */
    private int uploadSMT(final int topoId, final int l1Id, final int relId, final int hltId) throws SQLException {
        //Create the SMT  
        smt = new SuperMasterTable();
        if (fromcommandline) {
            smt.set_fromcommandline();
        }
        if (confName.equals("USEXML")) {
            //Read the name from a sensible place otherwise we will end up with a dummyHLT/L1/Topo
            if(hltId>0){
                HLTMaster master = new HLTMaster(hltId);
                confName = master.get_name();
            } else if(l1Id>0) {
                L1Master master = new L1Master(l1Id);
                confName = master.get_name();
            } else if(topoId>0) {
                TopoMaster master = new TopoMaster(topoId);
                confName = master.get_name();
            }
        }
        smt.set_name(confName);
        //set origin and parent:
        smt.set_origin("XML UPLOAD");
        smt.set_parent_key(0);
        //set children
        smt.set_topo_master_table_id(topoId);
        smt.set_topo_master_table(new TopoMaster(topoId));
        smt.set_l1_master_table_id(l1Id);
        smt.set_l1_master_table(new L1Master(l1Id));
        smt.set_hlt_master_table_id(hltId);
        smt.set_hlt_master_table(new HLTMaster(hltId));
        //set the status - online or offline - use the functions in SuperMasterTable
        smt.setOnline(onlineConfig);
        smt.setMC(offlineConfig);
        smt.setRep(markReplicate);
        //add comment to the supermaster table
        smt.set_comment(smComment);
        InitInfo theInitInfo = ConnectionManager.getInstance().getInitInfo();
        smt.set_username(theInitInfo.getUserName());
        int smtid;
        smtid = smt.save();

        //finally assign the release like we assign prescales to the menus
        String relName = (new HLTRelease(relId)).get_name();
        SMK_TO_RE newrellink = new SMK_TO_RE();

        newrellink.set_supermaster_id(smtid);
        newrellink.set_release_id(relId);
        newrellink.save();
        // INFO/DEBUG msg.
        String infoMsg = "\t Saving SuperMasterTable (ID: " + smtid
                + ") has name: " + smt.get_name();
        infoMsg += "\n\tRelease (ID: " + relId + ") " + relName;
        infoMsg += "\n\tL1 ID IS " + l1Id;
        infoMsg += "\n\tHLT ID IS " + hltId;
        logger.info(infoMsg);

        return smt.get_id();
    }

    /**
     * Creates a upload summary message and send it to INFO log level.
     *
     * @param smk the super master key.
     * @param relk the release key.
     * @param _l1psk the L1 prescale key.
     * @param _hltpsk the HLT prescale key.
     */
    private void logUpload(final int smk, final int relk, final int _l1psk, final int _hltpsk) throws SQLException {

        String configName = "";
        String l1psName = "";
        String hltpsName = "";
        String relName = "";

        // write this to a file (append)!
        FileWriter writer;
        try {

            configName = (new SuperMasterTable(smk)).get_name();
            l1psName = (new L1Prescale(_l1psk)).get_name();
            hltpsName = (new HLTPrescaleSet(_hltpsk)).get_name();
            relName = (new HLTRelease(relk)).get_name();

            writer = new FileWriter("MenusKeys.txt", true);
            writer.write("SM Key      " + smk + ": " + configName + "\n");
            writer.write("L1 PS Key   " + _l1psk + ": " + l1psName + "\n");
            writer.write("HLT PS Key  " + _hltpsk + ": " + hltpsName + "\n");
            writer.write("Release Key " + relk + ": " + relName + "\n\n");
            writer.close();
        } catch (IOException e) {
            logger.severe(e.getMessage());
        }

        // and print, for this is needed in many tests
        StringBuilder buffer = new StringBuilder(500);
        buffer.append("Uploaded trigger configuration\n");
        buffer.append("==============================\n");
        buffer.append("Names:\n");
        buffer.append("Configuration : ").append(configName).append("\n");
        buffer.append("L1 prescales  : ").append(l1psName).append("\n");
        buffer.append("HLT prescales : ").append(hltpsName).append("\n");
        buffer.append("Release       : ").append(relName).append("\n\n");
        buffer.append("Keys:\n");
        buffer.append("--- UPLOAD Supermasterkey  : ").append(smk).append("\n");
        buffer.append("--- UPLOAD Release         : ").append(relk).append("\n");
        buffer.append("--- UPLOAD LVL1prescalekey : ").append(_l1psk).append("\n");
        buffer.append("--- UPLOAD HLTprescalekey  : ").append(_hltpsk).append("\n");
        logger.info(buffer.toString());
    }

    /**
     * Set the rel DB id.
     *
     * @param relId
     */
    public void setRelease(final int relId) {
        this.releaseId = relId;
    }

    /**
     * The the release name and clear release Id.
     *
     * @param relName the Release identifier. e.g. "17.0.x"
     */
    public void setRelease(final String relName) {
        this.release = relName;
        this.releaseId = -1;
    }

    /**
     * Sets the id of the L1 Master Table.
     *
     * @param l1Id the db L1 Master table id.
     */
    public void setL1Id(final int l1Id) {
        this.l1Id = l1Id;
    }

    /**
     * Sets the l1 xml file and the L1 Master Id to -1;
     *
     * @param l1xml the l1 xml file name.
     */
    public void setL1xml(final String l1xml) {
        this.lvl1menuXML = l1xml;
        this.l1Id = -1;
    }
    
    /**
     * Sets the id of the Topo Master Table.
     *
     * @param topoId the db Topo Master table id.
     */
    public void setTopoId(final int topoId) {
        this.topoId = topoId;
    }

    /**
     * Sets the topo xml file and the Topo Master Id to -1;
     *
     * @param topoxml the topo xml file name.
     */
    public void setTopoxml(final String topoxml) {
        this.topomenuXML = topoxml;
        this.topoId = -1;
    }

    /**
     *
     * @param hLTid
     */
    public void setHLTid(final int hLTid) {
        this.hltId = hLTid;
    }

    /**
     *
     * @param hltMenu
     * @param hltsetup
     */
    public void setHLTfiles(final String hltMenu, final String hltsetup) {
        this.hltmenuXML = hltMenu.trim();
        this.hltSetupXML = hltsetup.trim();
        this.hltId = -1;
    }

    /**
     * Check that we have all info needed for upload. We need a - release - L1 -
     * HLT is not needed, we can create a dummy one.
     *
     * @return
     * <code>true</code> if ok to upload.
     */
    private boolean checkUploadData() {
        if (this.releaseId <= 0 && this.release.isEmpty() ) {
            logger.severe("No release ID or filename specified.");
            return false;
        } 
        
        if (this.topoId <= 0 && topomenuXML.isEmpty()) {
            logger.severe("No Topo ID or menu filename specified.");
            return false;
        }

        // CTP menu upload possible?
        if(maxUploadLevel>=1) {
            if (this.l1Id<=0 && lvl1menuXML.isEmpty()) {
                logger.severe("No Level 1 ID or menu filename specified.");
                return false;
            }
        }
  
        if(maxUploadLevel==2) {
            if (hltId < 0 && hltmenuXML.isEmpty()) {
                logger.severe("No HLT ID or menu filename specified.");
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param supermastername
     */
    public void setConfigurationName(final String supermastername) {
        this.confName = supermastername;
    }

    /**
     *
     * @param offline
     */
    public void setOffline(boolean offline) {
        this.offlineConfig = offline;
    }

    /**
     *
     * @param online
     */
    public void setOnline(boolean online) {
        this.onlineConfig = online;
    }
    
    /**
     *
     * @param maxUploadLevel
     */
    public void setMaxUploadLevel(int maxUploadLevel) {
        this.maxUploadLevel = maxUploadLevel;
    }

    /**
     * @return the hltPsk
     */
    public int getHltPsk() {
        return hltPsk;
    }

    /**
     * @param hltPsk the hltPsk to set
     */
    public void setHltPsk(int hltPsk) {
        this.hltPsk = hltPsk;
    }

    /**
     * @return the l1Psk
     */
    public int getL1Psk() {
        return l1Psk;
    }

    /**
     * @param l1Psk the l1Psk to set
     */
    public void setL1Psk(int l1Psk) {
        this.l1Psk = l1Psk;
    }
    
}
