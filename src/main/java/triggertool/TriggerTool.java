package triggertool;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.DBConnections;
import triggerdb.Connections.DBTechnology;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.InternalWriteLock;
import triggerdb.Entities.L1.L1Prescale;
import triggerdb.Entities.SMT.SuperMasterTable;
import static triggertool.Auxiliary.ByteToHex.getHexString;
import triggertool.Auxiliary.LoggingWindowHandler;
import triggertool.BackEnd.XMLUploader;
import triggertool.Panels.InitDialog;
import triggertool.XML.ConfigurationException;
import triggertool.XML.HLT_DBtoXML;
import triggertool.XML.L1_DBtoXML;
import triggerdb.UploadException;
import triggertool.XML.XMLprescaleUploaderCLI;

/**
 * The trigger tool entry class.
 *
 * @author Simon Head
 * @author Paul Bell
 */
public final class TriggerTool {

    /**
     * Minimum compatible Trigger DB Schema. Note: - schema 10 for XS thresholds
     * - Schema 11 for Prescale set alias support.
     */
    public static final int TRIGGER_DB_MIN_SCHEMA = 8;
    /**
     * Constant DB doen't support XS
     */
    public static final int TRIGDB_NO_XS_SUPPORT = -1;
    /**
     * Constant DB doen't support ALIAS
     */
    public static final int TRIGDB_NO_ALIAS_SUPPORT = -2;
    /**
     * Message Logger setup here.
     */
    public static final Logger logger = Logger.getLogger("TriggerTool");
    /**
     * Adds a WindowHandler
     *
     */
    private static LoggingWindowHandler windowhandler;
    /**
     * Holds the database connection parameters.
     */
    private static final InitInfo theInitInfo = new InitInfo();
    /**
     * command line mode (property of the TriggerTool)
     */
    private static boolean commandLineMode = false;
    
    /**
     *
     */
    public static String logFileName;
    /**
     *
     * Version numbers of the trigger tool. - For deploy version same as the tag
     * - For dev version equals "trunk". The version is retrieved from file:
     * triggertool/version.properties.
     */
    public static final String TT_TAG = ReadTriggerToolVersion.getString("ConnectionManager.TT_TAG");

    /**
     * private static boolean markReplicate = false;
     *
     * @param args the arguments passed the the trigger tool
     * @return a CommandLine object with the parsed arguments.
     */
    private static CommandLine parseArguments(final String[] args) {
        CommandLine cmd = null;
        Options options = new Options();

        try {
            CommandLineParser parser = new PosixParser();
            options.addOption("v", "version", false, "Show version and exit.");
            options.addOption("tDb", "testDb", false, "Boolean flag, if set the"
                    + " TriggerTool will ONLY test the connection to the DB, and not perform any action.");
            // Connection strings may be e.g.:
            // oracle://ATLAS_CONFIG/ATLAS_CONF_TRIGGER -use dblookup, or full
            // spec:
            // oracle://atonr;schema=ATLAS_CONF_TRIGGER_V2;user=ATLAS_CONF_TRIGGER_V2_R;password=<pw>;
            // or ATLR, INTR, MC followed by the pass argument
            options.addOption("db", "dbConn", true, "DB connection string");
            options.addOption("pw", "passwd", true,
                    "Connect to ATONR, ATLR, INTR or MCDB with this password");
            options.addOption("user", "user", true,
                    "Set the username for the online TriggerDB (ATONR). "
                    + "If not present system user will be used.");

            // option for prescale set upload
            options.addOption("psup", "prescale_upload", true,
                    "Upload prescale sets from XML files.\n"
                    + " Argument can be a file, a directory or a path with "
                    + "wildcards.");
            // options for xml upload
            options.addOption("up", "upload", false,
                    "Upload XML (requries xml files and DB connection)");
            options.addOption("rel", "release", true, "HLT");
            options.addOption("l1topo", "topo_menu", true,
                    "L1 Topo XML file to upload to DB");
            options.addOption("l1", "l1_menu", true,
                    "L1 XML file to upload to DB");
            options.addOption("hlt", "hlt_menu", true,
                    "HLT Menu file to upload to db");
            options.addOption("efs", "hlt_setup", true,
                    "HLT Setup file to upload to DB");
            options.addOption("n", "name", true, "Name of the configuration");
            options.addOption("L1psn", "L1PS_name", true, "Name of the L1 PSS");
            options.addOption("HLTpsn", "HLTS_name", true,
                    "Name of the HLT PSS");

            options.addOption("onl", "onlineconfig", false,
                    "A configuration for use online");
            options.addOption("off", "offlineconfig", false,
                    "A configuration for use offline (MC)");
            options.addOption("mr", "markreplicate", false,
                    "Mark this for replication");
            
            options.addOption("m", "SMcomment", true,
                    "Comment for SM");


            // options for xml download
            options.addOption("down", "download", false,
                    "Download XML (requries keys and DB connection)");
            options.addOption("k", "smk", true,
                    "The supermaster key for xml download, prescale set upload"
                    + " or auto displaying");
            options.addOption("l1psk", "L1PSkeys", true,
                    "L1 prescale key(s) to be downloaded");
            options.addOption("hltpsk", "HLTPSkeys", true,
                    "HLT prescale key(s) to be downloaded");

            // further options
            options.addOption("p1TT", "onlineTT", false,
                    "Connecting TT at P1 -> should be removed, since we can"
                    + " check if ATONR");
            options.addOption("p1DB", "onlineDB", false,
                    "Connecting DB at P1 -> should be removed since we can"
                    + " check hostname");
            options.addOption("h", "help", false, "Display this message");

            // ACTIONS


            // get keys for a given release - needed for DBRelease check, makes
            // a file and prints to screen
            options.addOption("gk", "getkeys", true,
                    "Get all keys compatible for given release");

            options.addOption("show_l1ps", false, "Display the list of L1 "
                    + "prescale sets linked to a menu (Requires -smk and -db).");
            options.addOption("show_hltps", false, "Display the list of HLT "
                    + "prescale sets linked to a menu (Requires -smk and -db).");
            // Diffing
            options.addOption("diff", "doDiff", false,
                    "Get differences between two super master tables");
            options.addOption("smk1", "SMKey1", true,
                    "Super master key 1 for the diffing");
            options.addOption("smk2", "SMKey2", true,
                    "Super master key 2 for the diffing");
            options.addOption("xml", "XmlFileName", true,
                    "File name for diff xml output");

            // output file and level
            options.addOption("o", "output", true,
                    "Output filename or 'screen' or 'null' "
                    + "(default is 'screen')");
            options.addOption("l", "outputLevel", true,
                    "Output level [SEVERE|WARNING|INFO|CONFIG|FINE|FINER]"
                    + " (default is INFO)");

            // General options
            // l1 prescale int/fractional
            options.addOption("l1psInt", "l1psInteger", false,
                    "forces l1 prescales to integers, by default false");
            // Shoud we retry if we find loked DB.            
            options.addOption("w_n", "wait_n", true, "Number of times TriggerTool waits and retries to lock the DB.\n DEFAULT=1, i.e. only one try.");
            options.addOption("w_t", "wait_t", true, "Time in seconds to wait between consecutive writing tries.\n DEFAULT=3 secs.");


            cmd = parser.parse(options, args);

        } catch (ParseException ex) {
            logger.log(Level.SEVERE, "Unknown option{0}", ex.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("TriggerTool", options);
            System.exit(-1);
        }

        // Show Help
        if (cmd != null && cmd.hasOption("h")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("TriggerTool", options);
            System.exit(0);
        }
        return cmd;
    }

    /**
     *
     * @return
     */
    static public boolean isCommandLineMode() {
        return commandLineMode;
    }

    /**
     * @return the windowhandler
     */
    public static LoggingWindowHandler getWindowhandler() {
        return windowhandler;
    }

    /**
     * @param aWindowhandler the windowhandler to set
     */
    public static void setWindowhandler(LoggingWindowHandler aWindowhandler) {
        windowhandler = aWindowhandler;
    }

    static class RunWhenShuttingDown extends Thread {

        final Logger logger;

        public RunWhenShuttingDown(final Logger logger) {
            this.logger = logger;
        }

        // runs when shutting down (exit or ctrl-c or kill term)
        // -- The program exits normally, when the last non-daemon
        // thread exits or when the exit (equivalently, System.exit)
        // method is invoked, or
        // -- The virtual machine is terminated in response to a user
        // interrupt, such as typing ^C, or a system-wide event, such
        // as user logoff or system shutdown. (kill -13 does not call this)
        @Override
        public void run() {
            // we use the logger and sys out in here, since we do want it on
            // screen for sure
            String msg = "TT shutting down...";
            String perfMsg = " Performance Connection Manager forceLoad() : ";
            perfMsg += "\n Number of calls : " + ConnectionManager.HITS;
            perfMsg += "\n T prepare statements      : " + ConnectionManager.T0 / 1e3 + " sec";
            perfMsg += "\n T execute queries         : " + ConnectionManager.T1 / 1e3 + " sec";
            perfMsg += "\n T loop over rset and load : " + ConnectionManager.T2 / 1e3 + " sec";
            msg += "\n" + perfMsg;
            logger.info(msg);
            //System.out.println(msg);

            // try to disconnect form db. This will try to release possible
            // write locks.
            ConnectionManager mgr = ConnectionManager.getInstance();
            boolean op = mgr.isConnectionOpen();
            if (op && mgr.getInitInfo() != null) {
                try {
                    mgr.disconnect();
                } catch (SQLException ex) {
                    msg = " Error disconnecting from the database:\n"
                            + ex.getLocalizedMessage();
                    logger.severe(msg);
                    // Send to std err. If this happens we want the user to
                    // read it.
                    System.err.println(msg);
                }
            }
        }
    }

    /**
     * Entry point for running the TriggerTool.
     *
     * @param args Arguments
     * @throws SQLException To catch any database errors
     * @throws IOException To catch any io errors
     */
    public static void main(String[] args) throws SQLException, IOException {

        // Turn antialiasing on for text
        System.setProperty("awt.useSystemAAFontSettings","lcd");
        
        Runtime.getRuntime().addShutdownHook(new RunWhenShuttingDown(logger));

        TriggerTool boot = new TriggerTool();

        CommandLine cmd = parseArguments(args);

        if (cmd.hasOption("v")) {
            logger.log(Level.INFO, "TriggerTool version: {0}\n", TT_TAG);
            System.exit(0);
        }

        StringBuilder sb = new StringBuilder(300);
        sb.append("java -jar TriggerTool.jar ");
        for(String s: args) {
            sb.append(s).append(' ');
        }
        logger.log(Level.INFO,sb.toString());
        
        // these options cause commandline mode
        commandLineMode = (cmd.hasOption("diff")
                || cmd.hasOption("gk")
                || cmd.hasOption("down")
                || cmd.hasOption("r")
                || cmd.hasOption("psup")
                || cmd.hasOption("up")
                || cmd.hasOption("g")
                || cmd.hasOption("tDb")
                || cmd.hasOption("show_l1ps")
                || cmd.hasOption("show_hltps"));

        boot.setupLogger(cmd, commandLineMode);
        logger.log(Level.INFO, " COMMAND LINE MODE? {0}", commandLineMode);

        // set the username to the system user name
        theInitInfo.setSystemUsername(System.getProperty("user.name"));

        // L1 Prescales format
        if (cmd.hasOption("l1psInt")) {
            logger.warning(" Option \"l1psInt\" selected.\n"
                    + " Forcing L1 prescales to be integers.");
        }

        // Running online TT (at P1)
        boolean online = cmd.hasOption("p1TT");
        theInitInfo.setOnline(online);

        // Connecting to online DB or its replica
        boolean onlineDB = cmd.hasOption("p1DB");
        theInitInfo.setOnlineDB(onlineDB);

        // try to parse smk as int
        if (cmd.hasOption("smk")) {
            try {
                Integer.parseInt(cmd.getOptionValue("smk"));
            } catch (NumberFormatException e) {
                logger.log(Level.SEVERE, "Bad smk: {0} (not an integer)", cmd.getOptionValue("smk"));
                System.exit(-1);
            }
        }


        // TODO (tperez, 20120827) Alias manipulation and db connections setting
        // up should go to DBConnections!
        // get the DB Connection and tread some special shortcuts (COOL aliases
        // are not treated here, that is done in InitInfo)
        String dbConn = null;
        if (cmd.hasOption("db")) {
            dbConn = cmd.getOptionValue("db");
            // Connection at P1
            if (dbConn.equalsIgnoreCase("ATONR")) {
                if (cmd.hasOption("pw")) {
                    String onlinePWD = cmd.getOptionValue("pw").trim();
                    String onlineUSR = System.getProperty("user.name");
                    if (cmd.hasOption("user")) {
                        onlineUSR = cmd.getOptionValue("user").trim();
                    }
                    theInitInfo.setOnlineUserPassword(onlinePWD);
                    theInitInfo.setSystemUsername(onlineUSR);
                    DBConnections.setAtonrWriter(theInitInfo);

                } else {
                    logger.severe(" Please provide your pwd for the TriggerDB "
                            + "with -pw.");
                    System.exit(-1);
                }

                // treat some special short-cuts (that are there for simplicity)
                // - special cases are: ATLR, DEV1, DEV2, INTR(=DEV2), or MCDB
                // - if a password is given , the write account is assumed,
                // otherwise the reader account pw will be looked up
            } else if (dbConn.equals("ATLR") || dbConn.equals("DEV1")
                    || dbConn.equals("DEV2") || dbConn.equals("INTR")
                    || dbConn.equals("MCDB")) {

                if (dbConn.equals("ATLR")) {
                    dbConn = "oracle://ATLAS_CONFIG/ATLAS_CONF_TRIGGER_V2";
                }
                if (dbConn.equals("DEV1")) {
                    dbConn = "oracle://INTR/ATLAS_CONF_DEV1";
                }
                if (dbConn.equals("INTR") || dbConn.equals("DEV2")) {
                    dbConn = "oracle://INTR/ATLAS_CONF_DEV2";
                }
                if (dbConn.equals("MCDB")) {
                    dbConn = "oracle://ATLAS_CONFIG/ATLAS_CONF_TRIGGER_MC";
                }

                if (cmd.hasOption("pw")) {
                    String passwd = cmd.getOptionValue("pw");
                    String writer = dbConn.split("/")[3] + "_W";
                    dbConn += ";username=" + writer + ";";
                    dbConn += "password=" + passwd;
                }
            }
        }

        // check DB connection and exit
        if (cmd.hasOption("tDb")) {
            boot.checkDbConnection(cmd, dbConn);
        }

        // Set lock DB retry iterations and waiting time.
        if (commandLineMode) {
            
            if (dbConn == null) {
                logger.severe("No DB conection specified!");
                System.exit(1);
            } else if (!dbConn.equalsIgnoreCase("atonr")) {
                try {
                    theInitInfo.setConnectionFromCommandLine(dbConn);
                } catch (Exception ex) {
                    System.exit(-1);
                }
            }
            
            if (cmd.hasOption("w_n")) {
                int n = 1;
                try {
                    n = Integer.parseInt(cmd.getOptionValue("w_n").trim());
                    if (n < 1) {
                        n = 1;
                    }
                } catch (java.lang.NumberFormatException e) {
                    logger.severe(e.getMessage());
                }
                InternalWriteLock.setTRY_LOCKDB_N(n);
            }
            if (cmd.hasOption("w_t")) {
                Integer t = 3;
                try {
                    t = Integer.valueOf(cmd.getOptionValue("w_t").trim());
                    if (t < 0) {
                        t = 0;
                    }
                } catch (java.lang.NumberFormatException e) {
                    logger.severe(e.getMessage());
                }
                InternalWriteLock.setTRY_LOCKDB_TIME(t);
            }
        }


        // Command line diff (outputs xml) and exit
        if (cmd.hasOption("diff")) {
            boot.runDiff(cmd);
        }

        // get keys for given release and exit
        if (cmd.hasOption("gk")) {
            boot.runGetKeys(cmd);
        }

        // display L1 ps sets
        if (cmd.hasOption("show_l1ps")) {
            boot.runShowPS(cmd);
        }

        // display HLT ps sets
        if (cmd.hasOption("show_hltps")) {
            boot.runShowPS(cmd);
        }

        // download menu and exit
        if (cmd.hasOption("down")) {
            boot.downloadMenu(cmd);
        }

        // upload prescale sets from xml file and exit
        if (cmd.hasOption("psup")) {
            boot.uploadPrescaleSet(cmd);
        }

        // upload complete configuration from xml files and exit
        // TODO move this to an external class.
        if (cmd.hasOption("up")) {
            try {
                boot.uploadConfiguration(cmd);
            } catch (    UploadException | ConfigurationException ex) {
                InternalWriteLock writeLock = InternalWriteLock.getInstance();
                writeLock.removeLock();
                logger.log(Level.SEVERE, null, ex);
            }
        }

        // This launches a InitDialog gui panel, gets db connection info,
        // connects to db and launches a new MainPanel.
        InitDialog.loadFromGui(online, onlineDB);

    }

    private void checkDbConnection(CommandLine cmd, final String dbConn) {
        boolean connected = false;
        if (cmd.hasOption("db")) {
            if (!dbConn.equalsIgnoreCase("atonr")) {
                try {
                    theInitInfo.setConnectionFromCommandLine(dbConn);
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "problems setting up the connection to: {0}\n.{1}", new Object[]{dbConn, ex});
                    System.exit(-1);
                }
            }
            String pwd = theInitInfo.getPassWord();
            String onlinePwd = theInitInfo.getOnlineUserPassword();
            String atonrRpwd = theInitInfo.getAtonrRPassword();
            String atonrWpwd = theInitInfo.getAtonrWPassword();

            String msg = "\n Testing connection to " + dbConn;
            msg += "\n Connection parameter: \n";
            msg += theInitInfo.toString() + "\n";
            try {
                final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
                String utf8 = Charset.forName("UTF8").name();

                messageDigest.reset();
                // pwd
                messageDigest.update(pwd.getBytes(utf8));
                byte[] resultByte = messageDigest.digest();
                pwd = getHexString(resultByte);
                //onlinePwd
                byte[] inByte = onlinePwd.getBytes(utf8);
                messageDigest.update(inByte);
                resultByte = messageDigest.digest();
                onlinePwd = getHexString(resultByte);
                // atonrRpwd
                inByte = atonrRpwd.getBytes(utf8);
                messageDigest.update(inByte);
                resultByte = messageDigest.digest();
                atonrRpwd = getHexString(resultByte);
                // atonrWpwd
                inByte = atonrWpwd.getBytes(utf8);
                messageDigest.update(inByte);
                resultByte = messageDigest.digest();
                atonrWpwd = getHexString(resultByte);
                msg += " PWD         = " + pwd + "\n";
                msg += " online  PWD = " + onlinePwd + "\n";
                msg += " atonr R PWD = " + atonrRpwd + "\n";
                msg += " atonr W PWD = " + atonrWpwd + "\n";
            } catch (NoSuchAlgorithmException e) {
                logger.warning(e.getLocalizedMessage());
            } catch (IllegalCharsetNameException e) {
                logger.warning(e.getLocalizedMessage());
            } catch (IllegalArgumentException e) {
                logger.warning(e.getLocalizedMessage());
            } catch (UnsupportedEncodingException e) {
                logger.warning(e.getLocalizedMessage());
            }
            msg += " SystemUserv = " + theInitInfo.getSystemUsername();
            msg += "\n\n Note: For security reasons pwd are not displayed "
                    + "in clear text but as MD5 hash. You can test the "
                    + "diplayed against your pwd with the command :\n"
                    + " $> \"echo -n 'text to be encrypted' | openssl md5\""
                    + "\n";
            logger.info(msg);
            try {
                connected = connect();
            } catch (SQLException e) {
                logger.severe(" Problems connecting to this DB.");
            }
        }
        logger.log(Level.INFO,"\n *************************************************\n"
                + " ***   CONNECTED TO {0} / {1} : {2}\n *************************************************\n", new Object[]{theInitInfo.getdbHost(), theInitInfo.getUserName(), connected});
        System.exit(-1);
    }

    private void runDiff(CommandLine cmd) throws SQLException {
        // Get the SM keys
        Integer smk1 = null;
        Integer smk2 = null;
        if (cmd.hasOption("smk1") && cmd.getOptionValue("smk1") != null) {
            try {
                smk1 = Integer.parseInt(cmd.getOptionValue("smk1"));
            } catch (NumberFormatException ex) {
                logger.log(Level.SEVERE, "One of the SM keys is not a number: {0}", cmd.getOptionValue("smk1"));
                System.exit(1);
            }
        } else {
            logger.severe("You need to specfiy SuperMaster key 1 ID with"
                    + " -smk1 id");
            System.exit(1);
        }
        if (cmd.hasOption("smk2") && cmd.getOptionValue("smk2") != null) {
            try {
                smk2 = Integer.parseInt(cmd.getOptionValue("smk2"));
            } catch (NumberFormatException ex) {
                logger.log(Level.SEVERE, "One of the SM keys is not a number: {0}", cmd.getOptionValue("smk2"));
                System.exit(1);
            }
        } else {
            logger.severe("You need to specfiy SuperMaster key 2 ID with"
                    + " -smk2 id");
            System.exit(1);
        }

        // Get the output filename
        String xmlfilename = null;
        if (cmd.hasOption("xml") && cmd.getOptionValue("xml") != null) {
            xmlfilename = cmd.getOptionValue("xml");
        } else {
            logger.severe("You need to specify an output file name for the"
                    + " xml file produced by the diff with -xml"
                    + " filename.xml");
            System.exit(1);
        }

        // / Connect to db
        if (!connect()) {
            logger.severe("Connection to DB failed");
            System.exit(1);
        }

        // Now we have the keys & file name, run the diff
        SuperMasterTable smt1 = new SuperMasterTable(smk1);
        SuperMasterTable smt2 = new SuperMasterTable(smk2);
        triggertool.Diff.DiffTree tree = new triggertool.Diff.DiffTree();
        tree.update(smt1, smt2);
        tree.outputXML(xmlfilename);
        System.exit(0);
    }

    /**
     * get keys for given release
     *
     * @param cmd the arguments passed to the TriggerTool.
     * @throws SQLException problems accessing the DB.
     */
    private void runGetKeys(CommandLine cmd) throws SQLException {

        String relname = cmd.getOptionValue("gk");
        logger.log(Level.INFO, "Getting all keys for release {0}", relname);
        // don't do anything with any panels
        if (!connect()) {
            logger.warning("Connection to  DB failed");
        } else {
            ConnectionManager cmgr = ConnectionManager.getInstance();
            // get the release - hard since by name not db id!
            String query = "SELECT HRE_ID FROM HLT_RELEASE WHERE HRE_NAME=?";
            query = cmgr.fix_schema_name(query);
            int relid = -1;
            try {
                PreparedStatement ps = cmgr.getConnection().prepareStatement(query);
                ps.setString(1, relname);
                ResultSet rset = ps.executeQuery();
                if (rset.next()) {
                    relid = rset.getInt("HRE_ID");
                }
                rset.close();
                ps.close();
            } catch (SQLException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
            if (!(relid < 0)) {
                // Get that release and its SMK ids
                HLTRelease rel = new HLTRelease(relid);
                ArrayList<Integer> smks_v = rel.getSMKIDs();
                // System.out.println("GETKEYS nSMK " + smks_v.size());
                for (Integer smk_id : smks_v) {
                    SuperMasterTable smk = new SuperMasterTable(smk_id);
                    for (L1Prescale l1pss : smk.get_l1_master_table().get_menu().getPrescaleSets()) {
                    }
                    for (HLTPrescaleSet hltpss : smk.get_hlt_master_table().get_menu().getPrescaleSets()) {
                    }
                }
                // now get the prescales for these!
            }
        }
        System.exit(0);
    }

    /**
     * Retrieves and displays the prescale sets linked to a SMK.
     *
     * @param cmd
     */
    private void runShowPS(CommandLine cmd) throws SQLException {
        String optSMK = "smk";
        String optDB = "db";
        int smk = -1;
        // Ignore if not in command line mode.
        if (!TriggerTool.commandLineMode) {
            return;
        }
        // Break if missing options.         
        if (!cmd.hasOption(optSMK) || !cmd.hasOption(optDB)) {
            String msg = " Missing option! \n"
                    + " please provide a database connection \"-db\") "
                    + "and a super master table with \"-smk\".";
            logger.severe(msg);
            System.exit(-1);
        } else {
            String strSMK = cmd.getOptionValue(optSMK);
            try {
                smk = Integer.parseInt(strSMK);
            } catch (NumberFormatException e) {
                String msg = "Invalid super master key (" + strSMK + ")";
                logger.log(Level.SEVERE, "Invalid super master key ({0})", strSMK);
                System.exit(-1);
            }
        }

        boolean connected = this.connect();
        if (!connected) {
            logger.log(Level.SEVERE, " Problems connecting to db {0}.", cmd.getOptionValue(optDB));
            System.exit(-1);
        }
        //
        //// Show Prescale Sets        
        SuperMasterTable smt = new SuperMasterTable(smk);
        String msg = "";
        if (cmd.hasOption("show_l1ps")) {
            List<L1Prescale> sets = smt.get_l1_master_table().get_menu().getPrescaleSets();
            msg = " L1 Prescale Sets in Super Master Table " + smt.toString() + " : \n";
            for (L1Prescale set : sets) {
                msg += set + "\n";
            }
        } else if (cmd.hasOption("show_hltps")) {
            List<HLTPrescaleSet> sets = smt.get_hlt_master_table().get_menu().getPrescaleSets();
            msg = " HLT Prescale Sets in Super Master Table " + smt.toString() + " : \n";
            if (sets.isEmpty()) {
                msg += " NONE.\n";
            } else {
                for (HLTPrescaleSet set : sets) {
                    msg += set + "\n";
                }
            }
        }
        logger.info(msg);
        System.exit(0);
    }

    /**
     * Download a trigger menu. Requires SMK, L1PSK. Optional: HLTPSK
     *
     * @param cmd
     * @throws SQLException
     */
    private void downloadMenu(CommandLine cmd) throws SQLException {
        int superMasterKey = -1;
        int l1psk = -1;
        String opSMK = "k";
        String opL1PSK = "l1psk";
        // Check the options are there.
        if (!cmd.hasOption(opSMK) || !cmd.hasOption(opL1PSK)) {
            String msg = "Missing option.\n"
                    + " The Following options are mandatory:\n"
                    + " \t \"-smk\"    : The super master key.\n"
                    + " \t \"-l1psk\"  : A valid L1 Prescale Set Key.\n";
            msg += " The following option is optional:\n"
                    + " \t \"-hltpsk\" : A valid HLT Prescale Key (only if you want to download the HLT menu).\n\n";
            logger.severe(msg);
            //lease provide a Super Master Key with the option "	    
            System.exit(-1);
        }

        // Check the options are valid and set the keys
        try {
            superMasterKey = Integer.parseInt(cmd.getOptionValue(opSMK));
        } catch (NumberFormatException e) {
            logger.log(Level.SEVERE, " Invalid SuperMasterKey ({0}).", cmd.getOptionValue(opSMK));
            System.exit(-1);
        }

        try {
            l1psk = Integer.parseInt(cmd.getOptionValue(opL1PSK));
        } catch (NumberFormatException e) {
            logger.log(Level.SEVERE, " Invalid L1 Prescale Set Key ({0}).", cmd.getOptionValue(opL1PSK));
            System.exit(-1);
        }

        int hltPsKey = -1;
        if (cmd.hasOption("hltpsk")) {
            hltPsKey = Integer.parseInt(cmd.getOptionValue("hltpsk"));
            logger.log(Level.INFO, "Downloading L1 and HLT xmls for {0} {1} {2}", new Object[]{superMasterKey, l1psk, hltPsKey});
        } else {
            logger.log(Level.INFO, "Downloading L1 xmls only for {0} {1}", new Object[]{superMasterKey, l1psk});
        }

        // don't do anything with any panels, just download the xml
        if (!connect()) {
            logger.warning("Connection to  DB failed");
        } else {
            SuperMasterTable smt = new SuperMasterTable(superMasterKey);
            // write L1
            L1_DBtoXML l1Writer = new L1_DBtoXML();
            l1Writer.writeXML(smt.get_l1_master_table_id(), l1psk,
                    "LVL1Menu_" + superMasterKey + "_" + l1psk + ".xml", -1, false);

            if (hltPsKey > 0) {
                // write HLT
                Integer masterTableId = smt.get_hlt_master_table_id();
                HLT_DBtoXML hltWriter = new HLT_DBtoXML(masterTableId,
                        hltPsKey);
                String menu = "HLTMenu_" + superMasterKey + "_" + hltPsKey + ".xml";
                String release = "HLTReleases_" + superMasterKey + ".xml";
                String setup = "HLTSetup_" + superMasterKey + ".xml";
                hltWriter.direct_write(smt, menu, release, setup);
            }
        }
        System.exit(0);
    }

    /**
     * load prescale sets from file.
     *
     * @param cmd The arguments passed to the TriggerTool.
     * @throws SQLException thrown is something goes wrong saving to DB.
     */
    private void uploadPrescaleSet(CommandLine cmd) throws SQLException {
        // Need a SupermasterKey to upload prescale sets to.
        int smk = -1;
        if (!cmd.hasOption("smk")) {
            logger.severe(" Please provide a SuperMasterKey with the "
                    + "option -smk INT");
            System.exit(-1);
        } else {
            String key = cmd.getOptionValue("smk");
            try {
                smk = Integer.parseInt(key);
            } catch (NumberFormatException e) {
                logger.log(Level.SEVERE, " Wrong format for the Super Master Key \"{0}\".\n Please use one int as parameter.", key);
                System.exit(-1);
            }
        }

        String filename = cmd.getOptionValue("psup");
        // Connect to DB.
        connect();
        // load the xml file to the given super master table.
        XMLprescaleUploaderCLI.load(filename, smk);
        //
        System.exit(0);
    }

    /**
     * upload complete configuration from xml files and exit.
     *
     * @param cmd the command line arguments.
     */
    private void uploadConfiguration(final CommandLine cmd)
            throws SQLException, IOException, UploadException, ConfigurationException {
        // Try to connect. if fails it will exit via exception.        
        if (!this.connect()) {
            System.exit(-1);
        }
        logger.info("Successfully connected, uploading xml");
        XMLUploader upXML = new XMLUploader(cmd);
        int smk = upXML.upload();
        System.exit(0);
    }

    /**
     * Sets up the different logging handlers based upon the command line
     * parameters given.
     *
     * @param cmd the command line argument given as input.
     * @param commandLineMode
     * <code>true</code> if running in command line mode.
     */
    private void setupLogger(final CommandLine cmd, boolean commandLineMode) {
        //System.out.println("HERE " + cmd.getOptionValue("o") + " " + cmd.getOptionValue("l"));
        // setup the TriggerTool logger
        Logger triggerDBLogger = Logger.getLogger("TriggerDb");

        
        logger.setUseParentHandlers(false);
        triggerDBLogger.setUseParentHandlers(false);
        String logOutput = cmd.getOptionValue("o", "screen");

        String logLevel = cmd.getOptionValue("l", "INFO").toUpperCase();

        if (logOutput.equals("null")) {
            logger.setLevel(Level.OFF);
            triggerDBLogger.setLevel(Level.OFF);
            return;
        }

        //INFO is the default level
        Level logLvl = Level.INFO;

        if (logLevel.equals("SEVERE")) {
            logLvl = Level.SEVERE;
        } else if (logLevel.equals("WARNING")) {
            logLvl = Level.WARNING;
        } else if (logLevel.equals("INFO")) {
            logLvl = Level.INFO;
        } else if (logLevel.equals("CONFIG")) {
            logLvl = Level.CONFIG;
        } else if (logLevel.equals("FINE")) {
            logLvl = Level.FINE;
        } else if (logLevel.equals("FINER")) {
            logLvl = Level.FINER;
        } else if (logLevel.equals("FINEST")) {
            logLvl = Level.FINEST;
        } else {
            logger.log(Level.SEVERE, "Loglevel {0} unknown", cmd.getOptionValue("l"));
            System.exit(1);
        }

        // always console output (at least of errors)
        Handler handler = new ConsoleHandler(); // SimpleFormatter is default
        handler.setLevel(logLvl);
        logger.addHandler(handler);
        logger.setLevel(logLvl);
        triggerDBLogger.addHandler(handler);
        triggerDBLogger.setLevel(logLvl);

        if (!logOutput.equals("screen")) {
            // setup a FileHandler as well

            // first set the Console to print INFO, WARNINGS and ERRORS. 
            // should use StreamHandler for that
            handler.setLevel(Level.INFO);

            // turn %T in the filename into a date+time string
            String filename = logOutput;
            if (filename.lastIndexOf("%T") > filename.lastIndexOf('/')) {
                int timeIdx = filename.lastIndexOf("%T");
                Date now = new Date();
                SimpleDateFormat format = new SimpleDateFormat(
                        "yyyy_MM_dd_HH:mm:ss_EEE");
                String newFileName = filename.substring(0, timeIdx)
                        + format.format(now) + filename.substring(timeIdx + 2);
                filename = newFileName;
            }
            if (filename.lastIndexOf("%U") > filename.lastIndexOf('/')) {
                int userIdx = filename.lastIndexOf("%U");
                String newFileName = filename.substring(0, userIdx)
                        + System.getenv("USER")
                        + filename.substring(userIdx + 2);
                filename = newFileName;
            }
            File file = new File(filename);
            if (file.isDirectory() || filename.endsWith("/")) {
                String user = System.getenv("USER");
                SimpleDateFormat format = new SimpleDateFormat(
                        "yyyy_MM_dd_HH:mm:ss_EEE");
                String date = format.format(new Date());
                file = new File(filename, "TriggerTool_" + user + "_" + date
                        + ".log");
                filename = file.getPath();
            }
            if (file.exists() && file.isFile()) {
                File oldfile = new File(filename.concat(".old"));
                if (oldfile.exists()) {
                    oldfile.delete();
                }
                file.renameTo(oldfile);
            } else if (!file.exists()) {
                // check Dir exist
                String dirPath = file.getParent();
                if (dirPath == null) {
                    dirPath = "./";
                }
                File dir = new File(dirPath);
                if (!dir.exists()) {
                    try {
                        dir.mkdirs();
                    } catch (SecurityException e) {
                        logger.log(Level.SEVERE, "Error creating directory{0}", dirPath);
                    }
                }
                if (dir.exists()) {
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        logger.log(Level.SEVERE,"Error creating log file : {0}"
                                + ".\n --> Check that you have writing right"
                                + " for this path.", filename);
                    }
                }
            }
            try {
                logFileName = filename;
                handler = new FileHandler(logFileName, true);
                handler.setFormatter(new SimpleFormatter()); // text not xml
                handler.setLevel(Level.FINEST);
                logger.addHandler(handler);
                System.out.println(logFileName);
                triggerDBLogger.addHandler(handler);
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Could not open {0} for writing", logFileName);
            }

        }

        // in GUI mode we add the windowhandler
        if (!commandLineMode) {

            //Setup logging windowhandler
            setWindowhandler(LoggingWindowHandler.getInstance());

            //Adds the windowhandler to the logger
            logger.addHandler(getWindowhandler());
        }

        String msg = "Starting TriggerTool ver. ";
        msg += TT_TAG + ".";
        logger.info(msg);
    }

    /**
     * Connect to the Database. Form a connection to the database using
     * theInitInfo. This is used when connecting from command line - exits in
     * case of failing to connect
     *
     * @return True If connected.
     * @throws SQLException exception happens in while setting up the
     * connection.
     */
    private boolean connect() throws SQLException {

        boolean successfullConnection = false;
        try {
            successfullConnection = connect(theInitInfo);
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Could not connect. exception:\n{0}", ex.getLocalizedMessage());
            System.exit(-1);
        }
        if (!successfullConnection) {
            logger.severe("Could not connect.");
            System.exit(-1);
        }
        return true;
    }

    /**
     * Function connects to specified db.
     *
     * @param initinfo - contains db to connect to
     * @return true if connected OK.
     * @throws SQLException if there is an error setting up the connection.
     */
    private boolean connect(final InitInfo initinfo) throws SQLException {
        String msg = "Login as '" + initinfo.getUserMode() + "' into " + initinfo.getTechnology();
        msg += " db: " + initinfo.getdbServer()
                + " " + initinfo.getUserName();
        logger.info(msg);

        boolean successfullConnection =
                ConnectionManager.getInstance().connect(initinfo);
        return successfullConnection;
    }
}
