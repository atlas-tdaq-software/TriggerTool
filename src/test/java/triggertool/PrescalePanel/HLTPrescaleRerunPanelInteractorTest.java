/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.PrescalePanel;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import triggerdb.Entities.HLT.HLTPrescale;
import triggerdb.Entities.HLT.HLTPrescaleType;
import triggerdb.Entities.HLT.HLTTriggerChain;
import triggertool.Panels.PrescaleEditPanel.HLTPrescaleReRunRow;
import triggertool.Panels.PrescaleEditPanel.HLTPrescaleRerunPanelInteractor;
import triggertool.Panels.PrescaleEditPanel.HLTPrescaleRow;

/**
 *
 * @author Michele
 */
public class HLTPrescaleRerunPanelInteractorTest {
    
    private HLTPrescaleRerunPanelInteractor interactor;
    
    public HLTPrescaleRerunPanelInteractorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void CanCreateListOfReRunRows() {
        List<HLTPrescale> prescaleList = new ArrayList<HLTPrescale>();
        HLTPrescale p1 = new HLTPrescale();
        p1.set_value(1);
        p1.set_condition("Condition1");
        p1.set_type(HLTPrescaleType.ReRun);
        prescaleList.add(p1);
        
        HLTTriggerChain chain = new HLTTriggerChain();
        chain.set_name("chainName");
        chain.set_lower_chain_name("lowerChainName");
        
        HLTPrescaleRow mainTableRow = new HLTPrescaleRow(12, chain, prescaleList);
        
        interactor = new HLTPrescaleRerunPanelInteractor(mainTableRow);

        List<HLTPrescaleReRunRow> list = interactor.GetReRunRows();
        
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(1, Double.valueOf((String)list.get(0).get(0)), 0.001);
        Assert.assertEquals("Condition1", list.get(0).get(1));
        Assert.assertEquals(12, interactor.GetChainId());
    }
}
