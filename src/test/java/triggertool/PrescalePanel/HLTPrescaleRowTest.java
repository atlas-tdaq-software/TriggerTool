/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.PrescalePanel;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import triggerdb.Entities.HLT.HLTPrescale;
import triggerdb.Entities.HLT.HLTPrescaleType;
import triggerdb.Entities.HLT.HLTTriggerChain;
import triggertool.Panels.PrescaleEditPanel.HLTPrescaleRow;

/**
 *
 * @author Michele
 */
public class HLTPrescaleRowTest {
    
    HLTPrescaleRow row;
    List<HLTPrescale> list;
    HLTTriggerChain chain;
    
    public HLTPrescaleRowTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {        
        list = new ArrayList<>();
        HLTPrescale p1 = new HLTPrescale();
        p1.set_type(HLTPrescaleType.ReRun);
        p1.set_value(1);
        p1.set_condition("Condition1");
        list.add(p1);
        HLTPrescale p2 = new HLTPrescale();
        p2.set_type(HLTPrescaleType.Prescale);
        p2.set_value(2);
        list.add(p2);
        HLTPrescale p3 = new HLTPrescale();
        p3.set_type(HLTPrescaleType.Pass_Through);
        p3.set_value(3);
        list.add(p3);
        HLTPrescale p4 = new HLTPrescale();
        p4.set_type(HLTPrescaleType.Stream);
        p4.set_value(4);
        p4.set_condition("express");
        list.add(p4);
        
        chain = new HLTTriggerChain();
        chain.set_name("chainName");
        chain.set_lower_chain_name("lowerChainName");
        
        row = new HLTPrescaleRow(0, chain, list);
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void CanCreate() {        
        Assert.assertEquals("chainName", row.getName());
        Assert.assertEquals(0, (int)row.getCounter());
        Assert.assertEquals("express", row.getCondition());
        Assert.assertEquals(true, (boolean)row.getInOut());
        Assert.assertEquals("3", row.getPassThrough());
        Assert.assertEquals("2", row.getPrescale());
        Assert.assertEquals("lowerChainName", row.getSeed());
        Assert.assertEquals("1:Condition1;", row.getReRun());

        Assert.assertEquals(4, row.allPrescaleList.size());
        Assert.assertEquals(1, row.reRunList.size());
    }
    
    @Test
    public void CanReCalculateReRunField() {
        row.reRunList.get(0).set_value(10);
        row.reRunList.get(0).set_condition("Condition10");
        row.ReCalculateReRunField();
        Assert.assertEquals("10:Condition10;", row.getReRun());        
    }
    
    @Test
    public void EmptyStreamSetItToNAAndItIsNotEditable() {
        list.remove(3);
        row = new HLTPrescaleRow(0, chain, list);
        
        Assert.assertEquals("na", row.getCondition());        
        Assert.assertEquals("na", row.getStream());        

        row.setStream("10.1");
        row.setCondition("express");
        
        Assert.assertEquals("na", row.getCondition());        
        Assert.assertEquals("na", row.getStream());        
    }
}
