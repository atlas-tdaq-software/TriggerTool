/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.PrescalePanel;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import triggertool.Panels.PrescaleEditPanel.L1PrescaleEditItemNode;
import triggertool.Panels.PrescaleEditPanel.L1PrescaleTableModel;

/**
 *
 * @author Michele
 */
public class L1PrescaleTableModelTest {
    
    public L1PrescaleTableModelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void TableHasFourColumns() {
        L1PrescaleTableModel table = new L1PrescaleTableModel();
        assertEquals(4, table.getColumnCount());        
    }

    @Test
    public void OnlySecondAndForthColumnsAreEditable() {
        L1PrescaleTableModel table = new L1PrescaleTableModel();
        table.add(new L1PrescaleEditItemNode());
        assertTrue(!table.isCellEditable(0, 0));        
        assertTrue(table.isCellEditable(0, 1));        
        assertTrue(!table.isCellEditable(0, 2));        
        assertTrue(table.isCellEditable(0, 3));        
    }

    @Test
    public void SetInputModifyActualValueColumn() {
        L1PrescaleTableModel table = new L1PrescaleTableModel();
        L1PrescaleEditItemNode node = new L1PrescaleEditItemNode();
        node.setInput(1.0);
        table.add(node);
        assertEquals(1.0, table.getValueAt(0, 1));        
        assertEquals(1.0, table.getValueAt(0, 2));        
        
        table.setValueAt("10", 0, 1);
        assertEquals(10.0, table.getValueAt(0, 1));        
        assertEquals(10.0, (double)table.getValueAt(0, 2), 0.00001);
    }

    @Test
    public void ChangingSignDoesChangeBothValues() {
        L1PrescaleTableModel table = new L1PrescaleTableModel();
        L1PrescaleEditItemNode node = new L1PrescaleEditItemNode();
        node.setInput(1.0);
        table.add(node);
        assertEquals(1.0, table.getValueAt(0, 1));        
        assertEquals(1.0, table.getValueAt(0, 2));        
        
        table.setValueAt("0", 0, 3);
        assertEquals(-1.0, table.getValueAt(0, 1));        
        assertEquals(-1, (double)table.getValueAt(0, 2), 0.00001);
    }
}
