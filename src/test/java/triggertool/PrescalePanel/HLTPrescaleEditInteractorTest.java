/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.PrescalePanel;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import triggerdb.Entities.HLT.HLTPrescale;
import triggerdb.Entities.HLT.HLTPrescaleType;
import triggerdb.Entities.HLT.HLTTriggerChain;
import triggertool.Panels.PrescaleEditPanel.HLTPrescaleEditInteractor;
import triggertool.Panels.PrescaleEditPanel.HLTPrescaleRerunPanelInteractor;
import triggertool.Panels.PrescaleEditPanel.HLTPrescaleRow;

/**
 *
 * @author Michele
 */
public class HLTPrescaleEditInteractorTest {
    
    public HLTPrescaleEditInteractorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void EmptyTableReturnNull() {
        HLTPrescaleEditInteractor interactor = new HLTPrescaleEditInteractor(null, null);
        HLTPrescaleRerunPanelInteractor reRunInteractor = interactor.HandleOpenReRunTable(0, HLTPrescaleRow.RERUN);
        
        Assert.assertTrue(reRunInteractor == null);
    }

    @Test
    public void HandleTableClickWithOneReRunPrescale() {
        HLTPrescaleEditInteractor interactor = new HLTPrescaleEditInteractor(null, null);
        List<HLTPrescale> list = new ArrayList<HLTPrescale>();
        HLTPrescale p1 = new HLTPrescale();
        p1.set_type(HLTPrescaleType.ReRun);
        p1.set_value(1);
        p1.set_condition("Condition1");
        list.add(p1);

        HLTTriggerChain chain = new HLTTriggerChain();
        chain.set_name("chainName");
        chain.set_lower_chain_name("lowerChainName");
        
        HLTPrescaleRow row = new HLTPrescaleRow(0, chain, list);
        interactor.hltMainModel.add(row);
                
        HLTPrescaleRerunPanelInteractor reRunInteractor = interactor.HandleOpenReRunTable(0, HLTPrescaleRow.RERUN);
        
        Assert.assertEquals(1, reRunInteractor.GetReRunRows().size());
    }
    
    @Test
    public void ClickedOnCellDifferentFromReRunReturnNull()
    {
        HLTPrescaleEditInteractor interactor = new HLTPrescaleEditInteractor(null, null);
        List<HLTPrescale> list = new ArrayList<HLTPrescale>();
        HLTPrescale p1 = new HLTPrescale();
        p1.set_type(HLTPrescaleType.ReRun);
        p1.set_value(1);
        p1.set_condition("Condition1");
        list.add(p1);
        
        HLTTriggerChain chain = new HLTTriggerChain();
        chain.set_name("chainName");
        chain.set_lower_chain_name("lowerChainName");
        
        HLTPrescaleRow row = new HLTPrescaleRow(0, chain, list);
        interactor.hltMainModel.add(row);
                
        int columnIndex = HLTPrescaleRow.CONDITION;
        int rowIndex = 0;
        HLTPrescaleRerunPanelInteractor reRunInteractor = interactor.HandleOpenReRunTable(rowIndex, columnIndex);
        
        Assert.assertTrue(reRunInteractor == null);
    }
}
