/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.XML;

import triggertool.XML.Writers.XMLWriter_L1Item;
import java.util.ArrayList;
import org.junit.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.L1.L1Threshold;
import triggerdb.Entities.L1.L1ThresholdValue;

/**
 *
 * @author giannell
 */
public class XMLWriter_L1ItemTest {
    
    public XMLWriter_L1ItemTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of writeXML method, of class XMLWriter_L1Item.
     * @throws java.lang.Exception
     */
    @Test
    public void testWriteXML() throws Exception {
        //System.out.println("writeXML");
       
        L1Item item = new L1Item();
        item.set_partition(0);
        item.set_name("testName");
        item.set_definition("1&2&3");
        item.set_trigger_type("10000100");
        item.set_version(2);
        item.set_priority("1");
        item.set_ctp_id(0);
        
        L1ThresholdValue value1 = new L1ThresholdValue();
        value1.set_name("EM3");
        
        L1Threshold threshold = new L1Threshold();
        threshold.set_position(1);
        threshold.set_name("EM3");
        threshold.set_multiplicity(1);
        ArrayList<L1ThresholdValue> tvalues1 = new ArrayList<L1ThresholdValue>();
        tvalues1.add(value1);
        threshold.setThresholdValues(tvalues1);
        
        L1ThresholdValue value2 = new L1ThresholdValue();
        value2.set_name("BGRP0");
        
        L1Threshold threshold2 = new L1Threshold();
        threshold2.set_position(2);
        threshold2.set_name("BGRP0");
        threshold2.set_multiplicity(1);
        ArrayList<L1ThresholdValue> tvalues2 = new ArrayList<L1ThresholdValue>();
        tvalues2.add(value2);
        threshold.setThresholdValues(tvalues2);
             
        L1ThresholdValue value3 = new L1ThresholdValue();
        value3.set_name("BGRP1");
        
        L1Threshold threshold3 = new L1Threshold();
        threshold3.set_position(3);
        threshold3.set_name("BGRP1");
        threshold3.set_multiplicity(1);
        ArrayList<L1ThresholdValue> tvalues3 = new ArrayList<L1ThresholdValue>();
        tvalues3.add(value3);
        threshold.setThresholdValues(tvalues3);        
        
        ArrayList<L1Threshold> list = new ArrayList<L1Threshold>();
        list.add(threshold);
        list.add(threshold2);
        list.add(threshold3);
        
        item.setThresholds(list);
        
        XMLWriter_L1Item writer = new XMLWriter_L1Item(item);
        String output = writer.writeXML();
        String[] lines = output.split("\\r?\\n");
        
        String expectedLine1 = "    <TriggerItem ctpid=\"0\" partition=\"0\" name=\"testName\" complex_deadtime=\"1\" definition=\"EM3[x1]&amp;BGRP0&amp;BGRP1\" trigger_type=\"10000100\">";
        Assert.assertEquals(expectedLine1, lines[0]);
        Assert.assertEquals("      <AND>", lines[1]);
        Assert.assertEquals("        <TriggerCondition multi=\"1\" name=\"EM3_x1\" triggerthreshold=\"EM3\"/>", lines[2]);
        Assert.assertEquals("        <InternalTrigger name=\"BGRP0\"/>", lines[3]);
        Assert.assertEquals("        <InternalTrigger name=\"BGRP1\"/>", lines[4]);
        Assert.assertEquals("      </AND>", lines[5]);
        Assert.assertEquals("    </TriggerItem>", lines[6]);
        
    }
    
}
