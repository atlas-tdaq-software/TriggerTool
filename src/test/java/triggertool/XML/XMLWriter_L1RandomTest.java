/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.XML;

import triggertool.XML.Writers.XMLWriter_L1Random;
import org.junit.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import triggerdb.Entities.L1.L1Random;

/**
 *
 * @author giannell
 */
public class XMLWriter_L1RandomTest {
    
    L1Random random;
    
    public XMLWriter_L1RandomTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
         random = new L1Random();
         random.set_cut0(0);
         random.set_cut1(1);
         random.set_cut2(3);
         random.set_cut3(10);
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void CanWriteCorrectOutput() {        
        XMLWriter_L1Random writer = new XMLWriter_L1Random(random);
        String output = writer.Write();
        Assert.assertEquals("  <Random name0=\"Random0\" cut0=\"0\" name1=\"Random1\" cut1=\"1\" name2=\"Random2\" cut2=\"3\" name3=\"Random3\" cut3=\"10\" />\n", output);
    }
}
