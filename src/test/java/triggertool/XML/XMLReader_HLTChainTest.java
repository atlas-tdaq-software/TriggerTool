/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.XML;

import triggertool.XML.Readers.XMLReader_HLTChain;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.Assert;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import triggerdb.Entities.HLT.HLTPrescale;
import triggerdb.Entities.HLT.HLTPrescaleType;
/**
 *
 * @author Michele
 */
public class XMLReader_HLTChainTest {
    
    private XMLReader_HLTChain reader;
    private Document doc;
    
    public XMLReader_HLTChainTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void CanGetCorrectValues() throws ParserConfigurationException, SAXException, IOException {
        reader = new XMLReader_HLTChain();

        String xml = "<data><CHAIN EBstep=\"1\" chain_counter=\"170\" chain_name=\"HLT_e24_medium_iloose\" level=\"HLT\" lower_chain_name=\"L1_EM18VH\" pass_through=\"0\" prescale=\"1\" rerun_prescale=\"-1\"/></data>";
        Element element = GetElementFromXml(xml);
        
        ArrayList<HLTPrescale> psList = reader.CreatePrescales(element);
        
        Assert.assertEquals(3, psList.size());
        Assert.assertEquals((Double)1.0, psList.get(0).get_value());
        Assert.assertEquals((Double)0.0, psList.get(1).get_value());
        Assert.assertEquals((Integer)170, psList.get(0).get_chain_counter());
        Assert.assertEquals((Integer)170, psList.get(1).get_chain_counter());
        Assert.assertEquals(HLTPrescaleType.Prescale, psList.get(0).get_type());
        Assert.assertEquals(HLTPrescaleType.Pass_Through, psList.get(1).get_type());
        Assert.assertEquals("", psList.get(0).get_condition());
        Assert.assertEquals("", psList.get(1).get_condition());
    }

    private Element GetElementFromXml(String xml) throws IOException, ParserConfigurationException, SAXException {
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xml));
        Document document = db.parse(is);
        NodeList nodes = document.getElementsByTagName("CHAIN");
        return (Element) nodes.item(0);
    }
}
