/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.XML;

import triggertool.XML.Readers.XMLReader_HLTTriggerStream;
import java.io.IOException;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import triggerdb.Entities.HLT.HLTPrescale;
import triggerdb.Entities.HLT.HLTPrescaleType;
import triggerdb.Entities.HLT.HLTTriggerStream;

/**
 *
 * @author Michele
 */
public class XMLReader_HLTTriggerStreamTest {
    
    private XMLReader_HLTTriggerStream reader;
    
    public XMLReader_HLTTriggerStreamTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void CanCreateNormalStream() throws Exception {
        reader = new XMLReader_HLTTriggerStream();

        String xml = "<STREAMTAG obeyLB=\"yes\" prescale=\"1\" stream=\"Combined\" type=\"physics\"/>";
        Element element = GetElementFromXml(xml, "STREAMTAG");

        HLTTriggerStream s = reader.CreateHLTTriggerStream(element);
        Assert.assertEquals("physics", s.get_type());
        Assert.assertEquals("Combined", s.get_name());
        Assert.assertEquals((Integer)1, s.get_obeyLB());
        Assert.assertEquals("1", s.get_stream_prescale());
    }

    @Test
    public void CanCreateExpressStream() throws Exception {
        reader = new XMLReader_HLTTriggerStream();

        String xml = "<STREAMTAG obeyLB=\"yes\" prescale=\"1\" stream=\"express\" type=\"physics\"/>";
        Element element = GetElementFromXml(xml, "STREAMTAG");

        HLTTriggerStream s = reader.CreateHLTTriggerStream(element);
        Assert.assertEquals("physics", s.get_type());
        Assert.assertEquals("express", s.get_name());
        Assert.assertEquals((Integer)1, s.get_obeyLB());
        Assert.assertEquals("1", s.get_stream_prescale());
    }
       
    // This test can be used as an example for testing large parts of XML
    
    @Test
    public void CanGetCorrectStreamPrescaleValues() throws Exception {
        reader = new XMLReader_HLTTriggerStream();

        String xml = "<CHAIN chain_counter=\"170\" chain_name=\"HLT_e24_medium_iloose\" level=\"HLT\" lower_chain_name=\"L1_EM18VH\" pass_through=\"0\" prescale=\"1\" rerun_prescale=\"-1\">\n" +
"			<TRIGGERTYPE_LIST/>\n" +
"			<STREAMTAG_LIST>\n" +
"				<STREAMTAG obeyLB=\"yes\" prescale=\"1\" stream=\"express\" type=\"physics\"/>\n" +
"			</STREAMTAG_LIST>\n" +
"			<GROUP_LIST>\n" +
"				<GROUP name=\"RATE:SingleElectron\"/>\n" +
"				<GROUP name=\"BW:Egamma\"/>\n" +
"			</GROUP_LIST>\n" +
"			<SIGNATURE_LIST>\n" +
"				<SIGNATURE logic=\"1\" signature_counter=\"1\">\n" +
"					<TRIGGERELEMENT te_name=\"L2_e24_medium_iloose_L1_EM18VHcl\"/>\n" +
"				</SIGNATURE>\n" +
"				<SIGNATURE logic=\"1\" signature_counter=\"2\">\n" +
"					<TRIGGERELEMENT te_name=\"L2_e24_medium_iloose_L1_EM18VHid\"/>\n" +
"				</SIGNATURE>\n" +
"				<SIGNATURE logic=\"1\" signature_counter=\"3\">\n" +
"					<TRIGGERELEMENT te_name=\"L2_e24_medium_iloose_L1_EM18VH\"/>\n" +
"				</SIGNATURE>\n" +
"				<SIGNATURE logic=\"1\" signature_counter=\"4\">\n" +
"					<TRIGGERELEMENT te_name=\"EF_e24_medium_iloose_L1_EM18VHcalo\"/>\n" +
"				</SIGNATURE>\n" +
"				<SIGNATURE logic=\"1\" signature_counter=\"5\">\n" +
"					<TRIGGERELEMENT te_name=\"EF_e24_medium_iloose_L1_EM18VHid\"/>\n" +
"				</SIGNATURE>\n" +
"				<SIGNATURE logic=\"1\" signature_counter=\"6\">\n" +
"					<TRIGGERELEMENT te_name=\"EF_e24_medium_iloose_L1_EM18VH\"/>\n" +
"				</SIGNATURE>\n" +
"			</SIGNATURE_LIST>\n" +
"		</CHAIN>";
        
        Element element = GetElementFromXml(xml, "CHAIN");
        int chain_counter = Integer.parseInt(element.getAttribute("chain_counter"));
        element = GetElementFromXml(xml, "STREAMTAG");
        
        HLTPrescale p = XMLReader_HLTTriggerStream.CreatePrescales(element, chain_counter);
        Assert.assertEquals(HLTPrescaleType.Stream, p.get_type());
        Assert.assertEquals((Double)1.0, p.get_value());
        Assert.assertEquals((Integer)170, p.get_chain_counter());
        Assert.assertEquals("express", p.get_condition());
    }
    
    private Element GetElementFromXml(String xml, String tag) throws IOException, ParserConfigurationException, SAXException {
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xml));
        Document document = db.parse(is);
        NodeList nodes = document.getElementsByTagName(tag);
        return (Element) nodes.item(0);
    }

}
