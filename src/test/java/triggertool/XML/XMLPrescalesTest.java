/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.XML;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import triggertool.Panels.PrescaleEditPanel.L1PrescaleEditItemNode;

/**
 *
 * @author Michele
 */
public class XMLPrescalesTest {
    
    public XMLPrescalesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void CanCreate() {
        XMLprescales ps = new XMLprescales("");
    }
    
    @Test
    public void UpdateEmptyL1ReturnFalse() {
        XMLprescales ps = new XMLprescales("");
        boolean test = ps.updatePrescaleSetL1(new LinkedHashMap<Integer, L1PrescaleEditItemNode>(), true);
        assertEquals(false, test);
    }
        
    @Test
    public void UpdateL1ListOfOneWithSameElementReturnTrue() {
        XMLprescales ps = new XMLprescales("");
        ArrayList<Object> psVector = new ArrayList<Object>(2);
                psVector.add(0, 1);
                psVector.add(1, "Test");
        ps.prescaleSetMap.put("1", psVector);
        
        LinkedHashMap<Integer, L1PrescaleEditItemNode> link = new LinkedHashMap<Integer, L1PrescaleEditItemNode>();
        L1PrescaleEditItemNode node = new L1PrescaleEditItemNode();
        node.setId(1);
        node.setCutInIntFormat(1);
        node.setNodeName("Test");
        link.put(0, node);
        boolean test = ps.updatePrescaleSetL1(link, true);
        assertEquals(true, test);
    }
    
    @Test
    public void UpdateL1ListWithCTPIDNotInXMLWithDefaultValueSetPrescaleToMinusOne() {
        XMLprescales ps = new XMLprescales("");
        ArrayList<Object> psVector = new ArrayList<Object>(2);
                psVector.add(0, 1);
                psVector.add(1, "Test");
        ps.prescaleSetMap.put("1", psVector);
        
        LinkedHashMap<Integer, L1PrescaleEditItemNode> link = new LinkedHashMap<Integer, L1PrescaleEditItemNode>();
        L1PrescaleEditItemNode node = new L1PrescaleEditItemNode();
        node.setId(1);
        node.setCutInIntFormat(1);
        node.setNodeName("Test");
        L1PrescaleEditItemNode node2 = new L1PrescaleEditItemNode();
        node2.setId(2);
        node2.setCutInIntFormat(1);
        node2.setNodeName("Test");
        link.put(0, node);
        link.put(1, node2);
        boolean test = ps.updatePrescaleSetL1(link, false);
        assertEquals(true, test);
        L1PrescaleEditItemNode n1 = link.get(0);
        double v1 = (double) n1.getPrescaleValue();
        assertEquals(1.0, v1, 0.0001);
        assertEquals(-1.6777215E7, (double) link.get(1).getPrescaleValue(), 0.00001);
    }
   
    @Test
    public void UpdateL1ListOfOneWithDifferentElementReturnFalse() {
        XMLprescales ps = new XMLprescales("");
        ArrayList<Object> psVector = new ArrayList<Object>(2);
                psVector.add(0, 1);
                psVector.add(1, "Test");
        ps.prescaleSetMap.put("1", psVector);
        
        LinkedHashMap<Integer, L1PrescaleEditItemNode> link = new LinkedHashMap<Integer, L1PrescaleEditItemNode>();
        L1PrescaleEditItemNode node = new L1PrescaleEditItemNode();
        node.setId(1);
        node.setCutInIntFormat(1);
        node.setNodeName("Test2");
        link.put(0, node);
        boolean test = ps.updatePrescaleSetL1(link, true);
        assertEquals(false, test);
    }

}
