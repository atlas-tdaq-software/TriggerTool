/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.XML;

import java.sql.SQLException;
import triggertool.XML.Writers.XMLWriter_HLTChain;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Entities.HLT.HLTPrescale;
import triggerdb.Entities.HLT.HLTPrescaleType;
import triggerdb.Entities.HLT.HLTTriggerChain;
import triggerdb.Entities.HLT.HLTTriggerElement;
import triggerdb.Entities.HLT.HLTTriggerGroup;
import triggerdb.Entities.HLT.HLTTriggerSignature;
import triggerdb.Entities.HLT.HLTTriggerStream;

/**
 *
 * @author Michele
 */
public class XMLWriter_HLTChainTest {
    
    private XMLWriter_HLTChain writer;
    HLTTriggerChain chain;
    ArrayList<HLTPrescale> plist;
    
    public XMLWriter_HLTChainTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        writer = new XMLWriter_HLTChain();
        
        chain = new HLTTriggerChain();
        chain.set_chain_counter(170);
        chain.set_eb_point(3);
        chain.set_lower_chain_name("L1_EM18VH");
        chain.set_name("HLT_e24_medium_iloose");

        plist = new ArrayList<HLTPrescale>();        
        HLTPrescale p1 = new HLTPrescale();
        p1.set_prescale_set_id(1);
        p1.set_type(HLTPrescaleType.Prescale);
        p1.set_value(1);
        HLTPrescale p2 = new HLTPrescale();
        p2.set_prescale_set_id(1);
        p2.set_type(HLTPrescaleType.Pass_Through);
        p2.set_value(0);
        plist.add(p1);
        plist.add(p2);
    }
    
    @After
    public void tearDown() {
    }

    //@Test
    public void CanWriteChainXML() throws SQLException{                
        String xml = writer.WriteChain(chain, plist);
        
        String expectedXML = 
                "		<CHAIN EBstep=\"3\" chain_counter=\"170\" chain_name=\"HLT_e24_medium_iloose\" level=\"HLT\" lower_chain_name=\"L1_EM18VH\" pass_through=\"0\" prescale=\"1\" rerun_prescale=\"-1\">\n" +
                "			<TRIGGERTYPE_LIST/>\n" +
                "			<STREAMTAG_LIST>\n" +
                "			</STREAMTAG_LIST>\n" +
                "			<GROUP_LIST>\n" +
                "			</GROUP_LIST>\n" +
                "			<SIGNATURE_LIST/>\n" +
                "		</CHAIN>\n" +
                "";
        
        assertEquals(expectedXML, xml);
    }
    
    //@Test
    public void CanWriteChainWithStream() throws SQLException{
        HLTTriggerStream stream = new HLTTriggerStream();
        stream.set_obeyLB(1);
        stream.set_stream_prescale("1");
        stream.set_name("Egamma");
        stream.set_type("physics");
        ArrayList<HLTTriggerStream> streamList = new ArrayList<HLTTriggerStream>();
        streamList.add(stream);
        chain.setStreams(streamList);
                
        String xml = writer.WriteChain(chain, plist);
        
        String expectedXML = 
                "		<CHAIN EBstep=\"3\" chain_counter=\"170\" chain_name=\"HLT_e24_medium_iloose\" level=\"HLT\" lower_chain_name=\"L1_EM18VH\" pass_through=\"0\" prescale=\"1\" rerun_prescale=\"-1\">\n" +
                "			<TRIGGERTYPE_LIST/>\n" +
                "			<STREAMTAG_LIST>\n" +
                "				<STREAMTAG obeyLB=\"yes\" prescale=\"1\" stream=\"Egamma\" type=\"physics\"/>\n" +
                "			</STREAMTAG_LIST>\n" +
                "			<GROUP_LIST>\n" +
                "			</GROUP_LIST>\n" +
                "			<SIGNATURE_LIST/>\n" +
                "		</CHAIN>\n" +
                "";
        
        assertEquals(expectedXML, xml);
    }

    //@Test
    public void CanWriteChainWithGroups() throws SQLException{
        HLTTriggerGroup group1 = new HLTTriggerGroup();
        group1.set_name("RATE:SingleElectron");
        HLTTriggerGroup group2 = new HLTTriggerGroup();
        group2.set_name("BW:Egamma");
        ArrayList<HLTTriggerGroup> groupList = new ArrayList<HLTTriggerGroup>();
        groupList.add(group1);
        groupList.add(group2);
        chain.setTriggerGroups(groupList);
                
        String xml = writer.WriteChain(chain, plist);
        
        String expectedXML = 
                "		<CHAIN EBstep=\"3\" chain_counter=\"170\" chain_name=\"HLT_e24_medium_iloose\" level=\"HLT\" lower_chain_name=\"L1_EM18VH\" pass_through=\"0\" prescale=\"1\" rerun_prescale=\"-1\">\n" +
                "			<TRIGGERTYPE_LIST/>\n" +
                "			<STREAMTAG_LIST>\n" +
                "			</STREAMTAG_LIST>\n" +
                "			<GROUP_LIST>\n" +
                "				<GROUP name=\"RATE:SingleElectron\"/>\n" +
                "				<GROUP name=\"BW:Egamma\"/>\n" +
                "			</GROUP_LIST>\n" +
                "			<SIGNATURE_LIST/>\n" +
                "		</CHAIN>\n" +
                "";
        
         assertEquals(expectedXML, xml);
   }
    
    @Test
    public void dummyTest(){
        
    }
    //@Test
    public void CanWriteChainWithSignatures() throws SQLException{
        HLTTriggerSignature signature1 = new HLTTriggerSignature();
        signature1.set_logic(1);
        signature1.set_signature_counter(1);
        HLTTriggerElement element1 = new HLTTriggerElement();
        element1.set_name("L2_e24_medium_iloose_L1_EM18VHcl");
        element1.set_element_counter(1);
        ArrayList<HLTTriggerElement> sig1elList = new ArrayList<HLTTriggerElement>();
        sig1elList.add(element1);
        signature1.setElements(sig1elList);
        HLTTriggerSignature signature2 = new HLTTriggerSignature();
        signature2.set_logic(1);
        signature2.set_signature_counter(2);
        HLTTriggerElement element2 = new HLTTriggerElement();
        element2.set_name("L2_e24_medium_iloose_L1_EM18VHid");
        element2.set_element_counter(2);
        ArrayList<HLTTriggerElement> sig2elList = new ArrayList<HLTTriggerElement>();
        sig2elList.add(element2);
        signature2.setElements(sig2elList);
        
        ArrayList<HLTTriggerSignature> signatureList = new ArrayList<HLTTriggerSignature>();
        signatureList.add(signature1);
        signatureList.add(signature2);
        chain.setSignatures(signatureList);
        
        String xml = writer.WriteChain(chain, plist);
        
        String expectedXML = 
                "		<CHAIN EBstep=\"3\" chain_counter=\"170\" chain_name=\"HLT_e24_medium_iloose\" level=\"HLT\" lower_chain_name=\"L1_EM18VH\" pass_through=\"0\" prescale=\"1\" rerun_prescale=\"-1\">\n" +
                "			<TRIGGERTYPE_LIST/>\n" +
                "			<STREAMTAG_LIST>\n" +
                "			</STREAMTAG_LIST>\n" +
                "			<GROUP_LIST>\n" +
                "			</GROUP_LIST>\n" +
                "			<SIGNATURE_LIST>\n" +
                "				<SIGNATURE logic=\"1\" signature_counter=\"1\">\n" +
                "					<TRIGGERELEMENT te_name=\"L2_e24_medium_iloose_L1_EM18VHcl\"/>\n" +
                "				</SIGNATURE>\n" +
                "				<SIGNATURE logic=\"1\" signature_counter=\"2\">\n" +
                "					<TRIGGERELEMENT te_name=\"L2_e24_medium_iloose_L1_EM18VHid\"/>\n" +
                "				</SIGNATURE>\n" +
                "			</SIGNATURE_LIST>\n" +
                "		</CHAIN>\n" +
                "";

        assertEquals(expectedXML, xml);
    }
}
