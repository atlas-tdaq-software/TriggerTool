/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.XML;

import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import triggerdb.Entities.L1.L1Prescale;
import triggertool.XML.Readers.XMLReader_L1Prescale;

/**
 *
 * @author Michele
 */
public class XMLReader_L1PrescaleTest {
    
    XMLReader_L1Prescale reader;
    
    public XMLReader_L1PrescaleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void CreateL1PrescaleForNominalInput() throws Exception {
        reader = new XMLReader_L1Prescale();

        String xml = 
                "<?xml version=\"1.0\"?>\n" +
                "<LVL1Config name=\"Physics_pp_v5\" ctpVersion=\"4\" l1Version=\"1\">"+ 
                "  <PrescaleSet name=\"Physics_pp_v5_default_prescale\" type=\"Physics\" menuPartition=\"0\" version=\"0\">\n" +
                "    <Prescale ctpid=\"0\" cut=\"000001\" value=\"1\"/>\n" +
                "    <Prescale ctpid=\"1\" cut=\"000001\" value=\"1\"/>\n" +
                "  </PrescaleSet>\n" +
                "  <TriggerMenu name=\"Physics_pp_v5\" phase=\"lumi\">\n" +
                "    <TriggerItem ctpid=\"0\" partition=\"1\" name=\"L1_EM3\" complex_deadtime=\"0\" definition=\"(EM3[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
                "      <AND>\n" +
                "        <TriggerCondition multi=\"1\" name=\"EM3_x1\" triggerthreshold=\"EM3\"/>\n" +
                "        <InternalTrigger name=\"BGRP0\"/>\n" +
                "        <InternalTrigger name=\"BGRP1\"/>\n" +
                "      </AND>\n" +
                "    </TriggerItem>\n" +
                "    <TriggerItem ctpid=\"1\" partition=\"1\" name=\"L1_EM6\" complex_deadtime=\"0\" definition=\"(EM6[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
                "      <AND>\n" +
                "        <TriggerCondition multi=\"1\" name=\"EM6_x1\" triggerthreshold=\"EM6\"/>\n" +
                "        <InternalTrigger name=\"BGRP0\"/>\n" +
                "        <InternalTrigger name=\"BGRP1\"/>\n" +
                "      </AND>\n" +
                "    </TriggerItem>\n" +
                "  </TriggerMenu>"+
                "</LVL1Config>";
        
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xml));

        Document doc = db.parse(is);
       
        L1Prescale prescaleSet = reader.CreateL1Prescale(doc);
        
        assertEquals("Physics_pp_v5_default_prescale", prescaleSet.get_name());
        assertEquals("Physics", prescaleSet.get_type());
        assertEquals(1, (int)prescaleSet.get_val(1));
        assertEquals(1, (int)prescaleSet.get_val(2));
        for (int i = 3; i <= L1Prescale.LENGTH; ++i){
            assertEquals(-1, (int)prescaleSet.get_val(i));
        }
    }

    @Test
    public void PrescaleWithoutItemSetItToNegative() throws Exception {
        reader = new XMLReader_L1Prescale();

        String xml = 
                "<?xml version=\"1.0\"?>\n" +
                "<LVL1Config name=\"Physics_pp_v5\" ctpVersion=\"4\" l1Version=\"1\">"+ 
                "  <PrescaleSet name=\"Physics_pp_v5_default_prescale\" type=\"Physics\" menuPartition=\"0\" version=\"0\">\n" +
                "    <Prescale ctpid=\"0\" cut=\"000001\" value=\"1\"/>\n" +
                "    <Prescale ctpid=\"1\" cut=\"000001\" value=\"1\"/>\n" +
                "  </PrescaleSet>\n" +
                "  <TriggerMenu name=\"Physics_pp_v5\" phase=\"lumi\">\n" +
                "    <TriggerItem ctpid=\"0\" partition=\"1\" name=\"L1_EM3\" complex_deadtime=\"0\" definition=\"(EM3[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
                "      <AND>\n" +
                "        <TriggerCondition multi=\"1\" name=\"EM3_x1\" triggerthreshold=\"EM3\"/>\n" +
                "        <InternalTrigger name=\"BGRP0\"/>\n" +
                "        <InternalTrigger name=\"BGRP1\"/>\n" +
                "      </AND>\n" +
                "    </TriggerItem>\n" +
                "  </TriggerMenu>"+
                "</LVL1Config>";
        
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xml));

        Document doc = db.parse(is);
       
        L1Prescale prescaleSet = reader.CreateL1Prescale(doc);
        
        assertEquals("Physics_pp_v5_default_prescale", prescaleSet.get_name());
        assertEquals("Physics", prescaleSet.get_type());
        assertEquals(1, (int)prescaleSet.get_val(1));
        assertEquals(-1, (int)prescaleSet.get_val(2));
        for (int i = 3; i <= L1Prescale.LENGTH; ++i){
            assertEquals(-1, (int)prescaleSet.get_val(i));
        }
    }

    @Test
    public void PrescaleWitSkippedItemSetItToNegative() throws Exception {
        reader = new XMLReader_L1Prescale();

        String xml = 
                "<?xml version=\"1.0\"?>\n" +
                "<LVL1Config name=\"Physics_pp_v5\" ctpVersion=\"4\" l1Version=\"1\">"+ 
                "  <PrescaleSet name=\"Physics_pp_v5_default_prescale\" type=\"Physics\" menuPartition=\"0\" version=\"0\">\n" +
                "    <Prescale ctpid=\"0\" cut=\"000001\" value=\"1\"/>\n" +
                "    <Prescale ctpid=\"2\" cut=\"000001\" value=\"1\"/>\n" +
                "  </PrescaleSet>\n" +
                "  <TriggerMenu name=\"Physics_pp_v5\" phase=\"lumi\">\n" +
                "    <TriggerItem ctpid=\"0\" partition=\"1\" name=\"L1_EM3\" complex_deadtime=\"0\" definition=\"(EM3[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
                "      <AND>\n" +
                "        <TriggerCondition multi=\"1\" name=\"EM3_x1\" triggerthreshold=\"EM3\"/>\n" +
                "        <InternalTrigger name=\"BGRP0\"/>\n" +
                "        <InternalTrigger name=\"BGRP1\"/>\n" +
                "      </AND>\n" +
                "    </TriggerItem>\n" +
                "    <TriggerItem ctpid=\"2\" partition=\"1\" name=\"L1_EM6\" complex_deadtime=\"0\" definition=\"(EM6[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
                "      <AND>\n" +
                "        <TriggerCondition multi=\"1\" name=\"EM6_x1\" triggerthreshold=\"EM6\"/>\n" +
                "        <InternalTrigger name=\"BGRP0\"/>\n" +
                "        <InternalTrigger name=\"BGRP1\"/>\n" +
                "      </AND>\n" +
                "    </TriggerItem>\n" +                "  </TriggerMenu>"+
                "</LVL1Config>";
        
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xml));

        Document doc = db.parse(is);
       
        L1Prescale prescaleSet = reader.CreateL1Prescale(doc);
        
        assertEquals("Physics_pp_v5_default_prescale", prescaleSet.get_name());
        assertEquals("Physics", prescaleSet.get_type());
        assertEquals(1, (int)prescaleSet.get_val(1));
        assertEquals(-1, (int)prescaleSet.get_val(2));
        assertEquals(1, (int)prescaleSet.get_val(3));
        for (int i = 4; i <= L1Prescale.LENGTH; ++i){
            assertEquals(-1, (int)prescaleSet.get_val(i));
        }
    }

    @Test
    public void PrescaleFromMIG8() throws Exception {
        reader = new XMLReader_L1Prescale();

        String xml;
        String MenuXml1 = 
                "<?xml version=\"1.0\"?>\n" +
"<LVL1Config name=\"Physics_pp_v5\" ctpVersion=\"4\" l1Version=\"1\">\n" +
"  <!--File is generated by TriggerMenu-->\n" +
"  <!--No. L1 thresholds defined: 133-->\n" +
"  <!--No. L1 items defined: 207-->\n" +
"  <TriggerMenu name=\"Physics_pp_v5\" phase=\"lumi\">\n" +
"    <TriggerItem ctpid=\"0\" partition=\"1\" name=\"L1_EM3\" complex_deadtime=\"0\" definition=\"(EM3[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM3_x1\" triggerthreshold=\"EM3\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"1\" partition=\"1\" name=\"L1_EM7\" complex_deadtime=\"0\" definition=\"(EM7[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM7_x1\" triggerthreshold=\"EM7\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"2\" partition=\"1\" name=\"L1_EM12\" complex_deadtime=\"0\" definition=\"(EM12[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM12_x1\" triggerthreshold=\"EM12\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"3\" partition=\"1\" name=\"L1_EM8VH\" complex_deadtime=\"0\" definition=\"(EM8VH[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM8VH_x1\" triggerthreshold=\"EM8VH\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"4\" partition=\"1\" name=\"L1_EM10VH\" complex_deadtime=\"0\" definition=\"(EM10VH[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM10VH_x1\" triggerthreshold=\"EM10VH\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"5\" partition=\"1\" name=\"L1_EM13VH\" complex_deadtime=\"0\" definition=\"(EM10VH[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM10VH_x1\" triggerthreshold=\"EM10VH\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"6\" partition=\"1\" name=\"L1_EM8I\" complex_deadtime=\"0\" definition=\"(EM8I[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM8I_x1\" triggerthreshold=\"EM8I\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"7\" partition=\"1\" name=\"L1_EM15\" complex_deadtime=\"0\" definition=\"(EM15[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM15_x1\" triggerthreshold=\"EM15\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"8\" partition=\"1\" name=\"L1_EM15I\" complex_deadtime=\"0\" definition=\"(EM15I[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM15I_x1\" triggerthreshold=\"EM15I\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"9\" partition=\"1\" name=\"L1_EM15HI\" complex_deadtime=\"0\" definition=\"(EM15HI[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM15HI_x1\" triggerthreshold=\"EM15HI\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"10\" partition=\"1\" name=\"L1_EM15VH\" complex_deadtime=\"0\" definition=\"(EM15VH[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM15VH_x1\" triggerthreshold=\"EM15VH\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"11\" partition=\"1\" name=\"L1_EM18VH\" complex_deadtime=\"0\" definition=\"(EM18VH[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM18VH_x1\" triggerthreshold=\"EM18VH\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"12\" partition=\"1\" name=\"L1_EM20VH\" complex_deadtime=\"0\" definition=\"(EM20VH[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM20VH_x1\" triggerthreshold=\"EM20VH\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"13\" partition=\"1\" name=\"L1_EM20VHI\" complex_deadtime=\"0\" definition=\"(EM20VHI[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM20VHI_x1\" triggerthreshold=\"EM20VHI\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"14\" partition=\"1\" name=\"L1_EM22VHLIL\" complex_deadtime=\"0\" definition=\"(EM22VHLIL[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM22VHLIL_x1\" triggerthreshold=\"EM22VHLIL\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"15\" partition=\"1\" name=\"L1_EM50V\" complex_deadtime=\"0\" definition=\"(EM50V[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM50V_x1\" triggerthreshold=\"EM50V\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"16\" partition=\"1\" name=\"L1_EM3_EMPTY\" complex_deadtime=\"0\" definition=\"(EM3[x1]&amp;BGRP0&amp;BGRP3)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM3_x1\" triggerthreshold=\"EM3\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP3\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"17\" partition=\"1\" name=\"L1_EM7_EMPTY\" complex_deadtime=\"0\" definition=\"(EM7[x1]&amp;BGRP0&amp;BGRP3)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM7_x1\" triggerthreshold=\"EM7\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP3\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"18\" partition=\"1\" name=\"L1_EM3_UNPAIRED_ISO\" complex_deadtime=\"0\" definition=\"(EM3[x1]&amp;BGRP0&amp;BGRP4)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM3_x1\" triggerthreshold=\"EM3\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP4\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"19\" partition=\"1\" name=\"L1_J15.23ETA49\" complex_deadtime=\"0\" definition=\"(J15.23ETA49[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J15.23ETA49_x1\" triggerthreshold=\"J15.23ETA49\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"20\" partition=\"1\" name=\"L1_MU4\" complex_deadtime=\"0\" definition=\"(MU4[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU4_x1\" triggerthreshold=\"MU4\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"21\" partition=\"1\" name=\"L1_MU6\" complex_deadtime=\"0\" definition=\"(MU6[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU6_x1\" triggerthreshold=\"MU6\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"22\" partition=\"1\" name=\"L1_MU10\" complex_deadtime=\"0\" definition=\"(MU10[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU10_x1\" triggerthreshold=\"MU10\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"23\" partition=\"1\" name=\"L1_MU11\" complex_deadtime=\"0\" definition=\"(MU11[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU11_x1\" triggerthreshold=\"MU11\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"24\" partition=\"1\" name=\"L1_MU15\" complex_deadtime=\"0\" definition=\"(MU15[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU15_x1\" triggerthreshold=\"MU15\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"25\" partition=\"1\" name=\"L1_MU20\" complex_deadtime=\"0\" definition=\"(MU20[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU20_x1\" triggerthreshold=\"MU20\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"26\" partition=\"1\" name=\"L1_MU4_EMPTY\" complex_deadtime=\"0\" definition=\"(MU4[x1]&amp;BGRP0&amp;BGRP3)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU4_x1\" triggerthreshold=\"MU4\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP3\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"27\" partition=\"1\" name=\"L1_MU11_EMPTY\" complex_deadtime=\"0\" definition=\"(MU11[x1]&amp;BGRP0&amp;BGRP3)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU11_x1\" triggerthreshold=\"MU11\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP3\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"28\" partition=\"1\" name=\"L1_MU4_UNPAIRED_ISO\" complex_deadtime=\"0\" definition=\"(MU4[x1]&amp;BGRP0&amp;BGRP4)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU4_x1\" triggerthreshold=\"MU4\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP4\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"29\" partition=\"1\" name=\"L1_2EM3\" complex_deadtime=\"0\" definition=\"(EM3[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"2\" name=\"EM3_x2\" triggerthreshold=\"EM3\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"30\" partition=\"1\" name=\"L1_2EM8VH\" complex_deadtime=\"0\" definition=\"(EM8VH[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"2\" name=\"EM8VH_x2\" triggerthreshold=\"EM8VH\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"31\" partition=\"1\" name=\"L1_2EM10VH\" complex_deadtime=\"0\" definition=\"(EM10VH[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"2\" name=\"EM10VH_x2\" triggerthreshold=\"EM10VH\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"32\" partition=\"1\" name=\"L1_2EM15\" complex_deadtime=\"0\" definition=\"(EM15[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"2\" name=\"EM15_x2\" triggerthreshold=\"EM15\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"33\" partition=\"1\" name=\"L1_2EM15VH\" complex_deadtime=\"0\" definition=\"(EM15VH[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"2\" name=\"EM15VH_x2\" triggerthreshold=\"EM15VH\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"34\" partition=\"1\" name=\"L1_EM7_2EM3\" complex_deadtime=\"0\" definition=\"(EM7[x1]&amp;EM3[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM7_x1\" triggerthreshold=\"EM7\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"EM3_x2\" triggerthreshold=\"EM3\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"35\" partition=\"1\" name=\"L1_EM12_2EM3\" complex_deadtime=\"0\" definition=\"(EM12[x1]&amp;EM3[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM12_x1\" triggerthreshold=\"EM12\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"EM3_x2\" triggerthreshold=\"EM3\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"36\" partition=\"1\" name=\"L1_EM15VH_3EM7\" complex_deadtime=\"0\" definition=\"(EM15VH[x1]&amp;EM7[x3]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM15VH_x1\" triggerthreshold=\"EM15VH\"/>\n" +
"        <TriggerCondition multi=\"3\" name=\"EM7_x3\" triggerthreshold=\"EM7\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"37\" partition=\"1\" name=\"L1_2MU4\" complex_deadtime=\"0\" definition=\"(MU4[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"2\" name=\"MU4_x2\" triggerthreshold=\"MU4\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"38\" partition=\"1\" name=\"L1_2MU6\" complex_deadtime=\"0\" definition=\"(MU6[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"2\" name=\"MU6_x2\" triggerthreshold=\"MU6\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"39\" partition=\"1\" name=\"L1_2MU10\" complex_deadtime=\"0\" definition=\"(MU10[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"2\" name=\"MU10_x2\" triggerthreshold=\"MU10\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"40\" partition=\"1\" name=\"L1_MU10_2MU6\" complex_deadtime=\"0\" definition=\"(MU10[x1]&amp;MU6[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU10_x1\" triggerthreshold=\"MU10\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"MU6_x2\" triggerthreshold=\"MU6\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"41\" partition=\"1\" name=\"L1_MU10_2MU4\" complex_deadtime=\"0\" definition=\"(MU10[x1]&amp;MU4[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU10_x1\" triggerthreshold=\"MU10\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"MU4_x2\" triggerthreshold=\"MU4\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"42\" partition=\"1\" name=\"L1_3MU4\" complex_deadtime=\"0\" definition=\"(MU4[x3]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"3\" name=\"MU4_x3\" triggerthreshold=\"MU4\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"43\" partition=\"1\" name=\"L1_3MU6\" complex_deadtime=\"0\" definition=\"(MU6[x3]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"3\" name=\"MU6_x3\" triggerthreshold=\"MU6\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"44\" partition=\"1\" name=\"L1_EM15_MU4\" complex_deadtime=\"0\" definition=\"(EM15[x1]&amp;MU4[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM15_x1\" triggerthreshold=\"EM15\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU4_x1\" triggerthreshold=\"MU4\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"45\" partition=\"1\" name=\"L1_2EM8VH_MU10\" complex_deadtime=\"0\" definition=\"(EM8VH[x2]&amp;MU10[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"2\" name=\"EM8VH_x2\" triggerthreshold=\"EM8VH\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU10_x1\" triggerthreshold=\"MU10\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"46\" partition=\"1\" name=\"L1_EM8VH_2MU6\" complex_deadtime=\"0\" definition=\"(EM8VH[x1]&amp;MU6[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM8VH_x1\" triggerthreshold=\"EM8VH\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"MU6_x2\" triggerthreshold=\"MU6\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"47\" partition=\"1\" name=\"L1_EM15VH_MU10\" complex_deadtime=\"0\" definition=\"(EM15VH[x1]&amp;MU10[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10001000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM15VH_x1\" triggerthreshold=\"EM15VH\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU10_x1\" triggerthreshold=\"MU10\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"48\" partition=\"1\" name=\"L1_TAU6\" complex_deadtime=\"0\" definition=\"(HA6[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA6_x1\" triggerthreshold=\"HA6\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"49\" partition=\"1\" name=\"L1_TAU8\" complex_deadtime=\"0\" definition=\"(HA8[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA8_x1\" triggerthreshold=\"HA8\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"50\" partition=\"1\" name=\"L1_TAU12\" complex_deadtime=\"0\" definition=\"(HA12[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA12_x1\" triggerthreshold=\"HA12\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"51\" partition=\"1\" name=\"L1_TAU12I\" complex_deadtime=\"0\" definition=\"(HA12I[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA12I_x1\" triggerthreshold=\"HA12I\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"52\" partition=\"1\" name=\"L1_TAU20\" complex_deadtime=\"0\" definition=\"(HA20[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA20_x1\" triggerthreshold=\"HA20\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"53\" partition=\"1\" name=\"L1_TAU30\" complex_deadtime=\"0\" definition=\"(HA30[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA30_x1\" triggerthreshold=\"HA30\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"54\" partition=\"1\" name=\"L1_TAU40\" complex_deadtime=\"0\" definition=\"(HA40[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA40_x1\" triggerthreshold=\"HA40\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"55\" partition=\"1\" name=\"L1_TAU60\" complex_deadtime=\"0\" definition=\"(HA60[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA60_x1\" triggerthreshold=\"HA60\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"56\" partition=\"1\" name=\"L1_TAU8_EMPTY\" complex_deadtime=\"0\" definition=\"(HA8[x1]&amp;BGRP0&amp;BGRP3)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA8_x1\" triggerthreshold=\"HA8\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP3\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"57\" partition=\"1\" name=\"L1_TAU20_2TAU12\" complex_deadtime=\"0\" definition=\"(HA20[x1]&amp;HA12[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA20_x1\" triggerthreshold=\"HA20\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"HA12_x2\" triggerthreshold=\"HA12\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"58\" partition=\"1\" name=\"L1_TAU20_2TAU12I\" complex_deadtime=\"0\" definition=\"(HA20[x1]&amp;HA12I[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA20_x1\" triggerthreshold=\"HA20\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"HA12I_x2\" triggerthreshold=\"HA12I\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"59\" partition=\"1\" name=\"L1_EM15_2TAU12\" complex_deadtime=\"0\" definition=\"(EM15[x1]&amp;HA12[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM15_x1\" triggerthreshold=\"EM15\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"HA12_x2\" triggerthreshold=\"HA12\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"60\" partition=\"1\" name=\"L1_EM15_2TAU20\" complex_deadtime=\"0\" definition=\"(EM15[x1]&amp;HA20[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM15_x1\" triggerthreshold=\"EM15\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"HA20_x2\" triggerthreshold=\"HA20\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"61\" partition=\"1\" name=\"L1_EM15HI_2TAU12IL\" complex_deadtime=\"0\" definition=\"(EM15HI[x1]&amp;HA12IL[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM15HI_x1\" triggerthreshold=\"EM15HI\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"HA12IL_x2\" triggerthreshold=\"HA12IL\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"62\" partition=\"1\" name=\"L1_EM15_TAU40_2TAU15\" complex_deadtime=\"0\" definition=\"(EM15[x1]&amp;HA40[x1]&amp;HA15[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM15_x1\" triggerthreshold=\"EM15\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA40_x1\" triggerthreshold=\"HA40\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"HA15_x2\" triggerthreshold=\"HA15\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"63\" partition=\"1\" name=\"L1_EM15HI_TAU40_2TAU15\" complex_deadtime=\"0\" definition=\"(EM15HI[x1]&amp;HA40[x1]&amp;HA15[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM15HI_x1\" triggerthreshold=\"EM15HI\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA40_x1\" triggerthreshold=\"HA40\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"HA15_x2\" triggerthreshold=\"HA15\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"64\" partition=\"1\" name=\"L1_EM15_2TAU12_J25_2J15_3J12\" complex_deadtime=\"0\" definition=\"(EM15[x1]&amp;HA12[x2]&amp;J25[x1]&amp;J15[x2]&amp;J12[x3]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM15_x1\" triggerthreshold=\"EM15\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"HA12_x2\" triggerthreshold=\"HA12\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"J25_x1\" triggerthreshold=\"J25\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"J15_x2\" triggerthreshold=\"J15\"/>\n" +
"        <TriggerCondition multi=\"3\" name=\"J12_x3\" triggerthreshold=\"J12\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"65\" partition=\"1\" name=\"L1_EM15HI_2TAU12I_J25_2J15_3J12\" complex_deadtime=\"0\" definition=\"(EM15[x1]&amp;HA12I[x2]&amp;J25[x1]&amp;J15[x2]&amp;J12[x3]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM15_x1\" triggerthreshold=\"EM15\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"HA12I_x2\" triggerthreshold=\"HA12I\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"J25_x1\" triggerthreshold=\"J25\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"J15_x2\" triggerthreshold=\"J15\"/>\n" +
"        <TriggerCondition multi=\"3\" name=\"J12_x3\" triggerthreshold=\"J12\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"66\" partition=\"1\" name=\"L1_MU10_TAU12\" complex_deadtime=\"0\" definition=\"(MU10[x1]&amp;HA12[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU10_x1\" triggerthreshold=\"MU10\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA12_x1\" triggerthreshold=\"HA12\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"67\" partition=\"1\" name=\"L1_MU10_TAU12I\" complex_deadtime=\"0\" definition=\"(MU10[x1]&amp;HA12I[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU10_x1\" triggerthreshold=\"MU10\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA12I_x1\" triggerthreshold=\"HA12I\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"68\" partition=\"1\" name=\"L1_MU10_TAU12_J25_2J12\" complex_deadtime=\"0\" definition=\"(MU10[x1]&amp;HA12[x1]&amp;J25[x1]&amp;J12[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU10_x1\" triggerthreshold=\"MU10\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA12_x1\" triggerthreshold=\"HA12\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"J25_x1\" triggerthreshold=\"J25\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"J12_x2\" triggerthreshold=\"J12\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"69\" partition=\"1\" name=\"L1_MU10_TAU20\" complex_deadtime=\"0\" definition=\"(MU10[x1]&amp;HA20[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU10_x1\" triggerthreshold=\"MU10\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA20_x1\" triggerthreshold=\"HA20\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"70\" partition=\"1\" name=\"L1_MU10_TAU20I\" complex_deadtime=\"0\" definition=\"(MU10[x1]&amp;HA20I[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU10_x1\" triggerthreshold=\"MU10\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA20I_x1\" triggerthreshold=\"HA20I\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"71\" partition=\"1\" name=\"L1_TAU12I_MU10_J25_2J12\" complex_deadtime=\"0\" definition=\"(HA12I[x1]&amp;MU10[x1]&amp;J25[x1]&amp;J12[x2]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA12I_x1\" triggerthreshold=\"HA12I\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU10_x1\" triggerthreshold=\"MU10\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"J25_x1\" triggerthreshold=\"J25\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"J12_x2\" triggerthreshold=\"J12\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"72\" partition=\"1\" name=\"L1_TAU20_2TAU12_J25_2J20_3J12\" complex_deadtime=\"0\" definition=\"(HA20[x1]&amp;HA12[x2]&amp;J25[x1]&amp;J20[x2]&amp;J12[x3]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA20_x1\" triggerthreshold=\"HA20\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"HA12_x2\" triggerthreshold=\"HA12\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"J25_x1\" triggerthreshold=\"J25\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"J20_x2\" triggerthreshold=\"J20\"/>\n" +
"        <TriggerCondition multi=\"3\" name=\"J12_x3\" triggerthreshold=\"J12\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"73\" partition=\"1\" name=\"L1_TAU20_2TAU12I_J25_2J15_3J12\" complex_deadtime=\"0\" definition=\"(HA20[x1]&amp;HA12I[x2]&amp;J25[x1]&amp;J15[x2]&amp;J12[x3]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA20_x1\" triggerthreshold=\"HA20\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"HA12I_x2\" triggerthreshold=\"HA12I\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"J25_x1\" triggerthreshold=\"J25\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"J15_x2\" triggerthreshold=\"J15\"/>\n" +
"        <TriggerCondition multi=\"3\" name=\"J12_x3\" triggerthreshold=\"J12\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"74\" partition=\"1\" name=\"L1_TAU20_2J20_XE45\" complex_deadtime=\"0\" definition=\"(HA20[x1]&amp;J20[x2]&amp;XE45[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA20_x1\" triggerthreshold=\"HA20\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"J20_x2\" triggerthreshold=\"J20\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"XE45_x1\" triggerthreshold=\"XE45\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"75\" partition=\"1\" name=\"L1_EM15HI_2TAU12I_XE35\" complex_deadtime=\"0\" definition=\"(EM15HI[x1]&amp;HA12I[x2]&amp;XE35[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM15HI_x1\" triggerthreshold=\"EM15HI\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"HA12I_x2\" triggerthreshold=\"HA12I\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"XE35_x1\" triggerthreshold=\"XE35\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"76\" partition=\"1\" name=\"L1_MU10_TAU12I_XE35\" complex_deadtime=\"0\" definition=\"(MU10[x1]&amp;HA12I[x1]&amp;XE35[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MU10_x1\" triggerthreshold=\"MU10\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA12I_x1\" triggerthreshold=\"HA12I\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"XE35_x1\" triggerthreshold=\"XE35\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"77\" partition=\"1\" name=\"L1_TAU20_2TAU12_XE35\" complex_deadtime=\"0\" definition=\"(HA20[x1]&amp;HA12[x2]&amp;XE35[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA20_x1\" triggerthreshold=\"HA20\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"HA12_x2\" triggerthreshold=\"HA12\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"XE35_x1\" triggerthreshold=\"XE35\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"78\" partition=\"1\" name=\"L1_TAU20I_2TAU12I_XE35\" complex_deadtime=\"0\" definition=\"(HA20I[x1]&amp;HA12I[x2]&amp;XE35[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HA20I_x1\" triggerthreshold=\"HA20I\"/>\n" +
"        <TriggerCondition multi=\"2\" name=\"HA12I_x2\" triggerthreshold=\"HA12I\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"XE35_x1\" triggerthreshold=\"XE35\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"79\" partition=\"1\" name=\"L1_EM15VH_J15.23ETA49\" complex_deadtime=\"0\" definition=\"(EM15VH[x1]&amp;J15.23ETA49[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"EM15VH_x1\" triggerthreshold=\"EM15VH\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"J15.23ETA49_x1\" triggerthreshold=\"J15.23ETA49\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"80\" partition=\"1\" name=\"L1_J12\" complex_deadtime=\"0\" definition=\"(J12[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J12_x1\" triggerthreshold=\"J12\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"81\" partition=\"1\" name=\"L1_J20\" complex_deadtime=\"0\" definition=\"(J20[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J20_x1\" triggerthreshold=\"J20\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"82\" partition=\"1\" name=\"L1_J30\" complex_deadtime=\"0\" definition=\"(J30[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J30_x1\" triggerthreshold=\"J30\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"83\" partition=\"1\" name=\"L1_J50\" complex_deadtime=\"0\" definition=\"(J50[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J50_x1\" triggerthreshold=\"J50\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"84\" partition=\"1\" name=\"L1_J75\" complex_deadtime=\"0\" definition=\"(J75[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J75_x1\" triggerthreshold=\"J75\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"85\" partition=\"1\" name=\"L1_J100\" complex_deadtime=\"0\" definition=\"(J100[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J100_x1\" triggerthreshold=\"J100\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"86\" partition=\"1\" name=\"L1_J400\" complex_deadtime=\"0\" definition=\"(J400[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J400_x1\" triggerthreshold=\"J400\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"87\" partition=\"1\" name=\"L1_J20.32ETA49\" complex_deadtime=\"0\" definition=\"(J20.32ETA49[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J20.32ETA49_x1\" triggerthreshold=\"J20.32ETA49\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"88\" partition=\"1\" name=\"L1_J30.32ETA49\" complex_deadtime=\"0\" definition=\"(J30.32ETA49[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J30.32ETA49_x1\" triggerthreshold=\"J30.32ETA49\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"89\" partition=\"1\" name=\"L1_J50.32ETA49\" complex_deadtime=\"0\" definition=\"(J50.32ETA49[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J50.32ETA49_x1\" triggerthreshold=\"J50.32ETA49\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"90\" partition=\"1\" name=\"L1_J75.32ETA49\" complex_deadtime=\"0\" definition=\"(J75.32ETA49[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J75.32ETA49_x1\" triggerthreshold=\"J75.32ETA49\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"91\" partition=\"1\" name=\"L1_J100.32ETA49\" complex_deadtime=\"0\" definition=\"(J100.32ETA49[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J100.32ETA49_x1\" triggerthreshold=\"J100.32ETA49\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"92\" partition=\"1\" name=\"L1_J12_EMPTY\" complex_deadtime=\"0\" definition=\"(J12[x1]&amp;BGRP0&amp;BGRP3)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J12_x1\" triggerthreshold=\"J12\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP3\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"93\" partition=\"1\" name=\"L1_J12_FIRSTEMPTY\" complex_deadtime=\"0\" definition=\"(J12[x1]&amp;BGRP0&amp;BGRP6)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J12_x1\" triggerthreshold=\"J12\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP6\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"94\" partition=\"1\" name=\"L1_J12_UNPAIRED_ISO\" complex_deadtime=\"0\" definition=\"(J12[x1]&amp;BGRP0&amp;BGRP4)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J12_x1\" triggerthreshold=\"J12\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP4\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"95\" partition=\"1\" name=\"L1_J12_UNPAIRED_NONISO\" complex_deadtime=\"0\" definition=\"(J12[x1]&amp;BGRP0&amp;BGRP5)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J12_x1\" triggerthreshold=\"J12\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP5\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"96\" partition=\"1\" name=\"L1_J30_EMPTY\" complex_deadtime=\"0\" definition=\"(J30[x1]&amp;BGRP0&amp;BGRP3)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J30_x1\" triggerthreshold=\"J30\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP3\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"97\" partition=\"1\" name=\"L1_J30_FIRSTEMPTY\" complex_deadtime=\"0\" definition=\"(J30[x1]&amp;BGRP0&amp;BGRP6)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J30_x1\" triggerthreshold=\"J30\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP6\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"98\" partition=\"1\" name=\"L1_J30.32ETA49_EMPTY\" complex_deadtime=\"0\" definition=\"(J30.32ETA49[x1]&amp;BGRP0&amp;BGRP3)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J30.32ETA49_x1\" triggerthreshold=\"J30.32ETA49\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP3\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"99\" partition=\"1\" name=\"L1_J30.32ETA49_FIRSTEMPTY\" complex_deadtime=\"0\" definition=\"(J30.32ETA49[x1]&amp;BGRP0&amp;BGRP6)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J30.32ETA49_x1\" triggerthreshold=\"J30.32ETA49\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP6\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"100\" partition=\"1\" name=\"L1_J30.32ETA49_UNPAIRED_ISO\" complex_deadtime=\"0\" definition=\"(J30.32ETA49[x1]&amp;BGRP0&amp;BGRP4)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J30.32ETA49_x1\" triggerthreshold=\"J30.32ETA49\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP4\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n";
        String MenuXml2 =
"    <TriggerItem ctpid=\"101\" partition=\"1\" name=\"L1_3J15\" complex_deadtime=\"0\" definition=\"(J15[x3]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"3\" name=\"J15_x3\" triggerthreshold=\"J15\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"102\" partition=\"1\" name=\"L1_3J20\" complex_deadtime=\"0\" definition=\"(J20[x3]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"3\" name=\"J20_x3\" triggerthreshold=\"J20\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"103\" partition=\"1\" name=\"L1_3J15.0ETA24\" complex_deadtime=\"0\" definition=\"(J15.0ETA24[x3]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"3\" name=\"J15.0ETA24_x3\" triggerthreshold=\"J15.0ETA24\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"104\" partition=\"1\" name=\"L1_3J50\" complex_deadtime=\"0\" definition=\"(J50[x3]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"3\" name=\"J50_x3\" triggerthreshold=\"J50\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"105\" partition=\"1\" name=\"L1_4J20\" complex_deadtime=\"0\" definition=\"(J20[x4]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"4\" name=\"J20_x4\" triggerthreshold=\"J20\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"106\" partition=\"1\" name=\"L1_3J75\" complex_deadtime=\"0\" definition=\"(J75[x3]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"3\" name=\"J75_x3\" triggerthreshold=\"J75\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"107\" partition=\"1\" name=\"L1_4J30\" complex_deadtime=\"0\" definition=\"(J30[x4]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"4\" name=\"J30_x4\" triggerthreshold=\"J30\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"108\" partition=\"1\" name=\"L1_4J17.0ETA22\" complex_deadtime=\"0\" definition=\"(J17.0ETA22[x4]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"4\" name=\"J17.0ETA22_x4\" triggerthreshold=\"J17.0ETA22\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"109\" partition=\"1\" name=\"L1_3J25.0ETA22\" complex_deadtime=\"0\" definition=\"(J25.0ETA22[x3]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"3\" name=\"J25.0ETA22_x3\" triggerthreshold=\"J25.0ETA22\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"110\" partition=\"1\" name=\"L1_5J15.0ETA24\" complex_deadtime=\"0\" definition=\"(J15.0ETA24[x5]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"5\" name=\"J15.0ETA24_x5\" triggerthreshold=\"J15.0ETA24\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"111\" partition=\"1\" name=\"L1_2J15_XE55\" complex_deadtime=\"0\" definition=\"(J15[x2]&amp;XE55[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"2\" name=\"J15_x2\" triggerthreshold=\"J15\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"XE55_x1\" triggerthreshold=\"XE55\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"112\" partition=\"1\" name=\"L1_J40_XE50\" complex_deadtime=\"0\" definition=\"(J40[x1]&amp;XE50[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J40_x1\" triggerthreshold=\"J40\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"XE50_x1\" triggerthreshold=\"XE50\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"113\" partition=\"1\" name=\"L1_J75_XE40\" complex_deadtime=\"0\" definition=\"(J75[x1]&amp;XE40[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J75_x1\" triggerthreshold=\"J75\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"XE40_x1\" triggerthreshold=\"XE40\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"114\" partition=\"1\" name=\"L1_XE35\" complex_deadtime=\"0\" definition=\"(XE35[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"XE35_x1\" triggerthreshold=\"XE35\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"115\" partition=\"1\" name=\"L1_XE45\" complex_deadtime=\"0\" definition=\"(XE45[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"XE45_x1\" triggerthreshold=\"XE45\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"116\" partition=\"1\" name=\"L1_XE50\" complex_deadtime=\"0\" definition=\"(XE50[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"XE50_x1\" triggerthreshold=\"XE50\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"117\" partition=\"1\" name=\"L1_XE55\" complex_deadtime=\"0\" definition=\"(XE55[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"XE55_x1\" triggerthreshold=\"XE55\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"118\" partition=\"1\" name=\"L1_XE60\" complex_deadtime=\"0\" definition=\"(XE60[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"XE60_x1\" triggerthreshold=\"XE60\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"119\" partition=\"1\" name=\"L1_XE70\" complex_deadtime=\"0\" definition=\"(XE70[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"XE70_x1\" triggerthreshold=\"XE70\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"120\" partition=\"1\" name=\"L1_XE80\" complex_deadtime=\"0\" definition=\"(XE80[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"XE80_x1\" triggerthreshold=\"XE80\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"121\" partition=\"1\" name=\"L1_TE20\" complex_deadtime=\"0\" definition=\"(TE20[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"TE20_x1\" triggerthreshold=\"TE20\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"122\" partition=\"1\" name=\"L1_TE30\" complex_deadtime=\"0\" definition=\"(TE30[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"TE30_x1\" triggerthreshold=\"TE30\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"123\" partition=\"1\" name=\"L1_TE40\" complex_deadtime=\"0\" definition=\"(TE40[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000100\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"TE40_x1\" triggerthreshold=\"TE40\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"124\" partition=\"1\" name=\"L1_MBTS_1\" complex_deadtime=\"0\" definition=\"((MBTS_A[x1]|MBTS_C[x1])&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <OR>\n" +
"          <TriggerCondition multi=\"1\" name=\"MBTS_A_x1\" triggerthreshold=\"MBTS_A\"/>\n" +
"          <TriggerCondition multi=\"1\" name=\"MBTS_C_x1\" triggerthreshold=\"MBTS_C\"/>\n" +
"        </OR>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"125\" partition=\"1\" name=\"L1_MBTS_2\" complex_deadtime=\"0\" definition=\"((MBTS_A[x2]|MBTS_C[x2]|MBTS_A[x1])&amp;(MBTS_A[x2]|MBTS_C[x2]|MBTS_C[x1])&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <OR>\n" +
"          <TriggerCondition multi=\"2\" name=\"MBTS_A_x2\" triggerthreshold=\"MBTS_A\"/>\n" +
"          <TriggerCondition multi=\"2\" name=\"MBTS_C_x2\" triggerthreshold=\"MBTS_C\"/>\n" +
"          <TriggerCondition multi=\"1\" name=\"MBTS_A_x1\" triggerthreshold=\"MBTS_A\"/>\n" +
"        </OR>\n" +
"        <OR>\n" +
"          <TriggerCondition multi=\"2\" name=\"MBTS_A_x2\" triggerthreshold=\"MBTS_A\"/>\n" +
"          <TriggerCondition multi=\"2\" name=\"MBTS_C_x2\" triggerthreshold=\"MBTS_C\"/>\n" +
"          <TriggerCondition multi=\"1\" name=\"MBTS_C_x1\" triggerthreshold=\"MBTS_C\"/>\n" +
"        </OR>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"126\" partition=\"1\" name=\"L1_MBTS_1_1\" complex_deadtime=\"0\" definition=\"(MBTS_A[x1]&amp;MBTS_C[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_A_x1\" triggerthreshold=\"MBTS_A\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_C_x1\" triggerthreshold=\"MBTS_C\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"127\" partition=\"1\" name=\"L1_MBTS_2_UNPAIRED_ISO\" complex_deadtime=\"0\" definition=\"((MBTS_A[x2]|MBTS_C[x2]|MBTS_A[x1])&amp;(MBTS_A[x2]|MBTS_C[x2]|MBTS_C[x1])&amp;BGRP0&amp;BGRP4)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <OR>\n" +
"          <TriggerCondition multi=\"2\" name=\"MBTS_A_x2\" triggerthreshold=\"MBTS_A\"/>\n" +
"          <TriggerCondition multi=\"2\" name=\"MBTS_C_x2\" triggerthreshold=\"MBTS_C\"/>\n" +
"          <TriggerCondition multi=\"1\" name=\"MBTS_A_x1\" triggerthreshold=\"MBTS_A\"/>\n" +
"        </OR>\n" +
"        <OR>\n" +
"          <TriggerCondition multi=\"2\" name=\"MBTS_A_x2\" triggerthreshold=\"MBTS_A\"/>\n" +
"          <TriggerCondition multi=\"2\" name=\"MBTS_C_x2\" triggerthreshold=\"MBTS_C\"/>\n" +
"          <TriggerCondition multi=\"1\" name=\"MBTS_C_x1\" triggerthreshold=\"MBTS_C\"/>\n" +
"        </OR>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP4\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"128\" partition=\"1\" name=\"L1_MBTSA0\" complex_deadtime=\"0\" definition=\"(MBTS_A0[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_A0_x1\" triggerthreshold=\"MBTS_A0\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"129\" partition=\"1\" name=\"L1_MBTSA1\" complex_deadtime=\"0\" definition=\"(MBTS_A1[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_A1_x1\" triggerthreshold=\"MBTS_A1\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"130\" partition=\"1\" name=\"L1_MBTSA2\" complex_deadtime=\"0\" definition=\"(MBTS_A2[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_A2_x1\" triggerthreshold=\"MBTS_A2\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"131\" partition=\"1\" name=\"L1_MBTSA3\" complex_deadtime=\"0\" definition=\"(MBTS_A3[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_A3_x1\" triggerthreshold=\"MBTS_A3\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"132\" partition=\"1\" name=\"L1_MBTSA4\" complex_deadtime=\"0\" definition=\"(MBTS_A4[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_A4_x1\" triggerthreshold=\"MBTS_A4\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"133\" partition=\"1\" name=\"L1_MBTSA5\" complex_deadtime=\"0\" definition=\"(MBTS_A5[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_A5_x1\" triggerthreshold=\"MBTS_A5\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"134\" partition=\"1\" name=\"L1_MBTSA6\" complex_deadtime=\"0\" definition=\"(MBTS_A6[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_A6_x1\" triggerthreshold=\"MBTS_A6\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"135\" partition=\"1\" name=\"L1_MBTSA7\" complex_deadtime=\"0\" definition=\"(MBTS_A7[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_A7_x1\" triggerthreshold=\"MBTS_A7\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"136\" partition=\"1\" name=\"L1_MBTSA8\" complex_deadtime=\"0\" definition=\"(MBTS_A8[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_A8_x1\" triggerthreshold=\"MBTS_A8\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"137\" partition=\"1\" name=\"L1_MBTSA10\" complex_deadtime=\"0\" definition=\"(MBTS_A10[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_A10_x1\" triggerthreshold=\"MBTS_A10\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"138\" partition=\"1\" name=\"L1_MBTSA12\" complex_deadtime=\"0\" definition=\"(MBTS_A12[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_A12_x1\" triggerthreshold=\"MBTS_A12\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"139\" partition=\"1\" name=\"L1_MBTSA14\" complex_deadtime=\"0\" definition=\"(MBTS_A14[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_A14_x1\" triggerthreshold=\"MBTS_A14\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"140\" partition=\"1\" name=\"L1_MBTSC0\" complex_deadtime=\"0\" definition=\"(MBTS_C0[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_C0_x1\" triggerthreshold=\"MBTS_C0\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"141\" partition=\"1\" name=\"L1_MBTSC1\" complex_deadtime=\"0\" definition=\"(MBTS_C1[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_C1_x1\" triggerthreshold=\"MBTS_C1\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"142\" partition=\"1\" name=\"L1_MBTSC2\" complex_deadtime=\"0\" definition=\"(MBTS_C2[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_C2_x1\" triggerthreshold=\"MBTS_C2\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"143\" partition=\"1\" name=\"L1_MBTSC3\" complex_deadtime=\"0\" definition=\"(MBTS_C3[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_C3_x1\" triggerthreshold=\"MBTS_C3\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"144\" partition=\"1\" name=\"L1_MBTSC4\" complex_deadtime=\"0\" definition=\"(MBTS_C4[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_C4_x1\" triggerthreshold=\"MBTS_C4\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"145\" partition=\"1\" name=\"L1_MBTSC5\" complex_deadtime=\"0\" definition=\"(MBTS_C5[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_C5_x1\" triggerthreshold=\"MBTS_C5\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"146\" partition=\"1\" name=\"L1_MBTSC6\" complex_deadtime=\"0\" definition=\"(MBTS_C6[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_C6_x1\" triggerthreshold=\"MBTS_C6\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"147\" partition=\"1\" name=\"L1_MBTSC7\" complex_deadtime=\"0\" definition=\"(MBTS_C7[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_C7_x1\" triggerthreshold=\"MBTS_C7\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"148\" partition=\"1\" name=\"L1_MBTSC8\" complex_deadtime=\"0\" definition=\"(MBTS_C8[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_C8_x1\" triggerthreshold=\"MBTS_C8\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"149\" partition=\"1\" name=\"L1_MBTSC10\" complex_deadtime=\"0\" definition=\"(MBTS_C10[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_C10_x1\" triggerthreshold=\"MBTS_C10\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"150\" partition=\"1\" name=\"L1_MBTSC12\" complex_deadtime=\"0\" definition=\"(MBTS_C12[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_C12_x1\" triggerthreshold=\"MBTS_C12\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"151\" partition=\"1\" name=\"L1_MBTSC14\" complex_deadtime=\"0\" definition=\"(MBTS_C14[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"MBTS_C14_x1\" triggerthreshold=\"MBTS_C14\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"152\" partition=\"1\" name=\"L1_RD0_FILLED\" complex_deadtime=\"0\" definition=\"(RNDM0&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000001\">\n" +
"      <AND>\n" +
"        <InternalTrigger name=\"RNDM0\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"153\" partition=\"1\" name=\"L1_RD0_UNPAIRED_ISO\" complex_deadtime=\"0\" definition=\"(RNDM0&amp;BGRP0&amp;BGRP4)\" trigger_type=\"10000001\">\n" +
"      <AND>\n" +
"        <InternalTrigger name=\"RNDM0\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP4\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"154\" partition=\"1\" name=\"L1_RD0_EMPTY\" complex_deadtime=\"0\" definition=\"(RNDM0&amp;BGRP0&amp;BGRP3)\" trigger_type=\"10000001\">\n" +
"      <AND>\n" +
"        <InternalTrigger name=\"RNDM0\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP3\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"155\" partition=\"1\" name=\"L1_RD1_FILLED\" complex_deadtime=\"0\" definition=\"(RNDM1&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000001\">\n" +
"      <AND>\n" +
"        <InternalTrigger name=\"RNDM1\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"156\" partition=\"1\" name=\"L1_RD1_EMPTY\" complex_deadtime=\"0\" definition=\"(RNDM1&amp;BGRP0&amp;BGRP3)\" trigger_type=\"10000001\">\n" +
"      <AND>\n" +
"        <InternalTrigger name=\"RNDM1\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP3\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"157\" partition=\"1\" name=\"L1_RD0_FIRSTEMPTY\" complex_deadtime=\"0\" definition=\"(RNDM0&amp;BGRP0&amp;BGRP6)\" trigger_type=\"10000001\">\n" +
"      <AND>\n" +
"        <InternalTrigger name=\"RNDM0\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP6\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"158\" partition=\"1\" name=\"L1_LUCID\" complex_deadtime=\"0\" definition=\"((LUCID_A[x1]|LUCID_C[x1])&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <OR>\n" +
"          <TriggerCondition multi=\"1\" name=\"LUCID_A_x1\" triggerthreshold=\"LUCID_A\"/>\n" +
"          <TriggerCondition multi=\"1\" name=\"LUCID_C_x1\" triggerthreshold=\"LUCID_C\"/>\n" +
"        </OR>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"159\" partition=\"1\" name=\"L1_LUCID_EMPTY\" complex_deadtime=\"0\" definition=\"((LUCID_A[x1]|LUCID_C[x1])&amp;BGRP0&amp;BGRP3)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <OR>\n" +
"          <TriggerCondition multi=\"1\" name=\"LUCID_A_x1\" triggerthreshold=\"LUCID_A\"/>\n" +
"          <TriggerCondition multi=\"1\" name=\"LUCID_C_x1\" triggerthreshold=\"LUCID_C\"/>\n" +
"        </OR>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP3\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"160\" partition=\"1\" name=\"L1_LUCID_UNPAIRED_ISO\" complex_deadtime=\"0\" definition=\"((LUCID_A[x1]|LUCID_C[x1])&amp;BGRP0&amp;BGRP4)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <OR>\n" +
"          <TriggerCondition multi=\"1\" name=\"LUCID_A_x1\" triggerthreshold=\"LUCID_A\"/>\n" +
"          <TriggerCondition multi=\"1\" name=\"LUCID_C_x1\" triggerthreshold=\"LUCID_C\"/>\n" +
"        </OR>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP4\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"161\" partition=\"1\" name=\"L1_LUCID_A_C_EMPTY\" complex_deadtime=\"0\" definition=\"(LUCID_A[x1]&amp;LUCID_C[x1]&amp;BGRP0&amp;BGRP3)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"LUCID_A_x1\" triggerthreshold=\"LUCID_A\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"LUCID_C_x1\" triggerthreshold=\"LUCID_C\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP3\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"162\" partition=\"1\" name=\"L1_LUCID_A_C_UNPAIRED_ISO\" complex_deadtime=\"0\" definition=\"(LUCID_A[x1]&amp;LUCID_C[x1]&amp;BGRP0&amp;BGRP4)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"LUCID_A_x1\" triggerthreshold=\"LUCID_A\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"LUCID_C_x1\" triggerthreshold=\"LUCID_C\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP4\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"163\" partition=\"1\" name=\"L1_LUCID_A_C_UNPAIRED_NONISO\" complex_deadtime=\"0\" definition=\"(LUCID_A[x1]&amp;LUCID_C[x1]&amp;BGRP0&amp;BGRP5)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"LUCID_A_x1\" triggerthreshold=\"LUCID_A\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"LUCID_C_x1\" triggerthreshold=\"LUCID_C\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP5\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"164\" partition=\"1\" name=\"L1_BGRP7\" complex_deadtime=\"0\" definition=\"(BGRP0&amp;BGRP7)\" trigger_type=\"10000001\">\n" +
"      <AND>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP7\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"165\" partition=\"1\" name=\"L1_BCM_Wide_BGRP0\" complex_deadtime=\"0\" definition=\"(BCM_Wide[x1]&amp;BGRP0)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"BCM_Wide_x1\" triggerthreshold=\"BCM_Wide\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"166\" partition=\"1\" name=\"L1_BCM_AC_CA_BGRP0\" complex_deadtime=\"0\" definition=\"((BCM_AtoC[x1]|BCM_CtoA[x1])&amp;BGRP0)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <OR>\n" +
"          <TriggerCondition multi=\"1\" name=\"BCM_AtoC_x1\" triggerthreshold=\"BCM_AtoC\"/>\n" +
"          <TriggerCondition multi=\"1\" name=\"BCM_CtoA_x1\" triggerthreshold=\"BCM_CtoA\"/>\n" +
"        </OR>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"167\" partition=\"1\" name=\"L1_BCM_Wide_EMPTY\" complex_deadtime=\"0\" definition=\"(BCM_Wide[x1]&amp;BGRP0&amp;BGRP3)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"BCM_Wide_x1\" triggerthreshold=\"BCM_Wide\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP3\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"168\" partition=\"1\" name=\"L1_BCM_Wide_UNPAIRED_ISO\" complex_deadtime=\"0\" definition=\"(BCM_Wide[x1]&amp;BGRP0&amp;BGRP4)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"BCM_Wide_x1\" triggerthreshold=\"BCM_Wide\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP4\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"169\" partition=\"1\" name=\"L1_BCM_Wide_UNPAIRED_NONISO\" complex_deadtime=\"0\" definition=\"(BCM_Wide[x1]&amp;BGRP0&amp;BGRP5)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"BCM_Wide_x1\" triggerthreshold=\"BCM_Wide\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP5\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"170\" partition=\"1\" name=\"L1_BCM_AC_CA_UNPAIRED_ISO\" complex_deadtime=\"0\" definition=\"((BCM_AtoC[x1]|BCM_CtoA[x1])&amp;BGRP0&amp;BGRP4)\" trigger_type=\"10100000\">\n" +
"      <AND>\n" +
"        <OR>\n" +
"          <TriggerCondition multi=\"1\" name=\"BCM_AtoC_x1\" triggerthreshold=\"BCM_AtoC\"/>\n" +
"          <TriggerCondition multi=\"1\" name=\"BCM_CtoA_x1\" triggerthreshold=\"BCM_CtoA\"/>\n" +
"        </OR>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP4\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"171\" partition=\"1\" name=\"L1_0DR04-MU4ab-CJ15ab\" complex_deadtime=\"0\" definition=\"(0DR04-MU4ab-CJ15ab[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"0DR04-MU4ab-CJ15ab_x1\" triggerthreshold=\"0DR04-MU4ab-CJ15ab\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"172\" partition=\"1\" name=\"L1_0DR04-MU4ab-CJ30ab\" complex_deadtime=\"0\" definition=\"(0DR04-MU4ab-CJ30ab[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"0DR04-MU4ab-CJ30ab_x1\" triggerthreshold=\"0DR04-MU4ab-CJ30ab\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"173\" partition=\"1\" name=\"L1_0DR04-MU6ab-CJ25ab\" complex_deadtime=\"0\" definition=\"(0DR04-MU6ab-CJ25ab[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"0DR04-MU6ab-CJ25ab_x1\" triggerthreshold=\"0DR04-MU6ab-CJ25ab\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"174\" partition=\"1\" name=\"L1_0DR04-MU4ab-CJ17ab\" complex_deadtime=\"0\" definition=\"(0DR04-MU4ab-CJ17ab[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"0DR04-MU4ab-CJ17ab_x1\" triggerthreshold=\"0DR04-MU4ab-CJ17ab\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"175\" partition=\"1\" name=\"L1_0DR04-MU4ab-CJ20ab\" complex_deadtime=\"0\" definition=\"(0DR04-MU4ab-CJ20ab[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"0DR04-MU4ab-CJ20ab_x1\" triggerthreshold=\"0DR04-MU4ab-CJ20ab\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"176\" partition=\"1\" name=\"L1_J40_10MINDPHI-Js2-XE50\" complex_deadtime=\"0\" definition=\"(J40[x1]&amp;10MINDPHI-Js2-XE50[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J40_x1\" triggerthreshold=\"J40\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"10MINDPHI-Js2-XE50_x1\" triggerthreshold=\"10MINDPHI-Js2-XE50\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"177\" partition=\"1\" name=\"L1_J40_10MINDPHI-J20s2-XE50\" complex_deadtime=\"0\" definition=\"(J40[x1]&amp;10MINDPHI-J20s2-XE50[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J40_x1\" triggerthreshold=\"J40\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"10MINDPHI-J20s2-XE50_x1\" triggerthreshold=\"10MINDPHI-J20s2-XE50\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"178\" partition=\"1\" name=\"L1_J40_10MINDPHI-J20ab-XE50\" complex_deadtime=\"0\" definition=\"(J40[x1]&amp;10MINDPHI-J20ab-XE50[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J40_x1\" triggerthreshold=\"J40\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"10MINDPHI-J20ab-XE50_x1\" triggerthreshold=\"10MINDPHI-J20ab-XE50\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"179\" partition=\"1\" name=\"L1_J40_10MINDPHI-CJ20ab-XE50\" complex_deadtime=\"0\" definition=\"(J40[x1]&amp;10MINDPHI-CJ20ab-XE50[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"J40_x1\" triggerthreshold=\"J40\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"10MINDPHI-CJ20ab-XE50_x1\" triggerthreshold=\"10MINDPHI-CJ20ab-XE50\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"180\" partition=\"1\" name=\"L1_HT0-AJ0all.ETA49\" complex_deadtime=\"0\" definition=\"(HT0-AJ0all.ETA49[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HT0-AJ0all.ETA49_x1\" triggerthreshold=\"HT0-AJ0all.ETA49\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"181\" partition=\"1\" name=\"L1_1INVM5-EMs2-EMall\" complex_deadtime=\"0\" definition=\"(1INVM5-EMs2-EMall[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"1INVM5-EMs2-EMall_x1\" triggerthreshold=\"1INVM5-EMs2-EMall\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"182\" partition=\"1\" name=\"L1_1INVM5-EM7s2-EMall\" complex_deadtime=\"0\" definition=\"(1INVM5-EM7s2-EMall[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"1INVM5-EM7s2-EMall_x1\" triggerthreshold=\"1INVM5-EM7s2-EMall\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"183\" partition=\"1\" name=\"L1_1INVM5-EM12s2-EMall\" complex_deadtime=\"0\" definition=\"(1INVM5-EM12s2-EMall[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"1INVM5-EM12s2-EMall_x1\" triggerthreshold=\"1INVM5-EM12s2-EMall\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"184\" partition=\"1\" name=\"L1_10MINDPHI-AJj15s2-XE0\" complex_deadtime=\"0\" definition=\"(10MINDPHI-AJj15s2-XE0[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"10MINDPHI-AJj15s2-XE0_x1\" triggerthreshold=\"10MINDPHI-AJj15s2-XE0\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"185\" partition=\"1\" name=\"L1_20MINDPHI-AJjs6-XE0\" complex_deadtime=\"0\" definition=\"(20MINDPHI-AJjs6-XE0[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"20MINDPHI-AJjs6-XE0_x1\" triggerthreshold=\"20MINDPHI-AJjs6-XE0\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"186\" partition=\"1\" name=\"L1_20MINDPHI-AJj15s2-XE0\" complex_deadtime=\"0\" definition=\"(20MINDPHI-AJj15s2-XE0[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"20MINDPHI-AJj15s2-XE0_x1\" triggerthreshold=\"20MINDPHI-AJj15s2-XE0\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"187\" partition=\"1\" name=\"L1_10MINDPHI-EM6s1-XE0\" complex_deadtime=\"0\" definition=\"(10MINDPHI-EM6s1-XE0[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"10MINDPHI-EM6s1-XE0_x1\" triggerthreshold=\"10MINDPHI-EM6s1-XE0\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"188\" partition=\"1\" name=\"L1_20MINDPHI-EM9s6-XE0\" complex_deadtime=\"0\" definition=\"(20MINDPHI-EM9s6-XE0[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"20MINDPHI-EM9s6-XE0_x1\" triggerthreshold=\"20MINDPHI-EM9s6-XE0\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"189\" partition=\"1\" name=\"L1_20MINDPHI-EM6s1-XE0\" complex_deadtime=\"0\" definition=\"(20MINDPHI-EM6s1-XE0[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"20MINDPHI-EM6s1-XE0_x1\" triggerthreshold=\"20MINDPHI-EM6s1-XE0\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"190\" partition=\"1\" name=\"L1_05RATIO-XE0-HT0-AJj15all.ETA49\" complex_deadtime=\"0\" definition=\"(05RATIO-XE0-HT0-AJj15all.ETA49[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"05RATIO-XE0-HT0-AJj15all.ETA49_x1\" triggerthreshold=\"05RATIO-XE0-HT0-AJj15all.ETA49\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"191\" partition=\"1\" name=\"L1_08RATIO-XE0-HT0-AJj0all.ETA49\" complex_deadtime=\"0\" definition=\"(08RATIO-XE0-HT0-AJj0all.ETA49[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"08RATIO-XE0-HT0-AJj0all.ETA49_x1\" triggerthreshold=\"08RATIO-XE0-HT0-AJj0all.ETA49\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"192\" partition=\"1\" name=\"L1_40RATIO2-XE0-HT0-AJj15all.ETA49\" complex_deadtime=\"0\" definition=\"(40RATIO2-XE0-HT0-AJj15all.ETA49[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"40RATIO2-XE0-HT0-AJj15all.ETA49_x1\" triggerthreshold=\"40RATIO2-XE0-HT0-AJj15all.ETA49\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"193\" partition=\"1\" name=\"L1_90RATIO2-XE0-HT0-AJj0all.ETA49\" complex_deadtime=\"0\" definition=\"(90RATIO2-XE0-HT0-AJj0all.ETA49[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"90RATIO2-XE0-HT0-AJj0all.ETA49_x1\" triggerthreshold=\"90RATIO2-XE0-HT0-AJj0all.ETA49\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"194\" partition=\"1\" name=\"L1_HT20-AJj0all.ETA49\" complex_deadtime=\"0\" definition=\"(HT20-AJj0all.ETA49[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"HT20-AJj0all.ETA49_x1\" triggerthreshold=\"HT20-AJj0all.ETA49\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"195\" partition=\"1\" name=\"L1_NOT-02MATCH-EM9s1-AJj15all.ETA49\" complex_deadtime=\"0\" definition=\"(NOT-02MATCH-EM9s1-AJj15all.ETA49[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"NOT-02MATCH-EM9s1-AJj15all.ETA49_x1\" triggerthreshold=\"NOT-02MATCH-EM9s1-AJj15all.ETA49\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"196\" partition=\"1\" name=\"L1_NOT-02MATCH-EM9s1-AJj15all.ETA49_05RATIO-XE0-SUM0-EM9s1-HT0-AJj15all.ETA49\" complex_deadtime=\"0\" definition=\"(NOT-02MATCH-EM9s1-AJj15all.ETA49[x1]&amp;05RATIO-XE0-SUM0-EM9s1-HT0-AJj15all.ETA49[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"NOT-02MATCH-EM9s1-AJj15all.ETA49_x1\" triggerthreshold=\"NOT-02MATCH-EM9s1-AJj15all.ETA49\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"05RATIO-XE0-SUM0-EM9s1-HT0-AJj15all.ETA49_x1\" triggerthreshold=\"05RATIO-XE0-SUM0-EM9s1-HT0-AJj15all.ETA49\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"197\" partition=\"1\" name=\"L1_2DR15-2MU4ab\" complex_deadtime=\"0\" definition=\"(2DR15-2MU4ab[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"2DR15-2MU4ab_x1\" triggerthreshold=\"2DR15-2MU4ab\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"198\" partition=\"1\" name=\"L1_2INVM999-2MU4ab\" complex_deadtime=\"0\" definition=\"(2INVM999-2MU4ab[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"2INVM999-2MU4ab_x1\" triggerthreshold=\"2INVM999-2MU4ab\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"199\" partition=\"1\" name=\"L1_4INVM8-2MU4ab\" complex_deadtime=\"0\" definition=\"(4INVM8-2MU4ab[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"4INVM8-2MU4ab_x1\" triggerthreshold=\"4INVM8-2MU4ab\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"200\" partition=\"1\" name=\"L1_2DR15-2MU4ab_2INVM999-2MU4ab\" complex_deadtime=\"0\" definition=\"(2DR15-2MU4ab[x1]&amp;2INVM999-2MU4ab[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"2DR15-2MU4ab_x1\" triggerthreshold=\"2DR15-2MU4ab\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"2INVM999-2MU4ab_x1\" triggerthreshold=\"2INVM999-2MU4ab\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"201\" partition=\"1\" name=\"L1_2DR15-2MU4ab_4INVM8-2MU4ab\" complex_deadtime=\"0\" definition=\"(2DR15-2MU4ab[x1]&amp;4INVM8-2MU4ab[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"2DR15-2MU4ab_x1\" triggerthreshold=\"2DR15-2MU4ab\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"4INVM8-2MU4ab_x1\" triggerthreshold=\"4INVM8-2MU4ab\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"202\" partition=\"1\" name=\"L1_2DR15-2MU6ab\" complex_deadtime=\"0\" definition=\"(2DR15-2MU6ab[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"2DR15-2MU6ab_x1\" triggerthreshold=\"2DR15-2MU6ab\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"203\" partition=\"1\" name=\"L1_2INVM999-2MU6ab\" complex_deadtime=\"0\" definition=\"(2INVM999-2MU6ab[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"2INVM999-2MU6ab_x1\" triggerthreshold=\"2INVM999-2MU6ab\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"204\" partition=\"1\" name=\"L1_4INVM8-2MU6ab\" complex_deadtime=\"0\" definition=\"(4INVM8-2MU6ab[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"4INVM8-2MU6ab_x1\" triggerthreshold=\"4INVM8-2MU6ab\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"205\" partition=\"1\" name=\"L1_2DR15-2MU6ab_2INVM999-2MU6ab\" complex_deadtime=\"0\" definition=\"(2DR15-2MU6ab[x1]&amp;2INVM999-2MU6ab[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"2DR15-2MU6ab_x1\" triggerthreshold=\"2DR15-2MU6ab\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"2INVM999-2MU6ab_x1\" triggerthreshold=\"2INVM999-2MU6ab\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"    <TriggerItem ctpid=\"206\" partition=\"1\" name=\"L1_2DR15-2MU6ab_4INVM8-2MU6ab\" complex_deadtime=\"0\" definition=\"(2DR15-2MU6ab[x1]&amp;4INVM8-2MU6ab[x1]&amp;BGRP0&amp;BGRP1)\" trigger_type=\"10000000\">\n" +
"      <AND>\n" +
"        <TriggerCondition multi=\"1\" name=\"2DR15-2MU6ab_x1\" triggerthreshold=\"2DR15-2MU6ab\"/>\n" +
"        <TriggerCondition multi=\"1\" name=\"4INVM8-2MU6ab_x1\" triggerthreshold=\"4INVM8-2MU6ab\"/>\n" +
"        <InternalTrigger name=\"BGRP0\"/>\n" +
"        <InternalTrigger name=\"BGRP1\"/>\n" +
"      </AND>\n" +
"    </TriggerItem>\n" +
"  </TriggerMenu>\n";
        String prescaleXml = 
"  <PrescaleSet name=\"Physics_pp_v5_default_prescale\" type=\"Physics\" menuPartition=\"0\">\n" +
"    <Prescale ctpid=\"0\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"1\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"2\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"3\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"4\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"5\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"6\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"7\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"8\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"9\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"10\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"11\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"12\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"13\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"14\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"15\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"16\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"17\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"18\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"19\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"20\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"21\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"22\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"23\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"24\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"25\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"26\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"27\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"28\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"29\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"30\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"31\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"32\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"33\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"34\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"35\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"36\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"37\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"38\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"39\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"40\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"41\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"42\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"43\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"44\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"45\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"46\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"47\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"48\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"49\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"50\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"51\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"52\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"53\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"54\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"55\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"56\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"57\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"58\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"59\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"60\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"61\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"62\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"63\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"64\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"65\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"66\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"67\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"68\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"69\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"70\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"71\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"72\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"73\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"74\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"75\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"76\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"77\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"78\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"79\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"80\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"81\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"82\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"83\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"84\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"85\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"86\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"87\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"88\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"89\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"90\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"91\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"92\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"93\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"94\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"95\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"96\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"97\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"98\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"99\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"100\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"101\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"102\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"103\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"104\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"105\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"106\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"107\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"108\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"109\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"110\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"111\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"112\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"113\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"114\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"115\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"116\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"117\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"118\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"119\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"120\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"121\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"122\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"123\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"124\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"125\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"126\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"127\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"128\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"129\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"130\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"131\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"132\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"133\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"134\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"135\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"136\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"137\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"138\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"139\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"140\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"141\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"142\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"143\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"144\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"145\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"146\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"147\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"148\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"149\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"150\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"151\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"152\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"153\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"154\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"155\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"156\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"157\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"158\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"159\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"160\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"161\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"162\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"163\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"164\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"165\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"166\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"167\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"168\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"169\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"170\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"171\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"172\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"173\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"174\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"175\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"176\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"177\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"178\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"179\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"180\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"181\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"182\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"183\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"184\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"185\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"186\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"187\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"188\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"189\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"190\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"191\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"192\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"193\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"194\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"195\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"196\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"197\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"198\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"199\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"200\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"201\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"202\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"203\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"204\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"205\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"206\" cut=\"000001\" value=\"1\"/>\n" +
"    <Prescale ctpid=\"207\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"208\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"209\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"210\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"211\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"212\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"213\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"214\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"215\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"216\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"217\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"218\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"219\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"220\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"221\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"222\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"223\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"224\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"225\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"226\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"227\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"228\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"229\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"230\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"231\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"232\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"233\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"234\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"235\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"236\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"237\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"238\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"239\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"240\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"241\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"242\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"243\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"244\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"245\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"246\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"247\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"248\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"249\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"250\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"251\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"252\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"253\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"254\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"255\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"256\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"257\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"258\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"259\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"260\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"261\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"262\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"263\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"264\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"265\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"266\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"267\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"268\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"269\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"270\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"271\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"272\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"273\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"274\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"275\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"276\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"277\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"278\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"279\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"280\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"281\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"282\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"283\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"284\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"285\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"286\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"287\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"288\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"289\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"290\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"291\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"292\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"293\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"294\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"295\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"296\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"297\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"298\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"299\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"300\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"301\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"302\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"303\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"304\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"305\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"306\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"307\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"308\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"309\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"310\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"311\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"312\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"313\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"314\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"315\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"316\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"317\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"318\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"319\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"320\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"321\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"322\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"323\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"324\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"325\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"326\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"327\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"328\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"329\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"330\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"331\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"332\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"333\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"334\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"335\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"336\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"337\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"338\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"339\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"340\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"341\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"342\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"343\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"344\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"345\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"346\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"347\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"348\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"349\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"350\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"351\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"352\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"353\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"354\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"355\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"356\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"357\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"358\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"359\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"360\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"361\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"362\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"363\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"364\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"365\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"366\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"367\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"368\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"369\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"370\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"371\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"372\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"373\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"374\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"375\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"376\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"377\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"378\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"379\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"380\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"381\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"382\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"383\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"384\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"385\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"386\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"387\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"388\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"389\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"390\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"391\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"392\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"393\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"394\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"395\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"396\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"397\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"398\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"399\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"400\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"401\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"402\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"403\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"404\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"405\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"406\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"407\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"408\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"409\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"410\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"411\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"412\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"413\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"414\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"415\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"416\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"417\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"418\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"419\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"420\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"421\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"422\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"423\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"424\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"425\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"426\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"427\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"428\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"429\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"430\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"431\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"432\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"433\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"434\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"435\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"436\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"437\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"438\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"439\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"440\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"441\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"442\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"443\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"444\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"445\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"446\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"447\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"448\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"449\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"450\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"451\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"452\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"453\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"454\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"455\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"456\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"457\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"458\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"459\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"460\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"461\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"462\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"463\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"464\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"465\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"466\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"467\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"468\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"469\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"470\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"471\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"472\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"473\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"474\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"475\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"476\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"477\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"478\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"479\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"480\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"481\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"482\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"483\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"484\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"485\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"486\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"487\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"488\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"489\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"490\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"491\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"492\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"493\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"494\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"495\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"496\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"497\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"498\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"499\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"500\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"501\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"502\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"503\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"504\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"505\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"506\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"507\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"508\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"509\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"510\" cut=\"-000001\" value=\"-1\"/>\n" +
"    <Prescale ctpid=\"511\" cut=\"-000001\" value=\"-1\"/>\n" +
"  </PrescaleSet>\n" +
"</LVL1Config>\n";
        
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(MenuXml1+MenuXml2+prescaleXml));

        Document doc = db.parse(is);
       
        L1Prescale prescaleSet = reader.CreateL1Prescale(doc);
        
        assertEquals("Physics_pp_v5_default_prescale", prescaleSet.get_name());
        assertEquals("Physics", prescaleSet.get_type());
        for (int i = 1; i <= 207; ++i){
            assertEquals(1, (int)prescaleSet.get_val(1));
        }
        for (int i = 208; i <= L1Prescale.LENGTH; ++i){
            assertEquals(-1, (int)prescaleSet.get_val(i));
        }
    }
}
