/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggertool.XML;

import triggertool.XML.Writers.XMLWriter_L1PrescaleSet;
import org.junit.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import triggerdb.Entities.L1.L1Prescale;
import triggertool.L1Records.CutToValueConverter;

/**
 *
 * @author giannell
 */
public class XMLWriter_L1PrescaleSetTest {
    
    private L1Prescale prescaleSet;
    
    public XMLWriter_L1PrescaleSetTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void NegativeIdReturnBankString() {
        prescaleSet = new L1Prescale();
        XMLWriter_L1PrescaleSet writer = new XMLWriter_L1PrescaleSet(prescaleSet);
        String output = writer.Write();
        Assert.assertEquals("  <PrescaleSet name=\"Blank\" version=\"1\">\n  </PrescaleSet>", output);
    }
    
    @Test
    public void PositiveIdReturn512Elements() {
        prescaleSet = new L1Prescale();
        prescaleSet.set_name("TestName");
        prescaleSet.set_version(0);
        prescaleSet.set_id(10);
        prescaleSet.set_type("Physics");
        
        for (int i = 1; i <= L1Prescale.LENGTH; i++)
        {
            prescaleSet.set_val(i, 1);            
        }
        
        XMLWriter_L1PrescaleSet writer = new XMLWriter_L1PrescaleSet(prescaleSet);
        String output = writer.Write();
        String[] lines = output.split("\\r?\\n");
        
        // This is the actual line, at the moment we can only test the name and version 
        //Assert.assertEquals("  <PrescaleSet name=\"TestName\" type=\"Physics\" menuPartition=\"0\" version=\"0\">", lines[0]);
        Assert.assertEquals("  <PrescaleSet name=\"TestName\" type=\"Physics\" menuPartition=\"0\">", lines[0]);
        Assert.assertEquals("  </PrescaleSet>", lines[L1Prescale.LENGTH + 1]);
        
        for (int i = 0; i < L1Prescale.LENGTH; i++)
        {
            Assert.assertEquals("    <Prescale ctpid=\""+ i + "\" cut=\"000001\" value=\"1\"/>", lines[i+1]);
        }
    }
}
